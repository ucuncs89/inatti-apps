module.exports = {
  apps: [
    {
      name: "backend-staging",
      script: "nx",
      args: "serve backend --verbose"
    },
    {
      name: "backend-production",
      script: "nx",
      args: "serve backend --verbose"
    },
    {
      name: "template-pdf-production",
      script: "nx",
      args: "serve template-pdf --verbose",
      env: {
        PORT: process.env.NODE_PORT_TEMPLATE_PDF
      }
    },
    {
      name: "template-pdf-staging",
      script: "nx",
      args: "serve template-pdf --verbose",
      env: {
        PORT: process.env.NODE_PORT_TEMPLATE_PDF
      }
    },
  ]
}