import { Context } from "@nuxt/types"

export const refId = "refId"

/**
 * this middleware make apps to require query `refId`
 * ex: /home/?refId=123-24123-4123
 */

export default function (context: Context) {
  let excludeRoute = [
    "/login",
    "/login/app"
  ]
  let refIdQuery = context.query[refId]
  let refIdStore = context.store.state.refStore[refId]
  if (!refIdQuery) {
    if (refIdStore) {
      return context.redirect(context.route.path + "?refId=" + refIdStore)
    }
    if (!excludeRoute.includes(context.route.path)) {
      return context.redirect('/login/app');
    }
  } else if (!refIdStore) {
    context.store.commit('refStore/setRefId', refIdQuery)
  } else if (refIdStore !== refIdQuery) {
    context.store.commit('refStore/setRefId', refIdQuery)
  }
}