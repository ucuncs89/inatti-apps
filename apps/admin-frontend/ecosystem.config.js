module.exports = {
  apps: [
  {
    name   : "admin-inatti-prod",
    script : "yarn",
    args   : "start",
  },
  {
    name   : "admin-inatti-stag",
    script : "yarn",
    args   : "start",
  },
  ]
}
