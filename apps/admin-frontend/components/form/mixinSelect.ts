import { Component, Vue, Prop } from "vue-property-decorator"
import { VSelect } from "vuetify/lib"

@Component
export class MixinSelect extends VSelect {

  @Prop({ type: [Number, String, Object, Array] })
  value!: string

  get selected(): string {
    return this.value
  }
  set selected(val) {
    this.$emit('input', val)
  }

  listItems: any[] = []
  isLoading: boolean = false
}
export default MixinSelect