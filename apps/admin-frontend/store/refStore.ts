export const state = () => ({
  refId: null
})

export const mutations = {
  setRefId(state:any, refId:string){
    state.refId = refId
  }
}