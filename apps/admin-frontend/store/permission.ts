import { AdminApi } from "@inatti/shared/api-nuxt"
import { Context } from "@nuxt/types"

export const state = () => ({
  allowed: null,
  role: null
})

export const mutations = {
  setAllowed(state: any, payload: any){
    state.allowed = payload
  },
  setRole(state: any, payload:any){
    state.role = payload
  }
}

export const getters = {
  isReporter(state:any) {
    const REPORTER_ID = 2

    if(state.role){
      return state.role.id === REPORTER_ID
    }
    // deafult is reporter
    return true
  },
  isAdmin(state:any) {
    const ADMINROLE_ID = 1

    if(state.role){
      return state.role.id === ADMINROLE_ID
    }
    // deafult is not admin
    return false
  }
}

export const actions = {
  // `force` to force fetch data
  async fetchRole({ commit, state }:any, {context, force}: any){
    const { $axios }: Context = context
    
    // if force to fetch, continue
    if(state.allowed){
      if(!force){
        return false
      }
    }

    try {
      const adminApi = new AdminApi($axios)
      const result:any = await adminApi.getRoleUser()
      
      if (result.roleId) {        
        let {permission, ...role} = result.roleId
        commit('setRole', role)
        commit('setAllowed', permission)
        return result
      }else{
        throw new Error('No role selected')
      }
    }catch(error:any){
      console.log(error.response);
    }
  }
}