export const state = () => ({
  apotekGroup: {
    id: '',
    name: ''
  }
})

export const mutations = {
  setApotekGroup(state: any, apotekGroup: any){
    state.apotekGroup = {
      id: apotekGroup.id,
      name: apotekGroup.name
    }
  }
}