export default () => ({
  hostFrontend: 'https://inatti.id',
  userUrl: process.env.URL_FRONTEND_USER || 'https://app.inatti.id',
  apotekUrl: 'https://apotek.inatti.id',
})
