export interface FixtureFileType {
  entity: string
  items: {
    [name: string]:any
  }
}