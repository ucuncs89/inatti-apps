import { readFile } from 'fs';
import * as path from 'path';
import * as YAML from 'yaml'
import { FixtureFileType } from './fixtures-type';

export const getFixture = (name: string): FixtureFileType => {
  const locateFileFixture = path.resolve('apps/backend/test/fixtures/' + name + '.fixture.yml')
  const fileFixture = require('fs').readFileSync(locateFileFixture, 'utf8')
  if (!fileFixture) {
    throw new Error(`Fixture ${name}.fixture.yml not found`)
  }
  return YAML.parse(fileFixture)
}