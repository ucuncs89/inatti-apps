import * as path from 'path';
import { Connection, getConnection, getRepository } from "typeorm"
import { Builder, fixturesIterator, Loader, Parser, Resolver } from 'typeorm-fixtures-cli/dist';

export interface OptionsFixture {
  pathFixture: string
}

export const loadFixture = async (connection: Connection, options?: OptionsFixture) => {
  options = {
    pathFixture: './test/fixtures',
    ...options,
  }

  const pathFixture = path.resolve(options.pathFixture)
  
  const loader = new Loader()
  loader.load(pathFixture)
  
  const resolver = new Resolver()
  const fixtures = resolver.resolve(loader.fixtureConfigs)
  const builder = new Builder(connection, new Parser())

  for (const fixture of fixturesIterator(fixtures)) {
    const entity = await builder.build(fixture);
    await getRepository(entity.constructor.name).save(entity);
  }
}