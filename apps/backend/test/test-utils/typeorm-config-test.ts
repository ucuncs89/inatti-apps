import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";

export function TypeormConfigTest(entities: any[]){
  return [
    ConfigModule.forRoot({
      envFilePath: '.env.test'
    }),
    TypeOrmModule.forRoot({
      type: "postgres",
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities,
      synchronize: true,
      keepConnectionAlive: true,
    }),
    TypeOrmModule.forFeature(entities)
  ]
}