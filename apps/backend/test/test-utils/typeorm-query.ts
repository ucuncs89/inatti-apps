import { Connection, getRepository } from "typeorm"

export const cleanTable = async (connection: Connection, tableName: string|string[]) => {
  if(Array.isArray(tableName)) {
    for(const name of tableName) {
      return await cleanTable(connection, name)
    }
  }
  return connection.query(`TRUNCATE ${tableName} RESTART IDENTITY CASCADE;`);
}