import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OtpService } from '../otp/otp.service';
import { requetResetPasswordDTO, updatePasswordDTO } from './type';

@Injectable()
export class ResetPasswordService {
  constructor(
    @InjectRepository(User)
    private UserRepository: Repository<User>,
    private otpService: OtpService
  ){}

  secureInfoUser(user: User){
    let newUser = new User()
    newUser.id = user.id
    newUser.fullname = user.fullname
    newUser.username = this.hideMiddleText(user.username)
    newUser.phoneNumber = this.hideMiddleText(user.phoneNumber)
    newUser.email = this.hideMiddleText(user.email)

    return newUser;
  }

  hideMiddleText(text:string){
    if(!text) return null
    let newText = "";
    for(let i = 0; i < text.length; i++){
      if(i < 3 || i > text.length - 5){
        newText += text[i];
      }else{
        newText += "*";
      }
    }
    return newText;
  }

  async searchAccount(username:string){
    let user = await this.UserRepository.findOne({
      where: {
        username,
      }
    })
    if(!user){
      throw `Username ${username} not found`
    }

    return this.secureInfoUser(user)
  }

  async requestResetPassword(params: requetResetPasswordDTO){
    let user = await this.UserRepository.findOne({
      where: {
        id: params.idUser
      }
    })
    if(!user){
      throw `User not found`
    }

    const otp = this.otpService.createOtp();

    await this.otpService.saveOtp(otp, user);

    let res:any
    if(params.sendTo === 'whatsapp'){
      res = await this.otpService.sendWhatsapp(user.username, otp)
    } else {
      res = await this.otpService.sendEmail({
        email: user.email,
        name: user.fullname
      }, otp)
    }

    return this.secureInfoUser(user)
  }

  async updatePassword(params: updatePasswordDTO){
    if(!params.otp || !params.newPassword || !params.idUser){
      throw "Request not valid";
    }

    let user = await this.UserRepository.findOne({
      id: params.idUser
    })
    if(!user) throw "User not found";
    try{
      await this.otpService.validateOtp(params.otp, user.username)
      user.password = params.newPassword
      await user.hashPassword()
      await this.UserRepository.save(user);
      return true;
    } catch(err:any){
      if(err instanceof Error){
        throw err.message;
      }
      throw err;
    }

  }
}
