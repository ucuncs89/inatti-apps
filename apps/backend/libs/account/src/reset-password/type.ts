import { ApiProperty } from "@nestjs/swagger";
import { SendToOtp } from "../otp/type";

export class requetResetPasswordDTO{
  @ApiProperty()
  idUser: string;
  @ApiProperty()
  sendTo: SendToOtp;
}

export class updatePasswordDTO {
  @ApiProperty()
  otp: string;
  @ApiProperty()
  idUser: string;
  @ApiProperty()
  newPassword: string;
}