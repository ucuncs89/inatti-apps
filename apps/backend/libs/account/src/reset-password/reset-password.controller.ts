import { BadRequestException, Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ResetPasswordService } from './reset-password.service';
import { requetResetPasswordDTO, updatePasswordDTO } from './type';

@ApiTags('Account/Reset password')
@Controller('reset-password')
export class ResetPasswordController {
  constructor(
    private resetPasswordService: ResetPasswordService
  ){}

  @Get('/search-account')
  async searchAccount(
    @Query('username')
    username: string,
  ){
    try{
      return await this.resetPasswordService.searchAccount(username);
    }catch(err){
      throw new BadRequestException(err);
    }
  }

  @Post('/request-reset-password')
  async requestResetPassword(
    @Body()
    body: requetResetPasswordDTO
  ){
    try{
      return await this.resetPasswordService.requestResetPassword(body);
    }catch(err){
      throw new BadRequestException(err);
    }
  }
  
  @Post('/update-password')
  async updatePassword(
    @Body()
    body: updatePasswordDTO
  ){
    try{
      return await this.resetPasswordService.updatePassword(body);
    }catch(err){
      throw new BadRequestException(err);
    }
  }
}
