import { ApiProperty } from "@nestjs/swagger";
// Same as user.entity.ts but without password
export class UserUpdateDTO {
  @ApiProperty()
  fullname?: string;

  @ApiProperty()
  username?: string;

  @ApiProperty()
  email?: string;

  @ApiProperty()
  isActive?: boolean;
}

export class PasswordUpdateDTO {
  @ApiProperty()
  oldPassword: string;

  @ApiProperty()
  newPassword: string;
}