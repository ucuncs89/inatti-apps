import { ConfigAppService } from '@app/config-app';
import { CONFIGAPP_CATEGORY, CONFIGAPP_NAME } from '@app/config-app/type';
import { Otp } from '@app/database/entity/user/otp.entity';
import { User } from '@app/database/entity/user/user.entity';
import { EmailServiceService } from '@app/email-service';
import { WHATSAPP_SERVICE } from '@app/email-service/whatsapp-service/whatsapp/type';
import { WhatsappService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class OtpService {
  constructor(
    private whatsappService: WhatsappService,
    private emailService: EmailServiceService,
    @InjectRepository(Otp)
    private otpRepository: Repository<Otp>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
    private configAppService: ConfigAppService,
  ) { }

  createOtp(): string {
    let lengthOtp = 6;
    let min = 10 ** (lengthOtp - 1);
    let max = (10 ** lengthOtp) - 1;
    return Math.floor(Math.random() * (max - min) + min).toString();
  }

  saveOtp(otp: string, user: User) {
    let validHour = 1;
    let newOtp = new Otp;
    newOtp.otp = otp;
    newOtp.owner = user;
    newOtp.validUntil = new Date(new Date().getTime() + validHour * 60 * 60 * 1000);
    return this.otpRepository.save(newOtp);
  }

  async validateOtp(otp: string, username: string): Promise<boolean> {
    let user = await this.UserRepository.findOne({ username: username });
    let otpEntity = await this.otpRepository.findOne({
      where: {
        otp: otp,
        owner: user
      },
      order: {
        createdAt: 'DESC'
      }
    });

    if (otpEntity) {
      if (otpEntity.isUsed) {
        throw new BadRequestException("OTP Sudah digunakan")
      }
      if (otpEntity.validUntil > new Date()) {
        otpEntity.isUsed = true;
        await this.otpRepository.save(otpEntity);
        return true;
      }
      throw new BadRequestException("Otp expired")
    }

    throw new BadRequestException('Otp tidak tersedia')
  }

  async sendWhatsapp(phoneNumber: string, otp: string) {
    let message = `OTP anda adalah ${otp}`;

    let configServiceOtp = await this.configAppService.getConfigOrInit(
      CONFIGAPP_NAME.WA_SERVICE_OTP,
      {
        value: WHATSAPP_SERVICE.Taptalk,
        category: CONFIGAPP_CATEGORY.WhatsappService
      }
    )

    const param = {
      phone: phoneNumber,
      message,
    }
    return await this.whatsappService.sendWhatsappChoose(configServiceOtp, param)
  }

  async sendEmail(user: { email: string, name: string }, otp: string) {
    return this.emailService.sendEmailOtp({
      email: user.email,
      fullname: user.name,
      otp,
    })
  }
}
