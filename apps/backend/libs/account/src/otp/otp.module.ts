import { Module } from '@nestjs/common';
import { OtpService } from './otp.service';
import { EmailServiceModule } from '@app/email-service';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { DatabaseModule } from "@app/database"
import { ConfigAppModule } from '@app/config-app';

@Module({
  imports: [
    DatabaseModule,
    EmailServiceModule,
    WhatsappServiceModule,
    ConfigAppModule,
  ],
  providers: [
    OtpService
  ],
  exports: [
    OtpService
  ]
})
export class OtpModule {}
