import { UserService } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { PasswordUpdateDTO } from './types';
import { compare } from 'bcrypt';
import { PasswordIncorrectException } from '@app/authentication/exceptions';
@Injectable()
export class AccountService {
  constructor(
    private userService: UserService
  ) { }

  async getDetailUser(id: string) {
    let user: User = await this.userService.findOne({ id });
    user.hideCredential();
    return user;
  }

  updateUser(userId: string, user: any) {
    // prevent update password without hash
    delete user.password;
    return this.userService.update(userId, user)
  }

  async updatePassword(userId: string, payload: PasswordUpdateDTO) {
    let user: User = await this.userService.findOne({ id: userId })
    let isPasswordValid: boolean = await compare(payload.oldPassword, user.password);
    if (!isPasswordValid) {
      throw new PasswordIncorrectException();
    }
    user.password = payload.newPassword;
    return await this.userService.update(userId, user);
  }
}
