import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ManageUserService } from '../manage-user.service';
import { ChangeDafaultPasswordDTO } from '../type';


@Controller('user-manage')
export class UserManageController {
  constructor(
    private manageUserService: ManageUserService,
  ){}

  @Get('verify-key')
  async verifyKey(
    @Query('userId')
    userId: string,
    @Query('key')
    key: string
  ) {
    let user = await this.manageUserService.isDefaultPassword(userId, key)
    if(!user){
      throw new Error("Key is not valid")
    }
    return user;
  }

  @Post('change-default-password')
  async changeDefaultPassword(
    @Body()
    body: ChangeDafaultPasswordDTO
  ){
    return this.manageUserService.updateDefaultpassword(body)
  }
}