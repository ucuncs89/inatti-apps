import { ConfigAppModule } from '@app/config-app';
import { DatabaseModule } from '@app/database';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { Module } from '@nestjs/common';
import { AdminManageUserController } from './admin-area/admin-manage-user.controller';
import { ManageUserService } from './manage-user.service';
import { UserManageController } from './user-area/user-manage.controller';

/**
 * Manage account user anonymous
 */
@Module({
  imports: [
    DatabaseModule,
    WhatsappServiceModule,
    ConfigAppModule,
  ],
  controllers: [
    AdminManageUserController,
    UserManageController,
  ],
  providers: [
    ManageUserService
  ]
})
export class UserManageModule {}
