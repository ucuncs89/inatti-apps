import { ApiProperty } from "@nestjs/swagger";

export class UserCreateDTO{
  @ApiProperty()
  fullname: string;

  @ApiProperty()
  phonenumber: string;

  @ApiProperty({nullable:true})
  email?: string;
}

export class UserSearchDTO {
  @ApiProperty({required: false})
  username: string
  @ApiProperty({required: false})
  fullname: string
}

export class ChangeDafaultPasswordDTO {
  @ApiProperty()
  id: string
  @ApiProperty()
  key: string
  @ApiProperty()
  newPassword: string
}