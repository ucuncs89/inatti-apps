import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ManageUserService } from '../manage-user.service';
import { UserCreateDTO, UserSearchDTO } from '../type';
import * as _ from "lodash";

/**
 * All app can access this feature
 */
@RequireAuthApp(...AppsExcludeUser)
@Controller('admin/user-manage')
export class AdminManageUserController {
  constructor(
    private adminManageUserService: ManageUserService,
  ){}

  @Get('find')
  async findUser(
    @Query()
    query: UserSearchDTO,
  ){
    return await this.adminManageUserService.find(query)
  }

  @Post('create')
  async createUser(
    @Body()
    body: UserCreateDTO
  ){
    console.log('reade create user', body)
    let {user, key} = await this.adminManageUserService.create(body)
    console.log(`finish create user `, user, key)
    let message = await this.adminManageUserService.sendWhatsapp(user.id, user.phoneNumber, key)
    return Object.assign({}, message, {user: _.pick(user, ['id', 'fullname', 'username'])})
  }
}