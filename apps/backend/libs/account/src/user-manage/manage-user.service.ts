import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { ChangeDafaultPasswordDTO, UserCreateDTO, UserSearchDTO } from './type';
import * as crypto from 'crypto'
import { UserService } from '@app/database/entity/user/user.entities.services';
import * as bcrypt from 'bcrypt'
import { ConfigAppService, CONFIGAPP_CATEGORY, CONFIGAPP_NAME } from '@app/config-app';
import { WHATSAPP_SERVICE } from '@app/email-service/whatsapp-service/whatsapp/type';
import { WhatsappService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp.service';
import projectConfig from 'config/project.config'

@Injectable()
export class ManageUserService {
  constructor(
    @InjectRepository(User)
    private UserRepository: Repository<User>,
    private userService: UserService,
    private whatsappService: WhatsappService,
    private configAppService: ConfigAppService,
  ) { }
  prefixDefaultPassword = "default-password-inatti:"

  async find(query: UserSearchDTO) {
    let res = await this.UserRepository.find({
      where: [
        {
          username: ILike(`%${query.username}%`)
        },
        {
          fullname: ILike(`%${query.fullname}%`)
        }
      ],
    })
    res.forEach((user) => user.hideCredential())
    return res;
  }

  async create(data: UserCreateDTO) {
    let user = new User()
    user.fullname = data.fullname
    user.username = data.phonenumber
    user.phoneNumber = data.phonenumber
    user.email = data.email
    const key = this.genDefaultPassword()
    user.password = key
    let newUser = await this.userService.create(user)
    newUser.hideCredential()
    // key pure key
    let cutKey = key.slice(this.prefixDefaultPassword.length, key.length)
    return {
      user: newUser,
      key: cutKey
    };
  }

  /**
   * Update default password user, key is the default password
   * @param username 
   * @param key 
   */
  async updateDefaultpassword(payload: ChangeDafaultPasswordDTO) {
    let user = await this.isDefaultPassword(payload.id, payload.key)

    // if match, return and change password
    user.password = payload.newPassword
    user = await this.userService.update(user.id, user)
    user.hideCredential()
    return user;
  }

  async isDefaultPassword(id: string, key: string) {
    let user
    try {
      user = await this.userService.findOne({ id })
    } catch (err) {
      throw new BadRequestException('User not found')
    }
    if (!user) throw new BadRequestException('User not found')
    let isVerified = await this.verifyDefaultPassword(key, user.password)
    if (!isVerified) {
      throw new BadRequestException("Key is not valid")
    }
    user.hideCredential()
    return user;
  }

  async verifyDefaultPassword(password: string, hash: string) {
    let passwordLong = this.prefixDefaultPassword + password
    return await bcrypt.compare(passwordLong, hash)
  }

  genDefaultPassword() {
    return this.prefixDefaultPassword + this.randomKey()
  }

  randomKey(length?: number) {
    return crypto.randomBytes(length ?? 32).toString('hex')
  }

  /**
   * @param username Should a phonenumber
   * @param key 
   * @returns 
   */
  async sendWhatsapp(userId: string, phonenumber: string, key: string) {
    console.log('ready send whatsapp', userId, key)
    let hostFrontend = projectConfig().userUrl
    let urlKey = `${hostFrontend}/auth/update-default?uid=${userId}&key=${key}`
    let message = `Akun anda telah dibuat, buka ${urlKey} untuk memulai akun inatti`;

    let configServiceOtp = await this.configAppService.getConfigOrInit(
      CONFIGAPP_NAME.WA_SERVICE_OTP,
      {
        value: WHATSAPP_SERVICE.Taptalk,
        category: CONFIGAPP_CATEGORY.WhatsappService
      }
    )
    console.log('config srvice otp', configServiceOtp)

    const param = {
      phone: phonenumber,
      message,
    }
    try {
      let res = await this.whatsappService.sendWhatsappChoose(configServiceOtp, param)
      console.log(`finish send whatsaatp`, res)
      return {
        status: 'success send whatsapp',
        phonenumber,
        message,
      }
    } catch (err) {
      return {
        status: 'Account created, but failed send whatsapp',
        phonenumber,
        message,
      }
    }
  }
}