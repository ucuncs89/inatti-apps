import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account/account.controller';
import { ResetPasswordController } from './reset-password/reset-password.controller';
import { ResetPasswordService } from './reset-password/reset-password.service';
import { OtpModule } from './otp/otp.module';
import { UserManageModule } from './user-manage/user-manage.module';

@Module({
  imports: [DatabaseModule, OtpModule, UserManageModule],
  providers: [AccountService, ResetPasswordService],
  controllers: [AccountController, ResetPasswordController],
  exports: [AccountService, OtpModule],
})
export class AccountModule { }
