import { JwtAuthGuard } from '@app/authentication/authorization/jwt.authguard';
import { Body, Controller, Get, NotAcceptableException, Request, UseGuards, Patch } from '@nestjs/common';
import { AccountService } from '../account.service';
import { PasswordUpdateDTO, UserUpdateDTO } from '../types';
import { ApiTags, ApiBearerAuth, ApiNotAcceptableResponse, ApiOkResponse } from '@nestjs/swagger';
@ApiTags("Account")
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account')
export class AccountController {
  constructor(
    private accountService: AccountService
  ) { }


  @Get('me')
  userDetail(@Request() req) {
    return this.accountService.getDetailUser(req.user.id)
  }

  @Patch('update')
  async userUpdateDetail(@Request() req, @Body() body: UserUpdateDTO) {
    await this.accountService.updateUser(req.user.id, body);
    return "Success update"
  }

  @ApiNotAcceptableResponse({ description: "Password incorrect" })
  @ApiOkResponse({ description: "success update password" })
  @Patch('update/password')
  async updatePassword(@Request() req, @Body() body: PasswordUpdateDTO) {
    try {
      await this.accountService.updatePassword(req.user.id, body);
    } catch (error) {
      throw new NotAcceptableException(error.message);
    }
  }
}
