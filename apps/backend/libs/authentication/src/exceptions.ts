export class PasswordIncorrectException extends Error {
  name = "PasswordIncorrectException";
  message = "Password incorrect";
}

export class UserNotFoundException extends Error {
  constructor(msg?:any){
    super(msg)
    this.message = msg
  }
  name = "UserNotFoundException"
  message = "User not found"
}