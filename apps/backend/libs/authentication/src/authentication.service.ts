import { ForbiddenException, Injectable, Logger } from '@nestjs/common';
import { UserService } from '@app/database/entity/user/user.entities.services';
import { compare } from 'bcrypt';
import { User } from '@app/database/entity/user/user.entity';
import { JwtService, JwtSignOptions } from "@nestjs/jwt";
import { PasswordIncorrectException, UserNotFoundException } from './exceptions';
import { PayloadJwt } from './types';
import { AppsEnum } from '@app/database/entity/apps/apps.type'
import { UserAppsService } from './services/userapps.service';
import { RoleModel } from '@app/database/entity/role/role.model';
@Injectable()
export class AuthenticationService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private userAppService: UserAppsService
  ) { }

  logger = new Logger('AuthenticationService')

  /**
   * Login Authenticate user based username, password and apps.
   * @param payload 
   * @returns 
   */
  async authenticateUser(payload: { username: string, password: string, apps: AppsEnum }) {
    const { username, password, apps } = payload

    // Validate username and password
    let user: User = await this.validateUser(username, password);
    if (!apps) {
      throw new Error("No login apps selected")
    }

    let userRole: RoleModel
    if (apps !== AppsEnum.User) {
      // verify if user exist in table usersapp
      userRole = await this.userAppService.getUserApp(user, apps);
      if (!userRole) {
        throw new ForbiddenException("Not allowed to login this app");
      }
    }

    // generate token
    return {
      user,
      token: await this.login(user, apps, userRole)
    };
  }

  async validateUser(username: string, password: string) {
    this.logger.log(`${username} try to login`)
    let user: User = await this.userService.findOne({ username });
    if (!user) {
      throw new UserNotFoundException(`User ${username} not found`);
    }
    this.logger.log(`user found with id : ${user.id}`)

    let isValidate = await compare(password, user.password)
    if (isValidate) {
      this.logger.log(`user is validated`)
      user.hideCredential();
      return user;
    } else {
      this.logger.error(`user password is incorrect`)
      throw new PasswordIncorrectException();
    }
  }

  /**
   * Generate Token Login
   */
  async login(user: User, loginApps: AppsEnum, userRole?: RoleModel, roleId?: string) {
    const payload: PayloadJwt = {
      username: user.username,
      sub: user.id,
      apps: loginApps,
      role: userRole,
      roleId: roleId
    }
    return {
      access_token: this.jwtService.sign(payload)
    }
  }

  /**
   * Generate token with flexiblity to set payload
   * @param user 
   * @param data flexible data format
   * @returns {access_token: string}
   */
  async loginV2(user: User, data?: {
    apps?: AppsEnum,
    role?: RoleModel,
    roleId?: string,
    [key: string]: any
  }, jwtOptions?: JwtSignOptions) {
    const payload: PayloadJwt = {
      username: user.username,
      sub: user.id,
      ...data
    }
    return {
      access_token: this.jwtService.sign(payload, jwtOptions)
    }
  }


  async userInformation(id: string): Promise<User> {
    this.logger.log(`searching user id ${id}`)
    let userInfo: User = await this.userService.findOne({ id });
    if (!userInfo) {
      throw new UserNotFoundException(`User id ${id} not found`);
    }
    userInfo.hideCredential();
    return userInfo;
  }
}