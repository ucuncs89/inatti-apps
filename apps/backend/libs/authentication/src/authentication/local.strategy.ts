import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { User } from "@app/database/entity/user/user.entity";
import { Inject, Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthenticationService } from "../authentication.service";
import { UserAppsService } from "../services/userapps.service";
import { ConfigAuth, ConfigAuthConst } from "../types";

/**
 * IMPORTANT :
 * This local strategy not used on any apps. This code only as template
 */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  // Need to define on controller login
  loginApps: AppsEnum;

  constructor(
    protected authService: AuthenticationService,
    protected userAppsService: UserAppsService,
    @Inject(ConfigAuthConst)
    options: ConfigAuth
  ) {
    super();
    this.loginApps = options.loginApps
  }


  async validate(username: string, password: string): Promise<any> {
    try {
      return await this.authService.authenticateUser({
        apps: this.loginApps,
        username,
        password,
      })
    } catch (error) {
      Logger.error(`Failed Login : ${error}`, 'LocalStrategy')
      throw new UnauthorizedException(error.message);
    }
  }

  async customValidateUser(user: User): Promise<any> {
    return user;
  }
}