import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { User } from "@app/database/entity/user/user.entity";
import { BadRequestException, Injectable, Logger } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthenticationService } from "../authentication.service";
import { UserAppsService } from "../services/userapps.service";

@Injectable()
export class LocalUserStrategy extends PassportStrategy(Strategy, 'user') {
  // Need to define on controller login
  loginApps: AppsEnum = AppsEnum.User;

  constructor(
    protected authService: AuthenticationService,
    protected userAppsService: UserAppsService,
  ) {
    super();
  }


  async validate(username: string, password: string): Promise<any> {
    try {
      return await this.authService.authenticateUser({
        apps: this.loginApps,
        username,
        password,
      })
    } catch (error) {
      Logger.error(`Failed Login : ${error}`, 'LocalUserStrategy')
      throw new BadRequestException(error);
    }
  }

  async customValidateUser(user: User): Promise<any> {
    return user;
  }
}