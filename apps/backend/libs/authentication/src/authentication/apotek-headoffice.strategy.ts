import { ApotekHeadofficeRoleUser } from "@app/database/entity/apotek-head-office/apotek-head-office-role-user.entity";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { User } from "@app/database/entity/user/user.entity";
import { BadRequestException, Injectable, Logger } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { Strategy } from "passport-local";
import { Repository } from "typeorm";
import { AuthenticationService } from "../authentication.service";
import { UserAppsService } from "../services/userapps.service";

@Injectable()
export class LocalApotekHeadOfficeStrategy extends PassportStrategy(Strategy, AppsEnum.ApotekHeadOffice) {
  // Need to define on controller login
  loginApps: AppsEnum = AppsEnum.ApotekHeadOffice;

  constructor(
    protected authService: AuthenticationService,
    protected userAppsService: UserAppsService,
    @InjectRepository(ApotekHeadofficeRoleUser)
    private ApotekHeadofficeRoleUserRepository: Repository<ApotekHeadofficeRoleUser>,
  ) {
    super();
  }


  async validate(username: string, password: string): Promise<any> {
    try {
      const res = await this.authService.authenticateUser({
        apps: this.loginApps,
        username,
        password,
      })

      // regenerate token based user role
      return await this.regenerateTokenBasedOnRole(res.user)
    } catch (error) {
      Logger.error(`Failed Login : ${error}`, 'LocalApotekHeadOfficeStrategy')
      throw new BadRequestException(error);
    }
  }

  async regenerateTokenBasedOnRole(user: User): Promise<{
    user: User,
    token: {
      access_token: string,
    },
  }> {
    const role = await this.ApotekHeadofficeRoleUserRepository.findOne({
      where: {
        userId: user.id,
      }
    })

    if (role) {
      return {
        user,
        token: await this.authService.login(
          user,
          this.loginApps,
          null,
          role.id + ""
        )
      }
    }
  }

  async customValidateUser(user: User): Promise<any> {
    return user;
  }
}