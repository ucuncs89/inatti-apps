import { AdminRoleUser } from "@app/database/entity/admin/admin-role-user.entity";
import { ApotekGroupRoleUser } from "@app/database/entity/apotek-group/apotek-group-role-user.entity";
import { ApotekHeadofficeRoleUser } from "@app/database/entity/apotek-head-office/apotek-head-office-role-user.entity";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { LabRoleUser } from "@app/database/entity/lab/lab-role-user.entity";
import { RefRoleUser } from "@app/database/entity/ref/ref-role-user.entity";
import { RoleModel } from "@app/database/entity/role/role.model";
import { User } from "@app/database/entity/user/user.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

type WhereRole = { userId?: User, id?: number }

@Injectable()
export class UserAppsService {
  constructor(
    @InjectRepository(RefRoleUser)
    private refRoleuserRepository: Repository<RefRoleUser>,
    @InjectRepository(LabRoleUser)
    private labRoleUserRepository: Repository<LabRoleUser>,
    @InjectRepository(AdminRoleUser)
    private AdminRoleUserRepository: Repository<AdminRoleUser>,
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
    @InjectRepository(ApotekGroupRoleUser)
    private ApotekGroupRoleUserRepository: Repository<ApotekGroupRoleUser>,
    @InjectRepository(ApotekHeadofficeRoleUser)
    private ApotekHeadofficeRoleUserRepository: Repository<ApotekHeadofficeRoleUser>,
  ) { }

  /**
   * This could be ambigious, because 1 user have some role.
   * this function just to check is user have access to apps
   * 
   * @param repository 
   * @param user 
   * @returns 
   */
  async getRoleUser(repository: Repository<unknown>, where: WhereRole) {
    const roleUser: any = await repository.findOne({
      where,
      relations: ['roleId'],
      loadEagerRelations: true
    })
    if (!roleUser) {
      throw new RoleUserNotFound();
    }
    return roleUser.roleId
  }

  async getUserApp(user: User, appId: AppsEnum): Promise<RoleModel | null> {
    const where = { userId: user }
    return this.getAppRoleUser(appId, where)
  }

  async getAppRoleUser(appId: AppsEnum, where: WhereRole) {
    if (appId === AppsEnum.Ref) {
      return await this.getRoleUser(this.refRoleuserRepository, where)
    } else if (appId === AppsEnum.Lab) {
      return await this.getRoleUser(this.labRoleUserRepository, where)
    } else if (appId === AppsEnum.Admin) {
      return await this.getRoleUser(this.AdminRoleUserRepository, where)
    } else if (appId === AppsEnum.Apotek) {
      return await this.getRoleUser(this.ApotekRoleUserRepository, where)
    } else if (appId === AppsEnum.ApotekGroup) {
      return await this.getRoleUser(this.ApotekGroupRoleUserRepository, where)
    } else if (appId === AppsEnum.ApotekHeadOffice) {
      return await this.getRoleUser(this.ApotekHeadofficeRoleUserRepository, where)
    }
    // TODO: Add another get role user based apps
    return null
  }
}

export class RoleUserNotFound extends Error {
  name = 'RoleUserNotFound'
  message: string
  constructor(msg?: string) {
    super();
    this.message = msg || 'Role user not found'
  }
}