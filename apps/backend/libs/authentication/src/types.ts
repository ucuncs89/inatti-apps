import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { RoleModel } from "@app/database/entity/role/role.model";
import { User } from "@app/database/entity/user/user.entity";
import { ApiProperty } from "@nestjs/swagger";

export interface ConfigAuth {
  loginApps: AppsEnum
}

export const ConfigAuthConst = "CONFIG_AUTHENTICATION"

export const JWTSecret = process.env.JWT_SECRET

export const JWTSecretOtpAuth = process.env.JWT_SECRET_OTP_AUTH

export type PayloadJwt<T = any> = {
  username: string,
  sub: string, // id user
  apps?: AppsEnum,
  /** @deprecated not used anymore */
  role?: RoleModel,
  /** RoleId can be usedd to flag role user */
  roleId?: string

  detailRole?: T
  [key: string]: any
}

export class LoginDTO {
  @ApiProperty({
    description: "08223047617 or email@gmail.com or username123",
    example: "sahaware"
  })
  username: string;

  @ApiProperty({
    example: "sahaware123"
  })
  password: string;
}

export interface UserTokenData<T = any> extends User {
  payload?: PayloadJwt<T>
}
