import { DatabaseModule } from '@app/database';
import { DynamicModule, Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { UserAppsService } from './services/userapps.service';
import { LocalStrategy } from './authentication/local.strategy';
import { ConfigAuthConst, ConfigAuth, JWTSecret } from './types';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './authorization/jwt.strategy';
import { JwtAuthGuard } from './authorization/jwt.authguard';
import { LocalLabStrategy } from './authentication/lab.strategy';
import { LocalRefStrategy } from './authentication/ref.strategy';
import { UserService } from '@app/database/entity/user/user.entities.services';
import { LocalUserStrategy } from './authentication/user.strategy';
import { LocalAdminStrategy } from './authentication/admin.strategy';
import { LocalApotekStrategy } from './authentication/apotek.strategy';
import { LocalApotekGroupStrategy } from './authentication/apotek-group.strategy';
import { JwtAuthWebsocket } from './authorization/jwt.authguard.websocket';
import { LocalApotekHeadOfficeStrategy } from './authentication/apotek-headoffice.strategy';
import { JwtOtpAuth } from './authorization/otp-auth/jwt.otp-auth';

/**
 * Authentication Module
 * How to use : 
 * Put this code to import target module : 
 * import : [AuthenticationModule.register({ loginApps: AppsEnum.Ref })]
 */
@Module({})
export class AuthenticationModule {
  static register(optionAuth: ConfigAuth): DynamicModule {
    return {
      module: AuthenticationModule,
      imports: [
        DatabaseModule,
        JwtModule.register({
          secret: JWTSecret,
          signOptions: { expiresIn: '7d' }
        })
      ],
      providers: [
        UserService,
        AuthenticationService,
        JwtStrategy,

        LocalLabStrategy,
        LocalRefStrategy,
        LocalUserStrategy,
        LocalAdminStrategy,
        LocalApotekStrategy,
        LocalApotekGroupStrategy,
        LocalApotekHeadOfficeStrategy,

        UserAppsService,
        LocalStrategy,
        JwtAuthGuard,
        JwtAuthWebsocket,
        {
          provide: ConfigAuthConst,
          useValue: optionAuth
        },
        JwtOtpAuth
      ],
      exports: [UserService, UserAppsService, AuthenticationService, JwtAuthWebsocket],
    }
  }
}