import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { UserTokenData } from "../types";

export const UserCred = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): UserTokenData => {
    const request = ctx.switchToHttp().getRequest();
    return request?.user;
  }
)