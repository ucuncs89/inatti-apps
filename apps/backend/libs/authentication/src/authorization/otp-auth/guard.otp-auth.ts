import { applyDecorators, Injectable, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiForbiddenResponse } from "@nestjs/swagger";

export function OtpAuthGuard() {
  return applyDecorators(
    UseGuards(AuthGuard('jwt-otp-auth')),
    ApiForbiddenResponse({
      description: `Token user can access`
    }),
    ApiBearerAuth()
  )
}