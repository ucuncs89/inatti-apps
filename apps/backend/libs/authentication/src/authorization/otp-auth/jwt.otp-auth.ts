import { Injectable } from '@nestjs/common';
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JWTSecretOtpAuth, PayloadJwt } from '../../types';
import { JwtStrategy } from '../jwt.strategy';

@Injectable()
export class JwtOtpAuth extends PassportStrategy(Strategy, 'jwt-otp-auth') {
  constructor(
    private jwtStrategyOriginal: JwtStrategy
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JWTSecretOtpAuth
    });
  }

  async validate(payload: PayloadJwt) {
    return await this.jwtStrategyOriginal.validate(payload)
  }
}
