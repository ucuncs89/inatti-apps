import { User } from "@app/database/entity/user/user.entity";
import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { WsException } from "@nestjs/websockets";
import * as jwt from 'jsonwebtoken';
import { Observable } from "rxjs";
import { AuthenticationService } from "../authentication.service";
import { JWTSecret, PayloadJwt } from '../types';

@Injectable()
export class JwtAuthWebsocket {
  constructor(
    private authenticationService: AuthenticationService
  ){ }

  async verifyWebsocket(client:any): Promise<User>{
    let token = this.getToken(client)
    try{
      let payload = this.verifyToken(token)
      let user = await this.validateTokenUser(payload)
      return user
    }catch(err){
      throw new Error('Token not valid')
    }
  }

  getToken(client:any){
    return client.handshake.query.access_token
  }
  
  verifyToken(token:string){
    return jwt.verify(token, JWTSecret)
  }

  async validateTokenUser(jwtPayload: PayloadJwt|any){
    try {
      let userInfo = await this.authenticationService.userInformation(jwtPayload.sub);
      return userInfo;
    } catch (err) {
      throw new Error('user not found');
    }
  }
}