import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

/**
 * This is AuthGuard for Admin apps.
 * Every apps need to create their own auth guard.
 * And this should not be extends from other auth guard
 */
@Injectable()
export class JwtAuthGuardAdmin extends AuthGuard('jwt') {
  handleRequest(err, user) {
    if (err || !user || !user.payload) {
      throw err || new UnauthorizedException();
    }

    if (user.payload.apps !== AppsEnum.Admin) {
      throw new UnauthorizedException();
    }

    return user
  }
}