import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

/**
 * This is AuthGuard for Apotek apps.
 * Every apps need to create their own auth guard.
 * And this should not be extends from other auth guard
 */
@Injectable()
export class JwtAuthGuardApotek extends AuthGuard('jwt') {
  logger = new Logger('JwtAuthGuardApotek')
  handleRequest(err, user) {
    this.logger.log('user access : '+ JSON.stringify(user))
    
    if (err || !user || !user.payload) {
      this.logger.error(`failed to access : ${err}`)
      throw err || new UnauthorizedException();
    }

    if (user.payload.apps !== AppsEnum.Apotek) {
      this.logger.error(`failed to access : token is not for Apotek`)
      throw new UnauthorizedException();
    }

    return user
  }
}