import { AppsEnum, AppsExcludeUser } from "@app/database/entity/apps/apps.type";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { RefRoleUser } from "@app/database/entity/ref/ref-role-user.entity";
import { LocalPdfAuthService } from "@app/report-pdf/local-pdf-auth/local-pdf-auth.service";
import { applyDecorators, ExecutionContext, Injectable, Logger, SetMetadata, UnauthorizedException, UseGuards } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiConsumes, ApiForbiddenResponse, ApiHeader, ApiOperation, ApiSecurity } from "@nestjs/swagger";
import { InjectRepository } from "@nestjs/typeorm";
import { ApiV1AuthName, getNameAuth } from "apps/main/src/helper/swagger-helper";
import { Request } from 'express';
import { Repository } from "typeorm";
import { UserTokenData } from "../types";
export const AuthAppName = 'AuthAppMetadata'

/**
 * Authguard for multiapp, why not implement this for looong time..
 * 
 */
@Injectable()
export class JwtAuthGuardMultiApp extends AuthGuard('jwt') {
  constructor(
    private reflector: Reflector,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
  ) {
    super()
  }

  canActivate(context: ExecutionContext) {
    const requiredRoleApps = this.reflector.getAllAndOverride<AppsEnum[]>(AuthAppName, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (requiredRoleApps.includes(AppsEnum.TemplatePdfWeb)) {
      let isValidTempltePdf = this.handleAuthTemplatePdf(context.switchToHttp().getRequest())
      if (isValidTempltePdf) {
        return true;
      }
    }

    return super.canActivate(context)
  }

  logger = new Logger('JwtAuthGuardMultiApp')
  // @ts-ignore
  async handleRequest(err, user: UserTokenData, info, context: ExecutionContext) {
    this.logger.log(`user access : ${user?.username}, Apps '${user?.payload?.apps}' with role id ${user?.payload?.roleId}`)

    const requiredRoleApps = this.reflector.getAllAndOverride<AppsEnum[]>(AuthAppName, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (err || !user || !user.payload) {
      this.logger.error(`failed to access : ${err}`)
      throw err || new UnauthorizedException();
    }

    // if no app, must logged in
    if (requiredRoleApps.length < 1) {
      return user;
    }

    if (!requiredRoleApps.includes(user.payload.apps)) {
      this.logger.error(`failed to access : token is only for ${requiredRoleApps.join(',')}`)
      throw new UnauthorizedException();
    }

    // if only one require app, its possible to fetch detail role
    if (requiredRoleApps.length === 1) {
      await this.fetchDetailRole(user)
    }

    return user
  }

  handleAuthTemplatePdf(request: Request) {
    let pdfAuth = new LocalPdfAuthService()
    return pdfAuth.validateAuthLocal(request)
  }

  async fetchDetailRole(user: UserTokenData) {
    if (!user.payload?.roleId) return;

    if (user.payload?.apps === AppsEnum.Ref) {
      user.payload.detailRole = await this.RefRoleUserRepository.findOne({
        where: {
          id: user.payload.roleId
        }
      })
    }
    return;
  }

}

export const AuthApps = (...apps: AppsEnum[]) => SetMetadata(AuthAppName, apps);

function getAppPermission(apps: AppsEnum[]) {
  if (apps.length < 1) {
    return "All authenticated user"
  }
  return apps.join(", ")
}

/**
 * Create API Security document for swagger
 */
function getApiSecurity(apps: AppsEnum[]) {
  const apiSecurity: any[] = []
  if (apps.length < 1) {
    apiSecurity.push(
      ApiBearerAuth()
    )
  } else {
    apps.forEach((app) => {
      apiSecurity.push(
        ApiSecurity(getNameAuth(app))
      )
    })
  }
  return apiSecurity
}

export function RequireAuthApp(...apps: AppsEnum[]) {

  return applyDecorators(
    UseGuards(JwtAuthGuardMultiApp),
    AuthApps(...apps),
    ApiForbiddenResponse({
      description: `Token user can access : ${getAppPermission(apps)}`
    }),
    ...getApiSecurity(apps)
  )
}