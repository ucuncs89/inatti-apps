import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from 'passport-jwt';
import { AuthenticationService } from '../authentication.service';
import { UserAppsService } from '../services/userapps.service';
import { ConfigAuth, ConfigAuthConst, JWTSecret, PayloadJwt, UserTokenData } from '../types';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(ConfigAuthConst)
    private options: ConfigAuth,
    private authenticationService: AuthenticationService,
    private userApps: UserAppsService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JWTSecret
    });
  }

  /**
   * Validation jwt start from here.
   * it will search user information that will used by controller/service
   * @param payload 
   * @returns 
   */
  async validate(payload: PayloadJwt) {
    try {
      const userInfo: UserTokenData = await this.authenticationService.userInformation(payload.sub);
      userInfo.payload = payload
      return userInfo;
    } catch (err) {
      throw new UnauthorizedException(err);
    }
  }
}
