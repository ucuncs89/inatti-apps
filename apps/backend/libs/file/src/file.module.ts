import { AwsModule } from '@app/aws';
import { Module } from '@nestjs/common';
import awsConfig from 'config/aws.config';
import { FileController } from './file.controller';
import { FileService } from './file.service';

@Module({
  imports: [
    AwsModule.register({
      bucket: awsConfig().aws.bucket,
      uploadFolder: 'upload-file-module'
    })
  ],
  controllers: [FileController],
  providers: [FileService],
  exports: [FileService],
})
export class FileModule {}
