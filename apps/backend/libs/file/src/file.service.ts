import { AwsService } from '@app/aws';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FileService {
  constructor(
    private awsService: AwsService
  ){}

  async upload(file: Express.Multer.File, user: User){
    let metadata = this.awsService.setMetadataUser(user)
    let fileUploaded = await this.awsService.HashAndUploadS3(file, null, metadata);
    return fileUploaded.s3
  }
}
