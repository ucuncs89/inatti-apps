import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { AwsService } from '@app/aws';
import { AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { Controller, Get, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { FileService } from './file.service';

/**
 * For now, this controller is used except Users
 */
@RequireAuthApp(...AppsExcludeUser)
@Controller('file')
export class FileController {
  constructor(
    private fileService: FileService,
    private awsService: AwsService
  ){}

  @ApiOperation({
    description: "Upload file and save user information as metadata"
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    description: "Get property 'location' to get url of file",
    content: {
      'application/json': {
        example: {
          ETag: "6aeb3f5534e4f54445a95ab346abb48f",
          Location: "https://inatti-lab-1.s3.amazonaws.com/8901859cec54d5edb94248b72c1f3174.jpg",
          key: "8901859cec54d5edb94248b72c1f3174.jpg",
          Key: "8901859cec54d5edb94248b72c1f3174.jpg",
          Bucket: "inatti-lab-1"
        }
      }
    }
  })
  @UseInterceptors(FileFieldsInterceptor([
    {name: 'file', maxCount: 1}
  ]))
  @Post('upload')
  async uplaodFile(
    @UserCred()
    user: User,
    @UploadedFiles()
    file: {file:Express.Multer.File[]},
  ){
    if(!file || !file.file || file.file.length < 1){
      throw new Error("File not uploaded")
    }
    return await this.fileService.upload(file.file[0], user)
  }

  @ApiOperation({
    description: "Get file S3 information (metadata) that uploaded by api `/file/upload`.",
    summary: "Get file S3 metadata"
  })
  @Get('s3')
  async getFile(
    @Query('url')
    url: string
  ){
    return this.awsService.getFile(url)
  }
}