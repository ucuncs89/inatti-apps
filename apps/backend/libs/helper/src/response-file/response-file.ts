import { Response } from "express";
import { Readable } from "stream";

export function getReadableStream(bufferFile: Buffer) {
  const stream = new Readable()
  stream.push(bufferFile);
  stream.push(null)
  return stream;
}

export async function ResponseFile(response: Response, params: {
  type?: string,
  nameFile?: string,
  buffer: Buffer,
}) {
  const getNameFile = () => {
    if (params.nameFile) return params.nameFile

    const listFormat = {
      'application/pdf': '.pdf',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': '.xlsx'
    }
    return `inatti_${new Date().getTime()}${listFormat[params.type] || ''}`
  }
  response.set({
    'Content-type': params.type,
    'Content-length': params.buffer.length,
    'Content-Disposition': 'attachment; filename=' + getNameFile()
  })

  const stream = getReadableStream(params.buffer)
  stream.pipe(response)
}