import { MakeId } from "../utils";

export function genTransactionId(){
  const length = 7
  return MakeId(length);
}

export function genInvoiceId(trxId: string, productId){
  let now = new Date();

  return `INATTI-TEST-${productId}-${trxId}-${now.getTime()}`;
}