import { applyDecorators } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";

export function ExampleResponseOk(example: any) {
  return applyDecorators(
    ApiResponse({
      status: 200,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            example: example
          }
        }
      }
    })
  )
}