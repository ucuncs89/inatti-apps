import { createHash } from "crypto"

export function CryptoMd5(payload: string){
  return createHash('md5').update(payload).digest('hex');
}

export function CryptoSha1(payload: string){
  return createHash('sha1').update(payload).digest('hex');
}