export function ShouldArray(data: any | any[]) {
  if (!Array.isArray(data)) {
    data = [data]
  }
  return data
}