import * as dayjs from "dayjs"
import * as timezone from "dayjs/plugin/timezone"
import * as utc from "dayjs/plugin/utc"

dayjs.extend(utc)
dayjs.extend(timezone)

export function DateAsiaFormat(date) {
  return dayjs(date).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
}

export function DateFormat(date) {
  return dayjs(date).format('YYYY-MM-DD HH:mm:ss')
}

export function DateOnlyFormat(date) {
  return dayjs(date).format('DD-MM-YYYY')
}