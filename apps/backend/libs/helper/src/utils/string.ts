
/**
 * change text template {variable} to data
 * @param text 
 * @param data 
 * @returns 
 */
export const TemplateText = (text: string, data: Record<string, any>) => {
  let result = text
  for (const key in data) {
    if (Object.prototype.hasOwnProperty.call(data, key)) {
      const element = data[key];
      result = result.replace(`{${key}}`, element);
    }
  }
  return result
}