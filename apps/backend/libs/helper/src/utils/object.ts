import { DateFormat } from "./time";

export type FormatDeepKey = Record<string, string | { key: string, value: (value: string) => any }>

export function getDeepValueObject(object: Record<string, any> | Record<string, any>[], format: FormatDeepKey) {
  // recursive
  if (Array.isArray(object)) {
    let result = []
    for (const value of object) {
      result.push(getDeepValueObject(value, format))
    }
    return result
  }

  let result = { ...format };
  for (const key of Object.keys(format)) {
    const element = format[key];
    if (!element) continue;

    if (typeof element === 'object') {
      let value = getDeepValue(object, element.key)
      result[key] = value ? element.value(value) : ''
    } else {
      result[key] = getDeepValue(object, element) ?? ''
    }

  }
  return result
}

/**
 * Created by : Rio Chandra
 * Function to get deep value object using string deepkey
 * 
 * ex:
 * 
 * let obj = {
 *  foo: {
 *    bar: {
 *      value: '1'
 *    }
 *  }
 * }
 * 
 * let val = getDeepValue(obj, 'foo.bar.value')
 * 
 * @param object Object with deep property
 * @param deepKey string deep key, eg: 'foo.bar.foobar.fobarbar'
 * @returns Object with deep key
 */
export function getDeepValue(object: Record<string, any>, deepKey: string) {
  if (!deepKey) return null
  let listProperty = deepKey.split('.');
  if (listProperty.length < 1) return null

  const exceptIntance = ['Date']

  const getInstance = (obj: Record<string, unknown>) => obj ? obj.constructor.name : null

  let lastParent = object
  let value = null
  for (const property of listProperty) {
    if (!lastParent) continue;

    if (typeof lastParent[property] === 'object') {
      if (!exceptIntance.includes(getInstance(lastParent[property]))) {
        lastParent = lastParent[property]
        continue;
      }

      // Value is a Date
      if (getInstance(lastParent[property]) === 'Date') {
        value = DateFormat(lastParent[property])
        continue;
      }
    }

    // normal value (string, number)
    value = lastParent[property]
  }
  return value
}

export function cleanObj(obj: Record<string, any>) {
  for (const key of Object.keys(obj)) {
    const element = obj[key];
    if (element === null || element === undefined) delete obj[key]
  }
  return obj
}