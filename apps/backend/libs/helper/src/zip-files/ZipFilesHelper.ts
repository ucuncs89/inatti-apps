import archiver = require("archiver");
import { Response } from "express";

export interface ZipFilesHelperOptions {
  name?: string,
  logger?: any
}

/**
 * Use this to zip files, very handy
 * @param response 
 * @param options 
 * @returns 
 */
export function ZipFilesHelper(response: Response, options?: ZipFilesHelperOptions) {
  const opt = Object.assign({}, {
    name: `file-zip-${new Date().getTime()}`,
    logger: console,
  }, options)

  response.set({
    'Content-Type': 'application/zip',
    'Content-Disposition': `attachment; filename="${opt.name}.zip"`,
  });

  let archive = archiver('zip', {
    zlib: { level: 9 }
  })

  // Pipe archieve to response
  archive.pipe(response)

  archive.on('finish', () => {
    opt.logger.log('finish archive')
  })

  archive.on('error', (err) => {
    opt.logger.error(`failed to archive file`)
    opt.logger.error(err)
    throw err
  })

  return {
    archive,
    append(file: Buffer, name: string) {
      archive.append(file, { name })
    },
    finish() {
      archive.finalize()
    }
  }
}