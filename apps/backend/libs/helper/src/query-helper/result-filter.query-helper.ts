import { SelectQueryBuilder } from "typeorm";
import { CommonFilterDTO } from "../query-filter/type";

export interface ResultFilterDTO extends CommonFilterDTO {
  result: string
}

interface Options {
  filter: ResultFilterDTO,
  columnResult: string
}

export function QueryUseStatusFilter<Entity>(query: SelectQueryBuilder<Entity>, options: Options) {
  const { filter } = options

  if (!filter || !filter.result) {
    return false;
  }
  query.andWhere(`${options.columnResult}::text ilike '%${filter.result}%'`)
  return query;
}