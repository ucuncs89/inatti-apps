import { Brackets, SelectQueryBuilder } from 'typeorm';
import { PaginationOptionsInterface } from '../paginate';
import { CommonFilterDTO } from '../query-filter/type';

interface Options {
  filter?: CommonFilterDTO;
  paginate: PaginationOptionsInterface;
  columnDate?: string;
  columnSearch?: string | string[];
}

export function QueryUsePaginationAndFilter<Entity>(
  query: SelectQueryBuilder<Entity>,
  options: Options,
) {
  const { filter, paginate, columnDate, columnSearch } = options;
  if (filter) {
    if (filter.date) {
      query.andWhere(
        `${columnDate}::date > now() - interval '${filter.date.value}'`,
      );
    }
    if (filter.rangedate) {
      query.andWhere(
        `${columnDate}::date between '${filter.rangedate.start}' and '${filter.rangedate.end}'`,
      );
    }

    if (columnSearch && filter.search) {
      if (Array.isArray(columnSearch)) {
        query.andWhere(
          new Brackets((qs) => {
            qs.where(`${columnSearch[0]} ilike '%' || :search || '%'`, {
              search: filter.search,
            });
            columnSearch.shift();
            for (const col of columnSearch) {
              qs.orWhere(`${col} ilike '%' || :search || '%'`, {
                search: filter.search,
              });
            }
          }),
        );
      } else {
        // '%' || :search || '%' to escape string
        query.andWhere(`${columnSearch} ilike '%' || :search || '%'`, {
          search: filter.search,
        });
      }
    }
  }

  // nb: `Skip` `take` are not work. use `limit` `offset`
  // https://github.com/typeorm/typeorm/issues/4742#issuecomment-699789447
  query.limit(paginate.take).offset(paginate.skip);

  return query;
}
