/**
 * @deprecated Move to @inatti/shared/dtos
 */
export interface PaginationResultInterface<PaginationEntity> {
  results: PaginationEntity[];
  total: number;
  next?: string;
  previous?: string;
}
