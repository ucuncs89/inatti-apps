
import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { PaginationOptionsInterface } from '.';

/**
 * Validate pagination option and return default value.
 */
@Injectable()
export class PaginationPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): PaginationOptionsInterface {
    if (!metadata) {
      return value;
    }

    if (!value) {
      // default pagination
      return this.generateResult({
        limit: 10,
        page: 0,
      })
    }
    return this.generateResult(value);
  }

  generateResult(option: PaginationOptionsInterface): PaginationOptionsInterface {
    return {
      ...option,
      take: option.limit,
      skip: option.limit * option.page
    }
  }
}
