import { CommonFilterDTO } from "./type";

export function BindCommonFilterToTable(filter: CommonFilterDTO, obj: any) {
  const objAdd: any = {}
  if (filter.date) {
    // assume it's a 'today' param
    if (filter.date && filter.date.value) {
      objAdd.createdAt = filter.date
    }
  }

  if (filter.rangedate) {
    objAdd.createdAt = filter.rangedate
  }
  return {
    ...obj,
    ...objAdd
  }
}