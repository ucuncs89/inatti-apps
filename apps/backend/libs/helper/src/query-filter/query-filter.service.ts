/**
 * // default by date
 * ?filter[date]=today 
 * ?filter[date]=2021-09-10
 * ?filter[rangedate]=2021-09-10:2021-09-11
 * 
 * // search field by keyword using 'ilike'
 * ?filter[(field_name)]=keyword
 * ?filter[rujukan]=id
 * 
 * // all filter using 'or'
 */

import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";
import { Between, MoreThanOrEqual } from "typeorm";
import * as dayjs from 'dayjs'

@Injectable()
export class FilterQueryPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!value) {
      return null
    }
    // only process type query
    if (!metadata || metadata.type !== 'query') {
      return value;
    }
    if (metadata.data !== 'filter') {
      throw new BadRequestException('Invalid name query filter. Name query must be \'filter\'')
    }
    if (typeof value !== 'object') {
      throw new BadRequestException("Filter must have some key and value")
    }
    if (value.rangedate && typeof value.rangedate === 'string') {
      const rangedate = value.rangedate.split(":")
      if (rangedate.length !== 2) {
        throw new BadRequestException("Filter daterange not match with format (start:end) YYYY-MM-DD:YYYY-MM-DD")
      }

      
      // offset 1 day
      let endDate = dayjs(rangedate[1])
      endDate = endDate.add(1, 'day');
      // generated to typeorm query
      value.rangedate = Between(rangedate[0], endDate.format("YYYY-MM-DD"))
    }

    /**
     * Value.date will generated to typeorm query
     */
    if (value.date) {
      if (value.date === "today") {
        let now = new Date()
        now.setHours(0, 0, 0)
        value.date = MoreThanOrEqual(`${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`)
      }
    }

    return value
  }
}