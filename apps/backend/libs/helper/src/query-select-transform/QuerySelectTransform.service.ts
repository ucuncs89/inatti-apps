import { applyDecorators, ArgumentMetadata, Injectable, PipeTransform } from "@nestjs/common";
import { ApiQuery } from "@nestjs/swagger";

export function QuerySelectApi() {
  return applyDecorators(
    ApiQuery({
      name: 'select',
      required: false,
      description: 'Only show some data'
    })
  )
}