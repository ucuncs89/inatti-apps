import { Response } from "express";
import { ResponseFile } from '../response-file/response-file'
import { FormatDeepKey, getDeepValueObject } from "../utils/object";
import * as excel from 'exceljs'

export async function deepValueToXlsx(response: Response, targetObject:Record<string, any>[], format: FormatDeepKey){
  let formatted = getDeepValueObject(targetObject, format) as any[]
  await ToXlsx(response, formatted)
}

export async function ToXlsx(res: Response, value: Record<string, string>[]){
  if(value.length < 1){
    return;
  }

  const workbook = new excel.Workbook()
  const worksheet = workbook.addWorksheet('Sheet 1')

  worksheet.columns = Object.keys(value[0]).map((v) => {
    return {
      header: v,
      key: v,
      width: v.length + 5
    }
  })

  worksheet.addRows(value)

  worksheet.getRow(1).fill = {
    type: 'pattern',
    pattern:'solid',
    fgColor:{argb:'F08080'},
  }
  worksheet.getRow(1).alignment = {
    horizontal: 'center'
  }

  let buffer = await workbook.xlsx.writeBuffer() as Buffer

  return ResponseFile(res, {
    buffer,
    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  }) 
}