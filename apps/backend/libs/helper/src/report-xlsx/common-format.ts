// Common format csv

import { FormatDeepKey } from "../utils/object";


/**
 * Format order data, make sure relations fill this array
 * [
    'detailSwab',
    'detailSwab.patient',
    'detailSwab.refTarget',
    'detailSwab.labTarget',
    'orderTrx',
    'reference',
    'product',
    'methodCash',
    'product.product',
  ]
 */
export const CommonFormatOrderXlsx:FormatDeepKey = {
  'Id': 'id',
  'Invoice Id': 'orderTrx.invId',
  'Status Pembayaran': 'status',
  'Rujukan': 'reference.name',
  'Produk': 'product.name',
  'Tipe Produk': 'product.product.name',
  'Harga dibeli': 'price',
  'Metode Pembayaran': 'method',
  'Metode Cash': 'methodCash.name',
  'Tanggal dibuat': 'createdAt',
  'Nama Pasien': 'detailSwab.patient.name',
  'Nik Pasien': {
    key: 'detailSwab.patient.nik',
    value: (val) => `\`${val}`,
  },
  'Jenis kelamin': 'detailSwab.patient.gender',
  'Nomor Hp': {
    key:'detailSwab.patient.phonenumber',
    value: (val) => `\`${val}`,
  },
  'Alamat Pasien': 'detailSwab.patient.address',
  'Kode Booking': 'detailSwab.codeBooking',
  'Hasil Test': 'detailSwab.resultSwab',
  'Tanggal Keluar Hasil': 'detailSwab.resultSentAt',
  'Lab Penerima Sample': 'detailSwab.labTarget.name',
  'Tanggal Kirim ke LAB': 'detailSwab.sendToLabDate',
}

/**
 * Common format order trx data, make sure relations fill this array
 * [
    'orderSwab',
    'orderSwab.detailSwab',
    'orderSwab.detailSwab.patient',
    'reference',
    'product',
    'product.product',
    'methodCash',
  ]
 */
export const CommonFormatOrderTrxXlsx: FormatDeepKey = {
  'Id':'id',
  'Invoice Id':'invId',
  'Status Pembayaran':'status',
  'Metode Pembayaran':'orderSwab.method',
  'Metode Cash': 'methodCash.name',
  'Total harga':'totalPrice',
  'Fee Admin':'feeAdmin',
  'Transfer Channel':'paymentChannelName',
  'Tanggal Expired':'expiredAt',
  'Nama Rujukan': 'reference.name',
  'Nama Produk':'product.name',
  'Tipe Produk':'product.product.name',
  'Total pasien': 'totalOrder',
  'Tanggal dibuat':'createdAt',
}