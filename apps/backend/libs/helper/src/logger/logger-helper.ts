import { Logger } from "@nestjs/common"


export class LogTask {
  constructor(private _log: Logger) { }

  taskReady = (message: string, type = 'verbose') => {
    return this._log[type](`⌛ Ready : ${message}`)
  }

  taskSuccess = (message: string, type = 'verbose') => {
    return this._log[type](`✅ Succes : ${message}`)
  }

  taskFailed = (message: string, type = 'verbose') => {
    return this._log[type](`😰 Failed : ${message}`)
  }

  async runTask<T>(task: Promise<T>, message: string): Promise<T> {
    try {
      this.taskReady(message)
      const res = await task
      this.taskSuccess(message)
      return res
    } catch (err) {
      this.taskFailed(message)
      throw err
    }
  }
}