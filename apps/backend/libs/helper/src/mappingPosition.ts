export type cellType = {row: number | string, col: number | string}

export function A1toNumber(valueA1: string): cellType{
  let row = valueA1[0].charCodeAt(0) - 65;
  let col = +valueA1.slice(1) - 1
  return {
    row,
    col
  }
}

export function NumberToA1(cell: cellType){
  return String.fromCharCode(+cell.row + 65) + "" +cell.col + 1
}