import { ShouldArray } from "../utils/array"

export function PromiseTimeout<T = any>(task: Promise<T> | Promise<T>[], maxTimeout = 10000) {
  const timeoutMachine = new Promise((resolve, reject) => {
    setTimeout(() => {
      setTimeout(() => {
        reject(new Error('Timeout'))
      }, maxTimeout)
    })
  })

  return Promise.race<T>([
    ...ShouldArray(task),
    timeoutMachine
  ])
}