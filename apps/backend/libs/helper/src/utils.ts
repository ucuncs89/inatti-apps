import { ResultSwabEnum } from "@app/database/entity/swab-data/type";

export function ConvertToResultEnum(result:string): ResultSwabEnum | null{
  const regexNegatif = /negati(f|ve)/gi
  const regexPositif = /positi(f|ve)/gi
  if(regexNegatif.test(result)) return ResultSwabEnum.negatif
  if(regexPositif.test(result)) return ResultSwabEnum.positif
  return null
}

// author : http://stackoverflow.com/questions/1349404/ddg#1349426
export function MakeId(length) {
  let result           = '';
  let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let charactersLength = characters.length;
  for ( let i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


// This code copied from here
// https://stackoverflow.com/a/11172685/11218747
// explenation: https://en.wikipedia.org/wiki/Haversine_formula
export function MeasureGeoToMeter(pos1:number[], pos2:number[]){  // generally used geo measurement function
  const [lat1, lon1] = pos1
  const [lat2, lon2] = pos2

  let R = 6378.137; // Radius of earth in KM
  let dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
  let dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
  let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
  Math.sin(dLon/2) * Math.sin(dLon/2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  let d = R * c;
  return d * 1000; // meters
}