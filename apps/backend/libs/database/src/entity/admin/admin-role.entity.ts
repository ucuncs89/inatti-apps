import { Entity } from "typeorm";
import { RoleModel } from "../role/role.model";

@Entity('admin_role')
export class AdminRole extends RoleModel { }