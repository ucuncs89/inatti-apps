import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { AdminRole } from "./admin-role.entity";

@Entity('admin_role_user')
export class AdminRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => AdminRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: AdminRole;
}