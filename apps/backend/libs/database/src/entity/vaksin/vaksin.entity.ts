import { RelationsEntity } from "@app/database/utils/relations-entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";
import { SwabOrderTrx } from "../swab-data/order-trx.entity";
import { SwabOrder } from "../swab-data/order.entity";
import { PatientRelationsAll, SwabPatient } from "../swab-data/patient.entity";
import { VaksinNotificationWhatsapp } from "./vaksin-notification.entity";

@Entity()
export class Vaksin {
  constructor(id?: string) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => SwabPatient, { nullable: true })
  @JoinColumn()
  patient: SwabPatient;

  @Column({
    nullable: true,
    type: 'timestamptz',
  })
  requestDate?: Date;

  @Column({
    nullable: true,
    type: 'timestamptz',
  })
  checkinDateAt?: Date;

  @Column({
    nullable: true,
    type: 'timestamptz'
  })
  vaksinDateAt?: Date

  @ManyToOne(() => RefDetail, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  ref: RefDetail

  @OneToOne(() => SwabOrder, { nullable: true, onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  order?: SwabOrder;

  @ManyToOne(() => SwabOrderTrx, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  orderTrx?: SwabOrderTrx

  @OneToOne(() => VaksinNotificationWhatsapp, map => map.vaksin)
  notify?: VaksinNotificationWhatsapp

  /** ALL DATE MUST USE TYPE `timestamptz`  */
  @CreateDateColumn({ type: "timestamptz" })
  createdAt?: Date;
}

export const VaksinRelationsAll = RelationsEntity([
  'patient',
  'ref',
  'order',
  'order.product',
  'order.product.product',
  'orderTrx',
  'notify',
  ...PatientRelationsAll('patient')
])