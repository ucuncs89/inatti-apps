import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Vaksin } from './vaksin.entity';

export enum StatusNotifWA {
  QUEUE = 'queue',
  PROCESS = 'process',
  SENT = 'sent',
  FAILED = 'failed',
}

@Entity()
export class VaksinNotificationWhatsapp {
  @PrimaryGeneratedColumn()
  id?: number;

  @OneToOne(type => Vaksin, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  vaksin: Vaksin

  @Column({
    enum: StatusNotifWA,
    default: StatusNotifWA.QUEUE,
  })
  status?: StatusNotifWA;

  @Column({ nullable: true })
  messageId?: string;

  @Column({ nullable: true })
  message?: string;

  @CreateDateColumn()
  createdAt?: Date;
}