import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Medicine } from '../medicine/medicine.entity';
import { MedicinePmrNotes } from '../swab-data/type';
import { PmrPatient } from './pmr-patient.entity';

@Entity()
export class PmrMedicine {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => PmrPatient, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  pmr: PmrPatient

  @ManyToOne(() => Medicine, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  medicine: Medicine

  @Column()
  amount: string

  @Column()
  amountUnit: string

  @Column({ type: "jsonb", nullable: true })
  notes?: any | MedicinePmrNotes;
}
