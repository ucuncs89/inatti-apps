import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, JoinColumn, OneToMany, DeleteDateColumn } from 'typeorm';
import { ApotekDetail } from '../apotek/apotek-detail.entity';
import { Medicine } from '../medicine/medicine.entity';
import { SwabPatient } from '../swab-data/patient.entity';
import { User } from '../user/user.entity';
import { PmrMedicine } from './pmr-medicine.entity';

@Entity('pmr_patient')
export class PmrPatient {
  constructor(
    id?: string
  ) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => SwabPatient)
  @JoinColumn()
  patient: SwabPatient

  @Column()
  docterName: string

  @Column()
  case: string

  @Column({ type: 'text' })
  note: string

  @ManyToOne(() => User)
  @JoinColumn()
  insertBy: User

  @ManyToOne(() => ApotekDetail)
  @JoinColumn()
  apotek: ApotekDetail

  @Column({ nullable: true })
  attachment?: string;

  @Column({ nullable: true, type: 'jsonb' })
  proofConsultation?: string[];

  @OneToMany(() => PmrMedicine, PmrMedicine => PmrMedicine.pmr, { cascade: true })
  medicines: PmrMedicine[]

  @CreateDateColumn({ type: "timestamptz" })
  createdAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
