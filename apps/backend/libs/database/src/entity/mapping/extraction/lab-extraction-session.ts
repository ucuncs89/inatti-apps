import { Column, Entity, OneToMany } from "typeorm";
import { MappingSession } from "../templateMapping";
import { LabExtractionSessionDetail } from "./lab-extraction-session-detail";

@Entity()
export class LabExtractionSessions extends MappingSession {
  constructor(id?: number) {
    super();
    this.id = id
  }
  @OneToMany(() => LabExtractionSessionDetail, mapping => mapping.session)
  mapping: LabExtractionSessionDetail[];

  @Column({type: Boolean, nullable:true })
  isExtracted: boolean;

  @Column({ nullable:true })
  dateExtracted: Date;
}