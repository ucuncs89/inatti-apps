import { Entity, JoinColumn, ManyToOne } from "typeorm";
import { MappingSessionDetail } from "../templateMapping";
import { LabExtractionSessions } from "./lab-extraction-session";

@Entity()
export class LabExtractionSessionDetail extends MappingSessionDetail<LabExtractionSessions>{
  @ManyToOne(() => LabExtractionSessions, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  session: LabExtractionSessions;
}