import { Column, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { LabDetail } from "../lab/lab-detail.entity";
import { SwabDetail } from "../swab-data/swab-detail.entity";

export enum ControlTypeMapping {
  positif = "positif",
  negatif = "negatif",
}

export class MappingSession {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  rowTotal: number;

  @Column()
  columnTotal: number;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail

  @Column({ type: 'timestamptz', default: () => "CURRENT_TIMESTAMP" })
  createdAt: Date;
}

export class MappingSessionDetail<T extends MappingSession>{
  @PrimaryGeneratedColumn()
  id: number;

  // need to define on child class
  session: T;

  @ManyToOne(() => SwabDetail, { nullable: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  sample?: SwabDetail;

  @Column()
  locateRow: string;

  @Column()
  locateColumn: string;

  @Column({ nullable: true, default: false })
  isControl?: boolean;

  @Column({ type: 'enum', enum: ControlTypeMapping, nullable: true })
  controlType?: ControlTypeMapping

  @Column({ nullable: true })
  controlName?: string;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail
}

