import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * Static table.
 * only set on database
 */
@Entity()
export class AnalyzeTool {
  constructor(id?: number){
    this.id = id
  }
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
}