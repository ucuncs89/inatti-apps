import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne } from "typeorm";
import { LabAnalyzeDoctor } from "../../lab/lab-analyze-doctor.entity";
import { MappingSessionDetail } from "../templateMapping";
import { LabAnalyzeResult } from "./lab-analyze-result.entity";
import { LabAnalyzeSessions } from "./lab-analyze-session.entity";

@Entity()
export class LabAnalyzeSessionDetail extends MappingSessionDetail<LabAnalyzeSessions>{
  constructor(id?: number){
    super();
    this.id = id
  }

  @ManyToOne(() => LabAnalyzeSessions, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  session: LabAnalyzeSessions

  @OneToOne(() => LabAnalyzeResult, resultTool => resultTool.sampleSession, { nullable: true })
  resultTool: LabAnalyzeResult

  @OneToOne(() => LabAnalyzeDoctor, resultDoctor => resultDoctor.sampleSession, { nullable: true })
  resultDoctor: LabAnalyzeDoctor
}