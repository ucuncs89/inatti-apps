export type DyeType = "FAM" | "Cy5"
export type genName = "EAV" | "E-Gene" | "RdRP-Gene" | any

export class Flourescene{
  time: string
  value: string;
}

export class Amplification{
  cycles: string
  value: string
}

export class LC96Dye{
  dyeType: DyeType
  genName: genName
  fluorescene: Flourescene[]
  amplification: Amplification[]
}

export class LC96ResultSample{
  position: string; // eg: A1, B5
  sampleId: string;
  dyes: LC96Dye[]
}