import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { MappingSession } from "../templateMapping";
import { AnalyzeTool } from "./analyze-tool.entity";
import { LabAnalyzeSessionDetail } from "./lab-analyze-session-detail.entity";
import { ManagedUpload } from "aws-sdk/clients/s3";

@Entity()
export class LabAnalyzeSessions extends MappingSession {
  constructor(id?: number) {
    super();
    this.id = id
  }

  @OneToMany(() => LabAnalyzeSessionDetail, mapping => mapping.session, { nullable: true })
  mapping: LabAnalyzeSessionDetail[];

  @ManyToOne(() => AnalyzeTool)
  @JoinColumn()
  tool: AnalyzeTool

  totalMapping: number;

  // result bisa dari beberapa alat
  @Column({
    type: 'jsonb',
    nullable: true
  })
  result?: any // raw data

  @Column({
    type: 'jsonb',
    nullable: true
  })
  resultSummary?: any // raw data but summary

  @Column({
    type: 'timestamptz',
    nullable: true
  })
  resultUploadDate?: Date;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  attachedFile: any | AttachedFileType;
}

export type AttachedFileType = {
  originalName: string,
  hashedName: string,
  format: string,
  s3: ManagedUpload.SendData
}