import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { AnalyzeTool } from "./analyze-tool.entity";
import { LabAnalyzeSessionDetail } from "./lab-analyze-session-detail.entity";
import { LabAnalyzeSessions } from "./lab-analyze-session.entity";

@Entity()
export class LabAnalyzeResult {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabAnalyzeSessionDetail, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  sampleSession: LabAnalyzeSessionDetail;

  @ManyToOne(() => AnalyzeTool)
  @JoinColumn()
  tool: AnalyzeTool

  @ManyToOne(() => LabAnalyzeSessions, {nullable: true})
  @JoinColumn()
  session: LabAnalyzeSessions;

  @Column({ nullable: true })
  locateRow: string;

  @Column({ nullable: true })
  locateColumn: string;

  // result bisa dari beberapa alat
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  resultRaw: any

  /**
   * Place to put result by analysis
   * as soon as uploaded result from tool, this will filled
   * 
   * BUT, user can change this.
   * on client, there is a column 'suggested result', the result come out from client
   */
  @Column({ nullable: true })
  resultSummary: string;

  @Column({ nullable: true, type: 'jsonb' })
  CtValue?: any;

  @CreateDateColumn()
  createdAt: string;
}