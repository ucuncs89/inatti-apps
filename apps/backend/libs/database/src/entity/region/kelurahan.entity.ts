import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { Kecamatan } from "./kecamatan.entity";

@Entity({ name: "kelurahan" })
export class Kelurahan {
  constructor(id: string | number) {
    this.id = id + ""
  }

  // id cuma bisa string karena data yang di import dari kemenkes
  // punya id yang nilainya miliaran, artinya gak cocok sbg `number`
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @ManyToOne(() => Kecamatan)
  @JoinColumn({ name: 'kecamatan_id' })
  kecamatan: Kecamatan;
}