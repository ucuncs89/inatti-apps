import { JoinColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";
import { Kecamatan } from "./kecamatan.entity";
import { Provinsi } from "./provinsi.entity";
import { Region } from "./region.model";

@Entity({ name: "kabupaten" })
export class Kabupaten extends Region {
  @ManyToOne(() => Provinsi)
  @JoinColumn({ name: 'provinsi_id' })
  provinsi: Provinsi;

  @OneToMany(() => Kecamatan, Kecamatan => Kecamatan.kabupaten)
  kecamatan: Kecamatan[];

  @OneToMany(() => RefDetail, RefDetail => RefDetail.city)
  ref: RefDetail[];
}