import { Entity, OneToMany } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";
import { Kabupaten } from "./kabupaten.entity";
import { Region } from "./region.model";

@Entity({ name: "provinsi" })
export class Provinsi extends Region {
  @OneToMany(() => Kabupaten, Kabupaten => Kabupaten.provinsi)
  kabupaten: Kabupaten[];

  @OneToMany(() => RefDetail, RefDetail => RefDetail.province)
  ref: RefDetail[];
}