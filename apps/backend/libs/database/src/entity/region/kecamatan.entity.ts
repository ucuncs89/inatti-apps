import { Entity, JoinColumn, ManyToOne } from "typeorm";
import { Kabupaten } from "./kabupaten.entity";
import { Region } from "./region.model";

@Entity({ name: "kecamatan" })
export class Kecamatan extends Region {
  @ManyToOne(() => Kabupaten)
  @JoinColumn({ name: 'kabupaten_id' })
  kabupaten: Kabupaten;
}