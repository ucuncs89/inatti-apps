import { Column, PrimaryGeneratedColumn } from "typeorm";

export class Region {
  constructor(id: number) {
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}