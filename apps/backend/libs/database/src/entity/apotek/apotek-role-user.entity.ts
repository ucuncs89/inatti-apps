import { CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { ApotekDetail } from "./apotek-detail.entity"
import { ApotekRole } from "./apotek-role.entity"

@Entity()
export class ApotekRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => ApotekRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: ApotekRole;

  @ManyToOne(() => ApotekDetail, {eager: true})
  @JoinColumn()
  apotekDetail: ApotekDetail

  @CreateDateColumn({nullable: true})
  createdAt?: Date
}