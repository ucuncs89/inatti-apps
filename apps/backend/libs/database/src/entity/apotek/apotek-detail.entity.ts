import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ApotekGroupDetail } from "../apotek-group/apotek-group-detail.entity";
import { Kabupaten } from "../region/kabupaten.entity";
import { Provinsi } from "../region/provinsi.entity";
import { ApotekerDetail } from "./apoteker-detail.entity";
import { ApotekDetailStatus } from './type';

@Entity()
export class ApotekDetail {
  constructor(id?: string) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  address?: string;

  @Column({ nullable: true })
  description?: string;

  @Column({ nullable: true })
  phonenumber?: string;

  @Column({ nullable: true })
  thumbnail?: string;

  @ManyToOne(() => ApotekGroupDetail)
  @JoinColumn()
  apotekGroup: ApotekGroupDetail

  @ManyToOne(() => Provinsi, { nullable: true })
  @JoinColumn()
  provinsi?: Provinsi

  @ManyToOne(() => Kabupaten, { nullable: true })
  @JoinColumn()
  kabupaten?: Kabupaten

  @Column({ nullable: true })
  coord?: String;

  @Column({ nullable: true })
  logo?: String;

  @Column({ nullable: true })
  nomorSIA?: string;

  @Column({ nullable: true })
  typeApotek?: string;

  @Column({ nullable: true })
  suratIzinNo?: string;

  @Column({ nullable: true })
  suratIzinFile?: string;

  @Column({ nullable: true, enum: ApotekDetailStatus, type: 'enum' })
  validateStatus?: ApotekDetailStatus;

  @CreateDateColumn({ nullable: true, type: "timestamptz" })
  createdAt?: Date;

  @Column({ nullable: true })
  notActiveAt?: Date;

  @OneToMany(() => ApotekerDetail, apoteker => apoteker.apotek)
  apoteker: ApotekerDetail[]

  @DeleteDateColumn({ nullable: true, type: "timestamptz" })
  deletedAt?: Date;
}