import { Column, Entity } from "typeorm";
import { RoleModel } from "../role/role.model";

@Entity()
export class ApotekRole extends RoleModel {
  @Column({nullable: true})
  isCreateApoteker?: boolean
}