import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { GenderEnum } from '../swab-data/type';
import { User } from '../user/user.entity';
import { ApotekDetail } from './apotek-detail.entity';

@Entity()
export class ApotekerDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true
  })
  name: string

  @Column({
    type: "enum",
    enum: GenderEnum,
    nullable: true
  })
  gender: GenderEnum;

  @Column({
    nullable: true
  })
  birthdate: Date;

  @Column({
    nullable: true
  })
  photo: String;

  @Column({
    nullable: true
  })
  nomorSIPA: String;

  @Column({
    nullable: true
  })
  nomorSTRA: string;

  @Column({
    nullable: true
  })
  phonenumber: string;

  @Column({
    nullable: true
  })
  isActive: boolean;

  @ManyToOne(() => ApotekDetail)
  @JoinColumn()
  apotek: ApotekDetail;

  @ManyToOne(() => User)
  @JoinColumn()
  user: User;
}
