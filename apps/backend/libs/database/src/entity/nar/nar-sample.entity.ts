import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { SwabDetail } from '../swab-data/swab-detail.entity';

@Entity()
export class NarSample {
  @PrimaryGeneratedColumn()
  id: number;
  
  @OneToOne(type => SwabDetail, {onDelete: 'CASCADE', onUpdate: 'CASCADE'})
  @JoinColumn()
  swabDetail: SwabDetail

  @Column()
  isSuccess: boolean;
  
  @Column({nullable: true})
  message: string;

  @CreateDateColumn()
  createdAt: Date;
}
