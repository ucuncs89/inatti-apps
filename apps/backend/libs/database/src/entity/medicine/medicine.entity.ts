import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToMany, ManyToOne, JoinColumn, DeleteDateColumn } from 'typeorm';
import { ApotekDetail } from '../apotek/apotek-detail.entity';
import { User } from '../user/user.entity';

@Entity()
export class Medicine {
  constructor(id?: number) {
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true, unique: true })
  code?: string

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn()
  insertBy?: User

  @ManyToOne(() => ApotekDetail, { nullable: true })
  @JoinColumn()
  insertByApotek?: ApotekDetail

  /** ALL DATE MUST USE TYPE `timestamptz`  */
  @CreateDateColumn({ type: "timestamptz" })
  createdAt: Date

  @DeleteDateColumn({ type: 'timestamptz' })
  deletedAt?: Date
}
