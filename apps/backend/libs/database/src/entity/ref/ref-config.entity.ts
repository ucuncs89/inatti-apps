import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { RefDetail } from "./ref-detail.entity";

@Entity()
export class RefConfig {
  constructor(id?:number){
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail

  @Column({default: false})
  isDisableNotifyWhatsapp?:boolean

  // For now, I assume Inatti only work in Indonesia
  @Column({nullable: true})
  timezone: string
}