import { Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Kabupaten } from "../region/kabupaten.entity";
import { Provinsi } from "../region/provinsi.entity";
import { RefConfig } from "./ref-config.entity";
import { RefProduct } from "./ref-product.entity";
import { RefShift } from "./ref-shift.entity";

@Entity({ name: "ref_detail" })
export class RefDetail {
  constructor(id?: string) {
    this.id = id;
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ name: 'tenant_id', nullable: true })
  tenantId: string;

  @Column({ name: 'contact_whatsapp', nullable: true })
  contactWhatsapp: string;

  @Column({ name: 'ref_address', nullable: true })
  refAddress: string;

  @Column({ name: 'ref_address_point', nullable: true })
  refAddressPoint: string;

  @Column({ name: 'thumbnail', comment: "Image thumbnail", nullable: true })
  image: string

  @Column({ nullable: true })
  narFaskesId: string;

  @ManyToOne(() => Provinsi, provinsi => provinsi.id)
  @JoinColumn()
  province: Provinsi

  @ManyToOne(() => Kabupaten, kabupaten => kabupaten.id)
  @JoinColumn()
  city: Kabupaten

  @Column({ type: 'timestamptz', name: 'created_at', default: () => "CURRENT_TIMESTAMP" })
  createdAt: Date;

  @OneToMany(() => RefProduct, RefProduct => RefProduct.ref)
  @JoinColumn()
  products: RefProduct[];

  @OneToMany(() => RefShift, shift => shift.reference)
  @JoinColumn()
  shifts: RefShift[];

  @Column({nullable: true})
  isDelete?: boolean

  @DeleteDateColumn()
  deletedAt?: Date

  @OneToOne(() => RefConfig, config => config.ref, {nullable: true})
  config: RefConfig
}