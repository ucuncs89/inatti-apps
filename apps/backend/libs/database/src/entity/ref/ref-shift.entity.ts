import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { RefDetail } from "./ref-detail.entity";
import { DaysWeekEnum } from "./type";

@Entity({ name: 'ref_shift' })
export class RefShift {
  constructor(id?: number) {
    this.id = id;
  }

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RefDetail)
  @JoinColumn({ name: 'reference_id' })
  reference: RefDetail;

  @Column()
  name: string;

  @Column({ name: 'day_id', enum: DaysWeekEnum, type: "enum" })
  day: DaysWeekEnum;

  @Column({ name: 'time_start', type: "time" })
  timeStart: string;

  @Column({ name: 'time_end', type: "time" })
  timeEnd: string;
}