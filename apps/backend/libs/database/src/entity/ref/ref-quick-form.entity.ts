import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn } from "typeorm";
import { RefDetail } from "./ref-detail.entity";

/**
 * As entity and as DTO
 */
@Entity()
export class RefQuickForm {
  constructor(id?:string){
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({type: String, description: 'id ref'})
  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail

  @ApiProperty()
  @Column()
  name: string

  @ApiProperty({ nullable: true, required: false })
  @Column({ nullable: true })
  expiredAt?: Date

  @CreateDateColumn()
  createdAt: Date
}