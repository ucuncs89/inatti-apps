import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, CreateDateColumn } from 'typeorm';
import { RefDetail } from './ref-detail.entity';

@Entity()
export class RefOld {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  oldId: number;

  @Column({nullable: true})
  oldName: string;

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  newRef: RefDetail

  @CreateDateColumn()
  createdAt: Date;
}
