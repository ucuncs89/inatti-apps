export const ProductTypeId = {
  PCR: 1,
  Antigen: 2,
  Vaksin: 3,
}

export const isProductTestCovid = (productId) => {
  return [ProductTypeId.PCR, ProductTypeId.Antigen].includes(productId)
}

export const isProductVaksin = (productId) => {
  return productId === ProductTypeId.Vaksin
}