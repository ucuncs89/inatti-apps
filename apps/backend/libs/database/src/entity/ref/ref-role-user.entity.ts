import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { RefDetail } from "./ref-detail.entity";
import { RefRole } from "./ref-role.entity";

@Entity('ref_role_user')
export class RefRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RefDetail, { eager: true, onDelete: 'CASCADE', onUpdate: "CASCADE" })
  @JoinColumn({ name: "ref_id" })
  refId: RefDetail;

  @ManyToOne(() => User, { onDelete: 'CASCADE', onUpdate: "CASCADE" })
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => RefRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: RefRole;
}