import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Product } from './product.entity';
import { RefDetail } from './ref-detail.entity';

@Entity({ name: 'ref_product' })
export class RefProduct {
  constructor(id?: number) {
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  description?: string;

  @ManyToOne(() => RefDetail)
  @JoinColumn({ name: 'ref_id' })
  ref: RefDetail;

  @ManyToOne(() => Product, { eager: true })
  @JoinColumn({ name: 'product_id' })
  product: Product;

  @Column({ nullable: true })
  price?: number;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  methodPayments: any

  // Deprecated
  @Column({ nullable: true })
  isDelete?: boolean

  @DeleteDateColumn()
  deleteAt?: Date;
}
