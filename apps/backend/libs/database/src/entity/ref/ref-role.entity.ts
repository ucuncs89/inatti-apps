import { Entity } from "typeorm";
import { RoleModel } from "../role/role.model";

@Entity('ref_role')
export class RefRole extends RoleModel { }