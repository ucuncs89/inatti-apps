import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ApotekHeadOffice } from "../apotek-head-office/apotek-head-office.entity";
import { ApotekDetail } from "../apotek/apotek-detail.entity";

@Entity()
export class ApotekGroupDetail {
  constructor(id?: string) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  address?: string;

  @Column({ nullable: true })
  description?: string;

  @Column({ nullable: true })
  phonenumber?: string;

  @Column({ nullable: true })
  thumbnail?: string;

  @OneToMany(() => ApotekDetail, apotekDetail => apotekDetail.apotekGroup)
  apotekList: ApotekDetail[];

  @ManyToOne(() => ApotekHeadOffice, { nullable: true, eager: true })
  @JoinColumn()
  headOffice?: ApotekHeadOffice;

  @CreateDateColumn({ nullable: true, type: 'timestamptz' })
  createdAt?: Date;

  @DeleteDateColumn()
  deleteAt?: Date;
}