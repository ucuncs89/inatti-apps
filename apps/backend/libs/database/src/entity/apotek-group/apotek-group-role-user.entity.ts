import { CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { ApotekGroupDetail } from "./apotek-group-detail.entity";
import { ApotekGroupRole } from "./apotek-group-role.entity";7

@Entity()
export class ApotekGroupRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => ApotekGroupRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: ApotekGroupRole;

  @ManyToOne(() => ApotekGroupDetail, {eager: true})
  @JoinColumn()
  apotekGroup: ApotekGroupDetail

  @CreateDateColumn({nullable:true})
  createdAt?: Date
}