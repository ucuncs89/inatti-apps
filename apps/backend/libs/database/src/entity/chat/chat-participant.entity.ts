import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { User } from '../user/user.entity';
import { ChatRoomEntity } from './chat-room.entity';

@Entity()
export class ChatParticipantEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn()
  user: User

  @ManyToOne(() => ChatRoomEntity)
  @JoinColumn()
  room: ChatRoomEntity

  @Column({nullable:true})
  unread: number;
}
