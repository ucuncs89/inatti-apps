import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { ApotekDetail } from '../apotek/apotek-detail.entity';
import { User } from '../user/user.entity';

@Entity()
export class ChatRoomEntity {
  constructor(id?: string){this.id = id}
  
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => ApotekDetail)
  @JoinColumn()
  apotek: ApotekDetail

  @ManyToOne(() => User)
  @JoinColumn()
  apoteker: User

  @Column({nullable: true})
  closeAt?: Date

  @CreateDateColumn()
  createdAt:Date;
}
