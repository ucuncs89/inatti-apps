import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { User } from '../user/user.entity';
import { ChatRoomEntity } from './chat-room.entity';

@Entity()
export class ChatMessageEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @ManyToOne(() => User)
  @JoinColumn()
  user: User;

  @Column({
    type: 'text'
  })
  message: string;

  @ManyToOne(() => ChatRoomEntity)
  @JoinColumn()
  room: ChatRoomEntity;

  @CreateDateColumn()
  createdAt?: Date;
}
