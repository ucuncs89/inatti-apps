import { CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { ApotekHeadofficeAssigned } from "./apotek-head-office-assigned.entity";
import { ApotekHeadofficeRole } from "./apotek-head-office-role.entity";
import { ApotekHeadOffice } from "./apotek-head-office.entity";

@Entity()
export class ApotekHeadofficeRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User)
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => ApotekHeadofficeRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: ApotekHeadofficeRole;

  @ManyToOne(() => ApotekHeadOffice, { eager: true })
  @JoinColumn()
  apotekHeadoffice: ApotekHeadOffice

  @OneToMany(() => ApotekHeadofficeAssigned, assigned => assigned.role)
  apotekGroup?: ApotekHeadofficeAssigned[]

  @CreateDateColumn({ nullable: true })
  createdAt?: Date
}

export const ROLEID_HEADOFFICE = {
  HEADOFFICE: 1,
  BM: 2,
}