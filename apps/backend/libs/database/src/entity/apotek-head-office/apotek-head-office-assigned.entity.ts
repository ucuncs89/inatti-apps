import { CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApotekGroupDetail } from "../apotek-group/apotek-group-detail.entity";
import { ApotekHeadofficeRoleUser } from "./apotek-head-office-role-user.entity";

@Entity()
export class ApotekHeadofficeAssigned {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ApotekHeadofficeRoleUser)
  @JoinColumn()
  role: ApotekHeadofficeRoleUser

  @ManyToOne(() => ApotekGroupDetail, { eager: true })
  @JoinColumn()
  apotekGroup: ApotekGroupDetail

  @CreateDateColumn({ nullable: true })
  createdAt?: Date
}