import { Entity } from "typeorm";
import { RoleModel } from "../role/role.model";

@Entity()
export class ApotekHeadofficeRole extends RoleModel {
  get isHeadOffice() {
    return this.permission.includes('HeadOffice')
  }
}