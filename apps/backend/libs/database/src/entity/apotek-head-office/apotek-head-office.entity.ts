import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * As request client to create upper level for apotek. So this is it.
 * 
 * Head Office -> Apotek Group -> Apotek
 * 
 * Hell yeah.
 */
@Entity()
export class ApotekHeadOffice {
  constructor(id?: string) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt: Date;

  @DeleteDateColumn({ type: 'timestamptz' })
  deleteAt?: Date;
}