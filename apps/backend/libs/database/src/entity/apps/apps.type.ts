import { values } from "lodash";

export enum AppsEnum {
  User = "USER",
  Ref = "REF",
  Lab = "LAB",
  Admin = "Admin",
  Apotek = "Apotek",
  ApotekGroup = "ApotekGroup",
  ApotekHeadOffice = "ApotekHeadoffice",
  TemplatePdfWeb = "TemplatePdfWeb"
}

const ExcludeFn = (app: AppsEnum) => values(AppsEnum).filter(v => v !== app);

export const AppsExcludeUser = ExcludeFn(AppsEnum.User);

/**
 * Use this if templatepdfweb included
 */
export const AppsAll = values(AppsEnum);