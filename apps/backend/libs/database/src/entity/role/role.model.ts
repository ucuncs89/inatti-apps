import { Column, PrimaryGeneratedColumn } from "typeorm";

export class RoleModel {
  constructor(id?: number){
    this.id = id
  }

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  description?: string;

  @Column("text", { array: true })
  permission: string[];
}