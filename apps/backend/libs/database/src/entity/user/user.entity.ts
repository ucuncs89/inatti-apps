import { AfterLoad, BeforeInsert, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";
import * as bcrypt from 'bcrypt';

@Entity('users')
export class User {
  constructor(id?: string) {
    this.id = id
  }
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  fullname: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({ name: 'is_active', default: true })
  isActive: boolean;

  @Column({ default: false, nullable: true })
  isVerified: boolean;

  @Column({ nullable: true })
  email?: string;

  @Column({ nullable: true })
  phoneNumber?: string;

  @Column({ default: false, nullable: true })
  isAnonymous: boolean;

  @CreateDateColumn({
    type: 'timestamptz',
    nullable: true
  })
  createdAt?: Date;

  @BeforeInsert()
  async hashPassword() {
    if (!this.password) return;
    let salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    return this.password
  }

  hideCredential() {
    delete this.password
  }
}