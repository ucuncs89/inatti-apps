import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Otp {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User)
  @JoinColumn()
  owner: User;

  @Column()
  otp: string;

  @Column({default: false})
  isUsed: boolean;

  @Column()
  validUntil: Date;

  @CreateDateColumn()
  createdAt: Date;
}
