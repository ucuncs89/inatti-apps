import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsNotEmpty } from "class-validator"

export class UserManageDTO {
  @IsNotEmpty()
  @ApiProperty()
  fullname?: string

  @IsNotEmpty()
  @ApiProperty()
  username: string

  @IsEmail()
  @ApiProperty()
  email?: string

  @ApiProperty()
  password?: string

  @IsNotEmpty()
  @ApiProperty()
  roleId: number
}

export class UserManageUpdateDTO extends UserManageDTO {
  @ApiProperty()
  userId: string;
}