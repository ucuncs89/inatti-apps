import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) { }

  async usernameExistThrowError(username: string) {
    let user = await this.usersRepository.findOne({
      where: {
        username,
      }
    })
    if (user) {
      throw new BadRequestException(new UsernameExistException())
    }
  }

  validateUsername(username: string) {
    if (!username) return;
    let isNotValid = username.includes(' ')
    if (isNotValid) {
      throw new BadRequestException("Username not valid, can't contain space")
    }
    return true;
  }

  validateEmail(email: string) {
    if (!email) return;
    let isValid = email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
    if (!isValid) throw new BadRequestException("Email is not valid")
    return;
  }

  validatePassword(password: string) {
    if (!password) return;
    if (password.length < 8) throw new BadRequestException("Password minimal 8 character")
    return;
  }

  validateUser(user: User) {
    this.validateUsername(user.username)
    this.validatePassword(user.password)
    this.validateEmail(user.email)
    return;
  }

  async create(userData: User) {
    await this.usernameExistThrowError(userData.username)
    this.validateUser(userData)

    let newUser: User = this.usersRepository.create(userData);
    return this.usersRepository.save(newUser);
  }

  async findOrCreate(userData: User) {
    let userExist = await this.usersRepository.findOne({
      where: {
        username: userData.username
      }
    })
    if (userExist) return userExist

    return await this.create(userData)
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(search: Record<string, unknown>): Promise<User> {
    return this.usersRepository.findOne(search);
  }

  async remove(id: Record<string, unknown>): Promise<DeleteResult> {
    return await this.usersRepository.delete(id);
  }

  async update(idUser: string, newData: User): Promise<User> {
    let user: User = await this.usersRepository.findOne(idUser);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    if (newData.username && user.username !== newData.username) {
      await this.usernameExistThrowError(newData.username)
    }
    this.validateUser(newData)

    if (newData.password && user.password !== newData.password) {
      await newData.hashPassword()
    }
    newData.id = idUser
    Object.assign(user, newData);
    return await this.usersRepository.save(user);
  }
}

export class UsernameExistException extends Error {
  constructor(message?: string) {
    super(message ?? "Username exist")
    if (message) this.message = message
  }
  name = "UsernameExistException"
  message = "Username exist, please choose another username"
}
