import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { SwabDetail } from '../swab-data/swab-detail.entity';

export enum StatusNotifWA {
  QUEUE = 'queue',
  PROCESS = 'process',
  SENT = 'sent',
  FAILED = 'failed',
}

@Entity()
export class SwabNotificationWhatsapp {
  @PrimaryGeneratedColumn()
  id?: number;
  
  @OneToOne(type => SwabDetail, {onDelete: 'CASCADE', onUpdate: 'CASCADE'})
  @JoinColumn()
  swabDetail: SwabDetail

  @Column({
    enum: StatusNotifWA,
    default: StatusNotifWA.QUEUE,
  })
  status?: StatusNotifWA;

  @Column({ nullable:true })
  messageId?: string;
  
  @Column({nullable: true})
  message?: string;

  @CreateDateColumn()
  createdAt?: Date;
}