import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { RefDetail } from '../ref/ref-detail.entity';
import { FaspayMerchant } from './faspay-merchant.entity';

@Entity()
export class FaspayMerchantRef {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => FaspayMerchant)
  @JoinColumn()
  merchant: FaspayMerchant

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail
}
