import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class FaspayMerchant {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true, nullable: true})
  slug: string;

  @Column()
  merchantId: number;

  @Column()
  merchantName: string;
}
