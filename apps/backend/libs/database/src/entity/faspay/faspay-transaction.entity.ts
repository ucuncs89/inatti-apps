import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { SwabOrderTrx } from '../swab-data/order-trx.entity';

@Entity()
export class FaspayTransaction {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => SwabOrderTrx, {onDelete: 'SET NULL', onUpdate: 'CASCADE'})
  @JoinColumn()
  orderTrx: SwabOrderTrx

  @Column({type: 'jsonb'})
  requestApi: any;

  @Column({type: 'jsonb'})
  responseApi: any;

  @Column()
  transactionFaspayId: string;

  @CreateDateColumn()
  createdAt: Date;
}
