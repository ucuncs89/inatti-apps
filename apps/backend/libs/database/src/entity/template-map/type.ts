export type TemplateSertificateColumn =
  'headerLeftImage' | 
  'headerRightImage' | 
  'signaturImage'

export const helperCheckColumn = (column:string) => [
  'headerLeftImage',
  'headerRightImage',
  'signaturImage',
].includes(column)