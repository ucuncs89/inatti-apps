import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { LabDetail } from '../lab/lab-detail.entity';

@Entity()
export class LabTemplateSertifikat {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail;

  @Column({nullable: true})
  headerLeftImage: string;

  @Column({nullable: true})
  headerRightImage: string;

  @Column({nullable: true})
  signaturImage: string;

  @Column({nullable: true})
  doctorName: string;

  @Column({nullable: true})
  doctorSubname: string;

  @Column({nullable: true})
  nameLab: string;

  @Column({nullable: true})
  detailLab: string;

  @Column({nullable: true})
  reagent: string;

  @Column({nullable: true})
  note: string;

  @Column({nullable: true})
  genTarget: string;
}