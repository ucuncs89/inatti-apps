import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Product } from '../ref/product.entity';
import { RefDetail } from '../ref/ref-detail.entity';

@Entity()
export class RefTemplateSertifikat {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail;

  @ManyToOne(type => Product, {nullable: true})
  @JoinColumn()
  typeProduct: Product

  @Column({nullable: true})
  headerLeftImage: string;

  @Column({nullable: true})
  headerRightImage: string;

  @Column({nullable: true})
  signaturImage: string;

  @Column({nullable: true})
  doctorName: string;

  @Column({nullable: true})
  doctorSubname: string;

  @Column({nullable: true})
  refName: string;
  
  @Column({nullable: true})
  refDetail: string;
}
