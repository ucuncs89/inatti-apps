import { User } from './user/user.entity';
import { RefDetail } from './ref/ref-detail.entity';
import { RefRoleUser } from './ref/ref-role-user.entity';
import { RefRole } from './ref/ref-role.entity';
import { SwabPatient } from './swab-data/patient.entity';
import { Product } from './ref/product.entity';
import { RefProduct } from './ref/ref-product.entity';
import { Kabupaten } from './region/kabupaten.entity';
import { Kecamatan } from './region/kecamatan.entity';
import { Provinsi } from './region/provinsi.entity';
import { SwabDetail } from './swab-data/swab-detail.entity';
import { LabDetail } from './lab/lab-detail.entity';
import { RefShift } from './ref/ref-shift.entity';
import { SwabOrder } from './swab-data/order.entity';
import { SwabOrderTrx } from './swab-data/order-trx.entity';
import { LabRole } from './lab/lab-role.entity';
import { LabRoleUser } from './lab/lab-role-user.entity';
import { LabExtractionSessions } from './mapping/extraction/lab-extraction-session';
import { LabExtractionSessionDetail } from './mapping/extraction/lab-extraction-session-detail';
import { LabAnalyzeSessions } from './mapping/analyze/lab-analyze-session.entity';
import { LabAnalyzeSessionDetail } from './mapping/analyze/lab-analyze-session-detail.entity';
import { AnalyzeTool } from './mapping/analyze/analyze-tool.entity';
import { LabAnalyzeResult } from './mapping/analyze/lab-analyze-result.entity';
import { LabAnalyzeDoctor } from './lab/lab-analyze-doctor.entity';
import { Kelurahan } from './region/kelurahan.entity';
import { LabHandoverBatch } from './lab/lab-handover-batch.entity';
import { LabHandoverMap } from './lab/lab-handover-map.entity';
import { LabHistoryStatusSample } from './lab/lab-history-sample.entity';
import { LabStatusSample } from './lab/lab-status-sample.entity';
import { RefOld } from './ref/ref-old.entity';
import { FaspayMerchant } from './faspay/faspay-merchant.entity';
import { FaspayMerchantRef } from './faspay/faspay-merchant-ref.entity';
import { FaspayTransaction } from './faspay/faspay-transaction.entity';
import { NarSample } from './nar/nar-sample.entity';
import { Otp } from './user/otp.entity';
import { AdminRole } from './admin/admin-role.entity';
import { AdminRoleUser } from './admin/admin-role-user.entity';
import { ReportCacheLab } from './report/report-cache-lab.entity';
import { ReportCacheRef } from './report/report-cache-ref.entity';
import { RefTemplateSertifikat } from './template-map/ref-template-sertifikat.entity';
import { LabTemplateSertifikat } from './template-map/lab-template-sertifikat.entity';
import { ReportCacheAdmin } from './report/report-cache-admin.entity';
import { MethodCash } from './swab-data/method-cash.entity';
import { RefConfig } from './ref/ref-config.entity';
import { LabConfig } from './lab/lab-config.entity';
import { ConfigApp } from './config/config-app.entity';
import { RefQuickForm } from './ref/ref-quick-form.entity';
import { SwabNotificationWhatsapp } from './swab-notification/swab-notify-wa.entity';
import { LoggerActivity } from './logger-activity/logger-activity.entity';
import { LoggerActivityType } from './logger-activity/logger-activity-type.entity';
import { ApotekDetail } from './apotek/apotek-detail.entity';
import { ApotekGroupDetail } from './apotek-group/apotek-group-detail.entity';
import { ApotekRole } from './apotek/apotek-role.entity';
import { ApotekRoleUser } from './apotek/apotek-role-user.entity';
import { ApotekGroupRole } from './apotek-group/apotek-group-role.entity';
import { ApotekGroupRoleUser } from './apotek-group/apotek-group-role-user.entity';
import { ChatRoomEntity } from './chat/chat-room.entity';
import { ChatParticipantEntity } from './chat/chat-participant.entity';
import { ChatMessageEntity } from './chat/chat-message.entity';
import { ApotekerDetail } from './apotek/apoteker-detail.entity';
import { PmrPatient } from './pmr/pmr-patient.entity';
import { PmrMedicine } from './pmr/pmr-medicine.entity';
import { Medicine } from './medicine/medicine.entity';
import { DashboardReportCacheApp } from './report/dashboard-report-cache-app.entity';
import { Vaksin } from './vaksin/vaksin.entity';
import { ApotekHeadOffice } from './apotek-head-office/apotek-head-office.entity';
import { RefPaymentGateway } from './payment-gateway/ref-payment-gateway.entity';
import { XenditSubject } from './payment-gateway/xendit-subject.entity';
import { XenditTransaction } from './payment-gateway/xendit-transaction.entity';
import { VaksinNotificationWhatsapp } from './vaksin/vaksin-notification.entity';
import { ApotekHeadofficeAssigned } from './apotek-head-office/apotek-head-office-assigned.entity';
import { ApotekHeadofficeRole } from './apotek-head-office/apotek-head-office-role.entity';
import { ApotekHeadofficeRoleUser } from './apotek-head-office/apotek-head-office-role-user.entity';

export const Entities = [
  // all user
  User,
  Otp,

  // ref
  RefDetail,
  RefRoleUser,
  RefRole,
  RefProduct,
  RefShift,
  RefOld,
  Product,
  RefConfig,
  RefQuickForm,
  RefPaymentGateway,

  // This name maybe need to refactor
  // about swab
  SwabOrderTrx,
  SwabOrder,
  MethodCash,
  SwabPatient,
  SwabDetail,

  // common data
  Kabupaten,
  Kecamatan,
  Provinsi,
  Kelurahan,

  // lab
  LabDetail,
  LabRole,
  LabRoleUser,
  LabHandoverBatch,
  LabHandoverMap,
  LabHistoryStatusSample,
  LabStatusSample,
  LabConfig,

  // extraction
  LabExtractionSessions,
  LabExtractionSessionDetail,

  // Analyze
  LabAnalyzeSessions,
  LabAnalyzeSessionDetail,
  AnalyzeTool,
  LabAnalyzeResult,

  // Analyze Doctor
  LabAnalyzeDoctor,

  // Faspay
  FaspayMerchant,
  FaspayMerchantRef,
  FaspayTransaction,

  // Nar
  NarSample,

  // Admin
  AdminRole,
  AdminRoleUser,

  // Report Cache
  ReportCacheLab,
  ReportCacheRef,
  ReportCacheAdmin,
  DashboardReportCacheApp,

  RefTemplateSertifikat,
  LabTemplateSertifikat,

  // Config Inatti
  ConfigApp,

  // Swab Notification
  SwabNotificationWhatsapp,

  // Logger activity
  LoggerActivity,
  LoggerActivityType,

  // Apotek
  ApotekDetail,
  ApotekRole,
  ApotekRoleUser,
  ApotekerDetail,

  // Apotek Group
  ApotekGroupDetail,
  ApotekGroupRole,
  ApotekGroupRoleUser,

  // Apotek headoffice
  ApotekHeadOffice,
  ApotekHeadofficeRole,
  ApotekHeadofficeRoleUser,
  ApotekHeadofficeAssigned,

  // Chat
  ChatRoomEntity,
  ChatParticipantEntity,
  ChatMessageEntity,

  // PMR
  PmrPatient,
  PmrMedicine,

  // Medicine
  Medicine,

  // Vaksin
  Vaksin,
  VaksinNotificationWhatsapp,

  // xendit
  XenditSubject,
  XenditTransaction,
]