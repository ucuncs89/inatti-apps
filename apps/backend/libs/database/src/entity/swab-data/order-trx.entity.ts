import { AfterLoad, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";
import { RefProduct } from "../ref/ref-product.entity";
import { User } from "../user/user.entity";
import { MethodCash } from "./method-cash.entity";
import { SwabOrder } from "./order.entity";
import { SwabDetail } from "./swab-detail.entity";
import { MethodPaymentEnum, StatusOrderEnum } from "./type";

export class PaymentChannel {
  id: number;
  name: string;
}

/**
 * @desc This name 'swab' is because version 1 inatti started from 'swab test'
 */
@Entity({ name: 'swab_order_trx' })
export class SwabOrderTrx {
  constructor(id?: string) {
    this.id = id
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  // Generated
  @Column({ unique: true })
  trxId: string;

  // Generated
  @Column({ unique: true })
  invId: string;

  @ManyToOne(() => User, { onUpdate: 'CASCADE', onDelete: 'NO ACTION', nullable: true })
  @JoinColumn()
  customer?: User;

  @ManyToOne(() => RefDetail, { onUpdate: 'CASCADE', onDelete: 'NO ACTION' })
  @JoinColumn()
  reference: RefDetail;

  @ManyToOne(() => RefProduct, { nullable: true, onUpdate: 'CASCADE', onDelete: 'NO ACTION' })
  @JoinColumn({ name: 'product_swab_id' })
  product: RefProduct;

  @Column({ type: "enum", enum: StatusOrderEnum })
  status?: StatusOrderEnum;

  @Column()
  totalPrice?: number;

  @Column({ nullable: true })
  feeAdmin?: number;

  @Column({ type: "enum", enum: MethodPaymentEnum, nullable: true })
  method?: MethodPaymentEnum;

  @ManyToOne(() => MethodCash, { nullable: true })
  @JoinColumn()
  methodCash: MethodCash;

  @Column({ nullable: true })
  paymentChannelCode?: string;

  @Column({ nullable: true })
  paymentChannelName?: string;

  @OneToMany(() => SwabOrder, orderSwab => orderSwab.orderTrx)
  orderSwab?: SwabOrder[];

  totalOrder: number;
  getTotalOrder() {
    this.totalOrder = this.orderSwab.length
    delete this.orderSwab
  }

  @Column({ name: "created_at", default: () => "CURRENT_TIMESTAMP" })
  createdAt?: Date;

  @Column({ nullable: true })
  expiredAt?: Date;

  /**
   * @deprecated this variable should not used since other type of product (vaksin) is added
   */
  @OneToMany(() => SwabDetail, swabDetail => swabDetail.orderTrx, { nullable: true })
  sample?: SwabDetail
}