import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class MethodCash {
  constructor(id?:number){
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({nullable: true})
  description: string;
}
