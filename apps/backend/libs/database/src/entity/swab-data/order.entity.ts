import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";
import { User } from "../user/user.entity";
import { SwabDetail } from "./swab-detail.entity";
import { SwabOrderTrx } from "./order-trx.entity";
import { MethodPaymentEnum, StatusOrderEnum } from "./type";
import { RefProduct } from "../ref/ref-product.entity";
import { MethodCash } from "./method-cash.entity";

/**
 * @desc This name 'swab' is because version 1 inatti started from 'swab test'
 */
@Entity({ name: 'swab_order' })
export class SwabOrder {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  /**
   * @deprecated this columnn should not be used since other type of product included
   */
  @OneToOne(() => SwabDetail, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
  @JoinColumn({ name: 'detail_swab_id' })
  detailSwab?: SwabDetail;

  @Column({ type: Number })
  price: number;

  @Column({ type: 'enum', enum: MethodPaymentEnum })
  method: MethodPaymentEnum;

  @ManyToOne(() => MethodCash, { onUpdate: 'CASCADE', onDelete: 'NO ACTION', nullable: true })
  @JoinColumn()
  methodCash: MethodCash;

  /**
   * this must be reference to user entity
   * if null, it's mean transaction from reference
   */
  @ManyToOne(() => User, { onUpdate: 'CASCADE', onDelete: 'NO ACTION', nullable: true })
  @JoinColumn({ name: "customer_id" })
  customer?: User;

  @ManyToOne(() => RefDetail)
  @JoinColumn({ name: 'reference_id' })
  reference: RefDetail;

  @ManyToOne(() => RefProduct, { onUpdate: 'CASCADE', onDelete: 'NO ACTION', nullable: true })
  @JoinColumn({ name: 'product_swab_id' })
  product: RefProduct;

  @Column({ type: 'enum', enum: StatusOrderEnum, default: StatusOrderEnum.new })
  status?: StatusOrderEnum;

  @ManyToOne(() => SwabOrderTrx, { onUpdate: 'CASCADE', onDelete: 'CASCADE', nullable: true })
  @JoinColumn({ name: "order_trx_swab_id" })
  orderTrx?: SwabOrderTrx;

  @Column({ name: 'created_at', type: 'timestamptz', default: () => "CURRENT_TIMESTAMP" })
  createdAt?: Date;
}