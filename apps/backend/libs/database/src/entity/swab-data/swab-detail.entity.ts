import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { LabDetail } from "../lab/lab-detail.entity";
import { LabHistoryStatusSample } from "../lab/lab-history-sample.entity";
import { LabAnalyzeSessionDetail } from "../mapping/analyze/lab-analyze-session-detail.entity";
import { LabExtractionSessionDetail } from "../mapping/extraction/lab-extraction-session-detail";
import { NarSample } from "../nar/nar-sample.entity";
import { Product } from "../ref/product.entity";
import { RefDetail } from "../ref/ref-detail.entity";
import { RefShift } from "../ref/ref-shift.entity";
import { SwabNotificationWhatsapp } from "../swab-notification/swab-notify-wa.entity";
import { User } from "../user/user.entity";
import { SwabOrderTrx } from "./order-trx.entity";
import { SwabOrder } from "./order.entity";
import { SwabPatient } from "./patient.entity";
import { ResultSwabEnum, StatusSwabEnum } from "./type";

@Entity({ name: 'swab_detail' })
export class SwabDetail {
  constructor(id?: number) {
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => SwabPatient, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'patient_id' })
  patient: SwabPatient;

  // Generated
  @Column({ name: 'sample_id', nullable: true })
  sampleId: string;

  @ManyToOne(() => Product, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'product_swab_id' })
  productSwab: Product;

  @ManyToOne(() => RefDetail, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'ref_target' })
  refTarget: RefDetail;

  @ManyToOne(() => RefShift, { nullable: true, onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'ref_shift_schedule_id' })
  refShiftSchedule: RefShift;

  @Column({
    type: 'enum',
    enum: StatusSwabEnum,
    default: StatusSwabEnum.registered
  })
  status?: StatusSwabEnum;

  @Column({ nullable: true })
  requestSwabDate: Date;

  // date when swabbers take the sample
  @Column({ name: 'swab_date', nullable: true })
  swabDate?: Date;

  @ManyToOne(() => User, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'swab_by' })
  swabBy?: User;

  @Column({
    name: 'result_swab',
    type: 'enum',
    enum: ResultSwabEnum,
    nullable: true,
  })
  resultSwab?: ResultSwabEnum;

  @Column({ nullable: true })
  resultSentAt: Date;

  @Column({
    name: 'is_send_to_lab',
    type: 'bool',
    nullable: true
  })
  isSendToLab?: boolean;

  @ManyToOne(() => LabDetail, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'lab_target' })
  labTarget?: LabDetail;

  @Column({ name: 'sendtolab_date', nullable: true })
  sendToLabDate?: Date;

  @OneToOne(() => SwabOrder, { nullable: true, onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'order_detail_id' })
  orderSwab?: SwabOrder;

  @ManyToOne(() => SwabOrderTrx, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  orderTrx?: SwabOrderTrx

  @Column({ name: 'created_at', type: "timestamptz", default: () => "CURRENT_TIMESTAMP" })
  createdAt?: Date;

  @Column({ nullable: true })
  narSendat?: Date;

  @Column({ nullable: true })
  narTestcovidId?: string;

  @OneToMany(() => LabExtractionSessionDetail, labExtraction => labExtraction.sample, { nullable: true })
  labExtraction: LabExtractionSessionDetail[];

  @OneToMany(() => LabAnalyzeSessionDetail, labAnalyzeSession => labAnalyzeSession.sample)
  labAnalyzeSession: LabAnalyzeSessionDetail[];

  @OneToMany(() => LabHistoryStatusSample, history => history.sample)
  history: LabHistoryStatusSample[];

  @OneToOne(() => NarSample, sample => sample.swabDetail)
  narSample: NarSample;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn()
  OwnerBy: User

  @Column({ nullable: true })
  codeBooking: string;

  @OneToOne(() => SwabNotificationWhatsapp, sample => sample.swabDetail)
  notifyWhataspp: SwabNotificationWhatsapp
}

/**
 * This function to generate id sample like A004123, B523132
 * @param detailSwab 
 * @returns 
 */
export function IdSampleGenerator(detailSwab: SwabDetail) {
  if (!detailSwab.id) {
    throw new Error("id detail swab not found")
  }
  let roundCode = 1000000
  let prefixCode = Math.round(detailSwab.id / roundCode)
  let prefixAscii = String.fromCharCode(prefixCode + 65) // 65 = A
  let id = detailSwab.id % roundCode
  let result = prefixAscii + ("000000" + id).slice(-6)
  return result;
}