import { RelationsEntity } from "@app/database/utils/relations-entity";
import { Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ApotekDetail } from "../apotek/apotek-detail.entity";
import { Kabupaten } from "../region/kabupaten.entity";
import { Kecamatan } from "../region/kecamatan.entity";
import { Kelurahan } from "../region/kelurahan.entity";
import { Provinsi } from "../region/provinsi.entity";
import { User } from "../user/user.entity";
import { SwabDetail } from "./swab-detail.entity";
import { BloodTypeEnum, GenderEnum, NikTypeEnum } from "./type";

/**
 * This table maybe need to normalize
 */
@Entity({ name: 'swab_patient' })
export class SwabPatient {
  constructor(id?: string) {
    this.id = id;
  }

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ nullable: true })
  nik: string;

  @Column({ nullable: true })
  passport: string;

  @Column({ type: "enum", enum: NikTypeEnum })
  type: NikTypeEnum;

  @Column()
  name: string;

  @Column({ nullable: true })
  placebirth: string;

  @Column({ nullable: true })
  datebirth: string;

  @Column({ type: "enum", enum: GenderEnum, nullable: true })
  gender: GenderEnum;

  @Column({ nullable: true })
  phonenumber?: string;

  @Column({ nullable: true })
  email?: string;

  @Column({ type: "enum", enum: BloodTypeEnum, nullable: true })
  bloodtype?: BloodTypeEnum;

  @ManyToOne(() => Provinsi, { nullable: true })
  @JoinColumn({ name: 'provinsi_id' })
  provinsi: Provinsi;

  @ManyToOne(() => Kabupaten, { nullable: true })
  @JoinColumn({ name: 'kabupaten_id' })
  kabupaten: Kabupaten;

  @ManyToOne(() => Kecamatan, { nullable: true })
  @JoinColumn({ name: 'kecamatan_id' })
  kecamatan: Kecamatan;

  @ManyToOne(() => Kelurahan, { nullable: true })
  @JoinColumn({ name: 'kelurahan_id' })
  kelurahan: Kelurahan;

  @Column({ nullable: true })
  kelurahanRt: string;

  @Column({ nullable: true })
  kelurahanRw: string;

  @ManyToOne(() => Provinsi, { nullable: true })
  @JoinColumn()
  domisiliProvinsi: Provinsi;

  @ManyToOne(() => Kabupaten, { nullable: true })
  @JoinColumn()
  domisiliKabupaten: Kabupaten;

  @ManyToOne(() => Kecamatan, { nullable: true })
  @JoinColumn()
  domisiliKecamatan: Kecamatan;

  @ManyToOne(() => Kelurahan, { nullable: true })
  @JoinColumn()
  domisiliKelurahan: Kelurahan;

  @Column({ nullable: true })
  domisiliKelurahanRt: string;

  @Column({ nullable: true })
  domisiliKelurahanRw: string;

  @Column({ nullable: true })
  address: string;

  @Column({ name: "address_domisili", nullable: true })
  addressDomisili?: string;

  @Column({ nullable: true })
  citizenship?: string;

  @Column({ nullable: true })
  weight?: number;

  @Column({ nullable: true })
  height?: number;

  @Column({ nullable: true })
  job?: string;

  @OneToMany(() => SwabDetail, detailSwab => detailSwab.patient, { nullable: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  detailSwab?: SwabDetail[];

  @Column({
    name: 'created_at',
    type: "timestamptz",
    default: () => "CURRENT_TIMESTAMP"
  })
  createdAt: Date;

  @Column({ nullable: true })
  narOrangId: string

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn()
  ownedBy: User

  /**
   * @since client request to add 'homecare'
   */
  @Column({ nullable: true, type: 'jsonb' })
  etc?: any

  @ManyToOne(() => ApotekDetail, table => table.id, { nullable: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  createByApotek?: ApotekDetail

  @DeleteDateColumn()
  deleteAt?: Date
}

export const PatientRelationsAll = RelationsEntity([
  'provinsi',
  'kabupaten',
  'kecamatan',
  'kelurahan',
  'domisiliProvinsi',
  'domisiliKabupaten',
  'domisiliKecamatan',
  'domisiliKelurahan',
  'ownedBy',
])