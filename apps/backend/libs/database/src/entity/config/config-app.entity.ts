import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class ConfigApp {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true})
  name: string;

  @Column({nullable: true})
  value: string;

  @Column({type: 'jsonb', nullable: true})
  valueJson: Object|any[];

  @Column({nullable: true})
  category: string;
}
