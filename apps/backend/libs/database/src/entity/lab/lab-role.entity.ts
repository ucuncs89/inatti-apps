import { Entity } from "typeorm";
import { RoleModel } from "../role/role.model";

@Entity('lab_role')
export class LabRole extends RoleModel { }