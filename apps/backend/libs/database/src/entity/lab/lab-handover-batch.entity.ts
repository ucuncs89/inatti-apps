import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { RefDetail } from '../ref/ref-detail.entity'
import { LabDetail } from './lab-detail.entity';
import { LabHandoverMap } from './lab-handover-map.entity';

@Entity()
export class LabHandoverBatch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  createdBy: RefDetail;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  labTarget: LabDetail;

  @CreateDateColumn()
  createdAt: Date;

  @Column({nullable: true})
  isSubmitted: boolean;

  @OneToMany(() => LabHandoverMap, map => map.batch, {nullable: true})
  mapBatch: LabHandoverMap[];
}