import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../user/user.entity";
import { LabDetail } from "./lab-detail.entity";
import { LabRole } from "./lab-role.entity";

@Entity('lab_role_user')
export class LabRoleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabDetail, { eager: true, onDelete: 'CASCADE', onUpdate: "CASCADE" })
  @JoinColumn({ name: "lab_id" })
  labId: LabDetail;

  @ManyToOne(() => User, { onDelete: 'CASCADE', onUpdate: "CASCADE" })
  @JoinColumn({ name: "user_id" })
  userId: User;

  @ManyToOne(() => LabRole, { eager: true })
  @JoinColumn({ name: "role_id" })
  roleId: LabRole;
}