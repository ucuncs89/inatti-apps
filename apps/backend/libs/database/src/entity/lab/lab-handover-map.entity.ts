import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, CreateDateColumn, OneToOne } from 'typeorm';
import { SwabDetail } from '../swab-data/swab-detail.entity';
import { LabHandoverBatch } from './lab-handover-batch.entity';

@Entity()
export class LabHandoverMap {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabHandoverBatch)
  @JoinColumn()
  batch: LabHandoverBatch

  @OneToOne(() => SwabDetail)
  @JoinColumn()
  swabDetail: SwabDetail
  
  @CreateDateColumn()
  createAt: Date;
}
