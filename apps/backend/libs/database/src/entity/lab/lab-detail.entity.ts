import { Column, DeleteDateColumn, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { LabConfig } from "./lab-config.entity";

@Entity({ name: "lab_detail" })
export class LabDetail {
  constructor(id?: string) {
    this.id = id;
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ name: 'tenant_id', nullable: true })
  tenantId: string;

  @Column({ type: 'timestamptz', name: 'created_at', default: () => "CURRENT_TIMESTAMP" })
  createdAt: Date;

  @Column({ nullable: true })
  narLabId: string;

  @Column({nullable: true})
  isDelete?: boolean

  @DeleteDateColumn()
  deleteAt?: Date

  @OneToOne(() => LabConfig, config => config.lab, {nullable: true})
  config: LabConfig
}