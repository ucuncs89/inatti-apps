import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class LabStatusSample {
  constructor(id?: number){
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({nullable: true})
  description: string;

  @Column({nullable: true})
  color: string

  // Static variable, make sure in database have same id
  static statusEntry            = new LabStatusSample(1)
  static statusSubmitted        = new LabStatusSample(2)
  static statusMappedEkstration = new LabStatusSample(3)
  static statusEkstracted       = new LabStatusSample(4)
  static statusMappedPCRPlate   = new LabStatusSample(5)
  static statusTested           = new LabStatusSample(6)
  static statusResultImported   = new LabStatusSample(7)
  static statusValidateResult   = new LabStatusSample(8)
}
