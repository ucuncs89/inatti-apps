import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { LabAnalyzeSessionDetail } from "../mapping/analyze/lab-analyze-session-detail.entity";
import { SwabDetail } from "../swab-data/swab-detail.entity";
import { ResultSwabEnum } from "../swab-data/type";
import { User } from "../user/user.entity";
import { LabDetail } from "./lab-detail.entity";

@Entity()
export class LabAnalyzeDoctor {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => SwabDetail, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  sample: SwabDetail;

  @ManyToOne(() => LabAnalyzeSessionDetail,  { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  sampleSession: LabAnalyzeSessionDetail;

  @ManyToOne(() => User)
  @JoinColumn()
  createdBy: User;

  @Column({ type: "enum", enum: ResultSwabEnum })
  result: ResultSwabEnum;

  @Column({ type: String, nullable: true })
  note: string;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail;

  @Column({ nullable: true })
  publishAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}