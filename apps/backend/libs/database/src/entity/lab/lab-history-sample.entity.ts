import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { SwabDetail } from '../swab-data/swab-detail.entity';
import { User } from '../user/user.entity';
import { LabStatusSample } from './lab-status-sample.entity'

@Entity()
export class LabHistoryStatusSample {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => SwabDetail)
  @JoinColumn()
  sample: SwabDetail

  @ManyToOne(() => LabStatusSample)
  @JoinColumn()
  status: LabStatusSample  

  @ManyToOne(() => User)
  @JoinColumn()
  createdBy: User

  @CreateDateColumn()
  createdAt: Date;
}
