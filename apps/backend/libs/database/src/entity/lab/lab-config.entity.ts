import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { LabDetail } from './lab-detail.entity'

@Entity()
export class LabConfig {
  constructor(id?:number){
    this.id = id
  }

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail

  // For now, I assume Inatti only work in Indonesia
  @Column({nullable: true})
  timezone: string
}