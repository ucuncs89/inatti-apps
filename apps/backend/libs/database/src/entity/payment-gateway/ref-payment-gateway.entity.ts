import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { RefDetail } from "../ref/ref-detail.entity";

@Entity()
export class RefPaymentGateway {
  @PrimaryGeneratedColumn('increment')
  id: number

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail

  @Column()
  subjectUserId: string

  @Column()
  provider: string

  @Column({ default: false })
  isLive: boolean

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  updateAt: Date;
}