import { XenditAccountStatus, XenditAccountType } from "apps/data/src/payment-v2/xendit/xendit.type";
import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class XenditSubject {
  @PrimaryColumn()
  subjectUserId: string

  @Column()
  email: string

  @Column({ type: 'varchar' })
  status: XenditAccountStatus

  @Column({ type: 'varchar' })
  type: XenditAccountType

  @Column({ nullable: true })
  callbackUrl?: string

  @Column({ nullable: true })
  callbackToken?: string

  @Column()
  createdAt: string

  @Column()
  updatedAt: string

  @Column({ type: "timestamptz", default: () => "CURRENT_TIMESTAMP" })
  updateSyncAt: Date;
}