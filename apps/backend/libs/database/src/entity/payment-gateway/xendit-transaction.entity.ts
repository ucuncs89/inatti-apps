import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, CreateDateColumn, PrimaryColumn } from 'typeorm';
import { SwabOrderTrx } from '../swab-data/order-trx.entity';

@Entity()
export class XenditTransaction {
  @PrimaryColumn()
  id: string;

  @OneToOne(() => SwabOrderTrx, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn()
  orderTrx: SwabOrderTrx

  @Column({ type: 'jsonb' })
  requestApi: any;

  @Column({ type: 'jsonb' })
  responseApi: any;

  @Column()
  status: string;

  @Column()
  urlRedirect: string;

  @Column({ type: 'date' })
  expiredAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}
