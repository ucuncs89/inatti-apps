import { PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
export class ReportCacheAdmin {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column({
    type: 'jsonb'
  })
  data: any;
  
  @ManyToOne(type => User, { nullable:true })
  @JoinColumn()
  user?: User;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}