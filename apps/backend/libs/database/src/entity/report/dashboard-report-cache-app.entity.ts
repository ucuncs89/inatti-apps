import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from 'typeorm';

/**
 * This is version 2 of report cache table.
 * older version devide by app (ref, lab and admin)
 */
@Entity()
export class DashboardReportCacheApp {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Should feel like this:
   * name-app:uuid-app
   * ex: ref:18f8f8f8-f8f8-f8f8-f8f8-f8f8f8f8f8f8
   */
  @Column({
    unique: true,
  })
  idApp: string;

  @Column({
    type: 'jsonb'
  })
  data: any;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt: Date;


}
