import { PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, Entity, UpdateDateColumn, CreateDateColumn } from 'typeorm';
import { RefDetail } from '../ref/ref-detail.entity';

@Entity()
export class ReportCacheRef {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RefDetail)
  @JoinColumn()
  ref: RefDetail

  @Column({
    type: 'jsonb'
  })
  data: any;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}