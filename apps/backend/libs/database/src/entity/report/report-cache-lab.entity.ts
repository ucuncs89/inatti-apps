import { PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, Entity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { LabDetail } from '../lab/lab-detail.entity';

@Entity()
export class ReportCacheLab {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LabDetail)
  @JoinColumn()
  lab: LabDetail

  @Column({
    type: 'jsonb'
  })
  data: any;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}