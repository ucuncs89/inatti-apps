import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

@Entity()
export class LoggerActivityType {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({unique: true})
  name: string;

  @Column({ nullable:true })
  activity: string;
  
  @Column()
  area?: string;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt?: Date;
}
