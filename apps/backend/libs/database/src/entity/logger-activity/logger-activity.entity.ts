import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { SwabPatient } from '../swab-data/patient.entity';
import { SwabDetail } from '../swab-data/swab-detail.entity';
import { User } from '../user/user.entity';
import { LoggerActivityType } from './logger-activity-type.entity';

@Entity()
export class LoggerActivity {
  @PrimaryGeneratedColumn()
  id?: number;

  @ManyToOne(type => LoggerActivityType)
  @JoinColumn()
  type: LoggerActivityType

  @Column({
    type: "jsonb",
    nullable: true,
  })
  params?: any;

  /**
   * This used by test covid activity
   * @deprecated this removed and change to tag
   */
  @ManyToOne(type => SwabDetail, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
  @JoinColumn()
  swabDetail: SwabDetail

  /**
   * This used by apotek
   * @deprecated this removed and change to tag
   */
  @ManyToOne(type => SwabPatient, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
  @JoinColumn()
  patient: SwabPatient

  /**
   * New version to label-ing log instead using swabDetail and patient.
   * format : type:uuid. ex: vaksin:1111-1111-1111111-1111
   */
  @Column({ nullable: true })
  tag?: String

  @ManyToOne(type => User, user => user.id, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
  @JoinColumn()
  user?: User;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt?: Date;
}
