import { RelationsEntity } from './relations-entity'

describe('test relations entity', () => {
  it('Should return function', () => {
    let patients = RelationsEntity([
      'provinsi',
      'kabupaten'
    ])
    expect(typeof patients).toBe('function')
  })
  it('Should return array', () => {
    let list = [
      'provinsi',
      'kabupaten'
    ]
    let patients = RelationsEntity(list)
    expect(patients()).toStrictEqual(list)
    expect(Array.isArray(patients())).toBe(true)
  })

  it('Should return with prefix', () => {
    let list = [
      'provinsi',
      'kabupaten'
    ]
    let patients = RelationsEntity(list)
    expect(Array.isArray(patients())).toBe(true)
    expect(patients('patient')).toStrictEqual([
      'patient.provinsi',
      'patient.kabupaten'
    ])
  })
})