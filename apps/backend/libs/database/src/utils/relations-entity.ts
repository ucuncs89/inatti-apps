export const RelationsEntity = (columns: string[]) => {
  return (prefix?: string) => {
    return columns.map((v) => prefix ? `${prefix}.${v}` : v)
  }
}