import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UserService as UserEntityService } from './entity/user/user.entities.services';
import { Entities } from './entity/index-entity';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: "postgres",
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: Entities,
      logging: process.env.DB_LOG,
      synchronize: process.env.DB_SYNC === 'true'
    }),
    TypeOrmModule.forFeature(Entities)
  ],
  providers: [UserEntityService],
  exports: [
    UserEntityService,
    TypeOrmModule.forFeature(Entities)
  ],
})
export class DatabaseModule { }
