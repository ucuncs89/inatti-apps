import { User } from '@app/database/entity/user/user.entity';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as awsS3 from "aws-sdk/clients/s3";
import awsConfig from 'config/aws.config';
import { createHmac } from 'crypto';
import * as _ from 'lodash';
import { ConfigAwsConst, OptionAWS } from './register';

interface UploadedS3File {
  originalName: string,
  hashedName: string,
  format: string,
  s3: awsS3.ManagedUpload.SendData
}

@Injectable()
export class AwsService {
  constructor(
    @Inject(ConfigAwsConst)
    private config: OptionAWS,
    private configService:ConfigService
  ){ }

  s3: awsS3;

  onModuleInit(){
    this.s3 = new awsS3({
      accessKeyId: this.configService.get<string>('aws.accessKeyId'),
      secretAccessKey: this.configService.get<string>('aws.secretAccessKey'),
    });
  }

  s3Upload(file: Buffer, name: string, metadata?:any){
    const keyName = name
    return this.s3.upload({
      Bucket: this.config.bucket,
      Key: keyName,
      Body: file,
      Metadata: metadata,
    }).promise()
  }

  generatedFoldername(fileHased: string, folderName?: string){
    folderName = folderName ?? this.config.uploadFolder
    fileHased = folderName + "/" + fileHased;
    let isDev = awsConfig().isDevelopment
    if(isDev){
      fileHased = `dev-file/${fileHased}`
    }
    return fileHased
  }

  // helper
  async HashAndUploadS3(file: Express.Multer.File, folderName?: string, metadata = {}): Promise<UploadedS3File>{
    Object.assign(metadata, this.createMetadata({
      'file-name': file.originalname,
      'file-format': file.mimetype,
    }))

    const secret = new Date().getTime()
    const fileName = {
      originalName: file.originalname,
      hashedName: null,
      format: file.originalname.split('.').pop(),
      s3: null
    }
    
    let nameHashed = createHmac('md5', secret+"").update(file.originalname).digest('hex') + "." + fileName.format;
    // if has foldername in param, use it. if not, use config folder name
    nameHashed = this.generatedFoldername(nameHashed, folderName)
    fileName.hashedName = nameHashed;
  
    let resultS3:awsS3.ManagedUpload.SendData = await this.s3Upload(file.buffer, nameHashed, metadata);
    fileName.s3 = resultS3;
  
    return fileName
  }

  private createMetadata(data:Record<string, string>){
    return _.mapKeys(data, (value, key) => {
      return `x-amz-meta-${key}`
    })
  }

  setMetadataUser(user: User){
    let data = {
      'user-id': user.id,
      'user-fullname': user.fullname,
      'user-username': user.username,
    }
    return this.createMetadata(data)
  }

  private getKeyFromUrl(url: string){
    let rgx = new RegExp(/https:\/\/[^\/]+\/(.*)/)
    let res = url.match(rgx)
    return res[1]
  }

  async getFile(url: string){
    let key = this .getKeyFromUrl(url)
    if(!key) throw new Error("No key found")

    // amazon s3 get file from url
    let res = await this.s3.getObject({
      Bucket: this.config.bucket,
      Key: key,
    }).promise()
    
    // omit body (binary file)
    return _.omit(res, [
      'Body'
    ])
  }
}
