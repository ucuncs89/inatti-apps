import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AwsService } from './aws.service';
import { ConfigAwsConst, OptionAWS } from './register';
import ConfigAws from '../../../config/aws.config'

@Module({})
export class AwsModule {
  static register(optionAWS: OptionAWS): DynamicModule {
    return {
      module: AwsModule,
      imports: [
        ConfigModule.forFeature(ConfigAws),
      ],
      providers: [
        AwsService,
        {
          provide: ConfigAwsConst,
          useValue: optionAWS,
        },
      ],
      exports: [
        AwsService,
      ],
    }
  }
}
