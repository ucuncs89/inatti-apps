export interface OptionAWS {
  bucket:string,
  uploadFolder?: string
}

export const ConfigAwsConst = 'ConfigAwsConst';