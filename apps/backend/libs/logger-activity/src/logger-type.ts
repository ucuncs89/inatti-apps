export interface TypeLog {

  label: string,
  activity: string,
}

export const LoggerActivityList = {
  user: {
    createOrder: {
      label: "user.patient_create_order",
      activity: "Patient {patient} create order {orderTrxId}"
    },
    updateOrderStatus: {
      label: "user.update_order_status",
      activity: "Order {orderTrxId} set status to {status}"
    },
  },
  ref: {
    registerPatientSwab: {
      label: "ref.register_patient_swab",
      activity: "Patient registered to test"
    },
    updateStatusOrder: {
      label: "ref.update_status_order",
      activity: "Order {orderTrxId} update status to {status}"
    },
    updateCodeBooking: {
      label: "ref.update_code_booking",
      activity: "Update code booking to {code}"
    },
    updateStatusSwab: {
      label: "ref.update_status_swab",
      activity: "Update status swab to {status}"
    },
    sentToLab: {
      label: "ref.swab_sent_to_lab",
      activity: "Sent to lab {labId}"
    },
    updateResultSwab: {
      label: "ref.update_result_swab",
      activity: "Update result swab to {result}"
    },
    sendNotificationWhatsapp: {
      label: "ref.send_notification_whatsapp",
      activity: "Send notification whatsapp with status {responseWhatsapp}"
    },
    checkNotificationWhatsapp: {
      label: "ref.check_notification_whatsapp",
      activity: "Check notification whatsapp with status {responseWhatsapp}"
    }
  },
  lab: {
    submitSample: {
      label: "lab.submit_sample",
      activity: "Swab submitted to lab {labId}"
    },
    updateStatusSample: {
      label: 'lab.update_status_sample',
      activity: "Update status sample {status}"
    },
    removeStatusSample: {
      label: 'lab.remove_status_sample',
      activity: "Remove status sample {status}"
    },
    updateAnalyzeResultByTool: {
      label: "lab.update_analyze_result_by_tool",
      activity: "Update Result by tool to {result}"
    },
    updateAnalyzeResultByAnalisis: {
      label: "lab.update_analyze_result_by_analisis",
      activity: "Update Result by analisis to {result}"
    },
    updateAnalyzeResultByDoctor: {
      label: "lab.update_analyze_result_by_doctor",
      activity: "Update Result by doctor to {result}"
    },
    publishResult: {
      label: "lab.publish_result",
      activity: "Publish final result {result}"
    },
  },
  apotek: {
    /**
     * submitSample: {
      label: "lab.submit_sample",
      activity: "Swab submitted to lab {labId}"
    },
     */
    createPmr: {
      label: "apotek.createPmr",
      activity: "New PMR created, PMR id {pmrId}"
    },
    updatePmr: {
      label: "apotek.updatePmr",
      activity: "PMR with id {pmrId} updated"
    },
    deletPmr: {
      label: "apotek.deletePmr",
      activity: "PMR with id {pmrId} deleted"
    }
  },
  refVaksin: {
    orderVaksin: {
      label: "refVaskin.orderVaksin",
      activity: "Vaksin order created"
    },
    checkin: {
      label: "refVaksin.checkin",
      activity: "Patient checkin"
    },
    vaksinned: {
      label: "refVaksin.checkin",
      activity: "Patient vaccined"
    },
    notify: {
      label: "refVaksin.notify",
      activity: "Patient notified by whatsapp"
    },
    updateNotify: {
      label: "refVaksin.updateNotify",
      activity: "Check notification whatsapp"
    }
  },
  adminVerifyApotek: {
    verifyApotek: {
      label: "adminVerifyApotek.verifyApotek",
      activity: "Apotek {apotekName} change status to {status}"
    }
  }
}