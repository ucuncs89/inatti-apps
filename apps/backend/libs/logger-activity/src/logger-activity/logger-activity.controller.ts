import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { LoggerActivityService } from '../logger-activity.service';
import { createTagLog } from '../utils-logger-activity';

@ApiTags('Logger activity')
@RequireAuthApp(...AppsExcludeUser)
@Controller('logger-activity')
export class LoggerActivityController {
  constructor(
    private activity: LoggerActivityService
  ) { }

  @ApiQuery({
    name: 'area',
    required: false
  })
  @Get('/sample')
  async getLoggerSampleByArea(
    @Query('sampleId')
    sampleId: string,
    @Query('area')
    area?: string,
  ) {
    try {
      return await this.activity.getLoggerSampleByArea(sampleId, area)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @ApiQuery({
    name: 'area',
    required: false
  })
  @Get('/patient')
  async getLoggerPatientByArea(
    @Query('patientId')
    patientId: string,
    @Query('area')
    area?: string,
  ) {
    try {
      return await this.activity.getLoggerPatientByArea(patientId, area)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @ApiQuery({
    name: 'area',
    required: false
  })
  @ApiQuery({
    name: 'label',
    description: "Can be : 'vaksin', 'pmr', etc"
  })
  @Get('/tag')
  async getLoggerPatientByTag(
    @Query('label')
    label: string,
    @Query('id')
    id: string,
    @Query('area')
    area?: string,
  ) {
    try {
      const tag = createTagLog(label, id)
      return await this.activity.getLoggerByTag(tag, area)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }
}
