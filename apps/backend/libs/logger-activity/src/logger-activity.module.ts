import { DatabaseModule } from '@app/database';
import { DynamicModule, Module } from '@nestjs/common';
import { LoggerActivityService } from './logger-activity.service';
import { OptionsLoggerActivity, OptionsLoggerActivityConst } from './type';
import { LoggerActivityController } from './logger-activity/logger-activity.controller';

@Module({
  controllers: [LoggerActivityController]
})
export class LoggerActivityModule {
  static register(options: OptionsLoggerActivity): DynamicModule{
    return {
      module: LoggerActivityModule,
      imports: [
        DatabaseModule,
      ],
      providers: [
        LoggerActivityService,
        {
          provide: OptionsLoggerActivityConst,
          useValue: options
        }
      ],
      exports: [
        LoggerActivityService,
      ],
    }
  }
}
