import { TypeLog } from "./logger-type";

export interface OptionsLoggerActivity {
  area: string;
}

export const OptionsLoggerActivityConst = "OPTION_LOGGER_ACTIVITY_CONST"

export class LogActivityCreate {
  type: TypeLog
  /**
   * @deprecated
   */
  swabDetail?: any
  /**
   * @deprecated
   */
  patientId?: any
  tag?: string
  params?: any
  user?: any
}