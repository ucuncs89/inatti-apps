import { LoggerActivityType } from '@app/database/entity/logger-activity/logger-activity-type.entity';
import { LoggerActivity } from '@app/database/entity/logger-activity/logger-activity.entity';
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LoggerActivityList, TypeLog } from './logger-type';
import { LogActivityCreate, OptionsLoggerActivity, OptionsLoggerActivityConst } from './type';

@Injectable()
export class LoggerActivityService {
  constructor(
    @InjectRepository(LoggerActivity)
    private LoggerActivityRepository: Repository<LoggerActivity>,
    @InjectRepository(LoggerActivityType)
    private LoggerActivityTypeRepository: Repository<LoggerActivityType>,
    @Inject(OptionsLoggerActivityConst)
    private readonly optionsModule: OptionsLoggerActivity
  ) { }

  private logger = new Logger('Logger Activity')

  readonly activityList = LoggerActivityList

  /**
   * Method to save logs
   */
  async log(logActivity: LogActivityCreate) {
    this.logger.log(logActivity)
    let type = await this.LoggerActivityTypeRepository.findOne({ name: logActivity.type.label });
    if (!type) {
      type = new LoggerActivityType();
      type.name = logActivity.type.label;
      type.area = this.optionsModule.area;
      type.activity = logActivity.type.activity;
      type = await this.LoggerActivityTypeRepository.save(type)
    }
    let activity = new LoggerActivity();

    activity.params = logActivity.params
    activity.user = logActivity.user
    activity.swabDetail = logActivity.swabDetail
    activity.type = type;
    activity.patient = logActivity.patientId
    activity.tag = logActivity.tag

    return await this.LoggerActivityRepository.save(activity)
  }

  async getLoggerSampleByArea(sampleId: string, area?: string) {
    let where: Partial<LoggerActivity> = {
      swabDetail: new SwabDetail(+sampleId),
    }
    return await this.getLogger(where, area)
  }

  async getLoggerPatientByArea(patientId: string, area?: string) {
    let where: Partial<LoggerActivity> = {
      patient: new SwabPatient(patientId)
    }
    return await this.getLogger(where, area)
  }

  async getLoggerByTag(tag: string, area?: string) {
    return await this.getLogger({
      tag
    }, area)
  }

  async getLogger(where: any, area?: string) {
    if (area) {
      where.type = {
        area,
      }
    }
    let res = await this.LoggerActivityRepository.find({
      where,
      relations: [
        'type',
        'user'
      ],
      order: {
        createdAt: 'DESC'
      }
    })

    return res.map((v) => {
      v['formatted_activity'] = this.formatText(v.type.activity, v.params)
      if (v.user) {
        v.user.hideCredential()
      }
      return v;
    })
  }

  formatText(template: string, params: Record<string, string>) {
    for (const key in params) {
      let value = params[key]
      template = template.replace(`{${key}}`, value)
    }
    return template
  }
}
