import { createTagLog } from './utils-logger-activity'

describe('Test createTagLog', () => {
  it('Should return string tagname:id', () => {
    const tagName = 'foo'
    const id = 4123
    const res = createTagLog(tagName, id)
    expect(res).toBe(`${tagName}:${id}`)
    expect(typeof res).toBe('string')
  })
})