/**
 * Create tag for logger
 * @param label 
 * @param id 
 * @returns 
 */
export const createTagLog = (label: string, id: string | number) => {
  return `${label}:${id}`
}