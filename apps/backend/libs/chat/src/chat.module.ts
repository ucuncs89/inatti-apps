import { AuthenticationModule } from '@app/authentication';
import { DatabaseModule } from '@app/database';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ChatWebsocketGateway } from './websocket/chat-websocket.gateway';

@Module({
  imports: [
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.User }),
  ],
  providers: [ChatService, ChatWebsocketGateway],
  exports: [ChatService],
})
export class ChatModule {}
