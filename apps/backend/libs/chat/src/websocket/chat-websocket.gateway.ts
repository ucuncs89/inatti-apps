import { JwtAuthGuard } from "@app/authentication/authorization/jwt.authguard";
import { JwtAuthWebsocket } from "@app/authentication/authorization/jwt.authguard.websocket";
import { Logger, UseGuards } from "@nestjs/common";
import { OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer, WsException } from "@nestjs/websockets";
import { Socket, Server } from 'socket.io'
import { ChatService } from "../chat.service";

@WebSocketGateway({
  cors:{
    origin: '*',
  },
  namespace: '/chat-room'
})
export class ChatWebsocketGateway  implements  OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private chatService: ChatService,
    private jwtAuthWebSocket:JwtAuthWebsocket,
  ){}
  logger = new Logger('ChatWebsocketGateway')
  
  @WebSocketServer()
  server: Server;

  countClient = 0
  clientConnected:Record<string, ClientWebSocket> = {}

  /**
   * Handleconneciton to validate token and emit user-id to client 
   */
  async handleConnection(client:Socket){
    this.logger.log(`New connection ${client.id}`)
    this.logger.log(`token: ${client.handshake.query.access_token}`)
    try{
      let user = await this.jwtAuthWebSocket.verifyWebsocket(client)
      this.clientConnected[client.id] = new ClientWebSocket(client.id, user.id)
      
      this.emitUserId(client, user.id)
      this.emitShowListRoom(client, user.id)
      
      this.countClient++;
    }catch(err){
      this.logger.error(err)
      delete this.clientConnected[client.id]
      client.disconnect(true)
    }

    this.logger.log(`Total Server connection : ${this.countClient}`)
  }

  handleDisconnect(client: any) {
    this.countClient--;
    this.logger.log(`Disconnect ${client.id}`)
    delete this.clientConnected[client.id]
    this.logger.log(`Total Server connection : ${this.countClient}`)
  }

  /**
   * After create connection, emit user-id to client
   */
  emitUserId(client:Socket, userId:string){
    client.emit('user-id', userId)
  }


  @SubscribeMessage('create-room')
  async createRoom(
    client:Socket,
    payload: {
      userId: string,
      apotekerId: string,
      apotekId: string,
    }
  ){
    let res = await this.chatService.createRoom(payload.apotekId, payload.apotekerId, [payload.userId])
    this.showDetailRoom(client, res.id)
    await this.broadcastToParticipant(res.id)
  }

  @SubscribeMessage('join-room')
  async joinRoom(
    client:Socket,
    payload: {
      userId: string,
      roomId: string,
    }
  ){
    await this.chatService.joinRoom(payload.userId, payload.roomId)
    this.showDetailRoom(client, payload.roomId)
    this.emitShowListRoom(client, payload.userId)
  }
  
  @SubscribeMessage('send-message')
  async sendMessage(
    client:Socket,
    payload: {
      message: string,
      roomId: string
      userId: string
    }
  ){
    let res = await this.chatService.sendMessage({
      message: payload.message,
      room: payload.roomId,
      user: payload.userId
    })
    this.server.emit(`new-chat/${payload.roomId}`, res)
    await this.broadcastToParticipant(payload.roomId)
  }

  @SubscribeMessage('read-room')
  async readRoom(
    client: Socket,
    payload: {
      roomId: string,
      userId: string
    }
  ){
    await this.chatService.readRoom(payload.userId, payload.roomId)
    this.emitShowListRoom(client, payload.userId)
  }

  /**
   * Or can be used refresh list room
   */
  async emitShowListRoom(client:Socket, userId: string){
    client.emit('list-room', await this.chatService.getListRoom(userId))
  }
  
  // emit detail room to client
  async showDetailRoom(client:Socket, roomId: string){
    this.logger.log('show detail room ', roomId)
    let roomData = {
      roomId: roomId,
      historyChat: await this.chatService.getRoomHistory(roomId, true)
    }
    client.emit(`detail-room`, roomData)
  }
  
  // broadcast new room to user
  async broadcastToParticipant(roomId: string){
    let participants = await this.chatService.getParticipantRoom(roomId)
    participants.forEach( async (participant) => {
      let roomParticipant = await this.chatService.getListRoom(participant.user.id)
      let userId = participant.user.id
      // bc = broadcast
      this.server.emit(`bc-list-room/${userId}`, roomParticipant)
    })
  }
}

class ClientWebSocket {
  constructor(idSocket: string, userId: string){
    this.id = idSocket
    this.userId = userId
  }
  id: string
  userId: string
}