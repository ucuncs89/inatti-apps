import { JwtAuthWebsocket } from "@app/authentication/authorization/jwt.authguard.websocket";
import { ChatRoomEntity } from "@app/database/entity/chat/chat-room.entity";
import { faker } from "@faker-js/faker";
import { Test, TestingModuleBuilder } from "@nestjs/testing"
import { ModuleMocker } from 'jest-mock';
import { Socket } from "socket.io";
import { ChatService } from "../chat.service";
import { ChatWebsocketGateway } from "./chat-websocket.gateway"

describe('Chat webscoket gateway', () =>{
  let chatWs: ChatWebsocketGateway

  let chatServiceMock = {
    getListRoom: jest.fn(()=>[]),
    getRoomHistory: jest.fn(() => ({})),
    getParticipantRoom: jest.fn(() => ({})),
    createRoom: jest.fn(() => ({})),
    joinRoom: jest.fn(() => ({})),
    sendMessage: jest.fn(() => ({})),
  }

  let jwtAuthWsMock = {
    verifyWebsocket: jest.fn((client:Socket)=>({
      id: faker.datatype.uuid(),
      username: faker.name.firstName(),
    }))
  }

  const genSocketClient = (socketId: string) => {
    return {
      id: socketId,
      handshake: {
        query: {
          access_token: faker.random.numeric()
        }
      },
      disconnect(status){},
      emit(nameEvent, msg){}
    }
  }

  beforeEach(async () => {
    let moduleChat = await Test.createTestingModule({
      providers: [ChatWebsocketGateway],
    }).useMocker((token) => {
      if(token === ChatRoomEntity){
        return {}
      }
      if(token === ChatService){
        return chatServiceMock
      }
      if(token === JwtAuthWebsocket){
        return jwtAuthWsMock
      }
    }).compile()

    chatWs = moduleChat.get(ChatWebsocketGateway);
  })

  it('should be defined', () => {
    expect(chatWs).toBeDefined();
  })

  it('Should add connection on new connection', async () => {
    const socketId = '123'
    const socketClient = {
      id: socketId,
      handshake: {
        query: {
          access_token: faker.random.numeric()
        }
      },
      disconnect(status){},
      emit(nameEvent, msg){}
    }

    const emitUserId = jest.spyOn(chatWs, 'emitUserId')
    const emitShowListRoom = jest.spyOn(chatWs, 'emitShowListRoom')  
    
    const dataUser = {
      id: faker.datatype.uuid(),
      username: faker.name.firstName(),
    }

    jwtAuthWsMock.verifyWebsocket.mockImplementation(() => dataUser)

    //@ts-ignore
    await chatWs.handleConnection(socketClient)
    expect(chatWs.countClient == 1).toBeTruthy()
    expect(chatWs.clientConnected[socketId]).not.toBeNull();
    expect(chatWs.clientConnected[socketId].id).toBe(socketId)
    expect(chatWs.clientConnected[socketId].userId).toBe(dataUser.id)
    // expect(emitUserId).toBeCalled()
    // expect(emitShowListRoom).toBeCalled()
  })

  it('Should failed if token not valid', async () => {
    const socketId = '123'
    const socketClient = genSocketClient(socketId)

    jwtAuthWsMock.verifyWebsocket.mockImplementation((client: any) => {throw new Error('not valid')})

    //@ts-ignore
    await chatWs.handleConnection(socketClient)
    expect(chatWs.countClient).toBe(0)
    expect(chatWs.clientConnected[socketId]).toBeUndefined()
  })

  it('Should remove client if disconnect', () => {
    const socketId = '123'
    chatWs.countClient = 1
    chatWs.clientConnected[socketId] = {
      id: socketId,
      userId: '123'
    }
    const socketClient = genSocketClient(socketId)

    chatWs.handleDisconnect(socketClient)
    expect(chatWs.clientConnected[socketId]).toBeUndefined()
    expect(chatWs.countClient).toBe(0)
  })

  it('Should emit user id', () => {
    const socketId = '123'
    const userId = faker.datatype.uuid()
    const clientEmit = jest.fn((eventName, msg) => {
      expect(eventName).toBe('user-id')
      expect(msg).toBe(userId)
    })
    const socketClient = {
      ...genSocketClient(socketId),
      emit: clientEmit
    }
    
    //@ts-ignore
    chatWs.emitUserId(socketClient, userId)

  })

  it('Should emit show list room', () => {
    const socketId = '123'
    const roomList = [{
      id: faker.datatype.uuid(),
      name: faker.lorem.word(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    }]

    chatServiceMock.getListRoom.mockImplementation(() => roomList)

    const clientEmit = jest.fn((eventName, msg) => {
      expect(eventName).toBe('list-room')
      expect(msg).toBe(roomList)
    })
    const socketClient = {
      ...genSocketClient(socketId),
      emit: clientEmit
    }
    
    //@ts-ignore
    chatWs.emitShowListRoom(socketClient, roomList)
  })

  it('Should emit show detail room', () =>{
    const socketId = '123'
    const roomId = faker.datatype.uuid()
    const roomHistory = [{
      id: faker.datatype.number(),
      roomId,
      message: faker.lorem.sentence(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    }]

    chatServiceMock.getRoomHistory.mockImplementation(() => roomHistory)

    const clientEmit = jest.fn((eventName, msg) => {
      expect(eventName).toBe('detail-room')
      expect(msg).toStrictEqual({
        roomId: roomId,
        historyChat: roomHistory
      })
    })
    const socketClient = {
      ...genSocketClient(socketId),
      emit: clientEmit
    }
    
    //@ts-ignore
    chatWs.showDetailRoom(socketClient, roomId)
  })

  it('Should broadcast to participant', () => {
    const userId = faker.datatype.uuid()
    const fakeRoom = [{
      id: faker.datatype.uuid(),
      name: faker.lorem.word(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    }]

    const serverEmitMock = jest.fn((event, msg) =>{
      expect(event).toBe('bc-list-room/'+userId)
      expect(msg).toStrictEqual(fakeRoom)
    })

    chatServiceMock.getParticipantRoom.mockImplementation(() => [
      {
        user: {
          id: userId
        }
      }
    ])
    chatServiceMock.getListRoom.mockImplementation(() => {
      return fakeRoom
    })

    chatWs.server = {
      //@ts-ignore
      emit: serverEmitMock
    }

    chatWs.broadcastToParticipant(fakeRoom[0].id)
  })

  it('Should create Room', async () => {
    const socketId = '123'
    const socketClient = genSocketClient(socketId)
    const testRoom = {
      id: faker.datatype.uuid(),
      name: faker.lorem.word(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    }
    chatServiceMock.createRoom.mockImplementation(() => {
      return testRoom
    })

    const payload = {
      userId: faker.datatype.uuid(),
      apotekerId: faker.datatype.uuid(),
      apotekId: faker.datatype.uuid()
    }

    // mock chatws
    chatWs.showDetailRoom = jest.fn(async (client, roomId) => {
      expect(roomId).toBe(testRoom.id)
    })
    chatWs.broadcastToParticipant = jest.fn(async (roomId) => {
      expect(roomId).toBe(testRoom.id)
    })

    //@ts-ignore
    await chatWs.createRoom(socketClient, payload)
    
    expect(chatWs.showDetailRoom).toBeCalled()
    expect(chatWs.broadcastToParticipant).toBeCalled()
  })

  it('Should join Room', () => {
    const socketId = '123'
    const socketClient = genSocketClient(socketId)
    const payload = {
      userId: faker.datatype.uuid(),
      roomId: faker.datatype.uuid()
    }

    const testRoom = {
      id: payload.roomId,
      name: faker.lorem.word(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    }

    chatServiceMock.joinRoom.mockImplementation(() => {
      return testRoom
    })

    // mock chatws
    chatWs.showDetailRoom = jest.fn(async (client, roomId) => {
      expect(roomId).toBe(testRoom.id)
    })
    chatWs.emitShowListRoom = jest.fn(async (client, userId) => {
      expect(userId).toBe(payload.userId)
    })

    //@ts-ignore
    chatWs.joinRoom(socketClient, payload)
  })

  it('Should send message', () => {
    const socketId = '123'
    const socketClient = genSocketClient(socketId)
    const payload = {
      userId: faker.datatype.uuid(),
      roomId: faker.datatype.uuid(),
      message: faker.lorem.sentence()
    }

    const resMessage = {
      id: faker.datatype.number(),
      message: payload.message,
      createdAt: faker.date.recent(),
      room: payload.roomId,
      user: payload.userId
    }

    chatServiceMock.sendMessage.mockImplementation(() => {
      return resMessage
    })

    chatWs.server = {
      //@ts-ignore
      emit: jest.fn((event, msg) => {
        expect(event).toBe('new-chat/'+payload.roomId)
        expect(msg).toStrictEqual(resMessage)
      })
    }

    //@ts-ignore
    chatWs.sendMessage(socketClient, payload)
  })
})