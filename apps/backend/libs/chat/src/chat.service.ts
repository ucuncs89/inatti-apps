import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { ChatMessageEntity } from '@app/database/entity/chat/chat-message.entity';
import { ChatParticipantEntity } from '@app/database/entity/chat/chat-participant.entity';
import { ChatRoomEntity } from '@app/database/entity/chat/chat-room.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(ChatMessageEntity)
    private ChatMessageEntityRepository: Repository<ChatMessageEntity>,
    @InjectRepository(ChatRoomEntity)
    private ChatRoomEntityRepository: Repository<ChatRoomEntity>,
    @InjectRepository(ChatParticipantEntity)
    private ChatParticipantEntityRepository: Repository<ChatParticipantEntity>,
  ){}

  async createRoom(apotek:ApotekDetail|string, apoteker:User|string, participants?: any[]){
    let room = new ChatRoomEntity();
    if(typeof apotek === 'string'){
      const apotekId = apotek
      apotek = new ApotekDetail()
      apotek.id = apotekId
    }
    if(typeof apoteker === 'string'){
      const apotekerId = apoteker
      apoteker = new User()
      apoteker.id = apotekerId
    }
    room.apotek = apotek;
    room.apoteker = apoteker;
    room = await this.ChatRoomEntityRepository.save(room);

    if (participants) {
      // add apoteker to participant
      if(!participants.includes(apoteker.id)){
        participants.push(apoteker.id)
      }

      for (const participant of participants) {
        const chatParticipant = new ChatParticipantEntity();
        chatParticipant.room = room;
        chatParticipant.user = participant;
        await this.ChatParticipantEntityRepository.save(chatParticipant);
      }
    }
    
    return room
  }

  async getParticipantRoom(roomId: string){
    return await this.ChatParticipantEntityRepository.find({
      where: {
        room: roomId
      },
      relations: [
        'user',
        'room'
      ]
    })
  }

  async sendMessage(message: {
    message: string,
    room: string,
    user: string
  }){
    let newMessage = new ChatMessageEntity()
    Object.assign(newMessage, message)
    let res = await this.ChatMessageEntityRepository.save(newMessage)

    await this.setUnreadToOtherParticipant(message.room, message.user)

    return await this.ChatMessageEntityRepository.findOne(res.id, {
      relations: [
        'user',
        'room'
      ]
    })
  }

  getRoomHistory(roomId: string, hideRoom = false){
    const relations = [
      'user'
    ]
    if(!hideRoom) relations.push('room')

    return this.ChatMessageEntityRepository.find({
      where: {
        room: roomId
      },
      relations,
    })
  }

  getListRoom(userId: string){
    return this.ChatParticipantEntityRepository.find({
      where: {
        user: userId
      },
      relations: [
        'user',
        'room',
        'room.apotek',
        'room.apoteker',
      ]
    })
  }

  async joinRoom(userId: string, roomId: string){
    let participant = await this.ChatParticipantEntityRepository.find({
      where: {
        user: new User(userId),
        room: new ChatRoomEntity(roomId)
       }
    })
    console.log(`join room : ${userId}, room id : ${roomId}`, participant.length)
    // skip if already join
    if(participant.length >= 1){
      console.log(`already join room`)
      return;
    }
    console.log('insert user to room')
    const newParticipant = new ChatParticipantEntity()
    newParticipant.user = new User(userId),
    newParticipant.room = new ChatRoomEntity(roomId)
    return await this.ChatParticipantEntityRepository.save(newParticipant)
  }

  // Read room system
  async readRoom(userId: string, roomId: string){
    let participant = await this.ChatParticipantEntityRepository.findOne({
      where: {
        user: new User(userId),
        room: new ChatRoomEntity(roomId)
       }
    })
    if(!participant){
      throw new Error(`user ${userId} not join room ${roomId}`)
    }
    let participantEntity = participant
    participantEntity.unread = 0
    return await this.ChatParticipantEntityRepository.save(participantEntity)
  }

  async setUnreadToOtherParticipant(roomId: string, userId: string){
    let participants = await this.ChatParticipantEntityRepository.find({
      where: {
        room: new ChatRoomEntity(roomId)
      },
      relations: [
        'user',
        'room'
      ]
    })
    for(let participant of participants){
      if(participant.user.id !== userId){
        participant.unread = participant.unread + 1
        await this.ChatParticipantEntityRepository.save(participant)
      }
    }
  }
}
