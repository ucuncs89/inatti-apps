import { User } from '@app/database/entity/user/user.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { Connection, getConnection } from 'typeorm';
import { ChatService } from './chat.service';
import { ChatMessageEntity } from '@app/database/entity/chat/chat-message.entity';
import { ChatRoomEntity } from '@app/database/entity/chat/chat-room.entity';
import { ChatParticipantEntity } from '@app/database/entity/chat/chat-participant.entity';
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { TypeormConfigTest } from '../../../test/test-utils/typeorm-config-test';
import { getFixture } from '../../../test/fixtures-helper/fixturues-fn';
import { cleanTable } from '../../../test/test-utils/typeorm-query';
import { loadFixture } from '../../../test/test-utils/typeorm-fixture';
import { ApotekGroupDetail } from '@app/database/entity/apotek-group/apotek-group-detail.entity';
import { faker } from '@faker-js/faker';

/**
 * Valid test data
 */

describe('Test unit chat service comunicate to database', () => {
  let service: ChatService;
  let connection: Connection;

  let userFixture = getFixture('user')
  let apotekFixture = getFixture('apotek-detail')
  let chatRoomFixture = getFixture('chat-room')

  // from apotek detail fixtures
  let apotek1 = apotekFixture.items.apotek1

  // from user fixtures
  let user1 = userFixture.items.userSahaware
  let user2 = userFixture.items.userNormal1
  let user3 = userFixture.items.userNormal2
  let testRoom = chatRoomFixture.items.room1

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ...TypeormConfigTest([
          User,
          ApotekDetail,
          ApotekGroupDetail,
          ChatMessageEntity,
          ChatRoomEntity,
          ChatParticipantEntity,
        ])
      ],
      providers: [ChatService],
    }).compile();
    
    service = module.get<ChatService>(ChatService);
    try{
      connection = getConnection()
      await loadFixture(connection)
    }catch(err){
      throw err;
    }
  });

  afterAll(async () => {
    await cleanTable(connection, [
      'apotek_detail',
      'apotek_group_detail',
      'chat_message_entity',
      'chat_participant_entity',
      'chat_room_entity',
      'users',
    ])
  })

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should success insert room and participant', async () => {
    let insertRoom = await service.createRoom(apotek1, user1, [user2, user1]);
    
    expect(insertRoom).toBeDefined();
    expect(typeof insertRoom).toBe("object")
    expect(insertRoom.id).not.toBeNull();
    expect(insertRoom.apotek.id).toBe(apotek1.id)
  })

  it('Should added participant', async () => {
    let participants = await service.getParticipantRoom(testRoom.id)
    
    expect(Array.isArray(participants)).toBeTruthy()
    expect(participants.some(item => item.user.username === user1.username)).toBeTruthy()
    expect(participants.some(item => item.user.username === user2.username)).toBeTruthy()
  })

  const sendMessage = (message: string, user:string, room:any, id?:number) => {
    let newMessage:any = {}
    newMessage.id = id
    newMessage.message = message
    newMessage.user = user
    newMessage.room = room
    return service.sendMessage(newMessage)
  }

  const isMessageSended = async (roomId:string, userId: string, expectMessage: string) => {
    let history = await service.getRoomHistory(roomId)
    return history.some(item => item.user.id === userId && item.message === expectMessage)
  }

  it('Should user1 success insert message', async () => {
    let loremMessage = faker.lorem.lines()
    let resMessage = await sendMessage(loremMessage, user1.id, testRoom)

    expect(resMessage).toBeDefined()
    expect(await isMessageSended(testRoom.id, user1.id, loremMessage)).toBeTruthy()
  })

  it('Should user2 success insert message', async () => {
    let loremMessage = faker.lorem.lines()
    let resMessage = await sendMessage(loremMessage, user2.id, testRoom)

    expect(resMessage).toBeDefined()
    expect(await isMessageSended(testRoom.id, user2.id, loremMessage)).toBeTruthy()
  })

  it('Should return history room', async () => {
    let res = await service.getRoomHistory(testRoom.id)
    expect(res).not.toBeNull()
    expect(res).toBeDefined();
    expect(Array.isArray(res)).toBeTruthy()
    expect(res.length).toBeGreaterThan(0)
  })

  it('Should return null for wrong room ', () => {
    return service.getRoomHistory(faker.datatype.uuid()).then(res => {
      expect(res.length).toBe(0)
    })
  })

  it('Should User3 success join room', async () => {
    let res = await service.joinRoom(user3.id, testRoom.id)

    expect(res.room.id).toBe(testRoom.id)
    expect(res.user.id).toBe(user3.id)
  })

  it('Should show list room from user1', async () => {
    let rooms = await service.getListRoom(user1.id)
    expect(rooms).toBeDefined()
    expect(Array.isArray(rooms)).toBeTruthy()
    expect(rooms.length).toBeGreaterThan(0)
    expect(rooms.some((item) => item.room.id === testRoom.id)).toBeTruthy()
  })

  it('Should user1 set unread for user2 and user2 read room', async () => {
    let messageList = []
    let totalUnread = 10
    // 10 dummy msg
    for (let i = 0; i < totalUnread; i++) {
      messageList.push(faker.lorem.lines())
    }

    // send message
    for (let i = 0; i < messageList.length; i++) {
      await sendMessage(messageList[i], user1.id, testRoom, i)
    }    

    let dataParticipant = await service.getParticipantRoom(testRoom.id)
    expect(dataParticipant.length).toBe(2)
    expect(dataParticipant.some(item => item.user.id === user1.id)).toBeTruthy()
    expect(dataParticipant.some(item => item.user.id === user2.id)).toBeTruthy()
    expect(dataParticipant.some(item => item.unread === totalUnread)).toBeTruthy()

    // read message
    let res = await service.readRoom(testRoom.id, user2.id)
    console.log(res)
    expect(res).toBeDefined()
    let dataParticipantAfter = await service.getParticipantRoom(testRoom.id)
    expect(dataParticipantAfter.length).toBe(2)
    expect(dataParticipantAfter.some(item => item.user.id === user1.id)).toBeTruthy()
    expect(dataParticipantAfter.some(item => item.user.id === user2.id)).toBeTruthy()
    expect(dataParticipantAfter.some(item => item.unread === 0)).toBeTruthy()
  })
});
