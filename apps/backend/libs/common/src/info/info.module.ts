import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { InfoController } from './info.controller';
import { InfoService } from './info.service';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [InfoController],
  providers: [InfoService]
})
export class InfoModule { }
