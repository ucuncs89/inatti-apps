import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { BadRequestException, Controller, Get, Param } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import { InfoService } from './info.service';

@ApiTags('Info')
@Controller('info')
export class InfoController {
  constructor(
    private infoService: InfoService
  ) { }

  /**
   * @deprecated duplicate
   */
  @RequireAuthApp()
  @ApiOperation({
    deprecated: true
  })
  @Get('/references')
  async getAllReferences() {
    return await this.infoService.getReferences();
  }

  @Get('/reference/:id')
  async getReference(
    @Param('id')
    id: string
  ) {
    return await this.infoService.getReferences(id)
  }

  @Get('/available-product')
  async getAvailableProduct() {
    try {
      return await this.infoService.getAvailableProduct()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
