import { Product } from '@app/database/entity/ref/product.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class InfoService {
  constructor(
    @InjectRepository(RefDetail)
    private referenceRepository: Repository<RefDetail>,
    @InjectRepository(Product)
    private ProductRepository: Repository<Product>,
  ){}

  async getReferences(id?: string){
    if(!id){
      return await this.referenceRepository.find();
    }
    return await this.referenceRepository.findOne({
      where: {
        id
      },
      relations: ['products', 'shifts']
    })
  }

  getAvailableProduct() {
    return this.ProductRepository.find()
  }
}
