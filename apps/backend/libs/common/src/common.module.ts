import { Module } from '@nestjs/common';
import { CommonService } from './common.service';
import { WilayahModule } from './wilayah/wilayah.module';
import { KtpModule } from './ktp/ktp.module';
import { DatabaseModule } from '@app/database';
import { InfoModule } from './info/info.module';

@Module({
  providers: [CommonService],
  exports: [CommonService, WilayahModule, KtpModule],
  imports: [
    DatabaseModule,
    WilayahModule,
    KtpModule,
    InfoModule
  ],
})
export class CommonModule { }
