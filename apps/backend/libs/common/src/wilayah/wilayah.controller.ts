import { JwtAuthGuard } from '@app/authentication/authorization/jwt.authguard';
import { Kabupaten } from '@app/database/entity/region/kabupaten.entity';
import { Kecamatan } from '@app/database/entity/region/kecamatan.entity';
import { Kelurahan } from '@app/database/entity/region/kelurahan.entity';
import { Provinsi } from '@app/database/entity/region/provinsi.entity';
import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Like, Repository } from 'typeorm';
import {
  KabupatenDTO,
  KecamatanDTO,
  KelurahanDTO,
  KelurahanQueryDTO,
} from './types';

// @ApiBearerAuth()
// @UseGuards(JwtAuthGuard)
@ApiTags('Wilayah')
@Controller('wilayah')
export class WilayahController {
  constructor(
    @InjectRepository(Provinsi)
    private provinsiRepository: Repository<Provinsi>,
    @InjectRepository(Kabupaten)
    private kabupatenRepository: Repository<Kabupaten>,
    @InjectRepository(Kecamatan)
    private kecamatanRepository: Repository<Kecamatan>,
    @InjectRepository(Kelurahan)
    private KelurahanRepository: Repository<Kelurahan>,
  ) { }

  @Get('/provinsi')
  getProvinsi() {
    return this.provinsiRepository.find();
  }

  @Get('/kabupaten')
  getKabupaten(@Query() query: KabupatenDTO) {
    return this.kabupatenRepository.find({
      where: {
        provinsi: query.provinsi_id,
      },
    });
  }

  @Get('/kecamatan')
  getKecamatn(@Query() query: KecamatanDTO) {
    return this.kecamatanRepository.find({
      where: {
        kabupaten: query.kabupaten_id,
      },
    });
  }

  @Get('/kelurahan')
  getKelurahan(@Query() query: KelurahanDTO) {
    try {
      let responseKelurahan = this.KelurahanRepository.find({
        where: {
          kecamatan: query.kecamatan_id,
        },
      });

      return responseKelurahan;
    } catch (error) {
      console.log(error);
    }
  }

  @Get('/kelurahan-query')
  getKelurahanByQuery(@Query() query: KelurahanQueryDTO) {
    try {
      if (query.search) {
        return this.KelurahanRepository.find({
          where: {
            name: ILike(`%${query.search}%`),
          },
          relations: [
            'kecamatan',
            'kecamatan.kabupaten',
            'kecamatan.kabupaten.provinsi',
          ],
          order: {
            id: 'DESC',
          },
        });
      }

      return this.KelurahanRepository.find({
        relations: [
          'kecamatan',
          'kecamatan.kabupaten',
          'kecamatan.kabupaten.provinsi',
        ],
        cache: true,
        skip: 0,
        take: 25,
        order: {
          id: 'DESC',
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  @Get('/all')
  getAllRegion(@Query('kelurahan_id') kelurahanId: string) {
    return this.KelurahanRepository.findOneOrFail({
      where: {
        id: kelurahanId,
      },
      relations: [
        'kecamatan',
        'kecamatan.kabupaten',
        'kecamatan.kabupaten.provinsi',
      ],
    });
  }
}
