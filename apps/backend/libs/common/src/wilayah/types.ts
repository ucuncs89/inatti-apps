import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class KabupatenDTO {
  @ApiProperty()
  provinsi_id: string;
}

export class KecamatanDTO {
  @ApiProperty()
  kabupaten_id: string;
}

export class KelurahanDTO {
  @ApiProperty()
  kecamatan_id: string;

  @ApiProperty()
  @ApiPropertyOptional()
  search: string;
}

export class KelurahanQueryDTO {
  @ApiProperty()
  @ApiPropertyOptional()
  search: string;
}