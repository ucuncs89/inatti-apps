import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { WilayahController } from './wilayah.controller';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [WilayahController]
})
export class WilayahModule { }
