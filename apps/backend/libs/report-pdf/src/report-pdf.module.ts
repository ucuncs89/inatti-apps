import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ReportPdfService } from './report-pdf.service';
import { LocalPdfAuthService } from './local-pdf-auth/local-pdf-auth.service';
import { DatabaseModule } from '@app/database';

@Module({
  imports: [ConfigModule.forRoot(), DatabaseModule],
  providers: [ReportPdfService, LocalPdfAuthService],
  exports: [ReportPdfService],
})
export class ReportPdfModule { }
