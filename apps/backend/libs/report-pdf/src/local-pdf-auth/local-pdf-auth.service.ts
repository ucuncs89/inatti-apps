import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';

/**
 * Validate auth from local network service pdf
 * This validation is simple, only check token with env PDFTEMPLATE_WEB_TOKEN
 * Token format : Barear [token]
 */
@Injectable()
export class LocalPdfAuthService implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    return this.validateAuthLocal(context.switchToHttp().getRequest())
  }

  validateAuthLocal(request: Request) {
    let authorization = request.header('Authorization')
    if(!authorization) return false

    const PDFTEMPLATE_WEB_TOKEN = process.env.PDFTEMPLATE_WEB_TOKEN
    let token = authorization.split(" ")[1]
    if (!token) {
      return false
    }
    return token === PDFTEMPLATE_WEB_TOKEN
  }
}
