import { LabConfig } from '@app/database/entity/lab/lab-config.entity';
import { RefConfig } from '@app/database/entity/ref/ref-config.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import * as puppeteer from 'puppeteer';
import { Repository } from 'typeorm';
import * as archiver from 'archiver'

export type TypeTestCovid = 'antigen' | 'pcr'
export type TypeSource = 'lab' | 'ref'
@Injectable()
export class ReportPdfService {
  logger: Logger
  constructor(
    @InjectRepository(LabConfig)
    private LabConfigRepository: Repository<LabConfig>,
    @InjectRepository(RefConfig)
    private RefConfigRepository: Repository<RefConfig>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
  ) {
    this.logger = new Logger('ReportPdfService')
  }

  browser:puppeteer.Browser

  async onModuleDestroy() {
    if(this.browser){
      await this.browser.close()
    }
  }

  /**
   * You can use only this function to generate pdf
   */
  async generatePdfFromUrl(url: string): Promise<Buffer> {
    this.logger.verbose(`Try download template : ${url}`)

    if(!this.browser){
      this.browser = await puppeteer.launch()
    }
    let page = await this.browser.newPage();
    this.browser.pages().then(pages => {
      this.logger.verbose(`Puppeteer total page opened : ${pages.length}`)
    })

    await page.goto(url, {
      waitUntil: 'networkidle2'
    })
    let bufferPdf: Buffer = await page.pdf({ format: 'a4', printBackground: true, })
    await page.close()
    return bufferPdf
  }

  hostPdftemplateWeb = process.env.HOST_PDFTEMPLATE_WEB || "http://localhost:5000"

  /**
   * ==============================
   * Below is function to generate pdf from url
   */

  async ResultTest(params: {
    type: TypeTestCovid,
    src: TypeSource,
    idSrc: string, // id lab or id ref, but will not be used anymore, so dont believe it
    idTemplate?: string,
    idSwab: string | number
  }) {
    const hostPdftemplateWeb = this.hostPdftemplateWeb

    let templateFile = 'default'
    if (params.idTemplate) {
      templateFile = params.idSrc + "_" + params.idTemplate
    }
    const paramUrl = [params.type, params.src, templateFile]

    let config = await this.getConfig(params.idSwab, params.type)

    const query = {
      idswab: params.idSwab,
      idsrc: params.idSrc,
      ...config
    }
    // convert query to idswab=123&idsrc=0812
    const queryUrl = Object.keys(query).map((key) => `${key}=${query[key]}`).join("&")

    // Glue all
    const url = `${hostPdftemplateWeb}/${paramUrl.join('/')}?${queryUrl}`

    this.logger.log(`Try download template : ${url}`)
    return await this.generatePdfFromUrl(url)
  }

  async InvoicePage(invoiceId){
    let url = this.hostPdftemplateWeb + '/invoice/default?inv=' + invoiceId
    return await this.generatePdfFromUrl(url)
  }

  async getConfig(swabId: any, sourceOutput: TypeTestCovid): Promise<{
    tz:string
  } | null>{
    let swabDetail = await this.SwabDetailRepository.findOne({
      where: {
        id: swabId
      },
      relations: [
        'refTarget',
        'labTarget',
      ]
    })

    if(sourceOutput === 'pcr'){
      let config = await this.LabConfigRepository.findOne({
        where: {
          lab: swabDetail.labTarget.id
        }
      })
      if(config && config.timezone){
        return {
          tz: config.timezone
        }
      }
    }
    if(sourceOutput === 'antigen'){
      let config = await this.RefConfigRepository.findOne({
        where: {
          ref: swabDetail.refTarget.id
        }
      })
      if(config && config.timezone){
        return {
          tz: config.timezone
        }
      }
    }
    return null
  }

  async ResultTestZip(params: { type: TypeTestCovid, idSrc: string, idSwab: string[] }, response: Response) {
    // For any reason, using ref template
    let src: any = 'ref'
    
    let idSrc = params.idSrc

    response.set({
      'Content-Type': 'application/zip',
      'Content-Disposition': `attachment; filename="certificate_${new Date().getTime()}.zip"`,
    });

    let archive = archiver('zip', {
      zlib: { level: 9 }
    })
    // Pipe archieve to response
    archive.pipe(response)
    
    archive.on('finish', () => {
      this.logger.verbose('finish archive')
    })

    archive.on('error', (err) => {
      this.logger.verbose(`failed to archive file`)
      this.logger.verbose(err)
      throw err
    })

    for (let i = 0; i < params.idSwab.length; i++) {
      const idSwab = params.idSwab[i];
      this.logger.verbose(`Process ke ${i}, id ${idSwab}`)
      const filePdf: Buffer = await this.ResultTest({
        type: params.type,
        src,
        idSrc,
        idSwab: idSwab,
      })
      archive.append(filePdf, {name: `${idSwab}.pdf`})
      this.logger.verbose(`Simpan ${i}, id ${idSwab}`)
    }
    archive.finalize()
  }
}
