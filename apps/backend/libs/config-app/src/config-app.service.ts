import { ConfigApp } from '@app/database/entity/config/config-app.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

/**
 * Config app global libs
 */
@Injectable()
export class ConfigAppService {
  constructor(
    @InjectRepository(ConfigApp)
    private ConfigAppRepository: Repository<ConfigApp>,
  ) { }

  /**
   * @deprecated use getConfigApp instead for full return data
   * @param name 
   * @param defaultValue 
   * @returns 
   */
  async getConfigOrInit(name: string, defaultValue?: { value?: string, category?: string, valueJson?: any }) {
    let config = await this.ConfigAppRepository.findOne({
      name
    })
    if (!config || !config.value) {
      if (defaultValue) {
        let newConfig = new ConfigApp()
        newConfig.name = name
        newConfig.value = defaultValue.value
        newConfig.category = defaultValue.category
        newConfig.valueJson = defaultValue.valueJson
        await this.ConfigAppRepository.save(newConfig)
        return defaultValue.value
      }
      return null
    }

    return config.value
  }

  async getConfigApp(name: string, defaultValue?: Partial<ConfigApp>) {
    const existing = await this.ConfigAppRepository.findOne({
      name
    })
    if (!existing) {
      const newConfig = new ConfigApp()
      Object.assign(newConfig, defaultValue, { name })
      return await this.ConfigAppRepository.save(newConfig)
    }
    return existing
  }
}
