import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { ConfigAppService } from './config-app.service';

@Module({
  providers: [ConfigAppService],
  exports: [ConfigAppService],
})
export class ConfigAppModule { }
