export namespace Sendinblue {
  export interface SendTreansacProps {
    to: {
      email?: string,
      name?: string
    }[],
    templateId?: number,
    params?: any,
    headers?: any
    htmlContent?: string
    subject?: string
    sender?: {
      name?: string,
      email?: string,
      id?: string
    }
  }

  export interface ParamTemplate1 {
    EMAIL: string
    FIRSTNAME: string
    CODE_OTP: string
  }
}

export interface EmailSendOTPInterface {
  fullname: string,
  email: string,
  otp: string,
}