import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as SibApiV3Sdk from 'sib-api-v3-sdk';
import { EmailSendOTPInterface, Sendinblue } from './type';

@Injectable()
export class EmailServiceService {
  constructor(
    private configSendinblue: ConfigService
  ) { }


  async sendEmailOtp(params: EmailSendOTPInterface) {
    // selected service
    return this.sendinblue({
      to: [
        {
          email: params.email,
          name: params.fullname
        }
      ],
      params: {
        CODE_OTP: params.otp,
        EMAIL: params.email,
        FIRSTNAME: params.fullname
      },
    })
  }

  // https://developers.sendinblue.com/docs/send-a-transactional-email
  async sendinblue(payload: Sendinblue.SendTreansacProps) {
    let defaultClient = SibApiV3Sdk.ApiClient.instance;

    // Configure API key authorization: api-key
    let apiKey = defaultClient.authentications['api-key'];
    apiKey.apiKey = this.configSendinblue.get<string>('key');

    let apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();

    if (!payload.templateId && !payload.htmlContent) {
      payload.templateId = +this.configSendinblue.get<string>('templateConfirm')
    }

    if (!payload.sender) {
      payload.sender = this.configSendinblue.get<any>('sender')
    }

    return apiInstance.sendTransacEmail(payload)
  }
}
