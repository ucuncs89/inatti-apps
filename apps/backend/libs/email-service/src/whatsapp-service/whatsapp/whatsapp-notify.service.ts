import { ConfigApp } from "@app/database/entity/config/config-app.entity";
import { RefConfig } from "@app/database/entity/ref/ref-config.entity";
import { SwabDetail } from "@app/database/entity/swab-data/swab-detail.entity";
import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ResponseWhatsappInterface } from "./type";
import { WhatsappService } from "./whatsapp.service";
import { EmailServiceService } from "@app/email-service/email-service.service";
import { ConfigAppService } from '@app/config-app/config-app.service'
import { CONFIGAPP_NAME, CONFIGAPP_CATEGORY } from '@app/config-app/type'
import { WHATSAPP_SERVICE } from './type'
import * as dayjs from 'dayjs';

interface ParamsNotify {
  swabDetail: SwabDetail;
  template: string;
}

const ConfigTemplateNotify = 'TEMPLATE_NOTIFY_WA'

@Injectable()
export class WhatsappNotifyService {
  constructor(
    private configService: ConfigService,
    private whatsappService: WhatsappService,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(ConfigApp)
    private ConfigAppRepository: Repository<ConfigApp>,
    @InjectRepository(RefConfig)
    private RefConfigRepository: Repository<RefConfig>,
    private emailService: EmailServiceService,
    private configAppService: ConfigAppService
  ) { }
  logger = new Logger('WhatsappNotifyService')

  async prepareData(idSwab: number, templateId: string): Promise<ParamsNotify | null> {
    let swabDetail = await this.SwabDetailRepository.findOne({
      where: {
        id: idSwab
      },
      relations: [
        'patient',
        'productSwab',
        'labTarget',
        'refTarget',
        'orderSwab',
        'orderTrx'
      ]
    })

    if (!swabDetail || !swabDetail.patient || !swabDetail.patient.phonenumber) {
      return null;
    }

    // Get Config Ref
    let refConfig = await this.RefConfigRepository.findOne({
      ref: swabDetail.refTarget
    })
    if (refConfig) {
      // skip if ref disable this feature
      if (refConfig.isDisableNotifyWhatsapp === true) {
        this.logger.verbose(`Ref ${swabDetail.refTarget.id} disable notify whatsapp, skip`)
        return;
      }
    }
    if (process.env.IS_DISABLE_NOTIFY || process.env.IS_DISABLE_NOTIFY === 'true') {
      this.logger.verbose(`Disable notify whatsapp by environtment IS_DISABLE_NOTIFY`)
      return;
    }

    // Get config template
    let configTemplate = await this.ConfigAppRepository.findOne({
      name: templateId
    })
    let templateNotify
    if (!configTemplate) {
      templateNotify = `Selamat {greeting}, Halo {name}, Hasil Test {typeTest} telah keluar, download di {urlDetail}.`
    } else {
      templateNotify = configTemplate.value
    }

    return {
      swabDetail,
      template: templateNotify
    }
  }

  async notifyPatient(
    idSwab: number,
    templateId: string = ConfigTemplateNotify,
  ): Promise<ResponseWhatsappInterface> {
    let params: ParamsNotify | null = await this.prepareData(
      idSwab,
      templateId,
    );

    if (!params) return null;

    this.logger.verbose(`Run notify patient : ${JSON.stringify(params)}`);
    let now = new Date();
    let hour = now.getHours();
    let greeting = 'Pagi';
    if (hour < 23) greeting = 'Malam';
    if (hour < 18) greeting = 'Sore';
    if (hour < 15) greeting = 'Siang';
    if (hour < 10) greeting = 'Pagi';

    let urlUser =
      this.configService.get<string>('userUrl') || 'https://app.inatti.id';

    let urlPdf = `${urlUser}/download-pdf/${params.swabDetail.orderSwab.id}`;

    // Params that will replace string in template
    let paramTemplate = {
      greeting: greeting,
      name: params.swabDetail.patient.name,
      typeTest: params.swabDetail.productSwab.name,
      urlDetail: urlPdf,
    };

    for (const key in paramTemplate) {
      if (Object.prototype.hasOwnProperty.call(paramTemplate, key)) {
        const element = paramTemplate[key];
        params.template = params.template.replace(`{${key}}`, element);
      }
    }

    this.logger.verbose(`template notify : ${params.template}`);

    // For now, only support taptalk, because taptalk have return ID Message
    return this.whatsappService.serviceTaptalk({
      phone: params.swabDetail.patient.phonenumber,
      message: params.template,
    });
  }

  async notifyOrder(idSwab: number, templateId: string = ConfigTemplateNotify) {
    let params: ParamsNotify | null = await this.prepareData(
      idSwab,
      templateId,
    );

    if (!params) {
      console.log('id swab not found or data not found, ', params)
      return null
    };

    let urlUser =
      this.configService.get<string>('userUrl') || 'https://app.inatti.id';

    this.logger.verbose(`Run notify Order : ${JSON.stringify(params)}`);

    let urlTrx = `${urlUser}/testcovid/order/detail/${params.swabDetail.orderTrx.id}`;

    let paramTemplate = {
      name: params.swabDetail.patient.name,
      nik: params.swabDetail.patient.nik,
      dateTest: dayjs(params.swabDetail.requestSwabDate).format('DD-MM-YYYY'),
      nameFaskes: params.swabDetail.refTarget.name,
      trxId: params.swabDetail.orderTrx.trxId,
      urlTrx,
    };

    for (const key in paramTemplate) {
      if (Object.prototype.hasOwnProperty.call(paramTemplate, key)) {
        const element = paramTemplate[key];
        params.template = params.template.replace(`{${key}}`, element);
      }
    }

    this.logger.verbose(`template notify : ${params.template}`);

    let configWaService = await this.configAppService.getConfigOrInit(
      CONFIGAPP_NAME.WA_SERVICE_SEND_ORDER_ID,
      {
        value: WHATSAPP_SERVICE.Taptalk,
        category: CONFIGAPP_CATEGORY.WhatsappService
      }
    )

    let response = await this.whatsappService.sendWhatsappChoose(configWaService, {
      phone: params.swabDetail.patient.phonenumber,
      message: params.template,
    });

    this.logger.verbose('Response service whatsapp : ', response)

    if (params.swabDetail.patient.email) {
      // without await asyncronous, let the service run without interupe other
      await this.emailService.sendinblue({
        to: [{
          name: params.swabDetail.patient.name,
          email: params.swabDetail.patient.email,
        }],
        params: {
          NAMA: paramTemplate.name,
          NIK: paramTemplate.nik,
          DATETEST: paramTemplate.dateTest,
          NAMEFASKES: paramTemplate.nameFaskes,
          URLTRX: paramTemplate.urlTrx,
          TRXID: paramTemplate.trxId,
        },
        templateId: 2,
      })
    }
  }

  async checkStatusWhatsapp(messageId: any) {
    return await this.whatsappService.serviceTaptalkCheckStatus(messageId)
  }
}
