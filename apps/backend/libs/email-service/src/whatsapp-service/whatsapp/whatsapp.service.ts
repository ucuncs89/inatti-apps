import { ConfigAppService, CONFIGAPP_CATEGORY, CONFIGAPP_NAME } from '@app/config-app';
import { StatusNotifWA } from '@app/database/entity/swab-notification/swab-notify-wa.entity';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import got, { Got, Response } from 'got/dist/source';
import { ResponseWhatsappInterface, TaptalkSendMessageResponse, WhatsappSendInterface, WHATSAPP_SERVICE } from './type';

@Injectable()
export class WhatsappService {
  constructor(
    private configService: ConfigService,
    private configAppService: ConfigAppService
  ) {
    this.initInstanceTaptalkGot()
  }

  logger = new Logger('WhatsappService')

  sendWhatsappChoose(name: string, params: WhatsappSendInterface) {
    switch (name) {
      case WHATSAPP_SERVICE.Nusagateway:
        return this.serviceNusagateway(params)
      case WHATSAPP_SERVICE.Taptalk:
        return this.serviceTaptalk(params)
      default:
        return this.sendWhatsapp(params)
    }
  }

  async sendWhatsapp(params: WhatsappSendInterface) {
    // current active service
    return this.serviceTaptalk(params)
  }

  instanceGotTaptalk: Got

  initInstanceTaptalkGot() {
    const baseUrl = this.configService.get('taptalk.baseUrl')
    const key = this.configService.get('taptalk.key')
    this.instanceGotTaptalk = got.extend({
      prefixUrl: baseUrl,
      headers: {
        'API-key': key
      },
    })
  }

  async serviceTaptalk(params: WhatsappSendInterface): Promise<ResponseWhatsappInterface> {
    const checkError = (response: any) => {
      if (!response) {
        throw new Error('Response is empty')
      }
      if (response.status != 200) {
        throw response.error.message
      }
      if (!response.data) throw "Response data is empty"
      if (response.data.error) throw response.data.message
      return response
    }

    try {
      this.logger.verbose(params)
      let result: TaptalkSendMessageResponse = await this.instanceGotTaptalk.post(`api/v1/message/send_whatsapp`, {
        json: {
          phone: this.fixPhoneNumber(params.phone),
          messageType: 'otp',
          body: params.message
        },
      }).json()

      checkError(result)

      let status: StatusNotifWA
      status = StatusNotifWA.PROCESS
      if (result.data.message.includes("queue")) {
        status = StatusNotifWA.QUEUE
      }

      return {
        messageId: result.data.id,
        status,
        responseMessage: result.data.message
      }

    } catch (err) {
      throw err;
    }
  }

  async serviceTaptalkCheckStatus(messageId: any): Promise<ResponseWhatsappInterface> {
    let result: TaptalkSendMessageResponse<{
      status: string,
      isPending: boolean,
      isSent: boolean,
      sentTime: number,
      currency: string,
      price: number,
      createdTime: number,
    }> = await this.instanceGotTaptalk.post('api/v1/message/get_status', {
      json: {
        id: messageId
      }
    }).json()

    if (!result.data.isSent || result.data.isPending) {
      return {
        messageId: messageId,
        responseMessage: result.data.status,
        status: StatusNotifWA.FAILED
      }
    }
    return {
      messageId: messageId,
      responseMessage: result.data.status,
      status: StatusNotifWA.SENT
    }
  }

  serviceNusagateway(params: WhatsappSendInterface) {
    const baseUrl = this.configService.get('nusagateway.baseUrl')
    const key = this.configService.get('nusagateway.key')
    const form = {
      token: key,
      phone: this.fixPhoneNumber(params.phone),
      message: params.message
    }
    return got.post(baseUrl + '/api/send-message.php', {
      form,
    }).json()
  }

  async getService() {
    return await this.configAppService.getConfigOrInit(
      CONFIGAPP_NAME.WA_SERVICE_SEND_ORDER_ID,
      {
        value: WHATSAPP_SERVICE.Taptalk,
        category: CONFIGAPP_CATEGORY.WhatsappService
      }
    )
  }

  private fixPhoneNumber(phone: string) {
    if (phone.startsWith('8')) {
      return "62" + phone
    }
    return phone
  }
}
