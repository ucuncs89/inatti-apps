import { StatusNotifWA } from "@app/database/entity/swab-notification/swab-notify-wa.entity";

export interface WhatsappSendInterface {
  phone: string,
  message: string
}

export interface ResponseWhatsappInterface {
  status: StatusNotifWA
  responseMessage?: string,
  messageId?: string
}


// Taptalk
export interface TapTalkError {
  code: string;
  message: string;
  field: string;
}

export interface TaptalkData {
  success: boolean;
  message: string;
  reason: string;
  id: string;
}

export interface TaptalkSendMessageResponse<DATA = TaptalkData> {
  status: number;
  error: TapTalkError;
  data: DATA;
}

export const WHATSAPP_SERVICE = {
  Taptalk: 'taptalk',
  Nusagateway: 'nusagateway'
}