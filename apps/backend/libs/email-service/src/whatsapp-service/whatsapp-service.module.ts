import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { WhatsappService } from './whatsapp/whatsapp.service';
import WhatsappConfig from 'config/whatsapp.config';
import ProjectConfig from 'config/project.config';
import { WhatsappNotifyService } from './whatsapp/whatsapp-notify.service';
import { DatabaseModule } from '@app/database';
import { EmailServiceModule } from '../email-service.module';
import { ConfigAppModule } from '@app/config-app';

@Module({
  imports: [
    ConfigModule.forFeature(WhatsappConfig),
    ConfigModule.forFeature(ProjectConfig),
    DatabaseModule,
    EmailServiceModule,
    ConfigAppModule,
  ],
  providers: [WhatsappService, WhatsappNotifyService],
  exports: [WhatsappService, WhatsappNotifyService]
})
export class WhatsappServiceModule {}
