import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EmailServiceService } from './email-service.service';
import ConfigSendinblue from 'config/sendinblue.config';

@Module({
  imports: [
    ConfigModule.forFeature(ConfigSendinblue),
  ],
  providers: [EmailServiceService],
  exports: [EmailServiceService],
})
export class EmailServiceModule {}
