import got, { Response } from "got/dist/source";

export function FaspayGot(){
  return got.extend({
    prefixUrl: process.env.PAYMENT_FASPAY || 'https://debit-sandbox.faspay.co.id',
    hooks: {
      beforeError: [
        (err) => {
          return err;
        }
      ],
    }
  })
}