import { CryptoMd5, CryptoSha1 } from '@app/helper/utils/crypto';
import { Logger } from '@nestjs/common';
import { Got } from 'got/dist/source';
import {
  FasPayRequestTrxDTO,
  SuccessCancelTransactionDTO,
  SuccessCheckTransactionDTO,
  SuccessCreateTrxDTO,
} from '.';
import { FaspayGot } from './got-instance';
import { FasPayRequestTrx } from './type';

export class Faspay {
  private logger = new Logger('Payment/Faspay');

  private gotInstance: Got;
  private get apiFaspay() {
    if (!this.gotInstance) {
      this.gotInstance = FaspayGot();
    }
    return this.gotInstance;
  }

  static genSignature(billNo?: string) {
    const userId = process.env.PAYMENT_FASPAY_USERID;
    const password = process.env.PAYMENT_FASPAY_PASSWORD;
    if (!userId || !password) {
      throw new Error(`Payment faspay user id and password are not defined`);
    }
    let signature = `${userId}${password}${billNo || ''}`;

    return CryptoSha1(CryptoMd5(signature));
  }

  async createTransaction(
    request: FasPayRequestTrxDTO,
  ): Promise<SuccessCreateTrxDTO> {
    const payload = new FasPayRequestTrx();
    payload.request = 'Post Data Transaction';
    payload.signature = Faspay.genSignature(request.bill_no);
    // faspay require this
    Object.assign(payload, request);
    payload.bill_total = payload.bill_total + "00"
    payload.item = payload.item.map((v) => ({...v, amount: v.amount + "00"}))

    this.logger.verbose(`Payload send : ${JSON.stringify(payload)}`);

    try {
      this.logger.verbose('Request Faspay to create tranasction');
      let result: SuccessCreateTrxDTO = await this.apiFaspay
        .post('cvr/300011/10', {
          json: payload,
        })
        .json();

      this.logger.verbose(`Success request : ${JSON.stringify(result)}`);

      return result;
    } catch (err) {
      this.logger.error(`Error request create Trx : ${err.response}`);
      throw err;
    }
  }

  async getPaymentChannel(merchantId: string) {
    const payload = {
      request: 'Request List of Payment Gateway',
      merchant_id: merchantId,
      signature: Faspay.genSignature(),
    };

    return this.apiFaspay
      .post('cvr/100001/10', {
        json: payload,
      })
      .json();
  }

  async cancelPayment(
    merchantId: string,
    trx_id: string,
    bill_no: string,
  ): Promise<SuccessCancelTransactionDTO> {
    try {
      const payload = {
        request: 'Canceling Payment',
        trx_id: trx_id,
        merchant_id: merchantId,
        merchant: 'MTAC Farmasi Unpad',
        bill_no: bill_no,
        payment_cancel: 'Canceling Payment',
        signature: Faspay.genSignature(bill_no),
      };

      const response: SuccessCancelTransactionDTO = await this.apiFaspay
        .post(`cvr/100005/10`, {
          json: payload,
        })
        .json();

      return response;
    } catch (error) {
      this.logger.error(`Error cancel transaction Trx: ${error.response}`);
      throw error;
    }
  }

  async checkPayments(
    merchand_id: string,
    trx_id: string,
    bill_no: string,
  ): Promise<SuccessCheckTransactionDTO> {
    try {
      const payload = {
        request: 'Inquiry Status Payment',
        trx_id: trx_id,
        merchant_id: merchand_id,
        bill_no: bill_no,
        signature: Faspay.genSignature(bill_no),
      };

      const response: SuccessCheckTransactionDTO = await this.apiFaspay
        .post(`cvr/100004/10`, {
          json: payload,
        })
        .json();

      return response;
    } catch (error) {
      console.log(error);
      this.logger.error(`Error check transaction Trx: ${error.response}`);
      throw error;
    }
  }
}
