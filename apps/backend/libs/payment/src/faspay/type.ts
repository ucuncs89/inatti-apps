export class FaspayProductItem {
  product: string;
  qty: string;
  amount: string;
  payment_plan = '1';
  merchant_id: string;
  tenor = '00';
}

export class FasPayRequestTrxDTO {
  merchant_id: string;
  merchant: string;
  bill_no: string;
  bill_date: string;
  bill_expired: string;
  bill_desc: string;
  bill_total: string;
  cust_no: string;
  cust_name: string;
  payment_channel: string;
  msisdn: string;
  email: string;
  item: FaspayProductItem[];
}

export class FasPayRequestTrxOptional extends FasPayRequestTrxDTO {
  bill_reff?: string;
  bill_gross?: string;
  bill_miscfee?: string;
  bank_userid?: string;
  billing_name?: string;
  billing_lastname?: string;
  billing_address?: string;
  billing_address_city?: string;
  billing_address_region?: string;
  billing_address_state?: string;
  billing_address_poscode?: string;
  billing_msisdn?: string;
  billing_address_country_code?: string;
  receiver_name_for_shipping?: string;
  shipping_lastname?: string;
  shipping_address?: string;
  shipping_address_city?: string;
  shipping_address_region?: string;
  shipping_address_state?: string;
  shipping_address_poscode?: string;
  shipping_msisdn?: string;
  shipping_address_country_code?: string;
  reserve1?: string;
  reserve2?: string;
}

// https://docs.faspay.co.id/merchant-integration/api-reference-1/debit-transaction/post-data-transaction
export class FasPayRequestTrx extends FasPayRequestTrxOptional {
  request: string;
  bill_currency = 'IDR';
  pay_type = '1';
  terminal = 10;
  signature: string;
}
export interface BillItem {
  payment_plan: string;
  tenor: string;
  product: any[];
  amount: string;
  qty: string;
  merchant_id: string;
}

export interface SuccessCreateTrxDTO {
  response: string;
  trx_id: string;
  merchant_id: string;
  merchant: string;
  bill_no: string;
  bill_items: BillItem[];
  response_code: string;
  response_desc: string;
  redirect_url: string;
}

export interface SuccessCancelTransactionDTO {
  response: string;
  trx_id: string;
  merchant_id: string;
  merchant: string;
  bill_no: string;
  trx_status_code: string;
  trx_status_desc: string;
  payment_status_code: string;
  payment_status_desc: string;
  payment_cancel_date: string;
  payment_cancel: string;
  response_code: string;
  response_desc: string;
}

export interface SuccessCheckTransactionDTO {
  response: string;
  trx_id: string;
  merchant_id: string;
  merchant: string;
  bill_no: string;
  payment_reff: string;
  payment_date: string;
  payment_status_code: string;
  payment_status_desc: string;
  response_code: string;
  response_desc: string;
}
