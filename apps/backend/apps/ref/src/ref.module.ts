import { AuthenticationModule } from '@app/authentication';
import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { RefController } from './ref.controller';
import { RefService } from './ref.service';
import { PassportModule } from '@nestjs/passport';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { AccountModule } from 'libs/account/src';
import { RefPatientSwabController } from './patient/patient-swab/ref-patient-swab.controller';
import { PatientController } from './patient/patient/patient.controller';
import { ApiExternalController } from './api-external/api-external.controller';
import { PatientSwabService } from './patient-swab/patient-swab.service';
import { PatientService } from './patient-swab/patient.service';
import { UserManageController } from './user-manage/user-manage.controller';
import { UserManageService } from './user-manage/user-manage.service';
import { ReportPdfController } from './report-pdf/report-pdf.controller';
import { RefReportService } from './report-pdf/ref-report.service';
import { ReportPdfModule } from '@app/report-pdf';
import { ApiExternalService } from './api-external/api-external.service';
import { RefDetailController } from './settings/ref/ref-detail.controller';
import { RefDetailService } from './settings/ref/ref-detail.service';
import { RefProductController } from './settings/products/ref-product.controller';
import { RefProductService } from './settings/products/ref-product.service';
import { RefShiftController } from './settings/shift/ref-shift.controller';
import { RefShiftService } from './settings/shift/ref-shift.service';
import { TransactionController } from './transaction/transaction.controller';
import { TransactionService } from './transaction/transaction.service';
import { AwsModule } from '@app/aws';
import AwsConfig from "../../../config/aws.config";
import { RefPatientSwabBulkController } from './patient/patient-swab/bulk-action.controller';
import { DashboardController } from './dashboard/dashboard.controller';
import { DashboardService } from './dashboard/dashboard.service';
import { RefSettingTemplate } from './settings/template/ref-template.controller';
import { RefSettingTemplateService } from './settings/template/ref-template.service';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { NarAntigenIntegration } from './kemenkes-nar/nar-antigen-integration.service';
import { NarWebsocketGateway } from './kemenkes-nar/nar-ws-antigen.gateway';
import { DashboardProductService } from './dashboard-product/dashboard-product.service';
import { DashboardProductController } from './dashboard-product/dashboard-product.controller';
import { QuickFormService } from './quick-form/quick-form.service';
import { QuickFormController } from './quick-form/quick-form.controller';
import { NotificationSwabService } from './notification-swab/notification-swab.service';
import { NotificationSwabController } from './notification-swab/notification-swab.controller';
import { LoggerActivityModule } from '@app/logger-activity';
import { VaksinModule } from 'apps/data/src/vaksin/vaksin.module';
import { PatientModule } from 'apps/data/src/patient/patient.module';

@Module({
  imports: [
    AwsModule.register({
      bucket: AwsConfig().aws.bucket,
      uploadFolder: 'ref',
    }),
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.Ref }),
    PassportModule,
    AccountModule,
    ReportPdfModule,
    WhatsappServiceModule,
    LoggerActivityModule.register({
      area: 'ref'
    }),
    VaksinModule,
    PatientModule,
  ],
  controllers: [
    RefController,
    RefPatientSwabController,
    PatientController,
    ApiExternalController,
    UserManageController,
    ReportPdfController,
    RefDetailController,
    RefProductController,
    RefShiftController,
    TransactionController,
    RefPatientSwabBulkController,
    DashboardController,
    RefSettingTemplate,
    DashboardProductController,
    QuickFormController,
    NotificationSwabController,
  ],
  providers: [
    RefService,
    PatientSwabService,
    PatientService,
    UserManageService,
    RefReportService,
    ApiExternalService,
    RefDetailService,
    RefProductService,
    RefShiftService,
    TransactionService,
    DashboardService,
    RefSettingTemplateService,
    NarAntigenIntegration,
    NarWebsocketGateway,
    DashboardProductService,
    QuickFormService,
    NotificationSwabService,
  ],
  exports: [RefService],
})
export class RefModule { }
