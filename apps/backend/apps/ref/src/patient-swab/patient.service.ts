import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, FindConditions, ILike, MoreThanOrEqual, ObjectLiteral, Repository } from 'typeorm';
import { PatientDTO, QuerySearchPatient } from './type';
import { PatientService as PatientModuleService } from 'apps/data/src/patient/patient.service';
/**
 * WARNING !!!
 * This file is old version of API patient. to new API move to API patient module at 
 * apps/data/src/patient/patient.module.ts
 * 
 * This only used for ref project
 */

@Injectable()
export class PatientService {
  constructor(
    @InjectRepository(SwabPatient)
    private patientSwabRepository: Repository<SwabPatient>,
    private patientModuleService: PatientModuleService
  ) { }

  async create(patientData: PatientDTO) {
    return this.patientModuleService.create(patientData)
  }

  async findOne(patientId: string) {
    let res = await this.patientSwabRepository.findOne({
      where: {
        id: patientId,
      },
      relations: [
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'domisiliProvinsi',
        'domisiliKabupaten',
        'domisiliKecamatan',
        'domisiliKelurahan',
        'ownedBy',
      ],
      withDeleted: true
    })
    if (res.ownedBy) {
      res.ownedBy.hideCredential()
    }
    return res;
  }

  async find(patientId: string, refId?: string) {
    let res = await this.patientSwabRepository.findOne({
      where: {
        id: patientId,
      },
      relations: [
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'domisiliProvinsi',
        'domisiliKabupaten',
        'domisiliKecamatan',
        'domisiliKelurahan',
        'ownedBy',
        'detailSwab',
        'detailSwab.productSwab',
        'detailSwab.refTarget',
      ],
      withDeleted: true
    })
    if (res.ownedBy) {
      res.ownedBy.hideCredential()
    }
    // filter based on refid manually. dont do this at home
    if (res.detailSwab && Array.isArray(res.detailSwab) && refId) {
      res.detailSwab = res.detailSwab.filter((v) => v.refTarget.id === refId)
    }
    return res;
  }

  /**
   * Find all patient, this action not require ref id
   * because all patient became 1 database.
   */
  async findAll(paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    let where: FindConditions<SwabPatient>[] | FindConditions<SwabPatient> | ObjectLiteral | string = {}

    if (filter) {
      if (filter.date) {
        // assume it's a 'today' param
        if (filter.date && filter.date.value) {
          where.createdAt = filter.date
        }
      }

      if (filter.rangedate) {
        where.createdAt = filter.rangedate
      }

      if (filter.search) {
        where = [
          {
            name: ILike(`%${filter.search}%`)
          },
          {
            nik: ILike(`%${filter.search}%`)
          }
        ]
      }
    }

    let results = await this.patientSwabRepository.find({
      take: paginate.take,
      skip: paginate.skip,
      order: {
        createdAt: 'DESC'
      },
      where,
    })

    const total = await this.patientSwabRepository.count({ where })

    return new Pagination({
      results,
      total,
    }, paginate)
  }

  async edit(patientId: string, patientData: PatientDTO) {
    let patient = await this.patientSwabRepository.findOne({
      id: patientId
    })
    if (!patient) throw "Patient not found"
    // Object.keys(patientData).forEach(key => {
    //   if(patientData[key] && typeof patientData[key] === 'object') {
    //     patientData[key] = patientData[key].id
    //   }
    // })

    Object.assign(patient, patientData)
    return this.patientSwabRepository.save(patient)
  }

  delete(patientId: string) {
    return this.patientSwabRepository.delete(patientId);
  }

  search(option: QuerySearchPatient) {
    let repository = this.patientSwabRepository
      .createQueryBuilder('patient')
    if (option.query) {
      repository.orWhere('nik ilike :q', {
        q: `%${option.query}%`
      })
        .orWhere('name ilike :q', {
          q: `%${option.query}%`
        });
    } else if (option.id) {
      repository.where('id = :id', { id: option.id })
    }
    return repository.getMany()
  }

  async assignToUser(patientId: string, targetUser: string) {
    let patient = await this.find(patientId)
    let user = new User(targetUser)
    patient.ownedBy = user
    return await this.patientSwabRepository.save(patient)
  }
}
