import { MethodPaymentEnum, ResultSwabEnum, StatusOrderEnum } from "@app/database/entity/swab-data/type";
import { ApiProperty } from "@nestjs/swagger";
import { PatientDTO as PatientDTOFromData } from '@inatti/shared/dtos'

export class PatientDTO extends PatientDTOFromData { }

export class PatientSwabDTO {
  @ApiProperty()
  patientId: string;

  @ApiProperty({ description: "Product swab id" })
  productId: string;

  @ApiProperty({ description: "Target Reference id" })
  referenceTargetId: string;

  @ApiProperty({ description: "Id shift reference" })
  shiftId: string;

  @ApiProperty({ description: 'Price product, if null will set default price from product' })
  price?: number;

  @ApiProperty({ description: "Method payment", enum: MethodPaymentEnum })
  methodPayment?: MethodPaymentEnum;

  @ApiProperty({ enum: StatusOrderEnum })
  statusOrder?: StatusOrderEnum;

  @ApiProperty({ description: "id user customer" })
  customerId?: string;
}

export class UpdateSwabDateDTO {
  @ApiProperty({
    required: false,
    description: "swab date, null will be set current time",
    example: new Date()
  })
  swabDate?: Date;
}

export class UpdateSwabResultDTO {
  @ApiProperty({
    enum: ResultSwabEnum,
    example: ResultSwabEnum.negatif,
  })
  resultSwab?: ResultSwabEnum;

  @ApiProperty({
    description: "Id user swab. null will set current user",
    required: null
  })
  swabBy?: string;
}

export class SwabToLabDTO {
  @ApiProperty({ description: "Lab id(uuid)" })
  labId: string;
}

export class OrderUpdateDTO {
  @ApiProperty()
  price?: number;

  @ApiProperty({ enum: MethodPaymentEnum })
  method?: MethodPaymentEnum;

  @ApiProperty()
  methodCash?: number;

  @ApiProperty({ enum: StatusOrderEnum })
  status?: StatusOrderEnum
}

export class QuerySearchPatient {
  @ApiProperty()
  query: string
  @ApiProperty()
  id: string;
}

export class ListResultBulkDto {
  @ApiProperty()
  listSample: string[]
  @ApiProperty({
    enum: ResultSwabEnum
  })
  result: ResultSwabEnum
}

export class SendSwabToLabBulkDto {
  @ApiProperty()
  listSample: string[]
  @ApiProperty()
  labTarget: string
}