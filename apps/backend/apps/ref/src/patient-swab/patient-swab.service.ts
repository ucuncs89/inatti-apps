import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { SwabDetail, IdSampleGenerator } from '@app/database/entity/swab-data/swab-detail.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { StatusOrderEnum, StatusSwabEnum } from '@app/database/entity/swab-data/type';
import { RefProduct } from '@app/database/entity/ref/ref-product.entity';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, HttpException, Injectable, Logger, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, FindConditions, getManager, ILike, In, MoreThanOrEqual, ObjectLiteral, QueryFailedError, Raw, Repository } from 'typeorm';
import { OrderUpdateDTO, PatientSwabDTO, UpdateSwabResultDTO } from './type';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { genInvoiceId, genTransactionId } from '@app/helper/utils/invoice';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefShift } from '@app/database/entity/ref/ref-shift.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { WhatsappNotifyService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp-notify.service';
import { RefConfig } from '@app/database/entity/ref/ref-config.entity';
import { TransactionService } from '../transaction/transaction.service';

@Injectable()
export class PatientSwabService {
  constructor(
    @InjectRepository(SwabPatient)
    private patientSwabRepository: Repository<SwabPatient>,
    @InjectRepository(SwabDetail)
    private detailSwabRepository: Repository<SwabDetail>,
    @InjectRepository(SwabOrder)
    private orderSwabRepository: Repository<SwabOrder>,
    @InjectRepository(RefProduct)
    private refProductRepository: Repository<RefProduct>,
    @InjectRepository(RefConfig)
    private RefConfigRepository: Repository<RefConfig>,
    private whatsappNotifyService: WhatsappNotifyService,
    private transactionService: TransactionService
  ) { }

  logger = new Logger('PatientSwabService')

  private swabIdSelected: string = null;

  // Property user login, should be an id
  idUserCurrent: any;
  referenceId: string;

  async findPatient(idswab: string) {
    return await this.detailSwabRepository.findOne({
      where: {
        id: idswab
      },
      relations: [
        'productSwab',
        'patient',
        'refTarget',
        'refShiftSchedule',
        'labTarget',
        'orderSwab',
        'orderSwab.product',
        'orderSwab.methodCash',
        'orderSwab.orderTrx'
      ],
      withDeleted: true,
    })
  }

  async findAllPatient(paginate: PaginationOptionsInterface, filter: CommonFilterDTO, idProduct: number, options?: { statusFilter:string, paymentStatusFilter:string }) {
    let whereOption: FindConditions<SwabDetail>[] | FindConditions<SwabDetail> | ObjectLiteral | string = {
      refTarget: {
        id: this.referenceId
      },
      productSwab: {
        id: idProduct
      },
    }

    if(options){
      const { statusFilter, paymentStatusFilter } = options;
      if(statusFilter){
        if(statusFilter.split(',').length > 0){
          const listFilter = statusFilter.split(',').map((v) => v)
          whereOption.status = In(listFilter)
        } else {
          whereOption.status = statusFilter
        }
      }
      if(paymentStatusFilter){
        if(paymentStatusFilter.split(',').length > 0){
          const listFilter = paymentStatusFilter.split(',').map((v) => v)
          whereOption.orderSwab = {
            status: In(listFilter)
          }
        } else {
          whereOption.orderSwab = {
            status: paymentStatusFilter
          }
        }
      }
    }

    if(filter){
      if(filter.date){
        whereOption.createdAt = filter.date
      }
      if(filter.rangedate){
        whereOption.createdAt = filter.rangedate
      }
      if(filter.search){
        whereOption = [
          {
            ...whereOption,
            patient: {
              nik: ILike(`%${filter.search}%`)
            }
          },
          {
            ...whereOption,
            patient: {
              name: ILike(`%${filter.search}%`)
            }
          },
          {
            ...whereOption,
            sampleId: ILike(`%${filter.search}%`)
          }
        ]
      }
    }

    const relations = [
      'patient',
      'orderSwab',
      'narSample',
      'notifyWhataspp',
      'refTarget'
    ]

    let results = await this.detailSwabRepository.find({
      take: paginate.take,
      skip: paginate.skip,
      relations,
      order: {
        createdAt: 'DESC'
      },
      where: whereOption,
    })

    const total = await this.detailSwabRepository.count({
      where: whereOption,
      relations,
    })

    return new Pagination({
      results,
      total,
    }, paginate)
  }

  // Sorry if this code is messup
  async registerPatient(patient: PatientSwabDTO, loggedInUser: User): Promise<SwabDetail> {
    try {
      let swabDataPatient;
      // use transaction to make sure all data inserted
      await getManager().transaction(async transactionEntityManager => {
        let patientData: SwabPatient = new SwabPatient(patient.patientId)

        const productRef = await this.getProduct(patient)

        let swabData: SwabDetail = new SwabDetail()

        swabData.patient = patientData;
        swabData.refTarget = new RefDetail(patient.referenceTargetId);
        swabData.productSwab = productRef.product
        swabData.refShiftSchedule = new RefShift(+patient.shiftId)
        swabData = await transactionEntityManager.save(swabData);
        
        // save to log
        swabDataPatient = swabData

        let orderData: SwabOrder = new SwabOrder()
        orderData.detailSwab = swabData
        orderData.method = patient.methodPayment
        orderData.status = patient.statusOrder ?? StatusOrderEnum.new // default
        orderData.reference = swabData.refTarget
        orderData.price = productRef.price
        orderData.customer = patient.customerId ? new User(patient.customerId) : new User(loggedInUser.id)
        orderData.product = new RefProduct(+patient.productId)
        orderData = await transactionEntityManager.save(orderData)

        swabData.orderSwab = orderData
        await transactionEntityManager.save(swabData)

        const trxId = genTransactionId();
        const invId = genInvoiceId(trxId, orderData.product.id);
        let orderTrx = new SwabOrderTrx();
        orderTrx.invId = invId;
        orderTrx.trxId = trxId;
        orderTrx.customer = orderData.customer
        orderTrx.reference = orderData.reference
        orderTrx.product = productRef
        orderTrx.method = orderData.method
        orderTrx.status  = orderData.status
        orderTrx.totalOrder = 1 // only 1
        orderTrx.totalPrice = productRef.price * 1 // because only 1

        this.logger.verbose(JSON.stringify(orderTrx))
        
        orderTrx = await transactionEntityManager.save(orderTrx)

        orderData.orderTrx = orderTrx
         
        return await transactionEntityManager.save(orderData)
      })
      return swabDataPatient;
    } catch (error) {
      throw this.checkError(error)
    }
  }

  async getProduct(patient: PatientSwabDTO): Promise<RefProduct> {
    return this.refProductRepository.findOne({
      where: {
        ref: patient.referenceTargetId,
        id: patient.productId
      },
      relations: [
        'product'
      ]
    })
  }

  async getPatient(patientId: string): Promise<SwabPatient> {
    let patient: SwabPatient = await this.patientSwabRepository.findOne({ id: patientId });
    if (!patient) {
      throw new SwabNotFoundException(patientId)
    }
    return patient;
  }

  /**
   * Reusable detail swab
   * 
   * @param swabId 
   * @param initMode Init mode, will create new instance if `true`, if else will trhow error not found
   * @returns 
   */
  async getDetailSwab(swabId: string, initMode = false): Promise<SwabDetail> {
    let detailSwab: SwabDetail = await this.detailSwabRepository.findOne({
      where: { id: swabId },
      relations: [
        'refTarget',
        'labTarget',
        'productSwab'
      ]
    })
    if (!detailSwab) {
      if (initMode) {
        detailSwab = new SwabDetail();
      } else {
        throw new SwabNotFoundException(this.swabIdSelected);
      }
    }
    return detailSwab;
  }

  /**
   * service checkin patient. only checkin
   * 
   * @param patientId 
   * @returns 
   */
  async checkinPatient(swabId: string) {
    try {
      this.swabIdSelected = swabId;
      let detailSwab: SwabDetail = await this.getDetailSwab(swabId, false)
      detailSwab.sampleId = IdSampleGenerator(detailSwab)
      detailSwab.status = StatusSwabEnum.checkin;
      return await this.detailSwabRepository.save(detailSwab);
    } catch (error) {
      return this.checkError(error);
    }
  }

  /**
   * Swab Now
   * @param swabId 
   * @returns 
   */
  async swabNow(swabId: string) {
    try {
      // fetch and validation
      this.swabIdSelected = swabId;
      let detailSwab: SwabDetail = await this.getDetailSwab(swabId, false)

      // process update
      detailSwab.swabDate = new Date() // now

      detailSwab.status = StatusSwabEnum.done
      return await this.detailSwabRepository.save(detailSwab);
    } catch (error) {
      return this.checkError(error);
    }
  }

  /**
   * Update patient result.
   * TODO : Create validation `swabBy` is valid or is found
   * @param swabId 
   * @param updatePatientDTO 
   */
  async swabResultPatient(swabId: string, updatePatientDTO: UpdateSwabResultDTO) {
    try {

      // fetch and validation
      this.swabIdSelected = swabId;
      let detailSwab: SwabDetail = await this.getDetailSwab(swabId, false)
      this.logger.verbose(detailSwab);
      let swabByUser: User = new User();
      if (!updatePatientDTO.swabBy) {
        if (!this.idUserCurrent) {
          throw new NotAcceptableException("Cannot found user login")
        }
        updatePatientDTO.swabBy = this.idUserCurrent
      }
      swabByUser.id = updatePatientDTO.swabBy

      // Process update
      detailSwab.resultSwab = updatePatientDTO.resultSwab
      detailSwab.status = StatusSwabEnum.resultOutRef
      detailSwab.swabBy = swabByUser
      detailSwab.resultSentAt = new Date()
      let result = await this.detailSwabRepository.save(detailSwab)
      this.logger.verbose(result);
    } catch (error) {
      throw this.checkError(error);
    }
  }

  async sendSwabToLab(swabId: string, labId: string) {
    try {
      let detailSwab: SwabDetail = await this.getDetailSwab(swabId, false);
      // Skip if have result
      if(detailSwab.resultSwab){
        return;
      }
      detailSwab.labTarget = new LabDetail(labId);
      detailSwab.isSendToLab = true;
      detailSwab.sendToLabDate = new Date(); // now
      detailSwab.status = StatusSwabEnum.onLab
      return await this.detailSwabRepository.save(detailSwab);
    } catch (error) {
      this.logger.verbose(error);
      return this.checkError(error);
    }
  }

  /**
   * Update status order will update all order in transaction
   */
  async updateOrder(orderId: string, orderUpdateDTO: OrderUpdateDTO, user: User) {
    try {
      let orderData = await this.orderSwabRepository.findOne({
        where: {
          id: orderId
        },
        relations: [
          'detailSwab',
          'orderTrx'
        ]
      })
      if(!orderData){
        throw "Order id not found"
      }
      if(orderUpdateDTO.status !== StatusOrderEnum.paid){
        orderUpdateDTO.methodCash = null
      }
      
      /**
       * Update status order in transaction
       */
      await this.transactionService.updateTransactionStatus(orderData.orderTrx.id, {
        status: orderUpdateDTO.status,
        methodCash: orderUpdateDTO.methodCash
      }, user.id)

      return orderData;
    } catch (error) {
      return this.checkError(error);
    }
  }

  async setCodeBooking(swabId: string, code:string){
    let detailSwab: SwabDetail = await this.getDetailSwab(swabId);
    detailSwab.codeBooking = code;
    return this.detailSwabRepository.save(detailSwab)
  }

  /**
   * Error validation
   * 
   * @param error 
   * @param patientId 
   * @returns 
   */
  checkError(error: any, swabId?: string) {
    if (!swabId) {
      swabId = this.swabIdSelected;
    }
    if (error instanceof QueryFailedError) {
      throw new BadRequestException(error.driverError);
    }
    if (error instanceof PatientSwabExtention) {
      throw new BadRequestException(error);
    }
    this.logger.error(error);
    throw new HttpException('Sepertinya error tidak terdeteksi', 500);
  }
}

class PatientSwabExtention extends Error { }

class SwabNotFoundException extends PatientSwabExtention {
  constructor(swabId: string) {
    super();
    this.message = `Swab id ${swabId} sepertinya tidak ditemukan`
  }
  name = "SwabNotFoundException"
}