import { ApiProperty } from "@nestjs/swagger";

export class UpdateInformationTemplateDTO {
  @ApiProperty({type: 'string'})
  doctorName: string;

  @ApiProperty({type: 'string'})
  doctorSubname: string;

  @ApiProperty({type: 'string'})
  refName: string;

  @ApiProperty({type: 'string'})
  refDetail: string;
}