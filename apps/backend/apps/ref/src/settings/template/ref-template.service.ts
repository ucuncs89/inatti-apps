import { AwsService } from "@app/aws";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { RefTemplateSertifikat } from "@app/database/entity/template-map/ref-template-sertifikat.entity";
import { helperCheckColumn, TemplateSertificateColumn } from "@app/database/entity/template-map/type";
import { Product } from "@app/database/entity/ref/product.entity"
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UpdateInformationTemplateDTO } from "./type";

@Injectable()
export class RefSettingTemplateService {
  constructor(
    @InjectRepository(RefTemplateSertifikat)
    private RefTemplateSertifikatRepository: Repository<RefTemplateSertifikat>,
    private awsService: AwsService
  ){}

  getTemplateSertificate(idref: string, typeProduct: number){
    return this.RefTemplateSertifikatRepository.findOne({
      where: {
        ref: {
          id: idref
        },
        typeProduct: {
          id: typeProduct
        }
      },
      relations: ['ref', 'typeProduct']
    })
  }

  async updateTemplate(idRef:string, column:TemplateSertificateColumn, typeProduct: number, image: Express.Multer.File){    
    let refCertificate = await this.getTemplateSertificate(idRef, typeProduct)
    if(!refCertificate){
      refCertificate = new RefTemplateSertifikat()
      refCertificate.ref = new RefDetail(idRef)
      refCertificate.typeProduct = new Product(typeProduct)
    }
    if(!helperCheckColumn(column)){
      throw "Name Column not available"
    }

    let fileImage = await this.awsService.HashAndUploadS3(image)
    refCertificate[column] = fileImage.s3.Location

    return await this.RefTemplateSertifikatRepository.save(refCertificate)
  }

  async updateInformation(body: UpdateInformationTemplateDTO, idRef: string) {
    // ref only use antigen
    const idProductAntigen = 2
    let refCertificate = await this.getTemplateSertificate(idRef, idProductAntigen)
    if(!refCertificate){
      refCertificate = new RefTemplateSertifikat()
      refCertificate.typeProduct = new Product(idProductAntigen)
      refCertificate.ref = new RefDetail(idRef)
    }
    Object.assign(refCertificate, body)

    return await this.RefTemplateSertifikatRepository.save(refCertificate)
  }
}