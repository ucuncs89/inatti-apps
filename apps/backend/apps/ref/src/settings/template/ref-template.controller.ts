import { TemplateSertificateColumn } from '@app/database/entity/template-map/type';
import { BadRequestException, Body, Controller, Get, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../../helper/auth.decorator';
import { RefSettingTemplateService } from './ref-template.service';
import { UpdateInformationTemplateDTO } from './type';

class RefTemplateImageDTO { 
  @ApiProperty({type: 'string', format: 'binary'})
  image: Express.Multer.File[]
}

@ApiTags('Ref API')
@RequireAuth()
@Controller('setting/template')
export class RefSettingTemplate {
  constructor(
    private refSettingTemplate: RefSettingTemplateService
  ){}


  @Get('/')
  async getTemplateSertificate(
    @Query('idref')
    idref: string,
    @Query('typeProduct')
    typeProduct: number
  ){
    try {
      return await this.refSettingTemplate.getTemplateSertificate(idref, typeProduct)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @UseInterceptors(FileFieldsInterceptor([
    { name: 'image', maxCount: 1 },
  ]))
  @Post('/')
  async updateTemplate(
    @Query('idref')
    idRef: string,
    @Query('name')
    name: TemplateSertificateColumn,
    @Query('typeProduct')
    typeProduct: number,
    @UploadedFiles()
    files: RefTemplateImageDTO,
  ){ 
    try {
      if(!files || files.image.length < 1) throw "No image uploaded"

      return await this.refSettingTemplate.updateTemplate(idRef, name, typeProduct, files.image[0])
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/information')
  async updateInformation(
    @Body()
    body: UpdateInformationTemplateDTO,
    @Query('idref')
    idRef: string,
  ){
    try {
      return await this.refSettingTemplate.updateInformation(body, idRef)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}