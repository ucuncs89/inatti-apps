import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class RefDetailPostDTO {
  @ApiProperty()
  refName: string;

  @ApiProperty()
  @ApiPropertyOptional()
  contactWhatsapp: string;

  @ApiProperty()
  @ApiPropertyOptional()
  refAddress: string;

  @ApiProperty()
  @ApiPropertyOptional()
  refPointMap: string;

  @ApiProperty()
  @ApiPropertyOptional()
  refTenantId: string;

  @ApiProperty()
  @ApiPropertyOptional()
  refNarFaskesId: string;

  @ApiProperty()
  @ApiPropertyOptional()
  refProvinceId: number;

  @ApiProperty()
  @ApiPropertyOptional()
  refCityId: number;

  @ApiProperty()
  @ApiPropertyOptional()
  thumbnail: string

  @ApiPropertyOptional({
    description: "API config ref",
    example: "{timezone: 'WIB', 'WITA', 'WIB'}",
  })
  config: {
    timezone: string
  }
}

export class UpdateRefDetailProperties extends RefDetailPostDTO {
  @ApiProperty()
  refId: string;
}

export class RefGetAllDTO {
  @ApiProperty()
  @ApiPropertyOptional()
  provinceId?: number;

  @ApiProperty()
  @ApiPropertyOptional()
  cityId?: number;
}

export class RefProductFormDTO {
  @ApiProperty()
  productName: string;

  @ApiProperty()
  @ApiPropertyOptional()
  productPrice: number;

  @ApiProperty()
  productId: number;

  @ApiProperty()
  refId: string;

  @ApiProperty({ required: false })
  description: string;

  @ApiProperty({ required: false, example: "['Cash', 'Transfer']" })
  methodPayments: any;
}

export class RefProductFormUpdateDTO extends RefProductFormDTO {
  @ApiProperty()
  refProductId: number;
}
