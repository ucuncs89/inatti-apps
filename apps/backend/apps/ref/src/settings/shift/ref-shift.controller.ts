import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { RequireAuth } from "../../helper/auth.decorator";
import { RefShiftService } from "./ref-shift.service";
import { RefShiftFormDTO, RefShiftFormUpdateDTO } from "./ref-shift.type";


@ApiTags('Ref API')
@Controller('setting')
@RequireAuth()
export class RefShiftController {

  constructor(
    private refShiftService: RefShiftService
  ) { }

  @Get('/shift/:refId')
  async getAll(
    @Param('refId') refId: string,
  ) {
    try {
      return await this.refShiftService.getAllRefShift(refId)
    } catch (error) {
      console.log(error);
      throw new BadRequestException()
    }
  }

  @Get('/shift/:refId/:refShiftId')
  async getById(@Param('refId') refId: string, @Param('refShiftId') refShiftId: number) {
    try {
      return await this.refShiftService.getRefShiftById(refId, refShiftId)
    } catch (error) {
      throw new BadRequestException()
    }
  }

  @Post('/shift')
  async postRefShift(@Body() refShiftFormDTO: RefShiftFormDTO) {
    try {
      return await this.refShiftService.postRefShift(refShiftFormDTO)
    } catch (error) {
      throw new BadRequestException()
    }
  }

  @Patch('/shift/:refId/:refShiftId')
  async patchRefShift(@Param('refId') refId: string, @Param('refShiftId') refShiftId: number, @Body() refShisftFormUpdateDTO: RefShiftFormUpdateDTO) {
    try {
      return await this.refShiftService.patchRefShift(refId, refShiftId, refShisftFormUpdateDTO)
    } catch (error) {
      throw new BadRequestException()
    }
  }

  @Delete('/shift/:refId/:refShiftId')
  async deleteRefShift(@Param('refId') refId: string, @Param('refShiftId') refShiftId: number) {
    try {
      return await this.refShiftService.deleteRefShift(refId, refShiftId)
    } catch (error) {
      console.log(error);
      throw new BadRequestException()
    }
  }



}