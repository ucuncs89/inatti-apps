import { DaysWeekEnum } from "@app/database/entity/ref/type";
import { ApiProperty } from "@nestjs/swagger";

export class RefShiftFormDTO {

  @ApiProperty()
  refShiftName: string

  @ApiProperty()
  refShiftDay: DaysWeekEnum

  @ApiProperty()
  refShiftTimeStart: string

  @ApiProperty()
  refShiftTimeEnd: string

  @ApiProperty()
  refId: string

}


export class RefShiftFormUpdateDTO extends RefShiftFormDTO {

  @ApiProperty()
  refShiftId: number

}