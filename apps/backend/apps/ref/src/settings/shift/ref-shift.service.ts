import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { RefShift } from "@app/database/entity/ref/ref-shift.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { getManager, Repository } from "typeorm";
import { RefShiftFormDTO, RefShiftFormUpdateDTO } from "./ref-shift.type";

export class RefShiftService {

  constructor(
    @InjectRepository(RefShift)
    private RefShiftRepository: Repository<RefShift>
  ) { }

  async getAllRefShift(refId: string) {
    try {
      const query = await this.RefShiftRepository
        .createQueryBuilder('rs')
        .leftJoinAndSelect('rs.reference', 'reference')
        .where('rs.reference = :refId', { refId })

      return query.getMany()

    } catch (error) {
      throw error
    }
  }

  async getRefShiftById(refId: string, refShiftId: number) {
    try {
      const refShift: RefShift = await this.RefShiftRepository.findOne({ id: refShiftId, reference: new RefDetail(refId) });

      if (!refShift) {
        throw new RefShiftException()
      }

      return refShift
    } catch (error) {
      throw error
    }
  }

  async postRefShift(refShiftFormDTO: RefShiftFormDTO) {
    try {
      return getManager().transaction(async entity => {
        let refShift = new RefShift();

        refShift.name = refShiftFormDTO.refShiftName
        refShift.timeStart = refShiftFormDTO.refShiftTimeStart
        refShift.timeEnd = refShiftFormDTO.refShiftTimeEnd
        refShift.day = refShiftFormDTO.refShiftDay
        refShift.reference = new RefDetail(refShiftFormDTO.refId)

        try {
          refShift = await entity.save(refShift)
          return refShift
        } catch (error) {
          throw error
        }
      })
    } catch (error) {
      throw error
    }
  }

  async patchRefShift(refId: string, refShiftId: number, refShiftFormUpdateDTO: RefShiftFormUpdateDTO) {
    try {
      const refShift: RefShift = await this.RefShiftRepository.findOne({ id: refShiftId, reference: new RefDetail(refId) })

      if (!refShift) {
        throw new RefShiftException()
      }

      refShift.name = refShiftFormUpdateDTO.refShiftName
      refShift.timeEnd = refShiftFormUpdateDTO.refShiftTimeEnd
      refShift.timeStart = refShiftFormUpdateDTO.refShiftTimeStart
      refShift.day = refShiftFormUpdateDTO.refShiftDay
      refShift.reference = new RefDetail(refShiftFormUpdateDTO.refId)

      const responseRefShift = await this.RefShiftRepository.save(refShift);

      if (responseRefShift) {
        return { message: 'Reference Shift updated' }
      } else {
        return { message: 'Reference Shift not updated' }
      }
    } catch (error) {
      throw error
    }
  }

  async deleteRefShift(refId: string, refShiftId: number) {
    try {
      const refShift: RefShift = await this.RefShiftRepository.findOne({ id: refShiftId, reference: new RefDetail(refId) });

      if (!refShift) {
        throw new RefShiftException()
      }

      const response = await this.RefShiftRepository.delete({ id: refShiftId })

      if (response) {
        return { message: 'Reference Shift deleted' }
      } else {
        return { message: 'Reference Shift not deleted' }
      }
    } catch (error) {
      throw error
    }
  }
}

export class RefShiftException extends Error {
  name = "RefShiftException"
  message = "Reference Shift not Exists"
}