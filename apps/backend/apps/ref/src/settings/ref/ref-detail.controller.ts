import { RequireAuth } from "../../helper/auth.decorator";
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, UploadedFiles, UseGuards, UseInterceptors } from "@nestjs/common";
import { ApiConsumes, ApiProperty, ApiTags } from "@nestjs/swagger";
import { RefDetailService } from "./ref-detail.service";
import { RefDetailPostDTO, } from "../type";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { RefConfigDTO } from "./type";

class RefUpdateImageDTO { 
  @ApiProperty({type: 'string', format: 'binary'})
  thumbnail: Express.Multer.File[]
}

@ApiTags('Ref API')
@Controller('setting')
@RequireAuth()
export class RefDetailController {
  constructor(
    private settingRefServices: RefDetailService
  ) { }


  @Get('/ref/:refId')
  async getByIdRefDetail(@Param('refId') refId: string) {
    try {
      return await this.settingRefServices.getRefById(refId)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/ref')
  async postRefDetail(@Body() refDetailForm: RefDetailPostDTO) {
    try {
      return await this.settingRefServices.createRef(refDetailForm);
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/ref/:refId')
  async patchRefDetail(@Param('refId') refId: string, @Body() refDetailForm: RefDetailPostDTO) {
    try {
      return await this.settingRefServices.updateRefDetail(refId, refDetailForm)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Delete('/ref/:refId')
  async deleteRefDetail(@Param('refId') refId: string) {
    try {
      return await this.settingRefServices.deleteRef(refId)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @UseInterceptors(FileFieldsInterceptor([
    { name: 'thumbnail', maxCount: 1 },
  ]))
  @ApiConsumes('multipart/form-data')
  @Patch('/ref/:refId/thumbnail')
  async updateThumbnailRef(
    @Param('refId') refId: string,
    @UploadedFiles()
    files: RefUpdateImageDTO,
  ) {
    try {
      return await this.settingRefServices.updateThumbnailRef(refId, files.thumbnail[0])
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Patch('/config/:refId')
  async foo(
    @Param('refId')
    idRef: string,
    @Body()
    config: RefConfigDTO
  ){
    try {
      return await this.settingRefServices.updateConfigRef(idRef, config)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}