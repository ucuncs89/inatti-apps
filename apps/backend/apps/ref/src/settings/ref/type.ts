import { ApiProperty } from "@nestjs/swagger";

export class RefConfigDTO {
  @ApiProperty({
    description: 'Timezone report pdf',
    example: 'WIB, WIT, WITA'
  })
  timezone: string;
}