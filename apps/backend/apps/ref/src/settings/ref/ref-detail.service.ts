import { AwsService } from '@app/aws';
import { RefConfig } from '@app/database/entity/ref/ref-config.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { Kabupaten } from '@app/database/entity/region/kabupaten.entity';
import { Provinsi } from '@app/database/entity/region/provinsi.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { RefDetailPostDTO } from '../type';
import { RefConfigDTO } from './type';


@Injectable()
export class RefDetailService {
  constructor(
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
    @InjectRepository(RefConfig)
    private RefConfigRepository: Repository<RefConfig>,
    private awsService: AwsService
  ) { }

  async isRefExists(refName: string) {
    let user = await this.RefDetailRepository.find({ where: { name: refName } })
    if (user.length > 0) {
      throw new RefExistException();
    }
  }

  // This function are not used
  async getRefList(city: number, province: number, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {

    try {
      const query = await this.RefDetailRepository.createQueryBuilder('ru').select([
        'ru.id',
        'ru.name',
        'ru.ref_address',
      ])
        .leftJoinAndSelect('ru.products', 'product')
        .leftJoinAndSelect('ru.province', 'province')
        .leftJoinAndSelect('ru.city', 'city')

      console.log(query.getMany())


      if (province) {
        query.where('ru.provinceId = :province', { province })
      }

      if (city) {
        query.where('ru.cityId = :city', { city })
      }

      const results = await QueryUsePaginationAndFilter(query, {
        paginate, filter,
        columnDate: '"ru"."created_at"',
        columnSearch: '"ru"."name"'
      }).getMany();

      const total = await this.RefDetailRepository.count();

      return new Pagination({
        results,
        total,
      }, paginate)
    } catch (error) {
      console.log(error)
    }
  }

  async createRef(refDetailForm: RefDetailPostDTO) {
    await this.isRefExists(refDetailForm.refName)

    return getManager().transaction(async entity => {
      let refDetail = new RefDetail();
      refDetail.name = refDetailForm.refName
      refDetail.contactWhatsapp = refDetailForm.contactWhatsapp
      refDetail.refAddress = refDetailForm.refAddress
      refDetail.refAddressPoint = refDetailForm.refPointMap
      refDetail.tenantId = refDetailForm.refTenantId
      refDetail.province = new Provinsi(refDetailForm.refProvinceId)
      refDetail.city = new Kabupaten(refDetailForm.refCityId)
      refDetail.image = refDetailForm.thumbnail
      refDetail.narFaskesId = refDetailForm.refNarFaskesId
      refDetail = await entity.save(refDetail)

      return true
    })
  }

  async updateRefDetail(refId: string, refDetailForm: RefDetailPostDTO) {
    let ref: RefDetail = await this.RefDetailRepository.findOne(refId);

    if (!ref) {
      throw new RefNotExistsException()
    }

    if (ref) {
      let updateRefDetail = await this.RefDetailRepository.update(refId, {
        name: refDetailForm.refName,
        refAddress: refDetailForm.refAddress,
        contactWhatsapp : refDetailForm.contactWhatsapp,
        refAddressPoint: refDetailForm.refPointMap,
        tenantId: refDetailForm.refTenantId,
        narFaskesId: refDetailForm.refNarFaskesId,
        province: new Provinsi(refDetailForm.refProvinceId),
        city: new Kabupaten(refDetailForm.refCityId),
        image: refDetailForm.thumbnail
      })

      if(refDetailForm.config){
        await this.updateConfigRef(refId, refDetailForm.config)
      }

      return updateRefDetail
    }

    return false
  }

  async getRefById(refId: string) {
    return await this.RefDetailRepository.findOne(refId, {
      relations: ['products', 'city', 'province', 'config']
    })
  }

  async deleteRef(refId: string) {
    return await this.RefDetailRepository.delete({ id: refId })
  }

  async updateThumbnailRef(refId: string, thumbnail: Express.Multer.File){
    if(!thumbnail) throw new Error("Thumbnail not found")
    let ref: RefDetail = await this.RefDetailRepository.findOne(refId);
    if(!ref) throw new RefNotExistsException()

    let fileThumbnail = await this.awsService.HashAndUploadS3(thumbnail)
    ref.image = fileThumbnail.s3.Location
    return await this.RefDetailRepository.save(ref)
  }

  async updateConfigRef(idRef: string, payload: RefConfigDTO){
    let config = await this.RefConfigRepository.findOne({
      ref: {
        id: idRef
      }
    })
    if(!config){
      config = new RefConfig()
      config.ref = new RefDetail(idRef)
    }
    Object.assign(config, payload)

    return await this.RefConfigRepository.save(config)
  }
}

export class RefExistException extends Error {
  name = "RefExistException"
  message = "Reference is exist"
}

export class RefNotExistsException extends Error {
  name = "RefNotExistsException"
  message = "Reference is not exist"
}