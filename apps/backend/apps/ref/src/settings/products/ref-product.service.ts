import { Product } from "@app/database/entity/ref/product.entity";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { RefProduct } from "@app/database/entity/ref/ref-product.entity";
import { BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { getManager, In, Not, Repository } from "typeorm";
import { RefProductFormDTO } from "../type";

export class RefProductService {
  constructor(
    @InjectRepository(RefProduct)
    private RefProductRepository: Repository<RefProduct>
  ) { }


  async getAllRefProducts(refId: string, typeProduct?: 'vaksin' | 'test-covid') {
    const filterTypeProduct: any = {}
    if (typeProduct === 'vaksin') {
      filterTypeProduct.id = 3
    }
    if (typeProduct === 'test-covid') {
      filterTypeProduct.id = In([1, 2])
    }
    try {
      let res = await this.RefProductRepository.find({
        where: {
          ref: {
            id: refId,
          },
          product: filterTypeProduct
        },
        relations: [
          'product',
          'ref',
        ],
        order: {
          id: "DESC"
        }
      })

      return res;
    } catch (error) {
      throw new BadRequestException()
    }
  }

  async getRefProductsById(refId: string, refProductsId: number) {
    try {
      return await this.RefProductRepository.findOne({
        where: { id: refProductsId, ref: new RefDetail(refId) },
        relations: ['product']
      })
    } catch (error) {
      throw new BadRequestException()
    }
  }

  async postRefProducts(refProductDTO: RefProductFormDTO) {
    try {
      return getManager().transaction(async entity => {
        let refProduct = new RefProduct();

        refProduct.price = refProductDTO.productPrice
        refProduct.name = refProductDTO.productName
        refProduct.description = refProductDTO.description
        refProduct.product = new Product(refProductDTO.productId)
        refProduct.ref = new RefDetail(refProductDTO.refId)
        refProduct.methodPayments = refProductDTO.methodPayments

        refProduct = await entity.save(refProduct);

        return true;
      })
    } catch (error) {
      throw new BadRequestException()
    }
  }

  async patchRefProducts(refId: string, refProductId: number, refProductFormDTO: RefProductFormDTO) {
    try {
      const refProduct: RefProduct = await this.RefProductRepository.findOne({
        id: refProductId, ref: new RefDetail(refId)
      })

      if (!refProduct) {
        throw new RefProductNotExistsException()
      }

      refProduct.price = refProductFormDTO.productPrice
      refProduct.description = refProductFormDTO.description
      refProduct.product = new Product(refProductFormDTO.productId)
      refProduct.name = refProductFormDTO.productName
      refProduct.ref = new RefDetail(refProductFormDTO.refId)
      refProduct.methodPayments = refProductFormDTO.methodPayments

      const response = await this.RefProductRepository.save(refProduct)

      if (response) {
        return { message: 'Product Ref is Updated' }
      } else {
        return { message: 'Product Ref not updated' }
      }
    } catch (error) {
      throw new BadRequestException()
    }
  }

  async deleteRefProduct(refId: string, refProductId: number) {
    try {
      const refProduct = await this.getRefProductsById(refId, refProductId)

      if (!refProduct) {
        throw new RefProductNotExistsException()
      }

      if (refProduct) {
        // manual soft delete
        await this.RefProductRepository.softDelete(refProductId)
      }

      return { message: 'Product Ref deleted' }
    } catch (error) {
      throw new BadRequestException()
    }
  }
}

export class RefProductExistException extends Error {
  name = "RefExistException"
  message = "Reference Product is exist"
}

export class RefProductNotExistsException extends Error {
  name = "RefNotExistsException"
  message = "Reference Product is not exist"
}