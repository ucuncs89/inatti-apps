import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query } from "@nestjs/common";
import { ApiQuery, ApiTags } from "@nestjs/swagger";
import { RequireAuth } from "../../helper/auth.decorator";
import { RefProductFormDTO, RefProductFormUpdateDTO } from "../type";
import { RefProductService } from "./ref-product.service";

@ApiTags('Ref API')
@Controller('setting')
@RequireAuth()
export class RefProductController {

  constructor(
    private settingRefProductService: RefProductService
  ) { }

  @ApiQuery({
    name: 'typeProduct',
    required: false,
    enum: ['vaksin', 'test-covid']
  })
  @Get('/products/:refId')
  async getReferenceProducts(
    @Param('refId') refId: string,
    @Query('typeProduct') typeProduct?: 'vaksin' | 'test-covid'
  ) {
    try {
      return await this.settingRefProductService.getAllRefProducts(refId, typeProduct)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/product/:refId/:refProductId')
  async getReferenceProductById(@Param('refId') refId: string, @Param('refProductId') refProductId: number) {
    try {
      return await this.settingRefProductService.getRefProductsById(refId, refProductId)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/products')
  async postReferenceProducts(@Body() refProductDTO: RefProductFormDTO) {
    try {
      return await this.settingRefProductService.postRefProducts(refProductDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/product/:refId/:refProductId')
  async patchReferenceProduct(@Param('refId') refId: string, @Param('refProductId') refProductId: number, @Body() refProductUpdateDTO: RefProductFormUpdateDTO) {
    try {
      return await this.settingRefProductService.patchRefProducts(refId, refProductId, refProductUpdateDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Delete('/product/:refId/:refProductId')
  async deleteReferenceProduct(@Param('refId') refId: string, @Param('refProductId') refProductId: number) {
    try {
      return await this.settingRefProductService.deleteRefProduct(refId, refProductId)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}