import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { RefRole } from '@app/database/entity/ref/ref-role.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RefService {
  constructor(
    @InjectRepository(RefRoleUser)
    private refRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(RefRole)
    private refRoleRepository: Repository<RefRole>,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(LabDetail)
    private labDetailRepository: Repository<LabDetail>,
  ) { }

  getRegisteredRefUser(user: User) {
    return this.refRoleUserRepository.find({
      where: {
        userId: user
      },
      withDeleted: true
    })
  }

  getRegisteredRefUserById(user: User, idRef: string) {
    return this.refRoleUserRepository.findOne({
      where: {
        userId: user.id,
        refId: idRef
      },
      withDeleted: true
    })
  }

  async getListLab(idRef: string) {
    let labs: LabDetail[] = await this.labDetailRepository.find()
    return labs;
  }

  getRoleUser(idUser: any, idRef: string) {
    return this.RefRoleUserRepository.findOne({
      where: {
        userId:{
          id: idUser
        },
        refId: {
          id: idRef
        }
      }
    })
  }
}
