export class SendResult2NarDTO {
  cookies: string;
  listSample: string[];
  isForce: boolean; // force send to nar even if sample is already sent
}