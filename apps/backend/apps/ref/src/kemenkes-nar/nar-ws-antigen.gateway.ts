import { SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { Socket } from 'net';
import { NarAntigenIntegration } from './nar-antigen-integration.service';
import { SendResult2NarDTO } from './type';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
  namespace: '/nar-antigen'
})
export class NarWebsocketGateway {
  constructor(
    private narAntigenIntegration: NarAntigenIntegration
  ){}

  @SubscribeMessage('nar_antigen')
  async NarAntigen(client: Socket, payload: SendResult2NarDTO) {
    await this.narAntigenIntegration.sendSampleToNar(
      payload,
      {
        log: (params:any, type?:any) => {
          client.emit('log', {
            params,
            type: type ?? null,
            timestamp: new Date()
          })
        },
        finish: () => {
          client.emit('finish')
        },
        updateSample: (sampleId: string, isSuccess: boolean, message?: string) => {
          client.emit('updateSample', {
            sampleId,
            isSuccess,
            message
          })
        }
      }
    )
  }
}
