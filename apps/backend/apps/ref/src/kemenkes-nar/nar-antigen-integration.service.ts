import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { NarAntigen } from '@inatti/nar-api-unofficial'
import { SendResult2NarDTO } from './type';
import { GenderEnum, NikTypeEnum, ResultSwabEnum } from '@app/database/entity/swab-data/type';
import { NarSample } from '@app/database/entity/nar/nar-sample.entity';
import { LoggerFileNar } from 'apps/lab/src/kemenkes-nar/logger-nar'

@Injectable()
export class NarAntigenIntegration {
  constructor(
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(NarSample)
    private NarSampleRepository: Repository<NarSample>,
  ) { }

  logger = new Logger('NarAntigenIntegration')

  formatDateNar(value: string | Date) {
    if (!value) return null;
    return dayjs(value).format("YYYY-MM-DD HH:mm:ss")
  }

  getSwabDetail(idSample: number): Promise<SwabDetail> {
    return this.SwabDetailRepository.findOneOrFail({
      where: {
        id: idSample,
      },
      relations: [
        'refTarget',
        'labTarget',
        'patient',
        'patient.domisiliProvinsi',
        'patient.domisiliKabupaten',
        'patient.domisiliKecamatan',
        'patient.domisiliKelurahan',
        'narSample'
      ]
    })
  }

  async sendSampleToNar(
    params: SendResult2NarDTO,
    cb:{
      log: (params:any, type?:'log' | 'success' | 'error' | 'json') => void,
      finish: () => void,
      updateSample: (sampleId: string, isSuccess: boolean, message?: string) => void
    }
  ){
    let loggerNar = new LoggerFileNar('nar-antigen')

    const printIdentity = (sample:SwabDetail) => `${sample.patient.name} [${sample.patient.nik}, ${sample.patient.datebirth}]`
    const formatDate = (date: any) => date ? dayjs(date).format('DD-MM-YYYY'): ''
    const cleanNumberPhone = (phone: string) => phone.replace(/^\+?62/, '0')

    const callbackLog = (params:any, type?:'log' | 'success' | 'error' | 'json') => {
      cb.log(params, type) // callback log to client
      loggerNar.log(params) // callback log to file nar
    }


    let narAntigen = new NarAntigen();
    callbackLog(`Mencoba login dengan Cookies ${params.cookies}`, 'log')

    try{
      await narAntigen.setCookies(params.cookies)
      callbackLog(`Berhasil atur cookies`, 'success')
    } catch(err){
      this.logger.error(`Gagal Login : ${err.message}`)
      callbackLog(`Gagal Login : ${err.message}`, 'error')
      cb.finish()
      return;
    }

    let cookies = await narAntigen.getCookies()
    callbackLog(`Cookies yang diatur : ${await cookies.getCookieString(narAntigen.url.base)}`)

    try{
      callbackLog(`Mencoba mencari faskes Id`)
      let faskesId = await narAntigen.getFaskesId()
      callbackLog(`Faskes id ditemukan : ${faskesId}`, 'success')
    }catch(err){
      callbackLog(`Gagal ambil faskes id : ${err}`, 'error')
      cb.finish()
      return;
    }
    
    narAntigen.log = (msg: string, type: 'error' | 'success' | 'warning') => {
      callbackLog(msg)
      if(type === 'error'){
        throw(msg)
      }
    }
     
    for (const sampleId of params.listSample) {
      let sample = await this.getSwabDetail(+sampleId)
      callbackLog(`Proses Sample ${printIdentity(sample)}`, 'log')

      if(!sample.resultSwab){
        callbackLog(`Gagal proses sample ${printIdentity(sample)} Karena hasil belum di publish`, 'error')
        cb.updateSample(sampleId, false, 'Hasil belum di publish')
        continue;
      }

      if(sample.narSample?.isSuccess && !params.isForce){
        callbackLog(`Sample ${printIdentity(sample)} Telah di masukkan ke NAR, batal aksi`, 'error')
        cb.updateSample(sampleId, true, 'Sudah dimasukkan')
        continue;
      }

      if(sample.patient.type !== NikTypeEnum.nik){
        callbackLog(`Sample ${printIdentity(sample)} Tipe identitas Tidak NIK KTP, batal aksi`, 'error')
        cb.updateSample(sampleId, true, 'Tipe identitas Tidak NIK KTP')
        continue;
      }
      
      const resutlNarPayload = {
        id: '',
        sess_status: '',
        ct_value: '',
        tujuan_detail: '1', // Pelaku Perjalanan
        asal_data: '1', 
        tujuan: '3', // skrinning
        jns_identitas: '1', // NIK

        nik: sample.patient.nik,
        jkel: sample.patient.gender === GenderEnum.male ? 'L' : 'P',
        nama: sample.patient.name,
        tgl_lahir: formatDate(sample.patient.datebirth),
        telp: cleanNumberPhone(sample.patient.phonenumber),
        alamat: sample.patient.address,
        propinsi: sample.patient.domisiliProvinsi?.id + '' ?? '',
        kabupaten: sample.patient.domisiliKabupaten?.id + '' ?? '',
        kecamatan: sample.patient.domisiliKecamatan?.id + '' ?? '',
        desa: sample.patient.domisiliKelurahan?.id + '' ?? '',
        rw: sample.patient.domisiliKelurahanRw,
        rt: sample.patient.domisiliKelurahanRt,
        alamat_domisili_checked: 'on',
        alamat_domisili: sample.patient.addressDomisili ?? '',
        no_lab: sample.sampleId,
        hsl_lab: sample.resultSwab === ResultSwabEnum.negatif ? 'NEGATIF' : 'POSITIF',
        
        tgl_periksa: formatDate(sample.resultSentAt),
      }
      callbackLog(JSON.stringify({payload: resutlNarPayload}), 'json')

      try{
        // set callback everytime send request, will save as the log
        narAntigen.callbackResultPage = (pageResult:string, url:string) => {
          loggerNar.log(`Result page : ${url}`)
          loggerNar.log(pageResult)
        }

        let resultNar = await narAntigen.registerPatient(resutlNarPayload)
        loggerNar.log(`Result page nar`)
        loggerNar.log(resultNar)

        callbackLog(`Pasien ${printIdentity(sample)} Sukses dikirim ke NAR`, 'success')
        await this.inputNarDone(sample, true, null)
        cb.updateSample(sampleId, true)
      }catch(err){
        const errMessage = err.message ?? err
        this.logger.error(`Gagal kirim Pasien ${printIdentity(sample)} : ${errMessage}`)
        callbackLog(`Gagal kirim Pasien ${printIdentity(sample)} : ${errMessage}`, 'error')
        await this.inputNarDone(sample, false, errMessage)
        cb.updateSample(sampleId, false, errMessage)
      }
    }
    cb.finish()
  }

  async inputNarDone(sample: SwabDetail, isSuccess: boolean, message?: string){
    let narSample = await this.NarSampleRepository.findOne({swabDetail: sample})
    if(!narSample) {
      narSample = new NarSample();
    }
    narSample.swabDetail = sample
    narSample.isSuccess = isSuccess
    narSample.message = message

    this.NarSampleRepository.save(narSample)
  }
}
