import { BadRequestException, Body, Controller, Logger, Param, Post, Query, Request, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { ApiExternalService } from './api-external.service';
import { RujukanToLabDTO } from './type';

@ApiTags('Ref API')
@Controller('api-external')
export class ApiExternalController {
  constructor(
    private apiExternalService: ApiExternalService
  ) { 
    this.logger = new Logger('Api Eksternal')
  }

  logger:Logger;

  @RequireAuth()
  @Post('/swab-patient/pcr/to-lab/:idlab')
  async pcrImportPatientAndSendToLab(
    @Param('idlab')
    idLab: string,
    @Body()
    body: RujukanToLabDTO,
    @Request()
    req:any
  ) {
    const refOld = await this.apiExternalService.getRefDataFromExisting(body.refData)

    if(!refOld || !refOld.newRef.id) throw new Error('Ref old does not found')

    const idRef = refOld.newRef.id

    if (!Array.isArray(body.patientRujukan)) {
      body.patientRujukan = [body.patientRujukan]
    }
    this.logger.log(`Data received from ${idRef} : ${JSON.stringify(body.patientRujukan)}`)
    if(body.patientRujukan.length < 1){
      throw new BadRequestException('No input found')
    }

    const response = await this.apiExternalService.pcrImportBulkToLab(body.patientRujukan, { idLab, idRef, userId: req.user.id })

    if (response.failedBulk.length > 0) {
      throw new BadRequestException(response);
    }
    return response
  }
}
