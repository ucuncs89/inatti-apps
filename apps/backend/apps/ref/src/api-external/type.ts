import { BloodTypeEnum, GenderEnum, NikTypeEnum } from "@app/database/entity/swab-data/type";
import { ApiProperty } from "@nestjs/swagger";

export class RefOldDTO {
  @ApiProperty({
    description: 'id rujukan pada service'
  })
  id: number;
  @ApiProperty()
  name: string;
}

export class PatientRujukanToLabDTO {
  @ApiProperty()
  nik: string;

  @ApiProperty({
    enum: NikTypeEnum
  })
  type: NikTypeEnum;

  @ApiProperty()
  name: string;

  @ApiProperty()
  placebirth: string;

  @ApiProperty()
  datebirth: string;

  @ApiProperty({
    enum: GenderEnum
  })
  gender: GenderEnum;

  @ApiProperty()
  phonenumber: string;

  @ApiProperty()
  email: string;

  @ApiProperty({
    enum: BloodTypeEnum
  })
  bloodtype: BloodTypeEnum;

  @ApiProperty()
  provinsi: number;

  @ApiProperty()
  kabupaten: number;

  @ApiProperty()
  kecamatan: number;

  @ApiProperty()
  address: string;

  @ApiProperty()
  citizenship: string;

  @ApiProperty()
  addressDomisili: string;

  @ApiProperty()
  sampleId: string;
}

export class RujukanToLabDTO {
  @ApiProperty({
    type: () => RefOldDTO
  })
  refData: RefOldDTO
  @ApiProperty({
    type: () => [PatientRujukanToLabDTO]
  })
  patientRujukan: PatientRujukanToLabDTO | PatientRujukanToLabDTO[]
}