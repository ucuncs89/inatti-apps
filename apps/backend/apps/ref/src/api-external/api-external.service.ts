import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabHandoverBatch } from '@app/database/entity/lab/lab-handover-batch.entity';
import { LabHandoverMap } from '@app/database/entity/lab/lab-handover-map.entity';
import { LabHistoryStatusSample } from '@app/database/entity/lab/lab-history-sample.entity';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { Product } from '@app/database/entity/ref/product.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefOld } from '@app/database/entity/ref/ref-old.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { Kabupaten } from '@app/database/entity/region/kabupaten.entity';
import { Kecamatan } from '@app/database/entity/region/kecamatan.entity';
import { Provinsi } from '@app/database/entity/region/provinsi.entity';
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { StatusSwabEnum } from '@app/database/entity/swab-data/type';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, getManager, Repository } from 'typeorm';
import { PatientRujukanToLabDTO, RefOldDTO } from './type';

@Injectable()
export class ApiExternalService {
  constructor(
    @InjectRepository(LabHandoverBatch)
    private LabHandoverBatchRepository: Repository<LabHandoverBatch>,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(RefOld)
    private RefOldRepository: Repository<RefOld>,
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
  ) {
    this.logger = new Logger('ApiExternalService')
  }

  private logger: Logger;

  async getRefDataFromExisting(refOld: RefOldDTO): Promise<RefOld>{
    this.logger.log(`Get data ref old ${JSON.stringify(refOld)}`)
    let refOldData = await this.RefOldRepository.findOne({
      where: {
        oldId: refOld.id
      },
      relations: ['newRef']
    })
    if(!refOldData){
      this.logger.warn(`Ref old ${JSON.stringify(refOld)} not registered, Register now`)
      return await this.createNewRefFromOld(refOld);
    }
    this.logger.verbose(`Ref old ${JSON.stringify(refOld)} have been registered`)
    return refOldData;
  }

  async createNewRefFromOld(refOld: RefOldDTO){
    let newRef = new RefDetail();
    newRef.name = refOld.name;

    newRef = await this.RefDetailRepository.save(newRef);

    let oldRef = new RefOld();
    oldRef.oldId = refOld.id;
    oldRef.oldName = refOld.name
    oldRef.newRef = newRef;

    oldRef = await this.RefOldRepository.save(oldRef);
    this.logger.log(`Success register new ref from exisiting : ${JSON.stringify(oldRef)}`)

    return await this.getRefDataFromExisting(refOld);
  }
  
  async pcrImportBulkToLab(payload: PatientRujukanToLabDTO[], options: {idLab: string, idRef: string, userId: string}){
    let processBulk = 0
    let successBulk = 0
    let failedBulk = []
    
    let batchLab = await this.createBatch(options.idRef, options.idLab)
    this.logger.log(`batch lab ${batchLab.id ?? 'null'}`)
    for (const patient of payload) {
      await getManager().transaction(async entity => {
        processBulk++;
        this.logger.log(`process : ${JSON.stringify(patient)}`)
        try {
          await this.pcrImportPatientAndSendToLab(entity, patient, options, batchLab)
          successBulk++;
        } catch (error) {
          this.logger.error(`Failed insert patient`)
          this.logger.error(error)
          failedBulk.push({
            sampleId: patient.sampleId,
            errorMessage: error.message
          })
        }
      });
    }
    return {
      processBulk,
      successBulk,
      failedBulk
    }
  }

  async createBatch(createdBy: string, labId: string):Promise<LabHandoverBatch>{
    let newBatch = new LabHandoverBatch();
    newBatch.name = `Batch ${Date.now()}`
    newBatch.createdBy = new RefDetail(createdBy)
    newBatch.labTarget = new LabDetail(labId)
    
    return this.LabHandoverBatchRepository.save(newBatch)
  }

  async pcrImportPatientAndSendToLab(entity:EntityManager, payload: PatientRujukanToLabDTO, options: { idLab: string, idRef: string, userId: string }, batch: LabHandoverBatch) {
    const { idLab, idRef } = options
    let patient:any
    // Add patient data
    try{
      patient = await this.addPatientData(entity, payload)
    }catch(error){
      this.logger.error(`Failed add patient data : ${JSON.stringify(payload)}`)
      throw error
    }

    // add swab detail
    let swabDetail:any
    const paramSwabDetail = {
      patient,
      idLab,
      idRef,
      sampleId: payload.sampleId
    }
    try{
      swabDetail = await this.addSwabDetail(entity, paramSwabDetail)
    } catch(error) {
      this.logger.error(`Failed add swab detail : ${JSON.stringify(paramSwabDetail)}`)
      throw error
    }

    
    // add status sample data
    try{
      await this.setStatusEntrySample(entity, swabDetail, options.userId)
    }catch(error){
      this.logger.error(`Failed`)
    }

    // assign to batch
    try{
      await this.assignSampleToBatch(entity, swabDetail, batch)
    }catch(error){
      this.logger.error(`Failed add sample batch : ${batch}`)
      throw error
    }
  }

  async addPatientData(entity: EntityManager, payload:PatientRujukanToLabDTO):Promise<SwabPatient>{
    let patient = new SwabPatient()
    patient.nik = payload.nik;
    patient.type = payload.type;
    patient.name = payload.name;
    patient.placebirth = payload.placebirth;
    patient.datebirth = payload.datebirth;
    patient.gender = payload.gender;
    patient.phonenumber = payload.phonenumber;
    patient.email = payload.email;
    patient.bloodtype = payload.bloodtype;

    // Data provinsi, kabpaten and kecamatan from existing sometimes not match to new DB
    // this place will be return null if not available to new DB
    let dataProvinsi = await entity.find(Provinsi, { where: { id: payload.provinsi } })
    if (dataProvinsi.length < 1) payload.provinsi = null
    let dataKabupaten = await entity.find(Kabupaten, { where: { id: payload.kabupaten } })
    if (dataKabupaten.length < 1) payload.kabupaten = null
    let dataKecamatan = await entity.find(Kecamatan, { where: { id: payload.kecamatan } })
    if (dataKecamatan.length < 1) payload.kecamatan = null

    patient.provinsi = payload.provinsi ? new Provinsi(payload.provinsi) : undefined;
    patient.kabupaten = payload.kabupaten ? new Kabupaten(payload.kabupaten) : undefined;
    patient.kecamatan = payload.kecamatan ? new Kecamatan(payload.kecamatan) : undefined;

    patient.address = payload.address;
    patient.citizenship = payload.citizenship;
    patient.addressDomisili = payload.addressDomisili;
    
    patient = await entity.save(patient)
    return patient;
  }

  async addSwabDetail(entity: EntityManager, options: {patient: SwabPatient, idLab:string, idRef: string, sampleId: string}):Promise<SwabDetail>{
    let swabData = new SwabDetail();
    swabData.patient = options.patient;
    swabData.isSendToLab = true;
    swabData.labTarget = new LabDetail(options.idLab)
    swabData.sendToLabDate = new Date(); // now
    swabData.productSwab = new Product(1) // PCR
    swabData.refTarget = new RefDetail(options.idRef)
    swabData.status = StatusSwabEnum.onLab // default
    swabData.sampleId = options.sampleId

    return await entity.save(swabData)
  }

  async assignSampleToBatch(entity: EntityManager, sample: SwabDetail, batch: LabHandoverBatch){
    let mapBatch = new LabHandoverMap()
    mapBatch.batch = batch
    mapBatch.swabDetail = sample
    
    return await entity.save(mapBatch)
  }

  async setStatusEntrySample(entity: EntityManager, sample: SwabDetail, userId: string){
    let historyStatus           = new LabHistoryStatusSample()
        historyStatus.sample    = sample
        historyStatus.status    = LabStatusSample.statusEntry
        historyStatus.createdBy = new User(userId)

    return entity.save(historyStatus)
  }
}