import { JwtAuthGuardRef } from "@app/authentication/authorization/jwt.authguard.ref";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth } from "@nestjs/swagger";

export function RequireAuth() {
  return applyDecorators(
    UseGuards(JwtAuthGuardRef),
    ApiBearerAuth()
  )
}