import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { ReportCacheRef } from '@app/database/entity/report/report-cache-ref.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from "dayjs";
import { getManager, Repository } from 'typeorm';
import { DashboardData } from './type';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(ReportCacheRef)
    private ReportCacheRefRepository: Repository<ReportCacheRef>,
  ){}

  // https://day.js.org/docs/en/manipulate/add
  MAX_AGE_REPORT:any[] = [1, 'day']

  async getDashboard(idRef:string, isForce?: boolean){
    let reportCache = await this.ReportCacheRefRepository.findOne({
      where: {
        ref: {
          id: idRef
        }
      }
    })
    if(!reportCache || this.isOld(reportCache.updatedAt) || isForce){
      let result = await this.generateDashboard(idRef);
      await this.saveData(result, idRef)
      return this.getDashboard(idRef);
    }

    return reportCache;
  }

  isOld(date:Date){
    let updateDate = dayjs(date)
    //@ts-ignore
    let expired = updateDate.add(+this.MAX_AGE_REPORT[0], this.MAX_AGE_REPORT[1] + "")
    return dayjs().isAfter(expired)
  }

  private async generateDashboard(idRef:string){
    console.log('generate dashboard')
    const resultData = new DashboardData();
    
    resultData.patientRegistered = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where ref_target = $1', [idRef])
    resultData.sampleProcessed = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where ref_target = $1 and result_swab is not null', [idRef])
    resultData.totalSampleNegatif = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where ref_target = $1 and result_swab = $2', [idRef, 'negatif'])
    resultData.totalSamplePositif = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where ref_target = $1 and result_swab = $2', [idRef, 'positif'])
    resultData.recordSampleResult = await this.runQueryGroupResult(idRef)
    resultData.recordSampleType = await this.runQueryGroupType(idRef)
    resultData.recordSampleResultPcr = await this.runQueryGroupResult(idRef, '1')
    resultData.recordSampleResultAntigen = await this.runQueryGroupResult(idRef, '2')
    return resultData;
  }

  private async saveData(result:any, idRef:string){
    let report = await this.ReportCacheRefRepository.findOne({
      where: {
        ref: {
          id: idRef
        }
      }
    })

    if(!report){
      report = new ReportCacheRef()
      report.data = result;
      report.ref = new RefDetail(idRef)
      report.updatedAt = new Date()
    } else {
      report.data = result;
    }
    report = await this.ReportCacheRefRepository.save(report)

    return report;
  }

  async runQueryTotal(column:string, query:string, params: any[]){
    let result = await getManager().query(query, params)
    
    if(!result) return null
    if(result.length < 1) return null

    return result[0][column]
  }

  /**
   * Graph result (sisi kiri dashboard)
   */
  async runQueryGroupResult(idRef:string, type?:string){
    let result = {}
    const intervals = ['day', 'month', 'year']
    for (const interval of intervals) {
      result[interval] = await this.runQueryGroupResultInterval({idRef, type}, interval)
    }
    return result;
  }

  async runQueryGroupResultInterval(param:{idRef:string, type?:string}, interval:string){
    let where = [
      `ref_target = '${param.idRef}'`,
      `result_swab is not null`,
    ]
    if(param.type){
      where.push(`product_swab_id = '${param.type}'`)
    }

    if(interval === 'day'){
      let from = dayjs().subtract(2, 'month')
      where.push(`created_at >= '${from.format('YYYY-MM-DD')}'`)
    }

    let query = `select
      count(*) as value,
      result_swab as group,
      date_trunc('${interval}', created_at)::date::varchar as xaxis
    from
      swab_detail sd
    where
       ${where.join(" and ")}
    group by
      xaxis, result_swab
    order by xaxis desc`;

    let result = await getManager().query(query);
    if(Array.isArray(result)){
      result.reverse();
    }
    return result;
  }

  /**
   * Graph type test (sisi kanan dashboard)
   */
  async runQueryGroupType(idRef: string){
    let result = {}
    const intervals = ['day', 'month', 'year']
    for (const interval of intervals) {
      result[interval] = await this.runQueryGroupTypeInterval(idRef, interval)
    }
    return result;
  }

  async runQueryGroupTypeInterval(idRef:string, interval: string){
    let where = [
      `ref_target = '${idRef}'`,
      `"resultSentAt" is not null`
    ]

    if(interval === 'day'){
      let from = dayjs().subtract(2, 'month')
      where.push(`"created_at" >= '${from.format('YYYY-MM-DD')}'`)
    }

    let query = `select
      count(*) as value,
      p.id, p."name" as group,
      date_trunc('${interval}', sd.created_at)::date::varchar as xaxis
    from
      swab_detail sd
    inner join product p on p.id = sd.product_swab_id 
    where
       ${where.join(" and ")}
    group by
      xaxis, p.id, p."name" 
    order by xaxis desc`
    
    let result:Record<string, string>[] = await getManager().query(query);
    if(Array.isArray(result)){
      // reverse becasue order by desc
      result.reverse()
    }

    // combine with total test
    result = result.concat(await this.runQueryTotalTestTypeInterval(idRef, interval))

    return result;
  }

  async runQueryTotalTestTypeInterval(idRef: string, interval: string){
    let where = [
      `ref_target = '${idRef}'`,
      `"resultSentAt" is not null`
    ]

    if(interval === 'day'){
      let from = dayjs().subtract(2, 'month')
      where.push(`"created_at" >= '${from.format('YYYY-MM-DD')}'`)
    }

    let query = `select
      count(*) as value,
      'Total Test' as group,
      date_trunc('${interval}', sd.created_at)::date::varchar as xaxis
    from
      swab_detail sd
    where
       ${where.join(" and ")}
    group by
      xaxis
    order by xaxis desc`
    
    let result = await getManager().query(query);
    if(Array.isArray(result)){
      // reverse becasue order by desc
      result.reverse()
    }
    return result;
  }
}
