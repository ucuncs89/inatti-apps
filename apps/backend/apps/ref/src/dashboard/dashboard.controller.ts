import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { DashboardService } from './dashboard.service';

@ApiTags('Ref API')
@RequireAuth()
@Controller('dashboard')
export class DashboardController {
  constructor(
    private dashboardService: DashboardService
  ){}

  @Get('/')
  async getDashboard(
    @Query('idref')
    idRef: string,
    @Query('force')
    force?: boolean,
  ){
    try {
      return await this.dashboardService.getDashboard(idRef, force)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
