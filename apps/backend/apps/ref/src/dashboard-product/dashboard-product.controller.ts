import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { BadRequestException, Controller, Get, Param, Query, Response } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from "../helper/auth.decorator";
import { FilterTransactionDTO } from '../transaction/transaction.controller';
import { DashboardProductService } from './dashboard-product.service';

@ApiTags('Ref API')
@RequireAuth()
@Controller('dashboard-product')
export class DashboardProductController {
  constructor(
    private dashboardProduct: DashboardProductService
  ){}

  @Get('/products')
  async getProducts(
    @Query('idref')
    idRef: string
  ){
    try {
      return await this.dashboardProduct.getProducts(idRef)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/products/export/transaction')
  async getProductsExportTransaction(
    @Query('idref')
    idRef: string,
    @Query('filter', new FilterQueryPipe())
    filter: FilterTransactionDTO,
    @Response()
    res:any
  ){
    try {
      return await this.dashboardProduct.getProductsExportTransaction(idRef, filter, res)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/products/export/transaction-perpatient')
  async getProductsExportTransactionPerpatient(
    @Query('idref')
    idRef: string,
    @Query('filter', new FilterQueryPipe())
    filter: FilterTransactionDTO,
    @Response()
    res:any
  ){
    try {
      return await this.dashboardProduct.getProductsExportTransactionPerpatient(idRef, filter, res)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
  

  @Get('/product/:id')
  async getProductTransaction(
    @Param('id')
    productId: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: FilterTransactionDTO
  ){
    try {
      return await this.dashboardProduct.getProductTransaction(+productId, paginate, filter)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/product/:id/export')
  async getProductTransactionExport(
    @Param('id')
    productId: string,
    @Query('filter', new FilterQueryPipe())
    filter: FilterTransactionDTO,
    @Response()
    res:any
  ){
    try {
      return await this.dashboardProduct.getProductTransactionExport(+productId, filter, res)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

}
