import { RefProduct } from '@app/database/entity/ref/ref-product.entity';
import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFormatOrderTrxXlsx, CommonFormatOrderXlsx } from '@app/helper/report-xlsx/common-format';
import { deepValueToXlsx } from '@app/helper/report-xlsx/to-xlsx';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, FindConditions, In, ObjectLiteral, Raw, Repository } from 'typeorm';
import { FilterTransactionDTO } from '../transaction/transaction.controller';

@Injectable()
export class DashboardProductService {
  constructor(
    @InjectRepository(RefProduct)
    private RefProductRepository: Repository<RefProduct>,
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
  ){}

  async getTotalTrxProduct(productId: number){
    let result:any = await this.SwabOrderRepository.createQueryBuilder('so')
      .select('sum(price) as total')
      .where('product_swab_id = :productId', {productId})
      .andWhere('status = \'paid\'')
      .addGroupBy('product_swab_id')
    console.log(result.getQuery())
    result = await result.getRawOne()
    console.log(result);
      
    if(!result || !result.total) return null
    console.log(`${productId} total : ${result}`)
    return result.total
  }

  async getProducts(idRef: string) {
    let results = await this.RefProductRepository.find({
      where: {
        ref: {
          id: idRef
        }
      },
      relations: [
        'product',
      ]
    })

    for (const result of results) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //@ts-ignore
      result.total = await this.getTotalTrxProduct(result.id)
    }
    return results
  }

  getWhereParam(filter:FilterTransactionDTO){
    let whereParam:FindConditions<SwabOrderTrx>[] | FindConditions<SwabOrderTrx> | ObjectLiteral | string = {}

    if(filter.idProduct){
      whereParam.product = {
        id: +filter.idProduct
      }
    }

    if(filter.method){
      if(filter.method.split(",").length < 1){
        whereParam.method = filter.method
      } else {
        const listMethod = filter.method.split(",").map((v) => v)
        whereParam.method = In(listMethod)
      }
    }

    if(filter.search){
      whereParam.invId = filter.search
    }
    if(filter.rangedate){
      whereParam.createdAt = filter.rangedate
    }
    if(filter.date){
      // it should using raw query
      whereParam.createdAt = Raw((alias) => `"SwabOrderTrx".created_at::date > now() - interval '${filter.date.value}'`)
    }
    if(filter.status){
      whereParam.status = filter.status
    }
    if(filter.idRef){
      whereParam.reference = {
        id: filter.idRef
      }
    }
    return whereParam
  }

  relationsSwabOrderTrx = [
    'orderSwab',
    'customer',
    'product',
    'product.product',
    'reference',
    'methodCash',
  ]
  
  async getProductTransaction(idProduct:number,paginate: PaginationOptionsInterface, filter: FilterTransactionDTO){
    filter.idProduct = idProduct
    let whereParam = this.getWhereParam(filter)

    let results = await this.SwabOrderTrxRepository.find({
      take: paginate.take,
      skip: paginate.skip,
      where: whereParam,
      relations: this.relationsSwabOrderTrx,
      order: {
        createdAt: 'DESC'
      }
    })
    let total = await this.SwabOrderTrxRepository.count({
      where: whereParam
    })

    for (let i = 0; i < results.length; i++) {
      const v = results[i];
      v.getTotalOrder()
      if(v.customer){
        v.customer.hideCredential()
      }

      results[i] = v;
    }

    return new Pagination({
      results,
      total,
    }, paginate)
  }

  async getProductsExportTransaction(idRef: string, filter:FilterTransactionDTO, res: any){
    if(!filter) filter = new FilterTransactionDTO()

    filter.idRef = idRef
    let whereParam = this.getWhereParam(filter)
    let results = await this.SwabOrderTrxRepository.find({
      where: whereParam,
      relations: this.relationsSwabOrderTrx,
      order: {
        createdAt: 'DESC'
      }
    })

    for (let i = 0; i < results.length; i++) {
      const v = results[i];
      v.getTotalOrder()
      if(v.customer){
        v.customer.hideCredential()
      }

      results[i] = v;
    }

    await deepValueToXlsx(res, results, CommonFormatOrderTrxXlsx)
  }

  async getProductsExportTransactionPerpatient(idRef: string, filter:FilterTransactionDTO, res: any){
    if(!filter) filter = new FilterTransactionDTO()
    filter.idRef = idRef
    
    this.exportOrderPatient(filter, res)
  }

  getProductTransactionExport(productId: number, filter:FilterTransactionDTO, res:any){
    if(!filter) filter = new FilterTransactionDTO();
    
    filter.idProduct = productId
    this.exportOrderPatient(filter, res)
  }

  async exportOrderPatient(filter:FilterTransactionDTO, res:any){
    let whereParam = this.getWhereParam(filter)
    let results = await this.SwabOrderRepository.find({
      where: whereParam,
      relations: [
        'detailSwab',
        'detailSwab.patient',
        'detailSwab.refTarget',
        'detailSwab.labTarget',
        'orderTrx',
        'reference',
        'product',
        'product.product',
        'methodCash'
      ],
      order:{
        createdAt:'DESC'
      }
    })

    await deepValueToXlsx(res, results, CommonFormatOrderXlsx)
  }
}
