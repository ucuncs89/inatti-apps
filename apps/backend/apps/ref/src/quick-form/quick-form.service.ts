import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefQuickForm } from '@app/database/entity/ref/ref-quick-form.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class QuickFormService {
  constructor(
    @InjectRepository(RefQuickForm)
    private RefQuickFormRepository: Repository<RefQuickForm>,
  ){}

  list(refid?: string){
    return this.RefQuickFormRepository.find({
      where: {
        ref: {
          id: refid
        }
      }
    })
  }

  getDetail(id?: string){
    return this.RefQuickFormRepository.findOne({ id })
  }

  delete(id: string, refId:string){
    return this.RefQuickFormRepository.delete({
      id,
    })
  }

  create(payload: RefQuickForm){
    return this.RefQuickFormRepository.save(payload)
  }
}
