import { RefQuickForm } from '@app/database/entity/ref/ref-quick-form.entity';
import { Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { QuickFormService } from './quick-form.service';

@ApiTags('Ref API')
@RequireAuth()
@Controller('quick-form')
export class QuickFormController {
  constructor(
    private quickFormService: QuickFormService,
  ){}

  @Get()
  listQuickForm(
    @Query('refId')
    refId: string
  ){
    return this.quickFormService.list(refId)
  }

  @Get('/:id')
  getDetail(
    @Param('id')
    id: string
  ){
    return this.quickFormService.getDetail(id)
  }

  @Delete('/:id')
  delete(
    @Param('id')
    id:string,
    @Query("refId")
    refId:string
  ){
    return this.quickFormService.delete(id, refId)
  }

  @Post()
  create(
    @Body()
    body:RefQuickForm,
  ){
    return this.quickFormService.create(body)
  }
}
