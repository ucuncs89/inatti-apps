import { ApiProperty } from "@nestjs/swagger";

export class SendNotificationDTO{
  @ApiProperty()
  listIdSwab: number[]
  @ApiProperty({
    required: false
  })
  isForce?: boolean
}