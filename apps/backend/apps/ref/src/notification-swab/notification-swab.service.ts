import { SwabDetail } from "@app/database/entity/swab-data/swab-detail.entity";
import { StatusNotifWA, SwabNotificationWhatsapp } from "@app/database/entity/swab-notification/swab-notify-wa.entity";
import { WhatsappNotifyService } from "@app/email-service/whatsapp-service/whatsapp/whatsapp-notify.service";
import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { NumberFormat } from "xlsx";

@Injectable()
export class NotificationSwabService {
  constructor(
    @InjectRepository(SwabNotificationWhatsapp)
    private SwabNotificationWhatsappRepository: Repository<SwabNotificationWhatsapp>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    private whatsappNotifyService: WhatsappNotifyService
  ){}

  logger = new Logger('NotificationSwabService')

  async sendNotify(idSwab: NumberFormat, isForce?: boolean): Promise<SwabNotificationWhatsapp>{
    let swab = await this.SwabDetailRepository.findOne(idSwab)
    if(!swab){
      throw "Swab not found"
    }
    
    let statusNotify = await this.SwabNotificationWhatsappRepository.findOne({where: {swabDetail: swab}})

    if(statusNotify && !isForce){
      return statusNotify
    }

    try{
      let notify = await this.whatsappNotifyService.notifyPatient(swab.id)
      return await this.setStatusNotify({
        swabDetail: swab,
        message: notify.responseMessage,
        status: notify.status,
        messageId: notify.messageId
      })

    } catch(err){
      this.logger.error(err)
      let message
      if(typeof err !== 'object'){
        message = err
      } else {
        message = err.message
      }
      return await this.setStatusNotify({
        swabDetail: swab,
        message: 'Error: ' + message,
        status: StatusNotifWA.FAILED,
      })
    }
  }

  async setStatusNotify(status: SwabNotificationWhatsapp): Promise<SwabNotificationWhatsapp>{
    let swabNotification = await this.SwabNotificationWhatsappRepository.findOne({where: {swabDetail: status.swabDetail}})
    if(!swabNotification){
      swabNotification = new SwabNotificationWhatsapp()
    }
    Object.assign(swabNotification, status)
    
    return this.SwabNotificationWhatsappRepository.save(swabNotification)
  }

  async fetchStatusNotify(idSwab: number): Promise<SwabNotificationWhatsapp>{
    let status = await this.SwabNotificationWhatsappRepository.findOne({
      where: {
        swabDetail: idSwab
      },
      relations: ['swabDetail']
    })
    if(!status){
      throw "Status whatsapp not found"
    }

    if(!status.messageId){
      throw "No message id found, maybe message not sended"
    }

    let result = await this.whatsappNotifyService.checkStatusWhatsapp(status.messageId)

    return await this.setStatusNotify({
      swabDetail: status.swabDetail,
      message: result.responseMessage,
      status: result.status,
      messageId: result.messageId
    })
  }
}