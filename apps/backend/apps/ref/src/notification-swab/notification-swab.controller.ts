import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { LoggerActivityService } from '@app/logger-activity';
import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { NotificationSwabService } from './notification-swab.service';
import { SendNotificationDTO } from './type';

@ApiTags('Ref API')
@RequireAuth()
@Controller('notification-swab')
export class NotificationSwabController {
  constructor(
    private notificationSwabService: NotificationSwabService,
    private activity: LoggerActivityService
  ){}

  @Post('send-notification')
  async sendNotification(
    @Body()
    body: SendNotificationDTO,
    @UserCred() user: User
  ){
    try{
      if(!body.listIdSwab || body.listIdSwab.length < 0){
        return;
      }
      let responses = []
      for (let i = 0; i < body.listIdSwab.length; i++) {
        const idSwab = body.listIdSwab[i];
        let response = await this.notificationSwabService.sendNotify(idSwab, body.isForce) 
        responses.push(response)

        await this.activity.log({
          type: this.activity.activityList.ref.sendNotificationWhatsapp,
          swabDetail: idSwab,
          user: user.id,
          params: {
            responseWhatsapp: response.status
          }
        })
      }
      return responses;
    }catch(err){
      throw new BadRequestException(err)
    }
  }

  @Post('check-notification')
  async checkNotification(
    @Body()
    body: SendNotificationDTO,
    @UserCred() user: User
  ){
    try{
      if(!body.listIdSwab || body.listIdSwab.length < 0){
        return;
      }
      let responses = []
      for (let i = 0; i < body.listIdSwab.length; i++) {
        const idSwab = body.listIdSwab[i];
        let response = await this.notificationSwabService.fetchStatusNotify(idSwab) 
        responses.push(response)
        
        await this.activity.log({
          type: this.activity.activityList.ref.checkNotificationWhatsapp,
          swabDetail: idSwab,
          user: user.id,
          params: {
            responseWhatsapp: response.status
          }
        })
      }
      return responses;
    }catch(err){
      throw new BadRequestException(err)
    }
  }

}
