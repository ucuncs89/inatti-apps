import { UserNotFoundException } from '@app/authentication/exceptions';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { RefRole } from '@app/database/entity/ref/ref-role.entity';
import { UsernameExistException } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { UserManageRefDTO, UserManageUpdateDTO } from './type';

@Injectable()
export class UserManageService {
  constructor(
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(RefRole)
    private RefRoleRepository: Repository<RefRole>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ) { }

  queryUserWithRole() {
    return this.RefRoleUserRepository.createQueryBuilder('ru')
      .select([
        'ru.id',
        'u.id',
        'u.fullname',
        'u.username',
        'u.email',
        'r.id',
        'r.name',
        'r.description'
      ])
      .leftJoin('ru.userId', 'u')
      .leftJoin('ru.roleId', 'r')
  }

  getUserList(idRef: string) {
    let query = this.queryUserWithRole()
      .where('ru.refId = :refId', { refId: idRef })
    return query.getMany();
  }

  getUserDetail(idRef: string, idroleuser: string) {
    let query = this.queryUserWithRole()
      .addSelect(['r.permission'])
      .where('ru.refId = :refId', { refId: idRef })
      .andWhere('ru.id = :idroleuser', { idroleuser })
    return query.getOne();
  }

  getRolesRef() {
    return this.RefRoleRepository.find();
  }

  async isUsernameExist(username: string) {
    let user = await this.UserRepository.find({ where: { username } })
    if (user.length > 0) {
      throw new UsernameExistException();
    }
  }

  async addUser(idRef: string, userManageDTO: UserManageRefDTO) {
    await this.isUsernameExist(userManageDTO.username)
    return getManager().transaction(async entity => {
      let user = new User()
      user.fullname = userManageDTO.fullname
      user.username = userManageDTO.username
      user.password = userManageDTO.password
      user.email = userManageDTO.email

      user = await entity.save(user)

      let refRoleUser = new RefRoleUser();
      refRoleUser.userId = user
      refRoleUser.roleId = new RefRole(userManageDTO.roleId)
      refRoleUser.refId = new RefDetail(idRef)
      await entity.save(refRoleUser)
      return true;
    })
  }

  async updateUser(idroleuser: number, userManageUpdateDTO: UserManageUpdateDTO) {
    try {
      let user: User = await this.UserRepository.findOne({ id: userManageUpdateDTO.userId })
      if (!user) {
        throw new UserNotFoundException();
      }
      // check username
      if (user.username !== userManageUpdateDTO.username) {
        await this.isUsernameExist(userManageUpdateDTO.username);
      }

      // save data
      user.fullname = userManageUpdateDTO.fullname
      user.username = userManageUpdateDTO.username
      user.email = userManageUpdateDTO.email
      if (userManageUpdateDTO.password) {
        user.password = userManageUpdateDTO.password
        await user.hashPassword() // manual hash
      }
      await this.UserRepository.save(user)

      if (userManageUpdateDTO.roleId) {
        await this.RefRoleUserRepository.update(idroleuser, {
          roleId: new RefRole(userManageUpdateDTO.roleId)
        })
      }
      return true;
    } catch (error) {
      throw error;
    }
  }

  async deleteUser(idRef: string, idUser: string) {
    let creteria = {
      userId: new User(idUser),
      refId: new RefDetail(idRef)
    }
    let result = await this.RefRoleUserRepository.find(creteria)
    if (!result.length) {
      throw new Error(`id user ${idUser} not found`)
    }
    await this.RefRoleUserRepository.delete(creteria)
    await this.UserRepository.delete({ id: idUser })
  }
}
