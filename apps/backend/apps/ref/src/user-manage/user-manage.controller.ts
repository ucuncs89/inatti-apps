import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { UserManageRefDTO, UserManageUpdateDTO } from './type';
import { UserManageService } from './user-manage.service';

@ApiTags('Ref API')
@RequireAuth()
@Controller('user-manage')
export class UserManageController {
  constructor(
    private userManageService: UserManageService
  ) { }

  @Get('/roles')
  async getRolesLab() {
    try {
      return await this.userManageService.getRolesRef()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/users')
  async getUserList(
    @Query('idref')
    idRef: string
  ) {
    try {
      return await this.userManageService.getUserList(idRef)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/user/:idroleuser')
  async getUserDetail(
    @Query('idref')
    idRef: string,
    @Param('idroleuser')
    idroleuser: string
  ) {
    try {
      return await this.userManageService.getUserDetail(idRef, idroleuser)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/user')
  async addUser(
    @Query('idref')
    idref: string,
    @Body()
    userManageDTO: UserManageRefDTO
  ) {
    try {
      return await this.userManageService.addUser(idref, userManageDTO)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Patch('/user/:idroleuser')
  async updateUser(
    @Param('idroleuser')
    idroleuser: number,
    @Body()
    userManageUpdateDTO: UserManageUpdateDTO
  ) {
    try {
      return await this.userManageService.updateUser(idroleuser, userManageUpdateDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Delete('/user/:idUser')
  async deleteUser(
    @Query('idref')
    idref: string,
    @Param('idUser')
    idUser: string,
  ) {
    try {
      return await this.userManageService.deleteUser(idref, idUser)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }
}
