import { UserManageDTO } from "@app/database/entity/user/user.dto";
import { ApiProperty } from "@nestjs/swagger"

export class UserManageRefDTO extends UserManageDTO {}

export class UserManageUpdateDTO extends UserManageRefDTO {
  @ApiProperty()
  userId: string;
}