import { Controller, Get, Post, Body, Request, UseGuards, Param, BadRequestException } from '@nestjs/common';
import { RefService } from './ref.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { LoginDTO } from '@app/authentication/types';
import { RequireAuth } from './helper/auth.decorator';

@ApiTags('Reference Auth API')
@Controller()
export class RefController {
  constructor(private readonly refService: RefService) { }

  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard('ref'))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @RequireAuth()
  @Get('/registered-ref')
  async registeredRef(@Request() req) {
    let listRef = await this.refService.getRegisteredRefUser(req.user)
    return listRef
  }

  @RequireAuth()
  @Get('/registered-ref/:id')
  async registeredRefById(
    @Param('id') idRef: string,
    @Request() req
  ) {
    let listRef = await this.refService.getRegisteredRefUserById(req.user, idRef)
    return listRef
  }

  // Get lab with same tenant id
  @RequireAuth()
  @Get('/labs/:idref')
  async getListLab(
    @Param('idref')
    idRef: string
  ) {
    return await this.refService.getListLab(idRef);
  }

  @RequireAuth()
  @Get('/role-user/:idref')
  async getRoleUser(
    @Param('idref')
    idRef: string,
    @Request()
    req: any
  ) {
    try {
      return await this.refService.getRoleUser(req.user.id, idRef)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
