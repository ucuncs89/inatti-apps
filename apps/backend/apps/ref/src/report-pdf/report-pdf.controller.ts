import { ReportPdfService, TypeTestCovid } from '@app/report-pdf';
import { LocalPdfAuthService } from '@app/report-pdf/local-pdf-auth/local-pdf-auth.service';
import { BadRequestException, Controller, Get, Logger, Query, Request, Response, UseGuards } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import * as Express from 'express';
import { RequireAuth } from '../helper/auth.decorator';
import { RefReportService } from './ref-report.service';

@ApiTags("Ref API")
@Controller('report-pdf')
export class ReportPdfController {
  constructor(
    private refReportService: RefReportService,
    private reportPdfService: ReportPdfService,
  ) { }
  logger = new Logger('Ref/ReportPdfController')

  @RequireAuth()
  @Get('download-sertifikat')
  async downloadCertificate(
    @Query('idref')
    idRef: string,
    @Query('idswab')
    idSwab: string,
    @Query('type')
    type: TypeTestCovid,
    @Response()
    res: Express.Response
  ) {
    try {
      await this.refReportService.downloadSertificate({
        type,
        idSwab,
        idRef,
      }, res)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @ApiQuery({
    name: 'idswab',
    description: 'can be separate using comma. Ex: 31,32,33'
  })
  @ApiQuery({
    name: "type",
    type: String,
    description: "Type option : 'pcr' or 'antigen'"
  })
  @RequireAuth()
  @Get('download-sertifikat/zip')
  async downloadCertificateZip(
    @Query('idref')
    idRef: string,
    @Query('idswab')
    idSwab: string,
    @Query('type')
    type: TypeTestCovid,
    @Response()
    res: Express.Response,
    @Request()
    req: Express.Request
  ) {
    req.setTimeout(360000) // 5 minutes
    let listIdSwab = idSwab.split(',')

    this.logger.log(`Download certificate zip, total id swab : ${listIdSwab.length}`)

    try {
      const MAX_DATA = 20

      if(listIdSwab.length > MAX_DATA){
        throw `Maksimal ${MAX_DATA} data untuk di generate`
      }
      return await this.reportPdfService.ResultTestZip({
        type,
        idSwab: listIdSwab,
        idSrc: idRef,
      }, res)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @UseGuards(LocalPdfAuthService)
  @Get('swab-detail/pcr')
  async getSwabDetailPcr(
    @Query('idswab')
    idSwab: number,
  ) {
    try {
      return await this.refReportService.getSwabDetailPcr(idSwab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @UseGuards(LocalPdfAuthService)
  @Get('swab-detail/antigen')
  async getSwabDetailAntigen(
    @Query('idswab')
    idSwab: number,
  ) {
    try {
      return await this.refReportService.getSwabDetailAntigen(idSwab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @UseGuards(LocalPdfAuthService)
  @Get('template')
  async getTemplatePdf(
    @Query('idref')
    idref: string,
    @Query('typeProduct')
    typeProduct: string,
  ){
    try {
      return await this.refReportService.getTemplatePdf(idref, +typeProduct)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
