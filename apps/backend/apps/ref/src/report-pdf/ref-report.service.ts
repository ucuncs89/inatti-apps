import { LabAnalyzeSessionDetail } from '@app/database/entity/mapping/analyze/lab-analyze-session-detail.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { RefTemplateSertifikat } from '@app/database/entity/template-map/ref-template-sertifikat.entity';
import { ResponseFile } from '@app/helper/response-file/response-file';
import { ReportPdfService, TypeTestCovid } from '@app/report-pdf';
import { Injectable, Logger, StreamableFile } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import { Repository } from 'typeorm';
import * as archiver from 'archiver';

@Injectable()
export class RefReportService {
  constructor(
    @InjectRepository(LabAnalyzeSessionDetail)
    private LabAnalyzeSessionDetailRepository: Repository<LabAnalyzeSessionDetail>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(RefTemplateSertifikat)
    private RefTemplateSertifikatRepository: Repository<RefTemplateSertifikat>,
    private reportPdfService: ReportPdfService
  ) { }

  logger = new Logger('RefReportService')

  async downloadSertificate(params: { type: TypeTestCovid, idRef: string, idSwab: string }, response: Response) {
    let src: any = 'ref'
    let idSrc = params.idRef
    const filePdf: Buffer = await this.reportPdfService.ResultTest({
      type: params.type,
      src,
      idSrc,
      idSwab: params.idSwab,
    })
    ResponseFile(response, {
      type: 'application/pdf',
      buffer: filePdf
    })
  }

  getSwabDetailPcr(idSwab: number) {
    return this.SwabDetailRepository.findOne({
      where: {
        id: idSwab
      },
      relations: [
        'patient',
        'orderSwab',
        'refTarget',
        'labTarget',
        'labAnalyzeSession',
        'labAnalyzeSession.resultTool',
      ],
      withDeleted: true
    })
  }

  getSwabDetailAntigen(idSwab: number) {
    return this.SwabDetailRepository.findOne({
      where: {
        id: idSwab,
      },
      relations: [
        'patient',
        'orderSwab',
        'refTarget',
      ],
      withDeleted: true
    })
  }

  getTemplatePdf(idRef: string, typeProduct: number) {
    return this.RefTemplateSertifikatRepository.findOne({
      where: {
        ref: {
          id: idRef
        },
        typeProduct: {
          id: typeProduct
        }
      },
      relations: [
        'ref'
      ],
      withDeleted: true
    })
  }
}
