import { StatusOrderEnum } from '@app/database/entity/swab-data/type';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { BadRequestException, Body, Controller, Get, Param, Patch, Query, Request, Response } from '@nestjs/common';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { VaksinService } from 'apps/data/src/vaksin/vaksin.service';
import { RequireAuth } from '../helper/auth.decorator';
import { TransactionService } from './transaction.service';

export class FilterTransactionDTO extends CommonFilterDTO {
  @ApiProperty({ name: 'filter[idProduct]', required: false })
  idProduct?: number
  @ApiProperty({ name: 'filter[method]', required: false })
  method?: string
  @ApiProperty({ name: 'filter[status]', required: false })
  status?: string
  // added later
  idRef?: string
}

export class TransactionEditDTO {
  @ApiProperty()
  listTrxId?: string[]
  @ApiProperty()
  status: StatusOrderEnum
  @ApiProperty({ description: 'Method cash id get from /method-cash' })
  methodCash: number
}

@ApiTags('Ref API')
@RequireAuth()
@Controller('transaction')
export class TransactionController {
  constructor(
    private transactionService: TransactionService,
  ) { }

  @Get('/list')
  async getListTransaction(
    @Query('idref')
    idRef: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: FilterTransactionDTO
  ) {
    try {
      return await this.transactionService.getListTransaction(idRef, paginate, filter)
    } catch (error) {
      console.log(error);

      throw new BadRequestException(error)
    }
  }

  @Get('/detail/:id')
  async getDetailTransaction(
    @Query('idref')
    idRef: string,
    @Param('id')
    idOrder: string
  ) {
    try {
      let res: any = await this.transactionService.getDetailTransaction(idRef, idOrder)
      return res;
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error.message)
    }
  }

  @Patch('/edit-status/bulk')
  async updateTransaction(
    @Body()
    body: TransactionEditDTO,
    @Request()
    req: any
  ) {
    try {
      if (body.listTrxId.length < 1) {
        throw "Transaction id not found"
      }
      for (const idTrx of body.listTrxId) {
        await this.transactionService.updateTransactionStatus(idTrx, body, req.user.id)
      }
      return true;
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/print-invoice/:invId')
  async printInvoice(
    @Param('invId')
    invId: string,
    @Response()
    res: any
  ) {
    try {
      return await this.transactionService.getInvoicePdf(invId, res)
    } catch (err) {
      throw new BadRequestException(err)
    }
  }

  @Get('/method-cash')
  async getMethodCash() {
    try {
      return await this.transactionService.getMethodCash()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
