import { isProductTestCovid, isProductVaksin, ProductTypeId } from '@app/database/entity/ref/const';
import { RefProduct } from '@app/database/entity/ref/ref-product.entity';
import { MethodCash } from '@app/database/entity/swab-data/method-cash.entity';
import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { PatientRelationsAll, SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { ResponseFile } from '@app/helper/response-file/response-file';
import { LoggerActivityService } from '@app/logger-activity';
import { ReportPdfService } from '@app/report-pdf';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VaksinService } from 'apps/data/src/vaksin/vaksin.service';
import { Response } from 'express';
import { get, omit } from 'lodash';
import { Between, In, Raw, Repository } from 'typeorm';
import { FilterTransactionDTO, TransactionEditDTO } from './transaction.controller';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(RefProduct)
    private RefProductRepository: Repository<RefProduct>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    private reportPdfService: ReportPdfService,
    @InjectRepository(MethodCash)
    private MethodCashRepository: Repository<MethodCash>,
    private activity: LoggerActivityService,
    private vaksinService: VaksinService,
  ) { }

  async getListTransaction(idRef: string, paginate: PaginationOptionsInterface, filter: FilterTransactionDTO) {
    let whereParam: any = {
      reference: {
        id: idRef,
      },
    }

    if (filter.method) {
      if (filter.method.split(",").length < 1) {
        whereParam.method = filter.method
      } else {
        const listMethod = filter.method.split(",").map((v) => v)
        whereParam.method = In(listMethod)
      }
    }

    if (filter.search) {
      whereParam.invId = filter.search
    }

    if (filter.rangedate) {
      whereParam.createdAt = filter.rangedate
    }
    if (filter.date) {
      whereParam.createdAt = filter.date
    }

    let results = await this.SwabOrderTrxRepository.find({
      take: paginate.take,
      skip: paginate.skip,
      where: whereParam,
      relations: [
        'orderSwab',
        'customer',
        'product',
        'product.product',
        'reference',
        'methodCash',
      ],
      order: {
        createdAt: 'DESC'
      }
    })
    let total = await this.SwabOrderTrxRepository.count({
      where: whereParam
    })

    for (let i = 0; i < results.length; i++) {
      const v = results[i];
      v.getTotalOrder()
      if (v.customer) {
        v.customer.hideCredential()
      }

      results[i] = v;
    }

    return new Pagination({
      results,
      total,
    }, paginate)
  }
  async getDetailTransaction(idRef: string, idOrder: string) {
    let result = await this.SwabOrderTrxRepository.findOne({
      relations: [
        'customer',
        'reference',
        'product',
        'orderSwab',
        'methodCash'
      ],
      where: {
        reference: idRef,
        id: idOrder
      }
    })
    if (result.customer) {
      result.customer.hideCredential();
    }
    await this.bindDetailProduct(result)
    return result;
  }

  /**
   * @deprecated This will changed by transactionService at apps/backend/apps/data/src/transaction/transaction-order.service.ts
   */
  async bindDetailProduct(orderTrx: SwabOrderTrx) {
    return Promise.all(orderTrx.orderSwab.map((orderData, index) => {
      return new Promise(async (resolve, reject) => {
        try {
          let injectProperty: InjectPropertyOrder
          const productId = orderTrx.product.product.id
          if (!productId) reject(new BadRequestException("No product found"))
          if (!Object.values(ProductTypeId).includes(productId)) {
            console.error(`${productId} not support yet, please provide it`)
            resolve(null)
          }

          if (isProductTestCovid(productId)) {
            injectProperty = await this.bindDetailTestcovid(orderData)
          }
          if (isProductVaksin(productId)) {
            injectProperty = await this.vaksinService.injectVaksinToOrder(orderData)
          }
          if (get(injectProperty, 'patient.ownedBy')) {
            injectProperty.patient.ownedBy.hideCredential()
          }
          resolve(Object.assign(orderData, injectProperty))
        } catch (err) {
          reject(err)
        }
      })
    }))
  }

  async bindDetailTestcovid(order: SwabOrder): Promise<InjectPropertyOrder> {
    let swabDetail = await this.SwabDetailRepository.findOne({
      where: {
        orderSwab: order
      },
      relations: [
        'patient',
        ...PatientRelationsAll('patient')
      ]
    })
    if (!swabDetail) return null

    const { patient, ...detailOrderProduct } = swabDetail
    let injectData: InjectPropertyOrder = {
      patient,
      detailOrderProduct,
    }
    return injectData
  }

  async updateTransactionStatus(idTrx: string, params: TransactionEditDTO, userId: string) {
    let trx = await this.SwabOrderTrxRepository.findOne({
      where: {
        id: idTrx
      },
      relations: [
        'orderSwab',
        'orderSwab.detailSwab'
      ]
    })
    if (!trx) throw "Transaction not found"

    if (params.status !== 'paid') {
      params.methodCash = null
    }

    trx.status = params.status
    trx.methodCash = new MethodCash(params.methodCash)
    await this.SwabOrderTrxRepository.save(trx)
    for (const trxOrder of trx.orderSwab) {
      trxOrder.status = params.status
      trxOrder.methodCash = new MethodCash(params.methodCash)
      await this.SwabOrderRepository.save(trxOrder)

      await this.activity.log({
        type: this.activity.activityList.ref.updateStatusOrder,
        params: {
          orderTrxId: trxOrder.id,
          status: params.status,
        },
        swabDetail: trxOrder.detailSwab,
        user: userId
      })
    }
    return true;
  }

  async getInvoicePdf(invId: string, res: Response) {
    let orderTrx = await this.SwabOrderTrxRepository.findOne({
      where: {
        invId,
      }
    })
    if (!orderTrx) throw "Invoice id not found"

    let bufferPdf = await this.reportPdfService.InvoicePage(invId)

    ResponseFile(res, {
      type: 'application/pdf',
      buffer: bufferPdf
    })
  }

  async getMethodCash() {
    return await this.MethodCashRepository.find()
  }
}

export interface InjectPropertyOrder {
  patient: SwabPatient,
  detailOrderProduct: {
    id: string | number
    [key: string]: any
  }
}