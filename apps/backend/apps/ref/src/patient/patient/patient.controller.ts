import { PatientDTO, QuerySearchPatient } from 'apps/ref/src/patient-swab/type';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { PatientService } from '../../patient-swab/patient.service';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';

/**
 * WARNING !!!
 * This file is old version of API patient. to new API move to API patient module at 
 * apps/data/src/patient/patient.module.ts
 * 
 * This only used for ref project
 */

@ApiTags('Ref API')
@RequireAuthApp(AppsEnum.Ref)
@Controller()
export class PatientController {
  constructor(
    private patientService: PatientService
  ) { }

  @ApiOperation({ description: "Get all patient" })
  @Get('patients')
  async getPatient(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    try {
      return await this.patientService.findAll(paginate, filter)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @ApiOperation({ description: "Get patient by id" })
  @Get('patient/:idpatient')
  async getPatientById(
    @Param('idpatient')
    idPatient: string,
    @Query('refId')
    refId: string
  ) {
    try {
      return await this.patientService.find(idPatient, refId);
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * This API created after getPatientById,
   * getPatientById already used on ref app, if change that, maybe cause problem on frontend
   */
  @Get('patient-detail/:idpatient')
  async getPatientDetail(
    @Param('idpatient')
    idPatient: string,
  ){
    return await this.patientService.findOne(idPatient);
  }

  @Get('patient-search/')
  async getPatientSearch(
    @Query()
    querySearch: QuerySearchPatient
  ) {
    try {
      return await this.patientService.search(querySearch)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @ApiOperation({ description: "Add patient" })
  @Post('patient/')
  async addPatient(
    @Body()
    patientDTO: PatientDTO
  ) {
    try {
      return await this.patientService.create(patientDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @ApiOperation({ description: "Edit patient" })
  @Patch('patient/:idpatient')
  async editPatient(
    @Param('idpatient')
    idPatient: string,
    @Body()
    patientDTO: PatientDTO
  ) {
    try {
      return await this.patientService.edit(idPatient, patientDTO)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Delete('patient/:idpatient')
  async deletePatient(
    @Param('idpatient')
    idPatient: string
  ) {
    try {
      return await this.patientService.delete(idPatient)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('patient-owned-by/:idpatient')
  async setOwner(
    @Param('idpatient')
    patientId: string,
    @Query('targetUser')
    targetUser: string
  ){
    return await this.patientService.assignToUser(patientId, targetUser)
  }
}