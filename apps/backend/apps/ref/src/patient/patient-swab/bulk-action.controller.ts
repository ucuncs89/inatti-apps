import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { LoggerActivityService } from '@app/logger-activity';
import { BadRequestException, Body, Controller, Patch } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../../helper/auth.decorator';
import { PatientSwabService } from '../../patient-swab/patient-swab.service';
import { ListResultBulkDto, SendSwabToLabBulkDto } from '../../patient-swab/type';

@ApiTags('Ref API')
@RequireAuth()
@Controller('patient-swab-bulk')
export class RefPatientSwabBulkController {
  constructor(
    private patientSwabService:PatientSwabService,
    private activity: LoggerActivityService
  ){}

  @Patch('checkin')
  async BulkCheckin(
    @Body()
    listIdSwab: string[],
    @UserCred() user: User
  ){
    try {
      if(listIdSwab.length < 1) throw "List id not found"
      for (const swabId of listIdSwab) {
        await this.patientSwabService.checkinPatient(swabId)
        await this.activity.log({
          type: this.activity.activityList.ref.updateStatusSwab,
          params: {
            status: "Checkin"
          },
          user: user.id,
          swabDetail: swabId
        })
      }
      return true
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('swab-now')
  async BulkSwabnow(
    @Body()
    listIdSwab: string[],
    @UserCred() user: User,
  ){
    try {
      if(listIdSwab.length < 1) throw "List id not found"
      for (const swabId of listIdSwab) {
        await this.patientSwabService.swabNow(swabId)
        await this.activity.log({
          type: this.activity.activityList.ref.updateStatusSwab,
          params: {
            status: "Swab now"
          },
          user: user.id,
          swabDetail: swabId
        })
      }
      return true
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('set-result')
  async setResult(
    @Body()
    body: ListResultBulkDto,
    @UserCred()
    user: User
  ){
    try {
      if(!body) throw "Data emtpy"
      if(body.listSample.length < 1) throw "list id sample not found"

      this.patientSwabService.idUserCurrent = user.id
      for (let i = 0; i < body.listSample.length; i++) {
        const idSample = body.listSample[i];
        await this.patientSwabService.swabResultPatient(idSample, {
          resultSwab: body.result
        })

        await this.activity.log({
          type: this.activity.activityList.ref.updateResultSwab,
          params: {
            result: body.result
          },
          user: user.id,
          swabDetail: idSample
        })
      }
      return true
    } catch (error) {
      console.log(error);
      
      throw new BadRequestException(error)
    }
  }

  @Patch('swab-to-lab')
  async sendToLab(
    @Body()
    body: SendSwabToLabBulkDto,
    @UserCred()
    user: User,
  ){
    try {
      if(!body) throw "Data empty"
      if(body.listSample.length < 1) throw "list id sample not found"
      for (const idSample of body.listSample) {
        await this.patientSwabService.sendSwabToLab(idSample, body.labTarget)

        await this.activity.log({
          type: this.activity.activityList.ref.sentToLab,
          params: {
            labId: body.labTarget
          },
          user: user.id,
          swabDetail: idSample
        })        
      }
      return true
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

}