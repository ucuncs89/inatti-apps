import { PatientSwabService } from 'apps/ref/src/patient-swab/patient-swab.service';
import { PatientService } from 'apps/ref/src/patient-swab/patient.service';
import { OrderUpdateDTO, PatientSwabDTO, SwabToLabDTO, UpdateSwabResultDTO } from 'apps/ref/src/patient-swab/type';
import { BadRequestException, Body, Controller, Get, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { StatusSwabEnum } from '@app/database/entity/swab-data/type';
import { RequireAuth } from '../../helper/auth.decorator';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { LoggerActivityService } from '@app/logger-activity';

/**
 * Controller for patient swab activity detail. NOT for patient information.
 */
@ApiTags('Ref API')
@RequireAuth()
@Controller('patient-swab')
export class RefPatientSwabController {
  constructor(
    private patientSwabService: PatientSwabService,
    private patientService: PatientService,
    private activity: LoggerActivityService,
  ) { }


  @Get('/pcr')
  async listPatienSwabPCR(
    @Query('refId')
    refId: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    if (!refId) {
      throw new BadRequestException('Need ref id as query')
    }
    this.patientSwabService.referenceId = refId;
    return await this.patientSwabService.findAllPatient(paginate, filter, 1)
  }

  @Get('/antigen')
  async listPatienSwabAntigen(
    @Query('refId')
    refId: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    if (!refId) {
      throw new BadRequestException('Need ref id as query')
    }
    this.patientSwabService.referenceId = refId;
    return await this.patientSwabService.findAllPatient(paginate, filter, 2)
  }

  @ApiQuery({
    name: "status",
    description: "Status berupa string, jika banyak dipisah dengan koma. Ex: foo,bar"
  })
  @Get('/status')
  async listPatienSwabFilter(
    @Query('refId')
    refId: string,
    @Query('status')
    statusFilter: string,
    @Query('paymentStatus')
    paymentStatus: string,
    @Query('product')
    typeProduct: number,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    if (!refId) {
      throw new BadRequestException('Need ref id as query')
    }

    this.patientSwabService.referenceId = refId;
    return await this.patientSwabService.findAllPatient(paginate, filter, typeProduct, {
      statusFilter,
      paymentStatusFilter: paymentStatus,
    })
  }

  @Get('/:idswab')
  async getPatientById(@Param('idswab') idSwab: string) {
    let swabData = await this.patientSwabService.findPatient(idSwab);
    let patientData = await this.patientService.find(swabData.patient.id)
    swabData.patient = patientData
    return swabData
  }


  @ApiOperation({
    description: "Pastikan data sama dengan schema, dari backend tidak ada validasi data yang banyak saat ini !"
  })
  @Post('register')
  async addPatientSwab(
    @Body() patientSwabDTO: PatientSwabDTO,
    @UserCred() user: User
  ) {
    try{
      let res = await this.patientSwabService.registerPatient(patientSwabDTO, user)

      await this.activity.log({
        swabDetail: res?.id ?? null,
        type: this.activity.activityList.ref.registerPatientSwab,
        user: user.id
      })

      if(res?.id ?? null){
        return res.id
      }
      return true;
    } catch(err){
      console.log(err)
      throw new BadRequestException(err);
    }
  }

  @Patch('checkin/:idswab')
  async checkinPatient(
    @Param('idswab')
    swabId: string,
    @UserCred() user: User,
  ) {
    let res = await this.patientSwabService.checkinPatient(swabId)

    await this.activity.log({
      swabDetail: swabId,
      type: this.activity.activityList.ref.updateStatusSwab,
      params: {
        status: 'Checkin'
      },
      user: user.id
    })

    return res;
  }

  @Patch('swab-now/:idswab')
  async swabDatePatient(
    @Param('idswab')
    swabId: string,
    @UserCred() user: User,
  ) {
    let res = await this.patientSwabService.swabNow(swabId)

    await this.activity.log({
      swabDetail: swabId,
      type: this.activity.activityList.ref.updateStatusSwab,
      params: {
        status: 'Swab Now'
      },
      user: user.id
    })

    return res;
  }

  @Patch('swab-result/:idswab')
  async swabResultPatient(
    @Param('idswab')
    swabId: string,
    @Query()
    updatePatientDTO: UpdateSwabResultDTO,
    @Req()
    request
  ) {
    this.patientSwabService.idUserCurrent = request.user.id;
    let res = await this.patientSwabService.swabResultPatient(swabId, updatePatientDTO);

    await this.activity.log({
      swabDetail: swabId,
      type: this.activity.activityList.ref.updateResultSwab,
      params: {
        result: updatePatientDTO.resultSwab
      },
      user: request.user.id
    })

    return res;
  }

  @Patch('swab-to-lab/:idswab')
  async swabToLab(
    @Param('idswab')
    swabId: string,
    @Body()
    dataDTO: SwabToLabDTO,
    @UserCred() user: User
  ) {
    let res = await this.patientSwabService.sendSwabToLab(swabId, dataDTO.labId);
    if(!res){
      throw new BadRequestException('Failed to send swab to lab')
    }

    await this.activity.log({
      swabDetail: swabId,
      type: this.activity.activityList.ref.sentToLab,
      params: {
        labId: res.labTarget.id
      },
      user: user.id
    })

    return res;
  }

  @Patch('order/:idorder')
  async updateOrderPatient(
    @Param('idorder')
    idOrder: string,
    @Body()
    orderUpdateDTO: OrderUpdateDTO,
    @UserCred() user: User
  ) {
    let res = await this.patientSwabService.updateOrder(idOrder, orderUpdateDTO, user)
    if(!res){
      throw new BadRequestException('Failed to update order')
    }

    return res;
  }

  @Patch('code-booking/:idswab')
  async setCodeBooking(
    @Param('idswab')
    idswab: string,
    @Query('code')
    codeBooking: string,
    @UserCred() user: User
  ){
    let res = await this.patientSwabService.setCodeBooking(idswab, codeBooking)

    await this.activity.log({
      swabDetail: idswab,
      type: this.activity.activityList.ref.updateCodeBooking,
      params: {
        code: codeBooking
      },
      user: user.id
    })

    return res;
  }
}
