import { NestFactory } from '@nestjs/core';
import { ApotekModule } from './apotek.module';

async function bootstrap() {
  const app = await NestFactory.create(ApotekModule);
  await app.listen(3000);
}
bootstrap();
