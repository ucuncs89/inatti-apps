import { JwtAuthGuardApotek } from '@app/authentication/authorization/jwt.authguard.apotek';
import { LoginDTO } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApotekUserService } from 'apps/data/src/apotek/user/apotek-user.service';

@ApiTags('Apotek Auth API')
@Controller()
export class ApotekController {
  constructor(private readonly apotekUserService: ApotekUserService) {}

  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard(AppsEnum.Apotek))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardApotek)
  @Get('/registered-apotek')
  async registeredLab(@Request() req) {
    let listRef = await this.apotekUserService.getUserRole(req.user)
    return listRef
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardApotek)
  @Get('/role-user/:idapotek')
  async getRoleUser(
    @Param('idapotek')
    idApotek: string,
    @Request()
    req: any
  ) {
    try {
      return await this.apotekUserService.getUserRole(req.user.id, idApotek)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
