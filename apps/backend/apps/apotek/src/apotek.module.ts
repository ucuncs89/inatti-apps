import { AccountModule } from '@app/account';
import { AuthenticationModule } from '@app/authentication';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { LoggerActivityModule } from '@app/logger-activity';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ApotekController } from './apotek.controller';
import { AwsModule } from '@app/aws';
import awsConfig from 'config/aws.config';
import { DatabaseModule } from '@app/database';
import { ApotekModule as DataApotekModule } from 'apps/data/src/apotek/apotek.module'

@Module({
  imports: [
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.Apotek }),
    PassportModule,
    AccountModule,
    LoggerActivityModule.register({
      area: AppsEnum.Apotek
    }),
    AwsModule.register({
      bucket: awsConfig().aws.bucket,
      uploadFolder: 'apotek'
    }),
    DataApotekModule
  ],
  controllers: [ApotekController],
  providers: [],
})
export class ApotekModule {}
