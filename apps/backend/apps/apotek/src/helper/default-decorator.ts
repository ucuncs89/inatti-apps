import { JwtAuthGuardApotek } from "@app/authentication/authorization/jwt.authguard.apotek";
import { applyDecorators, Controller, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";

export function ApiApotek(route: string) {
  let tag = ApiTags('Apotek API')
  
  let controller = Controller(route)

  return applyDecorators(
      tag,
      controller,
  )
}

export function ApotekAuth(){
  let tag = ApiTags('Apotek API')
  return applyDecorators(
    UseGuards(JwtAuthGuardApotek),
    ApiBearerAuth(),
    tag,
  )
}