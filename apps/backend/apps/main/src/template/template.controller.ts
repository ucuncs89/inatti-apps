import { LocalPdfAuthService } from '@app/report-pdf/local-pdf-auth/local-pdf-auth.service';
import { BadRequestException, Controller, Get, Param, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { TemplateService } from './template.service';

@Controller('template-api')
export class TemplateController {

  constructor(
    private templateService: TemplateService
  ){}
  
  @ApiBearerAuth()
  @UseGuards(LocalPdfAuthService)
  @Get('invoice-detail/:invId')
  async getInvoiceDetail(
    @Param('invId')
    invoiceId: string,
  ){
    try{
      return await this.templateService.getInvoiceDetail(invoiceId)
    }catch(err){
      throw new BadRequestException(err)
    }
  }
}
