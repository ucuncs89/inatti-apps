import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TemplateService {
  constructor(
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
  ){}

  getInvoiceDetail(invoiceId: string) {
    return this.SwabOrderTrxRepository.findOne({
      where: {
        invId: invoiceId
      },
      relations: [
        'reference',
        'product',
        'product.product',
        'orderSwab',
        'orderSwab.detailSwab',
        'orderSwab.detailSwab.patient',
      ]
    })
  }
}
