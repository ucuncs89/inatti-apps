import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { getAuthSwagger, getDescription, getServerSwagger } from './helper/swagger-helper';
import { LoggerInterceptor } from './interceptor/Logger.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(new LoggerInterceptor())
  app.useGlobalPipes(new ValidationPipe())
  app.enableCors()

  if (process.env.NODE_ENV === 'development') {
    const config = new DocumentBuilder()
      .setTitle('Inatti Documentation')
      .setDescription(await getDescription())
      .setVersion('2.0')

    getAuthSwagger(config)
    getServerSwagger(config)
    
    const document = SwaggerModule.createDocument(app, config.build());
    SwaggerModule.setup('api', app, document);
  }
  await app.listen(process.env.NODE_PORT_BACKEND);
  Logger.log(`App Main jalan di port : ${process.env.NODE_PORT_BACKEND}`, 'RUN APP')
}
bootstrap();
