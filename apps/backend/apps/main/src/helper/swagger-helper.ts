import { AppsEnum } from "@app/database/entity/apps/apps.type"
import { DocumentBuilder } from "@nestjs/swagger"
import { runExec } from "./exec-function"

function getCurrenctBranchGit(){
  return runExec('git rev-parse --abbrev-ref HEAD')
}

function getLatestCommit(){
  return runExec(`git log -1 --pretty='%h %s %d, %ad'`)
}

export async function getDescription(){ 
  let branch = await getCurrenctBranchGit()
  let lastCommit = await getLatestCommit()

  return `
  ## Inatti API Documentation description
  
  - Branch : ${branch}
  - Last Commit : ${lastCommit}
  - NODE_ENV : ${process.env.NODE_ENV}
  `
}

export function getServerSwagger(doc: DocumentBuilder){
  const configServer = [
    ['http://localhost:4000', 'Develop Local'],
    ['https://dev-api.inatti.id', 'Staging'],
    ['https://api.inatti.id', 'Production'],
  ]
  configServer.forEach(conf => {
    doc.addServer(conf[0], conf[1])
  });
}

export function getAuthSwagger(doc: DocumentBuilder){
  let apps = Object.values(AppsEnum)

   // Add old version bearer auth
   doc.addBearerAuth({
    name: ApiV1AuthName,
    type: 'http',
    scheme: 'bearer',
    description: 'Auth V1 JWT',
  })

  apps.forEach(app => {
    doc.addSecurity(getNameAuth(app), {
      type: 'http',
      scheme: 'bearer',
      description: `${app} Auth`,
    })
  })
}

export function getNameAuth(app: AppsEnum){
  return `${app} Auth`
}

export const ApiV1AuthName = 'API V1 Auth'