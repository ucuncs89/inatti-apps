import { exec } from "child_process";

export async function runExec(cmd: string){
  return new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        // node couldn't execute the command
        return;
      }
      if (stderr) {
        // the command had a problem
        return;
      }
      // successful execution
      resolve(stdout)
    })
  })
}