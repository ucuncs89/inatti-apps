import { ChatModule } from '@app/chat';
import { CommonModule } from '@app/common';
import { FileModule } from '@app/file';
import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { AdminModule } from 'apps/admin/src/admin.module';
import { ApotekGroupModule } from 'apps/apotek-group/src/apotek-group.module';
import { ApotekModule } from 'apps/apotek/src/apotek.module';
import { DataModule } from 'apps/data/src/data.module';
import { LabModule } from 'apps/lab/src/lab.module';
import { RefModule } from 'apps/ref/src/ref.module';
import { UserModule } from 'apps/user/src/user.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemplateModule } from './template/template.module';

@Module({
  imports: [
    CommonModule,
    RefModule,
    LabModule,
    UserModule,
    AdminModule, // exclusive for admin admin, not using RouterModule
    ApotekGroupModule,
    ApotekModule,
    RouterModule.register([
      {
        path: 'api-ref',
        module: RefModule
      },
      {
        path: 'api-lab',
        module: LabModule
      },
      {
        path: 'api-user',
        module: UserModule
      },
      {
        path: `api-apotek-group`,
        module: ApotekGroupModule,
      },
      {
        path: `api-apotek`,
        module: ApotekModule,
      }
    ]),
    TemplateModule,
    ChatModule,

    // Data module, other common data must in this module
    DataModule,

    // Upload file module
    FileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
