import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req } from "@nestjs/common";
import { ApiQuery, ApiTags } from "@nestjs/swagger";
import { ApotekHeadofficeRole } from "../guard/apotek-headoffice-guard-decorator";
import { ApotekHeadofficeUserService } from "./apotek-headoffice-user.service";
import { UserHeadofficeManageDTO } from "./type";

@ApiTags('Apotek Headoffice User Manage')
@RequireAuthApp(AppsEnum.ApotekHeadOffice, AppsEnum.Admin)
@Controller('apotek-headoffice-user')
export class ApotekHeadofficeUserController {
  constructor(
    private apotekHeadofficeUserService: ApotekHeadofficeUserService
  ) { }

  @Post('user-manage')
  async create(
    @Body() body: UserHeadofficeManageDTO,
    @UserCred() user: UserTokenData
  ) {
    await this.apotekHeadofficeUserService.identifyRoleUser(
      user,
      {
        ifAppHq: async (roleUser) => {
          body.headofficeId = roleUser.apotekHeadoffice.id
        }
      }
    )

    return this.apotekHeadofficeUserService.create(body)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @ApiQuery({
    name: 'apotekHeadoffice',
    required: false
  })
  @Get('user-manage')
  async list(
    @Query('apotekHeadoffice') apotekHeadOffice: string,
    @Req() req: any
  ) {
    if (!apotekHeadOffice) {
      apotekHeadOffice = req.apotekHeadoffice.id
    }
    return this.apotekHeadofficeUserService.findAll(apotekHeadOffice)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @Get('user-manage/:idRole')
  async getDetail(
    @Param('idRole') idRole: number,
    @Req() req: any
  ) {
    return await this.apotekHeadofficeUserService.find(+idRole, req.apotekHeadoffice.id)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @Patch('user-manage/:idRole')
  async update(
    @Param('idRole') idRole: number,
    @Body() body: UserHeadofficeManageDTO,
    @Req() req: any
  ) {
    return this.apotekHeadofficeUserService.update(body, idRole, req.apotekHeadoffice.id)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @Delete('user-manage/:idRole')
  async delete(
    @Param('idRole') idRole: number,
    @Req() req: any
  ) {
    return this.apotekHeadofficeUserService.delete(+idRole, req.apotekHeadoffice.id)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @Get('/role')
  getRole() {
    return this.apotekHeadofficeUserService.getRole()
  }
}