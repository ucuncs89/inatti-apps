import { ApotekHeadofficeRoleUser } from '@app/database/entity/apotek-head-office/apotek-head-office-role-user.entity';
import { ApotekHeadofficeRole } from '@app/database/entity/apotek-head-office/apotek-head-office-role.entity';
import { ApotekHeadOffice } from '@app/database/entity/apotek-head-office/apotek-head-office.entity'
import { ApotekHeadofficeAssigned } from '@app/database/entity/apotek-head-office/apotek-head-office-assigned.entity'
import { UserService } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserHeadofficeManageDTO } from './type';
import { ApotekGroupDetail } from '@app/database/entity/apotek-group/apotek-group-detail.entity';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';

@Injectable()
export class ApotekHeadofficeUserService {
  constructor(
    @InjectRepository(ApotekHeadofficeRoleUser)
    private ApotekHeadofficeRoleUserRepository: Repository<ApotekHeadofficeRoleUser>,
    @InjectRepository(ApotekHeadofficeAssigned)
    private ApotekHeadofficeAssignedRepository: Repository<ApotekHeadofficeAssigned>,
    @InjectRepository(ApotekHeadofficeRole)
    private ApotekHeadofficeRoleRepository: Repository<ApotekHeadofficeRole>,
    private userService: UserService
  ) { }

  private roleUserRelationsDefault = [
    'roleId',
    'userId',
    'apotekHeadoffice'
  ]

  private userDTOtoEntity(payload: UserHeadofficeManageDTO) {
    const userInput = new User()
    Object.assign(userInput, {
      fullname: payload.fullname,
      email: payload.email,
      password: payload.password,
      username: payload.username
    })
    return userInput
  }


  async create(payload: UserHeadofficeManageDTO) {
    this.checkUserBM(payload)

    const userInput = this.userDTOtoEntity(payload)
    const userData = await this.userService.findOrCreate(userInput)

    if (await this.getAssignedRoleuser(userData, payload.headofficeId)) {
      throw new BadRequestException('User already assigned')
    }
    const roleUser = await this.updateAssignRoleUser(userData, payload.headofficeId, payload.roleId)

    if (this.isUserBM(payload)) {
      await this.assignApotekGroup(roleUser, payload.apotekGroupIds)
    }
    userData.hideCredential()
    return userData;
  }

  private async getAssignedRoleuser(user: User, headofficeId?: string) {
    const where: Partial<ApotekHeadofficeRoleUser> = {
      userId: user
    }
    if (headofficeId) {
      where.apotekHeadoffice = new ApotekHeadOffice(headofficeId)
    }
    return await this.ApotekHeadofficeRoleUserRepository.findOne({
      where,
      relations: this.roleUserRelationsDefault
    })
  }

  private async updateAssignRoleUser(user: User, headOfficeId: string, roleId: number) {
    let roleUser = await this.getAssignedRoleuser(user, headOfficeId)
    if (!roleUser) {
      roleUser = new ApotekHeadofficeRoleUser()
    }
    if (headOfficeId) {
      roleUser.apotekHeadoffice = new ApotekHeadOffice(headOfficeId)
    }
    if (roleId) {
      roleUser.roleId = new ApotekHeadofficeRole(roleId)
    }
    roleUser.userId = user
    return this.ApotekHeadofficeRoleUserRepository.save(roleUser)
  }

  RoleIdHQ = 1
  isUserBM(payload: UserHeadofficeManageDTO) {
    return payload.roleId !== this.RoleIdHQ && payload.apotekGroupIds && payload.apotekGroupIds.length > 0
  }

  checkUserBM(payload: UserHeadofficeManageDTO) {
    if (payload.roleId !== this.RoleIdHQ) {
      if (!payload.apotekGroupIds || payload.apotekGroupIds.length === 0) throw new BadRequestException('BM must have apotek group')
    }
  }

  // user pass to here, must be only BM
  private async assignApotekGroup(roleUser: ApotekHeadofficeRoleUser, groupIds: string[]) {
    // reset first
    await this.ApotekHeadofficeAssignedRepository.delete({
      role: roleUser
    })

    const assigned = groupIds.map(groupId => {
      const assigned = new ApotekHeadofficeAssigned()
      assigned.role = roleUser
      assigned.apotekGroup = new ApotekGroupDetail(groupId)
      return assigned
    })
    return this.ApotekHeadofficeAssignedRepository.save(assigned)
  }

  async findAll(apotekHeadOfficeId?: string) {
    let where;
    if (apotekHeadOfficeId) {
      where = {
        apotekHeadoffice: apotekHeadOfficeId
      }
    }
    let res = await this.ApotekHeadofficeRoleUserRepository.find({
      where,
      relations: this.roleUserRelationsDefault
    })

    res = res.map((v) => {
      v.userId.hideCredential()
      return v;
    })

    return res;
  }

  async find(idRole: number, apotekHeadOfficeId?: string) {
    const where: Partial<ApotekHeadofficeRoleUser> = {
      id: idRole
    }
    if (apotekHeadOfficeId) {
      where.apotekHeadoffice = new ApotekHeadOffice(apotekHeadOfficeId)
    }
    const res = await this.ApotekHeadofficeRoleUserRepository.findOne({
      where,
      relations: this.roleUserRelationsDefault.concat('apotekGroup'),
      withDeleted: true
    })
    if (!res) {
      throw new NotFoundException('User not found')
    }
    if (res?.userId) {
      res.userId.hideCredential()
    }
    return res;
  }

  async update(payload: UserHeadofficeManageDTO, idRole: number, apotekHeadOfficeId?: string) {
    let user = await this.find(idRole, apotekHeadOfficeId)
    if (!user) throw new NotFoundException('User not found')

    const userInput = this.userDTOtoEntity(payload)
    const userData = await this.userService.update(user.userId.id, userInput)

    const roleUser = await this.updateAssignRoleUser(userData, apotekHeadOfficeId, payload.roleId)
    if (this.isUserBM(payload)) {
      await this.assignApotekGroup(roleUser, payload.apotekGroupIds)
    }
    userData.hideCredential()
    return userData;
  }

  async delete(idRole: number, apotekHeadOfficeId?: string) {
    const dataAssigned = await this.ApotekHeadofficeAssignedRepository.find({
      where: {
        role: {
          id: idRole
        }
      }
    })
    if (dataAssigned && dataAssigned.length > 0) {
      await this.ApotekHeadofficeAssignedRepository.delete(dataAssigned.map(v => v.id))
    }
    const data = await this.find(idRole, apotekHeadOfficeId)
    return await this.ApotekHeadofficeRoleUserRepository.delete(data.id)
  }

  getRole() {
    return this.ApotekHeadofficeRoleRepository.find()
  }

  getRoleUser(id: number) {
    return this.ApotekHeadofficeRoleUserRepository.findOne({
      where: {
        id,
      },
      relations: [
        'roleId',
        'apotekGroup',
        'apotekHeadoffice'
      ],
      withDeleted: true
    })
  }

  /**
   * This function only check if user from app Admin or Apotek Headoffice
   */
  async identifyRoleUser(user: UserTokenData, cb?: {
    ifBM?: (roleUser: ApotekHeadofficeRoleUser) => any,
    ifHQ?: (roleUser: ApotekHeadofficeRoleUser) => any,
    ifAppHq?: (roleUser: ApotekHeadofficeRoleUser) => any,
    ifAppAdmin?: () => Promise<any>,
  }) {
    cb = cb || {}

    // If admin, fire ifAppAdmin
    if (user.payload.apps === AppsEnum.Admin) {
      if (cb.ifAppAdmin) {
        return await cb.ifAppAdmin()
      }
      return;
    }

    // if other than Admin and headoffice, return nothing
    if (![AppsEnum.ApotekHeadOffice, AppsEnum.Admin].includes(user.payload.apps)) {
      return;
    }

    // if doesn't have roleId, fire error
    if (!user.payload.roleId) {
      throw new BadRequestException('User apotek headoffice not assigned to any role')
    }
    const roleUser = await this.getRoleUser(+user.payload.roleId)
    if (cb.ifAppHq) {
      await cb.ifAppHq(roleUser)
    }
    if (roleUser.roleId.id !== this.RoleIdHQ) {
      if (cb.ifBM) {
        return await cb.ifBM(roleUser)
      }
    }
    if (cb.ifHQ) {
      return await cb.ifHQ(roleUser)
    }
  }
}