import { UserManageDTO } from "@app/database/entity/user/user.dto";
import { ApiProperty } from "@nestjs/swagger";

export class UserHeadofficeManageDTO extends UserManageDTO {
  @ApiProperty()
  headofficeId?: string

  @ApiProperty()
  apotekGroupIds?: string[]
}