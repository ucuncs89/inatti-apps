import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { LoginDTO, UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Body, Controller, Get, Post, Request, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { ApotekHeadofficeUserService } from '../user/apotek-headoffice-user.service'

@Controller('apotek-headoffice-auth')
export class ApotekHeadofficeAuthController {
  constructor(
    private apotekHeadofficeUser: ApotekHeadofficeUserService
  ) { }


  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard(AppsEnum.ApotekHeadOffice))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @Get('role-user')
  @RequireAuthApp(AppsEnum.ApotekHeadOffice)
  async getUserRole(
    @UserCred() user: UserTokenData
  ) {
    return this.apotekHeadofficeUser.getRoleUser(+user.payload.roleId)
  }
}