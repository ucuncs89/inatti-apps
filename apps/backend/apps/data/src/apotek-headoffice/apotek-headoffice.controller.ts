import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ApotekHeadofficeService } from "./apotek-headoffice.service";
import { ApotekHeadofficeRole } from "./guard/apotek-headoffice-guard-decorator";
import { ApotekHeadofficeDTO, ApotekHeadofficeListDTO } from "./type";

@ApiTags('Apotek Headoffice')
@RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekHeadOffice)
@Controller('apotek-headoffice')
export class ApotekHeadofficeController {
  constructor(
    private apotekHeadofficeService: ApotekHeadofficeService
  ) { }

  @ApotekHeadofficeRole({ admin: true })
  @Post()
  create(
    @Body() body: ApotekHeadofficeDTO
  ) {
    return this.apotekHeadofficeService.create(body)
  }

  @ApotekHeadofficeRole({ admin: true })
  @Get()
  getList(
    @Query() query: ApotekHeadofficeListDTO
  ) {
    return this.apotekHeadofficeService.getList(query)
  }

  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.apotekHeadofficeService.get(id)
  }

  @ApotekHeadofficeRole({ admin: true, HQ: true })
  @Patch(':id')
  updateOne(@Param('id') id: string, @Body() body: ApotekHeadofficeDTO) {
    return this.apotekHeadofficeService.edit(id, body)
  }

  @ApotekHeadofficeRole({ admin: true })
  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.apotekHeadofficeService.delete(id)
  }
}