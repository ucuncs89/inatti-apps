import { ApiProperty } from "@nestjs/swagger";

export class ApotekHeadofficeDTO {
  @ApiProperty()
  name: string
}

export class ApotekHeadofficeListDTO {
  @ApiProperty({ required: false })
  search?: string
}