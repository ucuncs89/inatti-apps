import { Module } from '@nestjs/common';
import { ApotekHeadofficeController } from './apotek-headoffice.controller';
import { ApotekHeadofficeService } from './apotek-headoffice.service';
import { ApotekHeadofficeAuthController } from './auth/apotek-headoffice-auth.controller';
import { ApotekHeadofficeUserController } from './user/apotek-headoffice-user.controller';
import { ApotekHeadofficeUserService } from './user/apotek-headoffice-user.service';

@Module({
  controllers: [
    ApotekHeadofficeController,
    ApotekHeadofficeAuthController,
    ApotekHeadofficeUserController,
  ],
  providers: [
    ApotekHeadofficeService,
    ApotekHeadofficeUserService
  ],
  exports: [
    ApotekHeadofficeUserService
  ]
})
export class ApotekHeadofficeModule { }