import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ApotekHeadofficeUserService } from '../user/apotek-headoffice-user.service';
import { ApotekHeadofficeMetadata, ApotekHeadofficeRoleOptions, ApotekHeadofficeRoleParamOptions } from './apotek-headoffice-guard-decorator';

@Injectable()
export class ApotekHeadofficeGuard implements CanActivate {
  constructor(
    private apotekHeadofficeUserService: ApotekHeadofficeUserService,
    private reflector: Reflector
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest()
    const user = request.user

    const roles = this.reflector.get<string[]>(ApotekHeadofficeMetadata, context.getHandler())

    if (!roles && user) {
      return true
    }

    const roleHave = (area: ApotekHeadofficeRoleOptions) => roles.includes(area)

    return await this.apotekHeadofficeUserService.identifyRoleUser(
      user,
      {
        ifHQ: async (roleUser) => {
          request.apotekHeadoffice = roleUser.apotekHeadoffice
          return roleHave('HQ')
        },
        ifBM: async (roleUser) => {
          request.apotekHeadoffice = roleUser.apotekHeadoffice
          return roleHave('BM')
        },
        ifAppAdmin: async () => {
          request.apotekHeadoffice = {}
          return roleHave('admin')
        }
      }
    )
  }
}