import { applyDecorators, createParamDecorator, SetMetadata, UseGuards } from "@nestjs/common";
import { ApotekHeadofficeGuard } from "./apotek-headoffice-guard";

export enum ApotekHeadofficeRoleParamOptions {
  admin = 'admin',
  HQ = 'HQ',
  BM = 'BM',
}

export type ApotekHeadofficeRoleOptions = keyof typeof ApotekHeadofficeRoleParamOptions

export type ApotekHeadofficeRoleParam = { [key in ApotekHeadofficeRoleParamOptions]?: boolean }

export const ApotekHeadofficeMetadata = 'apotekHeadofficeMetadata'

export function ApotekHeadofficeRole(role?: ApotekHeadofficeRoleParam) {
  // filter by true value
  const roleSelected = Object.keys(role).filter(key => role[key])

  return applyDecorators(
    SetMetadata(ApotekHeadofficeMetadata, roleSelected),
    UseGuards(ApotekHeadofficeGuard),
  )
}