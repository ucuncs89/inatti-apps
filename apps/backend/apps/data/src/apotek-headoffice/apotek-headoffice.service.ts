import { ApotekHeadOffice } from "@app/database/entity/apotek-head-office/apotek-head-office.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ILike, Repository } from "typeorm";
import { ApotekHeadofficeDTO, ApotekHeadofficeListDTO } from "./type";

@Injectable()
export class ApotekHeadofficeService {
  constructor(
    @InjectRepository(ApotekHeadOffice)
    private ApotekHeadOfficeRepository: Repository<ApotekHeadOffice>,
  ) { }

  create(data: ApotekHeadofficeDTO) {
    const newHo = new ApotekHeadOffice()
    Object.assign(newHo, data)
    return this.ApotekHeadOfficeRepository.save(newHo)
  }

  getList(query: ApotekHeadofficeListDTO) {
    const where: { [K in keyof ApotekHeadOffice]?: any } = {}
    if (query.search) {
      where.name = ILike(`%${query.search}%`)
    }
    return this.ApotekHeadOfficeRepository.find({
      where,
      order: {
        createdAt: 'DESC'
      }
    })
  }

  get(id: string) {
    return this.ApotekHeadOfficeRepository.findOne(id)
  }

  edit(id: string, data: ApotekHeadofficeDTO) {
    return this.ApotekHeadOfficeRepository.update(id, data)
  }

  delete(id: string) {
    return this.ApotekHeadOfficeRepository.softDelete(id)
  }
}