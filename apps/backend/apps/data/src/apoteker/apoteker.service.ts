import { AwsService } from '@app/aws';
import { ConfigAppService, CONFIGAPP_CATEGORY, CONFIGAPP_NAME } from '@app/config-app';
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { ApotekerDetail } from '@app/database/entity/apotek/apoteker-detail.entity';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import projectConfig from 'config/project.config';
import { FindConditions, ILike, In, IsNull, Not, ObjectLiteral, Repository } from 'typeorm';
import { ApotekService } from '../apotek/apotek.service';
import { ApotekerDetailDTO, ApotekerListQuery } from './type';
import * as _ from 'lodash'
import { Pagination } from '@inatti/shared/dtos';
import { Kabupaten } from '@app/database/entity/region/kabupaten.entity';
import { Provinsi } from '@app/database/entity/region/provinsi.entity';

type ApotekerDetailWhere = FindConditions<ApotekerDetail>[] | FindConditions<ApotekerDetail> | ObjectLiteral | string
@Injectable()
export class ApotekerService {
  constructor(
    @InjectRepository(ApotekerDetail)
    private ApotekerDetailRepository: Repository<ApotekerDetail>,
    private awsService: AwsService,
    private configService: ConfigAppService,
    private apotekService: ApotekService
  ) { }

  defaultRelations: string[] = [
    'apotek',
    'apotek.provinsi',
    'apotek.kabupaten',
  ]

  private qualifiedApoteker(where: FindConditions<ApotekerDetail>) {
    const notNull = Not(IsNull())
    where.nomorSIPA = notNull
    where.nomorSTRA = notNull
    where.phonenumber = notNull
    if (!where.name) {
      where.name = notNull
    }
    return where
  }

  async getAll(query: ApotekerListQuery) {
    if (query.filterCoordinate) {
      return this.getApotekerNearCoord(query)
    }

    const { where, relations } = this.queryToWhere(query)

    this.qualifiedApoteker(where)

    const res = await this.ApotekerDetailRepository.find({
      where,
      relations: relations,
      skip: query.pagination.skip,
      take: query.pagination.take,
    })

    const total = await this.ApotekerDetailRepository.count({
      where,
      relations: relations
    })

    return new Pagination({
      results: res,
      total,
    }, query.pagination)
  }

  queryToWhere(query: ApotekerListQuery) {
    const where: ApotekerDetailWhere = {}
    if (query.search) {
      where.name = ILike(`%${query.search}%`)
    }
    if (query.filterProvinsi && query.filterKabupaten) {
      where.apotek = {
        provinsi: new Provinsi(+query.filterProvinsi),
        kabupaten: new Kabupaten(+query.filterKabupaten)
      }
    }
    if (String(query.isActive) === 'true') {
      where.isActive = String(query.isActive) === 'true'
    }

    return {
      where,
      relations: this.defaultRelations,
    }
  }

  async getApotekerNearCoordData(query: ApotekerListQuery) {
    const apotekNear = await this.apotekService.getApotekNearCoordData({
      ...query,
      withDeleted: false,
      filterApotekGroup: null,
    })

    const { where, relations } = this.queryToWhere(query)

    where.apotek = In(apotekNear.map(a => a.id))

    this.qualifiedApoteker(where)
    const apoteker = await this.ApotekerDetailRepository.find({
      where,
      relations: relations
    })
    return apoteker
  }

  async getApotekerNearCoord(query: ApotekerListQuery) {
    let apoteker = await this.getApotekerNearCoordData(query)
    apoteker = _.slice(apoteker, query.pagination.skip, query.pagination.skip + query.pagination.take)
    return new Pagination({
      results: apoteker,
      total: apoteker.length,
    }, query.pagination)
  }

  async getApotekerDetail(apotekerId: string) {
    return await this.ApotekerDetailRepository.findOne({
      where: {
        id: apotekerId
      },
      relations: [
        'apotek'
      ]
    })
  }

  async getApotekerInApotek(apotekId: string) {
    const where = {
      apotek: {
        id: apotekId
      }
    }
    this.qualifiedApoteker(where)
    let res = await this.ApotekerDetailRepository.find({
      where,
    })

    return res;
  }

  async getApotekerInfo(userId: string, apotekId: string) {
    if (!userId || !apotekId) {
      throw new BadRequestException('userId and apotekId is required')
    }
    let apoteker = await this.ApotekerDetailRepository.findOne({
      where: {
        user: {
          id: userId,
        },
        apotek: {
          id: apotekId
        }
      },
      relations: [
        'apotek',
        'user'
      ]
    })
    if (!apoteker) {
      apoteker = await this.createApotekerInfo(userId, apotekId)
    }
    if (apoteker.user) {
      apoteker.user.hideCredential()
    }
    return apoteker
  }

  async createApotekerInfo(userId: string, apotekId: string) {
    let newApoteker = new ApotekerDetail()
    newApoteker.user = new User(userId)
    newApoteker.apotek = new ApotekDetail(apotekId)
    return await this.ApotekerDetailRepository.save(newApoteker)
  }

  async updateApoteker(userId: string, data: ApotekerDetailDTO) {
    if (!userId) throw new Error("User id not found")
    let apoteker = await this.getApotekerInfo(userId, data.apotek)
    Object.assign(apoteker, data)
    await this.ApotekerDetailRepository.save(apoteker)
  }

  async uploadPhoto(userId: string, apotekId: string, photoFile: Express.Multer.File) {
    let apoteker = await this.getApotekerInfo(userId, apotekId)
    let photo = await this.awsService.HashAndUploadS3(photoFile)
    apoteker.photo = photo.s3.Location
    return await this.ApotekerDetailRepository.save(apoteker)
  }

  async generatePreMessageWhatsapp(patientId: string, apotekerId: string) {
    let message = await this.getMessage(patientId)
    let apoteker = await this.ApotekerDetailRepository.findOne(apotekerId)

    if (!apoteker.phonenumber) {
      throw new BadRequestException("Apoteker phone number not found")
    }

    let urlWhatsapp = `https://api.whatsapp.com/send?phone=${apoteker.phonenumber}&text=${encodeURIComponent(message)}`
    return {
      urlWhatsapp,
    }
  }

  private async getMessage(patientId: string) {
    let urlPasien = projectConfig().apotekUrl + '/patient/' + patientId

    let config = await this.configService.getConfigOrInit(CONFIGAPP_NAME.WA_PREMESSAGE_APOTEKER, {
      value: 'Konsultasi Apotek. \nData Pasien : {urlPasien}\n--Jangan dihapus data diatas--.',
      category: CONFIGAPP_CATEGORY.Apoteker
    })
    config = config.replace('{urlPasien}', urlPasien)
    return config
  }
}
