import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { Body, Controller, Get, Patch, Query, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { ApotekUserService } from '../apotek/user/apotek-user.service';
import { ApotekerService } from './apoteker.service';
import { ApotekerDetailDTO } from './type';

export const tags = 'Data Apoteker API'
@ApiTags(tags)
@RequireAuthApp(AppsEnum.Apotek)
@Controller('api-apotek/account-apoteker')
export class AccountApotekerController {
  constructor(
    private accountApotekerService: ApotekerService,
    private userApotek: ApotekUserService
  ) { }

  @Get()
  async getInfo(
    @Query('apotekId') apotekId: string,
    @UserCred() user: User
  ) {
    return await this.accountApotekerService.getApotekerInfo(user.id, apotekId)
  }

  @Patch()
  async updateAccount(
    @Body() body: ApotekerDetailDTO,
    @UserCred() user: UserTokenData
  ) {
    // if body.apotek not found, search by user id
    if (!body.apotek) {
      const roleUser = await this.userApotek.getUserRoleLoggedin(user)
      body.apotek = roleUser.apotekDetail.id
    }
    return await this.accountApotekerService.updateApoteker(user.id, body)
  }

  @UseInterceptors(FileFieldsInterceptor([
    { name: 'photo', maxCount: 1 }
  ]))
  @Patch('photo')
  async updatePhotoAccount(
    @Query('apotekId') apotekId: string,
    @UploadedFiles()
    file: { photo: Express.Multer.File[] },
    @UserCred() user: User
  ) {
    console.log(file.photo)
    if (!file.photo || file.photo.length < 1) throw "Photo not uploaded"
    return await this.accountApotekerService.uploadPhoto(
      user.id,
      apotekId,
      file.photo[0]
    )
  }
}
