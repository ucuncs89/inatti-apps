import { GenderEnum, PaginationOptionsInterface } from "@app/database/entity/swab-data/type";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ApotekerDetailDTO {
  @IsNotEmpty()
  @ApiProperty()
  name?: string

  @ApiProperty({ enum: GenderEnum })
  gender?: GenderEnum

  @ApiProperty()
  birthdate?: Date

  @ApiProperty()
  photo?: string

  @ApiProperty()
  isActive: Boolean;

  @ApiProperty()
  nomorSIPA: string;

  @ApiProperty()
  nomorSTRA: string;

  @ApiProperty()
  apotek: any

  @ApiProperty()
  phonenumber: string
}

export class ApotekerListQuery {
  @ApiProperty({
    required: false,
  })
  search: string

  @ApiProperty({ required: false })
  filterProvinsi: string

  @ApiProperty({ required: false })
  filterKabupaten: string

  @ApiProperty({ required: false })
  filterCoordinate: string

  @ApiProperty({ required: false })
  isActive?: boolean

  pagination: PaginationOptionsInterface
}