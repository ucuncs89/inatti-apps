import { AwsModule } from '@app/aws';
import { ConfigAppModule } from '@app/config-app';
import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import awsConfig from 'config/aws.config';
import { ApotekModule } from '../apotek/apotek.module';
import { AccountApotekerController } from './account-apoteker.controller';
import { ApotekerController } from './apoteker.controller';
import { ApotekerService } from './apoteker.service';

@Module({
  imports: [
    DatabaseModule,
    ConfigAppModule,
    AwsModule.register({
      bucket: awsConfig().aws.bucket,
      uploadFolder: 'apoteker',
    }),
    ApotekModule
  ],
  controllers: [ApotekerController, AccountApotekerController],
  providers: [ApotekerService]
})
export class ApotekerModule { }
