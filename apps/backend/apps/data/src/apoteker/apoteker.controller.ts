import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { PaginationOptionsInterface } from '@inatti/shared/dtos';
import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { tags } from './account-apoteker.controller';
import { ApotekerService } from './apoteker.service';
import { ApotekerListQuery } from './type';

@ApiTags(tags)
@RequireAuthApp()
@Controller('apoteker-info')
export class ApotekerController {
  constructor(
    private apotekerService: ApotekerService
  ) { }

  @Get('find')
  async findApoteker(
    @Query()
    query: ApotekerListQuery,
    @Query('paginate', PaginationPipe)
    pagination: PaginationOptionsInterface
  ) {
    query.pagination = pagination

    return await this.apotekerService.getAll(query)
  }

  @Get('apotek/:apotekId')
  async getApotekerInfo(
    @Param('apotekId') apotekId: string,
  ) {
    return await this.apotekerService.getApotekerInApotek(apotekId)
  }

  @Get('detail/:apotekerId')
  async getApotekerDetail(
    @Param('apotekerId') apotekerId: string,
  ) {
    return await this.apotekerService.getApotekerDetail(apotekerId)
  }

  @Get('generate/pre-message/whatsapp')
  async generatePreMessageWhatsapp(
    @Query('patientId') patientId: string,
    @Query('apotekerId') apotekerId: string,
  ) {
    return await this.apotekerService.generatePreMessageWhatsapp(patientId, apotekerId)
  }
}