import { CommonFilterDTO } from "@inatti/shared/dtos"
import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty } from 'class-validator'

export class MedicineCreateDTO {
  @IsNotEmpty()
  @ApiProperty()
  name: string
}

export class MedicineUpdateDTO extends MedicineCreateDTO {
  @ApiProperty()
  id: number
}

export class MedicineInserterDTO {
  @ApiProperty()
  userId?: string
  @IsNotEmpty()
  @ApiProperty()
  apotekId?: string
}

export class FilterMedicineDTO extends CommonFilterDTO { }

export class UploadExcelMedicineDTO {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: Express.Multer.File[]
}