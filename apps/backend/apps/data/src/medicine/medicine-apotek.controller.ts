import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { ApiFile } from '@app/file/api-file.decorator';
import { FilterCommonTelemedicineList, PaginationOptionsInterface } from '@inatti/shared/dtos';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { MedicineService } from './medicine.service';
import { MedicineCreateDTO, MedicineUpdateDTO, UploadExcelMedicineDTO } from './type';

@ApiTags('Data Medicine')
@RequireAuthApp(AppsEnum.Apotek, AppsEnum.ApotekGroup, AppsEnum.Admin, AppsEnum.ApotekHeadOffice)
@Controller('medicine/apotek')
export class MedicineApotekController {
  constructor(
    private medicineService: MedicineService
  ) { }

  @Get()
  async getBasedApotek(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter')
    filter: FilterCommonTelemedicineList
  ) {
    console.log(filter)
    return await this.medicineService.getListNonPublic(paginate, filter)
  }

  @Post()
  addByApotek(
    @Body()
    payload: MedicineCreateDTO,
    @Query('idapotek')
    idApotek: string,
    @UserCred()
    user: User
  ) {
    return this.medicineService.add(payload, {
      apotekId: idApotek,
      userId: user.id
    })
  }

  /**
   * Small API to search medicine for PMR
   * @param apotekId 
   * @param name 
   * @returns 
   */
  @Get('search')
  getSearchName(
    @Query('apotekId')
    apotekId: string,
    @Query('name')
    name: string,
  ) {
    return this.medicineService.search(apotekId, name)
  }

  @Patch()
  update(
    @Body()
    payload: MedicineUpdateDTO,
    @Query('idapotek')
    idApotek: string,
    @UserCred()
    user: User
  ) {
    return this.medicineService.update(payload, {
      apotekId: idApotek,
      userId: user.id
    })
  }

  @Delete()
  deleteMedicine(
    @Query('idapotek')
    idApotek: string,
    @Query('id')
    id: number
  ) {
    return this.medicineService.delete(id, idApotek)
  }

  @Post('upload-excel')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'file', maxCount: 1 }
  ]))
  @ApiConsumes('multipart/form-data')
  @ApiFile('file')
  uploadExcel(
    @UploadedFiles()
    file: UploadExcelMedicineDTO,
    @Query('idapotek')
    idApotek: string,
    @UserCred()
    user: UserTokenData
  ) {
    if (!idApotek) throw "Apotek id is required"
    if (!file.file || file.file.length < 1) throw "File not uploaded"
    return this.medicineService.uploadExcel(file.file[0], user, idApotek)
  }
}