import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum, AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { ApiFile } from '@app/file/api-file.decorator';
import { PaginationOptionsInterface } from '@inatti/shared/dtos';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { CommonFilterDTO } from '@inatti/shared/dtos';
import { Body, Controller, Delete, Get, Patch, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { MedicineService } from './medicine.service';
import { FilterMedicineDTO, MedicineCreateDTO, MedicineUpdateDTO, UploadExcelMedicineDTO } from './type';

@ApiTags('Data Medicine')
@Controller('medicine/public')
export class MedicinePublicController {
  constructor(
    private medicineService: MedicineService
  ) { }

  @RequireAuthApp(...AppsExcludeUser)
  @Get()
  getAll(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter')
    filter: CommonFilterDTO
  ) {
    return this.medicineService.getPublic(paginate, filter)
  }

  @ApiOperation({
    summary: "Add medicine public (not for apotek), only admin"
  })
  @RequireAuthApp(AppsEnum.Admin)
  @Post()
  addPublicMed(
    @Body()
    payload: MedicineCreateDTO,
    @UserCred()
    user: User
  ) {
    return this.medicineService.add(payload, {
      userId: user.id
    })
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Patch()
  updateApotek(
    @Body()
    payload: MedicineUpdateDTO,
    @UserCred()
    user: User
  ) {
    return this.medicineService.update(payload, {
      userId: user.id
    })
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Delete()
  deleteMedicine(
    @Query('id')
    id: number
  ) {
    return this.medicineService.delete(id)
  }

  @Post('upload-excel')
  @RequireAuthApp(AppsEnum.Admin)
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'file', maxCount: 1 }
  ]))
  @ApiConsumes('multipart/form-data')
  @ApiFile('file')
  uploadExcel(
    @UploadedFiles()
    file: UploadExcelMedicineDTO,
    @UserCred()
    user: UserTokenData
  ) {
    if (!file.file || file.file.length < 1) throw "File not uploaded"
    return this.medicineService.uploadExcel(file.file[0], user)
  }
}