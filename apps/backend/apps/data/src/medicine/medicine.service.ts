import { Medicine } from '@app/database/entity/medicine/medicine.entity';
import { User } from '@app/database/entity/user/user.entity';
import { CommonFilterDTO, FilterCommonTelemedicineList, Pagination, PaginationOptionsInterface } from '@inatti/shared/dtos';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, getManager, ILike, In, IsNull, ObjectLiteral, Repository } from 'typeorm';
import * as _ from "lodash";
import * as Excel from 'exceljs'
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { MedicineCreateDTO, MedicineInserterDTO, MedicineUpdateDTO } from './type';
import { Multer } from 'multer';
import { ShouldArray } from '@app/helper/utils/array';

type WhereMedicine = FindConditions<Medicine>[] | FindConditions<Medicine> | ObjectLiteral | string

@Injectable()
export class MedicineService {
  constructor(
    @InjectRepository(Medicine)
    private MedicineRepository: Repository<Medicine>,
  ) { }

  pickImportant(data: Medicine[]) {
    data.forEach((result) => {
      if (!result.insertBy) return;
      result.insertBy = _.pick(result.insertBy, ['id', 'fullname', 'username']) as User
      result.insertByApotek = _.pick(result.insertByApotek, ['id', 'name', 'apotekGroup.id', 'apotekGroup.name']) as ApotekDetail
    })
  }

  /**
   * Get public medicine, __no need to__ filter by apotek or apotek group
   * @param paginate 
   * @param filter 
   * @returns 
   */
  getPublic(paginate: PaginationOptionsInterface, filter?: CommonFilterDTO) {
    let whereMed: WhereMedicine = { insertByApotek: IsNull() }
    if (filter && filter.search) {
      whereMed.name = ILike(`%${filter.search}%`)
    }
    return this.get(whereMed, paginate)
  }

  filterMedicine(filter: FilterCommonTelemedicineList) {
    let whereMed: WhereMedicine = {}
    if (filter && filter.search) {
      whereMed.name = ILike(`%${filter.search}%`)
    }
    if (filter.apotekGroup) {
      whereMed.insertByApotek = {
        apotekGroup: In(ShouldArray(filter.apotekGroup))
      }
    }
    if (filter.apotek) {
      whereMed.insertByApotek = In(ShouldArray(filter.apotek))
    }
    return whereMed
  }

  /**
   * Get list medicine filter by apotek or apotek group
   * @param paginate 
   * @param filter 
   * @returns 
   */
  async getListNonPublic(paginate: PaginationOptionsInterface, filter: FilterCommonTelemedicineList) {
    let whereMed: WhereMedicine = this.filterMedicine(filter)
    let res = await this.get(whereMed, paginate, [
      'insertByApotek',
      'insertByApotek.apotekGroup',
      'insertBy'
    ])
    this.pickImportant(res.results)
    return res;
  }

  async get(where: WhereMedicine, paginate: PaginationOptionsInterface, relations?: string[]) {
    let total = await this.MedicineRepository.count({ where, relations })
    let res = await this.MedicineRepository.find({
      where,
      order: {
        id: 'DESC',
      },
      relations,
      take: paginate.take,
      skip: paginate.skip
    })
    return new Pagination({
      results: res,
      total
    }, paginate)
  }

  /**
   * Small API to search medicine for PMR
   * @param apotekId 
   * @param name 
   * @returns 
   */
  async search(apotekId: string, name: string) {
    const max = 10
    const res = await this.get([
      {
        name: ILike(`%${name}%`),
        insertByApotek: IsNull()
      },
      {
        name: ILike(`%${name}%`),
        insertByApotek: apotekId
      },
    ], {
      limit: max,
      page: 0
    }, [
      'insertByApotek',
      'insertBy'
    ])
    this.pickImportant(res.results)
    return res
  }

  add(data: MedicineCreateDTO, inserter: MedicineInserterDTO) {
    let newMed = new Medicine()

    Object.assign(newMed, data)

    newMed.insertBy = new User(inserter.userId)
    newMed.insertByApotek = inserter.apotekId ? new ApotekDetail(inserter.apotekId) : null

    return this.MedicineRepository.save(newMed)
  }

  async update(
    data: MedicineUpdateDTO,
    inserter: MedicineInserterDTO
  ) {
    let med = await this.MedicineRepository.findOne({
      where: {
        id: data.id
      }
    })

    Object.assign(med, data)

    med.insertBy = new User(inserter.userId)
    med.insertByApotek = inserter.apotekId ? new ApotekDetail(inserter.apotekId) : null
    return this.MedicineRepository.save(med)
  }

  async delete(id: number, idApotek?: string) {
    let where = {
      id,
    }
    if (idApotek) {
      where['insertByApotek'] = idApotek
    }
    return this.MedicineRepository.softDelete(where)
  }

  private async parseExcelMedicine(file: Express.Multer.File): Promise<any[]> {
    const workbook = new Excel.Workbook();
    try {
      await workbook.xlsx.load(file.buffer);
    } catch (error) {
      throw "Failed to load excel file"
    }
    let worksheet = workbook.worksheets[0];

    const CELL_NAME = 1
    let namesMeicine = worksheet.getRows(0, worksheet.rowCount).map((v) => {
      return v.getCell(CELL_NAME).value
    }).filter((v) => !!v)
    return namesMeicine
  }

  async uploadExcel(file: any, inserter: User, apotekId?: string) {
    let namesMedicine = await this.parseExcelMedicine(file)
    if (namesMedicine.length < 1) {
      throw "No medicine found"
    }

    let insertedData = 0

    await getManager().transaction(async trx => {
      for (let name of namesMedicine) {
        let newMed = new Medicine()
        newMed.name = name
        newMed.insertBy = inserter
        newMed.insertByApotek = new ApotekDetail(apotekId)
        await trx.insert(Medicine, newMed)
        insertedData++
      }
    })
    return {
      message: `Inserted ${insertedData} medicines`,
      insertedData
    }
  }
}