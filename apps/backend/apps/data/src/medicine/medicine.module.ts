import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { MedicineApotekController } from './medicine-apotek.controller';
import { MedicinePublicController } from './medicine-public.controller';
import { MedicineService } from './medicine.service';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [
    MedicinePublicController,
    MedicineApotekController
  ],
  providers: [
    MedicineService
  ]
})
export class MedicineModule {}
