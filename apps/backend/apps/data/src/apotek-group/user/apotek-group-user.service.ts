import { UserTokenData } from '@app/authentication/types';
import { ApotekGroupDetail } from '@app/database/entity/apotek-group/apotek-group-detail.entity';
import { ApotekGroupRoleUser } from '@app/database/entity/apotek-group/apotek-group-role-user.entity';
import { ApotekGroupRole } from '@app/database/entity/apotek-group/apotek-group-role.entity';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { UserService } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserManageApotekDTO } from '../type/user-dto';

@Injectable()
export class ApotekGroupUserService {
  constructor(
    @InjectRepository(ApotekGroupRoleUser)
    private ApotekGroupRoleUserRepository: Repository<ApotekGroupRoleUser>,
    @InjectRepository(ApotekGroupRole)
    private ApotekGroupRoleRepository: Repository<ApotekGroupRole>,
    private userService: UserService
  ) { }

  async getRoles() {
    return await this.ApotekGroupRoleRepository.find();
  }

  /**
   * Get detail information logged in user
   * @param id 
   * @returns 
   */
  async getAssignedRoleUser(roleId: number) {
    return await this.ApotekGroupRoleUserRepository.findOne({
      where: {
        id: roleId
      },
      relations: [
        'apotekGroup',
        'roleId',
        'apotekGroup.headOffice'
      ],
      withDeleted: true
    })
  }

  async getUsers(apotekId: string) {
    return await this.getUserApotek(apotekId)
  }
  async getUser(apotekId: string, id: number) {
    const res = await this.getUserApotek(apotekId, id)
    if (Array.isArray(res)) return res[0]
    return res
  }

  private async getUserApotek(apotekId: string, id?: number) {
    const where: any = {
      apotekGroup: {
        id: apotekId
      },
    }
    if (id) {
      where.id = id
    }
    const users = await this.ApotekGroupRoleUserRepository.find({
      where,
      relations: [
        'userId',
        'roleId'
      ],
      order: {
        createdAt: 'DESC'
      }
    })
    users.forEach((userRole) => {
      userRole.userId.hideCredential()
    })
    return users
  }

  userDtoTomodel(user: UserManageApotekDTO) {
    const { roleId, ...dataUser } = user
    const newUser = new User()
    Object.assign(newUser, dataUser)
    return newUser
  }

  async addUser(apotekId: string, user: UserManageApotekDTO) {
    const newUser = this.userDtoTomodel(user)
    const userData = await this.userService.findOrCreate(newUser)

    const apotekGroupRoleUser = new ApotekGroupRoleUser()
    apotekGroupRoleUser.roleId = new ApotekGroupRole(user.roleId)
    apotekGroupRoleUser.userId = userData
    const apotekGroup = new ApotekGroupDetail()
    apotekGroup.id = apotekId;
    apotekGroupRoleUser.apotekGroup = apotekGroup
    await this.ApotekGroupRoleUserRepository.save(apotekGroupRoleUser)
    return true;
  }

  async updateUser(apotekId: string, id: number, user: UserManageApotekDTO) {
    const userAssigned = await this.ApotekGroupRoleUserRepository.findOne({
      where: {
        id: id,
        apotekGroup: apotekId
      },
      relations: [
        'userId',
        'apotekGroup',
        'roleId'
      ],
      withDeleted: true
    })
    if (!userAssigned) throw Error("User not found")

    const newUser = this.userDtoTomodel(user)
    await this.userService.update(userAssigned.userId.id, newUser)

    if (user.roleId) {
      userAssigned.roleId = new ApotekGroupRole(user.roleId)
    }

    return await this.ApotekGroupRoleUserRepository.save(userAssigned)
  }

  async deleteUser(apotekId: string, id: number) {
    const ApotekGroupRoleUser = await this.ApotekGroupRoleUserRepository.findOne({
      where: {
        id,
        apotekGroup: apotekId
      },
      withDeleted: true
    })
    if (!ApotekGroupRoleUser) throw new Error("Data not found")
    return await this.ApotekGroupRoleUserRepository.delete(id)
  }

  registeredUser(user: User) {
    return this.ApotekGroupRoleUserRepository.find({
      where: {
        userId: user,
      },
      relations: [
        'apotekGroup'
      ]
    })
  }

  async registeredApotekGroupUser(user: User) {
    const roles = await this.registeredUser(user)
    return roles.map((v) => v.apotekGroup).filter((v) => !!v)
  }

  async strictAccess(user: UserTokenData, apotekGroupid: string, action: string) {
    if (user.payload.apps !== AppsEnum.ApotekGroup) {
      return;
    }
    const access = await this.isUserHasAccess(user, apotekGroupid)
    if (!access || !access.roleId?.permission.includes(action)) {
      throw new BadRequestException("You have no permission for that")
    }
    return access
  }

  isUserHasAccess(user: User, apotekGroupId: string) {
    return this.ApotekGroupRoleUserRepository.findOne({
      where: {
        userId: user,
        apotekGroup: apotekGroupId
      },
      relations: [
        'apotekGroup',
        'roleId'
      ]
    })
  }

}

