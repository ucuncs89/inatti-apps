import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ApotekGroupUserService } from './apotek-group-user.service';

@ApiTags('Data Apotek Group API')
@RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekGroup)
@Controller('apotek-group-user')
export class ApotekGroupUserRoleController {
  constructor(
    private userManagementService: ApotekGroupUserService
  ) { }

  @Get('roles')
  async getRoles(
    @UserCred() user: UserTokenData
  ) {
    const roles = await this.userManagementService.getRoles()

    /**
     * need to filter role based user logged in
     */
    if (user.payload.apps === AppsEnum.ApotekGroup) {
      const roleUser = await this.userManagementService.getAssignedRoleUser(+user.payload.roleId)
      if (!roleUser) throw new BadRequestException("Please re-login. Failed search role user")

      // Assume this is HQ, have permission manage:apotek-group
      const permissionManageGroup = 'manage:apotek-group'
      if (roleUser.roleId.permission.includes(permissionManageGroup)) {
        return roles;
      }
      return roles.filter((v) => !v.permission.includes(permissionManageGroup))
    }

    /** Other than that, it must be admin */
    return roles
  }
}