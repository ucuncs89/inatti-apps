import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserManageApotekDTO } from '../type/user-dto';
import { ApotekGroupUserService } from './apotek-group-user.service';

@ApiTags('Data Apotek Group API')
@RequireAuthApp(AppsEnum.ApotekGroup, AppsEnum.Admin)
@Controller('apotek-group/:apotekGroupId/user')
export class ApotekGroupUserController {
  constructor(
    private userManagementService: ApotekGroupUserService
  ){}

  @Get(':id')
  async getUser(
    @Param('apotekGroupId')
    apotekGroupId: string,
    @Param('id')
    id: number
  ){
    try{
      return await this.userManagementService.getUser(apotekGroupId, id)
    } catch(error){
      throw new BadRequestException(error)
    }
  }

  @Get()
  async getUsers(
    @Param('apotekGroupId')
    apotekGroupId: string,
  ){
    try{
      return await this.userManagementService.getUsers(apotekGroupId)
    } catch(error){
      throw new BadRequestException(error)
    }
  }

  @Post()
  async addUser(
    @Param('apotekGroupId')
    apotekGroupId: string,
    @Body()
    body: UserManageApotekDTO
  ){
    try{
      return await this.userManagementService.addUser(apotekGroupId, body)
    } catch(error){
      throw new BadRequestException(error)
    }
  }
  
  @Patch(':id')
  async updateUser(
    @Param('apotekGroupId')
    apotekGroupId: string,
    @Param('id')
    id: number,
    @Body()
    body: UserManageApotekDTO
  ){
    try{
      return await this.userManagementService.updateUser(apotekGroupId, id, body)
    } catch(error){
      throw new BadRequestException(error)
    }
  }

  @Delete(':id')
  async deleteUser(
    @Param('apotekGroupId')
    apotekGroupId: string,
    @Param('id')
    id: number
  ){
    try{
      return await this.userManagementService.deleteUser(apotekGroupId, +id)
    } catch(error){
      throw new BadRequestException(error)
    }
  }
  
}