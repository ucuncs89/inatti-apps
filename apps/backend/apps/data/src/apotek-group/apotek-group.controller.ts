import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { ApotekHeadOffice } from '@app/database/entity/apotek-head-office/apotek-head-office.entity';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiOperation, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { ApotekHeadofficeUserService } from '../apotek-headoffice/user/apotek-headoffice-user.service';
import { ApotekGroupDetailService } from './apotek-group.service';
import { HeadofficeManageGroup } from './headoffice/headoffice-manage-group.service';
import { ApotekGroupDTO, ApotekGroupFilter, ApotekGroupUpdateDTO } from './type/apotek-group-dto';
import { ApotekGroupUserService } from './user/apotek-group-user.service';

const PermissionApotekGroup = {
  Edit: 'edit:apotek-group',
  Delete: 'delete:apotek-group',
  Create: 'create:apotek-group'
}

class ApotekGroupUpdateImageDTO {
  @ApiProperty({ type: 'string', format: 'binary' })
  thumbnail: Express.Multer.File[]
}

@ApiTags('Data Apotek Group API')
@Controller('apotek-group')
export class ApotekGroupInformationControllerBase {
  constructor(
    private apotekGroupDetail: ApotekGroupDetailService,
    private apotekGroupUserService: ApotekGroupUserService,
    private apotekHeadofficeUser: ApotekHeadofficeUserService,
    private headofficeManageGroup: HeadofficeManageGroup
  ) { }

  @ApiQuery({
    name: 'withDeleted',
    required: false,
    description: "Only admin can see withdeleted data",
  })
  @RequireAuthApp()
  @Get()
  async getAll(
    @UserCred()
    user: UserTokenData,
    @Query()
    queryFilter: ApotekGroupFilter,
  ) {
    await this.headofficeManageGroup.isHeadoffice(user, async () => {
      await this.apotekHeadofficeUser.identifyRoleUser(user, {
        ifAppHq: (roleUser) => {
          if (roleUser.apotekHeadoffice) {
            queryFilter.apotekHeadoffice = roleUser.apotekHeadoffice.id
          }
        }
      })
    })
    queryFilter.withDeleted = queryFilter.withDeleted + "" === 'true'
    return this.apotekGroupDetail.get(queryFilter)
  }

  @RequireAuthApp(AppsEnum.ApotekGroup)
  @Get('/registered/to-user')
  async getByRegisteredToUser(
    @UserCred()
    user: UserTokenData
  ) {
    return this.apotekGroupDetail.getBasedUser(user)
  }

  @RequireAuthApp()
  @Get('/:idapotekGroup')
  async get(
    @Param('idapotekGroup')
    idApotekGroup: string,
  ) {
    return await this.apotekGroupDetail.getOne(idApotekGroup)
  }

  @RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekGroup, AppsEnum.ApotekHeadOffice)
  @Post()
  async add(
    @Body()
    body: ApotekGroupDTO,
    @UserCred()
    user: UserTokenData
  ) {
    if (user.payload.apps === AppsEnum.ApotekGroup) {
      if (!user.payload.roleId) {
        throw new BadRequestException("Please re-login")
      }
      const role = await this.apotekGroupUserService.getAssignedRoleUser(+user.payload.roleId)
      const apotekGroup = await this.apotekGroupDetail.create(body, role.apotekGroup.headOffice)
      await this.apotekGroupDetail.addUsernameToApotekGroup(apotekGroup.id, user.username)
      return apotekGroup
    }

    let headoffice
    if (body.headOffice) {
      headoffice = new ApotekHeadOffice(body.headOffice)
    }

    await this.headofficeManageGroup.isHeadoffice(user, async () => {
      await this.apotekHeadofficeUser.identifyRoleUser(user, {
        ifAppHq: (roleUser) => {
          if (roleUser.apotekHeadoffice) {
            headoffice = new ApotekHeadOffice(roleUser.apotekHeadoffice.id)
          }
        }
      })
    })

    return await this.apotekGroupDetail.create(body, headoffice)
  }

  @RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekGroup, AppsEnum.ApotekHeadOffice)
  @ApiQuery({
    name: 'isDelete',
    example: "1: yes, 0: false"
  })
  @Delete('/:idapotekGroup')
  async delete(
    @Param('idapotekGroup')
    idApotekGroup: string,
    @Query('isDelete')
    isDelete: number,
    @UserCred()
    user: UserTokenData
  ) {
    await this.apotekGroupUserService.strictAccess(user, idApotekGroup, PermissionApotekGroup.Delete)
    await this.headofficeManageGroup.strictAccessUnderHO(user, idApotekGroup)
    return await this.apotekGroupDetail.delete(idApotekGroup, isDelete == 1)
  }

  @RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekGroup, AppsEnum.ApotekHeadOffice)
  @Patch('/:idapotekGroup')
  async update(
    @Param('idapotekGroup')
    idApotekGroup: string,
    @Body()
    body: ApotekGroupUpdateDTO,
    @UserCred()
    user: UserTokenData
  ) {
    await this.apotekGroupUserService.strictAccess(user, idApotekGroup, PermissionApotekGroup.Edit)
    await this.headofficeManageGroup.strictAccessUnderHO(user, idApotekGroup)
    return await this.apotekGroupDetail.update(idApotekGroup, body)
  }


  /** @deprecated: not used anymore */
  @RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekGroup)
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'thumbnail', maxCount: 1 },
  ]))
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ deprecated: true })
  @Patch('/:idapotekGroup/thumbnail')
  async updateThumbnailRef(
    @Param('idapotekGroup') idApotekGroup: string,
    @UploadedFiles()
    files: ApotekGroupUpdateImageDTO,
  ) {
    try {
      return await this.apotekGroupDetail.updateThumbnail(idApotekGroup, files.thumbnail[0])
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @RequireAuthApp(AppsEnum.ApotekHeadOffice)
  @Get('/based/headoffice')
  async getListBasedHeadOffice(
    @UserCred() user: UserTokenData,
  ) {
    return await this.headofficeManageGroup.listBasedApotekHeadoffice(user)
  }
}
