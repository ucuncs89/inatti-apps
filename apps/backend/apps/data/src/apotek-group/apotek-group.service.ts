import { UserTokenData } from '@app/authentication/types';
import { AwsService } from '@app/aws';
import { ApotekGroupDetail } from '@app/database/entity/apotek-group/apotek-group-detail.entity';
import { ApotekHeadofficeRoleUser } from '@app/database/entity/apotek-head-office/apotek-head-office-role-user.entity';
import { ApotekHeadOffice } from '@app/database/entity/apotek-head-office/apotek-head-office.entity';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, In, Repository } from 'typeorm';
import { ApotekHeadofficeUserService } from '../apotek-headoffice/user/apotek-headoffice-user.service';
import { ApotekGroupDTO, ApotekGroupFilter, ApotekGroupUpdateDTO } from './type/apotek-group-dto';
import { ApotekGroupUserService } from './user/apotek-group-user.service';

@Injectable()
export class ApotekGroupDetailService {
  constructor(
    @InjectRepository(ApotekGroupDetail)
    private ApotekGroupDetailRepository: Repository<ApotekGroupDetail>,
    private awsService: AwsService,
    private apotekGroupUser: ApotekGroupUserService
  ) { }

  repository() {
    return this.ApotekGroupDetailRepository
  }

  /**
   * Get list apotek group and detail apotek group
   * 
   * @param idApotekGroup 
   * @param withDeleted 
   * @param apotekHeadoffice 
   * @returns 
   */
  get(filter: ApotekGroupFilter) {
    const where: any = {}
    if (filter.apotekHeadoffice) {
      where.headOffice = new ApotekHeadOffice(filter.apotekHeadoffice)
    }
    if (filter.search) {
      where.name = ILike(`%${filter.search}%`)
    }
    return this.ApotekGroupDetailRepository.find({
      where,
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'headOffice'
      ],
      withDeleted: filter.withDeleted,
    });
  }

  getOne(id: string) {
    return this.ApotekGroupDetailRepository.findOne({
      where: {
        id: id
      },
      relations: [
        'headOffice'
      ],
      withDeleted: true,
    })
  }

  getApotek(idApotekGroup: string) {
    return this.ApotekGroupDetailRepository.find({
      where: {
        id: idApotekGroup,
      },
      relations: [
        'apotekList'
      ],
      withDeleted: true
    })
  }

  async getBasedUser(user: UserTokenData) {
    const roleUser = await this.apotekGroupUser.getAssignedRoleUser(+user.payload.roleId)

    if (roleUser.roleId.isHeadOffice) {
      const res = await this.ApotekGroupDetailRepository.find({
        headOffice: roleUser.apotekGroup.headOffice
      })
      return res.filter((v) => !!v)
    }

    // if non-headoffice, get all apotek group based registered role
    const registeredApotekGroup = await this.apotekGroupUser.registeredApotekGroupUser(user)
    console.log('registered apotek gorup : ', registeredApotekGroup)
    return registeredApotekGroup
  }

  async create(payload: ApotekGroupDTO, headOffice?: ApotekHeadOffice) {
    const newApotekGroup = new ApotekGroupDetail()
    Object.assign(newApotekGroup, payload)

    newApotekGroup.headOffice = headOffice

    const apotekGroup = await this.ApotekGroupDetailRepository.save(newApotekGroup)

    return apotekGroup
  }

  async update(idApotekGroup: string, body: ApotekGroupUpdateDTO) {
    const apotekGroup = await this.getOne(idApotekGroup)
    Object.assign(apotekGroup, body)
    return this.ApotekGroupDetailRepository.save(apotekGroup)
  }

  async delete(idApotekGroup: string, isDelete: boolean) {
    if (isDelete) {
      return this.ApotekGroupDetailRepository.softDelete(idApotekGroup)
    }
    // not delete
    const data = await this.getOne(idApotekGroup)
    data.deleteAt = null
    return await this.ApotekGroupDetailRepository.save(data)
  }

  async updateThumbnail(idApotekGroup: string, thumbnail: Express.Multer.File) {
    if (!thumbnail) throw new Error("Thumbnail not found")
    const apotek: ApotekGroupDetail = await this.getOne(idApotekGroup)
    if (!apotek) throw new Error("Apotek not found")

    const fileThumbnail = await this.awsService.HashAndUploadS3(thumbnail)
    apotek.thumbnail = fileThumbnail.s3.Location
    return await this.ApotekGroupDetailRepository.save(apotek)
  }

  async addUsernameToApotekGroup(apotekGroupId: string, username: string) {
    // section for apotek group user
    const ROLE_USER_HIGHEST = 1
    return await this.apotekGroupUser.addUser(apotekGroupId, {
      username: username,
      roleId: ROLE_USER_HIGHEST,
    })
  }
}
