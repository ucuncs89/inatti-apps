import { UserTokenData } from "@app/authentication/types";
import { ApotekGroupDetail } from "@app/database/entity/apotek-group/apotek-group-detail.entity";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { BadRequestException, Injectable } from "@nestjs/common";
import { ApotekHeadofficeUserService } from "../../apotek-headoffice/user/apotek-headoffice-user.service";
import { ApotekGroupDetailService } from "../apotek-group.service";

function IsArrayNullish(arr: any[] | null) {
  return (arr?.length || 0) < 1
}

@Injectable()
export class HeadofficeManageGroup {
  constructor(
    private apotekHeadofficeUser: ApotekHeadofficeUserService,
    private apotekGroup: ApotekGroupDetailService
  ) { }

  /**
   * Get list apotek group based user BM or HQ
   * this can be used for list patient or pmr
   */
  async listBasedApotekHeadoffice(user: UserTokenData): Promise<ApotekGroupDetail[]> {
    return await this.apotekHeadofficeUser.identifyRoleUser(user, {
      ifHQ: (roleUser) => {
        return this.apotekGroup.get({
          apotekHeadoffice: roleUser.apotekHeadoffice.id,
          withDeleted: true
        })
      },
      ifBM: async (roleUser) => {
        return roleUser.apotekGroup.map((v) => v.apotekGroup)
      }
    })
  }

  async isHeadoffice(user: UserTokenData, cb: () => any) {
    if (user.payload.apps !== AppsEnum.ApotekHeadOffice) return;
    // callback if apotek headoffice
    return await cb()
  }

  async isGroupUnderHO(user: UserTokenData, targetApotekGroup: string) {
    const apotekGroup = await this.apotekGroup.getOne(targetApotekGroup)
    if (!apotekGroup) return false
    let isUnderHO = true
    await this.apotekHeadofficeUser.identifyRoleUser(user, {
      ifAppHq: (roleUser) => {
        isUnderHO = roleUser.apotekHeadoffice.id === apotekGroup.headOffice.id
      },
      ifBM: (roleUser) => {
        // BM not allow to update other group
        isUnderHO = false
      },
      ifHQ: (roleUser) => {
        isUnderHO = roleUser.apotekHeadoffice.id === apotekGroup.headOffice.id
      }
    })

    return isUnderHO
  }

  async strictAccessUnderHO(user: UserTokenData, targetApotekGroup: string) {
    return await this.isHeadoffice(user, async () => {
      const isOk = await this.isGroupUnderHO(user, targetApotekGroup)
      if (!isOk) {
        throw new BadRequestException('You are not authorized to access this data')
      }
    })
  }

  /**
   * function to get list apotek group for filtering purpose
   * @param user 
   * @param reqApotekGroup commonly filled by filter.apotekGroup
   * @returns 
   */
  async getFilterApotekGroup(user: UserTokenData, reqApotekGroup: any[] | null) {
    if (!reqApotekGroup) {
      reqApotekGroup = []
    }
    const assignedApotekGroup = await this.listBasedApotekHeadoffice(user)
    const assignedApotekGroupId = assignedApotekGroup.map((v) => v.id)
    const isReqInAssigned = reqApotekGroup.every((v) => assignedApotekGroupId.includes(v))

    if (IsArrayNullish(reqApotekGroup) || !isReqInAssigned) {
      reqApotekGroup = assignedApotekGroupId
    }
    return reqApotekGroup
  }
}