import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ApotekGroupDTO {
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @ApiProperty()
  address?: string;

  @ApiProperty()
  description?: string;

  @ApiProperty()
  phonenumber?: string;

  @ApiProperty()
  thumbnail?: string;

  @ApiProperty()
  headOffice?: string;
}

export class ApotekGroupUpdateDTO extends ApotekGroupDTO { }

export class ApotekGroupFilter {
  @ApiProperty({ required: false })
  search?: string;

  @ApiProperty({ required: false })
  withDeleted?: boolean;

  @ApiProperty({ required: false })
  apotekHeadoffice?: string;
}