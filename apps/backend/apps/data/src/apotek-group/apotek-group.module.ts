import { AwsModule } from '@app/aws';
import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import awsConfig from 'config/aws.config';
import { ApotekHeadofficeModule } from '../apotek-headoffice/apotek-headoffice.module';
import { ApotekGroupInformationControllerBase } from './apotek-group.controller';
import { ApotekGroupDetailService } from './apotek-group.service';
import { HeadofficeManageGroup } from './headoffice/headoffice-manage-group.service';
import { ApotekGroupUserRoleController } from './user/apotek-group-role.controller';
import { ApotekGroupUserController } from './user/apotek-group-user.controller';
import { ApotekGroupUserService } from './user/apotek-group-user.service';

@Module({
  imports: [
    DatabaseModule,
    AwsModule.register({
      bucket: awsConfig().aws.bucket,
      uploadFolder: 'apotek-group',
    }),
    ApotekHeadofficeModule
  ],
  controllers: [
    ApotekGroupInformationControllerBase,
    ApotekGroupUserController,
    ApotekGroupUserRoleController
  ],
  providers: [
    ApotekGroupDetailService,
    ApotekGroupUserService,
    HeadofficeManageGroup
  ],
  exports: [
    ApotekGroupDetailService,
    ApotekGroupUserService,
    HeadofficeManageGroup
  ]
})
export class ApotekGroupModule { }
