import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsAll, AppsEnum } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { ResponseFile } from '@app/helper/response-file/response-file';
import { LoggerActivityService } from '@app/logger-activity';
import { LoggerActivityList } from '@app/logger-activity/logger-type';
import { FilterCommonTelemedicineList, PaginationOptionsInterface, PmrDataCreateDTO, PmrDataUpdateDTO } from '@inatti/shared/dtos';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, Response } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { Response as ResponseExpress } from "express";
import { HeadofficeManageGroup } from '../apotek-group/headoffice/headoffice-manage-group.service';
import { ApotekGroupUserService } from '../apotek-group/user/apotek-group-user.service';
import { PatientPmrSecurityService } from './patient-pmr-security.service';
import { PatientPmrService } from './patient-pmr.service';


const AppsApotek = [AppsEnum.Apotek, AppsEnum.ApotekGroup, AppsEnum.ApotekHeadOffice]

@ApiTags('PMR Data')
@Controller('patient-pmr')
export class PatientPmrController {
  constructor(
    private patientPmrService: PatientPmrService,
    private loggerActivity: LoggerActivityService,
    private apotekGroupUserService: ApotekGroupUserService,
    private patientPmrSecurityService: PatientPmrSecurityService,
    private headofficeManageGroup: HeadofficeManageGroup
  ) { }

  /**
   * -------- Area Auth Apotek --------
   */
  @RequireAuthApp(...AppsApotek)
  @Post('/pmr')
  async createPmr(
    @Body()
    data: PmrDataCreateDTO,
    @UserCred()
    user: User,
  ) {
    let res = await this.patientPmrService.create(data, user.id)

    await this.loggerActivity.log({
      type: LoggerActivityList.apotek.createPmr,
      patientId: data.pmrData.patient,
      params: {
        pmrId: res.id
      },
      user: user.id,
    })

    return res;
  }

  @RequireAuthApp(...AppsApotek)
  @Delete('/pmr/:idpmr')
  async deletePmr(
    @Param('idpmr')
    idPmr: string,
    @UserCred()
    user: User
  ) {
    await this.patientPmrSecurityService.canUserDoAction(user, idPmr)

    const pmrData = await this.patientPmrService.getDetail(idPmr)

    const res = await this.patientPmrService.deletePmr(idPmr)

    await this.loggerActivity.log({
      type: LoggerActivityList.apotek.deletPmr,
      patientId: pmrData.patient.id,
      params: {
        pmrId: pmrData.id
      },
      user: user.id,
    })

    return res;
  }

  @RequireAuthApp(...AppsApotek)
  @Patch('/pmr/:idpmr')
  async editPmr(
    @Param('idpmr')
    idPmr: string,
    @Body()
    payload: PmrDataUpdateDTO,
    @UserCred()
    user: User
  ) {
    await this.patientPmrSecurityService.canUserDoAction(user, idPmr)

    const pmrData = await this.patientPmrService.getDetail(idPmr)

    const res = await this.patientPmrService.editPmrInfo(idPmr, payload, user.id)

    await this.loggerActivity.log({
      type: LoggerActivityList.apotek.updatePmr,
      patientId: pmrData.patient.id,
      params: {
        pmrId: pmrData.id
      },
      user: user.id,
    })

    return res;
  }

  @RequireAuthApp(...AppsApotek)
  @Get('/pmr/list')
  async getPmrList(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: FilterCommonTelemedicineList,
    @UserCred()
    user: UserTokenData
  ) {
    filter = await this.detectFilterDefaultBasedUser(user, filter)
    return this.patientPmrService.listPmrApotek(paginate, filter)
  }

  /**
   * If user logged in as apotek, user must include apotek id
   * if user logged in as apotek group,as default will filter pmr based on apotek group user loggedin
   * @param user user loggedin
   * @param filter filter body
   * @returns 
   */
  async detectFilterDefaultBasedUser(user: UserTokenData, filter: FilterCommonTelemedicineList) {
    const app = user.payload?.apps
    if (!filter) {
      filter = {} as any
    }
    if (app === AppsEnum.Apotek) {
      if (!filter?.apotek?.length) {
        throw new BadRequestException("User apotek must include apotek id")
      }
    }
    if (app === AppsEnum.ApotekGroup) {
      if (filter?.apotekGroup?.length > 0) {
        return filter
      }
      const listApotekGroup = await this.apotekGroupUserService.registeredApotekGroupUser(user)
      filter.apotekGroup = listApotekGroup.map((v) => v.id)
    }
    if (app === AppsEnum.ApotekHeadOffice) {
      filter.apotekGroup = await this.headofficeManageGroup.getFilterApotekGroup(user, filter.apotekGroup)
    }
    return filter
  }

  @RequireAuthApp(...AppsApotek)
  @Get('/pmr/to-excel')
  async exportPmrData(
    @Query('filter', new FilterQueryPipe())
    filter: FilterCommonTelemedicineList,
    @Response()
    res: ResponseExpress,
    @UserCred()
    user: UserTokenData
  ) {
    filter = await this.detectFilterDefaultBasedUser(user, filter)
    return await this.patientPmrService.getExcel(filter, res)
  }

  /** --------- Area all auth -------*/
  @ApiQuery({
    name: 'idapotek',
    required: false
  })
  @RequireAuthApp(...AppsAll)
  @Get('/patient/:idpatient')
  async getPmrPatient(
    @Param('idpatient')
    idPatient: string,
    @UserCred()
    user: UserTokenData
  ) {
    if (user.payload.apps === AppsEnum.Apotek) {
      const role = await this.patientPmrSecurityService.getDetailRoleUser(+user.payload.roleId)
      const res = await this.patientPmrService.listPmrPatient(idPatient)
      return res.map((pmr) => this.patientPmrSecurityService.labelPmrEditable(pmr, role))
    }
    return await this.patientPmrService.listPmrPatient(idPatient)
  }

  @RequireAuthApp(...AppsAll)
  @Get('/pmr/:idpmr')
  async getPmrPatientDetail(
    @Param('idpmr')
    idPmr: string,
    @UserCred()
    user: UserTokenData
  ) {
    if (user?.payload?.apps === AppsEnum.Apotek && user.payload.roleId) {
      const res = await this.patientPmrService.getDetail(idPmr)
      const role = await this.patientPmrSecurityService.getDetailRoleUser(+user.payload.roleId)
      return this.patientPmrSecurityService.labelPmrEditable(res, role)
    }
    return this.patientPmrService.getDetail(idPmr)
  }

  /**
   * This API Can access by anyone
   * @param idPmr 
   * @param res 
   */
  @Get('/pmr/:idpmr/pdf')
  async getPmrPdf(
    @Param('idpmr')
    idPmr: string,
    @Response()
    res: ResponseExpress
  ) {
    let bufferFile = await this.patientPmrService.getPdf(idPmr)
    ResponseFile(res, {
      type: 'application/pdf',
      buffer: bufferFile
    })
  }
}