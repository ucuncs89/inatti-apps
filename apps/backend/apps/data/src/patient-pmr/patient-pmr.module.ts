import { DatabaseModule } from '@app/database';
import { LoggerActivityModule } from '@app/logger-activity';
import { ReportPdfModule } from '@app/report-pdf';
import { Module } from '@nestjs/common';
import { ApotekGroupModule } from '../apotek-group/apotek-group.module';
import { PatientPmrSecurityService } from './patient-pmr-security.service';
import { PatientPmrController } from './patient-pmr.controller';
import { PatientPmrService } from './patient-pmr.service';

@Module({
  imports: [
    DatabaseModule,
    ReportPdfModule,
    ApotekGroupModule,
    LoggerActivityModule.register({
      area: 'apotek'
    }),
    ApotekGroupModule
  ],
  controllers: [
    PatientPmrController
  ],
  providers: [
    PatientPmrService,
    PatientPmrSecurityService
  ]
})
export class PatientPmrModule { }
