import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity";
import { Medicine } from "@app/database/entity/medicine/medicine.entity";
import { PmrMedicine } from "@app/database/entity/pmr/pmr-medicine.entity";
import { PmrPatient } from "@app/database/entity/pmr/pmr-patient.entity";
import { User } from "@app/database/entity/user/user.entity";
import { Connection, QueryRunner } from "typeorm";
import { MedicineDTO, MedicinePmrDTO, PmrCreateDTO } from "@inatti/shared/dtos";

export class PatientPmrRunner {
  constructor(private connection: Connection) { }

  queryRunner: QueryRunner

  async startTrx() {
    this.queryRunner = this.connection.createQueryRunner();

    await this.queryRunner.connect();
    await this.queryRunner.startTransaction();
  }
  async commitTrx() {
    return await this.queryRunner.commitTransaction()
  }
  async rollbackTrx() {
    return await this.queryRunner.rollbackTransaction()
  }
  async releaseTrx() {
    return await this.queryRunner.release();
  }

  /**
   * This is shorthand code to create transaction
   * @param command 
   */
  async runTrx(command: () => any) {
    await this.startTrx()
    try {
      let res = await command()
      await this.commitTrx()
      return res;
    } catch (err) {
      await this.rollbackTrx()
      throw err
    } finally {
      await this.releaseTrx()
    }

  }

  userId: string
  apotekId: string

  /**
   * Create new PMR
   * @param data 
   * @param userId 
   * @param apotekId 
   * @returns 
   */
  async save(data: PmrCreateDTO, userId: string, apotekId: string) {
    this.userId = userId
    this.apotekId = apotekId
    if (!this.queryRunner) await this.startTrx()

    try {
      let { medicines, ...dataPmr } = data

      let pmrPatientInserted = await this.addPmrPatient(dataPmr)
      let medicinesInserted = await this.addMultiMedicinesToPmr(medicines, pmrPatientInserted.id)

      await this.commitTrx()

      pmrPatientInserted.medicines = medicinesInserted
      return pmrPatientInserted
    } catch (err) {
      await this.rollbackTrx()
      throw err;
    } finally {
      await this.releaseTrx()
    }
  }

  async addPmrPatient(dataPmr: Omit<PmrCreateDTO, 'medicines'>) {
    let newPmrPatient = new PmrPatient()
    Object.assign(newPmrPatient, dataPmr)
    newPmrPatient.insertBy = new User(this.userId)
    newPmrPatient.apotek = new ApotekDetail(this.apotekId)

    return await this.queryRunner.manager.save(newPmrPatient)
  }

  async addMultiMedicinesToPmr(medicines: MedicinePmrDTO[], pmrId: string) {
    let res = []
    for (let i = 0; i < medicines.length; i++) {
      let newPmrMedicine = await this.addMedicineToPmr(medicines[i], pmrId);

      res.push(newPmrMedicine)
    }
    return res;
  }

  async addMedicineToPmr(medicinePatient: MedicinePmrDTO, pmrId: string) {
    let newPmrMedicine = new PmrMedicine()
    let { medicine, ...dataMedicines } = medicinePatient
    Object.assign(newPmrMedicine, dataMedicines)

    newPmrMedicine.medicine = await this.getOrCreateMedicinie(medicine)
    newPmrMedicine.pmr = new PmrPatient(pmrId)

    console.log(newPmrMedicine)

    return await this.queryRunner.manager.save(newPmrMedicine)
  }

  async getOrCreateMedicinie(medicine: MedicineDTO) {
    if (!this.userId) {
      throw new Error("PatientPmrRunner require userid")
    }
    if (!this.apotekId) {
      throw new Error("PatientPmrRunner require apotekid")
    }

    if (medicine.id) {
      return new Medicine(+medicine.id)
    }
    let newMedicine = new Medicine()
    newMedicine.name = medicine.name
    newMedicine.insertBy = new User(this.userId)
    newMedicine.insertByApotek = new ApotekDetail(this.apotekId)

    return await this.queryRunner.manager.save(newMedicine)
  }

  async updatePmr(pmrId: string, pmrData: Omit<PmrCreateDTO, 'medicines'>) {
    let pmr = await this.connection.manager.findOne(PmrPatient, {
      where: {
        id: pmrId
      }
    })
    console.log(pmr)
    if (!pmr) throw new Error("Pmr not found")

    Object.assign(pmr, pmrData)
    return await this.connection.manager.save(pmr)
  }

  async deleteMedicinePmr(pmrId: string) {
    return await this.connection.manager.delete(PmrMedicine, {
      pmr: pmrId
    })
  }
}