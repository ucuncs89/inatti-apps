import { ApotekerDetail } from '@app/database/entity/apotek/apoteker-detail.entity';
import { PmrPatient } from '@app/database/entity/pmr/pmr-patient.entity';
import { BindCommonFilterToTable } from '@app/helper/query-filter/utils';
import { deepValueToXlsx } from '@app/helper/report-xlsx/to-xlsx';
import { FormatDeepKey } from '@app/helper/utils/object';
import { ReportPdfService } from '@app/report-pdf';
import { FilterCommonTelemedicineList, Pagination, PaginationOptionsInterface, PmrDataCreateDTO, PmrDataUpdateDTO } from '@inatti/shared/dtos';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from "express";
import * as _ from "lodash";
import { Connection, ILike, In, IsNull, Repository } from 'typeorm';
import { PatientPmrRunner } from './patient-pmr-runner';

@Injectable()
export class PatientPmrService {
  constructor(
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
    @InjectRepository(ApotekerDetail)
    private ApotekerDetailRepository: Repository<ApotekerDetail>,
    private connection: Connection,
    private reportPdf: ReportPdfService
  ) { }

  async create(data: PmrDataCreateDTO, userId: string) {
    let patientPmrRunner = new PatientPmrRunner(this.connection)
    try {
      return await patientPmrRunner.save(
        data.pmrData,
        userId,
        data.apotekId
      )
    } catch (err) {
      throw new BadRequestException(err)
    }
  }

  // order by created desc
  async listPmrPatient(patientId: string) {
    return this.PmrPatientRepository.find({
      where: {
        patient: patientId,
      },
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'medicines',
        'medicines.medicine',
        'apotek'
      ],
      withDeleted: true
    })
  }

  private filterTablePmr(filter: FilterCommonTelemedicineList) {
    let where: Partial<any | PmrPatient> = {}
    if (!filter) return where;

    where = BindCommonFilterToTable(filter, where)

    // filter patient is not deleted
    // because patient table is soft-delete
    where.patient = {
      deleteAt: IsNull()
    }

    if (filter.search) {
      where.case = ILike(`%${filter.search}%`)
    }

    if (filter.apotekGroup?.length) {
      if (!Array.isArray(filter.apotekGroup)) {
        filter.apotekGroup = [filter.apotekGroup]
      }
      where.apotek = {
        apotekGroup: In(filter.apotekGroup)
      }
    }
    // reset where.apotek to apotek, this will prevent searching by group
    if (filter.apotek?.length) {
      if (!Array.isArray(filter.apotek)) {
        filter.apotek = [filter.apotek]
      }
      where.apotek = In(filter.apotek)
    }
    return where
  }

  async listPmrApotek(paginate: PaginationOptionsInterface, filter: FilterCommonTelemedicineList) {
    const where: Partial<any | PmrPatient> = this.filterTablePmr(filter)
    const relations = [
      'apotek',
      'apotek.apotekGroup',
      'patient'
    ]
    const defualtOptions = {
      where,
      relations,

      withDeleted: false
    }

    const total = await this.PmrPatientRepository.count({ ...defualtOptions })

    let result: any = await this.PmrPatientRepository.find({
      ...defualtOptions,
      order: {
        createdAt: 'DESC'
      },
      skip: paginate.skip,
      take: paginate.take,
    })

    result = result.map((v) => ({
      ...v,
      patient: _.pick(v.patient, ['id', 'name', 'birthDate'])
    }))

    return new Pagination({
      results: result,
      total: total
    }, paginate)
  }

  async getDetail(pmrId: string) {
    let res: PmrPatient = await this.PmrPatientRepository.findOne({
      where: {
        id: pmrId
      },
      relations: [
        'medicines',
        'medicines.medicine',
        'apotek',
        'apotek.apotekGroup',
        'patient',
        'insertBy'
      ],
      withDeleted: true
    })

    if (!res) throw new BadRequestException(`Pmr ${pmrId} not found`)
    res.insertBy = _.pick(res.insertBy, ['id', 'fullname', 'username']) as any

    res['apoteker'] = await this.getApotekerInfo(res.insertBy.id, res.apotek.id)

    return res;
  }

  async editPmrInfo(pmrId: string, payload: PmrDataUpdateDTO, userId: string) {
    let patientPmrRunner = new PatientPmrRunner(this.connection)

    let { medicines, ...pmrData } = payload.pmrData

    patientPmrRunner.userId = userId
    patientPmrRunner.apotekId = payload.apotekId

    return await patientPmrRunner.runTrx(async () => {
      let res = await patientPmrRunner.updatePmr(pmrId, pmrData)
      if (!medicines) {
        return res
      }
      await patientPmrRunner.deleteMedicinePmr(pmrId)
      await patientPmrRunner.addMultiMedicinesToPmr(medicines, pmrId)
      return res;
    })
  }

  async deletePmr(pmrId: string) {
    return await this.PmrPatientRepository.delete({
      id: pmrId
    })
  }

  async getPdf(pmrId: string) {
    let pmr = await this.PmrPatientRepository.findOne({
      where: {
        id: pmrId
      },
      relations: [
        'patient'
      ],
      withDeleted: true,
    })
    if (!pmr.patient) throw new BadRequestException('No patient found')
    let host = this.reportPdf.hostPdftemplateWeb
    let urlPage = `${host}/pmr?pmrId=${pmrId}&patientId=${pmr.patient.id}`

    return await this.reportPdf.generatePdfFromUrl(urlPage)
  }

  async getExcel(filter: FilterCommonTelemedicineList, res: Response) {
    const where = this.filterTablePmr(filter)
    let listPmr: PmrPatient[] = await this.PmrPatientRepository.find({
      where: where,
      relations: [
        'patient',
        'medicines',
        'medicines.medicine',
        'apotek',
        'apotek.apotekGroup',
        'insertBy'
      ],
      withDeleted: true
    })

    /** Re-map the medicine data */
    listPmr = listPmr.map((v) => {
      let medicines = v.medicines.map((med) => {
        if (!med.medicine) return null
        return `${med.medicine.name} (${med.amount} ${med.amountUnit})`
      }).filter((v) => !!v)

      return {
        ...v,
        summaryMed: medicines.join(','),
      }
    })


    const formatValueXlsx: FormatDeepKey = {
      "Id PMR": "id",
      "Nama Dokter": "docterName",
      "Kasus": "case",
      "Catatan": "note",
      "File": "attachment",
      "bukti konsultasi": "proofConsultation",
      "Tanggal dibuat": "createdAt",
      "Pasien Id": "patient.id",
      "Pasien Nama": "patient.name",
      "Pasien Tempat Lahir": "patient.placebirth",
      "Pasien Tanggal lahir": "patient.datebirth",
      "Pasien Jenis kelamin": "patient.gender",
      "Apotek Id": "apotek.id",
      "Apotek Nama": "apotek.name",
      "Unit Bisnis Id": "apotek.apotekGroup.id",
      "Unit Bisnis Nama": "apotek.apotekGroup.name",
      "Apoteker username": "insertBy.username",
      "Obat": "summaryMed"
    }

    return deepValueToXlsx(res, listPmr, formatValueXlsx)
  }

  async getApotekerInfo(userId: string, apotekId: string) {
    console.log(userId, apotekId)
    return this.ApotekerDetailRepository.findOne({
      where: {
        user: {
          id: userId
        },
        apotek: {
          id: apotekId
        }
      }
    })
  }
}