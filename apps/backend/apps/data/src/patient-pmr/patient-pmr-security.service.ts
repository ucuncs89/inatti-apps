import { UserTokenData } from "@app/authentication/types";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { PmrPatient } from "@app/database/entity/pmr/pmr-patient.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

@Injectable()
export class PatientPmrSecurityService {
  constructor(
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
  ) { }

  async getCreatorPmr(pmrId: string) {
    const pmr = await this.PmrPatientRepository.findOne({
      where: {
        id: pmrId,
      },
      relations: [
        'apotek'
      ]
    })
    if (!pmr) throw new BadRequestException('Pmr not found')
    if (!pmr.apotek) throw new BadRequestException('Pmr are not owned by anybody')
    return pmr.apotek
  }

  async getDetailRoleUser(roleId: number) {
    const role = await this.ApotekRoleUserRepository.findOne({
      where: { id: roleId, },
      relations: [
        'apotekDetail',
        'apotekDetail.apotekGroup',
        'apotekDetail.apotekGroup.headOffice',
      ],
      withDeleted: true
    })
    if (!role) throw new BadRequestException("User role not found")
    return role
  }

  async canUserDoAction(user: UserTokenData, pmrId: string) {
    const apotek = await this.getCreatorPmr(pmrId)
    const roleUser = await this.getDetailRoleUser(+user.payload.roleId)
    if (apotek.id !== roleUser.apotekDetail.id) {
      throw new BadRequestException("Pmr not belongs to this user apotek")
    }
  }

  labelPmrEditable(pmr: PmrPatient, roleApotek: ApotekRoleUser) {
    return {
      ...pmr,
      isEditable: pmr?.apotek?.id === roleApotek?.apotekDetail?.id
    }
  }
}
