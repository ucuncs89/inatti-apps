import { Module } from '@nestjs/common';
import { PatientPmrModule } from './patient-pmr/patient-pmr.module';
import { MedicineModule } from './medicine/medicine.module';
import { PatientModule } from './patient/patient.module';
import { ApotekGroupModule } from './apotek-group/apotek-group.module';
import { ApotekModule } from './apotek/apotek.module';
import { ApotekerModule } from './apoteker/apoteker.module';
import { DashboardReportModule } from './dashboard-report/dashboard-report.module';
import { VaksinModule } from './vaksin/vaksin.module';
import { TenancyModule } from './tenancy/tenancy.module';
import { ApotekDashboardModule } from './dashboard-report/apotek-group/apotek-dashboard.module';
import { PaymentV2Module } from './payment-v2/payment-v2.module';
import { ApotekHeadofficeModule } from './apotek-headoffice/apotek-headoffice.module';
@Module({
  imports: [
    PatientPmrModule,
    MedicineModule,
    PatientModule,
    ApotekGroupModule,
    ApotekModule,
    ApotekerModule,
    DashboardReportModule,
    ApotekDashboardModule,
    VaksinModule,
    TenancyModule,
    PaymentV2Module,
    ApotekHeadofficeModule,
  ],
  controllers: [],
  providers: [],
})
export class DataModule { }
