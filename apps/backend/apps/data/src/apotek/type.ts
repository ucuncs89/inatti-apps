import { ApotekDetailStatus } from "@app/database/entity/apotek/type"
import { PaginationOptionsInterface } from "@app/helper/paginate"
import { ApiProperty } from "@nestjs/swagger"

export class ApotekListQuery {
  @ApiProperty({
    required: false,
  })
  search?: string

  @ApiProperty({ required: false })
  filterProvinsi?: string

  @ApiProperty({ required: false })
  filterKabupaten?: string

  @ApiProperty({ required: false })
  filterCoordinate?: string

  @ApiProperty({ required: false })
  filterApotekGroup?: string[];

  @ApiProperty({ required: false })
  haveApoteker?: boolean;

  pagination: PaginationOptionsInterface
  withDeleted?: boolean
  onlyValidated?: boolean
}

export class VerifyApotekDTO {
  @ApiProperty({ required: true, example: ApotekDetailStatus.APPROVED })
  status: ApotekDetailStatus
}

export class UpdateApotekGroupDTO {
  @ApiProperty({ required: true })
  apotekGroupId: string
}

export class ApotekVerifyNotificationDTO {
  @ApiProperty({ required: true, example: 'phone/email' })
  channel: string
  /**
   * Format : `user:{id}`
   */
  @ApiProperty({ required: true })
  idDestination: string
}