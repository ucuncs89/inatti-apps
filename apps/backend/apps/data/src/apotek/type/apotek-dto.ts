import { ApotekDetailStatus } from "@app/database/entity/apotek/type";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ApotekDetailUpdateDTO {
  @IsNotEmpty()
  @ApiProperty()
  name?: string;

  @ApiProperty()
  address?: string;

  @ApiProperty()
  description?: string;

  @ApiProperty()
  phonenumber?: string;

  @ApiProperty()
  thumbnail?: string;

  @ApiProperty({ example: "true/false", description: "null = active" })
  isActive?: boolean;

  @ApiProperty()
  coord?: String;

  @ApiProperty()
  kabupaten?: String;

  @ApiProperty()
  provinsi?: String;

  @ApiProperty()
  typeApotek?: string

  @ApiProperty()
  suratIzinNo?: string

  @ApiProperty()
  suratIzinFile?: string

  validateStatus?: ApotekDetailStatus;
}

export class ApotekDetailCreateDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  address?: string;

  @ApiProperty()
  description?: string;

  @ApiProperty()
  phonenumber?: string;

  @ApiProperty()
  thumbnail?: string;

  @ApiProperty()
  typeApotek?: string

  @ApiProperty()
  suratIzinNo?: string

  @ApiProperty()
  suratIzinFile?: string

  @ApiProperty({ example: "2022-01-02", description: "null = active" })
  notActiveAt?: string;

  @ApiProperty()
  apotekGroup: string;

  @ApiProperty()
  nomorSIA: string;

  validateStatus?: ApotekDetailStatus;
}