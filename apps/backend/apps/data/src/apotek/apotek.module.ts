import { AwsModule } from '@app/aws';
import { ConfigAppModule } from '@app/config-app';
import { DatabaseModule } from '@app/database';
import { EmailServiceModule } from '@app/email-service';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { LoggerActivityModule } from '@app/logger-activity';
import { Module } from '@nestjs/common';
import awsConfig from 'config/aws.config';
import { ApotekNotificationService } from './apotek-notify.service';
import { ApotekController } from './apotek.controller';
import { ApotekService } from './apotek.service';
import { ApotekStatusController } from './status/apotek-status.controller';
import { ApotekStatusService } from './status/apotek-status.service';
import { ApotekUserRoleController } from './user/apotek-user-role.controller';
import { ApotekUserController } from './user/apotek-user.controller';
import { ApotekUserService } from './user/apotek-user.service';

@Module({
  imports: [
    DatabaseModule,
    LoggerActivityModule.register({
      area: 'admin'
    }),
    AwsModule.register({
      bucket: awsConfig().aws.bucket,
      uploadFolder: 'apotek',
    }),
    EmailServiceModule,
    WhatsappServiceModule,
    ConfigAppModule
  ],
  controllers: [
    ApotekController,
    ApotekUserController,
    ApotekUserRoleController,
    ApotekStatusController,
  ],
  providers: [
    ApotekService,
    ApotekUserService,
    ApotekStatusService,
    ApotekNotificationService
  ],
  exports: [
    ApotekService,
    ApotekUserService
  ]
})
export class ApotekModule { }
