import { AwsService } from '@app/aws';
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { Kabupaten } from '@app/database/entity/region/kabupaten.entity';
import { Provinsi } from '@app/database/entity/region/provinsi.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { MeasureGeoToMeter } from '@app/helper/utils';
import { BadRequestException, Body, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, ILike, In, ObjectLiteral, Repository, SelectQueryBuilder } from 'typeorm';
import { ApotekListQuery, ApotekVerifyNotificationDTO, UpdateApotekGroupDTO } from './type';
import { ApotekDetailCreateDTO, ApotekDetailUpdateDTO } from './type/apotek-dto';
import * as _ from "lodash";
import { PmrPatient } from '@app/database/entity/pmr/pmr-patient.entity';
import { ShouldArray } from '@app/helper/utils/array';
import { UserTokenData } from '@app/authentication/types';
import { ApotekUserService } from './user/apotek-user.service';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { ApotekGroupDetail } from '@app/database/entity/apotek-group/apotek-group-detail.entity';
import { ApotekDetailStatus } from '@app/database/entity/apotek/type';

type ApotekDetailWhere = FindConditions<ApotekDetail>[] | FindConditions<ApotekDetail> | ObjectLiteral | string

@Injectable()
export class ApotekService {
  constructor(
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
    private awsService: AwsService,
    private apotekUserService: ApotekUserService
  ) { }

  defaultRelations = [
    'apotekGroup',
    'provinsi',
    'kabupaten',
  ]

  repository() {
    return this.ApotekDetailRepository
  }

  async getAll(options: ApotekListQuery) {
    if (options.filterCoordinate) {
      return await this.getApotekNearCoord(options)
    }

    const { where, relations, cleaner } = this.queryToWhere(options)

    let res = await this.ApotekDetailRepository.find({
      where,
      relations: relations,
      skip: options.pagination.skip,
      take: options.pagination.take,
      withDeleted: options.withDeleted,
      order: {
        createdAt: 'DESC'
      }
    })

    res = cleaner(res)

    const total = await this.ApotekDetailRepository.count({
      where,
      relations: relations
    })

    return new Pagination({
      results: res,
      total,
    }, options.pagination)
  }

  queryToWhere(options: ApotekListQuery) {
    let where: ApotekDetailWhere = {}

    if (options.search) {
      where.name = ILike(`%${options.search}%`)
    }
    if (options.filterProvinsi && options.filterKabupaten) {
      where.provinsi = new Provinsi(+options.filterProvinsi)
      where.kabupaten = new Kabupaten(+options.filterKabupaten)
    }

    if (options.filterApotekGroup) {
      where.apotekGroup = In(ShouldArray(options.filterApotekGroup))
    }

    if (options.onlyValidated) {
      where.validateStatus = ApotekDetailStatus.APPROVED
    }

    if (String(options.haveApoteker) === 'true') {
      const tempWhere = { ...where }
      where = (qs: SelectQueryBuilder<ApotekDetail>) => {
        /**
         * Search apoteker with qualified data
         * source code just like at : apps/backend/apps/data/src/apoteker/apoteker.service.ts:34
         */
        qs.where("ApotekDetail__apoteker.id IS NOT NULL")
        qs.andWhere(`"ApotekDetail__apoteker"."nomorSIPA" IS NOT NULL`)
        qs.andWhere(`"ApotekDetail__apoteker"."nomorSTRA" IS NOT NULL`)
        qs.andWhere(`"ApotekDetail__apoteker".phonenumber IS NOT NULL`)
        qs.andWhere(`"ApotekDetail__apoteker".name IS NOT NULL`)
        if (tempWhere && Object.keys(tempWhere).length > 0) {
          qs.andWhere(tempWhere)
        }
      }
    }

    return {
      where,
      relations: [...this.defaultRelations, "apoteker"],
      cleaner: (data: ApotekDetail[]) => {
        if (!data || !data.length) return []
        return data.map((d) => {
          if (d.apoteker) {
            delete d.apoteker
          }
          return d
        })
      }
    }
  }

  async getApotekNearCoordData(options: ApotekListQuery) {
    const { where, relations, cleaner } = this.queryToWhere(options)

    let res = await this.ApotekDetailRepository.find({
      where: where,
      relations: relations,
      withDeleted: options.withDeleted,
      order: {
        createdAt: 'DESC'
      }
    })

    res = cleaner(res)

    let filtered = this.filterByCoord(options.filterCoordinate, res)
    return filtered
  }

  async getApotekNearCoord(options: ApotekListQuery) {
    let filtered = await this.getApotekNearCoordData(options)

    filtered = _.slice(filtered, options.pagination.skip, options.pagination.skip + options.pagination.take)

    return new Pagination({
      results: filtered,
      total: filtered.length,
    }, options.pagination)
  }

  filterByCoord(nearCoordinate: string, listApotek: ApotekDetail[]) {
    const posUser = nearCoordinate.split(',').map((v) => +v)
    let listApotekTmp = []
    for (let i = 0; i < listApotek.length; i++) {
      const apotek = listApotek[i];
      if (!apotek.coord) continue;

      const posFaskes = apotek.coord.split(',').map((v) => +v)
      const distance = MeasureGeoToMeter(posUser, posFaskes)
      apotek['distanceMeter'] = distance

      // only save to faskes that fill coordiante
      listApotekTmp.push(apotek)
    }
    // sort from nearby
    listApotekTmp = listApotekTmp.sort((a, b) => a['distanceMeter'] - b['distanceMeter'])

    return listApotekTmp
  }

  async get(idApotek: string, withDeleted = false) {
    const res = await this.ApotekDetailRepository.findOne({
      where: {
        id: idApotek,
      },
      relations: [
        'apotekGroup',
        'provinsi',
        'kabupaten'
      ],
      withDeleted
    })
    if (!res) {
      throw new Error("Apotek not found")
    }
    return res;
  }

  async whereGroup(idGroupApotek: string | string[], withDeleted = false, paginate: PaginationOptionsInterface) {
    let apoteks = await this.ApotekDetailRepository.find({
      where: {
        apotekGroup: In(ShouldArray(idGroupApotek))
      },
      relations: [
        'apotekGroup'
      ],
      skip: paginate.skip,
      take: paginate.take,
      order: {
        createdAt: 'DESC'
      },
      withDeleted,
    })

    const pmrPromises = apoteks.map(async (apotek, i) => {
      Object.assign(apotek, {
        totalPmr: await this.getTotalPmr(apotek.id)
      })
      return apotek
    });

    apoteks = await Promise.all(pmrPromises)

    const total = await this.ApotekDetailRepository.count({
      where: {
        apotekGroup: In(ShouldArray(idGroupApotek))
      },
      relations: [
        'apotekGroup'
      ]
    })

    return new Pagination({
      results: apoteks,
      total,
    }, paginate)
  }

  async getTotalPmr(apotekId: string) {
    return await this.PmrPatientRepository.count({
      where: {
        apotek: apotekId
      }
    })
  }

  async create(data: ApotekDetailCreateDTO, userCreator: UserTokenData) {
    let apotek = new ApotekDetail()
    Object.assign(apotek, data)
    apotek = await this.ApotekDetailRepository.save(apotek)

    /** Create account apotek for user apotek group */
    if (userCreator.payload.apps === AppsEnum.ApotekGroup) {
      const ROLE_HIGHEST = 1
      await this.apotekUserService.addUser(apotek.id, {
        roleId: ROLE_HIGHEST,
        username: userCreator.username
      })
    }
    return apotek
  }

  async update(idApotek: string, body: ApotekDetailUpdateDTO) {
    const apotek = await this.ApotekDetailRepository.findOne({
      where: {
        id: idApotek
      },
      relations: [
        'kabupaten',
        'provinsi',
        'apotekGroup',
      ],
      withDeleted: true
    })
    if (!apotek) throw new Error("Apotek not found")

    if (body.isActive === false) {
      apotek.notActiveAt = new Date()
    } else if (body.isActive === true) {
      apotek.notActiveAt = null
    }
    delete body.isActive

    Object.assign(apotek, body)

    return this.ApotekDetailRepository.save(apotek)
  }

  async updateThumbnail(idApotek: string, thumbnail: Express.Multer.File) {
    if (!thumbnail) throw new Error("Thumbnail not found")
    const apotek: ApotekDetail = await this.ApotekDetailRepository.findOne({
      where: {
        id: idApotek
      },
      withDeleted: true
    });
    if (!apotek) throw new Error("Apotek not found")

    const fileThumbnail = await this.awsService.HashAndUploadS3(thumbnail)
    apotek.thumbnail = fileThumbnail.s3.Location
    return await this.ApotekDetailRepository.save(apotek)
  }

  async updateLogo(idApotek: string, logo: Express.Multer.File) {
    if (!logo) throw new Error("logo not found")
    const apotek: ApotekDetail = await this.ApotekDetailRepository.findOne({
      where: {
        id: idApotek
      },
      withDeleted: true
    });
    if (!apotek) throw new Error("Apotek not found")

    const filelogo = await this.awsService.HashAndUploadS3(logo)
    apotek.logo = filelogo.s3.Location
    return await this.ApotekDetailRepository.save(apotek)
  }

  async delete(idApotek: string, isDelete: boolean) {
    if (isDelete) {
      return this.ApotekDetailRepository.softDelete(idApotek)
    }
    const data = await this.get(idApotek, true)
    data.deletedAt = null
    return this.ApotekDetailRepository.save(data)
  }

  async listContactApotek(idApotek: string) {
    const listUser = await this.apotekUserService.getUsers(idApotek)
    const detailApotek = await this.get(idApotek)

    function getIdContact(type: string, id: string) {
      return `${type}:${id}`
    }

    const listContact: {
      [key: string]: {
        type?: string,
        name?: string,
        phone?: string,
        id?: string
      }[]
    } = {
      phone: [],
      email: []
    }

    const listContactPhone = listUser.map((user) => ({
      type: 'user',
      name: user.userId.fullname,
      phone: user.userId.phoneNumber,
      id: getIdContact('user', user.userId.id)
    }))
    listContactPhone.push({
      type: 'apotek',
      name: detailApotek.name,
      phone: detailApotek.phonenumber,
      id: getIdContact('apotek', detailApotek.id)
    })
    listContact.phone = listContactPhone.filter((v) => !!v.phone)

    const listContactEmail = listUser.map((user) => ({
      type: 'user',
      name: user.userId.fullname,
      email: user.userId.email,
      id: getIdContact('user', user.userId.id)
    }))
    listContact.email = listContactEmail.filter((v) => !!v.email)

    return listContact
  }

  async updateApotekGroup(idApotek: string, payload: UpdateApotekGroupDTO) {
    if (!payload.apotekGroupId) throw new BadRequestException('Apotek group id is required')
    const apotek = await this.get(idApotek)
    apotek.apotekGroup = new ApotekGroupDetail(payload.apotekGroupId)
    return await this.ApotekDetailRepository.save(apotek)
  }
}
