import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { BadRequestException, Body, Controller, Delete, Get, Param, ParseBoolPipe, Patch, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiOperation, ApiProperty, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApotekService } from './apotek.service';
import { AppsAllowAccessApotek } from './const';
import { ApotekListQuery, ApotekVerifyNotificationDTO, UpdateApotekGroupDTO, VerifyApotekDTO } from './type';
import { ApotekDetailCreateDTO, ApotekDetailUpdateDTO } from './type/apotek-dto';
import { ApotekStatusResponse } from './api-response'
import { LoggerActivityService } from '@app/logger-activity';
import { LoggerActivityList } from "@app/logger-activity/logger-type";
import { ApotekNotificationService } from './apotek-notify.service';
import { ExampleResponseOk } from '@app/helper/utils/api-response';
import { ApotekDetailStatus } from '@app/database/entity/apotek/type';

class ApotekUpdateImageDTO {
  @ApiProperty({ type: 'string', format: 'binary' })
  thumbnail: Express.Multer.File[]
}

/**
 * Apotek Information Base Controller on Apotek
 */
@ApiTags('Data Apotek API')
@Controller('apotek')
export class ApotekController {
  constructor(
    private apotekService: ApotekService,
    private logActivityService: LoggerActivityService,
    private apotekNotifyService: ApotekNotificationService
  ) { }

  @ApiQuery({
    name: 'withDeleted',
    required: false,
    description: 'Only admin can see withdeleted data',
  })
  @RequireAuthApp()
  @Get()
  async getAll(
    @UserCred()
    user: UserTokenData,
    @Query()
    query: ApotekListQuery,
    @Query('paginate', PaginationPipe)
    pagination: PaginationOptionsInterface,
    @Query('withDeleted')
    withDeleted: boolean
  ) {
    query.pagination = pagination
    if (withDeleted) {
      if (typeof withDeleted === 'string') {
        withDeleted = withDeleted === 'true'
      }
      query.withDeleted = withDeleted
    }

    if (query.withDeleted && user.payload.apps !== AppsEnum.Admin) {
      throw new BadRequestException('You are not authorized to access this data')
    }

    /** Only if user from apps user, show only validated */
    if (user.payload.apps === AppsEnum.User) {
      query.onlyValidated = true
    }

    return await this.apotekService.getAll(query)
  }

  @RequireAuthApp()
  @Get('filter/by-group')
  async getWhereGroup(
    @Query('groupApotekId')
    groupApotekId: string,
    @Query('withDeleted', ParseBoolPipe)
    withDeleted: boolean,
    @Query('paginate', PaginationPipe)
    pagination: PaginationOptionsInterface,
  ) {
    const groups = groupApotekId.split(',')
    return await this.apotekService.whereGroup(groups, withDeleted, pagination)
  }

  @RequireAuthApp()
  @Get('/:idapotek')
  async get(
    @Param('idapotek')
    idApotek: string,
  ) {
    return await this.apotekService.get(idApotek, true)
  }

  @RequireAuthApp(AppsEnum.Admin, AppsEnum.ApotekHeadOffice)
  @Post()
  async create(
    @Body()
    data: ApotekDetailCreateDTO,
    @UserCred()
    user: UserTokenData
  ) {
    /**
     * new apotek that created by admin and headoffice automatic approved
     */
    data.validateStatus = ApotekDetailStatus.APPROVED
    return await this.apotekService.create(data, user)
  }

  @RequireAuthApp(...AppsAllowAccessApotek)
  @Patch('/:idapotek')
  async update(
    @Param('idapotek')
    idApotek: string,
    @Body()
    body: ApotekDetailUpdateDTO
  ) {
    return await this.apotekService.update(idApotek, body)
  }

  @RequireAuthApp(...AppsAllowAccessApotek)
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'thumbnail', maxCount: 1 },
  ]))
  @ApiConsumes('multipart/form-data')
  @Patch('/:idapotek/thumbnail')
  async updateThumbnailRef(
    @Param('idapotek') idApotek: string,
    @UploadedFiles()
    files: ApotekUpdateImageDTO,
  ) {
    return await this.apotekService.updateThumbnail(idApotek, files.thumbnail[0])
  }

  @RequireAuthApp(...AppsAllowAccessApotek)
  @ApiOperation({
    description: "Test API menggunakan postman, type multipart/form-data, property file adalah : 'logo'",
  })
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'logo', maxCount: 1 },
  ]))
  @ApiConsumes('multipart/form-data')
  @Patch('/:idapotek/logo')
  async updatelogoRef(
    @Param('idapotek') idApotek: string,
    @UploadedFiles()
    files: { logo: Express.Multer.File[] },
  ) {
    return await this.apotekService.updateLogo(idApotek, files.logo[0])
  }

  @RequireAuthApp(...AppsAllowAccessApotek)
  @ApiQuery({
    name: 'isDelete',
    example: "1: yes, 0: false"
  })
  @Delete('/:idApotek')
  async delete(
    @Param('idApotek')
    idApotek: string,
    @Query('isDelete')
    isDelete: number
  ) {
    return await this.apotekService.delete(idApotek, isDelete == 1)
  }

  @ExampleResponseOk(ApotekStatusResponse)
  @RequireAuthApp(AppsEnum.Admin)
  @Get('/:idapotek/contact')
  async listContactApotek(
    @Param('idapotek') idApotek: string
  ) {
    return await this.apotekService.listContactApotek(idApotek)
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Patch('/:idapotek/verify')
  async verifyApotek(
    @Param('idapotek') idApotek: string,
    @Body() body: VerifyApotekDTO,
    @UserCred() user: UserTokenData
  ) {
    const existing = await this.apotekService.get(idApotek, true)
    if (existing.validateStatus === body.status) {
      return existing
    }

    const res = await this.apotekService.update(idApotek, {
      validateStatus: body.status
    })

    await this.logActivityService.log({
      user,
      tag: `admin-verify-apotek:${res.id}`,
      type: LoggerActivityList.adminVerifyApotek.verifyApotek,
      params: {
        apotekName: res.name,
        status: body.status,
      }
    })

    return res;
  }


  @RequireAuthApp(AppsEnum.Admin)
  @Patch('/:idapotek/update-apotek-group')
  async updateApotekGroup(
    @Param('idapotek') idapotek: string,
    @Body() body: UpdateApotekGroupDTO,
  ) {
    return await this.apotekService.updateApotekGroup(idapotek, body)
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Patch('/:idapotek/verify/notification')
  async verifyApotekNotification(
    @Param('idapotek') idapotek: string,
    @Body() body: ApotekVerifyNotificationDTO,
  ) {
    return await this.apotekNotifyService.verifyApotekNotification(idapotek, body)
  }
}