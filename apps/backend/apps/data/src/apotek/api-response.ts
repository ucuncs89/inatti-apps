export const ApotekStatusResponse = {
  "phone": [
    {
      "type": "user",
      "name": "Rio chandra",
      "phone": "082230475617",
      "id": "user:515963f5-273e-49de-ae4d-a81076c3390e"
    },
    {
      "type": "apotek",
      "name": "INi yang banyak",
      "phone": "82230475617",
      "id": "apotek:7380cbd4-1828-4875-a0c5-295fb4682591"
    }
  ],
  "email": [
    {
      "type": "user",
      "name": "sahaware apoteker",
      "email": "sahaware-apoteker@gmail.com",
      "id": "user:f1046c19-d3dd-48e3-8c37-21370f736a71"
    },
    {
      "type": "user",
      "name": "apoteker2",
      "email": "apoteker22@gmail.com",
      "id": "user:b24642ad-91b7-45fe-a067-52b7f954a7d7"
    },
    {
      "type": "user",
      "name": "Rio",
      "email": "Test123123@gmail.com",
      "id": "user:6901b5cf-9b03-4ee6-8c8f-e4f913d4b345"
    },
    {
      "type": "user",
      "name": "Rio",
      "email": "Test123123@gmail.com",
      "id": "user:0640bf1f-66c5-418d-a39b-ee0cd6e3a13c"
    },
    {
      "type": "user",
      "name": "Rio",
      "email": "rio@gmail.com",
      "id": "user:d42f8fbe-5c52-4e21-a6da-55f1b730a7c6"
    },
    {
      "type": "user",
      "name": "Rio chandra",
      "email": "sahaware@gmail.com",
      "id": "user:515963f5-273e-49de-ae4d-a81076c3390e"
    }
  ]
}