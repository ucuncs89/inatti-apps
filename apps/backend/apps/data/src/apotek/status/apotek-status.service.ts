import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { ApotekerDetail } from "@app/database/entity/apotek/apoteker-detail.entity";
import { ApotekDetailStatus } from "@app/database/entity/apotek/type";
import { User } from "@app/database/entity/user/user.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

interface ResponseStatus {
  status: boolean;
  message?: string;
  messagId?: string
}

@Injectable()
export class ApotekStatusService {
  constructor(
    @InjectRepository(ApotekerDetail)
    private ApotekerDetailRepository: Repository<ApotekerDetail>,
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
  ) { }

  async getStatus(user: User, roleUser: ApotekRoleUser): Promise<Record<string, ResponseStatus>> {
    return {
      isApotekerActive: await this.isApotekerActive(user, roleUser),
      isApotekActice: await this.isApotekActice(user, roleUser),
    }
  }

  private isAdmin(roleUser: ApotekRoleUser) {
    const RoleAdmin = 1
    return roleUser.roleId.id === RoleAdmin
  }

  private async isApotekerActive(user: User, roleUser: ApotekRoleUser): Promise<ResponseStatus> {
    const apoteker = await this.ApotekerDetailRepository.findOne({
      where: {
        user: {
          id: user.id
        },
        apotek: {
          id: roleUser.apotekDetail.id
        }
      }
    })

    if (this.isAdmin(roleUser)) {
      return {
        status: true,
        message: 'User is admin, no need to check apoteker status',
        messagId: 'user-is-admin'
      }
    }

    if (!apoteker) return {
      status: false,
      message: 'Apoteker information is empty, please fill it first',
      messagId: 'apoteker-information-is-empty'
    }

    if ([
      apoteker.nomorSIPA,
      apoteker.nomorSTRA,
      apoteker.name,
      apoteker.phonenumber,
    ].some((v) => !v)) {
      return {
        status: false,
        message: 'Information apoteker not filled completely',
        messagId: 'apoteker-information-not-filled-completely'
      }
    }

    return {
      status: true
    }
  }

  private async isApotekActice(user: User, roleUser: ApotekRoleUser): Promise<ResponseStatus> {
    const apotekId = roleUser.apotekDetail.id
    const apotek = await this.ApotekDetailRepository.findOne(apotekId)
    const apotekStatus = apotek.validateStatus || 'WAITING'

    return {
      status: apotekStatus === ApotekDetailStatus.APPROVED,
      message: `Apotek status is ${apotekStatus}`,
      messagId: `apotek-status-is-${apotekStatus.toLowerCase()}`
    }
  }
}