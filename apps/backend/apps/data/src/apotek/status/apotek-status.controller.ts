import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { ExampleResponseOk } from "@app/helper/utils/api-response";
import { Controller, Get } from "@nestjs/common";
import { ApotekUserService } from "../user/apotek-user.service";
import { ApotekStatusService } from "./apotek-status.service";

@Controller('apotek-status')
export class ApotekStatusController {
  constructor(
    private readonly apotekStatusService: ApotekStatusService,
    private readonly apotekUserService: ApotekUserService
  ) {
  }

  @ExampleResponseOk({
    "isApotekerActive": {
      "status": false,
      "message": "Information apoteker not filled completely",
      "messagId": "apoteker-information-not-filled-completely"
    },
    "isApotekActice": {
      "status": false,
      "message": "Apotek status is WAITING",
      "messagId": "apotek-status-is-waiting"
    },
  })
  @RequireAuthApp(AppsEnum.Apotek)
  @Get('/user')
  async getStatus(
    @UserCred() user: UserTokenData
  ) {
    const roleUser = await this.apotekUserService.getUserRoleLoggedin(user)
    return this.apotekStatusService.getStatus(user, roleUser)
  }
}