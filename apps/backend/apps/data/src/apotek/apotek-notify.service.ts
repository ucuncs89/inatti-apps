import { ConfigAppService } from "@app/config-app";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { ApotekDetailStatus } from "@app/database/entity/apotek/type";
import { EmailServiceService } from "@app/email-service";
import { WhatsappService } from "@app/email-service/whatsapp-service/whatsapp/whatsapp.service";
import { BadRequestException, Injectable } from "@nestjs/common";
import { ApotekService } from "./apotek.service";
import { ApotekVerifyNotificationDTO } from "./type";
import { ApotekUserService } from "./user/apotek-user.service";

@Injectable()
export class ApotekNotificationService {
  constructor(
    private apotekService: ApotekService,
    private apotekUserService: ApotekUserService,
    private emailService: EmailServiceService,
    private whatsappService: WhatsappService,
    private configAppService: ConfigAppService
  ) { }

  async verifyApotekNotification(idApotek: string, payload: ApotekVerifyNotificationDTO) {
    const apotek = await this.apotekService.get(idApotek)
    if (!apotek.validateStatus) {
      throw new BadRequestException('Apotek are not have validate status')
    }
    const destination = await this.getInfoDestinations(idApotek, payload.idDestination)
    if (payload.channel === 'email') {
      await this.sendEmail(destination.email, apotek.validateStatus)
      return {
        message: 'Email sent'
      }
    } else if (payload.channel === 'phone') {
      await this.sendWhatsapp(destination.phone, apotek.validateStatus)
      return {
        message: 'Whatsapp sent'
      }
    }
  }

  async getInfoDestinations(idApotek: string, idDestination: string): Promise<{
    email?: string,
    phone?: string
  }> {
    const [type, id] = idDestination.split(':')
    if (type === 'user') {
      const user = await this.apotekUserService.getUserRole(id, idApotek) as ApotekRoleUser
      return {
        email: user.userId.email,
        phone: user.userId.phoneNumber
      }
    }
    if (type === 'apotek') {
      const apotek = await this.apotekService.get(idApotek)
      return {
        email: '',
        phone: apotek.phonenumber
      }
    }
  }

  async sendEmail(destination: string, status: ApotekDetailStatus) {
    if (!destination) throw new BadRequestException('Destination email is empty')
    const subject = 'Verifikasi Apoteker'
    const html = await this.configAppService.getConfigApp(
      'APOTEK_VERIFY_NOTIFICATION_EMAIL',
      {
        valueJson: {
          [ApotekDetailStatus.APPROVED]: `<h1>Verifikasi Apoteker</h1><p>Apoteker anda telah diverifikasi</p><br><a href="https://apoteker.inatti.id">Klik disini untuk login</a>`,
          [ApotekDetailStatus.REJECTED]: `<h1>Verifikasi Apoteker</h1><p>Maaf apotek anda tidak diterima. Untuk info lebih lanjut hubungi </p><br><a href="https://inatti.id/help">Inatti Help</a>`,
        }
      }
    )
    return await this.emailService.sendinblue({
      to: [
        {
          email: destination,
        }
      ],
      subject,
      htmlContent: html.valueJson[status]
    })
  }

  async sendWhatsapp(destination: string, status: ApotekDetailStatus) {
    if (!destination) throw new BadRequestException('Destination phonenumber is empty')

    const message = await this.configAppService.getConfigApp(
      'APOTEK_VERIFY_NOTIFICATION_WHATSAPP',
      {
        valueJson: {
          [ApotekDetailStatus.APPROVED]: `NOTIFIKASI INATTI. Apoteker anda telah diverifikasi, silahkan login ke https://apoteker.inatti.id`,
          [ApotekDetailStatus.REJECTED]: `NOTIFIKASI INATTI. Mohon maaf, apotek anda tidak diterima. Hubungi https://inatti.id/help untuk info lebih lanjut. Terima kasih`,
        }
      }
    )
    return await this.whatsappService.serviceNusagateway({
      message: message.valueJson[status],
      phone: destination
    })
  }
}