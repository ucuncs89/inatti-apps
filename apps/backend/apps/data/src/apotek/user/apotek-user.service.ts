import { UserTokenData } from '@app/authentication/types';
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { ApotekRoleUser } from '@app/database/entity/apotek/apotek-role-user.entity';
import { ApotekRole } from '@app/database/entity/apotek/apotek-role.entity';
import { ApotekerDetail } from '@app/database/entity/apotek/apoteker-detail.entity';
import { UserService } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserManageApotekDTO } from '../type/user-dto';

@Injectable()
export class ApotekUserService {
  constructor(
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
    @InjectRepository(ApotekRole)
    private ApotekRoleRepository: Repository<ApotekRole>,
    @InjectRepository(ApotekerDetail)
    private ApotekerDetailRepository: Repository<ApotekerDetail>,
    private userService: UserService
  ) { }

  async getRoles() {
    return await this.ApotekRoleRepository.find();
  }

  async getRole(roleId: number) {
    return await this.ApotekRoleRepository.findOne(roleId)
  }

  /**
   * get user apotek by id apotek
   */
  async getUsers(apotekId: string) {
    return await this.getUserApotek(apotekId)
  }

  /**
   * Get user apotek by id role user (NOT ID USER)
   * 
   */
  async getUser(apotekId: string, id: number) {
    const res = await this.getUserApotek(apotekId, id)
    if (Array.isArray(res)) return res[0]
    return res
  }

  /**
   * Get list user role after login
   */
  async getUserRole(userId: string | User, apotekId?: string) {
    const where: any = {
      userId: userId
    }
    if (apotekId) {
      where.apotekDetail = apotekId
      const res = await this.ApotekRoleUserRepository.findOne({
        where,
        withDeleted: true,
        relations: [
          'userId'
        ]
      })
      res.userId?.hideCredential()
      return res;
    }
    return this.ApotekRoleUserRepository.find({
      where,
      withDeleted: true
    })
  }

  private async getUserApotek(apotekId: string, id?: number) {
    const where: any = {
      apotekDetail: {
        id: apotekId
      },
    }
    if (id) {
      where.id = id
    }
    const users = await this.ApotekRoleUserRepository.find({
      where,
      relations: [
        'userId',
        'roleId'
      ],
      order: {
        createdAt: 'DESC'
      },
      withDeleted: true
    })
    users.forEach((userRole) => {
      userRole.userId.hideCredential()
    })
    return users
  }

  userDtoTomodel(user: UserManageApotekDTO) {
    const { roleId, ...dataUser } = user
    const newUser = new User()
    Object.assign(newUser, dataUser)
    return newUser
  }

  async addUser(apotekId: string, user: UserManageApotekDTO) {
    const newUser = this.userDtoTomodel(user)
    const userData = await this.userService.findOrCreate(newUser)

    const apotekRoleUser = new ApotekRoleUser()
    apotekRoleUser.roleId = await this.getRole(user.roleId)
    apotekRoleUser.userId = userData
    const apotek = new ApotekDetail()
    apotek.id = apotekId;
    apotekRoleUser.apotekDetail = apotek

    const result = await this.ApotekRoleUserRepository.save(apotekRoleUser)

    if (apotekRoleUser.roleId.isCreateApoteker) {
      await this.addApotekerAccount(userData.id, apotek.id)
    }

    return result
  }

  async addApotekerAccount(userId: string, apotekId: string) {
    const apoteker = await this.ApotekerDetailRepository.findOne({
      where: {
        user: {
          id: userId
        },
        apotek: {
          id: apotekId
        }
      }
    })
    if (apoteker) {
      return;
    }
    const newApoteker = new ApotekerDetail()
    newApoteker.user = new User(userId)
    newApoteker.apotek = new ApotekDetail(apotekId)

    return await this.ApotekerDetailRepository.save(newApoteker)
  }

  async updateUser(apotekId: string, id: number, user: UserManageApotekDTO) {
    const apotekRoleUser = await this.ApotekRoleUserRepository.findOne({
      where: {
        id: id,
        apotekDetail: apotekId
      },
      relations: [
        'userId',
        'apotekDetail',
        'roleId'
      ],
      withDeleted: true
    })
    if (!apotekRoleUser) throw Error("User not found")

    const newUser = this.userDtoTomodel(user)
    await this.userService.update(apotekRoleUser.userId.id, newUser)

    if (user.roleId) {
      apotekRoleUser.roleId = new ApotekRole(user.roleId)
    }

    return await this.ApotekRoleUserRepository.save(apotekRoleUser)
  }

  async deleteUser(apotekId: string, id: number) {
    const apotekRoleUser = await this.ApotekRoleUserRepository.findOne({
      where: {
        id,
        apotekDetail: apotekId
      },
      withDeleted: true,
    })

    if (!apotekRoleUser) throw new Error("Data not found")
    return await this.ApotekRoleUserRepository.delete(apotekRoleUser.id)
  }

  async getUserRoleLoggedin(user: UserTokenData) {
    if (!user.payload.roleId) throw new BadRequestException("Role id not found, please relogin")
    let role = await this.ApotekRoleUserRepository.findOne({
      where: {
        id: user.payload.roleId,
      },
      relations: [
        'apotekDetail',
        'roleId'
      ],
      withDeleted: true
    })
    return role;
  }
}

