import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppsAllowAccessApotek } from '../const';
import { ApotekUserService } from './apotek-user.service';

@ApiTags('Data Apotek API')
@RequireAuthApp(...AppsAllowAccessApotek)
@Controller('apotek-user')
export class ApotekUserRoleController {
  constructor(
    private userManagementService: ApotekUserService
  ) { }

  @Get('roles')
  async getRoles() {
    return await this.userManagementService.getRoles()
  }
}