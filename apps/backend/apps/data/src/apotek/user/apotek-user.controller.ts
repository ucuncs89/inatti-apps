import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppsAllowAccessApotek } from '../const';
import { UserManageApotekDTO } from '../type/user-dto';
import { ApotekUserService } from './apotek-user.service';

/**
 * Apotek user management for apotek (base)
 */
@ApiTags('Data Apotek API')
@RequireAuthApp(...AppsAllowAccessApotek)
@Controller('apotek/:idapotek/user')
export class ApotekUserController {
  constructor(
    private userManagementService: ApotekUserService
  ) { }

  @Get()
  async getUsers(
    @Param('idapotek')
    apotekId: string,
  ) {
    try {
      return await this.userManagementService.getUsers(apotekId)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get(':id')
  async getUser(
    @Param('idapotek')
    apotekId: string,
    @Param('id')
    id: number
  ) {
    try {
      return await this.userManagementService.getUser(apotekId, id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post()
  async addUser(
    @Param('idapotek')
    apotekId: string,
    @Body()
    body: UserManageApotekDTO
  ) {
    console.log('add user apotek')
    console.log(body)
    try {
      return await this.userManagementService.addUser(apotekId, body)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Patch(':id')
  async updateUser(
    @Param('idapotek')
    apotekId: string,
    @Param('id')
    id: number,
    @Body()
    body: UserManageApotekDTO
  ) {
    try {
      return await this.userManagementService.updateUser(apotekId, id, body)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Delete(':id')
  async deleteUser(
    @Param('idapotek')
    apotekId: string,
    @Param('id')
    id: number
  ) {
    try {
      return await this.userManagementService.deleteUser(apotekId, id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

}