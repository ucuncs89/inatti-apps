import { AppsEnum } from "@app/database/entity/apps/apps.type";

export const AppsAllowAccessApotek = [AppsEnum.Apotek, AppsEnum.ApotekGroup, AppsEnum.Admin, AppsEnum.ApotekHeadOffice]