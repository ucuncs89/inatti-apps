import { OtpService } from '@app/account/otp/otp.service';
import { SendToOtp } from '@app/account/otp/type';
import { AuthenticationService } from '@app/authentication';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SignupDTO } from './type';

@Injectable()
export class UserAuthService {
  constructor(
    @InjectRepository(User)
    private UserRepository: Repository<User>,
    private authService: AuthenticationService,
    private otpService: OtpService
  ){}

  async signupUser(params: SignupDTO){

    let isUsernameExist = await this.UserRepository.findOne({username: params.phonenumber});
    if(isUsernameExist) {
      throw 'Nomor hp sudah terdaftar';
    }

    let newUser = new User();
    newUser.fullname = params.fullname;
    newUser.password = params.password;
    newUser.username = params.phonenumber;
    newUser.email = params.email;
    newUser.phoneNumber = params.phonenumber;
    newUser.isVerified = false
    let user = await this.UserRepository.save(newUser);
    
    const otp = this.otpService.createOtp();
    await this.otpService.saveOtp(otp, user)

    if(params.sendOtpTo === 'whatsapp'){
      await this.otpService.sendWhatsapp(params.phonenumber, otp);
    } else if(params.sendOtpTo === 'email'){
      let sendEmail = await this.otpService.sendEmail({
        email: params.email,
        name: params.fullname
      }, otp);
      console.log(sendEmail);
      
    }

    return this.authService.login(user, AppsEnum.User)
  }

  async validateOtp(otp: string, username: string){
    await this.otpService.validateOtp(otp, username);

    let user = await this.UserRepository.findOne({username: username});
    user.isVerified = true;
    await this.UserRepository.save(user);
    return this.authService.login(user, AppsEnum.User)
  }

  async resendOtp(username: string, sendTo: SendToOtp){
    let user = await this.UserRepository.findOne({username: username});
    let otp = this.otpService.createOtp();
    
    await this.otpService.saveOtp(otp, user);
    
    if(sendTo === 'whatsapp'){
      return await this.otpService.sendWhatsapp(user.phoneNumber, otp);
    } else if(sendTo === 'email'){
      if(!user.email) throw "Email not registered to this account";
      await this.otpService.sendEmail({
        email: user.email,
        name: user.fullname
      }, otp);
    }

  }
}
