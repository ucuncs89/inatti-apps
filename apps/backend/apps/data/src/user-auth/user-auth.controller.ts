import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ResendOtpDTO, SignupDTO, ValidateOtpDTO } from './type';
import { UserAuthService } from './user-auth.service';

@ApiTags('User API')
@Controller('user-auth')
export class UserAuthController {
  constructor(private readonly userAuthService: UserAuthService) {}

  @Post('signup')
  async signup(
    @Body()
    body: SignupDTO
  ){
    try {
      return await this.userAuthService.signupUser(body);
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }

  @Post('validate/otp')
  async validateOtp(@Body() body: ValidateOtpDTO){
    try {
      return await this.userAuthService.validateOtp(body.otp, body.username);
    } catch (error) {
      console.log(error);
      
      throw new BadRequestException(error.message);
    }
  }

  @Post('resend/otp')
  async resendOtp(@Body() body: ResendOtpDTO){
    try {
      return await this.userAuthService.resendOtp(body.username, body.sendTo);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
