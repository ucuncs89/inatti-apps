import { Module } from '@nestjs/common';
import { UserAuthService } from './user-auth.service';
import { UserAuthController } from './user-auth.controller';
import { DatabaseModule } from '@app/database';
import { AuthenticationModule } from '@app/authentication';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { EmailServiceModule } from '@app/email-service';
import { OtpModule } from '@app/account/otp/otp.module';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { ConfigAppModule } from '@app/config-app';
import { UserAuthV2Controller } from './v2/user-auth-v2.controller';
import { UserAuthV2Service } from './v2/user-auth-v2.service';
import { ForgetPasswordV2Service } from './v2/forget-password-v2.service';
import { VerifyOtpV2Service } from './v2/verify-otp-v2.service';
import { SignupApotekController } from './v2/signup-apotek/signup-apotek.controller';
import { SignupApotekService } from './v2/signup-apotek/signup-apotek.service';

@Module({
  imports: [
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.User }),
    EmailServiceModule,
    WhatsappServiceModule,
    OtpModule,
    ConfigAppModule,
  ],
  controllers: [
    UserAuthController,
    UserAuthV2Controller,
    SignupApotekController
  ],
  providers: [
    UserAuthService,
    UserAuthV2Service,
    ForgetPasswordV2Service,
    VerifyOtpV2Service,
    SignupApotekService
  ]
})
export class UserAuthModule { }
