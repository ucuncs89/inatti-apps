import { User } from "@app/database/entity/user/user.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { OtpService } from "@app/account/otp/otp.service";
import { UserTokenData } from "@app/authentication/types";

@Injectable()
export class VerifyOtpV2Service {
  constructor(
    private otpSerivce: OtpService,
  ) { }

  async generateAndSendOtp(user: User) {
    const otp = this.otpSerivce.createOtp();
    await this.otpSerivce.saveOtp(otp, user);

    // for now, only send by whatsapp
    await this.otpSerivce.sendWhatsapp(user.phoneNumber, otp);
    return { message: 'OTP sent' }
  }

  async validateOtpOrThrow(otp: string, user: UserTokenData) {
    const isValild = await this.otpSerivce.validateOtp(otp, user.username)
    if (!isValild) throw new BadRequestException('Invalid OTP');
    return true
  }
}