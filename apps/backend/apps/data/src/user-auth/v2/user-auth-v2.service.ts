import { AuthenticationService } from "@app/authentication";
import { UserService } from "@app/database/entity/user/user.entities.services";
import { User } from "@app/database/entity/user/user.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { UserAuthV2SignupDTO } from "./type";
import { VerifyOtpV2Service } from "./verify-otp-v2.service";
import { JWTSecretOtpAuth, UserTokenData } from "@app/authentication/types";
import { pick } from "lodash";

@Injectable()
export class UserAuthV2Service {
  constructor(
    private userService: UserService,
    private otpService: VerifyOtpV2Service,
    private authService: AuthenticationService,
  ) { }

  async signup(payload: UserAuthV2SignupDTO) {
    let user = new User();
    Object.assign(user, payload);
    user.isVerified = false;
    user = await this.userService.create(user)
    await this.otpService.generateAndSendOtp(user)
    return user;
  }

  async signupWithToken(payload: UserAuthV2SignupDTO, additionalPayload?: Record<string, any>) {
    const user = await this.signup(payload);
    return await this.loginAsOtpAuth(user, {
      apps: 'user',
      action: 'register',
      ...additionalPayload
    })
  }

  async verifyUser(user: UserTokenData) {
    const existingUser = await this.userService.findOne({
      id: user.id
    })
    existingUser.isVerified = true
    return await this.userService.update(user.id, existingUser)
  }

  async verifyUsername(username: string) {
    // will throw error
    this.userService.validateUsername(username)
    const user = await this.userService.findOne({
      username: username
    })
    if (user) {
      throw new BadRequestException('Username already exists')
    }
    return {
      messag: "Username is available"
    }
  }

  async signupUpdateAccount() { }

  /**
   * This function will return token with jwt secret different from the default one
   * The secret key for jwt is saved on .env 'JWT_SECRET_OTP_AUTH'
   * 
   * @param user 
   * @param payload 
   * @returns token
   */
  loginAsOtpAuth(user: User, payload: any) {
    return this.authService.loginV2(user, payload, {
      secret: JWTSecretOtpAuth,
      expiresIn: '1h'
    })
  }

  async getInfoUserCencorred(user: UserTokenData) {
    const userDetail = await this.userService.findOne({
      id: user.id
    })
    const res = pick<User>(userDetail, ['id', 'username', 'email', 'phoneNumber'])

    res.email = res.email?.replace(/(?<=.).(?=.*@)/g, '*')
    res.phoneNumber = res.phoneNumber?.replace(/(?<=.{4}).(?=.{3})/g, '*')
    return res;
  }
}