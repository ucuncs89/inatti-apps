import { UserTokenData } from "@app/authentication/types";
import { UserService } from "@app/database/entity/user/user.entities.services";
import { BadRequestException, Injectable } from "@nestjs/common";
import { VerifyOtpV2Service } from './verify-otp-v2.service';

@Injectable()
export class ForgetPasswordV2Service {
  constructor(
    private userService: UserService,
    private verifyOtpV2: VerifyOtpV2Service
  ) { }

  async sendOtp(username: string) {
    const user = await this.userService.findOne({
      username: username
    })
    if (!user) throw new BadRequestException('Username not found')
    if (!user.phoneNumber) throw new BadRequestException('Username found but have no phonenumber')
    const res = await this.verifyOtpV2.generateAndSendOtp(user)
    if (!res) throw new BadRequestException('Failed to send otp')
    return {
      user,
      message: 'OTP Sent'
    }
  }

  async changePassword(user: UserTokenData, password: string) {
    const existingUser = await this.userService.findOne({
      id: user.id
    })
    existingUser.password = password
    return await this.userService.update(user.id, existingUser)
  }
}