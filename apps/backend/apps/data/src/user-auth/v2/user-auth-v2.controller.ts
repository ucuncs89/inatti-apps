import { AuthenticationService } from "@app/authentication";
import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { OtpAuthGuard } from "@app/authentication/authorization/otp-auth/guard.otp-auth";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { ExampleResponseOk } from "@app/helper/utils/api-response";
import { BadRequestException, Body, Controller, Get, Patch, Post, Query, UnauthorizedException, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from "@nestjs/swagger";
import { ForgetPasswordV2Service } from "./forget-password-v2.service";
import { UserAuthV2ForgetPasswordDTO, UserAuthV2SignupDTO, UserAuthV2ValidateOtpDTO } from "./type";
import { UserAuthV2Service } from "./user-auth-v2.service";
import { VerifyOtpV2Service } from "./verify-otp-v2.service";

@ApiTags('User Auth V2')
@Controller('/user-auth/v2')
export class UserAuthV2Controller {
  constructor(
    private userAuthV2Service: UserAuthV2Service,
    private verifyOtpV2service: VerifyOtpV2Service,
    private forgetPasswordV2Service: ForgetPasswordV2Service,
    private authService: AuthenticationService,
  ) { }

  /**
   * New api to signup
   * 
   * @param user 
   * @returns Token for user
   */
  @ExampleResponseOk({
    "access_token": "eyJhbGciOiJ...........9mGKuPdks"
  })
  @ApiQuery({
    name: 'register-app',
    required: false,
    example: Object.keys(AppsEnum).join(", ")
  })
  @Post('signup')
  async signup(
    @Body() user: UserAuthV2SignupDTO,
    @Query('register-app') registerApp: AppsEnum
  ) {
    if (registerApp && !Object.values(AppsEnum).includes(registerApp)) {
      throw new BadRequestException('Invalid register app')
    }
    return this.userAuthV2Service.signupWithToken(user, {
      registerApp,
    })
  }

  @ExampleResponseOk({
    "messag": "Username is available"
  })
  @Get('verify-username')
  async verifyUsername(
    @Query('username') username: string
  ) {
    return await this.userAuthV2Service.verifyUsername(username);
  }


  @ApiOperation({
    description: "validation will return token for user",
  })
  @ExampleResponseOk({
    "access_token": "eyJhbGciOiJ...........9mGKuPdks",
    "message": "You have been verified, please login"
  })
  @OtpAuthGuard()
  @Patch('validate/otp')
  async validateOtp(
    @UserCred() user: UserTokenData,
    @Body() payload: UserAuthV2ValidateOtpDTO
  ) {
    await this.verifyOtpV2service.validateOtpOrThrow(payload.otp, user)

    if (user.payload.action === 'register') {

      /**
       * After all, redirect user to login page
       */
      await this.userAuthV2Service.verifyUser(user)

      const payloadToken: any = {
        action: 'register_otp_verified'
      }

      /**
       * User register from app, return token with app info
       */
      if (user?.payload?.registerApp) {
        payloadToken.registerApp = user.payload.registerApp
      }

      const res = await this.userAuthV2Service.loginAsOtpAuth(user, payloadToken)

      return {
        ...res,
        message: 'You have been verified, please login'
      }

    } else if (user.payload.action === 'request_forget_password') {

      /**
       * redirect user to page update password using token AppsEnum.User
       * this is OK, because we don't need to validate user role
       * but, after user update password, we need to redirect user to login page
       */

      return this.userAuthV2Service.loginAsOtpAuth(user, {
        action: 'forget_password_otp_verified'
      })
    }

    throw new BadRequestException('Token not valid, action not found')
  }

  @ExampleResponseOk({
    "message": "OTP sent"
  })
  @OtpAuthGuard()
  @Get('resend/otp')
  async resendOtp(
    @UserCred() user: UserTokenData,
  ) {
    return await this.verifyOtpV2service.generateAndSendOtp(user)
  }

  @ExampleResponseOk({
    "id": "d5236e90-6536-4eb8-b695-94c5ab539a0d",
    "username": "628*******617",
    "email": "rio*****@gmail.com",
    "phoneNumber": "6282******617"
  })
  @OtpAuthGuard()
  @Get('info-user/otp')
  async infoUserOtp(
    @UserCred() user: UserTokenData,
  ) {
    return await this.userAuthV2Service.getInfoUserCencorred(user)
  }

  @ExampleResponseOk({
    "access_token": "eyJhbGciOiJ...........9mGKuPdks"
  })
  @Get('forget-password/send-otp')
  async forgetPasswordFindUsername(
    @Query('username') username: string
  ) {
    // will throw error
    const res = await this.forgetPasswordV2Service.sendOtp(username)
    /**
     * This could be potential security issue
     * because we don't know if user logged in as forget password or not
     * user can be used this endpoint and use as token to bypass login
     */
    return this.userAuthV2Service.loginAsOtpAuth(res.user, {
      action: 'request_forget_password'
    })
  }

  @ExampleResponseOk({
    "message": "Password has been changed"
  })
  @OtpAuthGuard()
  @Post('forget-password/change-password')
  async forgetPasswordChangePassword(
    @UserCred() user: UserTokenData,
    @Body() body: UserAuthV2ForgetPasswordDTO
  ) {
    // only payload with action 'forget_password_otp_verified' can be used
    if (user.payload.action !== 'forget_password_otp_verified') throw new BadRequestException('Not allowed')

    const isSuccess = await this.forgetPasswordV2Service.changePassword(user, body.password)
    if (!isSuccess) throw new BadRequestException('Failed to change password')
    return {
      message: "Password has been changed"
    }
  }
}