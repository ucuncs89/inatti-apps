import { ApiProperty } from "@nestjs/swagger";

export class SignupApotekSetup {
  @ApiProperty()
  name: string
}