import { AuthenticationService } from "@app/authentication";
import { OtpAuthGuard } from "@app/authentication/authorization/otp-auth/guard.otp-auth";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { ExampleResponseOk } from "@app/helper/utils/api-response";
import { BadRequestException, Body, Controller, Post } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { UserAuthV2Service } from "../user-auth-v2.service";
import { SignupApotekService } from "./signup-apotek.service";
import { SignupApotekSetup } from "./type";

@ApiTags('User Auth V2')
@Controller('/user-auth/v2/signup-apotek')
export class SignupApotekController {
  constructor(
    private userAuthV2Service: UserAuthV2Service,
    private signupApotekService: SignupApotekService,
    private authService: AuthenticationService
  ) { }

  @ExampleResponseOk({
    "access_token": "eyJhbGciOiJ...........9mGKuPdks"
  })
  @OtpAuthGuard()
  @Post('/setup')
  async setup(
    @Body() body: SignupApotekSetup,
    @UserCred() user: UserTokenData
  ) {
    if (
      user.payload.action !== 'register_otp_verified' ||
      user.payload.registerApp !== AppsEnum.Apotek
    ) throw new BadRequestException('Not allowed')

    await this.signupApotekService.setupApotek(user, body)
    return this.authService.loginV2(user, {
      apps: AppsEnum.Apotek
    })
  }
}