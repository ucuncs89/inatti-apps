import { UserTokenData } from "@app/authentication/types";
import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { SignupApotekSetup } from "./type";
import { ApotekDetailStatus } from "@app/database/entity/apotek/type";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { User } from "@app/database/entity/user/user.entity";
import { ApotekRole } from "@app/database/entity/apotek/apotek-role.entity";
import { ApotekGroupDetail } from "@app/database/entity/apotek-group/apotek-group-detail.entity";

@Injectable()
export class SignupApotekService {
  constructor(
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
  ) { }

  async setupApotek(user: UserTokenData, payload: SignupApotekSetup) {
    await this.validateDuplicateAction(user)
    const apotek = await this.createApotek(payload.name)
    await this.assingUserToApotek(user.id, apotek.id)
    return apotek
  }

  async validateDuplicateAction(user: UserTokenData) {
    const roleExist = await this.ApotekRoleUserRepository.find({
      where: {
        userId: {
          id: user.id
        }
      }
    })
    if (roleExist.length > 0) throw new BadRequestException('User already have role')
  }

  async createApotek(name: string) {
    const apotek = new ApotekDetail()
    apotek.name = name
    apotek.validateStatus = ApotekDetailStatus.WAITING
    apotek.apotekGroup = this.getDefaultApotekGroup()
    return await this.ApotekDetailRepository.save(apotek)
  }

  async assingUserToApotek(userId: string, apotekId: string) {
    const ROLE_ADMIN = 1

    const roleUser = new ApotekRoleUser()
    roleUser.userId = new User(userId)
    roleUser.apotekDetail = new ApotekDetail(apotekId)
    roleUser.roleId = new ApotekRole(ROLE_ADMIN)
    return await this.ApotekRoleUserRepository.save(roleUser)
  }

  getDefaultApotekGroup() {
    // This id from 'libs/backend-seeding/seed/staging/state-staging.json' 'Create apotek Group inatti'
    const id = '2e8ba305-b613-438c-9d7d-c80a6c9296b7'
    return new ApotekGroupDetail(id)
  }
}