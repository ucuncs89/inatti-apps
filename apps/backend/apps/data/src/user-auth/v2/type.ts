import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { ApiProperty } from "@nestjs/swagger";

export class UserAuthV2SignupDTO {
  @ApiProperty()
  fullname: string;

  @ApiProperty()
  username: string

  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}

export class UserAuthV2ValidateOtpDTO {
  @ApiProperty()
  otp: string;
}

export class UserAuthV2ForgetPasswordDTO {
  @ApiProperty()
  password: string
}