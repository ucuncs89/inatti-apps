import { SendToOtp } from "@app/account/otp/type";
import { ApiProperty } from "@nestjs/swagger";

export class SignupDTO{
  @ApiProperty()
  fullname: string;

  @ApiProperty()
  phonenumber: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;

  @ApiProperty({ example: "'email' or 'whatsapp'"})
  sendOtpTo: SendToOtp
}

export class ValidateOtpDTO {
  @ApiProperty()
  otp: string

  @ApiProperty()
  username: string
}

export class ResendOtpDTO {
  @ApiProperty()
  username: string
  @ApiProperty({example: "'email' or 'whatsapp'"})
  sendTo: SendToOtp
}