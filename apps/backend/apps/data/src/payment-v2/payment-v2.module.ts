import { Module } from "@nestjs/common";
import { PaymentXenditService } from "./xendit/payment-xendit.service";
import { RefIntegrateController } from "./ref-integrate/ref-integrate.controller";
import { XenditCallbackController } from "./xendit/xendit-callback.controller";
import { RefIntegrateService } from "./ref-integrate/ref-integrate.service";
import { ConfigAppModule } from "@app/config-app";

/**
 * @since integration using xendit
 */
@Module({
  imports: [
    ConfigAppModule,
  ],
  controllers: [
    XenditCallbackController,
    RefIntegrateController
  ],
  providers: [
    PaymentXenditService,
    RefIntegrateService,
  ],
  exports: [
    PaymentXenditService,
    RefIntegrateService
  ]
})
export class PaymentV2Module { }