export type XenditAccountType = "OWNED" | "MANAGED"

export type XenditAccountStatus = "INVITED" | "REGISTERED" | "AWAITING_DOCS" | "LIVE"

export interface XenditAccount {
  id: string,
  created: string,
  updated: string,
  type: XenditAccountType,
  email: string,
  public_profile?: {
    business_name: string
  },
  status: XenditAccountStatus
}

export const XenditProvider = 'xendit'

export interface XenditCallbackBody {
  id: string;
  external_id: string;
  user_id: string;
  is_high: boolean;
  payment_method: string;
  status: string;
  merchant_name: string;
  amount: number;
  paid_amount: number;
  bank_code: string;
  paid_at: Date;
  payer_email: string;
  description: string;
  adjusted_received_amount: number;
  fees_paid_amount: number;
  updated: Date;
  created: Date;
  currency: string;
  payment_channel: string;
  payment_destination: string;
}

export interface XenditSetCallbackUrl {
  status: string;
  user_id: string;
  url: string;
  environment: string;
  callback_token: string;
}