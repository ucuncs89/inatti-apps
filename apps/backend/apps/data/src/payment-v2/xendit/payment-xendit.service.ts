import { XenditSubject } from "@app/database/entity/payment-gateway/xendit-subject.entity";
import { ForbiddenException, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { XenditAccount, XenditSetCallbackUrl } from "./xendit.type";
import Xendit = require("xendit-node")
import { InvoiceResult } from "../ref-integrate/ref-integrate.type";
import { StatusOrderEnum } from "@inatti/shared/dtos";
import { XenditTransaction } from "@app/database/entity/payment-gateway/xendit-transaction.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { ConfigAppService, CONFIGAPP_NAME } from "@app/config-app";

export const XenditPaymentStatus: Record<string, StatusOrderEnum> = {
  PENDING: StatusOrderEnum.new,
  PAID: StatusOrderEnum.paid,
  EXPIRED: StatusOrderEnum.close,
}

@Injectable()
export class PaymentXenditService {
  constructor(
    @InjectRepository(XenditSubject)
    private XenditSubjectRepository: Repository<XenditSubject>,
    @InjectRepository(XenditTransaction)
    private XenditTransactionRepository: Repository<XenditTransaction>,
    private configAppService: ConfigAppService
  ) { }

  log = new Logger('PaymentXenditService')

  xendit = new Xendit({
    secretKey: process.env.XENDIT_KEY,
  })

  platform = new this.xendit.Platform({})
  invoice = new this.xendit.Invoice({})

  async accountPartnerCreate(payload: { email: string }): Promise<XenditAccount> {
    const type = 'MANAGED'
    const res = await this.platform.createV2Account({
      email: payload.email,
      type,
    })
    return res
  }

  async createCallbackInvoice(account: XenditAccount) {
    const Type = 'invoice'
    const urlCallback = await this.configAppService.getConfigOrInit(CONFIGAPP_NAME.XENDIT_CALLBACK_INVOICE)
    const res: XenditSetCallbackUrl = await this.platform.setCallbackURL({
      type: Type,
      url: urlCallback,
      forUserID: account.id
    })

    return await this.saveTokenCallback(res)
  }

  async saveXenditAccount(account: XenditAccount) {
    const xenditSubject = new XenditSubject()
    xenditSubject.email = account.email
    xenditSubject.subjectUserId = account.id
    xenditSubject.status = account.status
    xenditSubject.type = account.type
    xenditSubject.createdAt = account.created
    xenditSubject.updatedAt = account.updated
    xenditSubject.updateSyncAt = new Date()

    return await this.XenditSubjectRepository.save(xenditSubject)
  }

  async saveTokenCallback(data: XenditSetCallbackUrl) {
    return await this.XenditSubjectRepository.update({ subjectUserId: data.user_id }, {
      callbackUrl: data.url,
      callbackToken: data.callback_token
    })
  }

  accountPartnerGet(id: string): Promise<XenditAccount> {
    return this.platform.getAccountByID({ id }) as any
  }

  /**
   * Fetch user to xendit and update to database
   */
  async syncAccountPartner(id: string) {
    const account = await this.accountPartnerGet(id)
    const resUpdate = await this.saveXenditAccount(account)
    await this.createCallbackInvoice(account)
    return resUpdate
  }

  async invoiceCreate(payload: {
    trxId: string,
    amount: number,
  }, userXenditId: string): Promise<InvoiceResult> {
    const requestApi = {
      externalID: payload.trxId,
      amount: payload.amount,
      successRedirectURL: this.getUrlTrx(payload.trxId, 'success'),
      failureRedirectURL: this.getUrlTrx(payload.trxId, 'failed'),
      forUserID: userXenditId
    }
    const res = await this.invoice.createInvoice(requestApi)

    const resData: InvoiceResult = {
      id: res.id,
      trxId: payload.trxId,
      amount: payload.amount,
      status: XenditPaymentStatus[res.status],
      urlRedirect: res.invoice_url,
      responseApi: res,
      expiredAt: res.expiry_date,
      requestApi,
    }

    await this.saveToTransaction(resData)

    return resData;
  }

  private async saveToTransaction(invoice: InvoiceResult) {
    const trx = new XenditTransaction()
    trx.id = invoice.id
    trx.orderTrx = new SwabOrderTrx(invoice.trxId)
    trx.expiredAt = new Date(invoice.expiredAt)
    trx.requestApi = invoice.requestApi
    trx.responseApi = invoice.responseApi
    trx.status = invoice.status
    trx.urlRedirect = invoice.urlRedirect

    return this.XenditTransactionRepository.save(trx)
  }

  private getUrlTrx(trxId: string, status: 'success' | 'failed') {
    const url = new URL(process.env.URL_FRONTEND_USER)
    url.pathname = `testcovid/order/detail/${trxId}`
    url.search = `status=${status}`
    return url.toString()
  }

  async validateToken(token: string) {
    const account = await this.XenditSubjectRepository.findOne({
      where: {
        callbackToken: token
      }
    })
    if (!account) {
      throw new ForbiddenException()
    }
    return true;
  }
}