import { Body, Controller, ForbiddenException, Logger, Post, Req } from "@nestjs/common";
import { Request } from "express";
import { RefIntegrateService } from "../ref-integrate/ref-integrate.service";
import { PaymentXenditService } from "./payment-xendit.service";
import { XenditCallbackBody, XenditProvider } from "./xendit.type";

@Controller('payment-v2/xendit/callback')
export class XenditCallbackController {
  constructor(
    private refIntegrateService: RefIntegrateService,
    private xenditService: PaymentXenditService
  ) { }

  log = new Logger('XenditCallbackController')

  @Post('invoice')
  async invoiceCallback(
    @Body() body: XenditCallbackBody,
    @Req() req: Request
  ) {
    const token = req?.headers['x-callback-token'] as string
    this.log.verbose(`Request callback invoice from token ${token}`)
    await this.xenditService.validateToken(token)

    const idTrx = body.external_id
    const status = body.status
    // for testing perpuse
    if (idTrx === 'invoice_123124123') return true;

    this.log.verbose(`Callback ${idTrx} with status ${status}`)
    await this.refIntegrateService.updateInvoice(idTrx, status, XenditProvider)

    return true;
  }
}