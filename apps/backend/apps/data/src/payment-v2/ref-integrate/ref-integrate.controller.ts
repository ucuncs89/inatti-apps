import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { RefRoleUser } from "@app/database/entity/ref/ref-role-user.entity";
import { User } from "@app/database/entity/user/user.entity";
import { BadRequestException, Body, Controller, Get, Post, Query } from "@nestjs/common";
import { RefIntegrateService } from "./ref-integrate.service";
import { CreateAccountPartnerDTO } from "./ref-integrate.type";

@Controller('payment-v2/ref-integrate')
export class RefIntegrateController {
  constructor(
    private refIntegrateService: RefIntegrateService
  ) { }

  @RequireAuthApp(AppsEnum.Ref)
  @Post('create-account')
  createAccount(
    @UserCred('ref') user: UserTokenData<RefRoleUser>,
    @Body() body: CreateAccountPartnerDTO
  ) {
    const refId = user.payload?.detailRole?.refId?.id
    if (!refId) throw new BadRequestException('ref id not found')
    return this.refIntegrateService.registerPaymentGateway(body.email, refId)
  }

  @RequireAuthApp()
  @Get('transfer-detail')
  getDetail(
    @Query('refId') refId: string
  ) {
    return this.refIntegrateService.get(refId)
  }

  /**
   * this api will get status and update it as long as not live
   * 
   * @param refId 
   * @returns 
   */
  @RequireAuthApp()
  @Get('transfer-detail/sync-provider')
  getDetailStatus(
    @Query('refId') refId: string
  ) {
    return this.refIntegrateService.getStatus(refId)
  }
}