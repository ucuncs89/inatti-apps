import { RefPaymentGateway } from "@app/database/entity/payment-gateway/ref-payment-gateway.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { LogTask } from "@app/helper/logger/logger-helper";
import { PromiseTimeout } from "@app/helper/promise/promise-helper";
import { StatusOrderEnum } from "@inatti/shared/dtos";
import { BadRequestException, Injectable, InternalServerErrorException, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import dayjs = require("dayjs");
import { Repository } from "typeorm";
import { PaymentXenditService, XenditPaymentStatus } from "../xendit/payment-xendit.service";
import { XenditProvider } from "../xendit/xendit.type";
import { InvoiceResult } from "./ref-integrate.type";

@Injectable()
export class RefIntegrateService {
  constructor(
    @InjectRepository(RefPaymentGateway)
    private RefPaymentGatewayRepository: Repository<RefPaymentGateway>,
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    private paymentXenditService: PaymentXenditService
  ) { }

  log = new Logger('RefIntegrateService')
  logTask = new LogTask(this.log)

  async get(refId: string) {
    return await this.RefPaymentGatewayRepository.findOne({
      where: {
        ref: refId
      }
    })
  }

  async registerToProvider(provider: string, payload: any): Promise<{
    subjectUserId: string,
    isLive: boolean
  }> {
    if (provider === XenditProvider) {
      const res = await this.logTask.runTask(
        PromiseTimeout(
          this.paymentXenditService.accountPartnerCreate(payload),
        ),
        'Create account xendit'
      )
      if (!res) throw new InternalServerErrorException("Failed to fetch payment xendit")
      return {
        subjectUserId: res.id,
        isLive: SubjectIsLive(res.status),
      }
    }
    throw new ProviderNotSupportedExeption(provider)
  }

  async registerPaymentGateway(email: string, refId: string, provider = 'xendit') {
    let isExist = await this.get(refId)
    if (isExist) throw new BadRequestException(`Reference ${refId} have registered`)

    const providerRegistered = await this.logTask.runTask(
      this.registerToProvider(provider, { email }),
      `register email ${email} with ref ${refId}`
    )

    const res = await this.logTask.runTask(
      this.RefPaymentGatewayRepository.save({
        ref: { id: refId },
        subjectUserId: providerRegistered.subjectUserId,
        isLive: providerRegistered.isLive,
        provider: provider,
      }),
      'save to database ref payment'
    )

    return res;
  }

  isExpired(updatedAt: Date): boolean {
    if (!updatedAt) return true;
    let expired = dayjs(updatedAt).add(10, 'minutes')
    return dayjs().isAfter(expired)
  }

  async getStatus(refId: string) {
    const detail = await this.get(refId)
    if (!detail) {
      return {}
    }
    const isExpired = this.isExpired(detail.updateAt)
    this.log.verbose(`${refId} integration status : ${isExpired ? 'expired, need to refetch' : 'not expired, no need to refetch'}`)

    if (!detail.isLive && isExpired) {
      // if detail udpate is more than 10 minutes, update id
      const res = await this.logTask.runTask(
        this.syncStatusByProvider(detail.provider, detail.subjectUserId),
        'Sync payment gateway status to provider'
      )
      detail.isLive = SubjectIsLive(res.status)
      detail.updateAt = new Date()
      await this.logTask.runTask(
        this.RefPaymentGatewayRepository.save(detail),
        'Save status to database'
      )
    }
    //@ts-ignore
    detail.providerDetail = await this.getStatusByProvider(detail.provider, detail.subjectUserId)
    return detail
  }

  async getStatusByProvider(provider: string, subjectId: string) {
    if (provider === XenditProvider) {
      return this.paymentXenditService.accountPartnerGet(subjectId)
    }
    throw new ProviderNotSupportedExeption(provider)
  }

  async syncStatusByProvider(provider: string, subjectId: string) {
    if (provider === XenditProvider) {
      /**
       * It will refresh account and set callback
       */
      return this.paymentXenditService.syncAccountPartner(subjectId)
    }
    throw new ProviderNotSupportedExeption(provider)
  }

  async createInvoice(refId: string, payload: { trxId: string, amount: number }): Promise<InvoiceResult> {
    const detail = await this.get(refId)
    if (detail.provider === XenditProvider) {
      return this.paymentXenditService.invoiceCreate(payload, detail.subjectUserId)
    }
  }

  async updateInvoice(trxId: string, status: string, provider: string) {
    let newStatus: StatusOrderEnum
    if (provider === XenditProvider) {
      newStatus = XenditPaymentStatus[status]
    }

    return await Promise.all([
      this.SwabOrderTrxRepository.update({
        id: trxId
      }, { status: newStatus }),
      this.SwabOrderRepository.update({
        orderTrx: { id: trxId },
      }, { status: newStatus })
    ])
  }

}

export class ProviderNotSupportedExeption extends BadRequestException {
  constructor(provider: string) {
    super()
    this.message = `Provider ${provider} not registered`
  }
}

export interface RefSubject {
  id?: number
  refId: string,
  subjectUserId: string,
  isLive: boolean,
  provider: string
}

export const SubjectIsLive = (status: string) => status === 'LIVE'