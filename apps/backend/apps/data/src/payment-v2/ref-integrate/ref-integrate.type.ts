import { StatusOrderEnum } from "@inatti/shared/dtos";
import { ApiProperty } from "@nestjs/swagger";

export class CreateAccountPartnerDTO {
  @ApiProperty()
  email: string
}

export interface InvoiceResult {
  id: string
  trxId: string
  status: StatusOrderEnum
  urlRedirect: string
  amount: number
  expiredAt?: string
  responseApi?: any
  requestApi?: any
}