export async function QueryInterval(cb: (interval: string, limitQuery?: (...parms: any[]) => string) => {}) {
  let result = {}
  const intervals = {
    day: {
      limitQuery: (col: string) => `${col} > now() - interval '7 days'`,
    },
    month: {
      limitQuery: (col: string) => `${col} > now() - interval '12 months'`,
    },
    year: {
      limitQuery: (col: string) => `${col} > now() - interval '3 years'`,
    },
  }
  for (const interval of Object.keys(intervals)) {
    const { limitQuery } = intervals[interval]
    result[interval] = await cb(interval, limitQuery);
  }
  return result;
}