import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { DashboardReportService } from './dashboard-report.service';
import { PmrService } from './pmr/pmr.service';

@Module({
  imports: [
    DatabaseModule,
  ],
  providers: [
    DashboardReportService,
    PmrService,
  ],
  controllers: [],
  exports: [
    DashboardReportService,
    PmrService
  ]
})
export class DashboardReportModule {}
