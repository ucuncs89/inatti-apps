import { DashboardReportCacheApp } from '@app/database/entity/report/dashboard-report-cache-app.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as dayjs from "dayjs";

@Injectable()
export class DashboardReportService {
  constructor(
    @InjectRepository(DashboardReportCacheApp)
    private dashboardReport: Repository<DashboardReportCacheApp>,
  ){}

  private getId(app: string, appId: string){
    return `${app}:${appId}`
  }

  // https://day.js.org/docs/en/manipulate/add
  private MAX_AGE_REPORT = [1, 'day']
  private isOld(date:Date){
    let updateDate = dayjs(date)
    //@ts-ignore
    let expired = updateDate.add(+this.MAX_AGE_REPORT[0], this.MAX_AGE_REPORT[1] + "")
    return dayjs().isAfter(expired)
  }

  /**
   * Get data dashboard, return null if expired
   * @param app 
   * @param appId 
   * @returns 
   */
  async get(app: string, appId: string){
    let data = await this.dashboardReport.findOne({
      where: {
        idApp: this.getId(app, appId)
      }
    })
    if(data && this.isOld(data.updatedAt)){
      data = null
    }
    return data
  }

  async save(app: string, appId: string, data: any){
    let existing = await this.dashboardReport.findOne({
      where: {
        idApp: this.getId(app, appId)
      }
    })
    if(!existing){
      existing = new DashboardReportCacheApp()
      existing.idApp = this.getId(app, appId)
    }
    existing.updatedAt = new Date()
    existing.data = data
    return this.dashboardReport.save(existing)
  }
}
