import { UserTokenData } from "@app/authentication/types";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { DashboardReportService } from "../../dashboard-report.service";
import { PmrService } from "../../pmr/pmr.service";
import { QueryBuilderApotek } from "../query-builder/query-builder-apotek.helper";

@Injectable()
export class ApotekDashboardService {
  constructor(
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
    private dashboardReport: DashboardReportService,
    private queryBuilder: QueryBuilderApotek,
    private pmrService: PmrService
  ) { }

  app = 'apotek'

  async getDashboard(user: UserTokenData, isForce?: boolean) {
    const hashId = this.getHashDashboard(user)
    const dashboard = await this.dashboardReport.get(this.app, hashId)
    // if available
    if (dashboard && !isForce) return dashboard

    const role = await this.getRoleUser(user)

    // generate here
    const dataNewDashboard = await this.generateDashboard(role)
    const data = await this.dashboardReport.save(this.app, hashId, dataNewDashboard)
    return data
  }

  async getRoleUser(user: UserTokenData) {
    if (!user.payload.roleId) throw new BadRequestException("Role id not found, please relogin")
    let role = await this.ApotekRoleUserRepository.findOne({
      where: {
        id: user.payload.roleId,
      },
      relations: [
        'apotekDetail',
        'roleId'
      ],
      withDeleted: true
    })
    return role;
  }

  /**
   * Generate hash dashboard based user
   */
  getHashDashboard(user: UserTokenData) {
    return `user:${user.id},roleid:${user.payload.roleId}`
  }

  async generateDashboard(role: ApotekRoleUser) {
    const result = {}
    await Promise.all([
      this.getTotalPmr(result, role),
      this.getPmrGraphInterval(result, role),
      this.getTotalPmrApotek(result, role),
      this.getPatientHomecare(result, role),
      this.getPatientAlreadyHomecare(result, role),
      this.getTotalPmrPerApoteker(result, role)
    ])
    return result
  }

  async getTotalPmr(result: Record<string, any>, role: ApotekRoleUser) {
    const [, query] = this.queryBuilder.queryPmr(role)
    query.select("count(*)")
    const data = await query.getRawOne()
    result.TotalPmr = data?.count ?? 0
  }

  async getPmrGraphInterval(result: Record<string, any>, role: ApotekRoleUser) {
    const [, query] = this.queryBuilder.queryApotekDetail(role)
    const apotekList = await query.getMany()
    result.PmrGraphInterval = await this.pmrService.getPmrGraphInterval(apotekList.map((v) => v.id))
  }

  async getTotalPmrApotek(result: Record<string, any>, role: ApotekRoleUser) {
    const [, query] = this.queryBuilder.queryPmr(role)
    query.select([
      `count(*) as total`
    ])
    query.groupBy(this.queryBuilder.AliasApotek)
    query.addGroupBy(this.queryBuilder.AliasApotek + ".id")
    query.orderBy('total', 'DESC')
    const res = await query.getRawOne()

    result.TotalPmrPerApotek = res?.total ?? 0
  }

  async getPatientHomecare(result: Record<string, any>, role: ApotekRoleUser) {
    result.PatientHomecare = await this.queryPatientHomecare(role, { isHomecare: true })
  }

  async getPatientAlreadyHomecare(result: Record<string, any>, role: ApotekRoleUser) {
    result.PatientAlreadyHomecare = await this.queryPatientHomecare(role, { isAlreadyHomecare: true })
  }

  private async queryPatientHomecare(role: ApotekRoleUser, filterEtc: any) {
    const [alias, query] = this.queryBuilder.queryPatient(role)
    query.select('count(*)')
    query.andWhere(`${alias}.etc @> :etcFormat`, { etcFormat: filterEtc })

    const data = await query.getRawOne()
    return data?.count ?? 0
  }

  async getTotalPmrPerApoteker(result: Record<string, any>, role: ApotekRoleUser) {
    const [alias, query] = this.queryBuilder.queryPmr(role)
    query.select([
      `apoteker.username as apoteker_name`,
      `count(*) as total`
    ])
    query.leftJoin(`${alias}.insertBy`, 'apoteker')
    query.groupBy('apoteker.id')
    query.orderBy('total', 'DESC')
    result.TotalPmrPerApoteker = await query.getRawMany()
  }
}