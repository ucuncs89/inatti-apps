import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Controller, Get, Param, ParseBoolPipe, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { isForceValidation } from '../utils';
import { ApotekDashboardService } from './apotek-dashboard.service';

@ApiTags('Data Dashboard Report')
@RequireAuthApp(AppsEnum.Apotek)
@Controller('dashboard-report/apotek')
export class ApotekController {
  constructor(
    private dashboard: ApotekDashboardService
  ) { }

  @Get('v2/dashboard')
  async getDashboard(
    @Query('isForce')
    isForce: boolean,
    @UserCred()
    user: UserTokenData
  ) {
    isForce = isForceValidation(isForce)
    return this.dashboard.getDashboard(user, isForce)
  }
}
