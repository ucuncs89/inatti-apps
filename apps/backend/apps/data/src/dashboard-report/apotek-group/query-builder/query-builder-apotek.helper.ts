import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity"
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity"
import { PmrPatient } from "@app/database/entity/pmr/pmr-patient.entity"
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity"
import { Injectable } from "@nestjs/common"
import { InjectRepository } from "@nestjs/typeorm"
import { Repository, SelectQueryBuilder } from "typeorm"

type QueryBuilder = SelectQueryBuilder<any>
type ReturnQueryBuilder = [alias: string, query: QueryBuilder]

@Injectable()
export class QueryBuilderApotek {
  constructor(
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
  ) { }

  AliasApotek = 'apotek'
  AliasGroup = 'group'

  private setWhereQuery(query: QueryBuilder, role: ApotekRoleUser) {
    query.andWhere(this.AliasApotek + ".id = :idApotek", { idApotek: role.apotekDetail.id })
  }

  /**
   * Set join from any table column to table apotek
   */
  private setJoinApotek(query: QueryBuilder, columnTable: string) {
    query.leftJoin(columnTable, this.AliasApotek)
  }

  queryApotekDetail(role: ApotekRoleUser): ReturnQueryBuilder {
    const query = this.ApotekDetailRepository.createQueryBuilder(this.AliasApotek)
    this.setWhereQuery(query, role)
    return [this.AliasApotek, query]
  }

  queryPatient(role: ApotekRoleUser): ReturnQueryBuilder {
    const alias = 'patient'
    const query = this.SwabPatientRepository.createQueryBuilder(alias)
    this.setJoinApotek(query, `${alias}.createByApotek`)
    this.setWhereQuery(query, role)
    return [alias, query]
  }

  queryPmr(role: ApotekRoleUser): ReturnQueryBuilder {
    const alias = 'pmr'
    const query = this.PmrPatientRepository.createQueryBuilder(alias)
    this.setJoinApotek(query, `${alias}.apotek`)
    this.setWhereQuery(query, role)
    return [alias, query]
  }

}