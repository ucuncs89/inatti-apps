import { ApotekGroupDetail } from "@app/database/entity/apotek-group/apotek-group-detail.entity"
import { ApotekGroupRoleUser } from "@app/database/entity/apotek-group/apotek-group-role-user.entity"
import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity"
import { PmrPatient } from "@app/database/entity/pmr/pmr-patient.entity"
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity"
import { Injectable } from "@nestjs/common"
import { InjectRepository } from "@nestjs/typeorm"
import { Repository, SelectQueryBuilder } from "typeorm"

type QueryBuilder = SelectQueryBuilder<any>
type ReturnQueryBuilder = [alias: string, query: QueryBuilder]

/**
 * @deprecated apotek group not used anymore
 */
@Injectable()
export class QueryBuilderGroup {
  constructor(
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
    @InjectRepository(ApotekGroupDetail)
    private ApotekGroupDetailRepository: Repository<ApotekGroupDetail>,
  ) { }

  AliasApotek = 'apotek'
  AliasGroup = 'group'

  private setWhereQuery(query: QueryBuilder, role: ApotekGroupRoleUser) {
    if (role.roleId.isHeadOffice) {
      query.where(`${this.AliasGroup}.headOffice = :idHeadoffce`, { idHeadoffce: role.apotekGroup.headOffice.id })
    } else {
      query.where(`${this.AliasGroup}.id = :idApotekGroup`, { idApotekGroup: role.apotekGroup.id })
    }
  }

  /**
   * set join from any column that related to table apotek to apotek group
   */
  private setJoinGroup(query: QueryBuilder, tableApotek: string) {
    query.leftJoin(`${tableApotek}.apotekGroup`, this.AliasGroup)
  }

  /**
   * Set join from any table column to table apotek
   */
  private setJoinApotek(query: QueryBuilder, columnTable: string) {
    query.leftJoin(columnTable, this.AliasApotek)
  }

  private setJoinApotekAndGroup(query: QueryBuilder, columnApotek: string, tableApotek: string = this.AliasApotek) {
    this.setJoinApotek(query, columnApotek)
    this.setJoinGroup(query, tableApotek)
  }

  queryApotekDetail(role: ApotekGroupRoleUser): ReturnQueryBuilder {
    const query = this.ApotekDetailRepository.createQueryBuilder(this.AliasApotek)
    this.setJoinGroup(query, this.AliasApotek)
    this.setWhereQuery(query, role)
    return [this.AliasApotek, query]
  }

  queryApotekGroup(role: ApotekGroupRoleUser): ReturnQueryBuilder {
    const query = this.ApotekGroupDetailRepository.createQueryBuilder(this.AliasGroup)
    // get all group information based id head office
    query.where(`${this.AliasGroup}.headOffice = :idHeadoffce`, { idHeadoffce: role.apotekGroup.headOffice.id })
    return [this.AliasGroup, query]
  }

  queryPatient(role: ApotekGroupRoleUser): ReturnQueryBuilder {
    const alias = 'patient'
    const query = this.SwabPatientRepository.createQueryBuilder(alias)
    this.setJoinApotekAndGroup(query, `${alias}.createByApotek`)
    this.setWhereQuery(query, role)
    return [alias, query]
  }

  queryPmr(role: ApotekGroupRoleUser): ReturnQueryBuilder {
    const alias = 'pmr'
    const query = this.PmrPatientRepository.createQueryBuilder(alias)
    this.setJoinApotekAndGroup(query, `${alias}.apotek`)
    query.leftJoin(`${alias}.patient`, 'patient')
    this.setWhereQuery(query, role)
    return [alias, query]
  }

}