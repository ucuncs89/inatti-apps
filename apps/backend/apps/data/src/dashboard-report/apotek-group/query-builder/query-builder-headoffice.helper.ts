import { ApotekGroupDetail } from "@app/database/entity/apotek-group/apotek-group-detail.entity"
import { ApotekGroupRoleUser } from "@app/database/entity/apotek-group/apotek-group-role-user.entity"
import { ApotekDetail } from "@app/database/entity/apotek/apotek-detail.entity"
import { PmrPatient } from "@app/database/entity/pmr/pmr-patient.entity"
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity"
import { Injectable } from "@nestjs/common"
import { InjectRepository } from "@nestjs/typeorm"
import { Repository, SelectQueryBuilder } from "typeorm"

type QueryBuilder = SelectQueryBuilder<any>
type ReturnQueryBuilder = [alias: string, query: QueryBuilder]

export interface IGroupWhere {
  headofficeIds?: string[]
  apotekGroupIds?: string[]
  apotekIds?: string[]
}

@Injectable()
export class QueryBuilderHeadoffice {
  constructor(
    @InjectRepository(ApotekDetail)
    private ApotekDetailRepository: Repository<ApotekDetail>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    @InjectRepository(PmrPatient)
    private PmrPatientRepository: Repository<PmrPatient>,
    @InjectRepository(ApotekGroupDetail)
    private ApotekGroupDetailRepository: Repository<ApotekGroupDetail>,
  ) { }

  AliasApotek = 'apotek'
  AliasGroup = 'group'
  AliasHeadoffice = 'headoffice'

  private arrayToInQuery(array: string[]) {
    if (!Array.isArray(array)) {
      if (typeof array === 'string') {
        return `'${array}'`
      }
      return ''
    }
    return array.map((v) => `'${v}'`).join(',')
  }

  private QueryIn(query: QueryBuilder, column: string, array: string[]) {
    if (!array || array.length < 1) return;
    query.andWhere(`${column} IN (${this.arrayToInQuery(array)})`)
  }

  private setWhereQueryV2(query: QueryBuilder, filter: IGroupWhere) {
    if (!filter) return;
    this.QueryIn(query, `${this.AliasGroup}.headOfficeId`, filter.headofficeIds)
    this.QueryIn(query, `${this.AliasGroup}.id`, filter.apotekGroupIds)
    this.QueryIn(query, `${this.AliasApotek}.id`, filter.apotekIds)
  }

  /**
   * set join from any column that related to table apotek to apotek group
   */
  private setJoinGroup(query: QueryBuilder, tableApotek: string) {
    query.leftJoin(`${tableApotek}.apotekGroup`, this.AliasGroup)
  }

  /**
   * Set join from any table column to table apotek
   */
  private setJoinApotek(query: QueryBuilder, columnTable: string) {
    query.leftJoin(columnTable, this.AliasApotek)
  }

  private setJoinHeadoffice(query: QueryBuilder, tableGroup: string) {
    query.leftJoin(`${tableGroup}.headOffice`, this.AliasHeadoffice)
  }

  private setJoinApotekAndGroup(query: QueryBuilder, columnApotek: string, tableApotek: string = this.AliasApotek) {
    this.setJoinApotek(query, columnApotek)
    this.setJoinGroup(query, tableApotek)
    this.setJoinHeadoffice(query, this.AliasGroup)
  }

  queryApotekDetail(filter: IGroupWhere): ReturnQueryBuilder {
    const query = this.ApotekDetailRepository.createQueryBuilder(this.AliasApotek)
    this.setJoinGroup(query, this.AliasApotek)
    this.setWhereQueryV2(query, filter)
    return [this.AliasApotek, query]
  }

  queryApotekGroup(filter: IGroupWhere): ReturnQueryBuilder {
    const query = this.ApotekGroupDetailRepository.createQueryBuilder(this.AliasGroup)
    // get all group information based id head office
    this.setWhereQueryV2(query, filter)
    return [this.AliasGroup, query]
  }

  queryPatient(filter: IGroupWhere): ReturnQueryBuilder {
    const alias = 'patient'
    const query = this.SwabPatientRepository.createQueryBuilder(alias)
    this.setJoinApotekAndGroup(query, `${alias}.createByApotek`)
    this.setWhereQueryV2(query, filter)
    return [alias, query]
  }

  queryPmr(filter: IGroupWhere): ReturnQueryBuilder {
    const alias = 'pmr'
    const query = this.PmrPatientRepository.createQueryBuilder(alias)
    this.setJoinApotekAndGroup(query, `${alias}.apotek`)
    query.leftJoin(`${alias}.patient`, 'patient')
    this.setWhereQueryV2(query, filter)
    return [alias, query]
  }

}