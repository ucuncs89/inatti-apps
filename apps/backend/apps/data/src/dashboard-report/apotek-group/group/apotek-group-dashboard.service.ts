import { UserTokenData } from "@app/authentication/types";
import { ApotekGroupRoleUser } from "@app/database/entity/apotek-group/apotek-group-role-user.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { get } from "lodash";
import { Repository } from "typeorm";
import { DashboardReportService } from "../../dashboard-report.service";
import { PmrService } from "../../pmr/pmr.service";
import { QueryBuilderGroup } from "../query-builder/query-builder-group.helper";

@Injectable()
export class ApotekGroupDashboardService {
  constructor(
    @InjectRepository(ApotekGroupRoleUser)
    private ApotekGroupRoleUserRepository: Repository<ApotekGroupRoleUser>,
    private dashboardReport: DashboardReportService,
    private queryBuilder: QueryBuilderGroup,
    private pmrService: PmrService
  ) { }

  app = 'apotek-group'

  async getDashboard(user: UserTokenData, isForce?: boolean) {
    const hashId = this.getHashDashboard(user)
    const dashboard = await this.dashboardReport.get(this.app, hashId)
    // if available
    if (dashboard && !isForce) return dashboard

    const role = await this.getRoleUser(user)
    if (!get(role, 'apotekGroup.headOffice.id')) {
      throw new BadRequestException("Sorry, your unit bisnis have no Headoffice. please contact admin")
    }

    // generate here
    const dataNewDashboard = await this.generateDashboard(role)
    const data = await this.dashboardReport.save(this.app, hashId, dataNewDashboard)
    return data
  }

  async getRoleUser(user: UserTokenData) {
    if (!user.payload.roleId) throw new BadRequestException("Role id not found, please relogin")
    let role = await this.ApotekGroupRoleUserRepository.findOne({
      where: {
        id: user.payload.roleId,
      },
      relations: [
        'apotekGroup',
        'apotekGroup.headOffice',
        'roleId'
      ],
      withDeleted: true
    })
    return role;
  }

  /**
   * Generate hash dashboard based user
   */
  getHashDashboard(user: UserTokenData) {
    return `user:${user.id},roleid:${user.payload.roleId}`
  }

  async generateDashboard(role: ApotekGroupRoleUser) {
    const result = {}
    await Promise.all([
      this.getTotalApotek(result, role),
      this.getTotalGroupHeadOffice(result, role),
      this.getTotalPmr(result, role),
      this.getPmrGraphInterval(result, role),
      this.getTotalPmrPerApotek(result, role),
      this.getPatientHomecare(result, role),
      this.getPatientAlreadyHomecare(result, role),
    ])
    return result
  }

  async getTotalApotek(result: Record<string, any>, role: ApotekGroupRoleUser) {
    const [, query] = this.queryBuilder.queryApotekDetail(role)
    query.select('count(*)')
    const data = await query.getRawOne()
    result.TotalApotek = data?.count ?? 0
  }

  async getTotalPmr(result: Record<string, any>, role: ApotekGroupRoleUser) {
    const [, query] = this.queryBuilder.queryPmr(role)
    query.select("count(*)")
    const data = await query.getRawOne()
    result.TotalPmr = data?.count ?? 0
  }

  async getPmrGraphInterval(result: Record<string, any>, role: ApotekGroupRoleUser) {
    const [, query] = this.queryBuilder.queryApotekDetail(role)
    const apotekList = await query.getMany()
    result.PmrGraphInterval = await this.pmrService.getPmrGraphInterval(apotekList.map((v) => v.id))
  }

  async getTotalPmrPerApotek(result: Record<string, any>, role: ApotekGroupRoleUser) {
    const [, query] = this.queryBuilder.queryPmr(role)
    query.select([
      this.queryBuilder.AliasApotek + ".id",
      this.queryBuilder.AliasApotek + ".name",
      `count(*) as total`
    ])
    query.groupBy(this.queryBuilder.AliasApotek)
    query.addGroupBy(this.queryBuilder.AliasApotek + ".id")
    query.orderBy('total', 'DESC')
    result.TotalPmrPerApotek = await query.getRawMany()
  }

  async getPatientHomecare(result: Record<string, any>, role: ApotekGroupRoleUser) {
    result.PatientHomecare = await this.queryPatientHomecare(role, { isHomecare: true })
  }

  async getPatientAlreadyHomecare(result: Record<string, any>, role: ApotekGroupRoleUser) {
    result.PatientAlreadyHomecare = await this.queryPatientHomecare(role, { isAlreadyHomecare: true })
  }

  private async queryPatientHomecare(role: ApotekGroupRoleUser, filterEtc: any) {
    const [alias, query] = this.queryBuilder.queryPatient(role)
    query.select('count(*)')
    query.andWhere(`${alias}.etc @> :etcFormat`, { etcFormat: filterEtc })
    const data = await query.getRawOne()
    return data?.count ?? 0
  }

  async getTotalGroupHeadOffice(result: Record<string, any>, role: ApotekGroupRoleUser) {
    const [, query] = this.queryBuilder.queryApotekGroup(role)
    query.select('count(id)')
    const data = await query.getRawOne()
    result.TotalGroupHeadOffice = data?.count ?? 0
  }
}