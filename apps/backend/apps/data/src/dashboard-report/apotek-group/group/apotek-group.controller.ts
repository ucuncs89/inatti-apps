import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { isForceValidation } from '../utils';
import { ApotekGroupDashboardService } from './apotek-group-dashboard.service';

@ApiTags('Data Dashboard Report')
@RequireAuthApp(AppsEnum.ApotekGroup)
@Controller('dashboard-report/apotek-group')
export class ApotekGroupDashboardController {
  constructor(
    private apotekGroupDashboard: ApotekGroupDashboardService
  ) { }

  @Get('v2/dashboard')
  async getDashboardV2(
    @UserCred()
    user: UserTokenData,
    @Query('isForce')
    isForce: boolean
  ) {
    isForce = isForceValidation(isForce)
    return await this.apotekGroupDashboard.getDashboard(user, isForce)
  }
}
