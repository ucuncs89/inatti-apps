import { Module } from "@nestjs/common";
import { DashboardReportModule } from "../dashboard-report.module";
import { ApotekDashboardService } from "./apotek/apotek-dashboard.service";
import { ApotekGroupDashboardService } from "./group/apotek-group-dashboard.service";
import { ApotekGroupDashboardController } from "./group/apotek-group.controller";
import { ApotekController } from "./apotek/apotek.controller";
import { ApotekHeadofficeDashboardController } from "./headoffice/apotek-headoffice.controller";
import { ApotekHeadofficeDashboardService } from "./headoffice/apotek-headoffice.service";
import { QueryBuilderApotek } from "./query-builder/query-builder-apotek.helper";
import { QueryBuilderGroup } from "./query-builder/query-builder-group.helper";
import { QueryBuilderHeadoffice } from "./query-builder/query-builder-headoffice.helper";
import { ApotekHeadofficeAdminDashboardService } from "./headoffice/apotek-headoffice-byadmin.service";

@Module({
  imports: [
    DashboardReportModule
  ],
  controllers: [
    ApotekGroupDashboardController,
    ApotekController,
    ApotekHeadofficeDashboardController
  ],
  providers: [
    ApotekGroupDashboardService,
    ApotekDashboardService,
    ApotekHeadofficeDashboardService,
    ApotekHeadofficeAdminDashboardService,
    QueryBuilderHeadoffice,
    QueryBuilderGroup,
    QueryBuilderApotek,
  ]
})
export class ApotekDashboardModule { }