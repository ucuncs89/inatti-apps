import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsEnum } from "@app/database/entity/apps/apps.type";
import { Controller, Get, Query } from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { isForceValidation } from '../utils';
import { ApotekHeadofficeAdminDashboardService, FilterHeadofficeDashboardQuery } from "./apotek-headoffice-byadmin.service";
import { ApotekHeadofficeDashboardService } from "./apotek-headoffice.service";
import { ExampleApiDashboardHQ_AdminApps, ExampleApiDashboardHQ_HQApps } from "./spec-api";

@ApiTags('Data Dashboard Report')
@Controller('dashboard-report/apotek-headoffice')
export class ApotekHeadofficeDashboardController {
  constructor(
    private apotekHeadofficeDashbaordService: ApotekHeadofficeDashboardService,
    private headofficeByAdmin: ApotekHeadofficeAdminDashboardService
  ) { }

  @ApiResponse({
    status: 200,
    content: {
      'application/json': {
        example: ExampleApiDashboardHQ_HQApps
      }
    }
  })
  @RequireAuthApp(AppsEnum.ApotekHeadOffice)
  @Get('v2/dashboard')
  async getDashboard(
    @UserCred()
    user: UserTokenData,
    @Query('isForce')
    isForce: boolean
  ) {
    isForce = isForceValidation(isForce)
    return await this.apotekHeadofficeDashbaordService.getDashboard(user, isForce)
  }

  @ApiResponse({
    status: 200,
    content: {
      'application/json': {
        example: ExampleApiDashboardHQ_AdminApps
      }
    }
  })
  @RequireAuthApp(AppsEnum.Admin)
  @Get('v2/dashboard/admin')
  async getDashboardFromAdmin(
    @Query()
    query: FilterHeadofficeDashboardQuery
  ) {
    return await this.headofficeByAdmin.getDashboard(query)
  }
}