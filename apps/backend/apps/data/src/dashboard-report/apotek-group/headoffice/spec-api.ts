export const ExampleApiDashboardHQ_AdminApps = {
  "TotalGroupHeadOffice": "25",
  "TotalApotek": "46",
  "TotalPmr": "13",
  "TotalPmrPerApotek": [
    {
      "apotek_id": "7380cbd4-1828-4875-a0c5-295fb4682591",
      "apotek_name": "INi yang banyak",
      "total": "6"
    },
    {
      "apotek_id": "2c493cbc-8c71-45f1-81b6-69aef98baf94",
      "apotek_name": "Tes44t132",
      "total": "2"
    },
    {
      "apotek_id": "360b5254-2066-465a-9905-192b9c05d6a7",
      "apotek_name": "51233 tst12313",
      "total": "2"
    },
    {
      "apotek_id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
      "apotek_name": "4 Apotek 123123 123 tet 12313",
      "total": "1"
    },
    {
      "apotek_id": "9f2c5b69-1fd0-4e80-a85b-6b8e856f78b2",
      "apotek_name": "1 test",
      "total": "1"
    },
    {
      "apotek_id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
      "apotek_name": "3 Apotek 123 baru 123123",
      "total": "1"
    }
  ],
  "PmrGraphInterval": {
    "day": [],
    "month": [
      {
        "value": "1",
        "id": "9f2c5b69-1fd0-4e80-a85b-6b8e856f78b2",
        "group": "1 test",
        "xaxis": "2022-06-01"
      },
      {
        "value": "1",
        "id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
        "group": "4 Apotek 123123 123 tet 12313",
        "xaxis": "2022-06-01"
      },
      {
        "value": "3",
        "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
        "group": "INi yang banyak",
        "xaxis": "2022-06-01"
      },
      {
        "value": "2",
        "id": "360b5254-2066-465a-9905-192b9c05d6a7",
        "group": "51233 tst12313",
        "xaxis": "2022-06-01"
      },
      {
        "value": "2",
        "id": "2c493cbc-8c71-45f1-81b6-69aef98baf94",
        "group": "Tes44t132",
        "xaxis": "2022-06-01"
      },
      {
        "value": "1",
        "id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
        "group": "3 Apotek 123 baru 123123",
        "xaxis": "2022-07-01"
      },
      {
        "value": "2",
        "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
        "group": "INi yang banyak",
        "xaxis": "2022-07-01"
      },
      {
        "value": "1",
        "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
        "group": "INi yang banyak",
        "xaxis": "2022-09-01"
      }
    ],
    "year": [
      {
        "value": "1",
        "id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
        "group": "3 Apotek 123 baru 123123",
        "xaxis": "2022-01-01"
      },
      {
        "value": "1",
        "id": "9f2c5b69-1fd0-4e80-a85b-6b8e856f78b2",
        "group": "1 test",
        "xaxis": "2022-01-01"
      },
      {
        "value": "1",
        "id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
        "group": "4 Apotek 123123 123 tet 12313",
        "xaxis": "2022-01-01"
      },
      {
        "value": "6",
        "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
        "group": "INi yang banyak",
        "xaxis": "2022-01-01"
      },
      {
        "value": "2",
        "id": "360b5254-2066-465a-9905-192b9c05d6a7",
        "group": "51233 tst12313",
        "xaxis": "2022-01-01"
      },
      {
        "value": "2",
        "id": "2c493cbc-8c71-45f1-81b6-69aef98baf94",
        "group": "Tes44t132",
        "xaxis": "2022-01-01"
      }
    ]
  },
  "PatientHomecare": "13",
  "PatientAlreadyHomecare": "8",
  "TotalPmrPerGroup": [
    {
      "group_id": "b2805d9d-78ef-468d-bf1c-6d6f4048e026",
      "group_name": "Apotek group 1 123123",
      "total": "8"
    },
    {
      "group_id": "5e04dd3a-0bff-438d-a524-4863d83bc0e8",
      "group_name": "Apotekg roup 123",
      "total": "4"
    },
    {
      "group_id": "06526707-1a6f-4917-bdfd-a21b32c30ebf",
      "group_name": "New 12",
      "total": "1"
    }
  ]
}

export const ExampleApiDashboardHQ_HQApps = {
  "id": 20,
  "idApp": "apotek-headoffice:user:515963f5-273e-49de-ae4d-a81076c3390e,roleid:1",
  "data": {
    "TotalApotek": "46",
    "TotalGroupHeadOffice": "1",
    "TotalPmr": "8",
    "TotalPmrPerApotek": [
      {
        "apotek_id": "7380cbd4-1828-4875-a0c5-295fb4682591",
        "apotek_name": "INi yang banyak",
        "total": "6"
      },
      {
        "apotek_id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
        "apotek_name": "4 Apotek 123123 123 tet 12313",
        "total": "1"
      },
      {
        "apotek_id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
        "apotek_name": "3 Apotek 123 baru 123123",
        "total": "1"
      }
    ],
    "PatientAlreadyHomecare": "1",
    "PatientHomecare": "1",
    "TotalPmrPerGroup": [
      {
        "group_id": "b2805d9d-78ef-468d-bf1c-6d6f4048e026",
        "group_name": "Apotek group 1 123123",
        "total": "8"
      }
    ],
    "PmrGraphInterval": {
      "day": [],
      "month": [
        {
          "value": "1",
          "id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
          "group": "4 Apotek 123123 123 tet 12313",
          "xaxis": "2022-06-01"
        },
        {
          "value": "3",
          "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
          "group": "INi yang banyak",
          "xaxis": "2022-06-01"
        },
        {
          "value": "1",
          "id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
          "group": "3 Apotek 123 baru 123123",
          "xaxis": "2022-07-01"
        },
        {
          "value": "2",
          "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
          "group": "INi yang banyak",
          "xaxis": "2022-07-01"
        },
        {
          "value": "1",
          "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
          "group": "INi yang banyak",
          "xaxis": "2022-09-01"
        }
      ],
      "year": [
        {
          "value": "1",
          "id": "c0e61817-9951-4b3f-a6cb-8c6451857379",
          "group": "3 Apotek 123 baru 123123",
          "xaxis": "2022-01-01"
        },
        {
          "value": "1",
          "id": "7c11a9e4-e1a0-4c18-a7d5-d3c9e606bf44",
          "group": "4 Apotek 123123 123 tet 12313",
          "xaxis": "2022-01-01"
        },
        {
          "value": "6",
          "id": "7380cbd4-1828-4875-a0c5-295fb4682591",
          "group": "INi yang banyak",
          "xaxis": "2022-01-01"
        }
      ]
    }
  },
  "updatedAt": "2022-10-31T03:16:42.073Z",
  "createdAt": "2022-10-25T03:30:49.769Z"
}