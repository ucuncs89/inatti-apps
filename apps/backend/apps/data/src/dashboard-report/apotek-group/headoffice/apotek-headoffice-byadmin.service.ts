import { Injectable } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IGroupWhere } from "../query-builder/query-builder-headoffice.helper";
import { ApotekHeadofficeDashboardService } from "./apotek-headoffice.service";

export class FilterHeadofficeDashboardQuery implements IGroupWhere {
  @ApiProperty({ required: false })
  headofficeIds?: string[];

  @ApiProperty({ required: false })
  apotekGroupIds?: string[];

  @ApiProperty({ required: false })
  apotekIds?: string[];
}

@Injectable()
export class ApotekHeadofficeAdminDashboardService {
  constructor(
    private headofficeDashboard: ApotekHeadofficeDashboardService
  ) { }

  async getDashboard(query: FilterHeadofficeDashboardQuery) {
    return await this.headofficeDashboard.generateDashboard(query)
  }
}