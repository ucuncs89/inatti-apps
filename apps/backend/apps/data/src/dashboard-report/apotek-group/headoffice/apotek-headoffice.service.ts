import { UserTokenData } from "@app/authentication/types";
import { ApotekHeadofficeRoleUser, ROLEID_HEADOFFICE } from "@app/database/entity/apotek-head-office/apotek-head-office-role-user.entity";
import { BadRequestException, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { DashboardReportService } from "../../dashboard-report.service";
import { PmrService } from "../../pmr/pmr.service";
import { IGroupWhere, QueryBuilderHeadoffice } from "../query-builder/query-builder-headoffice.helper";

interface ResponseGeneratorDashboard {
  data: any
  key: string
}

@Injectable()
export class ApotekHeadofficeDashboardService {
  constructor(
    @InjectRepository(ApotekHeadofficeRoleUser)
    private ApotekHeadofficeRoleUserRepository: Repository<ApotekHeadofficeRoleUser>,
    private dashboardReport: DashboardReportService,
    private queryBuilder: QueryBuilderHeadoffice,
    private pmrService: PmrService
  ) { }

  app = 'apotek-headoffice'

  async getDashboard(user: UserTokenData, isForce?: boolean) {
    const hashId = this.getHashDashboard(user)
    const dashboard = await this.dashboardReport.get(this.app, hashId)
    // if available
    if (dashboard && !isForce) return dashboard

    const role = await this.getRoleUser(user)
    const filter = this.getFilter(role)

    // generate here
    const dataNewDashboard = await this.generateDashboard(filter)
    const data = await this.dashboardReport.save(this.app, hashId, dataNewDashboard)
    return data
  }

  async getRoleUser(user: UserTokenData) {
    if (!user.payload.roleId) throw new BadRequestException("Role id not found, please relogin")
    let role = await this.ApotekHeadofficeRoleUserRepository.findOne({
      where: {
        id: user.payload.roleId,
      },
      relations: [
        'apotekGroup',
        'apotekHeadoffice',
        'roleId'
      ],
      withDeleted: true
    })
    return role;
  }

  getFilter(role: ApotekHeadofficeRoleUser): IGroupWhere {
    if (role.roleId.id === ROLEID_HEADOFFICE.HEADOFFICE) {
      return {
        headofficeIds: [role.apotekHeadoffice.id]
      }
    }
    if (role.roleId.id === ROLEID_HEADOFFICE.BM) {
      return {
        apotekGroupIds: role.apotekGroup.map((v) => v.apotekGroup.id),
      }
    }
  }

  /**
   * Generate hash dashboard based user
   */
  getHashDashboard(user: UserTokenData) {
    return `user:${user.id},roleid:${user.payload.roleId}`
  }

  async generateDashboard(filter: IGroupWhere) {
    const result = await this.runnerCommand([
      'getTotalApotek',
      'getTotalGroupHeadOffice',
      'getTotalPmr',
      'getPmrGraphInterval',
      'getTotalPmrPerApotek',
      'getPatientHomecare',
      'getPatientAlreadyHomecare',
      'getTotalPmrPerGroup',
    ], filter)
    return result
  }

  private async runnerCommand(commands: string[], filter: IGroupWhere) {
    const logger = new Logger('ApotekHeadofficeDashboardService')
    logger.verbose(`Setup command with filter : ${JSON.stringify(filter)}`)
    const executor = async (command: string) => {
      try {
        logger.verbose(`Running command ${command}`)
        const res = await this[command](filter)
        logger.verbose(`Finish command ${command}`)
        return res;
      } catch (err) {
        logger.error(`Error command ${command}`)
        logger.error(err)
        return null
      }
    }

    const result = {}

    await Promise.all(commands.map((command) => {
      return executor(command).then((res) => {
        if (!res) return;
        result[res.key] = res.data
      })
    }))

    return result
  }

  async getTotalApotek(result: Record<string, any>, filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryApotekDetail(filter)
    query.select('count(*)')
    const data = await query.getRawOne()
    return {
      key: 'TotalApotek',
      data: data?.count ?? 0
    }
  }

  async getTotalPmr(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryPmr(filter)
    query.select("count(*)")
    const data = await query.getRawOne()

    return {
      key: 'TotalPmr',
      data: data?.count ?? 0
    }
  }

  async getPmrGraphInterval(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryApotekDetail(filter)
    const apotekList = await query.getMany()
    const result = await this.pmrService.getPmrGraphInterval(apotekList.map((v) => v.id))
    return {
      key: 'PmrGraphInterval',
      data: result
    }
  }

  async getTotalPmrPerApotek(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryPmr(filter)
    query.select([
      this.queryBuilder.AliasApotek + ".id",
      this.queryBuilder.AliasApotek + ".name",
      this.queryBuilder.AliasGroup + ".name as group_name",
      this.queryBuilder.AliasHeadoffice + ".name as headoffice_name",
      `count(*) as total`
    ])
    query.groupBy(this.queryBuilder.AliasApotek)
    query.addGroupBy(this.queryBuilder.AliasApotek + ".id")
    query.addGroupBy(this.queryBuilder.AliasGroup + ".name")
    query.addGroupBy(this.queryBuilder.AliasHeadoffice + ".name")
    query.orderBy('total', 'DESC')
    const result = await query.getRawMany()
    return {
      key: 'TotalPmrPerApotek',
      data: result
    }
  }

  async getPatientHomecare(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const result = await this.queryPatientHomecare(filter, { isHomecare: true })
    return {
      key: 'PatientHomecare',
      data: result
    }
  }

  async getPatientAlreadyHomecare(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const result = await this.queryPatientHomecare(filter, { isAlreadyHomecare: true })
    return {
      key: 'PatientAlreadyHomecare',
      data: result
    }
  }

  private async queryPatientHomecare(filter: IGroupWhere, filterEtc: any) {
    const [alias, query] = this.queryBuilder.queryPatient(filter)
    query.select('count(*)')
    query.andWhere(`${alias}.etc @> :etcFormat`, { etcFormat: filterEtc })
    const data = await query.getRawOne()
    return data?.count ?? 0
  }

  async getTotalGroupHeadOffice(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryApotekGroup(filter)
    query.select('count(id)')
    const data = await query.getRawOne()
    return {
      key: 'TotalGroupHeadOffice',
      data: data?.count ?? 0
    }
  }

  async getTotalPmrPerGroup(filter: IGroupWhere): Promise<ResponseGeneratorDashboard> {
    const [, query] = this.queryBuilder.queryPmr(filter)
    query.select([
      this.queryBuilder.AliasGroup + ".id",
      this.queryBuilder.AliasGroup + ".name",
      this.queryBuilder.AliasHeadoffice + ".name as headoffice_name",
      `count(*) as total`
    ])
    query.addGroupBy(this.queryBuilder.AliasGroup + ".id")
    query.addGroupBy(this.queryBuilder.AliasGroup + ".name")
    query.addGroupBy(this.queryBuilder.AliasHeadoffice + ".name")
    query.orderBy('total', 'DESC')
    const result = await query.getRawMany()
    return {
      key: 'TotalPmrPerGroup',
      data: result
    }
  }
}