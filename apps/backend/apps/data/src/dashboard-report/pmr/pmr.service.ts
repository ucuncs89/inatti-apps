import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';
import { QueryInterval } from '../helper/query-builder';

@Injectable()
export class PmrService {
  constructor() { }

  async getPmrGraphInterval(apotekId: string | string[]) {
    return QueryInterval((interval, limitQuery) => {
      return this.getPmrGraph(apotekId, interval, limitQuery)
    })
  }

  private async getPmrGraph(apotekId: string | string[], interval: string, limitQuery: (...parms: any[]) => string) {
    let where = ''
    if (!apotekId) return []
    if (Array.isArray(apotekId)) {
      if (apotekId.length < 1) return []

      apotekId = apotekId.map((v) => `'${v}'`)
      where = `apotek.id IN (${apotekId.join(',')})`
    } else {
      where = `apotek.id = '${apotekId}'`
    }

    if (limitQuery) {
      where = `${where} AND ${limitQuery('pmr."createdAt"')}`
    }

    let query = `select
        count(*) as value,
        apotek.id, apotek."name" as group,
        date_trunc('${interval}', pmr."createdAt")::date::varchar as xaxis
      from pmr_patient pmr
      inner join apotek_detail apotek on apotek.id = pmr."apotekId" 
      where ${where}
      group by 
        xaxis, apotek.id, apotek."name" 
        order by xaxis desc`

    let result = await getManager().query(query)
    if (Array.isArray(result)) {
      // reverse becasue order by desc
      result.reverse()
    }

    return result
  }
}
