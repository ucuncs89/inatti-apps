import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { AppsExcludeUser } from "@app/database/entity/apps/apps.type";
import { Body, Controller, Post } from "@nestjs/common";
import { LoginTenancyDTO } from "@inatti/shared/dtos/lib/tenancy/login-tenancy.dto"
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { omit } from "lodash";
import { JwtService } from "@nestjs/jwt";
import { ApiTags } from "@nestjs/swagger";

@Controller()
export class LoginTenancy {
  constructor(
    private jwtService: JwtService
  ) { }

  /**
  * login tenancy prototype
  * @param body 
  * @param userCred 
  * @returns 
  */
  @RequireAuthApp(...AppsExcludeUser)
  @ApiTags('Authentication')
  @Post('login/tenancy')
  loginTenancy(
    @Body() body: LoginTenancyDTO,
    @UserCred() userCred: UserTokenData
  ) {
    const payload = omit(userCred.payload, ['iat', 'exp'])
    payload.roleId = body.roleId

    const token = this.jwtService.sign(payload, { expiresIn: '7d' })
    return { token }
  }
}