import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { LoginTenancy } from "./login-tenancy.controller";

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    })
  ],
  controllers: [
    LoginTenancy
  ]
})
export class TenancyModule { }