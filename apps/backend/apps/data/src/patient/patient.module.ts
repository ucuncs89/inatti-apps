import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { ApotekGroupModule } from '../apotek-group/apotek-group.module';
import { PatientSecurityService } from './patient-security.service';
import { PatientController } from './patient.controller';
import { PatientService } from './patient.service';

@Module({
  imports: [
    DatabaseModule,
    ApotekGroupModule,
  ],
  controllers: [
    PatientController
  ],
  providers: [
    PatientService,
    PatientSecurityService
  ],
  exports: [
    PatientService
  ]
})
export class PatientModule { }
