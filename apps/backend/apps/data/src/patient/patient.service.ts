import { Injectable } from "@nestjs/common";
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { User } from '@app/database/entity/user/user.entity';
import { FilterPatientMedicine, Pagination, PaginationOptionsInterface, PatientEtcEnum } from '@inatti/shared/dtos';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, FindConditions, ObjectLiteral, Repository, SelectQueryBuilder } from 'typeorm';
import { PatientDTO, QuerySearchPatient } from '@inatti/shared/dtos';
import { mapValues, pick } from "lodash";
import { ShouldArray } from "@app/helper/utils/array";
import { Response } from "express";
import { FormatDeepKey } from "@app/helper/utils/object";
import { deepValueToXlsx } from "@app/helper/report-xlsx/to-xlsx";
import { Provinsi } from "@app/database/entity/region/provinsi.entity";
import { Kabupaten } from "@app/database/entity/region/kabupaten.entity";
import { Kecamatan } from "@app/database/entity/region/kecamatan.entity";
import { Kelurahan } from "@app/database/entity/region/kelurahan.entity";

type SwabPatientWhere = FindConditions<SwabPatient>[] | FindConditions<SwabPatient> | ObjectLiteral | string

const TablePatient = 'patient'
const PatientCol = (cols: string) => TablePatient + '.' + cols

@Injectable()
export class PatientService {
  constructor(
    @InjectRepository(SwabPatient)
    private patientSwabRepository: Repository<SwabPatient>,
    @InjectRepository(Provinsi)
    private ProvinsiRepository: Repository<Provinsi>,
    @InjectRepository(Kabupaten)
    private KabupatenRepository: Repository<Kabupaten>,
    @InjectRepository(Kecamatan)
    private KecamatanRepository: Repository<Kecamatan>,
    @InjectRepository(Kelurahan)
    private KelurahanRepository: Repository<Kelurahan>,
  ) { }

  /** 
   * Process create patient, this process used 
   * by other module (ref, user, and transaction)
   * becarefull if you update this
   */
  async create(patientData: PatientDTO, part?: Partial<SwabPatient>) {
    let patient = new SwabPatient()
    // remove null value
    Object.keys(patientData).forEach(key => {
      if (patientData[key] === null) {
        delete patientData[key]
      }
    })

    Object.assign(patient, patientData)
    if (part) {
      Object.assign(patient, part)
    }
    await this.validateRegionPatient(patient)
    return this.patientSwabRepository.save(patient)
  }

  async validateRegionPatient(patient: SwabPatient) {
    const getData = async (repo: any, id: number) => {
      const data = await repo.findOne(id)
      if (!data) return null;
      return data.id
    }
    const validateProvinsi = (id: number) => getData(this.ProvinsiRepository, id)
    const validateKabupaten = (id: number) => getData(this.KabupatenRepository, id)
    const validateKecamatan = (id: number) => getData(this.KecamatanRepository, id)
    const validateKeluarahan = (id: number) => getData(this.KelurahanRepository, id)

    const map: Partial<Record<keyof SwabPatient, (id: number) => any>> = {
      provinsi: validateProvinsi,
      kabupaten: validateKabupaten,
      kecamatan: validateKecamatan,
      kelurahan: validateKeluarahan,
      domisiliProvinsi: validateProvinsi,
      domisiliKabupaten: validateKabupaten,
      domisiliKecamatan: validateKecamatan,
      domisiliKelurahan: validateKeluarahan,
    }

    const mapKey = Object.keys(map)
    for (let i = 0; i < Object.keys(patient).length; i++) {
      const key = Object.keys(patient)[i];
      if (mapKey.includes(key)) {
        const idData = patient[key]
        patient[key] = await map[key](idData)
      }
    }
    return patient
  }

  relationsPatient = [
    'provinsi',
    'kabupaten',
    'kecamatan',
    'kelurahan',
    'domisiliProvinsi',
    'domisiliKabupaten',
    'domisiliKecamatan',
    'domisiliKelurahan',
    'ownedBy',
    'createByApotek',
    'createByApotek.apotekGroup',
    'createByApotek.apotekGroup.headOffice',
  ]

  async findOne(patientId: string, select?: string[]) {
    const payloadRepo: any = {
      where: {
        id: patientId,
      },
      relations: this.relationsPatient,
      withDeleted: true
    }

    if (select) {
      payloadRepo.select = select
      payloadRepo.relations = payloadRepo.relations.filter((v) => select.includes(v))
    }

    let res = await this.patientSwabRepository.findOne(payloadRepo)
    if (res.ownedBy) {
      res.ownedBy.hideCredential()
    }
    return res;
  }

  /**
   * Find all patient, this action not require ref id
   * because all patient became 1 database.
   */
  async findAll(paginate: PaginationOptionsInterface, filter: FilterPatientMedicine) {
    let query = this.findAllQueryBuilder({
      paginate,
      filter,
    })
    const results = await query.findManyPaginate()

    const total = await query.count()

    return new Pagination({
      results,
      total,
    }, paginate)
  }

  findAllQueryBuilder(opt: {
    filter: FilterPatientMedicine,
    paginate?: PaginationOptionsInterface,
  }) {
    let repository = this.patientSwabRepository.createQueryBuilder(TablePatient)

    repository.leftJoinAndSelect(PatientCol('createByApotek'), 'createByApotek')
    repository.leftJoinAndSelect('createByApotek.apotekGroup', 'apotekGroup')
    repository.orderBy(PatientCol('createdAt'), 'DESC')

    this.patientRepoFilter(opt.filter, repository)

    const setPaginate = () => {
      if (opt.paginate) {
        repository.take(opt.paginate.take)
        repository.skip(opt.paginate.skip)
      }
    }

    const resetPaginate = () => {
      repository.take(null)
      repository.skip(null)
    }

    return {
      findManyPaginate: () => {
        setPaginate()
        return repository.getMany()
      },
      findMany: () => {
        resetPaginate()
        return repository.getMany()
      },
      count: () => {
        resetPaginate()
        return repository.getCount()
      }
    }
  }

  patientRepoFilter(filter: FilterPatientMedicine, repository: SelectQueryBuilder<SwabPatient>) {
    let where: SwabPatientWhere = {}
    if (filter) {
      if (filter.date && filter.date.value) {
        // assume it's a 'today' param
        repository.andWhere(PatientCol('created_at::date >= :createdAt'), { createdAt: filter.date.value })
      }

      if (filter.rangedate) {
        const start = filter.rangedate._value[0]
        const end = filter.rangedate._value[1]
        repository.andWhere(PatientCol('createdAt between :createdAtStart and :createdAtEnd'), { createdAtStart: start, createdAtEnd: end })
      }

      if (filter.apotek?.length) {
        repository.andWhere(PatientCol('createByApotek IN (:...apotekId)'), { apotekId: ShouldArray(filter.apotek) })
      }

      if (filter.apotekGroup?.length && !filter.apotek?.length) {
        repository.andWhere('apotekGroup.id IN (:...apotekGroupId)', { apotekGroupId: ShouldArray(filter.apotekGroup) })
      }

      if (filter.headOffice) {
        repository.andWhere('apotekGroup.headOffice = :headOffice', { headOffice: filter.headOffice })
      }

      if (filter.etc) {
        filter.etc = mapValues(filter.etc, (val) => val === 'true')
        repository.andWhere(PatientCol('etc @> :etcFilter'), { etcFilter: filter.etc })
      }

      if (filter.search) {
        repository.andWhere(new Brackets((qb) => {
          qb.where(PatientCol(`name ILIKE '%${filter.search}%'`))
          qb.orWhere(PatientCol(`nik ILIKE '%${filter.search}%'`))
        }))
      }
    }
    return where
  }

  async edit(patientId: string, patientData: PatientDTO) {
    let patient = await this.patientSwabRepository.findOne({
      id: patientId
    })
    if (!patient) throw "Patient not found"
    Object.assign(patient, patientData)
    await this.validateRegionPatient(patient)
    return this.patientSwabRepository.save(patient)
  }

  delete(patientId: string) {
    return this.patientSwabRepository.softDelete(patientId);
  }

  search(option: QuerySearchPatient) {
    let repository = this.patientSwabRepository
      .createQueryBuilder('patient')
    if (option.query) {
      repository.orWhere('nik ilike :q', {
        q: `%${option.query}%`
      })
        .orWhere('name ilike :q', {
          q: `%${option.query}%`
        });
    } else if (option.id) {
      repository.where('id = :id', { id: option.id })
    }
    return repository.getMany()
  }

  async assignToUser(patientId: string, targetUser: string) {
    let patient = await this.findOne(patientId)
    let user = new User(targetUser)
    patient.ownedBy = user
    return await this.patientSwabRepository.save(patient)
  }

  async getByUser(userId: String, select?: string[]) {
    let relationsPatient = this.relationsPatient.filter((v) => !v.includes('ownedBy'))

    let res: any = await this.patientSwabRepository.find({
      where: {
        ownedBy: {
          id: userId
        }
      },
      relations: relationsPatient
    })

    if (select) {
      res = res.map((v) => pick(v, select))
    }

    return res;
  }

  async toExcel(filter: FilterPatientMedicine, res: Response) {
    let query = this.findAllQueryBuilder({
      filter,
    })
    const results = await query.findManyPaginate()

    const etc = PatientEtcEnum
    const etcData = (col: string) => 'etc.' + col

    const formatValueXlsx: FormatDeepKey = {
      "Id": 'id',
      "Nik": 'nik',
      "Passport": 'passport',
      "Type": 'type',
      "Nama": 'name',
      "Tempate Lahir": 'placebirth',
      "Tanggal Lahir": 'datebirth',
      "Jenis Kelamin": 'gender',
      "Nomor hp": 'phonenumber',
      "Email": 'email',
      "Alamat": 'address',
      "Warganegara": 'citizenship',
      "Berat": 'weight',
      "Tinggi": 'height',
      "Pekerjaan": 'job',
      "Bersedia Homecare": etcData(etc.isHomecare),
      "Sudah Homecare": etcData(etc.isAlreadyHomecare),
      "Tanggal dibuat": 'createdAt',
      "Apotek id": 'createByApotek.id',
      "Apotek Nama": 'createByApotek.name',
      "Unit bisnis id": 'createByApotek.apotekGroup.id',
      "Unit bisnis Nama": 'createByApotek.apotekGroup.name',
    }

    return deepValueToXlsx(res, results, formatValueXlsx)
  }
}