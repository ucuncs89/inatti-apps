import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { UserTokenData } from '@app/authentication/types';
import { ApotekDetail } from '@app/database/entity/apotek/apotek-detail.entity';
import { AppsAll, AppsEnum, AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { User } from '@app/database/entity/user/user.entity';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { QuerySelectApi } from '@app/helper/query-select-transform/QuerySelectTransform.service';
import { FilterPatientMedicine, NullDataPagination, PaginationOptionsInterface, PatientDTO, QuerySearchPatient } from '@inatti/shared/dtos';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, Response } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Response as ResponseExpress } from 'express';
import { HeadofficeManageGroup } from '../apotek-group/headoffice/headoffice-manage-group.service';
import { PatientSecurityService } from './patient-security.service';
import { PatientService } from './patient.service';

@ApiTags('API Patient v2')
@Controller('patient-v2')
export class PatientController {
  constructor(
    private patientService: PatientService,
    private patientSecurityService: PatientSecurityService,
    private headofficeManageGroup: HeadofficeManageGroup
  ) { }

  @RequireAuthApp(...AppsExcludeUser)
  @Get('list')
  async getPatient(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: FilterPatientMedicine,
    @UserCred()
    user: UserTokenData
  ) {
    if (!filter) {
      filter = {} as any
    }
    try {
      if (user.payload.apps === AppsEnum.Apotek && (!filter?.apotek?.length)) {
        return NullDataPagination(paginate)
      }
      /**
       * If from app apotek group
       * but this is deprecated, apotek group not used anymore
       */
      if (user.payload.apps === AppsEnum.ApotekGroup) {
        if (!filter?.apotekGroup) return NullDataPagination(paginate)

        if (Array.isArray(filter?.apotekGroup) && filter.apotekGroup.length < 1) {
          return NullDataPagination(paginate)
        }
        // sometimes it give object
        filter.apotekGroup = Object.values(filter.apotekGroup)
        if (filter.apotekGroup.length < 1) {
          return NullDataPagination(paginate)
        }
      }
      /**
       * if user apotek, filter based head office
       * filter.apotek will be ignored
       */
      if (user.payload.apps === AppsEnum.Apotek && filter.forceFilterApotek !== 'true') {
        console.log('run in apotek')
        const roleApotek = await this.patientSecurityService.getDetailRoleUser(+user.payload.roleId)
        delete filter.apotek

        const result = await this.patientService.findAll(paginate, filter)
        return this.patientSecurityService.labellingPatientPaginations(result, roleApotek)
      }
      /**
       * If user from hq and role is Business manager and filter apotek group is null
       * add filter.apotekGroup to all that assigned to user
       */
      if (user.payload.apps === AppsEnum.ApotekHeadOffice) {
        filter.apotekGroup = await this.headofficeManageGroup.getFilterApotekGroup(user, filter?.apotekGroup)
      }
      return await this.patientService.findAll(paginate, filter)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * This API created after getPatientById,
   * getPatientById already used on ref app, if change that, maybe cause problem on frontend
   */

  @QuerySelectApi()
  @RequireAuthApp(...AppsAll)
  @Get(':idpatient')
  async getPatientDetail(
    @Param('idpatient')
    idPatient: string,
    @Query('select')
    select: string[],
    @UserCred()
    user: UserTokenData
  ) {
    if (user?.payload?.apps === AppsEnum.Apotek) {
      const roleApotek = await this.patientSecurityService.getDetailRoleUser(+user.payload.roleId)
      const result = await this.patientService.findOne(idPatient, select);
      return this.patientSecurityService.labelPatientEditable(result, roleApotek)
    }
    return await this.patientService.findOne(idPatient, select);
  }

  @RequireAuthApp(...AppsExcludeUser)
  @Get('search')
  async getPatientSearch(
    @Query()
    querySearch: QuerySearchPatient
  ) {
    try {
      return await this.patientService.search(querySearch)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * Add User by user, so can assign to user id instantly
   * @param patientDTO 
   * @param 
   * @returns 
   */
  @ApiOperation({ description: "API add patient for user" })
  @RequireAuthApp(AppsEnum.User)
  @Post('/by-user')
  async addPatientByUser(
    @Body()
    patientDTO: PatientDTO,
    @UserCred()
    user: User
  ) {
    try {
      patientDTO.ownedBy = user.id
      return await this.patientService.create(patientDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * Add User by admin exclude user, so set owned by to null
   * @param patientDTO 
   * @param 
   * @returns 
   */
  @ApiOperation({ description: "API add patient for admin (ref, apotek, dll)" })
  @RequireAuthApp(...AppsExcludeUser)
  @Post('/by-admin')
  async addPatientByAdmin(
    @Body()
    patientDTO: PatientDTO
  ) {
    try {
      return await this.patientService.create(patientDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * Exclude add patient
   * @param patientDTO 
   * @returns 
   */
  @ApiOperation({ description: "API add patient for admin (ref, apotek, dll)" })
  @RequireAuthApp(...AppsExcludeUser)
  @Post('/by-apotek')
  async addPatientByApotek(
    @Body()
    patientDTO: PatientDTO,
    @Query('apotekId')
    apotekId: string
  ) {
    try {
      return await this.patientService.create(patientDTO, {
        createByApotek: new ApotekDetail(apotekId)
      })
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @RequireAuthApp()
  @Patch(':idpatient')
  async editPatient(
    @Param('idpatient')
    idPatient: string,
    @Body()
    patientDTO: PatientDTO,
    @UserCred()
    user: UserTokenData
  ) {
    await this.patientSecurityService.canUserDoAction(user, idPatient)
    return await this.patientService.edit(idPatient, patientDTO)
  }

  @RequireAuthApp(...AppsExcludeUser)
  @Delete(':idpatient')
  async deletePatient(
    @Param('idpatient')
    idPatient: string,
    @UserCred()
    user: UserTokenData
  ) {
    await this.patientSecurityService.canUserDoAction(user, idPatient)
    return await this.patientService.delete(idPatient)
  }

  @RequireAuthApp(...AppsExcludeUser)
  @Patch('patient-owned-by/:idpatient')
  async setOwner(
    @Param('idpatient')
    patientId: string,
    @Query('targetUser')
    targetUser: string
  ) {
    return await this.patientService.assignToUser(patientId, targetUser)
  }

  @ApiQuery({
    name: 'userId',
    required: false,
    description: 'User id filled by admin'
  })
  @ApiQuery({
    name: 'select',
    required: false,
  })
  @RequireAuthApp()
  @Get('/list/by-user')
  async getPatientOwnedByUser(
    @UserCred()
    userLoggedIn: UserTokenData,
    @Query('userId')
    userId: string,
    @Query('select')
    select: string[]
  ) {
    if (userId) {
      if (userId !== userLoggedIn.id && userLoggedIn.payload.apps === AppsEnum.User) {
        throw new BadRequestException('You are not allowed to access this API')
      }
    }

    return await this.patientService.getByUser(userId ?? userLoggedIn.id, select)
  }

  @RequireAuthApp(AppsEnum.Apotek, AppsEnum.ApotekGroup, AppsEnum.ApotekHeadOffice)
  @Get('/export/to-xlsx')
  async getExcelList(
    @Query('filter', new FilterQueryPipe())
    filter: FilterPatientMedicine,
    @Response()
    res: ResponseExpress
  ) {
    return this.patientService.toExcel(filter, res)
  }
}