import { UserTokenData } from "@app/authentication/types";
import { ApotekRoleUser } from "@app/database/entity/apotek/apotek-role-user.entity";
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { Pagination } from "@inatti/shared/dtos";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { PatientService } from "./patient.service";

@Injectable()
export class PatientSecurityService {
  constructor(
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    @InjectRepository(ApotekRoleUser)
    private ApotekRoleUserRepository: Repository<ApotekRoleUser>,
    private patientService: PatientService
  ) { }

  async getCreatorPatient(patientId: string) {
    const patient = await this.SwabPatientRepository.findOne({
      where: {
        id: patientId
      },
      relations: ['createByApotek']
    })
    if (!patient) throw new BadRequestException("Patient Not found")
    if (!patient.createByApotek) throw new BadRequestException("Patient not belong to any apotek")
    return patient.createByApotek
  }

  async getDetailRoleUser(roleId: number) {
    const role = await this.ApotekRoleUserRepository.findOne({
      where: {
        id: roleId
      },
      relations: [
        'apotekDetail',
        'apotekDetail.apotekGroup',
        'apotekDetail.apotekGroup.headOffice',
      ],
      withDeleted: true
    })
    if (!role) throw new BadRequestException("User role not found")
    return role
  }

  /**
   * Function to check user can do action for patient or not
   * @param user 
   * @param patientId 
   */
  async canUserDoAction(user: UserTokenData, patientId: string) {
    const apotek = await this.getCreatorPatient(patientId)
    const roleUser = await this.getDetailRoleUser(+user.payload.roleId)
    if (apotek.id !== roleUser.apotekDetail.id) {
      throw new BadRequestException("User cannot do action to this patient")
    }
  }

  labellingPatientPaginations(paginatePatient: Pagination<SwabPatient>, roleApotek: ApotekRoleUser) {
    paginatePatient.results = paginatePatient.results.map((patient) => this.labelPatientEditable(patient, roleApotek))
    return paginatePatient
  }

  labelPatientEditable(patient: SwabPatient, roleApotek: ApotekRoleUser) {
    return {
      ...patient,
      isEditable: patient?.createByApotek?.id === roleApotek?.apotekDetail?.id
    }
  }
}