import { ConfigAppModule } from "@app/config-app";
import { DatabaseModule } from "@app/database";
import { WhatsappServiceModule } from "@app/email-service/whatsapp-service/whatsapp-service.module";
import { LoggerActivityModule } from "@app/logger-activity";
import { ReportPdfModule } from "@app/report-pdf";
import { Module } from "@nestjs/common";
import { TransactionModule } from "../transaction/transaction.module";
import { VaksinActionService } from "./vaksin-action.service";
import { VaksinController } from "./vaksin.controller";
import { VaksinService } from "./vaksin.service";

@Module({
  imports: [
    DatabaseModule,
    TransactionModule,
    WhatsappServiceModule,
    ConfigAppModule,
    LoggerActivityModule.register({
      area: 'ref'
    }),
    ReportPdfModule,
  ],
  controllers: [VaksinController],
  providers: [VaksinActionService, VaksinService],
  exports: [VaksinService],
})
export class VaksinModule { }