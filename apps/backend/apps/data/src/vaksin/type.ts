import { CommonFilterDTO } from "@app/helper/query-filter/type";
import { ApiProperty } from "@nestjs/swagger";

export enum FilterStatusEnum {
  ReadyCheckin = "ReadyCheckin",
  ReadyVaksin = "ReadyVaksin",
  AlreadyVaksin = "AlreadyVaksin"
}

export class FilterListVaksin extends CommonFilterDTO {
  @ApiProperty({
    enum: FilterStatusEnum,
    required: false,
    name: 'filter[status]'
  })
  status?: FilterStatusEnum

  @ApiProperty({
    name: 'filter[refId]',
    required: false
  })
  refId?: string[]
}