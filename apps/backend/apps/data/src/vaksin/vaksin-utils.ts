export function ActionVaksinBatch(listIdVaksin: string[], action: (id: string) => any) {
  if (!Array.isArray(listIdVaksin)) {
    return action(listIdVaksin)
  }
  let actions = listIdVaksin.map((idVaksin) => {
    return action(idVaksin)
  })
  return Promise.all(actions)
}