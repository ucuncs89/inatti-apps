import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsAll, AppsEnum } from "@app/database/entity/apps/apps.type";
import { User } from "@app/database/entity/user/user.entity";
import { PaginationOptionsInterface } from "@inatti/shared/dtos";
import { PaginationPipe } from "@app/helper/paginate/pagination.service";
import { FilterQueryPipe } from "@app/helper/query-filter/query-filter.service";
import { BadRequestException, Body, Controller, Get, Logger, Param, Post, Put, Query, Request, Response } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { get } from 'lodash'
import { VaksinCreateOrderByUserDTO } from "@inatti/shared/dtos";
import { VaksinActionService } from "./vaksin-action.service";
import { VaksinService } from "./vaksin.service";
import { ActionVaksinBatch } from "./vaksin-utils";
import { FilterListVaksin } from "./type";
import * as express from "express";
import { ResponseFile } from "@app/helper/response-file/response-file";
import { ZipFilesHelper } from "@app/helper/zip-files/ZipFilesHelper";

@ApiTags('Data Vaksin')
@Controller('vaksin')
export class VaksinController {
  constructor(
    private vaksinService: VaksinService,
    private vaksinActionService: VaksinActionService
  ) { }

  logger = new Logger('VaksinController')

  @RequireAuthApp(AppsEnum.User, AppsEnum.Ref)
  @Post('order-vaksin')
  async orderVaksin(
    @Body() params: VaksinCreateOrderByUserDTO,
    @UserCred() user?: User
  ) {
    let res = await this.vaksinService.orderVaksin(
      params,
      user
    )

    await this.vaksinActionService.createLogActivity(
      res.listVaksin.map((v) => v.id),
      'orderVaksin',
      user
    )

    return res;
  }

  @Post('order-vaksin/guest')
  async orderVaksinGuest(
    @Body() params: VaksinCreateOrderByUserDTO,
  ) {
    try {
      return await this.orderVaksin(params)
    } catch (err) {
      console.log(err)
    }
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Get('list')
  async getAll(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe)
    filter: FilterListVaksin
  ) {
    return await this.vaksinService.listVaksin(paginate, filter)
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Get('list-by-ref/:refId')
  async getBasedRef(
    @Param('refId')
    refId: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe)
    filter: FilterListVaksin
  ) {
    return await this.vaksinService.findVaksinByRef(refId, paginate, filter)
  }

  @RequireAuthApp(AppsEnum.User)
  @Get('list-by-user/relations')
  async getListVaksinByUser(
    @UserCred() user: UserTokenData,
  ) {
    return await this.vaksinService.findVaksinRelationsUser(user)
  }

  @RequireAuthApp(...AppsAll)
  @Get('detail/:id')
  async getDetail(
    @Param('id') idVaksin: string,
    @UserCred() user: UserTokenData
  ) {
    let res = await this.vaksinService.detailVaksin(idVaksin)
    if (user?.payload?.apps === AppsEnum.User && get(res, 'patient.ownedBy.id') !== user.id) {
      throw new BadRequestException('Vaksin not found or you have no access')
    }
    return res
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Put('action/checkin')
  async actionCheckin(
    @Query('id') idVaksinList: string[],
    @UserCred() user: UserTokenData
  ) {
    return await ActionVaksinBatch(idVaksinList, async (idVaksin) => {
      let res = await this.vaksinActionService.checkin(idVaksin)
      await this.vaksinActionService.createLogActivity(
        idVaksin,
        'checkin',
        user
      )
      return res
    })
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Put('action/vaksin')
  async actionVaksin(
    @Query('id') idVaksinList: string[],
    @UserCred() user: UserTokenData
  ) {
    return await ActionVaksinBatch(idVaksinList, async (idVaksin) => {
      let res = await this.vaksinActionService.vaksin(idVaksin)
      await this.vaksinActionService.createLogActivity(
        idVaksin,
        'vaksinned',
        user
      )
      return res;
    })
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Put('action/notify-whatsapp')
  async actionNotify(
    @Query('id') idVaksinList: string[],
    @UserCred() user: UserTokenData
  ) {
    return await ActionVaksinBatch(idVaksinList, async (idVaksin) => {
      let res = await this.vaksinActionService.sendNotification(idVaksin)
      await this.vaksinActionService.createLogActivity(
        idVaksin,
        'notify',
        user
      )
      return res;
    })
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Put('action/check-notify-whatsapp')
  async checkNotifyWhatsapp(
    @Query('id') idVaksinList: string[],
    @UserCred() user: UserTokenData
  ) {
    return await ActionVaksinBatch(idVaksinList, async (idVaksin) => {
      let res = await this.vaksinActionService.udpateStatusNotify(idVaksin)
      await this.vaksinActionService.createLogActivity(
        idVaksin,
        'updateNotify',
        user
      )
      return res;
    })
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Get('get-excel/:idRef')
  async getExcel(
    @Param('idRef') idRef: string,
    @Query('filter', new FilterQueryPipe)
    filter: FilterListVaksin,
    @Response()
    res: express.Response
  ) {
    return this.vaksinService.getExcelByReference(idRef, filter, res)
  }

  @RequireAuthApp(AppsEnum.Admin)
  @Get('get-excel-by-admin')
  async getExcelByAdmin(
    @Query('filter', new FilterQueryPipe)
    filter: FilterListVaksin,
    @Response()
    res: express.Response
  ) {
    return this.vaksinService.getExcel(filter, res)
  }

  @RequireAuthApp(...AppsAll)
  @Get('get-pdf/:idvaksin')
  async getPdf(
    @Param('idvaksin') idVaksin: string,
    @Response() res: any
  ) {
    const bufferFile = await this.vaksinService.getPdf(idVaksin)
    ResponseFile(res, {
      type: 'application/pdf',
      buffer: bufferFile,
      nameFile: `inatti_vaksin_${idVaksin}.pdf`
    })
  }

  @RequireAuthApp(AppsEnum.Ref)
  @Get('get-pdf-bulk')
  async getPdfBulk(
    @Query('idvaksin') idVaksins: string[],
    @Response() res: any,
    @Request() req: any
  ) {
    req.setTimeout(360000) // 5 minutes
    const MAX_DATA = 20

    if (idVaksins.length > MAX_DATA) {
      throw new BadRequestException(`Maksimal ${MAX_DATA} data untuk di generate`)
    }

    const archive = ZipFilesHelper(res, {
      name: `vaksin-pdf-${new Date().getTime()}`,
      logger: new Logger('VaksinControllerPdfBulk')
    })

    await ActionVaksinBatch(idVaksins, (idVaksin) => {
      return new Promise(async (resolve, reject) => {
        const bufferFile = await this.vaksinService.getPdf(idVaksin)
        const nameFile = `inatti_vaksin_${idVaksin}.pdf`
        archive.append(bufferFile, nameFile)
        resolve(true)
      })
    })

    archive.finish()
  }
}
