import { ProductTypeId } from "@app/database/entity/ref/const";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { PatientRelationsAll, SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { User } from "@app/database/entity/user/user.entity";
import { Vaksin, VaksinRelationsAll } from "@app/database/entity/vaksin/vaksin.entity";
import { MethodPaymentEnum, Pagination, PaginationOptionsInterface } from "@inatti/shared/dtos";
import { CommonFilterDTO } from "@app/helper/query-filter/type";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { extend, get } from 'lodash'
import { In, IsNull, Not, Repository } from "typeorm";
import { PatientDTO } from "@inatti/shared/dtos";
import { TransactionOperatorService } from "../transaction/transaction.service";
import { VaksinCreateOrderByUserDTO } from "@inatti/shared/dtos";
import { FilterListVaksin, FilterStatusEnum } from "./type";
import { Response } from "express";
import { FormatDeepKey } from "@app/helper/utils/object";
import { deepValueToXlsx } from "@app/helper/report-xlsx/to-xlsx";
import { InjectPropertyOrder } from "apps/ref/src/transaction/transaction.service";
import { ReportPdfService } from "@app/report-pdf";
import { ShouldArray } from "@app/helper/utils/array";
import archiver = require("archiver");

@Injectable()
export class VaksinService {
  constructor(
    @InjectRepository(Vaksin)
    private VaksinRepository: Repository<Vaksin>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    private transactionService: TransactionOperatorService,
    private reportPdf: ReportPdfService
  ) { }

  private createTrxInstance(params: VaksinCreateOrderByUserDTO, user: User) {
    return this.transactionService.createTransactionInstance(params, user, params.patients.length)
  }

  private async createPatient(patient: PatientDTO) {
    let newPatient = new SwabPatient()
    Object.assign(newPatient, patient)
    return await this.SwabPatientRepository.save(newPatient)
  }

  async createVaksin(payload: {
    patient: SwabPatient,
    order: SwabOrder,
    trx: SwabOrderTrx,
    dto: VaksinCreateOrderByUserDTO
  }) {
    const newVaksin = new Vaksin();
    newVaksin.patient = payload.patient
    newVaksin.order = payload.order
    newVaksin.orderTrx = payload.trx
    newVaksin.ref = new RefDetail(payload.dto.refTarget.refId)
    newVaksin.requestDate = payload.dto.info.date

    return await this.VaksinRepository.save(newVaksin)
  }

  async orderVaksin(params: VaksinCreateOrderByUserDTO, user: User) {
    let trxInstance = this.createTrxInstance(params, user)
    let trxInserted = await trxInstance.createOrderTrx()

    let listVaksin: Vaksin[] = []
    for (let i = 0; i < params.patients.length; i++) {
      const patient = params.patients[i];
      let orderInserted = await trxInstance.createOrder()
      let patientInserted = await this.createPatient(patient)
      let vaksin = await this.createVaksin({
        patient: patientInserted,
        order: orderInserted,
        trx: trxInserted,
        dto: params,
      })
      listVaksin.push(vaksin)
    }

    let trxTransfer;
    if (params.method === MethodPaymentEnum.transfer) {
      trxTransfer = await this.transactionService.createTransactionTransfer(trxInstance)
    }

    return {
      trx: trxInserted,
      listVaksin: listVaksin.map((v) => ({ id: v.id })),
      trxTransfer,
    };
  }

  async findVaksinByRef(refId: string, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    const where: Partial<Vaksin> = {
      ref: new RefDetail(refId),
    }
    return await this.listVaksin(paginate, filter, where)
  }

  private filterQuery(filter: FilterListVaksin, expandWhere?: any) {
    const where: Partial<Record<keyof Vaksin, any>> = {}

    if (filter) {
      if (filter.date) {
        // assume it's a 'today' param
        if (filter.date && filter.date.value) {
          where.createdAt = filter.date
        }
      }

      if (filter.rangedate) {
        where.createdAt = filter.rangedate
      }
      if (filter.status) {
        /**
         * FilterListVaksin.ReadyCheckin will show all
         */
        if (filter.status === FilterStatusEnum.ReadyVaksin) {
          where.checkinDateAt = Not(IsNull())
        }
        if (filter.status === FilterStatusEnum.AlreadyVaksin) {
          where.vaksinDateAt = Not(IsNull())
        }
      }
      if (filter.refId) {
        where.ref = In(ShouldArray(filter.refId))
      }
    }

    // override filter where
    if (expandWhere) {
      Object.assign(where, expandWhere)
    }

    return where
  }

  async listVaksin(paginate: PaginationOptionsInterface, filter: FilterListVaksin, expandWhere?: any) {
    const where = this.filterQuery(filter, expandWhere)

    const results = await this.VaksinRepository.find({
      where,
      relations: VaksinRelationsAll(),
      order: {
        createdAt: 'DESC'
      },
      skip: paginate.skip,
      take: paginate.take
    })

    const total = await this.VaksinRepository.count({
      where,
    })

    return new Pagination({
      results,
      total,
    }, paginate)
  }

  async findVaksinRelationsUser(user: User) {
    let relations = VaksinRelationsAll()
    relations = relations.filter((v) => !['patient.ownedBy'].includes(v))

    const results = await this.VaksinRepository.find({
      where: {
        patient: {
          ownedBy: user
        }
      },
      order: {
        createdAt: 'DESC'
      },
      relations,
    })

    return results
  }

  async detailVaksin(id: string) {
    let res = await this.VaksinRepository.findOne({
      where: {
        id
      },
      relations: VaksinRelationsAll(),
      withDeleted: true
    })
    if (!res) {
      throw new BadRequestException("Vaksin not found")
    }
    if (get(res, 'patient.ownedBy')) {
      res.patient.ownedBy.hideCredential()
    }
    return res
  }

  isProductVaksin(trxData: SwabOrderTrx | SwabOrder) {
    return +get(trxData, 'product.product.id') === ProductTypeId.Vaksin
  }

  /**
   * @deprecated This will changed by transactionService at apps/backend/apps/data/src/transaction/transaction-order.service.ts
   * @param orderData 
   * @returns 
   */
  async injectVaksinToOrder(orderData: SwabOrder): Promise<InjectPropertyOrder> {
    let relations = [
      'patient',
      ...PatientRelationsAll('patient')
    ]

    const results = await this.VaksinRepository.findOne({
      where: {
        order: orderData
      },
      relations,
    })
    if (!results) return null

    const { patient, ...detailOrderProduct } = results

    let injectData: InjectPropertyOrder = {
      patient,
      detailOrderProduct,
    }
    return injectData

  }

  async getExcelByReference(refId: string, filter: FilterListVaksin, res: Response) {
    return await this.getExcel(filter, res, {
      ref: new RefDetail(refId),
    })
  }

  async getExcel(filter: FilterListVaksin, res?: Response, extendWhere?: any) {
    const where = this.filterQuery(filter, extendWhere)

    const results = await this.VaksinRepository.find({
      where,
      relations: VaksinRelationsAll(),
      order: {
        createdAt: 'DESC'
      },
    })

    const formatValueXlsx: FormatDeepKey = {
      "id": "id",
      "Pasien Id": "patient.id",
      "Pasien Nama": "patient.name",
      "Pasien Tipe Indentitas": "patient.type",
      "Pasien NIK": "patient.nik",
      "Pasien Passport": "patient.passport",
      "Pasien Tempat Lahir": "patient.placebirth",
      "Pasien Tanggal lahir": "patient.datebirth",
      "Pasien Jenis kelamin": "patient.gender",
      "Pasien Nomor hp": "patient.phonenumber",
      "Rujukan Id": "ref.id",
      "Rujukan Nama": "ref.name",
      "Tanggal Request": "requestDate",
      "Tanggal Checkin": "checkinDateAt",
      "Tanggal Vaksin": "vaksinDateAt",
      "Tanggal dibuat": "createdAt",
      "Order trx Id": "orderTrx.id",
      "Order trx invoice id": "orderTrx.invId",
      "Order trx Metode": "orderTrx.method",
      "Biaya perindividu": "order.price",
      "Biaya total (per trx id)": "orderTrx.totalPrice",
      "Fee admin (per trx id)": "orderTrx.feeAdmin"
    }

    return deepValueToXlsx(res, results, formatValueXlsx)
  }

  async getPdf(vaksinId: string) {
    const data = await this.detailVaksin(vaksinId)
    if (!data) throw new BadRequestException("Vaksin not found")

    const host = this.reportPdf.hostPdftemplateWeb
    const url = `${host}/vaksin?vaksinId=${vaksinId}`

    return await this.reportPdf.generatePdfFromUrl(url)
  }
}