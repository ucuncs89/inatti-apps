import { createTagLog } from "@app/logger-activity/utils-logger-activity"

export const TagLogVaksin = (id: string) => {
  return createTagLog('vaksin', id)
}