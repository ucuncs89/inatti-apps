import { ConfigAppService } from "@app/config-app";
import { User } from "@app/database/entity/user/user.entity";
import { Vaksin } from "@app/database/entity/vaksin/vaksin.entity";
import { WhatsappService } from "@app/email-service/whatsapp-service/whatsapp/whatsapp.service";
import { ResponseWhatsappInterface, WHATSAPP_SERVICE } from '@app/email-service/whatsapp-service/whatsapp/type'
import { LoggerActivityService } from "@app/logger-activity";
import { LoggerActivityList } from "@app/logger-activity/logger-type";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import projectConfig from "config/project.config";
import { Repository } from "typeorm";
import { TagLogVaksin } from "./logger-vaksin";
import { VaksinService } from "./vaksin.service";
import { get } from 'lodash'
import { StatusNotifWA } from "@app/database/entity/swab-notification/swab-notify-wa.entity";
import { VaksinNotificationWhatsapp } from "@app/database/entity/vaksin/vaksin-notification.entity";

@Injectable()
export class VaksinActionService {
  constructor(
    @InjectRepository(Vaksin)
    private VaksinRepository: Repository<Vaksin>,
    @InjectRepository(VaksinNotificationWhatsapp)
    private VaksinNotificationWhatsappRepository: Repository<VaksinNotificationWhatsapp>,
    private vaksinService: VaksinService,
    private loggerActivity: LoggerActivityService,
    private whatsappService: WhatsappService,
    private configApp: ConfigAppService,
  ) { }

  async checkin(id?: string) {
    let vaksinData = await this.VaksinRepository.findOne({ id })
    if (!vaksinData) throw new Error('Vaksin not found')
    vaksinData.checkinDateAt = new Date()
    return await this.VaksinRepository.save(vaksinData)
  }

  async vaksin(id?: string) {
    let vaksinData = await this.VaksinRepository.findOne({ id })
    if (!vaksinData) throw new Error('Vaksin not found')
    vaksinData.vaksinDateAt = new Date()
    return await this.VaksinRepository.save(vaksinData)
  }

  async createLogActivity(id: string | string[], type: string, user: User) {
    if (Array.isArray(id)) {
      let res = []
      for (let i = 0; i < id.length; i++) {
        const idI = id[i];
        res.push(await this.createLogActivity(idI, type, user))
      }
      return res;
    }
    return await this.loggerActivity.log({
      user: user,
      type: LoggerActivityList.refVaksin[type],
      tag: TagLogVaksin(id)
    })
  }

  private async getMessage() {
    const name = 'TEMPLATE_NOTIFY_VAKSIN'
    const formatDefault = 'Inatti Notifikasi. url {urlVaksin}'
    const category = 'template-notify'

    let message = await this.configApp.getConfigOrInit(name, {
      value: formatDefault,
      category
    })
    return message
  }

  private async getPhonePatient(idVaksin: string) {
    const detail = await this.vaksinService.detailVaksin(idVaksin)
    if (!detail) return null

    return [detail, get(detail, 'patient.phonenumber')]
  }

  async sendNotification(id: string) {
    const [vaksinInfo, phone] = await this.getPhonePatient(id)
    if (!phone) throw new BadRequestException(`Patient ${get(vaksinInfo, 'patient.name')} (Vaksin id : ${vaksinInfo.id}) have no phonenumber`)
    const vaksinDetail = await this.vaksinService.detailVaksin(id)

    const orderId = vaksinDetail?.orderTrx?.id
    if (!orderId) throw new BadRequestException('No order transaction found')

    const tempalteUrl = `${projectConfig().userUrl}/testcovid/order/detail/${orderId}`

    let message = await this.getMessage()
    message = message.replace('{urlVaksin}', tempalteUrl)

    try {
      // for now, only taptalk ready have return id
      const res = await this.whatsappService.serviceTaptalk({
        message,
        phone
      })
      return await this.setStatusNotify(id, res)
    } catch (err) {
      return await this.setStatusNotify(id, {
        status: StatusNotifWA.FAILED
      })
    }
  }

  async udpateStatusNotify(idVaksin: string) {
    const notif = await this.VaksinNotificationWhatsappRepository.findOne({
      vaksin: new Vaksin(idVaksin)
    })
    if (!notif?.messageId) {
      return null;
    }
    const res = await this.whatsappService.serviceTaptalkCheckStatus(notif.messageId)
    return this.setStatusNotify(idVaksin, res)
  }

  async setStatusNotify(idVaksin: string, res: ResponseWhatsappInterface) {
    let notif = await this.VaksinNotificationWhatsappRepository.findOne({
      vaksin: new Vaksin(idVaksin)
    })
    if (!notif) {
      notif = new VaksinNotificationWhatsapp()
    }
    notif.vaksin = new Vaksin(idVaksin)
    notif.message = res.responseMessage
    notif.messageId = res.messageId
    notif.status = res.status

    return this.VaksinNotificationWhatsappRepository.save(notif)
  }
}