import { RefProduct } from "@app/database/entity/ref/ref-product.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { User } from "@app/database/entity/user/user.entity";
import { Logger } from "@nestjs/common";
import { getFeeFaspay, PriceProduct } from "apps/user/src/testcovid/testcovid-register";
import { Repository } from "typeorm";
import { TransactionCreateDTO } from "@inatti/shared/dtos";
import * as dayjs from 'dayjs'
import { MethodPaymentEnum, StatusOrderEnum } from "@app/database/entity/swab-data/type";
import { genInvoiceId, genTransactionId } from "@app/helper/utils/invoice";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { DateAsiaFormat } from "@app/helper/utils/time";
import { WhatsappService } from "@app/email-service/whatsapp-service/whatsapp/whatsapp.service";
import { ConfigAppService } from "apps/admin/src/config-app/config-app.service";
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { CONFIGAPP_NAME } from "@app/config-app";
import { ConfigApp } from "@app/database/entity/config/config-app.entity";
import { TemplateText } from "@app/helper/utils/string";

export class TransactionOperator {
  constructor(
    private SwabOrderRepository: Repository<SwabOrder>,
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    private RefProductRepository: Repository<RefProduct>,
    private whatsappService: WhatsappService,
    private configAppService: ConfigAppService,
    options: {
      payload: TransactionCreateDTO,
      totalPatient: number,
      customer: User
    }
  ) {
    this.payload = options.payload
    this.totalPatient = options.totalPatient
    this.customer = options.customer
  }
  protected logger = new Logger('TestCovidRegisterClass')
  protected totalPatient: number
  refProduct: RefProduct;
  priceProduct: PriceProduct
  payload: TransactionCreateDTO
  customer: User

  private getExpireDate() {
    let now = dayjs()
    return now.add(3, 'hour')
  }

  async getProduct() {
    if (!this.refProduct) {
      this.refProduct = await this.RefProductRepository.findOne({
        where: {
          id: +this.payload.refTarget.productId
        },
        relations: [
          'product',
          'ref'
        ]
      })
    }
    return this.refProduct;
  }

  private async getPrice(): Promise<PriceProduct> {
    if (this.priceProduct) {
      return this.priceProduct
    }
    let refProduct = await this.getProduct();
    const productPrice = refProduct.price * this.totalPatient
    let total = productPrice
    this.priceProduct = {
      item: refProduct.price,
      total,
    }
    return this.priceProduct
  }

  orderTrx: SwabOrderTrx
  async createOrderTrx() {
    if (this.orderTrx) {
      return this.orderTrx
    }
    const trxId = genTransactionId();
    const invId = genInvoiceId(trxId, this.payload.refTarget.productId);
    const price = await this.getPrice()

    let orderTrx = new SwabOrderTrx();
    orderTrx.invId = invId;
    orderTrx.trxId = trxId;
    orderTrx.customer = this.customer
    orderTrx.reference = new RefDetail(this.payload.refTarget.refId)
    orderTrx.product = new RefProduct(+this.payload.refTarget.productId)
    orderTrx.expiredAt = new Date(DateAsiaFormat(this.getExpireDate()))

    orderTrx.method = this.payload.method
    orderTrx.status = StatusOrderEnum.new // if cash
    if (this.payload.method === MethodPaymentEnum.transfer) {
      orderTrx.status = StatusOrderEnum.new // if transfer
    }

    orderTrx.totalPrice = price.total
    orderTrx.feeAdmin = price.fee

    this.orderTrx = await this.SwabOrderTrxRepository.save(orderTrx)
    return this.orderTrx
  }

  orderList: SwabOrder[] = []
  async createOrder() {
    const price = await this.getPrice()
    let order = new SwabOrder();
    order.price = price.item;
    order.customer = this.orderTrx.customer;
    order.method = this.orderTrx.method;
    order.orderTrx = this.orderTrx;
    order.product = new RefProduct(+this.payload.refTarget.productId)
    order.reference = new RefDetail(this.payload.refTarget.refId)
    order.status = StatusOrderEnum.new
    let result = await this.SwabOrderRepository.save(order)
    this.orderList.push(result)
    return result
  }

  /** Notification section */
  private notifyService: string
  private notifyTemplate: string
  private urlUser = process.env.URL_FRONTEND_USER || 'https://dev-user.inatti.id'
  private async notifyInit(payload: NotifyTemplateInterface) {
    if (!this.notifyService) {
      this.notifyService = await this.whatsappService.getService()
    }
    if (!this.notifyTemplate) {
      const config = await this.configAppService.getConfig({
        name: CONFIGAPP_NAME.TEMPLATE_NOTIFY_SUCCESS_REGISTER_TEST_WA
      }) as ConfigApp
      this.notifyTemplate = config.value
    }
    payload.nameFaskes = (await this.getProduct()).ref.name
  }
  private notifyUrlTrx(trxId: string) {
    const url = new URL(this.urlUser)
    url.pathname = `/testcovid/order/detail/${trxId}`
    return url
  }
  async notifyToPatient(payload: NotifyTemplateInterface) {
    if (!payload.phone) return;
    await this.notifyInit(payload)
    payload.urlTrx = this.notifyUrlTrx(payload.trxId).toString()
    const message = TemplateText(this.notifyTemplate, payload)
    return await this.whatsappService.sendWhatsappChoose(this.notifyService, {
      phone: payload.phone,
      message,
    })
  }
}

export interface NotifyTemplateInterface {
  phone: string
  name: string,
  nik: string,
  dateTest: string,
  trxId: string,
  nameFaskes?: string,
  urlTrx?: string,
}