import { RequireAuthApp } from "@app/authentication/authorization/jwt.authguard.multiapp";
import { UserCred } from "@app/authentication/helper/user.decorator";
import { UserTokenData } from "@app/authentication/types";
import { AppsAll } from "@app/database/entity/apps/apps.type";
import { TransactionCreateOrderByUserDTO } from "@inatti/shared/dtos";
import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { TransactionOrderService } from "./transaction-order.service";
import { TransactionOperatorService } from "./transaction.service";

@ApiTags('Transaction V2')
@Controller('transaction-v2')
export class TransactionOrderController {
  constructor(
    private transactionOrderService: TransactionOrderService,
    private transactionOperatorService: TransactionOperatorService
  ) { }

  /** 
   * This api are not required auth
   */
  @Get('detail/order-trx/:idOrderTrx')
  getDetailOrderTrx(
    @Param('idOrderTrx') idOrderTrx: string
  ) {
    return this.transactionOrderService.getDetail(idOrderTrx)
  }

  @RequireAuthApp()
  @Post('create/order/loggedin')
  createOrderLoggedin(
    @Body() body: TransactionCreateOrderByUserDTO,
    @UserCred() user: UserTokenData
  ) {
    return this.transactionOperatorService.createOrder(body, user)
  }

  @Post('create/order/guest')
  createOrderGuest(
    @Body() body: TransactionCreateOrderByUserDTO
  ) {
    return this.transactionOperatorService.createOrder(body)
  }
}