import { RefProduct } from "@app/database/entity/ref/ref-product.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { User } from "@app/database/entity/user/user.entity";
import { WhatsappService } from "@app/email-service/whatsapp-service/whatsapp/whatsapp.service";
import { LogTask } from "@app/helper/logger/logger-helper";
import { DateOnlyFormat } from "@app/helper/utils/time";
import { MethodPaymentEnum, PatientDTO, TransactionCreateDTO, TransactionCreateOrderByUserDTO } from "@inatti/shared/dtos";
import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ConfigAppService } from "apps/admin/src/config-app/config-app.service";
import { pick } from "lodash";
import { Repository } from "typeorm";
import { RefIntegrateService } from "../payment-v2/ref-integrate/ref-integrate.service";
import { InvoiceResult } from "../payment-v2/ref-integrate/ref-integrate.type";
import { TransactionFaspay } from "./transaction-faspay.service";
import { TransactionOperator } from "./transaction-operator";
import { TransactionTestcovid } from "./type/trx-testcovid.service";
import { TransactionTypeI } from "./type/trx-type.interface";
import { TransactionVaksin } from "./type/trx-vaksin.service";
import { PatientService } from "../patient/patient.service";

@Injectable()
export class TransactionOperatorService {
  constructor(
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    @InjectRepository(RefProduct)
    private RefProductRepository: Repository<RefProduct>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    private trxTypeVaksin: TransactionVaksin,
    private trxTypeTestcovid: TransactionTestcovid,
    private whatsappService: WhatsappService,
    private configAppService: ConfigAppService,
    private refIntegrateService: RefIntegrateService,
    private patientService: PatientService
  ) { }

  log = new Logger('TransactionOperatorService')
  logTask = new LogTask(this.log)

  createTransactionInstance(payload: TransactionCreateDTO, user: User, totalPatient: number) {
    let trxOp = new TransactionOperator(
      this.SwabOrderRepository,
      this.SwabOrderTrxRepository,
      this.RefProductRepository,
      this.whatsappService,
      this.configAppService,
      {
        payload,
        customer: user,
        totalPatient
      }
    )

    return trxOp
  }

  async createTransactionTransfer(trxOp: TransactionOperator) {
    const refId = trxOp.payload.refTarget.refId
    const res = await this.refIntegrateService.createInvoice(refId, {
      trxId: trxOp.orderTrx.id,
      amount: trxOp.orderTrx.totalPrice
    })

    return pick(res, [
      'id',
      'trxId',
      'urlRedirect'
    ])
  }

  async getListTrxMethod(trx: TransactionOperator): Promise<TransactionTypeI> {
    const product = await trx.getProduct()
    const VAKSIN_ID = 3
    if (product.product.id === VAKSIN_ID) {
      return this.trxTypeVaksin
    } else {
      return this.trxTypeTestcovid
    }
  }

  async createOrder(payload: TransactionCreateOrderByUserDTO, user?: User) {
    const trx = this.createTransactionInstance(payload, user, payload.patients.length)
    const trxMethod = await this.logTask.runTask(this.getListTrxMethod(trx), `Get list trx method`)
    const productList = trxMethod.productList()
    const trxInserted = await this.logTask.runTask(trx.createOrderTrx(), `Create order transaction`)

    for (let i = 0; i < payload.patients.length; i++) {
      const logMessage = (msg: string) => `Patient ${i} : ` + msg
      const patient = payload.patients[i];
      const orderInserted = await this.logTask.runTask(trx.createOrder(), logMessage('Create order'))
      const patientInserted = await this.logTask.runTask(this.createPatient(patient), logMessage('Create data patient'))
      await this.logTask.runTask(productList.push({
        dto: payload,
        order: orderInserted,
        trx: trxInserted,
        patient: patientInserted,
        user: user,
      }), logMessage(`Assign product to patient`))
      await this.logTask.runTask(trx.notifyToPatient({
        dateTest: DateOnlyFormat(payload.info.date),
        name: patient.name,
        nik: patient.nik,
        phone: patient.phonenumber,
        trxId: trxInserted.id
      }), logMessage(`Notify whatsapp to Patient`))
    }

    let trxTransfer
    if (payload.method === MethodPaymentEnum.transfer) {
      trxTransfer = await this.logTask.runTask(
        this.createTransactionTransfer(trx),
        'Create transaction transfer'
      )
    }

    return {
      trx: trxInserted,
      trxTransfer,
      ...productList.list
    }
  }

  private async createPatient(patient: PatientDTO) {
    return this.patientService.create(patient)
  }
}
