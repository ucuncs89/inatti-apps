import { WhatsappServiceModule } from "@app/email-service/whatsapp-service/whatsapp-service.module";
import { Module } from "@nestjs/common";
import { ConfigAppModule } from "apps/admin/src/config-app/config-app.module";
import { PatientModule } from "../patient/patient.module";
import { PaymentV2Module } from "../payment-v2/payment-v2.module";
import { TransactionFaspay } from "./transaction-faspay.service";
import { TransactionOrderController } from "./transaction-order.controller";
import { TransactionOrderService } from "./transaction-order.service";
import { TransactionOperatorService } from "./transaction.service";
import { TransactionTestcovid } from "./type/trx-testcovid.service";
import { TransactionVaksin } from "./type/trx-vaksin.service";

@Module({
  imports: [
    PaymentV2Module,
    WhatsappServiceModule,
    ConfigAppModule,
    PaymentV2Module,
    PatientModule
  ],
  controllers: [
    TransactionOrderController,
  ],
  providers: [
    TransactionOperatorService,
    TransactionFaspay,
    TransactionOrderService,
    TransactionVaksin,
    TransactionTestcovid,
  ],
  exports: [
    TransactionOperatorService
  ]
})
export class TransactionModule { }