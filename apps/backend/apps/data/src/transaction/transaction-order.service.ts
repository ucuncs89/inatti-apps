import { isProductTestCovid, isProductVaksin, ProductTypeId } from "@app/database/entity/ref/const";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { PatientRelationsAll, SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { SwabDetail } from "@app/database/entity/swab-data/swab-detail.entity";
import { Vaksin } from "@app/database/entity/vaksin/vaksin.entity";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { get } from "lodash";
import { Repository } from "typeorm";

@Injectable()
export class TransactionOrderService {
  constructor(
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(Vaksin)
    private VaksinRepository: Repository<Vaksin>,
  ) { }

  async getDetail(orderTrxId: string) {
    const orderTrx = await this.orderTrxDetail(orderTrxId)
    await this.bindDetailProduct(orderTrx)
    return orderTrx
  }

  async bindDetailProduct(orderTrx: SwabOrderTrx) {
    return Promise.all(orderTrx.orderSwab.map((orderData, index) => {
      return new Promise(async (resolve, reject) => {
        try {
          let injectProperty: InjectPropertyOrder
          const productId = orderTrx.product.product.id
          if (!productId) reject(new BadRequestException("No product found"))
          if (!Object.values(ProductTypeId).includes(productId)) {
            console.error(`${productId} not support yet, please provide it`)
            resolve(null)
          }

          const cbResultDetail = (result: any) => {
            if (!result) return;
            const { patient, ...detailOrderProduct } = result
            let injectData: InjectPropertyOrder = {
              patient,
              detailOrderProduct,
            }
            return injectData
          }

          if (isProductTestCovid(productId)) {
            injectProperty = await this.getDetailSwab(orderData, cbResultDetail)
          }
          if (isProductVaksin(productId)) {
            injectProperty = await this.getDetailVaksin(orderData, cbResultDetail)
          }
          if (get(injectProperty, 'patient.ownedBy')) {
            injectProperty.patient.ownedBy.hideCredential()
          }
          resolve(Object.assign(orderData, injectProperty))
        } catch (err) {
          reject(err)
        }
      })
    }))
  }

  async orderTrxDetail(orderTrxId: string) {
    let result = await this.SwabOrderTrxRepository.findOne({
      relations: [
        'customer',
        'reference',
        'product',
        'orderSwab',
        'methodCash'
      ],
      where: {
        id: orderTrxId
      }
    })
    if (result.customer) {
      result.customer.hideCredential();
    }
    return result;
  }

  async getDetailSwab(orderData: SwabOrder, cb?: (res: any) => InjectPropertyOrder) {
    let res = await this.SwabDetailRepository.findOne({
      where: {
        orderSwab: orderData
      },
      relations: [
        'patient',
        ...PatientRelationsAll('patient')
      ]
    })
    return cb(res)
  }

  async getDetailVaksin(orderData: SwabOrder, cb?: (res: any) => InjectPropertyOrder) {
    const res = await this.VaksinRepository.findOne({
      where: {
        order: orderData
      },
      relations: [
        'patient',
        ...PatientRelationsAll('patient')
      ]
    })
    return cb(res)
  }
}

export interface InjectPropertyOrder {
  patient: SwabPatient,
  detailOrderProduct: {
    id: string | number
    [key: string]: any
  }
}