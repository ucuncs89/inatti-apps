import { Product } from "@app/database/entity/ref/product.entity";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { SwabDetail } from "@app/database/entity/swab-data/swab-detail.entity";
import { StatusSwabEnum } from "@inatti/shared/dtos";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TransactionTypeI, TransactionTypePayload } from "./trx-type.interface";

export class TransactionTestcovid implements TransactionTypeI {
  constructor(
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
  ) { }

  productList(): { push: (payload: TransactionTypePayload) => any; list: Record<string, any[]>; } {
    const list = []

    return {
      push: async (payload) => {
        const res = await this.assignPatientToProduct(payload)
        list.push(res)
      },
      list: {
        listTestcovid: list
      },
    }
  }

  async assignPatientToProduct(payload: TransactionTypePayload) {
    let swabDetail = new SwabDetail();
    swabDetail.orderTrx = payload.trx;
    swabDetail.patient = payload.patient
    swabDetail.refTarget = new RefDetail(payload.dto.refTarget.refId);
    swabDetail.productSwab = new Product(payload.dto.info.type);
    swabDetail.requestSwabDate = new Date(payload.dto.info.date);
    swabDetail.status = StatusSwabEnum.registered;
    swabDetail.OwnerBy = payload.user
    swabDetail.orderSwab = payload.order
    swabDetail.orderTrx = payload.trx

    return await this.SwabDetailRepository.save(swabDetail)
  }

}