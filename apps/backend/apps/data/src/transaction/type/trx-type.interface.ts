import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity"
import { SwabOrder } from "@app/database/entity/swab-data/order.entity"
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity"
import { User } from "@app/database/entity/user/user.entity"
import { TransactionCreateOrderByUserDTO } from "@inatti/shared/dtos"

export interface TransactionTypePayload {
  patient: SwabPatient,
  order: SwabOrder,
  trx: SwabOrderTrx,
  dto: TransactionCreateOrderByUserDTO
  user?: User
}

export interface TransactionTypeI {
  productList(): {
    push: (payload: TransactionTypePayload) => Promise<any>
    list: Record<string, any[]>
  }
  assignPatientToProduct(payload: TransactionTypePayload): any
}