import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { Vaksin } from "@app/database/entity/vaksin/vaksin.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TransactionTypeI, TransactionTypePayload } from "./trx-type.interface";

@Injectable()
export class TransactionVaksin implements TransactionTypeI {
  constructor(
    @InjectRepository(Vaksin)
    private VaksinRepository: Repository<Vaksin>,
  ) { }

  productList(): { push: (payload: TransactionTypePayload) => any; list: Record<string, any[]>; } {
    const list = []

    return {
      push: async (payload) => {
        const res = await this.assignPatientToProduct(payload)
        list.push(res)
      },
      list: {
        listVaksin: list
      },
    }
  }

  async assignPatientToProduct(payload: TransactionTypePayload) {
    const newVaksin = new Vaksin();
    newVaksin.patient = payload.patient
    newVaksin.order = payload.order
    newVaksin.orderTrx = payload.trx
    newVaksin.ref = new RefDetail(payload.dto.refTarget.refId)
    newVaksin.requestDate = payload.dto.info.date

    return await this.VaksinRepository.save(newVaksin)
  }

}