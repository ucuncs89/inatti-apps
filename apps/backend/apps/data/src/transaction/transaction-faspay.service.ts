import { FaspayMerchant } from "@app/database/entity/faspay/faspay-merchant.entity";
import { FaspayTransaction } from "@app/database/entity/faspay/faspay-transaction.entity";
import { RefProduct } from "@app/database/entity/ref/ref-product.entity";
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { User } from "@app/database/entity/user/user.entity";
import { DateAsiaFormat } from "@app/helper/utils/time";
import { Faspay, FaspayProductItem, FasPayRequestTrxDTO, SuccessCreateTrxDTO } from "@app/payment";
import { TransactionCreateDTO } from "@inatti/shared/dtos";
import { BadRequestException, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { PriceProduct } from "apps/user/src/testcovid/testcovid-register";
import { get } from "lodash";
import { Repository } from "typeorm";

/** @deprecated not used faspay anymore */
@Injectable()
export class TransactionFaspay {
  constructor(
    @InjectRepository(FaspayMerchant)
    private FaspayMerchantRepository: Repository<FaspayMerchant>,
    @InjectRepository(FaspayTransaction)
    private FaspayTransactionRepository: Repository<FaspayTransaction>,
  ) { }

  logger = new Logger('TransactionFaspayV2')

  async getMerchantFaspay(): Promise<FaspayMerchant> {
    let merchant = await this.FaspayMerchantRepository.findOne({
      where: {
        slug: 'default',
      },
    });
    if (!merchant) {
      throw new Error(`Merchant default not found`);
    }
    return merchant;
  }

  async getItemsFaspay(merchantId: string, product: RefProduct): Promise<any[]> {
    let item = new FaspayProductItem();

    item.product = product.name;
    item.amount = product.price.toString();
    item.qty = '1';
    item.merchant_id = merchantId;

    return [item];
  }

  async saveFaspayTransaction(
    trx: { orderTrx: SwabOrderTrx; faspayTrx: string },
    api: { request: FasPayRequestTrxDTO; response: SuccessCreateTrxDTO },
  ) {
    const faspayTrx = new FaspayTransaction();

    faspayTrx.orderTrx = trx.orderTrx;
    faspayTrx.transactionFaspayId = trx.faspayTrx;
    faspayTrx.requestApi = api.request;
    faspayTrx.responseApi = api.response;

    return await this.FaspayTransactionRepository.save(faspayTrx);
  }

  async sendTransaction(
    params: TransactionFaspayDTO,
  ) {
    if (!get(params, 'payload.paymentChannel')) {
      throw new BadRequestException('Payment channel property not found')
    }

    const merchantFaspay = await this.getMerchantFaspay();

    const faspay = new Faspay();
    try {
      const payloadFaspay = {
        bill_date: DateAsiaFormat(params.orderTrx.createdAt),
        bill_desc: `Pembayaran #${params.orderTrx.trxId}`,
        merchant_id: merchantFaspay.merchantId.toString(),
        merchant: merchantFaspay.merchantName.toString(),
        bill_no: params.orderTrx.trxId,
        bill_expired: DateAsiaFormat(params.orderTrx.expiredAt),
        bill_total: params.orderTrx.totalPrice + '',
        payment_channel: params.payload.paymentChannel.id,
        cust_no: get(params, 'customer.id'),
        cust_name: get(params, 'customer.fullname'),
        msisdn: get(params, 'customer.phoneNumber'),
        email: get(params, 'customer.email'),
        item: await this.getItemsFaspay(merchantFaspay.merchantId.toString(), params.product),
      };
      this.logger.verbose(`Payload to faspay : ${payloadFaspay}`);
      const resFaspay = await faspay.createTransaction(payloadFaspay);

      await this.saveFaspayTransaction(
        {
          orderTrx: params.orderTrx,
          faspayTrx: resFaspay.trx_id,
        },
        {
          request: payloadFaspay,
          response: resFaspay,
        },
      );

      return resFaspay;
    } catch (err) {
      throw err;
    }
  }
}

export interface TransactionFaspayDTO {
  customer: User;
  orderTrx: SwabOrderTrx;
  price: PriceProduct,
  payload: TransactionCreateDTO,
  product: RefProduct
}