# New version

## Note

Folder `apps/backend/apps/data` adalah versi 2 dari versi pertama (`apps/backend/apps`). Perbedaannya adalah 

### Versi 1 (apps/backend/apps)

- pembuatan fitur berdasarkan lokasi apps (apotek, ref, lab, dll)
- security api tergantung lokasi apps
- pembagian api berdasarkan prefix `api-lab`, `api-ref`, `api-admin`, `api-user`, dan lain-lain.

**Kekurangan**

- Modul fitur terkadang digunakan pada apps lain, maka sulit untuk di gunakan pada berbagai apps
- harus menulis ulang fitur yang sama atau dimasukkan ke folder `apps/backend/libs`.
- sulit untuk mengatur authentikasi

### Versi 2 (apps/backend/apps/data)

> Disarankan fitur baru dimasukkan ke sini

- Pembuatan API berdasarkan modul fitur (bukan lagi apps)
- Authentikasi apps menggunakan decorator `@RequireAuthApp()`
- Fleksibilitas mengatur authentikasi
- satu fitur dapat memiliki fungsi authentikasi yang berbeda-beda. Misalnya `create` hanya bisa digunakan oleh `apps admin`, `view` bisa semua apps, dan lain-lain.

```
import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';

// Contoh : 
@RequireAuthApp(AppsEnum.Apotek)
```

By Rio Chandra
16-11-2022