import { JwtAuthGuardApotekGroup } from "@app/authentication/authorization/jwt.authguard.apotek-group";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";

export function ApiApotekGroup(options = {enableAuth: true}) {
  let tag = ApiTags('Apotek Group API')
  if(options.enableAuth){
    return applyDecorators(
      UseGuards(JwtAuthGuardApotekGroup),
      ApiBearerAuth(),
      tag
    )
  }
  return applyDecorators(
    tag
  )
}