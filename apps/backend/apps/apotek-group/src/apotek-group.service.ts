import { ApotekGroupRoleUser } from '@app/database/entity/apotek-group/apotek-group-role-user.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ApotekGroupService {
  constructor(
    @InjectRepository(ApotekGroupRoleUser)
    private apotekGrouopRoleUser: Repository<ApotekGroupRoleUser>
  ) { }
  async getRegisteredApotekgroup(user: User, withDeleted: boolean) {
    const res = await this.apotekGrouopRoleUser.find({
      where: {
        userId: user
      },
      withDeleted: withDeleted
    })

    return res.filter((v) => v.apotekGroup)
  }
  getRoleUser(idUser: string, idApotekGroup: string) {
    return this.apotekGrouopRoleUser.findOneOrFail({
      where: {
        userId: idUser,
        apotekGroup: idApotekGroup
      },
      withDeleted: true
    })
  }
}
