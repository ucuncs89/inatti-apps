import { NestFactory } from '@nestjs/core';
import { ApotekGroupModule } from './apotek-group.module';

async function bootstrap() {
  const app = await NestFactory.create(ApotekGroupModule);
  await app.listen(3000);
}
bootstrap();
