import { AccountModule } from '@app/account';
import { AuthenticationModule } from '@app/authentication';
import { AwsModule } from '@app/aws';
import { DatabaseModule } from '@app/database';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { LoggerActivityModule } from '@app/logger-activity';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ApotekGroupController } from './apotek-group.controller';
import { ApotekGroupService } from './apotek-group.service';
import AwsConfig from "../../../config/aws.config";


@Module({
  imports: [
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.ApotekGroup }),
    PassportModule,
    AccountModule,
    LoggerActivityModule.register({
      area: AppsEnum.ApotekGroup
    }),
    AwsModule.register({
      bucket: AwsConfig().aws.bucket,
      uploadFolder: 'apotek',
    }),
  ],
  controllers: [
    ApotekGroupController,
  ],
  providers: [
    ApotekGroupService,
  ],
})
export class ApotekGroupModule {}
