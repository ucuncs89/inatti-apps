import { JwtAuthGuardApotekGroup } from '@app/authentication/authorization/jwt.authguard.apotek-group';
import { LoginDTO } from '@app/authentication/types';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Get, Param, Post, Query, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApotekGroupService } from './apotek-group.service';

@ApiTags('Apotek Group Auth API')
@Controller()
export class ApotekGroupController {
  constructor(private readonly apotekGroupService: ApotekGroupService) { }

  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard(AppsEnum.ApotekGroup))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardApotekGroup)
  @Get('/registered-apotek-group')
  async registeredLab(
    @Request() req,
    @Query('withDeleted') withDeleted: string
  ) {
    // default is true
    const withDeletedBol = (withDeleted || 'true') === 'true'
    let listRef = await this.apotekGroupService.getRegisteredApotekgroup(req.user, withDeletedBol)
    return listRef
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardApotekGroup)
  @Get('/role-user/:apotekGroup')
  async getRoleUser(
    @Param('apotekGroup')
    apotekGroup: string,
    @Request()
    req: any
  ) {
    try {
      return await this.apotekGroupService.getRoleUser(req.user.id, apotekGroup)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
