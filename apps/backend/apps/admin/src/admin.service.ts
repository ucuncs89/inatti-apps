import { AdminRoleUser } from '@app/database/entity/admin/admin-role-user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(AdminRoleUser)
    private AdminRoleUserRepository: Repository<AdminRoleUser>,
  ){}

  getRoleUser(userId: string){
    return this.AdminRoleUserRepository.findOne({
      where: {
        userId: {
          id: userId
        }
      }
    })
  }
}
