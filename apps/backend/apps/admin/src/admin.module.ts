import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { LabModule } from './lab/lab.module';
import { AdminUserModule } from './admin-user/admin-user.module';
import { UserModule } from './user/user.module';
import { RefModule } from './ref/ref.module';
import { DashboardController } from './dashboard/dashboard.controller';
import { DashboardService } from './dashboard/dashboard.service';
import { DatabaseModule } from '@app/database';
import { TransactionController } from './transaction/transaction.controller';
import { TransactionService } from './transaction/transaction.service';
import { ReporterService } from './transaction/reporter.service';
import { ConfigAppModule } from './config-app/config-app.module';

@Module({
  imports: [
    DatabaseModule,
    LabModule,
    UserModule,
    RefModule,
    AdminUserModule,
    ConfigAppModule,
  ],
  controllers: [
    AdminController,
    DashboardController,
    TransactionController,
  ],
  providers: [
    AdminService,
    DashboardService,
    TransactionService,
    ReporterService
  ],
})
export class AdminModule {}
