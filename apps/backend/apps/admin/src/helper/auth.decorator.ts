import { JwtAuthGuardAdmin } from "@app/authentication/authorization/jwt.authguard.admin";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth } from "@nestjs/swagger";

export function RequireAuth() {
  return applyDecorators(
    UseGuards(JwtAuthGuardAdmin),
    ApiBearerAuth()
  )
}