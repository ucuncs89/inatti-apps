import { applyDecorators, Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { RequireAuth } from "./auth.decorator";

export function AdminApi(route?: string){
  if(route && route[0] === '/'){
    route = route.slice(1, route.length)
  }
  if(!route){
    route = '';
  }
  return applyDecorators(
    ApiTags('Api Admin'),
    Controller('/api-admin/' + route),
  )
}