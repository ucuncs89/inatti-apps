import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateLabDto } from './create-lab.dto';

export class UpdateLabDto extends PartialType(CreateLabDto) {}

export class UpdateConfigLab {
  @ApiProperty()
  isDisableNotifyWhatsapp?: boolean
}