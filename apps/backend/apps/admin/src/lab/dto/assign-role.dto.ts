import { ApiProperty } from "@nestjs/swagger";

export class AssignRoleLabDto{
  @ApiProperty()
  userId: string;
  @ApiProperty()
  labId: string;
  @ApiProperty()
  roleId: number;
}

export class AddUserAndRoleLabDTO{
  @ApiProperty()
  username: string;
  @ApiProperty({required:false})
  fullname: string;
  @ApiProperty({required:false})
  password: string;
  @ApiProperty()
  labId: string;
  @ApiProperty()
  roleId: number;
}
