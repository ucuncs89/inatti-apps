import { LabDetail } from "@app/database/entity/lab/lab-detail.entity";
import { ApiProperty } from "@nestjs/swagger";

export class CreateLabDto {
  @ApiProperty()
  name: string;
  @ApiProperty({nullable: true})
  tenantId?: string;
}
