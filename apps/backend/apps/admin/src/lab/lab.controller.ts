import { Get, Post, Body, Patch, Param, Delete, BadRequestException } from '@nestjs/common';
import { LabService } from './lab.service';
import { CreateLabDto } from './dto/create-lab.dto';
import { UpdateConfigLab, UpdateLabDto } from './dto/update-lab.dto';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';

@RequireAuth()
@AdminApi('/lab')
export class LabController {
  constructor(private readonly labService: LabService) {}

  @Post()
  create(@Body() createLabDto: CreateLabDto) {
    return this.labService.create(createLabDto);
  }

  @Get()
  findAll() {
    return this.labService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.labService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLabDto: UpdateLabDto) {
    try{
      return this.labService.update(id, updateLabDto);
    } catch(err){
      console.log(err);
      throw new BadRequestException(err)
    }
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    try{
      return await this.labService.remove(id);
    }catch(err){
      throw new BadRequestException(err)
    }
  }

  @Patch('config/:idlab')
  async updateConfig(
    @Param('idlab')
    idLab: string,
    @Body()
    body: UpdateConfigLab
  ){
    try{
      return await this.labService.updateConfig(idLab, body)
    } catch(err){
      throw new BadRequestException(err)
    }
  }
}
