import { Module } from '@nestjs/common';
import { LabService } from './lab.service';
import { LabController } from './lab.controller';
import { DatabaseModule } from '@app/database';
import { RolesController } from './roles.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [LabController, RolesController],
  providers: [LabService]
})
export class LabModule {}
