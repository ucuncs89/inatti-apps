import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { AddUserAndRoleLabDTO, AssignRoleLabDto } from './dto/assign-role.dto';
import { LabService } from './lab.service';

@RequireAuth()
@AdminApi('/lab-role')
export class RolesController {
  constructor(private readonly labService: LabService) {}

  @Get('roles')
  getRoles() {
    return this.labService.getRoles();
  }

  @Get('roles-assigned/:idlab')
  getRolesLab(@Param('idlab') id: string) {
    return this.labService.getLabRole(id);
  }

  @Post('roles-assign/add-user')
  async addUserToRole(@Body() addUserRoleDTO: AddUserAndRoleLabDTO) {
    try {
      return await this.labService.addUserRole(addUserRoleDTO);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  @Patch('roles-assign')
  assignRolesToUser(@Body() assignRoleUserDTO: AssignRoleLabDto) {
    return this.labService.setUserRole(assignRoleUserDTO);
  }

  @Delete('roles-assign/:id')
  deleteRolesUser(@Param('id') id: string) {
    return this.labService.deleteUserRole(+id);
  }

  @Get('roles-assigned-to-user/:idUser')
  async getRolesUser(@Param('idUser') id: string) {
    return await this.labService.getUserRole(id);
  }
}
