import { LabConfig } from '@app/database/entity/lab/lab-config.entity';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabRoleUser } from '@app/database/entity/lab/lab-role-user.entity';
import { LabRole } from '@app/database/entity/lab/lab-role.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { AddUserAndRoleLabDTO, AssignRoleLabDto } from './dto/assign-role.dto';
import { CreateLabDto } from './dto/create-lab.dto';
import { UpdateConfigLab, UpdateLabDto } from './dto/update-lab.dto';

@Injectable()
export class LabService {
  constructor(
    @InjectRepository(LabDetail)
    private LabDetailRepository: Repository<LabDetail>,
    @InjectRepository(LabRole)
    private LabRoleRepository: Repository<LabRole>,
    @InjectRepository(LabRoleUser)
    private LabRoleUserRepository: Repository<LabRoleUser>,
    @InjectRepository(LabConfig)
    private LabConfigRepository: Repository<LabConfig>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ){}

  create(createLabDto: CreateLabDto) {
    const newLab = new LabDetail()
    newLab.name = createLabDto.name;
    newLab.tenantId = createLabDto.tenantId
    return this.LabDetailRepository.save(newLab);
  }

  findAll() {
    return this.LabDetailRepository.find({
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'config'
      ]
    });
  }

  findOne(id: string) {
    return this.LabDetailRepository.findOne({
      where: {
        id
      },
      relations: [
        'config'
      ]
    })
  }

  async update(id: string, updateLabDto: UpdateLabDto) {
    let lab = await this.findOne(id)
    if(!lab){
      throw new Error('Lab not found')
    }
    Object.assign(lab, updateLabDto)
    return this.LabDetailRepository.save(lab)
  }

  async remove(id: string) {
    return this.LabDetailRepository.softDelete(id);
  }

  getRoles(){
    return this.LabRoleRepository.find()
  }

  async getLabRole(idLab: string){
    let result:any = await this.LabRoleUserRepository.find({
      where: {
        labId: new LabDetail(idLab)
      },
      relations: ['userId', 'roleId']
    })
    result = result.map((v) => {
      v.userId.hideCredential();
      return v
    })
    return result;
  }

  async setUserRole(assignRoleDTO: AssignRoleLabDto){
    const creteria = {
      userId: new User(assignRoleDTO.userId),
      labId: new LabDetail(assignRoleDTO.labId),
      roleId: new LabRole(assignRoleDTO.roleId)
    }
    let labRole = await this.LabRoleUserRepository.findOne({
      where: {
        userId: creteria.userId,
        labId: creteria.labId,
      }
    })
    if(!labRole){
      labRole = new LabRoleUser();
      labRole.labId = creteria.labId;
      labRole.userId = creteria.userId;
    }
    labRole.roleId = creteria.roleId
    return this.LabRoleUserRepository.save(labRole);
  }

  deleteUserRole(id: number){
    this.LabRoleUserRepository.delete(id);
  }

  async addUserRole(params: AddUserAndRoleLabDTO){
    let user = await this.UserRepository.findOne({
      username: params.username
    })
    if(!user){
      user = new User();
      user.username = params.username
      user.fullname = params.fullname
      user.password = params.password
      user = await this.UserRepository.save(user)
    }

    return await this.setUserRole({
      labId: params.labId,
      roleId: params.roleId,
      userId: user.id
    })
  }

  async updateConfig(labId: string, config:UpdateConfigLab){
    let labConfig = await this.LabConfigRepository.findOne({
      lab: {
        id: labId
      }
    })
    if(!labConfig){
      labConfig = new LabConfig()
      labConfig.lab = new LabDetail(labId)
    }
    Object.assign(labConfig, config)

    return this.LabConfigRepository.save(labConfig)
  }

  getUserRole(id: string){
    return this.LabRoleUserRepository.find({
      where: {
        userId: id,
      },
      relations: ['labId', 'roleId'],
      withDeleted:true
    })
  }
}
