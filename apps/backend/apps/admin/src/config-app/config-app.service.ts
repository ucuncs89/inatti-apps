import { ConfigApp } from '@app/database/entity/config/config-app.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as _ from 'lodash';
import { Repository } from 'typeorm';
import { ConfigAppUpdateDTO } from './type';

/**
 * Config app for admin apps
 */
@Injectable()
export class ConfigAppService {
  constructor(
    @InjectRepository(ConfigApp)
    private ConfigAppRepository: Repository<ConfigApp>,
  ) { }

  async getConfig(where: { category?: string, name?: string }) {
    where = _.pickBy(where, _.identity)

    let res: ConfigApp[] | ConfigApp = await this.ConfigAppRepository.find({
      where,
    })
    if (res.length < 1) return null

    if (where.name && res.length === 1) {
      // return object
      return res[0]
    }

    return res;
  }

  async updateConfig(body: Record<string, string>) {
    let keys = Object.keys(body)
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      await this.ConfigAppRepository.update({
        name: key
      }, {
        value: body[key]
      })
    }
  }

  async updateConfigById(id: number, payload: ConfigAppUpdateDTO) {
    return await this.ConfigAppRepository.update(id, payload)
  }
}
