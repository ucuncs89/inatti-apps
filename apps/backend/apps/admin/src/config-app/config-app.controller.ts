import { RequireAuthApp } from '@app/authentication/authorization/jwt.authguard.multiapp';
import { AppsExcludeUser } from '@app/database/entity/apps/apps.type';
import { BadRequestException, Body, Controller, Get, Param, Patch, Query } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { ConfigAppService } from './config-app.service';
import { ConfigAppUpdateDTO } from './type';

@RequireAuthApp(...AppsExcludeUser)
@AdminApi('config-app')
export class ConfigAppController {
  constructor(
    private readonly configAppService: ConfigAppService,
  ){}

  @Get('template-notify')
  async getTemplateNotify(){
    try{
      // Make sure in table config-app using category `template-notify`
      return await this.configAppService.getConfig({
        category: 'template-notify'
      })
    } catch(err){
      throw new BadRequestException(err)
    }
  }

  @Get('whatsapp-service')
  async getWhatsappService(){
    try{
      // Make sure in table config-app using category `whatsapp-service`
      return await this.configAppService.getConfig({
        category: 'whatsapp-service'
      })
    } catch(err){
      throw new BadRequestException(err)
    }
  }

  @ApiQuery({
    name: 'name',
    required:false
  })
  @ApiQuery({
    name: 'category',
    required:false
  })
  @Get('config')
  async getConfig(
    @Query("name")
    name: string,
    @Query("category")
    category: string
  ){
    return await this.configAppService.getConfig({
      name,
      category
    })
  }

  @ApiOperation({
    description: "Only used by whatsapp config",
    summary: "Update config category type",
  })
  @ApiBody({
    description: "Object with key as name config, value object as value",
  })
  @Patch('update-config')
  async udpateConfig(@Body() body:any){
    try{
      // Make sure in table config-app using category `whatsapp-service`
      return await this.configAppService.updateConfig(body)
    } catch(err){
      throw new BadRequestException(err)
    }
  }

  @ApiOperation({
    summary: "Update config by id"
  })
  @Patch('update/:idconfig')
  async updateConfig(
    @Param('idconfig')
    idConfig: number,
    @Body()
    paylaod: ConfigAppUpdateDTO
  ){
    return await this.configAppService.updateConfigById(idConfig, paylaod)
  }
}
