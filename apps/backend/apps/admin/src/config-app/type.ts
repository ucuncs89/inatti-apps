import { ApiProperty } from "@nestjs/swagger"

export class ConfigAppUpdateDTO {
  @ApiProperty()
  name: string
  
  @ApiProperty()
  value: string
  
  @ApiProperty({
    example: "['data1', 'data2']"
  })
  valueJson: Object|any[]
  
  @ApiProperty()
  category: string
}
