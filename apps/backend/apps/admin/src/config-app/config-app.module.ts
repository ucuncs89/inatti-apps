import { DatabaseModule } from '@app/database';
import { Module } from '@nestjs/common';
import { ConfigAppController } from './config-app.controller';
import { ConfigAppService } from './config-app.service';

@Module({
  imports: [DatabaseModule],
  controllers: [ConfigAppController],
  providers: [ConfigAppService],
  exports: [ConfigAppService]
})
export class ConfigAppModule { }
