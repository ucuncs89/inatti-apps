import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AdminModule } from './admin.module';

async function bootstrap() {
  const app = await NestFactory.create(AdminModule);
  const swagger = 'api'
  app.enableCors()
  if (process.env.NODE_ENV === 'development') {
    const config = new DocumentBuilder()
      .setTitle('Inatti Admin Documentation')
      .setDescription('Inatti Admin API Documentation description')
      .setVersion('1.0')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup(swagger, app, document);
  }
  await app.listen(process.env.NODE_PORT_ADMIN);
  Logger.log(`App Admin Jalan di port : ${process.env.NODE_PORT_ADMIN}`, 'RUN APP')
  Logger.log(`Akses Swagger : ${process.env.NODE_PORT_ADMIN}/${swagger}`, 'RUN APP')
}
bootstrap();
