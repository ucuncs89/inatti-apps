import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { BadRequestException,  Get, Param, Query, Request, Response } from '@nestjs/common'
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { ReporterService } from './reporter.service';
import { TransactionService } from './transaction.service';
import { CommonFilterTransaction } from './type';

@RequireAuth()
@AdminApi('transaction')
export class TransactionController {
  constructor(
    private transactionService: TransactionService,
    private reporterService: ReporterService
  ){}

  @Get('/history')
  async getTransactionHistory(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterTransaction
  ){
    try {
      return await this.transactionService.getTransactionHistory(paginate, filter)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/history/export')
  async getTransactionHistoryExport(
    @Response()
    res:any,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterTransaction
  ){
    try {
      return await this.transactionService.getTransactionHistoryExport(res, filter)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/detail/:id')
  async getDetailTransaction(
    @Param('id')
    idOrder: string
  ){
    try {
      return await this.transactionService.getDetailTransaction(idOrder)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error.message)
    }
  }

  @Get('/perpatient')
  async getTransactionPerpatient(
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterTransaction
  ){
    try {
      return await this.transactionService.getTransactionPerpatient(paginate, filter)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/perpatient/export')
  async getTransactionPerpatientExport(
    @Response()
    res:any,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterTransaction
  ){
    try {
      return await this.transactionService.getTransactionPerpatientExport(res, filter)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/lab/assigned')
  async getLabAssigned(
    @Request()
    req: any
  ){
    try {
      return await this.reporterService.getLabAssigned(req.user.id)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/ref/assigned')
  async getRefAssigned(
    @Request()
    req: any
  ){
    try {
      return await this.reporterService.getRefAssigned(req.user.id)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }
}
