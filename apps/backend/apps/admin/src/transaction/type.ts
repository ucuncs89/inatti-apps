import { ApiProperty } from "@nestjs/swagger";

export class CommonFilterTransaction {
  @ApiProperty({name: 'filter[rujukan]', required: false})
  rujukan?: string[]
  @ApiProperty({name: 'filter[rujukanProduct]', required: false})
  rujukanProduct?: string
  @ApiProperty({name: 'filter[lab]', required: false, description: 'only work in API perpatient'})
  lab?: string
  @ApiProperty({name: 'filter[method]', example: 'Cash or Transfer', required: false})
  method?: string
  @ApiProperty({name: 'filter[statusTrx]', required: false})
  statusTrx?: string
  @ApiProperty({name: 'filter[productType]', required: false})
  productType?: string
  @ApiProperty({name: 'filter[createdAtStart]', example: '2022-02-30 or 2022-02-30 00:00:00', required: false})
  createdAtStart?: string
  @ApiProperty({name: 'filter[createdAtEnd]',example: '2022-02-30 or 2022-02-30 00:00:00', required: false})
  createdAtEnd?: string
}