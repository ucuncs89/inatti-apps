import { AdminRoleUser } from '@app/database/entity/admin/admin-role-user.entity';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabRoleUser } from '@app/database/entity/lab/lab-role-user.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ReporterService {
  constructor(
    @InjectRepository(LabRoleUser)
    private LabRoleUserRepository: Repository<LabRoleUser>,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(AdminRoleUser)
    private AdminRoleUserRepository: Repository<AdminRoleUser>,
    @InjectRepository(LabDetail)
    private LabDetailRepository: Repository<LabDetail>,
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
  ){}

  async getRoleUser(userId:string){
    let roleAdmin = await this.AdminRoleUserRepository.findOne({
      where: {
        userId: {
          id: userId
        }
      },
      relations: [
        'roleId'
      ]
    })

    if(!roleAdmin){
      throw "not allowed"
    }

    return roleAdmin.roleId.id
  }

  isReporter(roleUser: number){
    return roleUser === 2
  }

  isAdmin(roleUser: number){
    return roleUser === 1
  }

  async getLabAssigned(userId: string){
    let roleUser = await this.getRoleUser(userId)
    
    if(this.isAdmin(roleUser)){
      return this.LabDetailRepository.find()
    }

    if(this.isReporter(roleUser)){
      let listLabRole = await this.LabRoleUserRepository.find({
        where: {
          userId: {
            id: userId
          }
        },
        relations: [
          'labId'
        ]
      })
      if(!listLabRole) return []

      return listLabRole.map((v) => v.labId)
    }
    return null
  }

  async getRefAssigned(userId: string){
    let roleUser = await this.getRoleUser(userId)
    
    if(this.isAdmin(roleUser)){
      return this.RefDetailRepository.find()
    }

    if(this.isReporter(roleUser)){
      let listRefRole = await this.RefRoleUserRepository.find({
        where: {
          userId: {
            id: userId
          }
        },
        relations: [
          'refId'
        ]
      })
      if(!listRefRole) return []

      return listRefRole.map((v) => v.refId)
    }
    return null
  }
}
