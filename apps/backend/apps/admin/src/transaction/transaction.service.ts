import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFormatOrderTrxXlsx, CommonFormatOrderXlsx } from '@app/helper/report-xlsx/common-format';
import { deepValueToXlsx } from '@app/helper/report-xlsx/to-xlsx';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, FindConditions, ObjectLiteral, Repository , In } from 'typeorm';
import { CommonFilterTransaction } from './type';
import * as dayjs from 'dayjs';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
  ){}

  private getSelectionWhere(filter:CommonFilterTransaction){
    let where:FindConditions<SwabOrderTrx | SwabOrder>[] | FindConditions<SwabOrderTrx | SwabOrder> | ObjectLiteral | string = {}
    if(filter){
      where = {}
      if(filter.rujukan){
        where.reference = {
          id: Array.isArray(filter.rujukan) ? In(filter.rujukan) : filter.rujukan
        }
      }
      if(filter.method){
        where.method = filter.method
      }
      if(filter.statusTrx){
        where.status = filter.statusTrx
      }
      if(filter.createdAtEnd && filter.createdAtStart){
        // increment createdAtEnd to 1 day
        let endDate = dayjs(filter.createdAtEnd)
        endDate = endDate.add(1, 'day');
        where.createdAt = Between(filter.createdAtStart, endDate.format('YYYY-MM-DD'))
      }
      if(filter.productType){
        where.product = {
          product: {
            id: +filter.productType
          }
        }
      }
      if(filter.rujukanProduct){
        where.product = {
          id: +filter.rujukanProduct
        }
      }
    }
    return where
  }

  relationsSwabOrderTrx = [
    'orderSwab',
    'orderSwab.detailSwab',
    'orderSwab.detailSwab.patient',
    'methodCash',
    'reference',
    'product',
    'product.product',
  ]

  async getTransactionHistory(paginate: PaginationOptionsInterface, filter: CommonFilterTransaction) {
    let where:any = this.getSelectionWhere(filter)
    
    let orderTrxs = await this.SwabOrderTrxRepository.find({
      take: paginate.take,
      skip: paginate.skip,
      order: {
        createdAt: 'DESC',
      },
      relations: this.relationsSwabOrderTrx,
      where,
    })
    for (const orderTrx of orderTrxs) {
      orderTrx.totalOrder = orderTrx.orderSwab.length
      delete orderTrx.orderSwab
    }
    const total = await this.SwabOrderTrxRepository.count({cache: true, where, relations: this.relationsSwabOrderTrx})
    return new Pagination({
      results: orderTrxs,
      total,
    }, paginate)
  }

  async getDetailTransaction(idOrder:string){
    let result = await this.SwabOrderTrxRepository.findOne({
      relations: this.relationsSwabOrderTrx,
      where: {
        id: idOrder
      }
    })
    if(result.customer) {
      result.customer.hideCredential();
    }
    return result;
  }

  async getTransactionHistoryExport(res:any, filter:CommonFilterTransaction){
    let where:any = this.getSelectionWhere(filter)

    let orderTrxs = await this.SwabOrderTrxRepository.find({
      order: {
        createdAt: 'DESC',
      },
      relations: this.relationsSwabOrderTrx,
      where,
    })
    for (const orderTrx of orderTrxs) {
      orderTrx.getTotalOrder()
    }

    await deepValueToXlsx(res, orderTrxs, CommonFormatOrderTrxXlsx)
  }

  relationsSwabOrder = [
    'detailSwab',
    'detailSwab.refTarget',
    'detailSwab.labTarget',
    'detailSwab.patient',
    'customer',
    'reference',
    'product',
    'product.product',
    'orderTrx',
    'methodCash',
  ]
  async getTransactionPerpatient(paginate: PaginationOptionsInterface, filter: CommonFilterTransaction) {
    let where:any = this.getSelectionWhere(filter)

    if(filter.lab){
      where.detailSwab = {
        labTarget: {
          id: Array.isArray(filter.lab) ? In(filter.lab) : filter.lab
        }
      }
    }

    let results = await this.SwabOrderRepository.find({
      order:{
        createdAt:'DESC'
      },
      take: paginate.take,
      skip: paginate.skip,
      relations: this.relationsSwabOrder,
      where
    })
    for (const result of results) {
      if(result.customer){
        result.customer.hideCredential()
      }
    }
    const total = await this.SwabOrderRepository.count({where, relations: this.relationsSwabOrder})
    return new Pagination({
      results,
      total,
    }, paginate)
  }

  async getTransactionPerpatientExport(res:any, filter:CommonFilterTransaction){
    let where:any = this.getSelectionWhere(filter)

    if(filter.lab){
      where.detailSwab = {
        labTarget: {
          id: Array.isArray(filter.lab) ? In(filter.lab) : filter.lab
        }
      }
    }

    let results = await this.SwabOrderRepository.find({
      order:{
        createdAt:'DESC'
      },
      relations: this.relationsSwabOrder,
      where
    })

    await deepValueToXlsx(res, results, CommonFormatOrderXlsx)
  }

}
