import { LoginDTO } from '@app/authentication/types';
import { BadRequestException, Body, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AdminService } from './admin.service';
import { AdminApi } from './helper/admin-controller.decorator';
import { RequireAuth } from './helper/auth.decorator';

@AdminApi()
export class AdminController {
  constructor(
    private adminService: AdminService
  ){}

  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard('admin'))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @RequireAuth()
  @Get('/role-user')
  async getRoleUser(
    @Request()
    req: any
  ) {
    try {
      return await this.adminService.getRoleUser(req.user.id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
