import { ApiProperty } from '@nestjs/swagger';

export class AssignRoleAdminDto {
  @ApiProperty()
  userId: string;
  @ApiProperty()
  roleId: number;
}

export class AddUserAndRoleDTO {
  @ApiProperty()
  username: string;
  @ApiProperty({required:false})
  fullname: string;
  @ApiProperty({required:false})
  password: string;
  @ApiProperty()
  labId: string;
  @ApiProperty()
  roleId: number;
}
