import { AdminRoleUser } from '@app/database/entity/admin/admin-role-user.entity';
import { User } from '@app/database/entity/user/user.entity';
import { AdminRole } from '@app/database/entity/admin/admin-role.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AssignRoleAdminDto, AddUserAndRoleDTO } from './dto/assign-role.dto';

@Injectable()
export class AdminUserService {
  constructor(
    @InjectRepository(AdminRole)
    private AdminRoleRepository: Repository<AdminRole>,
    @InjectRepository(AdminRoleUser)
    private AdminRoleUserRepository: Repository<AdminRoleUser>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ) {}
  getRoles() {
    return this.AdminRoleRepository.find();
  }
  getUsers() {
    return this.AdminRoleUserRepository.find({
      relations: ['userId', 'roleId'],
    });
  }
  async getAdminRole(idUser: string) {
    let result: any = await this.AdminRoleUserRepository.find({
      where: {
        userId: idUser,
      },
      relations: ['userId', 'roleId'],
    });
    result = result.map((v) => {
      v.userId.hideCredential();
      return v;
    });
    return result;
  }
  async setUserRole(assignRoleDTO: AssignRoleAdminDto) {
    const creteria = {
      userId: new User(assignRoleDTO.userId),
      roleId: new AdminRole(assignRoleDTO.roleId),
    };
    let adminRole = await this.AdminRoleUserRepository.findOne({
      where: {
        userId: creteria.userId,
      },
    });
    if (!adminRole) {
      adminRole = new AdminRoleUser();
      adminRole.userId = creteria.userId;
    }
    adminRole.roleId = creteria.roleId;
    return this.AdminRoleUserRepository.save(adminRole);
  }
  deleteUserRole(id: number) {
    this.AdminRoleUserRepository.delete(id);
  }
  async addUserRole(params: AddUserAndRoleDTO) {
    let user = await this.UserRepository.findOne({
      username: params.username,
    });
    console.log({ params, user });
    if (!user) {
      user = new User();
      user.username = params.username;
      user.fullname = params.fullname;
      user.password = params.password;
      user = await this.UserRepository.save(user);
    }
    return await this.setUserRole({
      roleId: params.roleId,
      userId: user.id,
    });
  }

  async getUserRole(idUser: string){
    return await this.AdminRoleUserRepository.find({
      where: {
        userId: idUser,
      },
      relations: ['userId', 'roleId'],
    });
  }
}
