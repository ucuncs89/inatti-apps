import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { RequireAuth } from '../helper/auth.decorator';
import { AssignRoleAdminDto, AddUserAndRoleDTO } from './dto/assign-role.dto';
import { AdminApi } from '../helper/admin-controller.decorator';
import { AdminUserService } from './admin-user.service';

@RequireAuth()
@AdminApi('/admin-user')
export class AdminUserController {
  constructor(private readonly adminUserService: AdminUserService) {}
  @Get('roles')
  getRoles() {
    return this.adminUserService.getRoles();
  }
  @Get('roles-assigned/:idUser')
  getRolesLab(@Param('idUser') id: string) {
    return this.adminUserService.getAdminRole(id);
  }
  @Get('user')
  getUsers() {
    return this.adminUserService.getUsers();
  }
  @Post('roles-assign/add-user')
  async addUserToRole(@Body() addUserRoleDTO: AddUserAndRoleDTO) {
    try {
      return await this.adminUserService.addUserRole(addUserRoleDTO);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
  @Patch('roles-assign')
  assignRolesToUser(@Body() assignRoleUserDTO: AssignRoleAdminDto) {
    return this.adminUserService.setUserRole(assignRoleUserDTO);
  }
  @Delete('roles-assign/:id')
  deleteRolesUser(@Param('id') id: string) {
    return this.adminUserService.deleteUserRole(+id);
  }

  @Get('roles-assigned-to-user/:idUser')
  async getRolesUser(@Param('idUser') id: string) {
    return await this.adminUserService.getUserRole(id);
  }

}
