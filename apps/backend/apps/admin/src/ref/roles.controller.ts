import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { AddUserAndRoleRefDTO, AssignRoleRefDto } from './dto/assign-role.dto';
import { RefService } from './ref.service';

@RequireAuth()
@AdminApi('/ref-role')
export class RefRolesController {
  constructor(
    private readonly refService: RefService
  ){}

  @Post('roles-assign/add-user')
  async addUserToRole(@Body() addUserRoleDTO: AddUserAndRoleRefDTO){
    try {
      return await this.refService.addUserRole(addUserRoleDTO)
    }catch(err){
      throw new BadRequestException(err)
    }
  }

  @Get('roles')
  getRoles(){
    return this.refService.getRoles()
  }

  @Get('roles-assigned/:idref')
  getRolesRef(@Param('idref') id:string){
    return this.refService.getRefRole(id);
  }

  @Patch('roles-assign')
  assignRolesToUser(@Body() assignRoleUserDTO: AssignRoleRefDto){
    return this.refService.setUserRole(assignRoleUserDTO)
  }

  @Delete('roles-assign/:id')
  deleteRolesUser(@Param('id') id: string){
    return this.refService.deleteUserRole(+id)
  }

  @Get('roles-assigned-to-user/:idUser')
  async getRolesUser(@Param('idUser') id: string) {
    return await this.refService.getUserRole(id);
  }
}
