import { PartialType } from '@nestjs/swagger';
import { UpdateConfigLab } from '../../lab/dto/update-lab.dto';
import { CreateRefDto } from './create-ref.dto';

export class UpdateRefDto extends PartialType(CreateRefDto) {}

export class UpdateConfigRef extends UpdateConfigLab { }