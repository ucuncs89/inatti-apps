import { ApiProperty } from "@nestjs/swagger";

export class CreateRefDto {
  @ApiProperty()
  name: string;
  @ApiProperty({nullable: true})
  tenantId?: string;
}
