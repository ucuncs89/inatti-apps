import { ApiProperty } from "@nestjs/swagger";

export class AssignRoleRefDto{
  @ApiProperty()
  userId: string;
  @ApiProperty()
  refId: string;
  @ApiProperty()
  roleId: number;
}


export class AddUserAndRoleRefDTO{
  @ApiProperty()
  username: string;
  @ApiProperty({nullable:true})
  fullname: string;
  @ApiProperty({nullable:true})
  password: string;
  @ApiProperty()
  refId: string;
  @ApiProperty()
  roleId: number;
}
