import { Module } from '@nestjs/common';
import { RefService } from './ref.service';
import { RefController } from './ref.controller';
import { DatabaseModule } from '@app/database';
import { RefRolesController } from "./roles.controller"

@Module({
  imports: [DatabaseModule],
  controllers: [RefController, RefRolesController],
  providers: [RefService]
})
export class RefModule {}
