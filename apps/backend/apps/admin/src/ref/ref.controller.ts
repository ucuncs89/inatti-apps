import { Get, Post, Body, Patch, Param, Delete, BadRequestException } from '@nestjs/common';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { CreateRefDto } from './dto/create-ref.dto';
import { UpdateRefDto, UpdateConfigRef } from './dto/update-ref.dto';
import { RefService } from './ref.service';

@RequireAuth()
@AdminApi('/ref')
export class RefController {
  constructor(private readonly refService: RefService) {}

  @Post()
  create(@Body() createRefDto: CreateRefDto) {
    return this.refService.create(createRefDto);
  }

  @Get()
  findAll() {
    return this.refService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.refService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRefDto: UpdateRefDto) {
    return this.refService.update(id, updateRefDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try{
      return await this.refService.remove(id);
    } catch(err){
      throw new BadRequestException(err)
    }
  }

  @Patch('config/:idref')
  async updateConfig(
    @Param('idref')
    idRef: string,
    @Body()
    body: UpdateConfigRef
  ){
    try{
      return await this.refService.updateConfig(idRef, body)
    } catch(err){
      throw new BadRequestException(err)
    }
  }
}
