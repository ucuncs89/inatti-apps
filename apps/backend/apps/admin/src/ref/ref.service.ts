import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';
import { RefRole } from '@app/database/entity/ref/ref-role.entity';
import { RefConfig } from '@app/database/entity/ref/ref-config.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddUserAndRoleRefDTO, AssignRoleRefDto } from './dto/assign-role.dto';
import { CreateRefDto } from './dto/create-ref.dto';
import { UpdateRefDto, UpdateConfigRef } from './dto/update-ref.dto';

@Injectable()
export class RefService {
  constructor(
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
    @InjectRepository(RefRole)
    private RefRoleRepository: Repository<RefRole>,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
    @InjectRepository(RefConfig)
    private RefConfigRepository: Repository<RefConfig>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ){}

  create(createRefDto: CreateRefDto) {
    const newRef = new RefDetail()
    newRef.name = createRefDto.name;
    newRef.tenantId = createRefDto.tenantId
    return this.RefDetailRepository.save(newRef);
  }

  findAll() {
    return this.RefDetailRepository.find({
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'config'
      ]
    });
  }

  findOne(id: string) {
    return this.RefDetailRepository.findOne({
      where: {
        id
      },
      relations: [
        'config',
        'products'
      ]
    })
  }

  async update(id: string, updateRefDto: UpdateRefDto) {
    let ref = await this.findOne(id)
    if(!ref){
      throw new Error('Ref not found')
    }
    Object.assign(ref, updateRefDto)
    return this.RefDetailRepository.save(ref)
  }

  async remove(id: string) {
    return this.RefDetailRepository.softDelete(id);
  }

  getRoles(){
    return this.RefRoleRepository.find()
  }

  async getRefRole(idRef: string){
    let result:any = await this.RefRoleUserRepository.find({
      where: {
        refId: {
          id: idRef
        }
      },
      relations: ['userId', 'roleId']
    })
    result = result.map((v) => {
      v.userId.hideCredential();
      return v
    })
    return result;
  }

  async setUserRole(assignRoleDTO: AssignRoleRefDto){
    const creteria = {
      userId: new User(assignRoleDTO.userId),
      refId: new RefDetail(assignRoleDTO.refId),
      roleId: new RefRole(assignRoleDTO.roleId)
    }
    let refRole = await this.RefRoleUserRepository.findOne({
      where: {
        userId: creteria.userId,
        refId: creteria.refId,
      }
    })
    if(!refRole){
      refRole = new RefRoleUser();
      refRole.refId = creteria.refId;
      refRole.userId = creteria.userId;
    }
    refRole.roleId = creteria.roleId
    return this.RefRoleUserRepository.save(refRole);
  }

  deleteUserRole(id: number){
    this.RefRoleUserRepository.delete(id);
  }

  async addUserRole(params: AddUserAndRoleRefDTO){
    let user = await this.UserRepository.findOne({
      username: params.username
    })
    if(!user){
      user = new User();
      user.username = params.username
      user.fullname = params.fullname
      user.password = params.password
      user = await this.UserRepository.save(user)
    }

    return await this.setUserRole({
      refId: params.refId,
      roleId: params.roleId,
      userId: user.id
    })
  }

  async updateConfig(idRef: string, config: UpdateConfigRef){
    let refConfig = await this.RefConfigRepository.findOne({
      ref: {
        id: idRef
      }
    })
    if(!refConfig){
      refConfig = new RefConfig()
      refConfig.ref = new RefDetail(idRef)
    }
    Object.assign(refConfig, config)

    return this.RefConfigRepository.save(refConfig)
  }

  getUserRole(id: string){
    return this.RefRoleUserRepository.find({
      where: {
        userId: id,
      },
      relations: ['roleId', 'refId'],
      withDeleted:true
    })
  }
}
