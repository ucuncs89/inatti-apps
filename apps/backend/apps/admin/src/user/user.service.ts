import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ){}

  create(createUserDto: CreateUserDto) {
    const user = this.UserRepository.create(createUserDto)
    return this.UserRepository.save(user);
  }

  findAll() {
    return this.UserRepository.find();
  }

  findOne(id: string) {
    return this.UserRepository.findOne(id);
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.UserRepository.findOne(id);
    if(!user){
      throw new Error("User not found");
    }
    Object.assign(user, updateUserDto)
    if(updateUserDto.password){
      await user.hashPassword();
    }
    return this.UserRepository.save(user)
  }

  remove(id: string) {
    return this.UserRepository.delete(id);
  }

  async findUsername(username: string) {
    const user = await this.UserRepository.findOne({
      username
    })
    return user
  }
}
