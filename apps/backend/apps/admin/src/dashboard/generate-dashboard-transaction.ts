import { User } from "@app/database/entity/user/user.entity"
import * as dayjs from "dayjs"
import { EntityManager } from "typeorm"
import { arrayJoin, FilterDashboard, GeneratorDashboardI, genWhere, runQueryGroupResult } from "./helper"

export class GenerateDashboardTransaction implements GeneratorDashboardI{
  constructor(manager:EntityManager){
    this.manager = manager
  }
  manager: EntityManager
  filter:FilterDashboard = {}

  async generate(filter?:FilterDashboard){
    if(filter) this.filter = filter

    const result = {
      totalTransaction: await this.getTotalTransaction(),
      totalTransactionCash: await this.getTotalTransactionType('Cash'),
      totalTransactionTransfer: await this.getTotalTransactionType('Transfer'),
      totalTransactionThisMonth: await this.getTotalTransactionMonthly(0),
      totalTransactionLastMonth: await this.getTotalTransactionMonthly(-1),
      chartTransaction: await this.getChartTransaction(),
    }
    return result;
  }

  private async queryGetTotal(query: string){
    let res = await this.manager.query(query)
    if(!query.includes('total')) throw "No field total found"
    if(!res || res.length < 1) return 0
    return res[0].total
  }

  private getTotalTransaction(){
    let where = []
    where.push(`status in ('paid', 'paymentReceived')`)
    if(this.filter.refIds){
      if(this.filter.refIds.length < 1) return 0
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select sum("totalPrice") as total from swab_order_trx sot ${genWhere(where)}`)
  }
  
  getChartTransaction(){
    return runQueryGroupResult(this.getChartTransactionInterval.bind(this))
  }

  private getChartTransactionInterval(interval){
    let xAxisFormat = {
      'month': `to_char(created_at, 'YYYY-MM')`,
      'day': `to_char(created_at, 'YYYY-MM-DD')`,
      'year': `to_char(created_at, 'YYYY')`,
    }

    let where = []
    where.push(`status = 'paid'`)
    if(this.filter.refIds){
      if(this.filter.refIds.length < 1) return null
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }

    let query = `select
      sum("totalPrice") as value,
      method as group,
      ${xAxisFormat[interval]} as xaxis
    from
      swab_order_trx sot 
    ${genWhere(where)}
    group by
      xaxis, "group"
    order by xaxis asc`;

    return this.manager.query(query)
  }

  private getTotalTransactionType(type: string){
    let where = []
    where.push(`status in ('paid', 'paymentReceived')`)
    where.push(`method = '${type}'`)
    if(this.filter.refIds){
      if(this.filter.refIds.length < 1) return 0
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select sum("totalPrice") as total from swab_order_trx sot ${genWhere(where)}`)
  }

  private getTotalTransactionMonthly(month?: number){
    let monthStr = ''
    let now = dayjs()
    let nowMonth:any = now.get('month') + 1 + month
    nowMonth = nowMonth === 0 ? 12 : nowMonth
    nowMonth = ("0" + nowMonth).slice(-2)
    let nowYear = now.get('year')

    monthStr = `'${nowMonth}-${nowYear}'`

    let where = []
    where.push(`to_char(created_at, 'MM-YYYY') = ${monthStr}`)
    where.push(`status in ('paid', 'paymentReceived')`)
    if(this.filter.refIds){
      if(this.filter.refIds.length < 1) return 0
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }

    const query = `select sum("totalPrice") as total from swab_order_trx sot ${genWhere(where)}`
    
    return this.queryGetTotal(query)
  }

}