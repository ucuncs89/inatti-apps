import { Vaksin } from "@app/database/entity/vaksin/vaksin.entity";
import { EntityManager, In, IsNull, Not } from "typeorm";
import { GenerateDashboardTransaction } from "./generate-dashboard-transaction";
import { arrayJoin, FilterDashboard, GeneratorDashboardI, genWhere, runQueryGroupResult } from './helper'

export class GenerateDashboardAdmin implements GeneratorDashboardI {
  constructor(manager: EntityManager) {
    this.manager = manager
  }
  manager: EntityManager
  filter: FilterDashboard = {}

  async generate(filter?: FilterDashboard) {
    let trx = new GenerateDashboardTransaction(this.manager)
    if (filter) this.filter = filter

    const result = {
      totalPasien: await this.getTotalPasien(),
      totalRujukan: await this.getTotalRujukan(),
      totalLab: await this.getTotalLab(),
      totalTransaction: await this.getTotalTransaction(),
      totalTransactionMonth: await this.getTotalTransactionMonth(),
      totalTest: await this.getTotalTest(),
      totalTestMonth: await this.getTotalTestMonth(),
      chartTransaction: await trx.getChartTransaction(),
      chartTest: await this.getChartTest(),
      chartTestPcr: await this.getChartResultTest(1),
      chartTestAntigen: await this.getChartResultTest(2),
      vaksinTotalAlreadyVaksin: await this.getVaksinTotalAlreadyVaksin(),
      vaksinTotalNotVaksin: await this.getVaksinTotalNotVaksin(),
      vaksinGraph: await this.getVaksinGraph()
    }
    return result
  }

  private async queryGetTotal(query: string) {
    let res = await this.manager.query(query)
    if (!query.includes('total')) throw "No field total found"
    if (!res || res.length < 1) return 0
    return res[0].total
  }

  private getTotalRujukan() {
    let where = []
    where.push('"deletedAt" is null')
    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return 0
      where.push(`id in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select count(*) as total from ref_detail ${genWhere(where)}`)
  }

  private getTotalPasien() {
    return this.queryGetTotal(`select count(*) as total from swab_patient sp`);
  }

  private getTotalLab() {
    let where = []
    where.push('"deleteAt" is null')
    if (this.filter.labIds) {
      if (this.filter.labIds.length < 1) return 0
      where.push(`id in (${arrayJoin(this.filter.labIds)})`)
    }
    return this.queryGetTotal(`select count(*) as total from lab_detail ${genWhere(where)}`)
  }

  private getTotalTransaction() {
    let where = []
    where.push(`status = 'paid'`) // default
    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return 0
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select sum("totalPrice") as total from swab_order_trx sot ${genWhere(where)}`)
  }

  private getTotalTransactionMonth() {
    let where = []
    where.push(`status = 'paid'`) // default
    where.push(`date_part('month', "created_at") = date_part('month', now())`)
    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return 0
      where.push(`"referenceId" in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select sum("totalPrice") as total from swab_order_trx sot ${genWhere(where)}`)
  }

  private getTotalTest() {
    let where = []
    where.push('result_swab is not null')  // default
    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return 0
      where.push(`ref_target in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select count(*) as total from swab_detail ${genWhere(where)}`)
  }

  private getTotalTestMonth() {
    let where = []
    where.push('result_swab is not null')  // default
    where.push(`date_part('month', "created_at") = date_part('month', now())`)
    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return 0
      where.push(`ref_target in (${arrayJoin(this.filter.refIds)})`)
    }
    return this.queryGetTotal(`select count(*) as total from swab_detail ${genWhere(where)}`)
  }

  private getChartTest(type?: number) {
    return runQueryGroupResult((interval) => {
      return this.getChartTestInterval(interval, type)
    })
  }

  private getChartTestInterval(interval: string, type?: number) {
    let xAxisFormat = {
      'month': `to_char(created_at, 'YYYY-MM')`,
      'day': `to_char(created_at, 'YYYY-MM-DD')`,
      'year': `to_char(created_at, 'YYYY')`,
    }

    let where = []
    where.push('sd.result_swab is not null')  // default
    if (type) {
      where.push('p.id = ' + type)
    }

    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return null
      where.push(`ref_target in (${arrayJoin(this.filter.refIds)})`)
    }

    const query = `select
      count(*) as value,
      p.name as group,
      ${xAxisFormat[interval]} as xaxis
    from
      swab_detail sd
    inner join product p on p.id = sd.product_swab_id 
    ${genWhere(where)}
    group by
      xaxis, "group"
    order by xaxis asc`
    return this.manager.query(query)
  }

  private getChartResultTest(type?: number) {
    return runQueryGroupResult((interval) => {
      let xAxisFormat = {
        'month': `to_char(created_at, 'YYYY-MM')`,
        'day': `to_char(created_at, 'YYYY-MM-DD')`,
        'year': `to_char(created_at, 'YYYY')`,
      }

      let where = []
      where.push('sd.result_swab is not null')  // default
      if (type) {
        where.push('sd.product_swab_id = ' + type)
      }

      if (this.filter.refIds) {
        if (this.filter.refIds.length < 1) return null
        where.push(`ref_target in (${arrayJoin(this.filter.refIds)})`)
      }

      const query = `select
        count(*) as value,
        sd.result_swab as group,
        ${xAxisFormat[interval]} as xaxis
      from
        swab_detail sd 
      ${genWhere(where)}
      group by
        xaxis, "group"
      order by xaxis asc`
      return this.manager.query(query)
    })
  }

  /** region: Vaccine */
  async countVaksin(isVaccined: boolean) {
    const options: any = {
      relations: [
        'ref'
      ],
      where: {
        vaksinDateAt: isVaccined ? Not(IsNull()) : IsNull()
      }
    }
    if (this.filter.refIds) {
      options.where.ref = In(this.filter.refIds)
    }
    return this.manager.count(Vaksin, options)
  }

  async getVaksinTotalAlreadyVaksin() {
    return this.countVaksin(true)
  }

  async getVaksinTotalNotVaksin() {
    return this.countVaksin(false)
  }

  async getVaksinGraph() {
    return runQueryGroupResult((interval) => {
      return this.getVaksinGraphInterval(interval)
    })
  }

  getVaksinGraphInterval(interval: string) {
    let xAxisFormat = {
      'month': `to_char("createdAt", 'YYYY-MM')`,
      'day': `to_char("createdAt", 'YYYY-MM-DD')`,
      'year': `to_char("createdAt", 'YYYY')`,
    }

    let where = []
    where.push('v."vaksinDateAt" is not null')  // default

    if (this.filter.refIds) {
      if (this.filter.refIds.length < 1) return null
      where.push(`ref.id in (${arrayJoin(this.filter.refIds)})`)
    }

    const query = `select
      count(*) as value,
      ref.name as group,
      ${xAxisFormat[interval]} as xaxis
    from
      vaksin v
    inner join ref_detail as ref on ref.id = "v"."refId"
    ${genWhere(where)}
    group by
      xaxis, ref.id
    order by xaxis asc`
    return this.manager.query(query)
  }
}