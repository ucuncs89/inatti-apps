export async function runQueryGroupResult(cb:any){
  let result = {}
  const intervals = ['day', 'month', 'year']
  for (const interval of intervals) {
    result[interval] = await cb(interval)
  }
  return result;
}

export function arrayJoin(arr: string[]){
  return arr.map((v) => `'${v}'`).join(',')
}

export function genWhere(where: string[]){
  return where.length > 0 ? 'where '+where.join(' and ') : ''
}

export interface FilterDashboard {
  refIds?: string[],
  labIds?: string[]
}

export interface GeneratorDashboardI {
  filter:FilterDashboard
  generate(filter?:FilterDashboard)
}