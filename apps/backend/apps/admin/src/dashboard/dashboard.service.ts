import { ReportCacheAdmin } from '@app/database/entity/report/report-cache-admin.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, getManager, Repository } from 'typeorm';
import * as dayjs from 'dayjs'
import { User } from '@app/database/entity/user/user.entity';
import { GenerateDashboardAdmin } from './generate-dashboard-admin';
import { GenerateDashboardTransaction } from './generate-dashboard-transaction';
import { FilterDashboard } from './helper';
import { AdminRoleUser } from '@app/database/entity/admin/admin-role-user.entity';
import { LabRoleUser } from '@app/database/entity/lab/lab-role-user.entity';
import { RefRoleUser } from '@app/database/entity/ref/ref-role-user.entity';

const LABEL_DASHBOARD = "dashboard_admin"
const LABEL_TRANSACTION = "transaction_admin"
const ID_ROLEUSER_REPORTER = 2
@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(ReportCacheAdmin)
    private ReportCacheAdminRepository: Repository<ReportCacheAdmin>,
    @InjectRepository(AdminRoleUser)
    private AdminRoleUserRepository: Repository<AdminRoleUser>,
    @InjectRepository(LabRoleUser)
    private LabRoleUserRepository: Repository<LabRoleUser>,
    @InjectRepository(RefRoleUser)
    private RefRoleUserRepository: Repository<RefRoleUser>,
  ){}

  async getDashboard(isForce?: boolean, user?: User) {
    let report = await this.ReportCacheAdminRepository.findOne({
      where: {
        label:LABEL_DASHBOARD,
        user: user
      }
    })
    if(!report || this.isExpired(report.updatedAt)|| isForce){
      return await this.generateDashboard(report, user)
    }
    return report
  }

  async getTransaction(isForce?: boolean, user?:User) {
    let report = await this.ReportCacheAdminRepository.findOne({
      where: {
        label:LABEL_TRANSACTION,
        user: user
      }
    })
    if(!report || this.isExpired(report.updatedAt)|| isForce){
      return await this.generateDashboardTransaction(report, user)
    }
    return report
  }


  // expired in 1 day
  isExpired(updatedAt: Date): boolean {
    let expired = dayjs(updatedAt).add(1, 'day')
    return dayjs().isAfter(expired)
  }

  manager:EntityManager
  private async generateDashboard(reportEntity?:ReportCacheAdmin, user?: User) {
    const gen = new GenerateDashboardAdmin(getManager())
    gen.filter = await this.getFilterByRoleUser(user)
    const result = await gen.generate()
    return await this.updateDashboard(reportEntity, LABEL_DASHBOARD, result, user)
  }

  private async generateDashboardTransaction(report: ReportCacheAdmin, user?: User) {
    try{
      const gen = new GenerateDashboardTransaction(getManager())
      gen.filter = await this.getFilterByRoleUser(user)
      const result = await gen.generate()
      return await this.updateDashboard(report, LABEL_TRANSACTION, result, user)
    }catch(err){
      return null
    }
  }

  async updateDashboard(reportEntity: ReportCacheAdmin, label: string, data:any, user: User){
    if(!reportEntity){
      reportEntity = new ReportCacheAdmin()
      Object.assign(reportEntity, {
        data,
        label: label,
        updatedAt: new Date(),
        createdAt: new Date(),
        user,
      })
    }
    reportEntity.data = data
    reportEntity.user = user
    reportEntity.updatedAt = new Date()
    return await this.ReportCacheAdminRepository.save(reportEntity)
  }

  async getFilterByRoleUser(user:User):Promise<FilterDashboard>{
    let roleUser = await this.AdminRoleUserRepository.findOne({
      where: {
        userId: user,
      },
      relations: [
        'roleId'
      ]
    })
    if(!roleUser) throw "No access to admin"

    if(roleUser.roleId.id === ID_ROLEUSER_REPORTER){
      let roleLab = await this.LabRoleUserRepository.find({
        where: {
          userId: user,
        },
        relations: [
          'labId'
        ],
        withDeleted: true
      })

      let roleRef = await this.RefRoleUserRepository.find({
        where: {
          userId: user,
        },
        relations: [
          'refId'
        ],
        withDeleted: true
      })

      return {
        labIds: roleLab ? roleLab.map((v) => v.labId.id) : [],
        refIds: roleRef ? roleRef.map((v) => v.refId.id) : []
      }
    }
    return {}
  }
}
