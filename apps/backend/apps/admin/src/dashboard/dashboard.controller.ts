import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Get, Query } from '@nestjs/common';
import { AdminApi } from '../helper/admin-controller.decorator';
import { RequireAuth } from '../helper/auth.decorator';
import { DashboardService } from './dashboard.service';

@RequireAuth()
@AdminApi('dashboard')
export class DashboardController {
  constructor(
    private dashboardService: DashboardService
  ){}

  @Get('/')
  async getDashboard(
    @Query('isForce') isForce: boolean,
    @UserCred() user: User,
  ){
    try {
      return await this.dashboardService.getDashboard(isForce, user)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/transaction')
  async getTransaction(
    @Query('isForce') isForce: boolean,
    @UserCred() user: User,
  ){
    try{
      let res = await this.dashboardService.getTransaction(isForce, user)
      return res;
    } catch (error){
      console.log(error)
      throw new BadRequestException(error)
    }
  }
}
