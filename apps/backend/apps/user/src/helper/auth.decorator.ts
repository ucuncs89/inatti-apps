import { JwtAuthGuardUser } from "@app/authentication/authorization/jwt.authguard.user";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth } from "@nestjs/swagger";

export function RequireAuth() {
  return applyDecorators(
    UseGuards(JwtAuthGuardUser),
    ApiBearerAuth()
  )
}