import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { User } from '@app/database/entity/user/user.entity';
import { ConfigApp } from '@app/database/entity/config/config-app.entity';
import { ResponseFile } from '@app/helper/response-file/response-file';
import { ReportPdfService } from '@app/report-pdf';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import * as dayjs from 'dayjs';
import { getManager, ILike, Repository } from 'typeorm';
import { ForgetOrderDto } from './type';
import { WhatsappNotifyService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp-notify.service';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    @InjectRepository(ConfigApp)
    private ConfigAppRepository: Repository<ConfigApp>,
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
    private reportPdfService: ReportPdfService,
    private whatsappNotifyService: WhatsappNotifyService,
  ) { }

  /**
   * To search forget order, we must do it manually, search using raw query
   */
  async forgetOrder(payload: ForgetOrderDto) {
    // normalize phone number, if start with 0, remove 0, if start with 62, remove 62, if start with +62, remove +62
    let phoneNumber = payload.phonenumber.replace(/^0+/, '');
    phoneNumber = phoneNumber.replace(/^62/, '');
    phoneNumber = phoneNumber.replace(/^\+62/, '');

    let patients = await this.SwabPatientRepository.find({
      where: {
        nik: payload.nik,
        name: ILike(payload.name),
        phonenumber: ILike(`%${phoneNumber}%`),
      }
    })

    let selectedSwab;
    for (let i = 0; i < patients.length; i++) {
      console.log(`searching for ${patients[i].id}`)
      const patient = patients[i];
      let swab: any[] = await getManager().query(`select id from swab_detail sd where TO_CHAR("requestSwabDate":: DATE, 'yyyy-mm-dd') = $1 and patient_id = $2`,
        [payload.swabDate, patient.id])
      console.log(swab)
      if (swab && swab.length) {
        selectedSwab = swab[0].id
      }
    }

    if (!selectedSwab) {
      throw "Data not found"
    }

    console.log(selectedSwab)
    return await this.whatsappNotifyService.notifyOrder(
      selectedSwab,
      'TEMPLATE_NOTIFY_WA_FORGET_ORDER',
    );
  }

  async listOrder(userId: string) {
    let result = await this.SwabOrderTrxRepository.find({
      where: {
        customer: {
          id: userId,
        },
      },
      relations: ['orderSwab', 'reference', 'product', 'product.product'],
      order: {
        createdAt: 'DESC',
      },
    });
    result.forEach((row) => {
      row.getTotalOrder();
    });
    return result;
  }

  async getDetailOrder(orderId: string) {
    let result = await this.SwabOrderTrxRepository.findOne({
      where: {
        id: orderId,
      },
      relations: [
        'orderSwab',
        'orderSwab.detailSwab',
        'orderSwab.detailSwab.patient',
        'reference',
        'reference.city',
        'reference.province',
        'product',
        'product.product',
      ],
    });
    return result;
  }

  async getListOrderSample(userId: string) {
    return this.SwabOrderRepository.find({
      where: {
        customer: {
          id: userId,
        },
      },
      relations: [
        'detailSwab',
        'detailSwab.patient',
        'reference',
        'reference.city',
        'reference.province',
        'product',
        'product.product',
      ],
    });
  }

  async getDetailOrderSample(idOrderSample: string, userId: string) {
    let result = await this.SwabOrderRepository.findOne({
      where: {
        id: idOrderSample,
        customer: {
          id: userId,
        },
      },
      relations: [
        'detailSwab',
        'detailSwab.patient',
        'reference',
        'reference.city',
        'reference.province',
        'product',
        'product.product',
      ],
    });
    if (!result) {
      throw new Error('Order not found');
    }
  }

  async getOrderPdf(invId: string, res: Response) {
    let orderTrx = await this.SwabOrderTrxRepository.findOne({
      where: {
        invId,
      },
    });
    if (!orderTrx) throw 'Invoice id not found';

    let bufferPdf = await this.reportPdfService.InvoicePage(invId);

    ResponseFile(res, {
      type: 'application/pdf',
      buffer: bufferPdf,
    });
  }

  async assignOrderTouser(orderTrxId: string, userId: string) {
    let orderTrx = await this.SwabOrderTrxRepository.findOne({
      where: {
        id: orderTrxId,
      },
      relations: [
        'customer',
        'orderSwab',
        'orderSwab.customer',
        'orderSwab.detailSwab',
        'orderSwab.detailSwab.OwnerBy',
        'orderSwab.detailSwab.patient',
        'orderSwab.detailSwab.patient.ownedBy',
      ],
    });

    if (!orderTrx) throw 'Order id not found';
    if (orderTrx.customer) {
      throw 'Transaction has been owned';
    }

    let customer = new User(userId);

    orderTrx.customer = customer;
    await this.SwabOrderTrxRepository.save(orderTrx);

    for (const orderSwab of orderTrx.orderSwab) {
      orderSwab.customer = customer;
      await this.SwabOrderRepository.save(orderSwab);

      orderSwab.detailSwab.OwnerBy = customer;
      await this.SwabDetailRepository.save(orderSwab.detailSwab);

      orderSwab.detailSwab.patient.ownedBy = customer;
      await this.SwabPatientRepository.save(orderSwab.detailSwab.patient);
    }

    return orderTrx;
  }

  async searchInvoice(invId: string) {
    let res = await this.SwabOrderTrxRepository.findOne({
      where: {
        // actually shorter inv id is trx id
        trxId: invId,
      },
    });
    if (!res) {
      throw 'Invoice id not found';
    }
    return res;
  }
}
