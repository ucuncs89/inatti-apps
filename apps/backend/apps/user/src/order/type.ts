import { ApiProperty } from '@nestjs/swagger';

export class ForgetOrderDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  nik: string;

  @ApiProperty()
  phonenumber: string;

  @ApiProperty()
  swabDate: string;
}
