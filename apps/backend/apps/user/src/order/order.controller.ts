import { ProductTypeId } from '@app/database/entity/ref/const';
import {
  BadRequestException,
  Controller,
  Body,
  Post,
  Get,
  Param,
  Patch,
  Query,
  Request,
  Response,
} from '@nestjs/common';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { VaksinService } from 'apps/data/src/vaksin/vaksin.service';
import { get } from 'lodash';
import { RequireAuth } from '../helper/auth.decorator';
import { OrderService } from './order.service';
import { ForgetOrderDto } from './type';

@ApiTags('User API')
@Controller('order')
export class OrderController {
  constructor(
    private orderService: OrderService,
    private vaksinService: VaksinService,
  ) { }

  @Post('/forget')
  async forgetOrder(
    @Body()
    formDTO: ForgetOrderDto,
  ) {
    try {
      if (Object.keys(formDTO).length < 1) {
        throw new Error('No input found');
      }
      return await this.orderService.forgetOrder(formDTO);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @RequireAuth()
  @Get('/list')
  async listOrder(@Request() req: any) {
    try {
      return await this.orderService.listOrder(req.user.id);
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error);
    }
  }

  @ApiProperty({
    deprecated: true
  })
  @Get('/detail/:idorder')
  /** @deprecated change to api apps/backend/apps/data/src/transaction/transaction-order.controller.ts */
  async getDetailOrder(@Param('idorder') idorder: string) {
    try {
      let res = await this.orderService.getDetailOrder(idorder);
      // if (this.vaksinService.isProductVaksin(res)) {
      //   res = await this.vaksinService.injectVaksinToOrderTrx(res)
      // }
      return res
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @RequireAuth()
  @Get('/samples-test')
  async getListOrderSample(@Request() req: any) {
    try {
      return await this.orderService.getListOrderSample(req.user.id);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @RequireAuth()
  @Get('/sample-test/:idordersample')
  async getDetailOrderSample(
    @Request() req: any,
    @Param('idordersample') idordersample: string,
  ) {
    try {
      return await this.orderService.getDetailOrderSample(
        idordersample,
        req.user.id,
      );
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @Get('/invoice-pdf/:invId')
  async getInvoicePdf(
    @Param('invId')
    invoiceId: string,
    @Response()
    res: any,
  ) {
    try {
      return await this.orderService.getOrderPdf(invoiceId, res);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @RequireAuth()
  @Patch('/assign-to-me')
  async assignOrderToUser(
    @Query('orderTrxId')
    orderTrxId: string,
    @Request()
    req: any,
  ) {
    try {
      return await this.orderService.assignOrderTouser(orderTrxId, req.user.id);
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }

  @Get('/search-inv')
  async searchInvoice(
    @Query('inv')
    invId: string,
  ) {
    try {
      return await this.orderService.searchInvoice(invId);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
