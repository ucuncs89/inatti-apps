import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PatientService {
  constructor(
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
  ){}

  async getListPatientSwab(userId: string){
    return this.SwabDetailRepository.find({
      where: {
        OwnerBy: {
          id: userId
        }
      },
      relations: [
        'patient',
        'productSwab',
        'refTarget',
        'orderTrx',
        'orderSwab'
      ],
      order: {
        createdAt: 'DESC'
      }
    })
  }

  async getDetailPatientSwab(userId: string, id: number){
    return this.SwabDetailRepository.findOne({
      where: {
        id,
        OwnerBy: {
          id: userId
        }
      },
      relations: [
        'patient',
        'productSwab',
        'refTarget',
        'refTarget.province',
        'refTarget.city',
        'refTarget.shifts',
        'orderSwab',
        'orderSwab.product',
        'orderTrx'
      ]
    })
  }
}
