import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { PatientService } from './patient.service';

@ApiTags('User API')
@RequireAuth()
@Controller('patient')
export class PatientController {
  constructor(
    private patientService: PatientService
  ){}

  @Get('/list')
  async getListPatientSwab(
    @UserCred()
    user: User
  ){
    try {
      return await this.patientService.getListPatientSwab(user.id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/detail/:idswabtest')
  async getDetailPatientSwab(
    @UserCred()
    user: User,
    @Param('idswabtest')
    idSwabtest: string
  ){
    try {
      return await this.patientService.getDetailPatientSwab(user.id, +idSwabtest)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
