import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { RefProduct } from '@app/database/entity/ref/ref-product.entity';
import { RefQuickForm } from '@app/database/entity/ref/ref-quick-form.entity';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { MeasureGeoToMeter } from '@app/helper/utils';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';

@Injectable()
export class FaskesService {
  constructor(
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
    @InjectRepository(RefProduct)
    private RefProductRepository: Repository<RefProduct>,
    @InjectRepository(RefQuickForm)
    private RefQuickFormRepository: Repository<RefQuickForm>,
  ) { }

  async getAllFaskes(
    provinceId?: number,
    cityId?: number,
    productId?: number,
    nearCoordinate?: string,
  ) {
    try {
      const query = this.RefDetailRepository.createQueryBuilder('ref')
        .leftJoinAndSelect('ref.products', 'products')
        .leftJoinAndSelect('products.product', 'detail')
        .leftJoinAndSelect('ref.province', 'province')
        .leftJoinAndSelect('ref.city', 'city')
        .where("ref.deletedAt is null")

      if (provinceId) {
        query.andWhere('ref.provinceId = :provinceId', { provinceId });
      }

      if (cityId) {
        query.andWhere('ref.cityId = :cityId', { cityId });
      }

      if (productId) {
        query.andWhere('detail.id = :productId', { productId });
      }

      let listFaskes: RefDetail | any[] = await query.getMany()

      if (nearCoordinate) {
        let posUser = nearCoordinate.split(',').map((v) => +v)
        let listFaskesTmp = []
        for (let i = 0; i < listFaskes.length; i++) {
          const faskes = listFaskes[i];
          if (!faskes.refAddressPoint) continue;

          let posFaskes = faskes.refAddressPoint.split(',').map((v) => +v)
          let distance = MeasureGeoToMeter(posUser, posFaskes)
          faskes['distanceMeter'] = distance

          // only save to faskes that fill coordiante
          listFaskesTmp.push(faskes)
        }
        // sort from nearby
        listFaskesTmp = listFaskesTmp.sort((a, b) => a['distanceMeter'] - b['distanceMeter'])

        listFaskes = listFaskesTmp
      }

      return listFaskes
    } catch (error) {
      throw error;
    }
  }

  async getTotalFaskes() {
    try {
      return await getManager().query(
        `select p.id, p.name, count(rp2.ref_id) from product p 
      inner join (select ref_id, rp.product_id from ref_product rp group by rp.ref_id, rp.product_id ) rp2 on rp2.product_id = p.id
      group by p.id`)
    } catch (error) {
      console.log(error)
      throw error
    }
  }



  async getDetailFaskes(faskesId: string) {
    try {
      return await this.RefDetailRepository.findOne({
        relations: ['products', 'shifts', 'products.product'],
        where: {
          id: faskesId,
        },
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async getListProductsByFaskesId(faskesId: string, productsId: number) {
    try {
      let where = {
        ref: faskesId,
        product: productsId,
      }

      if (!productsId) delete where.product

      let res = await this.RefProductRepository.find({
        where,
        relations: ['product'],
      });
      let filter = res.filter((v) => !v.isDelete)
      return filter
    } catch (error) {
      throw error;
    }
  }

  async getQuickForm(id: string) {
    try {
      let quickForm = await this.RefQuickFormRepository.findOneOrFail({
        where: {
          id,
        },
        relations: [
          'ref'
        ]
      })
      if (quickForm.expiredAt) {
        let isExpired = new Date().getTime() > quickForm.expiredAt.getTime()
        if (isExpired) {
          throw "Expired"
        }
      }
      return quickForm
    } catch (err) {
      throw "Code quick form not found"
    }
  }
}
