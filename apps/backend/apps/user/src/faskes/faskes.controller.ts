import {
  BadRequestException,
  Controller,
  Get,
  Logger,
  Param,
  Query,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { FaskesService } from './faskes.service';

@ApiTags('Faskes API')
@Controller('faskes')
export class FaskesController {
  constructor(private faskesService: FaskesService) { }

  @Get('/')
  @ApiQuery({
    name: 'product_id',
    type: Number,
    description: 'Product Ref ID',
    required: false,
  })
  @ApiQuery({
    name: 'near',
    type: String,
    description: 'position User, coordinate latlng.',
    example: '-6.915869086504323,107.60025113174055',
    required: false
  })
  async getListFaskes(
    @Query('product_id') productId: number,
    @Query('near') nearCoordinat: string,
  ) {
    try {
      return await this.faskesService.getAllFaskes(
        null,
        null,
        productId,
        nearCoordinat,
      );
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }

  @Get('/count-faskes')
  async getTotalFaskes() {
    try {
      return await this.faskesService.getTotalFaskes()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/:faskes_id')
  @ApiParam({
    name: 'faskes_id',
    type: String,
    description: 'Ref ID / Faskes ID',
    required: true,
  })
  async getDetailFasksesById(@Param('faskes_id') faskes_id: string) {
    try {
      return await this.faskesService.getDetailFaskes(faskes_id);
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }

  @Get('/:faskes_id/products')
  @ApiParam({
    name: 'faskes_id',
    type: String,
    description: 'Ref ID / Faskes ID',
    required: true,
  })
  @ApiQuery({
    name: 'type',
    type: 'number',
    required: false
  })
  async getListProductsByFaskesId(
    @Param('faskes_id') faskes_id: string,
    @Query('type') typeTest?: number,
  ) {
    try {
      const res = await this.faskesService.getListProductsByFaskesId(
        faskes_id,
        typeTest,
      );
      Logger.log(`total product ref ${faskes_id} : ${res.length}`)
      return res;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @Get('/filter/location')
  async FilterFaskesBasedLocation(
    @Query('prov_id')
    provId: string,
    @Query('kab_id')
    kabId: string,
    @Query('product_id')
    productId?: number,
  ) {
    try {
      return await this.faskesService.getAllFaskes(
        provId ? +provId : null,
        kabId ? +kabId : null,
        productId,
      );
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/quick-form/:id')
  async getQuickForm(
    @Param('id')
    id: string
  ) {
    try {
      return await this.faskesService.getQuickForm(id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }


}
