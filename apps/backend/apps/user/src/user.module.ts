import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { DatabaseModule } from '@app/database';
import { TestcovidController } from './testcovid/testcovid.controller';
import { TestcovidService } from './testcovid/testcovid.service';
import { FaskesController } from './faskes/faskes.controller';
import { FaskesService } from './faskes/faskes.service';
import { PaymentFaspayController } from './payment/faspay/faspay.controller';
import { FaspayService } from './payment/faspay/faspay.service';
import { OrderService } from './order/order.service';
import { ReportPdfModule } from '@app/report-pdf';
import { RelationsController } from './relations/relations.controller';
import { RelationsService } from './relations/relations.service';
import { PatientController } from './patient/patient.controller';
import { PatientService } from './patient/patient.service';
import { EmailServiceModule } from '@app/email-service';
import { LoggerActivityModule } from '@app/logger-activity';
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { VaksinModule } from 'apps/data/src/vaksin/vaksin.module';
import { UserAuthModule } from 'apps/data/src/user-auth/user-auth.module';
import { OrderController } from './order/order.controller';

@Module({
  imports: [DatabaseModule, UserAuthModule, ReportPdfModule, EmailServiceModule, LoggerActivityModule.register({ area: 'user' }), WhatsappServiceModule, VaksinModule],
  controllers: [UserController, TestcovidController, FaskesController, PaymentFaspayController, OrderController, RelationsController, PatientController],
  providers: [TestcovidService, FaskesService, FaspayService, OrderService, RelationsService, PatientService],
})
export class UserModule { }
