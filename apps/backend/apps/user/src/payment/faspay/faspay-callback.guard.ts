import { Faspay } from '@app/payment';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class FaspayCallbackGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const body = context.switchToHttp().getRequest().body
    
    let validatedSignature = Faspay.genSignature(body.bill_no+"2")
    
    return body.signature === validatedSignature
  }
}