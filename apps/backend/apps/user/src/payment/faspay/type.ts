import { ApiProperty } from '@nestjs/swagger';

export class NotificationDTO {
  @ApiProperty()
  request: string;
  @ApiProperty()
  trx_id: string;
  @ApiProperty()
  merchant_id: string;
  @ApiProperty()
  merchant: string;
  @ApiProperty()
  bill_no: string;
  @ApiProperty()
  payment_reff: string;
  @ApiProperty()
  payment_date: string;
  @ApiProperty()
  payment_status_code: string;
  @ApiProperty()
  payment_status_desc: string;
  @ApiProperty()
  bill_total: string;
  @ApiProperty()
  payment_total: string;
  @ApiProperty()
  payment_channel_uid: string;
  @ApiProperty()
  payment_channel: string;
  @ApiProperty()
  signature: string;
}
