import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../../helper/auth.decorator';
import { FaspayCallbackGuard } from './faspay-callback.guard';
import { FaspayService } from './faspay.service';
import { NotificationDTO } from './type';

@ApiTags('Faspay API')
@Controller('/payment/faspay')
export class PaymentFaspayController {
  constructor(private faspayService: FaspayService) {}

  @Get('channel')
  async getChannelFaspay() {
    try {
      return this.faspayService.getChannelFaspay();
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  @UseGuards(FaspayCallbackGuard)
  @HttpCode(200)
  @Post('notification')
  async paymentNotification(
    @Body()
    notification: NotificationDTO,
  ) {
    try {
      return this.faspayService.paymentNotification(notification);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  @RequireAuth()
  @Post('cancel/:trx_id')
  @ApiParam({
    name: 'trx_id',
    type: String,
    description: 'Transaction ID',
    required: true,
  })
  async cancelPayment(@Param('trx_id') trx_id: string, @Req() req: any) {
    try {
      return await this.faspayService.cancelPaymentService(trx_id, req.user.id);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @RequireAuth()
  @Get('check/:trx_id')
  @ApiParam({
    name: 'trx_id',
    type: String,
    description: 'Check Transaction ID',
    required: true,
  })
  async checkPayment(@Param('trx_id') trx_id: string) {
    try {
      return await this.faspayService.checkStatusPayments(trx_id);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
