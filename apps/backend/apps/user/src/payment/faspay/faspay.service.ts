import { FaspayMerchant } from '@app/database/entity/faspay/faspay-merchant.entity';
import { FaspayTransaction } from '@app/database/entity/faspay/faspay-transaction.entity';
import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { StatusOrderEnum } from '@app/database/entity/swab-data/type';
import { DateAsiaFormat } from '@app/helper/utils/time';
import { LoggerActivityService } from '@app/logger-activity';
import { LoggerActivityList } from '@app/logger-activity/logger-type';
import { Faspay } from '@app/payment';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NotificationDTO } from './type';

@Injectable()
export class FaspayService {
  constructor(
    @InjectRepository(FaspayMerchant)
    private FaspayMerchantRepository: Repository<FaspayMerchant>,
    @InjectRepository(SwabOrderTrx)
    private SwabOrderTrxRepository: Repository<SwabOrderTrx>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    @InjectRepository(FaspayTransaction)
    private FaspayTransactionRepository: Repository<FaspayTransaction>,
    private activity: LoggerActivityService
  ) {}

  async getChannelFaspay() {
    const merchant = await this.FaspayMerchantRepository.findOne({
      where: {
        slug: 'default',
      },
    });

    let faspay = new Faspay();
    return faspay.getPaymentChannel(merchant.merchantId + '');
  }

  async paymentNotification(payload: NotificationDTO) {
    const isSuccess = payload.payment_status_code == '2';
    const statusOrder = isSuccess
      ? StatusOrderEnum.paid
      : StatusOrderEnum.paymentFailed;

    let faspayTrx = await this.FaspayTransactionRepository.findOne({
      where: {
        transactionFaspayId: payload.trx_id,
      },
      relations: ['orderTrx'],
    });
    if (!faspayTrx) {
      throw new Error('No transaction found');
    }

    faspayTrx.orderTrx.status = statusOrder;
    await this.SwabOrderTrxRepository.save(faspayTrx.orderTrx);

    await this.SwabOrderRepository.update(
      { orderTrx: faspayTrx.orderTrx },
      {
        status: statusOrder,
      },
    );

    let listOrder = await this.SwabOrderRepository.find({
      where: {
        orderTrx: faspayTrx.orderTrx,
      },
      relations: [
        'detailSwab',
        'orderTrx'
      ]
    })

    for (let i = 0; i < listOrder.length; i++) {
      const order = listOrder[i];
      
      await this.activity.log({
        type: LoggerActivityList.user.updateOrderStatus,
        params: {
          orderTrxId: order.orderTrx.id,
          status: statusOrder
        },
        swabDetail: order.detailSwab,
        user: order.customer
      })
    }


    return {
      response: 'Payment Notification',
      trx_id: faspayTrx.transactionFaspayId,
      merchant_id: payload.merchant_id,
      merchant: payload.merchant,
      bill_no: payload.bill_no,
      response_code: '00',
      response_desc: 'Success',
      response_date: DateAsiaFormat(new Date()),
    };
  }

  async cancelPaymentService(trx_id: string, user_id: string) {
    try {
      const merchant = await this.FaspayMerchantRepository.findOne({
        where: {
          slug: 'default',
        },
      });

      const faspayTrx = await this.FaspayTransactionRepository.findOne({
        where: {
          transactionFaspayId: trx_id,
        },
        relations: ['orderTrx'],
      });

      const faspay = new Faspay();

      const response = await faspay.cancelPayment(
        merchant.merchantId + '',
        faspayTrx.transactionFaspayId,
        faspayTrx.orderTrx.trxId,
      );

      if (response.payment_status_code === '7') {
        // canceled;
        await this.SwabOrderTrxRepository.update(
          { trxId: response.bill_no },
          {
            status: StatusOrderEnum.canceled,
          },
        );

        await this.SwabOrderRepository.update(
          { orderTrx: faspayTrx.orderTrx },
          { status: StatusOrderEnum.canceled },
        );

        return {
          message: 'Payment has been Canceled',
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async checkStatusPayments(trx_id: string) {
    try {
      const merchant = await this.FaspayMerchantRepository.findOne({
        where: {
          slug: 'default',
        },
      });

      const faspayTrx = await this.FaspayTransactionRepository.findOne({
        where: {
          transactionFaspayId: trx_id,
        },
        relations: ['orderTrx'],
      });

      const faspay = new Faspay();

      const faspayResponse = await faspay.checkPayments(
        merchant.merchantId + '',
        faspayTrx.transactionFaspayId,
        faspayTrx.orderTrx.trxId,
      );

      let statusMessage = null;

      switch (faspayResponse.payment_status_code) {
        case '0':
          statusMessage = StatusOrderEnum.new;
          break;

        case '1':
          statusMessage = StatusOrderEnum.inProgress;
          break;

        case '2':
          statusMessage = StatusOrderEnum.paymentReceived;
          break;

        case '4':
        case '8':
          statusMessage = StatusOrderEnum.canceled;
          break;

        case '5':
          statusMessage = StatusOrderEnum.paymentFailed;
          break;

        case '9':
          statusMessage = StatusOrderEnum.paymentFailed;
          break;

        default:
          break;
      }

      if (faspayResponse.response_code === '00') {
        await this.SwabOrderTrxRepository.update(
          { trxId: faspayResponse.bill_no },
          {
            status: statusMessage,
          },
        );

        await this.SwabOrderRepository.update(
          { orderTrx: faspayTrx.orderTrx },
          { status: statusMessage },
        );

        return faspayResponse;
      } else {
        return { message: 'Something went wrong' };
      }
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
