import { ConfigApp } from '@app/database/entity/config/config-app.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { User } from '@app/database/entity/user/user.entity';
import { EmailServiceService } from '@app/email-service';
import { WhatsappNotifyService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp-notify.service';
import { WhatsappService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp.service';
import { LoggerActivityService } from '@app/logger-activity';
import { ReportPdfService } from '@app/report-pdf';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { TestCovidWithFaspay } from './testcovid-faspay';
import { RegisterPatientDTO } from './type';

@Injectable()
export class TestcovidService {
  constructor(
    @InjectRepository(User)
    private UserRepository: Repository<User>,
    private reportPdfService: ReportPdfService,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
    private whatsappNotifyService: WhatsappNotifyService,
    private activity: LoggerActivityService
  ) { }

  async registerTestCovid(payload: RegisterPatientDTO) {
    try {
      const register = new TestCovidWithFaspay(payload, this.activity);
      return await register.register();
    } catch (err) {
      throw err;
    }
  }

  async registerTestCovidLoggedinUser(paylod: RegisterPatientDTO, userId?: string) {
    try {
      let user:null|any
      if(userId){
        user = await this.UserRepository.findOneOrFail({ id: userId });
      }
      // Process testcovid using faspay
      const register = new TestCovidWithFaspay(paylod, this.activity);
      
      register.cbCreateNotification = (swabDetail:SwabDetail) => {
        console.log('called cbCreateNotification', swabDetail.id ?? null)
        return this.whatsappNotifyService.notifyOrder(swabDetail.id, 'TEMPLATE_NOTIFY_SUCCESS_REGISTER_TEST_WA')
      }

      return await register.register(user);
    } catch (err) {
      throw err;
    }
  }

  async getOwnerInfo(userId: string) {
    return this.UserRepository.findOneOrFail({
      select: [
        'id',
        'fullname',
        'username',
        'email',
        'phoneNumber',
      ],
      where: { id: userId },
    });
  }


  /**
   * This API should take data using orderSwabId to make it more secure
   */
  async printCertificate(orderSwabId: string) {
    let orderSwab = await this.SwabOrderRepository.findOne({
      where: {
        id: orderSwabId,
      },
      relations: [
        'detailSwab',
        'detailSwab.productSwab',
        'detailSwab.labTarget',
        'detailSwab.refTarget',
      ]
    })
    if(!orderSwab) throw new Error('Order Swab not found')
    
    const PRODUCT_PCR = 1;
    const PRODUCT_ANTIGEN = 2;

    if (orderSwab.detailSwab.productSwab.id === PRODUCT_PCR) {
      if (!orderSwab.detailSwab.labTarget) throw new Error('Lab target not found');
      const filePdf: Buffer = await this.reportPdfService.ResultTest({
        type: 'pcr',
        src: 'ref', // print based on ref pdf
        idSrc: orderSwab.detailSwab.refTarget.id,
        idSwab: orderSwab.detailSwab.id,
      })
      return filePdf
    }
    if (orderSwab.detailSwab.productSwab.id === PRODUCT_ANTIGEN) {
      if (!orderSwab.detailSwab.refTarget) throw new Error('Ref target not found');
      const filePdf: Buffer = await this.reportPdfService.ResultTest({
        type: 'antigen',
        src: 'ref',
        idSrc: orderSwab.detailSwab.refTarget.id,
        idSwab: orderSwab.detailSwab.id,
      })
      return filePdf
    }
  }
}
