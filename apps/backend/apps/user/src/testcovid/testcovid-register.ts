import { User } from "@app/database/entity/user/user.entity";
import { EntityManager, getManager } from "typeorm";
import { RegisterPatientDTO } from "./type";
import * as dayjs from 'dayjs'
import { SwabOrderTrx } from "@app/database/entity/swab-data/order-trx.entity";
import { RefDetail } from "@app/database/entity/ref/ref-detail.entity";
import { RefProduct } from "@app/database/entity/ref/ref-product.entity";
import { MethodPaymentEnum, StatusOrderEnum, StatusSwabEnum } from "@app/database/entity/swab-data/type";
import { SwabOrder } from "@app/database/entity/swab-data/order.entity";
import { SwabPatient } from "@app/database/entity/swab-data/patient.entity";
import { SwabDetail } from "@app/database/entity/swab-data/swab-detail.entity";
import { Product } from "@app/database/entity/ref/product.entity";
import { FaspayMerchant } from "@app/database/entity/faspay/faspay-merchant.entity";
import { Logger } from "@nestjs/common";
import { DateAsiaFormat } from "@app/helper/utils/time";
import { genInvoiceId, genTransactionId } from "@app/helper/utils/invoice";
import { LoggerActivityService } from "@app/logger-activity";
import { LoggerActivityList } from "@app/logger-activity/logger-type";

export interface PriceProduct { item: number, total: number, fee?: number }

/**
 * @deprecated This transaction only for testcovid
 * other type more elegant is @file apps/backend/apps/data/src/transaction
 */
export class TestCovidRegister {
  constructor(
    readonly payload: RegisterPatientDTO,
    private activity: LoggerActivityService
  ) { }
  protected logger = new Logger('TestCovidRegisterClass')
  protected _RefProduct: RefProduct;
  protected OrderTrx: SwabOrderTrx;
  protected Customer: User;
  protected Price: PriceProduct
  protected entityMng: EntityManager;
  protected Merchant: FaspayMerchant

  // callback
  cbCreateNotification: (swabDetail: SwabDetail) => Promise<any>

  getExpireDate() {
    let now = dayjs()
    return now.add(3, 'hour')
  }

  async createInvoice(entity: EntityManager, user: User, price: PriceProduct) {
    const trxId = genTransactionId();
    const invId = genInvoiceId(trxId, this.payload.refTarget.productId);

    let orderTrx = new SwabOrderTrx();
    orderTrx.invId = invId;
    orderTrx.trxId = trxId;
    orderTrx.customer = user
    orderTrx.reference = new RefDetail(this.payload.refTarget.refId)
    orderTrx.product = new RefProduct(+this.payload.refTarget.productId)
    orderTrx.expiredAt = new Date(DateAsiaFormat(this.getExpireDate()))

    orderTrx.method = this.payload.method
    orderTrx.status = StatusOrderEnum.new // if cash
    if (this.payload.method === MethodPaymentEnum.transfer) {
      orderTrx.status = StatusOrderEnum.inProgress // if transfer
      orderTrx.paymentChannelCode = this.payload.paymentChannel.id
      orderTrx.paymentChannelName = this.payload.paymentChannel.name
    }

    orderTrx.totalPrice = price.total
    orderTrx.feeAdmin = price.fee

    let saved = await entity.save(SwabOrderTrx, orderTrx)

    // fetch detail information, because I dont belive `entity.save`
    orderTrx = await entity.findOne(SwabOrderTrx, {
      where: {
        id: saved.id
      },
      relations: [
        'customer',
        'reference',
        'product',
      ]
    })
    return orderTrx;
  }

  async getPrice(entity: EntityManager): Promise<PriceProduct> {
    let refProduct = await this.getProduct(entity);
    const productPrice = refProduct.price * this.payload.patients.length

    let fee = 0
    if (this.payload.method === MethodPaymentEnum.transfer) {
      fee = getFeeFaspay(this.payload.paymentChannel.id, productPrice)
    }

    let total = productPrice + fee

    return {
      item: refProduct.price,
      fee,
      total,
    }
  }

  async getProduct(entity: EntityManager) {
    if (!this._RefProduct) {
      this._RefProduct = await entity.findOne(RefProduct, { id: +this.payload.refTarget.productId })
    }
    return this._RefProduct;
  }

  async createOrderPatients(entity: EntityManager, orderTrx: SwabOrderTrx, price: PriceProduct) {
    let listPatients: SwabPatient[] = []
    for (const patient of this.payload.patients) {
      let swabPatient: SwabPatient;

      // actually all process right now create a new patient
      if (!patient.id) {
        swabPatient = new SwabPatient();
        Object.assign(swabPatient, patient);
        swabPatient.ownedBy = this.Customer as User

        swabPatient = await entity.save(SwabPatient, swabPatient)
      } else {
        swabPatient = await entity.findOne(SwabPatient, {
          where: {
            id: patient.id
          }
        })
      }

      let swabDetail = new SwabDetail();
      swabDetail.orderTrx = orderTrx;
      swabDetail.patient = swabPatient
      swabDetail.refTarget = new RefDetail(this.payload.refTarget.refId);
      swabDetail.productSwab = new Product(+this.payload.testcovid.type);
      swabDetail.requestSwabDate = new Date(this.payload.testcovid.date);
      swabDetail.status = StatusSwabEnum.registered;
      swabDetail.OwnerBy = this.Customer as User;

      swabDetail = await entity.save(swabDetail);
      swabPatient.detailSwab = [swabDetail]

      listPatients.push(swabPatient)

      let order = new SwabOrder();
      order.price = price.item;
      order.customer = orderTrx.customer;
      order.method = orderTrx.method;
      order.orderTrx = orderTrx;
      order.product = new RefProduct(+this.payload.refTarget.productId)
      order.reference = new RefDetail(this.payload.refTarget.refId)
      order.status = StatusOrderEnum.new
      order.detailSwab = swabDetail

      order = await entity.save(order);
      swabDetail.orderSwab = order;
      await entity.save(swabDetail);
    }
    return listPatients
  }

  async register(user?: User) {
    this.logger.debug(`Payload : ${JSON.stringify(this.payload)}`)
    this.logger.verbose(`Ready to register`)

    let listPatients: SwabPatient[]
    try {
      let res = await getManager().transaction(async entity => {
        this.entityMng = entity;
        this.Price = await this.getPrice(entity);
        this.logger.verbose(`Price product get`)

        this.Customer = user;

        this.logger.verbose(`Ready to create invoice`)
        this.OrderTrx = await this.createInvoice(entity, this.Customer, this.Price);
        this.logger.verbose(`Finish Create invoice id : ${this.OrderTrx.invId}, id : ${this.OrderTrx.id}`)

        this.logger.verbose(`Ready to create order patient`)
        listPatients = await this.createOrderPatients(entity, this.OrderTrx, this.Price)
        this.logger.verbose(`Success create order patient`)

        let customerTrx = {
          id: '',
          fullname: '',
          email: '',
          phoneNumber: ''
        }

        if (!this.Customer) {
          let firstPatient = listPatients[0]
          customerTrx = {
            id: firstPatient.id,
            fullname: firstPatient.name,
            email: firstPatient.email,
            phoneNumber: firstPatient.phonenumber
          }
        } else {
          Object.assign(customerTrx, this.Customer)
        }


        if (this.payload.method === MethodPaymentEnum.transfer) {
          this.logger.verbose(`Ready to send to faspay`)
          let trxFaspay = await this.sendTransaction(entity, {
            customer: customerTrx,
            orderTrx: this.OrderTrx,
            price: this.Price
          })
          this.logger.verbose(`Success send trx to faspay : ${trxFaspay}`)
          return trxFaspay
        }
        this.logger.verbose(`Success create transaction`)

        return this.OrderTrx
      })

      for (let i = 0; i < listPatients.length; i++) {
        const patient = listPatients[i];
        await this.activity.log({
          type: LoggerActivityList.user.createOrder,
          params: {
            orderId: this.OrderTrx.id,
            patient: patient.name
          },
          user: this.Customer,
          swabDetail: patient.detailSwab[0].id,
        })

        // notify patient
        await this.cbCreateNotification(patient.detailSwab[0])
      }

      return res;
    } catch (err) {
      console.log(err);
      this.logger.error(`failed to register user : ${err.toString()}`)
      throw err
    }
  }

  sendTransaction: (entity: EntityManager, params: {
    customer: {
      id: string,
      fullname: string,
      email: string,
      phoneNumber: string
    }, orderTrx: SwabOrderTrx, price: PriceProduct
  }) => any
}


export function getFeeFaspay(code: string, totalPay: number) {
  const fee = {
    801: (_i) => 7500,
    825: (_i) => 7500,
    701: (_i) => 7500,
    708: (_i) => 7500,
    302: (_i) => 7500,
    802: (_i) => 7500,
    814: (_i) => 7500,
    408: (_i) => 7500,
    402: (_i) => 7500,
    713: (i) => ((i * 1.5 * 10 / 100) + 3000),
    711: (i) => ((i * 0.7 * 10 / 100) + 3000),
    818: (_i) => 7500
  }
  return fee[code](totalPay)
}