import { MethodPaymentEnum } from '@app/database/entity/swab-data/type';
import { ApiProperty } from '@nestjs/swagger';
import { PatientDTO } from 'apps/ref/src/patient-swab/type';

export class TestInfo {
  @ApiProperty({
    description: 'Format : MM-DD-YYYY',
  })
  date: Date;

  @ApiProperty({
    example: '0',
    description: 'id test: PCR = 0, antigen = 1',
  })
  type: number; // id product
}

export class RegistererDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  email: string;
}

export class RefDTO {
  @ApiProperty()
  refId: string;

  @ApiProperty()
  productId: string;
}

export class PaymentChannelDTO {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;
}

export class RegisterPatientDTO {
  @ApiProperty({ type: TestInfo })
  testcovid: TestInfo;

  @ApiProperty({ type: PatientDTO, isArray: true })
  patients: PatientDTO[];

  @ApiProperty()
  refTarget: RefDTO;

  @ApiProperty({
    enum: MethodPaymentEnum,
  })
  method?: MethodPaymentEnum;

  @ApiProperty({
    description: 'If method = transfer, fill this',
  })
  paymentChannel?: PaymentChannelDTO;
}
