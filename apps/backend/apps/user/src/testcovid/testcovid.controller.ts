import { ResponseFile } from '@app/helper/response-file/response-file';
import { BadRequestException, Body, Controller, Get, Param, Post, Request, Response } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TestcovidService } from './testcovid.service';
import { RegisterPatientDTO } from './type';
import * as Express from 'express';
import { RequireAuth } from '../helper/auth.decorator';

@ApiTags('User API')
@Controller('/testcovid')
export class TestcovidController {
  constructor(
    private readonly testcovid: TestcovidService
  ) { }

  /**
   * This route is not require authentication
   */
  @RequireAuth()
  @Post('register-loggedin')
  async regsiterLoggedinUser(
     @Body()
     formDTO: RegisterPatientDTO,
     @Request()
     req: any
   ) {
     try {
       if (Object.keys(formDTO).length < 1) {
         throw new Error('No input found')
       }
       // Authentication is not required
       let userId = req?.user?.id ?? null
 
       let res = await this.testcovid.registerTestCovidLoggedinUser(formDTO, userId);
       if(!res){
         throw "Failed to register patient"
       }
       return res;
     } catch (err) {
       console.log(err)
       throw new BadRequestException(err)
     }
   }
 

  /**
   * This route is not require authentication
   */
  @Post('register')
  async regsiterNotLoggedinUser(
    @Body()
    formDTO: RegisterPatientDTO,
  ) {
    try {
      if (Object.keys(formDTO).length < 1) {
        throw new Error('No input found')
      }

      // No user id
      let res = await this.testcovid.registerTestCovidLoggedinUser(formDTO, null);
      if(!res){
        throw "Failed to register patient"
      }
      return res;
    } catch (err) {
      console.log(err)
      throw new BadRequestException(err)
    }
  }

  @RequireAuth()
  @Get('owner-info')
  async getOwnerInfo(@Request() req: any) {
    try {
      return await this.testcovid.getOwnerInfo(req.user.id)
    } catch (err) {
      return new BadRequestException(err)
    }
  }

  @Get('print-certificate/:orderswabId')
  async printCertificate(
    @Param('orderswabId')
    orderswabId: string,
    @Response()
    res: Express.Response
  ) {
    try {
      let file: Buffer = await this.testcovid.printCertificate(orderswabId)
      ResponseFile(res, {
        type: 'application/pdf',
        buffer: file
      })
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }
}

