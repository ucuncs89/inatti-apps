import { FaspayMerchantRef } from '@app/database/entity/faspay/faspay-merchant-ref.entity';
import { FaspayMerchant } from '@app/database/entity/faspay/faspay-merchant.entity';
import { FaspayTransaction } from '@app/database/entity/faspay/faspay-transaction.entity';
import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { SwabOrderTrx } from '@app/database/entity/swab-data/order-trx.entity';
import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { User } from '@app/database/entity/user/user.entity';
import { DateAsiaFormat } from '@app/helper/utils/time';
import { LoggerActivityService } from '@app/logger-activity';
import {
  Faspay,
  FaspayProductItem,
  FasPayRequestTrxDTO,
  SuccessCreateTrxDTO,
} from '@app/payment';
import { EntityManager } from 'typeorm';
import {
  PriceProduct,
  TestCovidRegister,
} from './testcovid-register';
import { RegisterPatientDTO } from './type';

/**
 * @deprecated this will moved to apps/backend/apps/data/src/transaction/transaction-faspay.service.ts
 */
export class TestCovidWithFaspay
  extends TestCovidRegister {
  constructor(readonly payload: RegisterPatientDTO, activity: LoggerActivityService) {
    super(payload, activity);
  }

  async getMerchantFaspay(entity: EntityManager): Promise<FaspayMerchant> {
    let merchantRef = await entity.findOne(FaspayMerchantRef, {
      where: {
        ref: new RefDetail(this.payload.refTarget.refId),
      },
      relations: ['merchant'],
    });
    // default merchant with slug 'default'
    if (!merchantRef) {
      let merchant = await entity.findOne(FaspayMerchant, {
        where: {
          slug: 'default',
        },
      });
      if (!merchant) {
        throw new Error(`Merchant default not found`);
      }
      return merchant;
    }
    return merchantRef.merchant;
  }

  async getItemsFaspay(merchantId): Promise<any[]> {
    let item = new FaspayProductItem();
    const product = await this.getProduct(this.entityMng);

    item.product = product.name;
    item.amount = product.price.toString();
    item.qty = '1';
    item.merchant_id = merchantId;

    return [item];
  }

  async saveFaspayTransaction(
    entity: EntityManager,
    trx: { orderTrx: SwabOrderTrx; faspayTrx: string },
    api: { request: FasPayRequestTrxDTO; response: SuccessCreateTrxDTO },
  ) {
    const faspayTrx = new FaspayTransaction();

    faspayTrx.orderTrx = trx.orderTrx;
    faspayTrx.transactionFaspayId = trx.faspayTrx;
    faspayTrx.requestApi = api.request;
    faspayTrx.responseApi = api.response;

    return await entity.save(faspayTrx);
  }

  sendTransaction = async (
    entity: EntityManager,
    params: {
      customer: {
        id: string,
        fullname: string,
        email: string,
        phoneNumber: string
      };
      orderTrx: SwabOrderTrx;
      price: PriceProduct
    },
  ) => {
    this.Merchant = await this.getMerchantFaspay(entity);

    const faspay = new Faspay();
    try {
      const payloadFaspay = {
        bill_date: DateAsiaFormat(params.orderTrx.createdAt),
        bill_desc: `Pembayaran #${params.orderTrx.trxId}`,
        merchant_id: this.Merchant.merchantId.toString(),
        merchant: this.Merchant.merchantName.toString(),
        bill_no: params.orderTrx.trxId,
        bill_expired: DateAsiaFormat(params.orderTrx.expiredAt),
        bill_total: params.orderTrx.totalPrice + '',
        cust_no: params.customer.id,
        cust_name: params.customer.fullname,
        payment_channel: this.payload.paymentChannel.id,
        msisdn: params.customer.phoneNumber,
        email: params.customer.email,
        item: await this.getItemsFaspay(this.Merchant.merchantId.toString()),
      };
      this.logger.verbose(`Payload to faspay : ${payloadFaspay}`);
      const resFaspay = await faspay.createTransaction(payloadFaspay);

      await this.saveFaspayTransaction(
        entity,
        {
          orderTrx: params.orderTrx,
          faspayTrx: resFaspay.trx_id,
        },
        {
          request: payloadFaspay,
          response: resFaspay,
        },
      );

      return resFaspay;
    } catch (err) {
      throw err;
    }
  }
}
