import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { BadRequestException, Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { RelationsService } from './relations.service';

@ApiTags('User API')
@RequireAuth()
@Controller('relations')
export class RelationsController {
  constructor(
    private relationsService: RelationsService
  ){}

  @Get('/list')
  async getListRelations(
    @UserCred()
    user: User
  ){
    try {
      return await this.relationsService.getListRelations(user.id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/detail-swab/:idpatient')
  async detailSwabRelations (
    @UserCred()
    user: User,
    @Param('idpatient')
    idPatient:string
  ){
    try {
      return await this.relationsService.detailSwabRelations(user.id, idPatient)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/detail-info/:idpatient')
  async detailInfoRelations (
    @UserCred()
    user: User,
    @Param('idpatient')
    idPatient:string
  ){
    try {
      return await this.relationsService.detailInfoRelations(user.id, idPatient)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/search-nik')
  async searchNikRelations (
    @UserCred()
    user: User,
    @Query('nik')
    nik:string
  ){
    try {
      return await this.relationsService.searchNikRelations(user.id, nik)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
