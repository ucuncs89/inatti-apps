import { SwabPatient } from '@app/database/entity/swab-data/patient.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RelationsService {
  constructor(
    @InjectRepository(SwabPatient)
    private SwabPatientRepository: Repository<SwabPatient>,
  ){}

  async getListRelations(userId: string){
    return this.SwabPatientRepository.find({
      where: {
        ownedBy: {
          id: userId
        }
      },
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'detailSwab',
      ]
    });
  }

  async detailSwabRelations(userId: string, patientId: string){
    let result = await this.SwabPatientRepository.findOne({
      where: {
        id: patientId,
        ownedBy: {
          id: userId
        }
      },
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'detailSwab',
        'detailSwab.refTarget',
        'detailSwab.orderSwab',
        'detailSwab.orderSwab.product',
      ]
    });
    if(!result){
      throw new Error("Patient not found");
    }
    return result;
  }

  async detailInfoRelations(userId: string, patientId: string){
    let result = await this.SwabPatientRepository.findOne({
      where: {
        id: patientId,
        ownedBy: {
          id: userId
        }
      },
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'domisiliProvinsi',
        'domisiliKabupaten',
        'domisiliKecamatan',
        'domisiliKelurahan',
      ]
    });
    if(!result){
      throw new Error("Patient not found");
    }
    return result;
  }

  async searchNikRelations(userId: string, nik: string){
    let result = await this.SwabPatientRepository.findOne({
      where: {
        nik,
        ownedBy: {
          id: userId
        }
      },
      order: {
        createdAt: 'DESC'
      },
      relations: [
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'domisiliProvinsi',
        'domisiliKabupaten',
        'domisiliKecamatan',
        'domisiliKelurahan',
      ]
    });
    if(!result){
      throw "Nik not found";
    }
    return result;
  }
}
