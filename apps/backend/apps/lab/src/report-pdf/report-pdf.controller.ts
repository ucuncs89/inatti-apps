import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { ResponseFile } from '@app/helper/response-file/response-file';
import { ReportPdfService } from '@app/report-pdf';
import { LocalPdfAuthService } from '@app/report-pdf/local-pdf-auth/local-pdf-auth.service';
import { BadRequestException, Controller, Get, Logger, Query, Request, Response, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import * as Express from 'express';
import { RequireAuth } from '../helper/auth.decorator';
import { LabReportService } from './lab-report.service';

@Controller('report-pdf')
export class ReportPdfController {
  constructor(
    private reportPdfService: ReportPdfService,
    private labReportService: LabReportService
  ) { }

  logger = new Logger('Lab/ReportPdfController')

  @ApiQuery({
    name: 'formatRef',
    required: false,
    description: 'if true return pdf file with ref format',
    type: Boolean,
    example: false
  })
  @ApiTags("Lab API")
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardLab)
  @Get('download-sertifikat')
  async downloadCertificate(
    @Query('idlab')
    idLab: string,
    @Query('idswab')
    idSwab: string,
    @Query('formatRef')
    formatRef: boolean,
    @Response()
    res: Express.Response
  ) {
    try {
      const filePdf: Buffer = await this.reportPdfService.ResultTest({
        type: 'pcr',
        src: formatRef ? 'ref' : 'lab',
        idSrc: idLab,
        idSwab: idSwab,
      })
      ResponseFile(res, {
        type: 'application/pdf',
        buffer: filePdf
      })
    } catch (error) {
      console.log(error);

      throw new BadRequestException(error)
    }
  }

  @ApiQuery({
    name: 'idswab',
    description: 'can be separate using comma. Ex: 31,32,33'
  })
  @RequireAuth()
  @Get('download-sertifikat/zip')
  async downloadCertificateZip(
    @Query('idlab')
    idLab: string,
    @Query('idswab')
    idSwab: string,
    @Response()
    res: Express.Response,
    @Request()
    req: Express.Request
  ) {
    req.setTimeout(360000) // 5 minutes
    let listIdSwab = idSwab.split(',')

    this.logger.log(`Download certificate zip, total id swab : ${listIdSwab.length}`)

    try {
      const MAX_DATA = 20

      if (listIdSwab.length > MAX_DATA) {
        throw `Maksimal ${MAX_DATA} data untuk di generate`
      }
      return await this.reportPdfService.ResultTestZip({
        type: 'pcr',
        idSwab: listIdSwab,
        idSrc: idLab,
      }, res)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @ApiTags("Lab API")
  @UseGuards(LocalPdfAuthService)
  @Get('swab-detail')
  async getSwabDetail(
    @Query('idswab')
    idSwab: number,
  ) {
    try {
      const res = await this.labReportService.getSwabDetail(idSwab)
      return res;
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @ApiTags("Lab API")
  @UseGuards(LocalPdfAuthService)
  @Get("template")
  async getTemplate(
    @Query('idswab')
    idswab: string
  ) {
    try {
      return await this.labReportService.getTemplateFormat(idswab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/sample/ctvalue')
  async getSampleCtValue(
    @Query('idswab')
    idswab: string
  ) {
    try {
      return await this.labReportService.getSampleCtValue(idswab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
