import { LabAnalyzeResult } from '@app/database/entity/mapping/analyze/lab-analyze-result.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { LabTemplateSertifikat } from '@app/database/entity/template-map/lab-template-sertifikat.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class LabReportService {
  constructor(
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(LabTemplateSertifikat)
    private LabTemplateSertifikatRepository: Repository<LabTemplateSertifikat>,
    @InjectRepository(LabAnalyzeResult)
    private LabAnalyzeResultRepository: Repository<LabAnalyzeResult>,
  ) { }

  getSwabDetail(idSwab: number) {
    return this.SwabDetailRepository.findOne({
      where: {
        id: idSwab
      },
      relations: [
        'patient',
        'refTarget',
        'orderSwab',
        'labTarget',
        'labAnalyzeSession',
        'labAnalyzeSession.resultTool',
      ],
      withDeleted: true
    })
  }

  async getTemplateFormat(idswab: string) {
    let swab = await this.SwabDetailRepository.findOne({
      where: {
        id: idswab
      },
      relations: [
        'labTarget'
      ]
    })
    return this.LabTemplateSertifikatRepository.findOne({
      where: {
        lab: swab.labTarget
      }
    })
  }

  getSampleCtValue(idswab: string) {
    let result = this.LabAnalyzeResultRepository.findOne({
      where: {
        sampleSession: {
          sample: {
            id: idswab
          }
        }
      },
      relations: [
        'sampleSession',
        'sampleSession.sample',
      ]
    })
    return result
  }
}
