import { ResultSwabEnum } from "@app/database/entity/swab-data/type";
import { ApiProperty } from "@nestjs/swagger";
import { ExtractionMappingDTO, ExtractionSessionCreateDTO, MapUpdatePositionDTO } from "../extraction/type";

export class AnalyzeMappingDTO extends ExtractionMappingDTO { }

export class AnalyzeSessionCreateDTO extends ExtractionSessionCreateDTO {
  @ApiProperty({ isArray: true, type: AnalyzeMappingDTO })
  mapping: AnalyzeMappingDTO[]

  @ApiProperty({ description: "id Tool analyze" })
  tool: number;
}

export class AnalyzeResultAddDTO {
  @ApiProperty()
  idLab: string
  @ApiProperty()
  idSession: number;
}

export class UpdateResultByDoctorDTO {
  @ApiProperty({nullable: true})
  sessionDetailId?: number;
  @ApiProperty({ enum: ResultSwabEnum, type: "enum" })
  result: ResultSwabEnum
  @ApiProperty()
  note?: string
  @ApiProperty()
  asFinalResult?: boolean
}

export class UpdateResultToolDTO {
  @ApiProperty()
  id: number;
  @ApiProperty()
  resultSummary: string;
}

export class UpdateSessionPositionDTO implements MapUpdatePositionDTO{
  @ApiProperty()
  new: AnalyzeMappingDTO[]
  @ApiProperty()
  del: AnalyzeMappingDTO[]
  @ApiProperty()
  stay: AnalyzeMappingDTO[]
}

export type UpdateFinalResultDTO = {
  [idSample: number]: string
}

export class BulkUpdateResulBydoctorDTO {
  @ApiProperty()
  sessionDetailIds: number[]
  @ApiProperty()
  result: ResultSwabEnum | 'auto'
  @ApiProperty()
  asFinalResult: boolean
}