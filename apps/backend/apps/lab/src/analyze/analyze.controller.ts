import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { BadRequestException, Body, Controller, Delete, Get, Logger, Param, Patch, Post, Query, Req, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { UpdateSessionDetailDTO } from '../extraction/type';
import { AnalyzeService } from './analyze.service';
import { AnalyzeSessionCreateDTO, BulkUpdateResulBydoctorDTO, UpdateFinalResultDTO, UpdateResultByDoctorDTO, UpdateResultToolDTO, UpdateSessionPositionDTO } from './type';

@ApiTags("Lab API")
@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('analyze')
export class AnalyzeController {
  constructor(
    private analyzeService: AnalyzeService
  ) { }

  logger = new Logger('AnalyzeController')

  @Get('/sessions')
  async getAllSessions(
    @Query('idlab')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    try {
      return await this.analyzeService.getAllSessions(idLab, paginate, filter)
    } catch (error) {
      console.log(error);

      throw new BadRequestException(error)
    }
  }

  @Get('/session/:id')
  async getSessionById(
    @Query('idlab')
    idLab: string,
    @Param('id')
    idSession: string,
  ) {
    try {
      return await this.analyzeService.getSessionById(idSession, idLab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/sample-extracted')
  async getSampleExtracted(
    @Query('idlab')
    idLab: string
  ) {
    try {
      return await this.analyzeService.getSampleExtracted(idLab)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Post('/session')
  async addSessionAnalyze(
    @Body()
    body: AnalyzeSessionCreateDTO,
    @Request()
    req: any
  ) {
    try {
      await this.analyzeService.addSessionAnalyze(body)
      return await this.analyzeService.setSampleToMapped(body, req.user.id)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/result/:idsession')
  async getSessionAllResult(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number
  ) {
    try {
      return await this.analyzeService.getSessionAllResult(idLab, idSession)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Get('/result/:idsession/:locatecell')
  async getResultByLocateCell(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number,
    @Param('locatecell')
    locateCell: string,
  ) {
    try {
      return await this.analyzeService.getResultByLocateCell(idLab, idSession, locateCell)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/session/:idsession/update-position')
  async updateSessionPosition(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number,
    @Body()
    body: UpdateSessionPositionDTO,
    @Request()
    req: any
  ) {
    try {
      await this.analyzeService.updateSessionPosition(idLab, idSession, body, req.user.id)
      return await this.analyzeService.setSessionToMapped(idSession, req.user.id)
    } catch (error) {
      this.logger.error(error)
      throw new BadRequestException(error)
    }
  }

  @ApiOperation({ description: "Ambil list sample yang sudah keluar hasilnya" })
  @Get('/result-out')
  async getResultOut(
    @Query('idlab')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    try {
      return await this.analyzeService.getResultOut(idLab, paginate, filter)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Get('/result-final')
  async getResultFinal(
    @Query('idlab')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    try {
      return await this.analyzeService.getResultFinal(idLab, paginate, filter)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Post('/result-final/send/:idsample')
  async sendResultFinal(
    @Query('idlab')
    idLab: string,
    @Param('idsample')
    idsample: number,
    @Request()
    req:any
  ) {
    try {
      return await this.analyzeService.sendResultFinal(idLab, idsample, req.user.id)
    } catch (error) {
      console.log(error);
      
      throw new BadRequestException(error)
    }
  }

  @Post('/result-final/send-bulk/')
  async sendBulkResultFinal(
    @Query('idlab')
    idLab: string,
    @Body()
    listSample: number[] | number,
    @Request() req:any
  ) {
    try {
      console.log(`List sample : ${JSON.stringify(listSample)}`);
      
      if(!Array.isArray(listSample))[
        listSample = [listSample]
      ]
      for (const idSample of listSample) {
        await this.analyzeService.sendResultFinal(idLab, idSample, req.user.id)
      }
      return true;
    } catch (error) {
      this.logger.error(error);
      throw new BadRequestException(error.message)
    }
  }

  @Post('/result-doctor/:idSample')
  async updateResultByDoctor(
    @Query('idlab')
    idLab: string,
    @Param('idSample')
    idSample: number,
    @Req()
    req: any,
    @Body()
    body: UpdateResultByDoctorDTO
  ) {
    try {
      let user = req.user
      return await this.analyzeService.updateResultByDoctor(idLab, user.id, idSample, body)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/result-doctor/:idsample')
  @ApiResponse({ status: 400, description: "Data tidak ditemukan / kosong" })
  async getResultByDoctor(
    @Query('idlab')
    idLab: string,
    @Param('idsample')
    idSample: number
  ) {
    try {
      return await this.analyzeService.getResultByDoctor(idLab, idSample)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Patch('/result-doctor/result/bulk')
  async UpdateResultByDoctorBulk(
    @Query('idlab')
    idLab: string,
    @Request()
    req: any,
    @Body()
    body: BulkUpdateResulBydoctorDTO
  ) {
    try {
      return await this.analyzeService.bulkUpdateResultByDoctor(idLab, req.user.id, body)
    } catch (error) {
      this.logger.error(error)
      throw new BadRequestException(error)
    }
  }

  @ApiOperation({ description: "API untuk ganti posisi sample analyze mapping" })
  @Patch('/session/update-position/:idsession/:idSessionDetail')
  async updateSessionDetail(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number,
    @Param('idSessionDetail')
    idSessionDetail: number,
    @Body()
    body: UpdateSessionDetailDTO,
  ) {
    try {
      return await this.analyzeService.updateSessionDetail(idLab, idSessionDetail, idSession, body)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/result-tool')
  async UpdateResultTool(
    @Body()
    updateResultTool: UpdateResultToolDTO
  ) {
    try {
      return await this.analyzeService.UpdateResultTool(updateResultTool)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Delete('/session/:idsession/result')
  async deleteResultSession(
    @Param('idsession')
    idSession: string,
    @Query('idlab')
    idLab: string,
  ) {
    try {
      return await this.analyzeService.deleteResultTool(idSession, idLab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/session/:idsession/bulk-result-sample')
  async updateResultSample(
    @Param('idsession')
    idSession: number,
    @Body()
    body: UpdateFinalResultDTO,
    @Request()
    req: any
  ) {
    try {
      return await this.analyzeService.updateResultSample(idSession, body, req.user.id)
    } catch (error) {
      this.logger.error(`Failed update result sample : ${error}`)
      throw new BadRequestException(error)
    }
  }
}
