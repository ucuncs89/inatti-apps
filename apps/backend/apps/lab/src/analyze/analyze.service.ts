import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabAnalyzeResult } from '@app/database/entity/mapping/analyze/lab-analyze-result.entity';
import { LabAnalyzeSessionDetail } from '@app/database/entity/mapping/analyze/lab-analyze-session-detail.entity';
import { LabAnalyzeSessions } from '@app/database/entity/mapping/analyze/lab-analyze-session.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository, ILike, In, Not } from 'typeorm';
import { AnalyzeMappingDTO, AnalyzeSessionCreateDTO, BulkUpdateResulBydoctorDTO, UpdateFinalResultDTO, UpdateResultByDoctorDTO, UpdateResultToolDTO, UpdateSessionPositionDTO } from './type';
import { AnalyzeTool } from '@app/database/entity/mapping/analyze/analyze-tool.entity';
import { LabAnalyzeDoctor } from '@app/database/entity/lab/lab-analyze-doctor.entity';
import { User } from '@app/database/entity/user/user.entity';
import { StatusSwabEnum } from '@app/database/entity/swab-data/type';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { UpdateSessionDetailDTO } from '../extraction/type';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { StatusSampleService } from '../status-sample/status-sample.service';
import { ConvertToResultEnum } from '@app/helper/utils';
import { WhatsappNotifyService } from '@app/email-service/whatsapp-service/whatsapp/whatsapp-notify.service';
import { LoggerActivityService } from '@app/logger-activity';


@Injectable()
export class AnalyzeService {
  constructor(
    @InjectRepository(LabAnalyzeSessions)
    private LabAnalyzeSessionsRepository: Repository<LabAnalyzeSessions>,
    @InjectRepository(LabAnalyzeSessionDetail)
    private LabAnalyzeSessionDetailRepository: Repository<LabAnalyzeSessionDetail>,
    @InjectRepository(LabAnalyzeResult)
    private LabAnalyzeResultRepository: Repository<LabAnalyzeResult>,
    @InjectRepository(SwabDetail)
    private DetailSwabRepository: Repository<SwabDetail>,
    @InjectRepository(LabAnalyzeDoctor)
    private LabAnalyzeDoctorRepository: Repository<LabAnalyzeDoctor>,
    private whatsappNotifyService: WhatsappNotifyService,
    private statusSampleService: StatusSampleService,
    private activity: LoggerActivityService
  ) { }

  logger = new Logger('AnalyzeService')

  async getAllSessions(idLab: string, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    let where:any = {
      lab: {
        id: idLab
      }
    }
    if(filter){
      if(filter.date){
        where.createdAt = filter.date
      }
      if(filter.rangedate){
        where.createdAt = filter.rangedate
      }
      if(filter.search){
        where.name = ILike(`%${filter.search}%`)
      }
    }

    let sessions = await this.LabAnalyzeSessionsRepository.find({
      where,
      relations: [
        'mapping',
        'tool'
      ],
      order: {
        createdAt: 'DESC'
      },
      take: paginate.take,
      skip: paginate.skip,
    })

    for (let i = 0; i < sessions.length; i++) {
      const session = sessions[i];
      session['total_mapping'] = await this.LabAnalyzeSessionDetailRepository.count({ session })
    }

    let total = await this.LabAnalyzeSessionsRepository.count({ where })

    return new Pagination({
      results: sessions,
      total
    }, paginate)
  }
  getSessionById(idSession: string, idLab: string) {
    return this.LabAnalyzeSessionsRepository.findOne({
      where: {
        lab: idLab,
        id: idSession
      },
      relations: [
        'mapping',
        'tool',
        'mapping.sample',
        'mapping.sample.history',
        'mapping.sample.history.status',
        'mapping.sample.patient',
      ]
    })
  }
  getSampleExtracted(idLab: string) {
    return this.DetailSwabRepository
      .createQueryBuilder('ds')
      .leftJoin('ds.labAnalyzeSession', 'lasd')
      .leftJoinAndSelect('ds.patient', 'PatientSwab')
      .leftJoinAndSelect('ds.history', 'history')
      .leftJoinAndSelect('history.status', 'status')
      .leftJoin('ds.labExtraction', 'lext')
      .where('lasd.id is null')
      .andWhere('lext.id is not null')
      .andWhere('ds.labTarget = :idLab', { idLab })
      .andWhere('history.status = :statusHistory', { statusHistory: LabStatusSample.statusEkstracted.id })
      .getMany()
  }
  async addSessionAnalyze(dataDTO: AnalyzeSessionCreateDTO) {
    return await getManager().transaction(async entity => {
      let session = new LabAnalyzeSessions()
      session.name = dataDTO.name
      session.lab = new LabDetail(dataDTO.lab)
      session.rowTotal = dataDTO.rowTotal
      session.columnTotal = dataDTO.columnTotal
      session.tool = new AnalyzeTool(dataDTO.tool)

      session = await entity.save(session)

      let mappingSession = dataDTO.mapping.map((v: AnalyzeMappingDTO): LabAnalyzeSessionDetail => {
        let detailSession = new LabAnalyzeSessionDetail()
        detailSession.lab = new LabDetail(dataDTO.lab)
        detailSession.session = session
        detailSession.locateColumn = v.locateColumn
        detailSession.locateRow = v.locateRow
        if (v.isControl) {
          detailSession.isControl = v.isControl
          detailSession.controlType = v.controlType
          detailSession.controlName = v.controlName
        } else {
          detailSession.sample = new SwabDetail(v.sample)
        }
        return detailSession
      })
      await entity.save(mappingSession)
    })
  }

  /**
   * Typeorm is SUCK !!
   * Cannot select some field relations. Lack documentation.
   */
  async getSessionAllResult(idLab: string, idSession: number) {
    return this.LabAnalyzeSessionsRepository.findOne({
      where: {
        id: idSession,
        lab: new LabDetail(idLab)
      },
      order: {
        createdAt: 'DESC'
      },
      relations: ['tool', 'mapping', 'mapping.sample', 'mapping.sample.patient', 'mapping.resultTool']
    })
  }

  async getResultByLocateCell(idLab: string, idSession: number, locateCell: string) {
    let result = await this.getSessionAllResult(idLab, idSession)
    return result.result[locateCell]
  }

  /**
   * Result by doctor will create new if not exist, and update if exist
   */
  async updateResultByDoctor(idLab: string, userId: string, idSample: number, body: UpdateResultByDoctorDTO) {
    let sampleSessionId
    let whereQuery:any = {
      sample: idSample,
    }
    if(body.sessionDetailId){
      sampleSessionId = +body.sessionDetailId
      whereQuery.sampleSession = sampleSessionId
    }
    let resultDoctor: LabAnalyzeDoctor = await this.LabAnalyzeDoctorRepository.findOne({
      where: whereQuery 
    })
    if (!resultDoctor) {
      resultDoctor = new LabAnalyzeDoctor();
    }
    resultDoctor.lab = new LabDetail(idLab)
    resultDoctor.note = body.note;
    resultDoctor.result = body.result
    resultDoctor.sample = new SwabDetail(idSample)
    resultDoctor.createdBy = new User(userId)
    if(sampleSessionId){
      resultDoctor.sampleSession = new LabAnalyzeSessionDetail(sampleSessionId);
    } else {
      resultDoctor.sampleSession = await this.LabAnalyzeSessionDetailRepository.findOneOrFail({
        where: {
          sample: idSample
        },
        order: {
          id: "DESC"
        }
      }) 
    }

    await this.LabAnalyzeDoctorRepository.save(resultDoctor)
    if (body.asFinalResult) {
      await this.sendResultFinal(idLab, idSample, userId)
    }
    let res = await this.setSampleTostatusValidateResult(idSample, userId)

    await this.activity.log({
      type: this.activity.activityList.lab.updateAnalyzeResultByDoctor,
      swabDetail: idSample,
      params: {
        result: body.result
      },
      user: userId
    })

    return res;
  }

  async bulkUpdateResultByDoctor(idLab: string, userId: string, body: BulkUpdateResulBydoctorDTO) {
    try{

      this.logger.verbose(`bulkUpdateResultByDoctor: user ${userId} send with body ${JSON.stringify(body)}`)
      if (body.sessionDetailIds.length < 1) throw new Error('No sample input')
      if (body.result === 'auto') {
        for (const sessionDetailId of body.sessionDetailIds) {
          let sessionDetail = await this.LabAnalyzeSessionDetailRepository.findOne({
            where: {
              id: sessionDetailId,
            },
            relations: ['resultTool', 'sample']
          })
          if (!sessionDetail.resultTool) {
            continue;
          }
          const resultTool: LabAnalyzeResult = sessionDetail.resultTool
          let summaryResultTool = ConvertToResultEnum(resultTool.resultSummary)
          this.logger.verbose(`convert result from ${resultTool.resultSummary} to ${summaryResultTool}`)
          // skip if invalid result
          if (!summaryResultTool) continue;
  
          await this.updateResultByDoctor(idLab, userId, sessionDetail.sample.id, { result: summaryResultTool, sessionDetailId: sessionDetailId }, )
          if (body.asFinalResult) {
            await this.sendResultFinal(idLab, sessionDetail.sample.id, userId)
          }
        }
      } else {
        for (const sessionDetailId of body.sessionDetailIds) {
          let sessionDetail = await this.LabAnalyzeSessionDetailRepository.findOne({
            where: { id: sessionDetailId },
            relations: ['sample']
          })
          await this.updateResultByDoctor(idLab, userId, sessionDetail.sample.id, { result: body.result, sessionDetailId: sessionDetailId })
          if (body.asFinalResult) {
            await this.sendResultFinal(idLab, sessionDetail.sample.id, userId)
          }
        }
      }
    }catch(err){
      console.log(err)
      throw err;
    }
  }

  setSampleTostatusValidateResult(idSample: number, userId: string) {
    return this.statusSampleService.setStatusSample(idSample, LabStatusSample.statusValidateResult, userId)
  }

  async getResultByDoctor(idLab: string, idSample: number) {
    return await this.LabAnalyzeDoctorRepository.findOne({
      where: {
        lab: idLab,
        sample: idSample
      }
    })
  }

  /**
   * Get Sample from LabAnalyzeSessionDetail that result from tool not null (have result)
   * @param idLab 
   * @returns 
   */
  async getResultOut(idLab: string, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    let where:any = {
      sampleSession: {
        lab: {
          id: idLab
        },
        isControl: In([null, false])
      },
    }

    if(filter){
      if(filter.date){
        where.createdAt = filter.date
      }
      if(filter.rangedate){
        where.createdAt = filter.rangedate
      }
      if(filter.search){
        where.sampleSession.sample = {
          patient:{
            name: ILike(`%${filter.search}%`)
          }
        }
      }
    }

    let relations = [
      'sampleSession',
      'sampleSession.resultDoctor',
      'sampleSession.sample',
      'sampleSession.sample.patient',
      'sampleSession.lab'
    ]

    let sessions = await this.LabAnalyzeResultRepository.find({
      where,
      relations,
      take: paginate.take,
      skip: paginate.skip,
      order: {
        createdAt: 'DESC'
      }
    })

    let total = await this.LabAnalyzeResultRepository.count({
      where,
      relations
    })

    return new Pagination({
      results: sessions,
      total
    }, paginate)
  }


  async getResultFinal(idLab: string, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    let where:any = {
      lab: {
        id: idLab
      }
    };

    if(filter){
      if(filter.date){
        where.createdAt = filter.date
      }
      if(filter.rangedate){
        where.createdAt = filter.rangedate
      }
      if(filter.search){
        where.sample = {
          patient:{
            name: ILike(`%${filter.search}%`)
          }
        }
      }
    }

    let relations = [
      'lab',
      'sampleSession',
      'sampleSession.resultTool',
      'sample',
      'sample.patient',
      'sample.refTarget',
      'sample.narSample',
    ]

    let listSample = await this.LabAnalyzeDoctorRepository.find({
      where,
      relations,
      order: {
        id: 'DESC'
      },
      take: paginate.take,
      skip: paginate.skip,
    })

    for (const sample of listSample) {
      if(sample.sampleSession?.resultTool?.resultRaw ?? false){
        delete sample.sampleSession.resultTool.resultRaw
      }
    }

    let total = await this.LabAnalyzeDoctorRepository.count({
      where, relations
    })

    return new Pagination({
      results: listSample,
      total
    }, paginate)
  }

  async sendResultFinal(idLab: string, sampleId: number, userId: string) {
    try{
      let finalResult = null

      let result = await getManager().transaction(async entity => {
        let resultAnalyzeDoctor: LabAnalyzeDoctor = await this.LabAnalyzeDoctorRepository.findOneOrFail({
          where: {
            sample: sampleId,
            lab: idLab
          },
          relations: [
            'sample',
            'sample.patient',
            'sampleSession',
            'sampleSession.session'
          ]
        })
        resultAnalyzeDoctor.publishAt = new Date();
        await entity.save(resultAnalyzeDoctor)

        finalResult = resultAnalyzeDoctor.result
  
        return await entity.update(
          SwabDetail,
          { id: sampleId },
          {
            resultSwab: resultAnalyzeDoctor.result,
            resultSentAt: new Date(),
            status: StatusSwabEnum.resultOutLab,
          }
        )
      })
      if(!result){
        throw "Failed set final result"
      }

      await this.activity.log({
        type: this.activity.activityList.lab.publishResult,
        swabDetail: sampleId,
        params: {
          result: finalResult
        },
        user: userId
      })
      
      return result;
    } catch(err){
      throw err
    }
  }

  async updateSessionDetail(idLab: string, idSessionDetail: number, idSession: number, body: UpdateSessionDetailDTO) {
    // check is new position available in table
    let availablePosition = await this.LabAnalyzeSessionDetailRepository.find({
      where: {
        locateRow: body.locateRow,
        locateColumn: body.locateColumn,
        session: idSession
      }
    })
    if (availablePosition.length > 0) {
      throw new Error('New position is available')
    }
    let sessionDetail = await this.LabAnalyzeSessionDetailRepository.findOne({
      locateColumn: body.locateColumn,
      locateRow: body.locateRow
    })
    sessionDetail.id = idSession

    return this.LabAnalyzeSessionDetailRepository.update({
      id: idSessionDetail,
      lab: new LabDetail(idLab),
      session: new LabAnalyzeSessions(idSession)
    }, {
      locateColumn: body.locateColumn,
      locateRow: body.locateRow
    })
  }

  UpdateResultTool(body: UpdateResultToolDTO) {
    return this.LabAnalyzeResultRepository.update({ id: body.id }, body)
  }

  async setSampleToMapped(body: AnalyzeSessionCreateDTO, userId: string) {
    for (const mapping of body.mapping) {
      try {
        if (!mapping.sample) {
          continue;
        }
        await this.statusSampleService.setStatusSample(mapping.sample, LabStatusSample.statusMappedPCRPlate, userId)
      } catch (error) {
        throw error
      }
    }
    return true;
  }

  async setSessionToMapped(idSession: number, userId: string) {
    let sessionsDetail = await this.LabAnalyzeSessionDetailRepository.find({
      where: {
        session: new LabAnalyzeSessions(idSession)
      },
      relations: ['sample']
    })
    if (sessionsDetail.length < 1) {
      throw new Error('Session not found')
    }

    for (const sd of sessionsDetail) {
      if (!sd.isControl) {
        await this.statusSampleService.setStatusSample(sd.sample.id, LabStatusSample.statusMappedPCRPlate, userId)
      }
    }
  }

  async updateSessionPosition(idLab: string, idSession: number, payload: UpdateSessionPositionDTO, userId: string) {
    this.logger.verbose(`Update session ${idSession} with payload ${JSON.stringify(payload)}`)
    if (Array.isArray(payload.new)) {
      for (const sample of payload.new) {
        this.logger.debug(`Payload New : process ${sample}`)

        let sessionDetail = new LabAnalyzeSessionDetail()
        sessionDetail.lab = new LabDetail(idLab)
        sessionDetail.session = new LabAnalyzeSessions(idSession)
        sessionDetail.locateColumn = sample.locateColumn
        sessionDetail.locateRow = sample.locateRow
        if (sample.isControl) {
          sessionDetail.isControl = true
          sessionDetail.controlType = sample.controlType
          sessionDetail.controlName = sample.controlName
        } else {
          sessionDetail.sample = new SwabDetail(sample.sample)
        }
        await this.LabAnalyzeSessionDetailRepository.insert(sessionDetail)
      }
    }

    if (Array.isArray(payload.stay)) {
      const detailSession = await this.LabAnalyzeSessionDetailRepository.find({
        where: {
          session: idSession,
          lab: idLab
        },
        relations: ['sample']
      })
      this.logger.verbose(`Payload stay INFO : Length detail ${idSession} ${detailSession.length}`)
      for (const cell of payload.stay) {
        this.logger.debug(`Process stay : ${JSON.stringify(cell)}`)
        let detail = detailSession.find((v) => {
          if (v.isControl) {
            return v.isControl && v.controlName === cell.controlName
          } else {
            return v.sample.id === cell.sample
          }
        })

        if (detail) {
          this.logger.debug(`Update ${detail.id} : col ${detail.locateColumn}, row ${detail.locateRow}`)
          detail.locateRow = cell.locateRow
          detail.locateColumn = cell.locateColumn

          await this.LabAnalyzeSessionDetailRepository.save(detail)
        }
      }
    }

    if (Array.isArray(payload.del)) {
      this.logger.verbose(`Payload Del : ${JSON.stringify(payload.del)}`)
      for (const cell of payload.del) {
        if (cell.isControl) {
          this.logger.verbose(`Process delete cell CONTROL ${JSON.stringify(cell)}`)
          // const queryDelete = `delete from lab_extraction_session_detail where "sessionId" = '${idSession}' and "controlName" = '${cell.controlName}'`
          // this.logger.log(`Run query : ${queryDelete}`)
          // await getManager().query(queryDelete)

          // TYPEORM CANNOT RUN THIS CODE !!, it give us error
          await this.LabAnalyzeSessionDetailRepository.delete({
            session: new LabAnalyzeSessions(idSession),
            controlName: cell.controlName,
          })
        } else {
          this.logger.verbose(`Process delete cell NOT CONTROL ${JSON.stringify(cell)}`)
          await this.LabAnalyzeSessionDetailRepository.delete({
            session: new LabAnalyzeSessions(idSession),
            sample: new SwabDetail(cell.sample)
          })
          await this.statusSampleService.removeStatusSample(cell.sample, LabStatusSample.statusMappedPCRPlate, userId)
          await this.statusSampleService.removeStatusSample(cell.sample, LabStatusSample.statusResultImported, userId)
        }
      }
    }
  }

  async deleteResultTool(idSession: string, idLab: string) {
    let session = await this.LabAnalyzeSessionsRepository.findOne({
      where: {
        id: idSession,
        lab: new LabDetail(idLab)
      },
      relations: [
        'mapping'
      ]
    })

    session.result = null
    session.resultSummary = null
    session.resultUploadDate = null
    await this.LabAnalyzeSessionsRepository.save(session)

    for (const mapping of session.mapping) {
      await this.LabAnalyzeResultRepository.delete({
        sampleSession: mapping
      })
    }
    return true;
  }

  async updateResultSample(idSession: number, body: UpdateFinalResultDTO, userId: string) {
    this.logger.verbose(`updateResultSample: Process ${JSON.stringify(body)}`)
    for (const sampleId of Object.keys(body)) {
      const finalResult = body[sampleId]
      if (isNaN(+sampleId)) continue; // skip if Not number

      let sessionDetail = await this.LabAnalyzeResultRepository.findOne({
        where: {
          session: new LabAnalyzeSessions(idSession),
          sampleSession: new LabAnalyzeSessionDetail(+sampleId)
        },
        relations: [
          'session',
          'sampleSession',
          'sampleSession.sample',
        ]
      })
      if (!sessionDetail) {
        this.logger.warn(`updateResultSample: ${sampleId} not found in LabAnalyzeResult with session ${idSession}`)
        continue
      };

      sessionDetail.resultSummary = finalResult
      await this.LabAnalyzeResultRepository.save(sessionDetail)
      this.logger.log(`updateResultSample: Success update ${sampleId} result summary to ${finalResult}`)

      await this.activity.log({
        type: this.activity.activityList.lab.updateAnalyzeResultByTool,
        swabDetail: sessionDetail.sampleSession.sample.id,
        params: {
          analyzeId: sessionDetail.session.id,
          result: sessionDetail.resultSummary
        },
        user: userId
      })
    }
  }
}
