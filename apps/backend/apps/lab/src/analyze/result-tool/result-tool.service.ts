import { AwsService } from '@app/aws';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { AnalyzeTool } from '@app/database/entity/mapping/analyze/analyze-tool.entity';
import { LabAnalyzeResult } from '@app/database/entity/mapping/analyze/lab-analyze-result.entity';
import { LabAnalyzeSessionDetail } from '@app/database/entity/mapping/analyze/lab-analyze-session-detail.entity';
import { AttachedFileType, LabAnalyzeSessions } from '@app/database/entity/mapping/analyze/lab-analyze-session.entity';
import { A1toNumber } from '@app/helper/mappingPosition';
import { LoggerActivityService } from '@app/logger-activity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ManagedUpload } from 'aws-sdk/clients/s3';
import { createHmac } from 'crypto';
import { Repository } from 'typeorm';
import { StatusSampleService } from '../../status-sample/status-sample.service';
import { AnalyzeResultAddDTO } from '../type';
import { IMappingMachine } from './tools-covid/mapping-machine';

@Injectable()
export class ResultToolService {
  constructor(
    @InjectRepository(LabAnalyzeSessions)
    private LabAnalyzeSessionsRepository: Repository<LabAnalyzeSessions>,
    @InjectRepository(LabAnalyzeSessionDetail)
    private LabAnalyzeSessionDetailRepository: Repository<LabAnalyzeSessionDetail>,
    @InjectRepository(LabAnalyzeResult)
    private LabAnalyzeResultRepository: Repository<LabAnalyzeResult>,
    private statusSampleServie:StatusSampleService,
    private aws: AwsService,
    private activity: LoggerActivityService
  ) { }

  async uploadResult(body: AnalyzeResultAddDTO, resultMappingMachine: IMappingMachine, userLoggedin: string, fileAttach?: Express.Multer.File[]) {
    let uploadedAttach:any;
    if(fileAttach){
      uploadedAttach = await this.uploadFileAttach(fileAttach[0])
      console.log(uploadedAttach)
    }

    let session = await this.LabAnalyzeSessionsRepository.findOneOrFail({
      where: {
        id: body.idSession,
        lab: body.idLab,
      },
      relations: ['tool']
    })

    // save all raw data
    session.result = resultMappingMachine.rawData
    session.resultSummary = resultMappingMachine.summaryRawData
    session.resultUploadDate = new Date() // now

    if(uploadedAttach){
      session.attachedFile = uploadedAttach
    }
    await this.LabAnalyzeSessionsRepository.save(session)

    const { summaryRawData, ctValues } = resultMappingMachine

    await this.setSummaryDataToSessionCell(body, summaryRawData, ctValues, session.tool, userLoggedin)
  }

  async setSummaryDataToSessionCell(body: AnalyzeResultAddDTO, summaryRawData:any, ctValue: any, tool: AnalyzeTool, userLoggedin: string) {
    // save all result to database (if exist)
    for (const cellA1 of Object.keys(summaryRawData)) {
      let cell = A1toNumber(cellA1)
      let analyzeDetail = await this.LabAnalyzeSessionDetailRepository.findOne({
        where: {
          locateColumn: cell.col,
          locateRow: cell.row,
          session: body.idSession,
          lab: body.idLab
        },
        relations: [
          'sample' // get sample data
        ]
      })
      if (analyzeDetail) {
        // override data if exist
        let analyzeResult = await this.LabAnalyzeResultRepository.findOne({ sampleSession: analyzeDetail })
        if (!analyzeResult) {
          analyzeResult = new LabAnalyzeResult()
        }
        analyzeResult.sampleSession = analyzeDetail
        analyzeResult.tool = tool

        // automatically set result from tool
        analyzeResult.resultSummary = summaryRawData[cellA1].result

        analyzeResult.resultRaw = summaryRawData[cellA1].rawData
        analyzeResult.session = new LabAnalyzeSessions(body.idSession)
        analyzeResult.locateRow = cell.row + ""
        analyzeResult.locateColumn = cell.col + ""
        analyzeResult.CtValue = ctValue[cellA1]

        await this.LabAnalyzeResultRepository.save(analyzeResult)
      }
    }

    // set all status sample to resultImported
    let listSample = await this.LabAnalyzeSessionDetailRepository.find({
      where: {
        session: body.idSession
      },
      relations: ['sample', 'resultTool', 'session']
    })

    // create log
    for (const sessionDetail of listSample) {
      if(!sessionDetail.isControl && sessionDetail.sample){
        await this.statusSampleServie.setStatusSample(sessionDetail.sample.id, LabStatusSample.statusResultImported, userLoggedin)

        await this.activity.log({
          type: this.activity.activityList.lab.updateAnalyzeResultByTool,
          swabDetail: sessionDetail.sample.id,
          params: {
            analyzeId: sessionDetail.session.id,
            result: sessionDetail.resultTool.resultSummary
          },
          user: userLoggedin
        })
      }
    }
  }

  async uploadFileAttach(file: Express.Multer.File): Promise<AttachedFileType>{
    const secret = new Date().getTime()
    const fileName = {
      originalName: file.originalname,
      hashedName: null,
      format: file.originalname.split('.').pop(),
      s3: null
    }
    
    const nameHashed = createHmac('md5', secret+"").update(file.originalname).digest('hex') + "." + fileName.format;
    fileName.hashedName = nameHashed;

    let resultS3:ManagedUpload.SendData = await this.aws.s3Upload(file.buffer, nameHashed);
    fileName.s3 = resultS3;

    return fileName
  }
}
