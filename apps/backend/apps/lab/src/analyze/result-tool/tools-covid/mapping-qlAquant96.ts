import { IMappingMachine, ToolsSummaryType } from "./mapping-machine"
import { readFilesQlAquant96, resultQlAquant96, WellResultQlAquant96 } from '@inatti/mapping-analyze-result'
import { Logger } from "@nestjs/common"

export class FileQlAquant96DTO {
  fileCt?:Express.Multer.File[]
  fileCurve?:Express.Multer.File[]

  static interceptorFile = [
    { name: 'fileCt' },
    { name: 'fileCurve' }
  ]
}

export class MappingMachineQlAquant96 implements IMappingMachine {
  constructor(fileCt: Buffer, fileCurve: Buffer) {
    if (!fileCt || !fileCurve) {
      throw new Error('fileCt and fileCurve is required')
    }
    this.logger = new Logger("MappingMachineQlAquant96")

    this.logger.log("Ready to read file")
    this.rawData = this.readFile(fileCt, fileCurve)
    this.logger.log("finish to read file")
    // this.logger.log("Result : ", this.rawData)
    this.logger.log("Ready to read summary")
    this.summaryRawData = this.getSummary(this.rawData)
    this.logger.log("summary finish", JSON.stringify(this.summaryRawData))
  }
  logger: Logger
  rawData: any
  summaryRawData: any
  ctValues: Record<string, any>
  readFile(fileCt: Buffer, fileCurve: Buffer) {
    return readFilesQlAquant96(fileCt, fileCurve)
  }
  getSummary(result: resultQlAquant96): ToolsSummaryType {
    let summary: ToolsSummaryType = {}
    this.ctValues = {}
    for (const id in result) {
      console.log(id)
      console.log(JSON.stringify(result[id]))
      summary[id] = {
        rawData: result[id],
        result: result[id].result
      }
      
      this.ctValues[id] = this.getCtValues(result[id])
    }

    return summary 
  }

  getCtValues(result: WellResultQlAquant96): Record<string, number> {
    let ctValues: Record<string, number> = {}
    for (const row of result.ct) {
      if(!row.ct) continue;
      if(row.dye === 'ROX') continue; // Skip ROX
      ctValues[row.dye] = row.ct
    }
    return ctValues
  }

}