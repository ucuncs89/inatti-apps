import { RawDataType, readLc69File, ResultLc96, ResultRawType, StepDataType } from "@inatti/mapping-analyze-result";
import { IMappingMachine, ToolsSummaryType } from "./mapping-machine";

/** File DTO */
export class FileLc96DTO {
  fileResultFluorescene?: Express.Multer.File[]
  fileResultRaw?: Express.Multer.File[]

  /** For file interceptor Nestjs */
  static interceptorFile = [
    { name: 'fileResultFluorescene' },
    { name: 'fileResultRaw' },
  ]
}

export type RawDataLC96 = {
  analysis: ResultRawType,
  graph: StepDataType
}

export class MappingMachineLc96 implements IMappingMachine {
  rawData = null
  summaryRawData = null

  constructor(fileFluorescence?: Buffer, fileResultRaw?: Buffer) {
    if (fileFluorescence && fileResultRaw) {
      this.rawData = this.readFile(fileFluorescence, fileResultRaw)
      this.summaryRawData = this.getSummary(this.rawData)
    }
  }
  // TODO : Map this value
  ctValues: Record<string, any>;

  /** Read File with param, result is Raw Data */
  readFile(fileFluorescence: Buffer, fileResultRaw: Buffer): any {
    return readLc69File({
      fileFluorescence: fileFluorescence,
      fileRaw: fileResultRaw
    })
  }

  /** Generate summary from raw data to raw data per cell */
  getSummary(result: ResultLc96): ToolsSummaryType {
    let summary: { [key: string]: { rawData: RawDataLC96, result: string } } = {}
    this.ctValues = {}
    for (const cell of Object.keys(result.rawData)) {
      let value = result.rawData[cell]
      // based result, result only 1 dye 
      let firstDye = Object.values(value.dye)[0] as RawDataType ?? null

      // get graph from fluoresence
      let graph = result.stepData[cell]

      summary[cell] = {
        rawData: {
          analysis: { [cell]: value },
          graph: { [cell]: graph },
        },
        result: firstDye.Call
      }
    }
    return summary
  }
}