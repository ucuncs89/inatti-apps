import { IMappingMachine, ToolsSummaryType } from "./mapping-machine";
import { readVazyment3, ResultTypeVazyment3 } from "@inatti/mapping-analyze-result";

export class FileVazyment3DTO {
  fileSummary?:Express.Multer.File[]
  fileAttach?:Express.Multer.File[]

  static interceptorFile = [
    { name: 'fileSummary' },
    { name: 'fileAttach' }
  ]
}

export class MappingMachineVazyment3 implements IMappingMachine {
  constructor(
    fileSummary:Buffer
  ){
    if(!fileSummary) {
      throw new Error('fileSummary is required')
    }

    this.rawData = this.readFile(fileSummary)
    this.summaryRawData = this.getSummary(this.rawData);
  }
  ctValues: Record<string, any>;

  rawData: any;
  summaryRawData: any;
  readFile(fileSummary: Buffer) {
    return readVazyment3(fileSummary);
  }
  getSummary(result: Record<string, ResultTypeVazyment3>): ToolsSummaryType {
    const summary: ToolsSummaryType = {}
    this.ctValues = {}
    for (const cell in result) {
      if (Object.prototype.hasOwnProperty.call(result, cell)) {
        const element = result[cell];
        summary[element.well] = {
          rawData: element,
          result: element.result
        }
        // save ctvalue
        this.ctValues[element.well] = {}
        for (const row of element.rows) {
          if(!row.Ct) continue;
          if(row.Target === 'IC') continue; // Skip IC
          
          this.ctValues[element.well][row.Target] = row.Ct
        }
      }
    }

    return summary;
  }

}