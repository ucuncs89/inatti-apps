export type ToolsSummaryType = { [cellA1: string]: { rawData: any, result: any } }

/**
 * Alat yang digunakan untuk test covid 19 punya beberapa jenis,
 * maka disini punya MappingMachine untuk mapping datanya ke dalam bentuk JSON.
 * Data yang diubah ke json disebut RAW DATA.
 * Raw data terdiri dari beberapa cell, seperti A1, A2, B1, B2.  nilai per cell ini
 * disebut dengan "Summary Raw Data"
 * 
 * Interface mapping machine menjadi standard untuk hasil data dari file ke database
 */

export interface IMappingMachine {
  rawData: any
  summaryRawData: any
  ctValues: Record<string, any>;

  /** Read File with param, result is Raw Data */
  readFile(...params: any): any

  /** Generate summary from raw data to raw data per cell */
  getSummary(...params: any): ToolsSummaryType
}