import { Cfx96ResultDetector, readCfx96, resultCfx96, SummaryCellCfx96 } from "@inatti/mapping-analyze-result"
import { Logger } from "@nestjs/common";
import { IMappingMachine, ToolsSummaryType } from "./mapping-machine";

export class FileCfx96DTO {
  fileAmplification?: Express.Multer.File[]
  fileSummary?: Express.Multer.File[]

  static interceptorFile = [
    { name: 'fileAmplification' },
    { name: 'fileSummary' }
  ]
}

export class MappingMachineCfx96 implements IMappingMachine {
  rawData = null;
  summaryRawData = null;
  logger = new Logger('MappingMachineCfx96')

  constructor(fileAmplification?: Buffer, fileSummary?: Buffer) {
    if (fileAmplification && fileSummary) {
      this.rawData = this.readFile(fileAmplification, fileSummary)
      this.summaryRawData = this.getSummary(this.rawData)
    }
  }
  // Todo : map this value
  ctValues: Record<string, any>;

  readFile(fileAmplification: Buffer, fileSummary: Buffer) {
    return readCfx96(fileAmplification, fileSummary)
  }

  getSummary(result: resultCfx96): ToolsSummaryType {
    const { summary: summaryTool, amplification } = result

    const resultSummary: ToolsSummaryType = {}

    this.logger.verbose(`get cell summary tool ${JSON.stringify(summaryTool['0'])}`);

    this.ctValues = {}

    // loop through all cell
    for (const cell of Object.keys(summaryTool['0'])) {
      this.logger.verbose(`read cell ${cell}`);

      resultSummary[cell] = {
        rawData: {},
        result: null
      }
      const summaryCell: SummaryCellCfx96[] = summaryTool['0'][cell]

      resultSummary[cell].rawData.summary = summaryCell
      resultSummary[cell].result = Cfx96ResultDetector(summaryCell)
      resultSummary[cell].rawData.gens = {}

      let cellNormalize = this.normalizeCell(cell)

      let listGen = Object.keys(amplification).filter((v) => v !== 'Run Information') // gen except 'Run Information'
      this.logger.verbose(`list gen : ${JSON.stringify(listGen)}`)

      // save ctvalues
      this.ctValues[cell] = {}
      for (const detailSummaryCell of summaryCell) {
        if(!detailSummaryCell.Cq) continue;
        let ct = (+detailSummaryCell.Cq).toFixed(2)
        this.ctValues[cell][detailSummaryCell.Target] = +ct
      }

      for (const gen of listGen) {
        this.logger.verbose(`gen : ${gen}, normalyze cell : ${cellNormalize}`);

        if(!resultSummary[cell]) {
          this.logger.warn(`result summary cannot found ${cell}`)
          continue;
        };
        const genResult = amplification[gen][cellNormalize]
        resultSummary[cell].rawData.gens[gen] = genResult
      }
    }
    return resultSummary
  }

  /**
   * Name cell from "A01" must change to "A1"
   */
  normalizeCell(cell) {
    return cell.replace(/(?=.)0(?=.)/, '')
  }
}