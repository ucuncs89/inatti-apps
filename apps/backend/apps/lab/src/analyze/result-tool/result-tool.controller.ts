import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { BadRequestException, Body, Controller, Post, Request, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Logger } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes } from '@nestjs/swagger';
import { AnalyzeResultAddDTO } from '../type';
import { ResultToolService } from './result-tool.service';
import { FileCfx96DTO, MappingMachineCfx96 } from './tools-covid/mapping-cfx96';
import { FileLc96DTO, MappingMachineLc96 } from './tools-covid/mapping-lc96';
import { FileVazyment3DTO, MappingMachineVazyment3 } from './tools-covid/mapping-vazyment3';
import { AwsService } from '@app/aws';
import { FileQlAquant96DTO, MappingMachineQlAquant96 } from './tools-covid/mapping-qlAquant96';

@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('result-tool')
export class ResultToolController {
  constructor(
    private resultToolService: ResultToolService,
    private awsService: AwsService
  ) { }

  logger = new Logger('ResultToolController')

  @UseInterceptors(FileFieldsInterceptor(FileLc96DTO.interceptorFile))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Analyze Result DTO',
    type: AnalyzeResultAddDTO,
  })
  @Post('/result/lc69')
  async uploadResultLc96(
    @Body()
    body: AnalyzeResultAddDTO,
    @UploadedFiles()
    files: FileLc96DTO,
    @Request()
    req:any
  ) {
    try {
      this.logger.verbose(`${req.user.id} : Upload file lc96 with payload ${JSON.stringify(body)}`)

      const mappingMachine = new MappingMachineLc96(files.fileResultFluorescene[0].buffer, files.fileResultRaw[0].buffer)
      return await this.resultToolService.uploadResult(body, mappingMachine, req.user.id);
    } catch (error) {
      this.logger.error(error);
      throw new BadRequestException(error.message)
    }
  }

  @UseInterceptors(FileFieldsInterceptor(FileCfx96DTO.interceptorFile))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Analyze Result DTO',
    type: AnalyzeResultAddDTO,
  })
  @Post('/result/cfx96')
  async uploadResultCfx96(
    @Body()
    body: AnalyzeResultAddDTO,
    @UploadedFiles()
    files: FileCfx96DTO,
    @Request()
    req: any
  ) {
    try {
      this.logger.verbose(`${req.user.id} : Upload file cfx96 with payload ${JSON.stringify(body)}`)

      const mappingMachine = new MappingMachineCfx96(files.fileAmplification[0].buffer, files.fileSummary[0].buffer)
      return await this.resultToolService.uploadResult(body, mappingMachine, req.user.id);
    } catch (error) {
      this.logger.error(error);
      throw new BadRequestException(error.message)
    }
  }

  @UseInterceptors(FileFieldsInterceptor(FileVazyment3DTO.interceptorFile))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Analyze Result DTO',
    type: AnalyzeResultAddDTO,
  })
  @Post('/result/vazyment3')
  async uploadResultVazyment3(
    @Body()
    body: AnalyzeResultAddDTO,
    @UploadedFiles()
    files: FileVazyment3DTO,
    @Request()
    req: any
  ) {
    try {
      this.logger.verbose(`${req.user.id} : Upload file vazyment3 with payload ${JSON.stringify(body)}`)

      const mappingMachine = new MappingMachineVazyment3(files.fileSummary[0].buffer)
      return await this.resultToolService.uploadResult(body, mappingMachine, req.user.id, files.fileAttach);
    } catch (error) {
      this.logger.error(error);
      throw new BadRequestException(error.message)
    }
  }

  @UseInterceptors(FileFieldsInterceptor(FileQlAquant96DTO.interceptorFile))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Analyze Result DTO',
    type: AnalyzeResultAddDTO,
  })
  @Post('/result/ql-aquant96')
  async uploadResultQlAquant96(
    @Body()
    body: AnalyzeResultAddDTO,
    @UploadedFiles()
    files: FileQlAquant96DTO,
    @Request()
    req: any
  ) {
    try {
      this.logger.verbose(`${req.user.id} : Upload file ql aquant with payload ${JSON.stringify(body)}`)

      const mappingMachine = new MappingMachineQlAquant96(files.fileCt[0].buffer, files.fileCurve[0].buffer)
      return await this.resultToolService.uploadResult(body, mappingMachine, req.user.id);
    } catch (error) {
      this.logger.error(error);
      throw new BadRequestException(error.message)
    }
  }
}
