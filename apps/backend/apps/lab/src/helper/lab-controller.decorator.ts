import { applyDecorators, Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";

export function LabApi(route?: string){
  if(route && route[0] === '/'){
    route = route.slice(1, route.length)
  }
  if(!route){
    route = '';
  }
  return applyDecorators(
    ApiTags('Lab API'),
    Controller('/api-lab/' + route)
  )
}