import { JwtAuthGuardLab } from "@app/authentication/authorization/jwt.authguard.lab";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth } from "@nestjs/swagger";

export function RequireAuth() {
  return applyDecorators(
    UseGuards(JwtAuthGuardLab),
    ApiBearerAuth()
  )
}