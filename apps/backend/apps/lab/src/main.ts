import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { LabModule } from './lab.module';

async function bootstrap() {
  const app = await NestFactory.create(LabModule);
  app.useGlobalPipes(new ValidationPipe())
  app.enableCors()
  const config = new DocumentBuilder()
    .setTitle('Lab Documentation')
    .setDescription('Lab API Documentation description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(process.env.NODE_PORT_LAB);
  console.log(`App lab jalan di port : ${process.env.NODE_PORT_LAB}`);
  
}
bootstrap();
