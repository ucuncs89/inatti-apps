
import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { UserCred } from '@app/authentication/helper/user.decorator';
import { User } from '@app/database/entity/user/user.entity';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { ResultFilterDTO } from '@app/helper/query-helper/result-filter.query-helper';
import { BadRequestException, Body, Controller, Get, Logger, Param, Patch, Query, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SampleSwabService } from './sample-swab.service';
import { LoggerActivityService } from '@app/logger-activity/logger-activity.service'

@ApiTags('Lab API')
@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('sample-swab')
export class SampleSwabController {
  constructor(
    private sampleSwabService: SampleSwabService,
    private activity: LoggerActivityService,
  ) {}

  logger = new Logger('SampleSwabController')

  @Get('/')
  async handoverSampleRef(
    @Query('labId')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: ResultFilterDTO
  ) {
    try {
      return await this.sampleSwabService.getAllSampleLab(idLab, paginate, filter)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  // Because of this, we cannot create sub 1 path
  @Get('/:idswab')
  async getSampleId(
    @Param('idswab')
    swabId: number,
    @Query('labId')
    idLab: string,
  ) {
    try {
      return this.sampleSwabService.getSampleDetail(idLab, swabId);
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Get('/analyze-result/:idswab')
  @ApiResponse({ status: 200, description: "If no result, so it's empty" })
  async getAnalyzeResultSample(
    @Query('labId')
    idLab: string,
    @Param('idswab')
    idSwab: number
  ) {
    try {
      return await this.sampleSwabService.getSampleAnalyzeResult(idLab, idSwab)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Get('/batchs/list')
  async getBatchs(
    @Query('idlab')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ){
    try {
      return await this.sampleSwabService.getListBatch(idLab, paginate, filter)
    } catch (error) {
      this.logger.error(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/batch/:idbatch')
  async getBatchId(
    @Query('idlab')
    idLab: string,
    @Param('idbatch')
    idBatch: number
  ){
    try {
      return await this.sampleSwabService.getBatchDetail(idLab, idBatch)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/history/:idsample')
  async getHistorySample(
    @Param('idsample')
    idSample: number
  ){
    try {
      return await this.sampleSwabService.getHistorySample(idSample)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  /**
   * Submit batch sample, if listIdSample is null, will submit all sample in batch
   * 
   * @param listIdSample nullable
   * @param idbatch 
   * @param res 
   * @returns 
   */
  @Patch('/batch/:idbatch/submit')
  async submitSample(
    @Body()
    listIdSample:number[] | number | null,
    @Param('idbatch')
    idbatch: number, // nullable
    @Request()
    res:any
  ){
    try {
      this.logger.debug(`request batch submit : ${listIdSample}, isArray: ${Array.isArray(listIdSample)}`)
      if(!listIdSample){
        return await this.sampleSwabService.submitBatch(idbatch, res.user.id);
      }

      if(!Array.isArray(listIdSample)){
        listIdSample = [listIdSample]
      }

      return await this.sampleSwabService.submitSample(listIdSample, idbatch, res.user.id)
    } catch (error) {
      this.logger.error(error, 'submitSample');
      throw new BadRequestException(error)
    }
  }

  @Patch('/batch/submit/bulk')
  async bulkSubmitBatch(
    @Body()
    listBatch: number[] | number,
    @Request()
    req:any
  ){
    try {
      if(!listBatch){
        throw new Error('No list batch found');
      }

      if(!Array.isArray(listBatch)){
        listBatch = [listBatch]
      }

      return this.sampleSwabService.bulkSubmitBatch(listBatch, req.user.id)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/sample-submit')
  async sampleSubmit(
    @Body()
    listSwabDetail: number[],
    @UserCred()
    user: User
  ){
    try{
      for (const idSample of listSwabDetail) {
        let labTarget = await this.sampleSwabService.getLabSample(idSample)
        await this.sampleSwabService.setSubmitSample(idSample, user.id)
        
        await this.activity.log({
          type: this.activity.activityList.lab.submitSample,
          swabDetail: idSample,
          params: {
            labId: labTarget.id
          },
          user: user.id,
        })
      }
    } catch(err){
      throw new BadRequestException(err)
    }
  }
}
