import { ApiProperty } from "@nestjs/swagger";

// deprecated: not used
export class SampleLabDTO {
  @ApiProperty()
  labId: string;
}
