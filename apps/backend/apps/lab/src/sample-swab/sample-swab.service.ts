import { LabAnalyzeResult } from '@app/database/entity/mapping/analyze/lab-analyze-result.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository, MoreThanOrEqual, Between, FindConditions, ObjectLiteral, ILike } from 'typeorm';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { ResultFilterDTO } from '@app/helper/query-helper/result-filter.query-helper';
import { LabHandoverBatch } from '@app/database/entity/lab/lab-handover-batch.entity';
import { LabHistoryStatusSample } from '@app/database/entity/lab/lab-history-sample.entity';
import { User } from '@app/database/entity/user/user.entity';
import { StatusSampleService } from '../status-sample/status-sample.service';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { LabHandoverMap } from '@app/database/entity/lab/lab-handover-map.entity';
import { CommonFilterDTO } from '@app/helper/query-filter/type'
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabAnalyzeSessionDetail } from '@app/database/entity/mapping/analyze/lab-analyze-session-detail.entity';

@Injectable()
export class SampleSwabService {
  constructor(
    @InjectRepository(SwabDetail)
    private detailSwabRepository: Repository<SwabDetail>,
    @InjectRepository(LabAnalyzeResult)
    private LabAnalyzeResultRepository: Repository<LabAnalyzeResult>,
    @InjectRepository(LabHandoverBatch)
    private LabHandoverBatchRepository: Repository<LabHandoverBatch>,
    @InjectRepository(LabHistoryStatusSample)
    private LabHistoryStatusSampleRepository: Repository<LabHistoryStatusSample>,
    @InjectRepository(LabHandoverMap)
    private LabHandoverMapRepository: Repository<LabHandoverMap>,
    @InjectRepository(LabAnalyzeSessionDetail)
    private LabAnalyzeSessionDetailRepository: Repository<LabAnalyzeSessionDetail>,
    private statusSampleService: StatusSampleService
  ) { }

  logger = new Logger('SampleSwabService')

  async getAllSampleLab(labId: string, paginate: PaginationOptionsInterface, filter: ResultFilterDTO) {
    let where: FindConditions<SwabDetail>[] | FindConditions<SwabDetail> | ObjectLiteral | string = {
      labTarget: {
        id: labId
      }
    }

    if(filter){
      if(filter.date){
        where.createdAt = filter.date
      }

      if(filter.rangedate){
        where.createdAt = filter.rangedate
      }

      if(filter.result){
        where.resultSwab = filter.result
      }
      if(filter.search){
        where = [
          {
            ...where,
            patient: {
              nik: ILike(`%${filter.search}%`)
            }
          },
          {
            ...where,
            patient: {
              name: ILike(`%${filter.search}%`)
            }
          },
          {
            ...where,
            sampleId: filter.search,
          }
        ]
      }
    }

    let listSampleSwab = await this.detailSwabRepository.find({
      where,
      relations: [
        'patient',
        'refTarget'
      ],
      take: paginate.take,
      skip: paginate.skip,
      order: {
        createdAt: 'DESC'
      }
    })

    for (let i = 0; i < listSampleSwab.length; i++) {
      const sample = listSampleSwab[i];
      sample.history = await this.LabHistoryStatusSampleRepository.find({
        where: {
          sample: sample,
        },
        relations: [
          'status',
        ],
        order: {
          'createdAt': 'DESC'
        },
        take: 1
      })
    }

    let total = await this.detailSwabRepository.count({ where, relations: ['patient', 'refTarget'] })

    return new Pagination({
      results: listSampleSwab,
      total
    }, paginate)
  }

  async getSampleDetail(_labId: string, swabId: number) {
    // Not use labId for search because there will be some issue

    let data = await this.detailSwabRepository.findOne({
      where: {
        id: swabId,
      },
      relations: [
        'patient',
        'patient.provinsi',
        'patient.kabupaten',
        'patient.kecamatan',
        'patient.kelurahan',
        'patient.domisiliProvinsi',
        'patient.domisiliKabupaten',
        'patient.domisiliKecamatan',
        'patient.domisiliKelurahan',
        'history',
        'history.status',
        'productSwab',
        'refTarget'
      ]
    })
    if (!data) {
      return null;
    }
    return data;
  }

  async getSampleAnalyzeResult(labId: string, swabId: number) {
    let result = await this.LabAnalyzeResultRepository.findOne({
      where: {
        sampleSession: {
          sample: swabId,
          lab: labId
        }
      },
      relations: ['sampleSession', 'tool', 'sampleSession.sample', 'sampleSession.sample.patient']
    })

    if(!result){
      throw "Sample not analyze yet"
    }

    // Hack to get attached file only
    const sampleSession = await this.LabAnalyzeSessionDetailRepository.findOne({
      where: {
        lab: labId,
        sample: swabId,
      },
      relations: ['session']
    })
    let sessionPartial = {
      attachedFile: sampleSession.session.attachedFile
    }

    if(!sampleSession){
      throw "Sample are not confirmed yet"
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    result.session = sessionPartial
    return result;
  }

  async getListBatch(labId:string, paginate: PaginationOptionsInterface, filter:CommonFilterDTO){
    // looks like the query too complicated if only using query builder
    // so we use the old way
  
    let paginateQuery = ''
    if(paginate){
      paginateQuery = `limit ${paginate.take} offset ${paginate.skip}`
    }

    let filterQuery = ''
    if(filter){
      if(filter.date){
        filterQuery = `and "createdAt"::date > now() - interval '${filter.date.value}'`
      }
      if(filter.rangedate){
        filterQuery = `and "createdAt"::date between '${filter.rangedate.start}' and '${filter.rangedate.end}'`
      }
    }
    const queryListBatch = (filter:string, pagination:string) => {
      return `select
        lhb.*, count(lhm."batchId") as total_sample,
        count(sd.id) as result_out,
        count(sd2.id) as sample_onlab,
        rd.name as ref_name,
        ld.name as lab_name
      from
        lab_handover_batch lhb
      inner join lab_handover_map lhm on
        lhm."batchId" = lhb.id
      inner join ref_detail rd on rd.id = lhb."createdById"
      inner join lab_detail ld on ld.id = lhb."labTargetId"
      left join (select * from swab_detail sd where sd.status = 'resultOutLab') sd on lhm."swabDetailId" = sd.id
      left join (select * from swab_detail sd where sd.status = 'onLab') sd2 on lhm."swabDetailId" = sd2.id
      where lhb."labTargetId" = $1 ${filter}
      group by lhb.id, lhm."batchId", rd.name, ld.name
      order by "createdAt" desc
      ${pagination}
      `
    }

    let results = await getManager().query(queryListBatch(filterQuery, paginateQuery), [labId])
    let total = await this.LabHandoverBatchRepository.count({
      where: {
        labTarget: new LabDetail(labId)
      }
    })

    return new Pagination({
      results,
      total
    }, paginate)
  }

  async getBatchDetail(labId: string, batchId: number){
    return this.LabHandoverBatchRepository.findOne({
      where: {
        id: batchId,
        labTarget: labId
      },
      relations: [
        'createdBy',
        'labTarget',
        'mapBatch',
        'mapBatch.swabDetail',
        'mapBatch.swabDetail.history',
        'mapBatch.swabDetail.history.status',
        'mapBatch.swabDetail.patient',
      ]
    })
  }

  async getHistorySample(idSample: number){
    let history = await this.LabHistoryStatusSampleRepository.find({
      where: {
        sample: idSample
      },
      relations: [
        'status',
        'createdBy'
      ]
    })
    
    
    // remove credential user
    return history.map((v) => {
      let userCreator = new User();
      userCreator.username = v.createdBy.username
      userCreator.fullname = v.createdBy.fullname
      v.createdBy = userCreator
      return v
    })
  }

  async bulkSubmitBatch(listBatch: number[], userId: string){
    for (const batchId of listBatch) {
      await this.submitBatch(batchId, userId)
    }
  }

  async submitBatch(idBatch: number, userLoggedIn: string){
    let listSample = await this.LabHandoverMapRepository.find({
      where: {
        batch: idBatch,
      },
      relations: [
        'swabDetail'
      ]
    })

    let listIdSample = listSample.map((v) => v.swabDetail.id)
    return await this.submitSample(listIdSample, idBatch, userLoggedIn)
  }

  async submitSample(listIdSample: number[], idBatch:number, userLoggedIn: string){
    for (const idSample of listIdSample) {
      try{
        await this.statusSampleService.setStatusSample(idSample, LabStatusSample.statusSubmitted, userLoggedIn)
      }catch(error){
        throw error;
      }
    }
    await this.updateHandoverBatchStatus(idBatch)
  }

  // Everytime update submit sample
  // check status batch is submitted or not
  async updateHandoverBatchStatus(idBatch: number) {
    let listMap = await this.LabHandoverMapRepository.find({
      where: {
        batch: idBatch
      },
      relations: [
        'swabDetail',
        'swabDetail.history',
        'swabDetail.history.status'
      ]
    })
    let statusSubmit = 2
    let isSubmittedAll = listMap.every((v) => v.swabDetail.history.some((v) => v.status.id === statusSubmit))
    if(isSubmittedAll){
      await this.LabHandoverBatchRepository.update({
        id: idBatch
      }, {
        isSubmitted: isSubmittedAll
      })
    }
  }

  async setSubmitSample(sampleId:number, userId: string){
    return await this.statusSampleService.setStatusSample(sampleId, LabStatusSample.statusSubmitted, userId)
  }

  async getLabSample(sampleId: number){
    return await this.detailSwabRepository.findOne({
      where: {
        id: sampleId
      },
      relations: [
        'labTarget'
      ]
    })
  }
}
