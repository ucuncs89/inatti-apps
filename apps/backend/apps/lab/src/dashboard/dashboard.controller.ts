import { BadRequestException, Get, Query } from '@nestjs/common';
import { RequireAuth } from '../helper/auth.decorator';
import { LabApi } from '../helper/lab-controller.decorator';
import { DashboardService } from './dashboard.service';

@RequireAuth()
@LabApi('dashboard')
export class DashboardController {
  constructor(
    private dashboardService: DashboardService
  ){}

  @Get('/')
  async getDashboard(
    @Query('idlab')
    idLab: string,
    @Query('force')
    isForce?: boolean,
  ){
    try {
      return await this.dashboardService.getDashboard(idLab, isForce)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }
}
