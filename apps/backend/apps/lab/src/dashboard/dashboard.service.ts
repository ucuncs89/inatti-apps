import { ReportCacheLab } from '@app/database/entity/report/report-cache-lab.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import * as dayjs from "dayjs";
import { DashboardData } from './type';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(ReportCacheLab)
    private ReportCacheLabRepository: Repository<ReportCacheLab>,
  ){}

  // https://day.js.org/docs/en/manipulate/add
  MAX_AGE_REPORT = [1, 'day']

  async getDashboard(idLab:string, isForce?: boolean){
    let reportCache = await this.ReportCacheLabRepository.findOne({
      where: {
        lab: {
          id: idLab
        }
      }
    })
    if(!reportCache || this.isOld(reportCache.updatedAt) || isForce){
      let result = await this.generateDashboard(idLab);
      await this.saveData(result, idLab)
      return this.getDashboard(idLab);
    }

    return reportCache;
  }

  isOld(date:Date){
    let updateDate = dayjs(date)
    //@ts-ignore
    let expired = updateDate.add(+this.MAX_AGE_REPORT[0], this.MAX_AGE_REPORT[1] + "")
    return dayjs().isAfter(expired)
  }

  private async generateDashboard(idLab:string){
    console.log('generate dashboard')
    const resultData = new DashboardData();
    
    resultData.patientRegistered = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where lab_target = $1', [idLab])
    resultData.sampleProcessed = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where lab_target = $1 and result_swab is not null', [idLab])
    resultData.totalSampleNegatif = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where lab_target = $1 and result_swab = $2', [idLab, 'negatif'])
    resultData.totalSamplePositif = await this.runQueryTotal('total', 'select count(*) as total from swab_detail where lab_target = $1 and result_swab = $2', [idLab, 'positif'])
    resultData.recordSampleResult = await this.runQueryGroupResult(idLab)
    resultData.recordSampleRef = await this.runQueryGroupRef(idLab)
    return resultData;
  }

  private async saveData(result:any, idLab:string){
    let report = await this.ReportCacheLabRepository.findOne({
      where: {
        lab: {
          id: idLab
        }
      }
    })

    if(!report){
      report = new ReportCacheLab()
      report.data = result;
      report.lab = new LabDetail(idLab)
      report.updatedAt = new Date()
    } else {
      report.data = result;
    }
    report = await this.ReportCacheLabRepository.save(report)

    return report;
  }

  async runQueryTotal(column:string, query:string, params: any[]){
    let result = await getManager().query(query, params)
    
    if(!result) return null
    if(result.length < 1) return null

    return result[0][column]
  }

  async runQueryGroupResult(idLab:string){
    let result = {}
    const intervals = ['day', 'month', 'year']
    for (const interval of intervals) {
      result[interval] = await this.runQueryGroupResultInterval(idLab, interval)
    }
    return result;
  }

  async runQueryGroupResultInterval(idLab, interval){
    let where = [
      `lab_target = '${idLab}'`,
      `result_swab is not null`
    ]

    if(interval === 'day'){
      let from = dayjs().subtract(2, 'month')
      where.push(`created_at >= '${from.format('YYYY-MM-DD')}'`)
    }

    let query = `select
      count(*) as value,
      result_swab as group,
      date_trunc('${interval}', created_at)::date::varchar as xaxis
    from
      swab_detail sd
    where
       ${where.join(" and ")}
    group by
      xaxis, result_swab
    order by xaxis asc`;

    let result = await getManager().query(query);
    return result;
  }

  async runQueryGroupRef(idLab: string){
    let result = {}
    const intervals = ['day', 'month', 'year']
    for (const interval of intervals) {
      result[interval] = await this.runQueryGroupRefInterval(idLab, interval)
    }
    return result;
  }

  async runQueryGroupRefInterval(idLab:string, interval: string){
    let where = [
      `lab_target = '${idLab}'`
    ]
    if(interval === 'day'){
      let from = dayjs().subtract(2, 'month')
      where.push(`sd.created_at >= '${from.format('YYYY-MM-DD')}'`)
    }

    let query = `select
      count(*) as value,
      rd.id, rd."name" as group,
      date_trunc('${interval}', sd.created_at)::date::varchar as xaxis
    from
      swab_detail sd
    inner join ref_detail rd on rd.id = sd.ref_target 
    where
      ${where.join(" and ")}
    group by
      xaxis, rd.id, rd."name" 
    order by xaxis asc`
    
    let result = await getManager().query(query);
    return result;
  }
}
