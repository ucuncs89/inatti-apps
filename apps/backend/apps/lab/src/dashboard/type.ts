export class DashboardData {
  patientRegistered: number;
  sampleProcessed: number;
  totalSampleNegatif: number;
  totalSamplePositif: number;
  recordSampleResult: any;
  recordSampleRef: any;
}