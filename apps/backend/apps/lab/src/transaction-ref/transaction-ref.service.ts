import { RefDetail } from '@app/database/entity/ref/ref-detail.entity';
import { SwabOrder } from '@app/database/entity/swab-data/order.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFormatOrderXlsx } from '@app/helper/report-xlsx/common-format';
import { deepValueToXlsx } from '@app/helper/report-xlsx/to-xlsx';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, ILike, Repository } from 'typeorm';
import { FilterHistoryRef } from './type';
import * as dayjs from 'dayjs'
@Injectable()
export class TransactionRefService {
  constructor(
    @InjectRepository(RefDetail)
    private RefDetailRepository: Repository<RefDetail>,
    @InjectRepository(SwabOrder)
    private SwabOrderRepository: Repository<SwabOrder>,
  ){}

  getReference(){
    return this.RefDetailRepository.find()
  }

  getFilterAndWhere(idLab:string, idRef:string, filter:FilterHistoryRef){
    let whereParam:any = {
      detailSwab: {
        refTarget: {
          id: idRef
        },
        labTarget: {
          id: idLab
        }
      },
    }
    if(filter){
      if(filter.dateCreatedEnd && filter.dateCreatedStart){
        // offset 1 day
        let endDate = dayjs(filter.dateCreatedEnd)
        endDate = endDate.add(1, 'day');
        whereParam.createdAt = Between(filter.dateCreatedStart, endDate.format('YYYY-MM-DD'))
      }
      if(filter.invoiceId){
        whereParam.orderTrx = {
          invId: ILike(`%${filter.invoiceId}%`)
        }
      }
    }

    const relations = [
      'detailSwab',
      'detailSwab.patient',
      'detailSwab.refTarget',
      'detailSwab.labTarget',
      'orderTrx',
      'reference',
      'product',
      'methodCash'
    ]

    return {
      whereParam,
      relations
    }
  }

  async getHistoryTransaction(idLab: string, idRef: string, filter: FilterHistoryRef, paginate: PaginationOptionsInterface){
    const {relations, whereParam} = this.getFilterAndWhere(idLab, idRef, filter)
    
    let results = await this.SwabOrderRepository.find({
      skip: paginate.skip,
      take: paginate.take,
      where: whereParam,
      relations,
      order: {
        createdAt: 'DESC'
      }
    })
    const total = await this.SwabOrderRepository.count({where: whereParam, relations})
    return new Pagination({
      results,
      total,
    }, paginate)
  }

  async getHistoryTransactionExport(idLab: string, idRef: string, filter: FilterHistoryRef, res: any) {
    const {relations, whereParam} = this.getFilterAndWhere(idLab, idRef, filter)
    let result = await this.SwabOrderRepository.find({
      where: whereParam,
      relations
    })

    await deepValueToXlsx(res, result, CommonFormatOrderXlsx)
  }
}
