import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { BadRequestException, Controller, Get, Query, Response } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../helper/auth.decorator';
import { TransactionRefService } from './transaction-ref.service';
import { FilterHistoryRef } from './type';

@ApiTags('Lab API')
@RequireAuth()
@Controller('transaction-ref')
export class TransactionRefController {
  constructor(
    private transactionRefService: TransactionRefService
  ){}

  @Get('/refs')
  async getReference(){
    try {
      return await this.transactionRefService.getReference()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/history-trx')
  async getHistoryTransaction(
    @Query('idlab')
    idLab: string,
    @Query('idref')
    idRef: string,
    @Query('filter')
    filter: FilterHistoryRef,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
  ){
    try {
      return await this.transactionRefService.getHistoryTransaction(idLab, idRef, filter, paginate)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Get('/history-trx/export')
  async getHistoryTransactionExport(
    @Query('idlab')
    idLab: string,
    @Query('idref')
    idRef: string,
    @Query('filter')
    filter: FilterHistoryRef,
    @Response()
    res: any
  ){
    try {
      await this.transactionRefService.getHistoryTransactionExport(idLab, idRef, filter, res)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }
}
