import { ApiProperty } from "@nestjs/swagger";

export class FilterHistoryRef {
  @ApiProperty({required: false, name: 'filter[dateCreatedStart]', example: 'YYYY-MM-DD'})
  dateCreatedStart: string;
  @ApiProperty({required: false, name: 'filter[dateCreatedEnd]', example: 'YYYY-MM-DD'})
  dateCreatedEnd: string;
  @ApiProperty({required: false, name: 'filter[invoiceId]'})
  invoiceId: string;
}