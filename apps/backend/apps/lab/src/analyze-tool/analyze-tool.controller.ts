import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { AnalyzeTool } from '@app/database/entity/mapping/analyze/analyze-tool.entity';
import { BadRequestException, Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@ApiTags("Lab API")
@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('analyze-tool')
export class AnalyzeToolController {
  constructor(
    @InjectRepository(AnalyzeTool)
    private AnalyzeToolRepository: Repository<AnalyzeTool>,
  ) { }

  @Get('/')
  async getAnalyzeTool() {
    try {
      return await this.AnalyzeToolRepository.find()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
