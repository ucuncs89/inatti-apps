export class SendResult2NarDTO {
  cookies: string;
  labId: string;
  listSample: string[];
  isForce: boolean; // force send to nar even if sample is already sent
}