import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { NarPcr, ResponseCheckNik } from '@inatti/nar-api-unofficial'
import { SendResult2NarDTO } from './type';
import { GenderEnum, ResultSwabEnum } from '@app/database/entity/swab-data/type';
import { NarSample } from '@app/database/entity/nar/nar-sample.entity';
import { LoggerFileNar } from './logger-nar';

@Injectable()
export class NarLabIntegration {
  constructor(
    @InjectRepository(SwabDetail)
    private SwabDetailRepository: Repository<SwabDetail>,
    @InjectRepository(NarSample)
    private NarSampleRepository: Repository<NarSample>,
  ) { }

  logger = new Logger('NarLabIntegration')

  formatDateNar(value: string | Date) {
    if (!value) return null;
    return dayjs(value).format("YYYY-MM-DD HH:mm:ss")
  }

  async getSwabDetail(idSample: number, idLab: string): Promise<SwabDetail> {
    return await this.SwabDetailRepository.findOneOrFail({
      where: {
        id: idSample,
        labTarget: idLab
      },
      relations: [
        'refTarget',
        'labTarget',
        'patient',
        'patient.provinsi',
        'patient.kabupaten',
        'patient.kecamatan',
        'patient.kelurahan',
        'labAnalyzeSession',
        'labAnalyzeSession.session',
        'narSample'
      ]
    })
  }

  loggerNarFile:LoggerFileNar

  async sendSampleToNar(
    params: SendResult2NarDTO,
    cb:{
      log: (params:any, type?:'log' | 'success' | 'error' | 'json') => void,
      finish: () => void,
      updateSample: (sampleId: string, isSuccess: boolean, message?: string) => void
    }
  ){
    this.loggerNarFile = new LoggerFileNar()

    this.loggerNarFile.log(`Lab ${params.labId} send sample to NAR`)
    this.loggerNarFile.log(`Total sample ${params.listSample.length}`)

    const printIdentity = (sample:SwabDetail) => `${sample.patient.name} [${sample.patient.nik}, ${sample.patient.datebirth}]`
    const formatDate = (date: any) => date ? dayjs(date).format('DD-MM-YYYY'): ''

    let narPcr = new NarPcr();
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    narPcr.log = (...params) => {
      this.loggerNarFile.log(params)
    }
    narPcr.responseAPICallback = ((href:string, body:any) => {
      this.loggerNarFile.log(`Result API ${href} : ${body}`)
    })

    cb.log(`Mencoba login dengan cookies : ${params.cookies}`, 'log')
    try{
      let cookiesParse = JSON.parse(params.cookies)
      for (const cookie of cookiesParse) {
        await narPcr.saveCookie(`${cookie.name}=${cookie.value}`)
      }
      cb.log(`Berhasil login`, 'success')
    } catch(err){
      this.logger.error(`Gagal Login : ${err.message}`)
      cb.log(`Gagal Login : ${err.message}`, 'error')
      cb.finish()
      return;
    }
     
    for (const sampleId of params.listSample) {
      let sample = await this.getSwabDetail(+sampleId, params.labId)
      cb.log(`Proses Sample ${await printIdentity(sample)}`, 'log')

      if(!sample.resultSwab){
        cb.log(`Gagal proses sample ${printIdentity(sample)} Karena hasil belum di publish`, 'error')
        cb.updateSample(sampleId, false, 'Hasil belum di publish')
        continue;
      }

      if(sample.narSample?.isSuccess && !params.isForce){
        cb.log(`Sample ${printIdentity(sample)} Telah di masukkan ke NAR, batal aksi`, 'error')
        cb.updateSample(sampleId, true, 'Sudah dimasukkan')
        continue;
      }
      
      const resutlNarPayload = {
        id: '',
        sess_status: '',
        ct_value: '',
        tujuan_detail: '1', // Pelaku Perjalanan
        asal_data: '1', 
        tujuan: '3', // skrinning
        jns_identitas: '1', // NIK
        sgtf: "1",
        wgs: "1",
        sts_ppln: "1",
        sgtf_labtuju: "",
        wgs_labtuju: "",

        nik: sample.patient.nik,
        jkel: sample.patient.gender === GenderEnum.male ? 'L' : 'P',
        nama: sample.patient.name,
        tgl_lahir: formatDate(sample.patient.datebirth),
        telp: sample.patient.phonenumber,
        alamat: sample.patient.address,
        propinsi: sample.patient.provinsi?.id.toString() ?? '',
        kabupaten: sample.patient.kabupaten?.id.toString() ?? '',
        kecamatan: sample.patient.kecamatan?.id.toString() ?? '',
        desa: sample.patient.kelurahan?.id.toString() ?? '',
        rw: sample.patient.kelurahanRw,
        rt: sample.patient.kelurahanRt,
        alamat_domisili_checked: 'on',
        alamat_domisili: sample.patient.addressDomisili ?? '',
        no_lab: sample.sampleId,
        hsl_lab: sample.resultSwab === ResultSwabEnum.negatif ? 'NEGATIF' : 'POSITIF',
        
        tgl_pengambilan: formatDate(sample.sendToLabDate),
        tgl_terima: formatDate(sample.sendToLabDate),
        tgl_periksa: formatDate(sample.labAnalyzeSession[0]?.session.createdAt),
        tgl_hasil: formatDate(sample.resultSentAt),
      }
      cb.log({payload: resutlNarPayload}, 'json')

      try{
        let result:ResponseCheckNik = await narPcr.AddResult(resutlNarPayload)
        cb.log(`Pasien ${printIdentity(sample)} Sukses dikirim ke NAR, Periksa di : ${result.data.next_url}`, 'success')
        await this.inputNarDone(sample, true, null)
        cb.updateSample(sampleId, true)
      }catch(err){
        const errMessage = err.message
        this.logger.error(`Gagal kirim Pasien ${printIdentity(sample)} : ${errMessage}`)
        cb.log(`Gagal kirim Pasien ${printIdentity(sample)} : ${errMessage}`, 'error')
        await this.inputNarDone(sample, false, errMessage)
        cb.updateSample(sampleId, false, errMessage)
      }
    }
    this.loggerNarFile.log(`Finish send sample to NAR`)
    this.loggerNarFile.end()
    cb.finish()
  }

  async inputNarDone(sample: SwabDetail, isSuccess: boolean, message?: string){
    let narSample = await this.NarSampleRepository.findOne({swabDetail: sample})
    if(!narSample) {
      narSample = new NarSample();
    }
    narSample.swabDetail = sample
    narSample.isSuccess = isSuccess
    narSample.message = message

    this.NarSampleRepository.save(narSample)
  }
  
}
