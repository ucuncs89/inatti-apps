import * as fs from 'fs'

export class LoggerFileNar {
  constructor(name?: string){
    if(!name){
      name = 'narpcr'
    }
    this.createFile(name)
  }
  stream: fs.WriteStream
  nameFile = ''

  createFile(name?: string){
    let now = new Date();
    // create folder logs at root
    if (!fs.existsSync('./logs')){
      fs.mkdirSync('./logs');
    }

    this.nameFile = `./logs/log-${name}-${now.getTime()}-${now.toISOString()}.logs`

    this.stream = fs.createWriteStream(this.nameFile, {flags: 'a'})
  }

  log(params:any){
    let now = new Date().toISOString()
    let content = `${now}: ${params}`
    this.stream.write(content + '\n')
  }

  end(){
    this.stream.end()
  }
}