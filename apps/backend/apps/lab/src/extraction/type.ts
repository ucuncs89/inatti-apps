import { ControlTypeMapping } from "@app/database/entity/mapping/templateMapping";
import { ApiProperty } from "@nestjs/swagger";


export class ExtractionMappingDTO {
  @ApiProperty()
  locateRow: string;
  
  @ApiProperty()
  locateColumn: string;

  @ApiProperty({description: "sample id"})
  sample?: number;

  @ApiProperty()
  isControl?: boolean;

  @ApiProperty({enum: ControlTypeMapping})
  controlType?: ControlTypeMapping

  @ApiProperty()
  controlName?: string
}

export class ExtractionSessionCreateDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  rowTotal: number;

  @ApiProperty()
  columnTotal: number;

  @ApiProperty()
  lab: string;

  @ApiProperty({isArray: true, type: ExtractionMappingDTO})
  mapping: ExtractionMappingDTO[]
}

export class UpdateSessionDetailDTO{
  @ApiProperty()
  locateRow: string;
  
  @ApiProperty()
  locateColumn: string;
}

export interface MapUpdatePositionDTO {
  new: any[],
  del: any[],
  stay: any[]
}

class CellPosition {
  col: number
  row: number
  sampleId: number
}

export class UpdateSessionPositionDTO implements MapUpdatePositionDTO{
  @ApiProperty()
  new: CellPosition[]
  @ApiProperty()
  del: number[]
  @ApiProperty()
  stay: CellPosition[]
}