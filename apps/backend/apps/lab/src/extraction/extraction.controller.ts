import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { PaginationOptionsInterface } from '@app/helper/paginate';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ExtractionService } from './extraction.service';
import { ExtractionSessionCreateDTO, UpdateSessionDetailDTO, UpdateSessionPositionDTO } from './type';

@ApiTags("Lab API")
@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('extraction')
export class ExtractionController {
  constructor(
    private extractionService: ExtractionService
  ) { }

  @Get('/sessions')
  async getAllSessions(
    @Query('idlab')
    idLab: string,
    @Query('paginate', new PaginationPipe())
    paginate: PaginationOptionsInterface,
    @Query('filter', new FilterQueryPipe())
    filter: CommonFilterDTO
  ) {
    try {
      return await this.extractionService.getSessions(idLab, paginate, filter)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Get('/session/:id')
  async getSession(
    @Param('id')
    idSession: number,
    @Query('idlab')
    idLab: string
  ) {
    try {
      return await this.extractionService.getSession(idLab, idSession)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/sample-available')
  async getSampleAvailable(
    @Query('idlab')
    idLab: string
  ) {
    try {
      return await this.extractionService.getSampleAvailable(idLab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/session')
  async addSession(
    @Body()
    body: ExtractionSessionCreateDTO,
    @Request()
    req: any
  ) {
    try {
      let res = await this.extractionService.addSession(body);
      await this.extractionService.setSampleToMapped(body, req.user.id)

      return res;
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Patch('/session/:id/extract')
  async ExtractSession(
    @Query('idlab')
    idLab: string,
    @Param('id')
    idSession: number,
    @Request()
    req:any,
    @Query('isExtract')
    isExtract: '1' | '0'
  ){
    try {
      await this.extractionService.extractSession(idLab, idSession, isExtract)
      let res
      if(isExtract === '1'){
        res = await this.extractionService.setSampleToExtracted(idSession, req.user.id)
      } else if (isExtract === '0'){
        res = await this.extractionService.setSampleToNotExtracted(idSession, req.user.id)
      }
      return res;
    } catch (error) {
      console.log(error);
      
      throw new BadRequestException(error)
    }
  }

  @Patch('/session/extract-bulk')
  async bulkExtractSessions(
    @Query('idlab')
    idLab: string,
    @Request()
    req:any,
    @Body()
    sessionIds: number[],
    @Query('isExtract')
    isExtract: '1' | '0'
  ){
    try {
      if(sessionIds.length < 1) throw new Error('No session id found')
      for (const idSession of sessionIds) {
        await this.extractionService.extractSession(idLab, idSession, isExtract)
        if(isExtract === '1'){
          await this.extractionService.setSampleToExtracted(idSession, req.user.id)
        } else if (isExtract === '0'){
          await this.extractionService.setSampleToNotExtracted(idSession, req.user.id)
        }
      }
    } catch (error) {
      console.log(error);
      
      throw new BadRequestException(error)
    }
  }


  @ApiOperation({ description: "API untuk ganti posisi sample extraction mapping" })
  @Patch('/session/update-position/:idsession/:idSessionDetail')
  async updateSessionDetail(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number,
    @Param('idSessionDetail')
    idSessionDetail: number,
    @Body()
    body: UpdateSessionDetailDTO,
  ) {
    try {
      return await this.extractionService.updateSessionDetail(idLab, idSessionDetail, idSession, body)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Delete('/session/delete-position/:idsession/:idSessionDetail') 
  async deleteSessionDetail(
    @Query('idlab')
    idLab: string,
    @Param('idsession')
    idSession: number,
    @Param('idSessionDetail')
    idSessionDetail: number,
  ){
    try {
      return await this.extractionService.deleteSessionDetail(idLab, idSession, idSessionDetail)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Patch('/session/:id/update-position')
  async updateSessionDetailPosition(
    @Query('idlab')
    idLab: string,
    @Param('id')
    idSession: number,
    @Body()
    body: UpdateSessionPositionDTO,
    @Request()
    req:any
  ){
    try {
      return await this.extractionService.updateSessionPosition(idLab, idSession, body, req.user.id)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }
}
