import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { LabExtractionSessions } from '@app/database/entity/mapping/extraction/lab-extraction-session';
import { LabExtractionSessionDetail } from '@app/database/entity/mapping/extraction/lab-extraction-session-detail';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { Pagination, PaginationOptionsInterface } from '@app/helper/paginate';
import { CommonFilterDTO } from '@app/helper/query-filter/type';
import { QueryUsePaginationAndFilter } from '@app/helper/query-helper/pagination-filter.query-helper';
import { LoggerActivityService } from '@app/logger-activity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository, MoreThanOrEqual, Between, FindConditions, ObjectLiteral, ILike } from 'typeorm';
import { StatusSampleService } from '../status-sample/status-sample.service';
import { ExtractionMappingDTO, ExtractionSessionCreateDTO, UpdateSessionDetailDTO, UpdateSessionPositionDTO } from './type';

@Injectable()
export class ExtractionService {
  constructor(
    @InjectRepository(LabExtractionSessions)
    private labExtractSessionRepository: Repository<LabExtractionSessions>,
    @InjectRepository(LabExtractionSessionDetail)
    private labExtractionSessionDetailRepository: Repository<LabExtractionSessionDetail>,
    @InjectRepository(SwabDetail)
    private DetailSwabRepository: Repository<SwabDetail>,
    private statusSampleService:StatusSampleService,
    private activity: LoggerActivityService
  ) { }

  logger = new Logger('ExtractionService')

  async getSessions(idLab: string, paginate: PaginationOptionsInterface, filter: CommonFilterDTO) {
    let where:FindConditions<LabExtractionSessions>[] | FindConditions<LabExtractionSessions> | ObjectLiteral | string = {
      lab: idLab,
    }

    if(filter){
      if(filter.date){ 
        where.createdAt = filter.date
      }

      if(filter.rangedate){
        where.createdAt = filter.rangedate
      }

      if(filter.search){
        where.name = ILike(`%${filter.search}%`)
      }
    }

    let sessions = await this.labExtractSessionRepository.find({
      where,
      relations: [
        'mapping'
      ],
      take: paginate.take,
      skip: paginate.skip,
      order: {
        createdAt: 'DESC'
      }
    })

    for (let i = 0; i < sessions.length; i++) {
      const session = sessions[i];
      session['total_mapping'] = await this.labExtractionSessionDetailRepository.count({
        where: {
          session: session.id
        }
      })
    }

    let total = await this.labExtractSessionRepository.count({ where })

    return new Pagination({
      results: sessions,
      total
    }, paginate)
  }

  async getSession(idLab: string, idSession: number) {
    return await this.labExtractSessionRepository.findOne({
      where: {
        lab: idLab,
        id: idSession
      },
      relations: [
        'mapping',
        'mapping.sample',
        'mapping.sample.history',
        'mapping.sample.history.status',
        'mapping.sample.patient'
      ]
    })
  }

  /**
   * Find sample that have no relation to tbale LabExtractionSessionDetail
   * @param idLab 
   * @returns 
   */
  async getSampleAvailable(idLab: string) {
    let querySwabEmpty = this.DetailSwabRepository
      .createQueryBuilder('ds')
      .leftJoin('ds.labExtraction', 'LabExtractionSessionDetail')
      .leftJoinAndSelect('ds.history', 'history')
      .leftJoinAndSelect('history.status', 'status')
      .leftJoinAndSelect('ds.patient', 'PatientSwab')
      .where('LabExtractionSessionDetail.id is null')
      .andWhere('ds.labTarget = :idLab', { idLab: idLab })
      .andWhere('history.status = :statusHistory', {statusHistory : LabStatusSample.statusSubmitted.id})
      .orderBy('history.createdAt', 'DESC')

    // tips : use querySwabEmpty.getSql() to get sql query
    return await querySwabEmpty.getMany();
  }

  async addSession(dataDTO: ExtractionSessionCreateDTO) {
    return await getManager().transaction(async transactionEntityManager => {
      let session = new LabExtractionSessions()
      session.name = dataDTO.name
      session.lab = new LabDetail(dataDTO.lab)
      session.rowTotal = dataDTO.rowTotal
      session.columnTotal = dataDTO.columnTotal

      session = await transactionEntityManager.save(session)

      let mappingSession = dataDTO.mapping.map((v: ExtractionMappingDTO): LabExtractionSessionDetail => {
        let detailSession = new LabExtractionSessionDetail()
        detailSession.lab = new LabDetail(dataDTO.lab)
        detailSession.sample = new SwabDetail(v.sample)
        detailSession.session = session
        detailSession.locateColumn = v.locateColumn
        detailSession.locateRow = v.locateRow
        detailSession.isControl = v.isControl
        detailSession.controlType = v.controlType
        detailSession.controlName = v.controlName
        return detailSession
      })
      await transactionEntityManager.save(mappingSession)
      return session
    })
  }

  async setSampleToMapped(dataDTO: ExtractionSessionCreateDTO, userId: string){
    for (const mapping of dataDTO.mapping) {
      try{
        await this.statusSampleService.setStatusSample(mapping.sample, LabStatusSample.statusMappedEkstration, userId)
      }catch(error){
        throw error
      }
    }
    return true;
  }

  async setSampleToExtracted(idSession: number, userId: string){
    let listSessionDetail = await this.labExtractionSessionDetailRepository.find({
      where: {
        session: idSession
      },
      relations: ['sample', 'session']
    })

    for (const sessionDetail of listSessionDetail) {
      await this.statusSampleService.setStatusSample(sessionDetail.sample.id, LabStatusSample.statusEkstracted, userId)
    }
  }

  async setSampleToNotExtracted(idSession: number, userId: string){
    let listSessionDetail = await this.labExtractionSessionDetailRepository.find({where: {session: idSession}, relations: ['sample']})

    for (const sessionDetail of listSessionDetail) {
      await this.statusSampleService.removeStatusSample(sessionDetail.sample.id, LabStatusSample.statusEkstracted, userId)
    }
  }

  async extractSession(idLab:string, idSession: number, isExtracted: '1' | '0'){
    const whereObject = {
      id: idSession,
      lab: new LabDetail(idLab)
    }
    let session = await this.labExtractSessionRepository.findOne(whereObject)
    if(!session){
      throw new Error('Session Id ${idSession} not found')
    }
    if(isExtracted === '1'){
      session.isExtracted = true;
      session.dateExtracted = new Date
    } else if (isExtracted === '0'){
      session.isExtracted = false;
      session.dateExtracted = null;
    }

    let updateSession = await this.labExtractSessionRepository.save(session)
    if(!updateSession){
      throw new Error(`Failed update session ${idSession} to extracted`)
    }
  }

  async updateSessionDetail(idLab: string, idSessionDetail: number, idSession: number, body: UpdateSessionDetailDTO) {
    // check is new position available in table
    let availablePosition = await this.labExtractionSessionDetailRepository.find({
      where: {
        locateRow: body.locateRow,
        locateColumn: body.locateColumn,
        session: idSession
      }
    })
    if (availablePosition.length > 0) {
      throw new Error('New position is available')
    }
    return this.labExtractionSessionDetailRepository.update({
      id: idSessionDetail,
      lab: new LabDetail(idLab),
      session: new LabExtractionSessions(idSession)
    }, {
      locateColumn: body.locateColumn,
      locateRow: body.locateRow
    })
  }

  async deleteSessionDetail(idLab: string, idSession: number, idSessionDetail:number){
    return this.labExtractionSessionDetailRepository.delete({
      id: idSessionDetail,
      lab: new LabDetail(idLab),
      session: new LabExtractionSessions(idSession)
    })
  }

  async updateSessionPosition(idlab: string, idSession: number, payload: UpdateSessionPositionDTO, userId: string){
    let session = await this.labExtractSessionRepository.findOne({
      id: idSession
    })

    if(Array.isArray(payload.new)){
      for (const cell of payload.new) {
        this.logger.debug(`process new ${JSON.stringify(cell)}`);
        
        let sessionDetail = new LabExtractionSessionDetail()
        sessionDetail.sample = new SwabDetail(cell.sampleId)
        sessionDetail.locateColumn = cell.col + ""
        sessionDetail.locateRow = cell.row + ""
        sessionDetail.lab = new LabDetail(idlab)
        sessionDetail.session = new LabExtractionSessions(idSession)
        await this.labExtractionSessionDetailRepository.insert(sessionDetail)

        await this.statusSampleService.setStatusSample(cell.sampleId, LabStatusSample.statusMappedEkstration, userId)

        if(session.isExtracted){
          await this.statusSampleService.setStatusSample(cell.sampleId, LabStatusSample.statusEkstracted, userId)
        }
      }
    }

    if(Array.isArray(payload.stay)){
      let sessionDetails = await this.labExtractionSessionDetailRepository.find({
        where: {
          session: idSession
        },
        relations: ['sample']
      })
      this.logger.debug(`Length session detail ${idSession} ${sessionDetails.length}`);
      
      // all in 'stay' will update row and col
      for (const cell of payload.stay) {
        this.logger.debug(`process stay ${JSON.stringify(cell)}`);
        let detail = sessionDetails.find((v) => v.sample.id === cell.sampleId)
        if(detail){
          this.logger.debug(`update ${detail.id}`);
          
          detail.locateColumn = cell.col + ""
          detail.locateRow = cell.row + ""
          await this.labExtractionSessionDetailRepository.save(detail)
        }
      }

      for (const sampleId of payload.del) {
        this.logger.debug(`delete sample Id : ${sampleId}`);
        await this.labExtractionSessionDetailRepository.delete({sample: new SwabDetail(sampleId)})
        await this.statusSampleService.removeStatusSample(sampleId, LabStatusSample.statusEkstracted, userId)
      }
      return true
    }
  }
}
