import { LabRoleUser } from '@app/database/entity/lab/lab-role-user.entity';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class LabService {
  constructor(
    @InjectRepository(LabRoleUser)
    private labRoleUserRepository: Repository<LabRoleUser>
    ){}
  getRegisteredLabUser(user: User){
    return this.labRoleUserRepository.find({
      where: {
        userId: user
      },
      withDeleted: true
    })
  }
  getRoleUser(idUser: string, idLab: string) {
    return this.labRoleUserRepository.findOneOrFail({
      where: {
        userId: idUser,
        labId: idLab
      },
      withDeleted: true
    })
  }
}
