import { LabHistoryStatusSample } from '@app/database/entity/lab/lab-history-sample.entity';
import { LabStatusSample } from '@app/database/entity/lab/lab-status-sample.entity';
import { SwabDetail } from '@app/database/entity/swab-data/swab-detail.entity';
import { User } from '@app/database/entity/user/user.entity';
import { LoggerActivityService } from '@app/logger-activity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StatusSampleService {
  constructor(
    @InjectRepository(LabHistoryStatusSample)
    private LabHistoryStatusSampleRepository: Repository<LabHistoryStatusSample>,
    @InjectRepository(LabStatusSample)
    private LabStatusSampleRepository: Repository<LabStatusSample>,
    private activity: LoggerActivityService
  ){}
  logger = new Logger('StatusSampleService')

  async setStatusSample(sampleId: number, status:LabStatusSample, userLoggedIn: string){
    this.logger.debug(`Process sample ${sampleId} to status ${status.id}`)

    let statusSample = await this.LabStatusSampleRepository.findOne(status)

    let history = new LabHistoryStatusSample()
    history.sample = new SwabDetail(sampleId)
    history.createdBy = new User(userLoggedIn)
    history.status = statusSample
    
    history = await this.LabHistoryStatusSampleRepository.save(history)

    await this.activity.log({
      swabDetail: sampleId,
      type: this.activity.activityList.lab.updateStatusSample,
      params: {
        status: statusSample.name
      },
      user: userLoggedIn
    })

    this.logger.debug(`Sample ${sampleId} Success add status ${status.id}`)
  }

  async removeStatusSample(sampleId: number, status: LabStatusSample, userLoggedIn: string) {
    this.logger.debug(`Process sample ${sampleId} to delete status ${status.id}`)

    let statusSample = await this.LabStatusSampleRepository.findOne(status)

    let res = await this.LabHistoryStatusSampleRepository.delete({
      sample: new SwabDetail(sampleId),
      status: status
    })

    await this.activity.log({
      swabDetail: sampleId,
      type: this.activity.activityList.lab.removeStatusSample,
      params: {
        status: statusSample.name
      },
      user: userLoggedIn
    })

    return res;
  }
}
