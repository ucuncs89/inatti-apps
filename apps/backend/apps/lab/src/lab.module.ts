import { AccountModule } from '@app/account';
import { AuthenticationModule } from '@app/authentication';
import { DatabaseModule } from '@app/database';
import { AppsEnum } from '@app/database/entity/apps/apps.type';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { LabController } from './lab.controller';
import { LabService } from './lab.service';
import { SampleSwabController } from './sample-swab/sample-swab.controller';
import { ExtractionController } from './extraction/extraction.controller';
import { ExtractionService } from './extraction/extraction.service';
import { AnalyzeController } from './analyze/analyze.controller';
import { AnalyzeService } from './analyze/analyze.service';
import { AnalyzeToolController } from './analyze-tool/analyze-tool.controller';
import { SampleSwabService } from './sample-swab/sample-swab.service';
import { UserManageController } from './user-manage/user-manage.controller';
import { UserManageService } from './user-manage/user-manage.service';
import { PaginationPipe } from '@app/helper/paginate/pagination.service';
import { FilterQueryPipe } from '@app/helper/query-filter/query-filter.service';
import { NarLabIntegration } from './kemenkes-nar/nar-lab-integration.service';
import { ResultToolService } from './analyze/result-tool/result-tool.service';
import { ResultToolController } from './analyze/result-tool/result-tool.controller';
import { ReportPdfController } from './report-pdf/report-pdf.controller';
import { ReportPdfModule } from '@app/report-pdf';
import { LabReportService } from './report-pdf/lab-report.service';
import { StatusSampleService } from './status-sample/status-sample.service';
import { NarWebsocketGateway } from './kemenkes-nar/nar-ws.gateway';
import { AwsModule } from '@app/aws';
import { DashboardController } from './dashboard/dashboard.controller';
import { DashboardService } from './dashboard/dashboard.service';
import { LabTemplateController } from './settings/template/lab-template/lab-template.controller';
import { LabTemplateService } from './settings/template/lab-template/lab-template.service';
import AwsConfig from "../../../config/aws.config";
import { WhatsappServiceModule } from '@app/email-service/whatsapp-service/whatsapp-service.module';
import { TransactionRefController } from './transaction-ref/transaction-ref.controller';
import { TransactionRefService } from './transaction-ref/transaction-ref.service';
import { LoggerActivityModule } from '@app/logger-activity';
import { InformationController } from './settings/information/information.controller';
import { InformationLabService } from './settings/information/information.service';

@Module({
  imports: [
    DatabaseModule,
    AuthenticationModule.register({ loginApps: AppsEnum.Lab }),
    PassportModule,
    AccountModule,
    ReportPdfModule,
    WhatsappServiceModule,
    AwsModule.register({
      bucket: AwsConfig().aws.bucket,
      uploadFolder: 'lab'
    }),
    LoggerActivityModule.register({
      area: 'lab'
    })
  ],
  controllers: [LabController,
     SampleSwabController,
     ExtractionController,
     AnalyzeController,
     AnalyzeToolController,
     UserManageController,
     ResultToolController,
     ReportPdfController,
     DashboardController,
     LabTemplateController,
     TransactionRefController,
     InformationController
  ],
  providers: [LabService,
     ExtractionService,
     AnalyzeService,
     SampleSwabService,
     UserManageService,
     PaginationPipe,
     FilterQueryPipe,
     NarLabIntegration,
     ResultToolService,
     LabReportService,
     StatusSampleService,
     NarWebsocketGateway,
     DashboardService,
     LabTemplateService,
     TransactionRefService,
     InformationLabService
  ],
  exports: [LabService]
})
export class LabModule { }
