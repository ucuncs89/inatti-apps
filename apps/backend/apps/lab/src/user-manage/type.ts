import { UserManageDTO } from "@app/database/entity/user/user.dto";
import { ApiProperty } from "@nestjs/swagger"

export class UserManageLabDTO extends UserManageDTO {}

export class UserManageUpdateDTO extends UserManageLabDTO{
  @ApiProperty()
  userId: string;
}