import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { BadRequestException, Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserManageLabDTO, UserManageUpdateDTO } from './type';
import { UserManageService } from './user-manage.service';


@ApiBearerAuth()
@UseGuards(JwtAuthGuardLab)
@Controller('user-manage')
export class UserManageController {
  constructor(
    private userManageService: UserManageService
  ){}

  @Get('/roles')
  async getRolesLab(){
    try {
      return await this.userManageService.getRolesLab()
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/users')
  async getUserList(
    @Query('idlab')
    idLab: string
  ){
    try {
      return await this.userManageService.getUserList(idLab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Get('/user/:idroleuser')
  async getUserDetail(
    @Query('idlab')
    idLab: string,
    @Param('idroleuser')
    idroleuser: string
  ){
    try {
      return await this.userManageService.getUserDetail(idLab, idroleuser)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }

  @Post('/user')
  async addUser(
    @Query('idlab')
    idLab: string,
    @Body()
    userManageDTO: UserManageLabDTO
  ){
    try {
      return await this.userManageService.addUser(idLab, userManageDTO)
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error)
    }
  }

  @Patch('/user/:idroleuser')
  async updateUser(
    @Param('idroleuser')
    idroleuser: number,
    @Body()
    userManageUpdateDTO: UserManageUpdateDTO
  ){
    try {
      return await this.userManageService.updateUser(idroleuser, userManageUpdateDTO)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
