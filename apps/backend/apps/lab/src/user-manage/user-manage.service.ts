import { UserNotFoundException } from '@app/authentication/exceptions';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { LabRoleUser } from '@app/database/entity/lab/lab-role-user.entity';
import { LabRole } from '@app/database/entity/lab/lab-role.entity';
import { UsernameExistException } from '@app/database/entity/user/user.entities.services';
import { User } from '@app/database/entity/user/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { UserManageLabDTO, UserManageUpdateDTO } from './type';

@Injectable()
export class UserManageService {
  constructor(
    @InjectRepository(LabRoleUser)
    private LabRoleUserRepository: Repository<LabRoleUser>,
    @InjectRepository(LabRole)
    private LabRoleRepository: Repository<LabRole>,
    @InjectRepository(User)
    private UserRepository: Repository<User>,
  ) { }

  queryUserWithRole() {
    return this.LabRoleUserRepository.createQueryBuilder('ru')
      .select([
        'ru.id',
        'u.id',
        'u.fullname',
        'u.username',
        'u.email',
        'r.id',
        'r.name',
        'r.description'
      ])
      .leftJoin('ru.userId', 'u')
      .leftJoin('ru.roleId', 'r')
  }

  getUserList(idLab: string) {
    let query = this.queryUserWithRole()
      .where('ru.labId = :labid', { labid: idLab })
    return query.getMany();
  }

  getUserDetail(idLab: string, idroleuser: string) {
    let query = this.queryUserWithRole()
      .addSelect(['r.permission'])
      .where('ru.labId = :labid', { labid: idLab })
      .andWhere('ru.id = :idroleuser', { idroleuser })
    return query.getOne();
  }

  getRolesLab() {
    return this.LabRoleRepository.find();
  }

  async isUsernameExist(username: string) {
    let user = await this.UserRepository.find({ where: { username } })
    if (user.length > 0) {
      throw new UsernameExistException();
    }
  }

  async addUser(idLab: string, userManageDTO: UserManageLabDTO) {
    await this.isUsernameExist(userManageDTO.username)
    return getManager().transaction(async entity => {
      let user = new User()
      user.fullname = userManageDTO.fullname
      user.username = userManageDTO.username
      user.password = userManageDTO.password
      user.email = userManageDTO.email

      user = await entity.save(user)

      let labRoleUser = new LabRoleUser;
      labRoleUser.userId = user
      labRoleUser.roleId = new LabRole(userManageDTO.roleId)
      labRoleUser.labId = new LabDetail(idLab)
      await entity.save(labRoleUser)
      return true;
    })
  }

  async updateUser(idroleuser: number, userManageUpdateDTO: UserManageUpdateDTO) {
    try {
      let user: User = await this.UserRepository.findOne({ id: userManageUpdateDTO.userId })
      if (!user) {
        throw new UserNotFoundException();
      }
      // check username
      if (user.username !== userManageUpdateDTO.username) {
        await this.isUsernameExist(userManageUpdateDTO.username);
      }

      // save data
      user.fullname = userManageUpdateDTO.fullname
      user.username = userManageUpdateDTO.username
      user.email = userManageUpdateDTO.email
      if (userManageUpdateDTO.password) {
        user.password = userManageUpdateDTO.password
        await user.hashPassword() // manual hash
      }
      await this.UserRepository.save(user)

      if (userManageUpdateDTO.roleId) {
        await this.LabRoleUserRepository.update(idroleuser, {
          roleId: new LabRole(userManageUpdateDTO.roleId)
        })
      }
      return true;
    } catch (error) {
      throw error;
    }
  }
}
