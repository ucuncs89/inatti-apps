import { JwtAuthGuardLab } from '@app/authentication/authorization/jwt.authguard.lab';
import { LoginDTO } from '@app/authentication/types';
import { BadRequestException, Body, Controller, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LabService } from './lab.service';

@ApiTags('Lab Auth API')
@Controller()
export class LabController {
  constructor(private readonly labService: LabService) { }

  @ApiTags("Authentication", "Account")
  @UseGuards(AuthGuard('lab'))
  @Post('/login')
  @ApiResponse({ status: 201, description: "Success login, receive access_token" })
  @ApiResponse({ status: 401, description: "Login failed, user not found or password incorrect" })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Request() req, @Body() query: LoginDTO) {
    return req.user.token
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardLab)
  @Get('/registered-lab')
  async registeredLab(@Request() req) {
    let listRef = await this.labService.getRegisteredLabUser(req.user)
    return listRef
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardLab)
  @Get('/role-user/:idlab')
  async getRoleUser(
    @Param('idlab')
    idLab: string,
    @Request()
    req: any
  ) {
    try {
      return await this.labService.getRoleUser(req.user.id, idLab)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
