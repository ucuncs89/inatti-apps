import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class LabInformationUpdateDTO {
  @ApiPropertyOptional()
  name?:string

  @ApiPropertyOptional()
  config?: {
    timezone: string
  }
}