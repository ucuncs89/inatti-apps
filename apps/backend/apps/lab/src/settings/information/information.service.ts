import { LabConfig } from '@app/database/entity/lab/lab-config.entity';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LabInformationUpdateDTO } from './type';

@Injectable()
export class InformationLabService {
  constructor(
    @InjectRepository(LabDetail)
    private LabDetailRepository: Repository<LabDetail>,
    @InjectRepository(LabConfig)
    private LabConfigRepository: Repository<LabConfig>,
  ){}

  async getInformationLab(idLab:string) {
    return await this.LabDetailRepository.findOne({
      where: {
        id: idLab,
      },
      relations:[
         'config',
      ]
    })
  }

  async updateInformationLab(idLab: string, body: LabInformationUpdateDTO) {
    let labDetail = await this.getInformationLab(idLab)
    if(!labDetail) throw "No lab found"
    let {config, ...lab} = body

    Object.assign(labDetail, lab)

    await this.LabDetailRepository.save(labDetail)
    if(config){
      let configLab = labDetail.config
      if(!configLab){
        configLab = new LabConfig()
        configLab.lab = labDetail
      }
      Object.assign(configLab, config)
      await this.LabConfigRepository.save(configLab)
    }
    return true;
  }
}
