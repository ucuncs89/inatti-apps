import { BadRequestException, Body, Controller, Get, Patch, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from '../../helper/auth.decorator';
import { InformationLabService } from './information.service';
import { LabInformationUpdateDTO } from './type';

@ApiTags("Lab API")
@RequireAuth()
@Controller('setting/information')
export class InformationController {
  constructor(
    private readonly service: InformationLabService
  ){}

  @Get('/')
  async getInformationLab(
    @Query('idlab')
    idLab: string
  ){
    try {
      return await this.service.getInformationLab(idLab)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }

  @Patch('/')
  async updateInformationLab(
    @Query('idlab')
    idLab: string,
    @Body()
    body: LabInformationUpdateDTO
  ){
    try {
      return await this.service.updateInformationLab(idLab, body)
    } catch (error) {
      console.log(error)
      throw new BadRequestException(error)
    }
  }
}
