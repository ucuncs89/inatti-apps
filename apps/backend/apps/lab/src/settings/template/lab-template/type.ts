import { ApiProperty } from "@nestjs/swagger";

export class SetTemplateFileDTO{
  @ApiProperty({type: 'string', format: 'binary'})
  headerLeftImage: Express.Multer.File[]
  @ApiProperty({type: 'string', format: 'binary'})
  headerRightImage: Express.Multer.File[]
  @ApiProperty({type: 'string', format: 'binary'})
  signaturImage: Express.Multer.File[]
}

export class SetTemplateDTO {
  @ApiProperty()
  doctorName:string;
  @ApiProperty()
  doctorSubname:string;
  @ApiProperty()
  nameLab:string;
  @ApiProperty()
  detailLab:string;
  @ApiProperty()
  reagent:string;
  @ApiProperty()
  note:string;
  @ApiProperty()
  genTarget:string;
}