import { AwsService } from '@app/aws';
import { LabDetail } from '@app/database/entity/lab/lab-detail.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {LabTemplateSertifikat} from 'libs/database/src/entity/template-map/lab-template-sertifikat.entity'
import { Repository } from 'typeorm';
import { SetTemplateDTO, SetTemplateFileDTO } from './type';

@Injectable()
export class LabTemplateService {
  constructor(
    @InjectRepository(LabTemplateSertifikat)
    private LabTemplateSertifikatRepository: Repository<LabTemplateSertifikat>,
    private awsService: AwsService
  ){}

  getTemplateLab(idLab: string){
    return this.LabTemplateSertifikatRepository.findOne({
      where: {
        lab: {
          id: idLab
        }
      },
    })
  }

  async setTemplateLab(idLab: string, body:SetTemplateDTO, files:SetTemplateFileDTO){
    let template = await this.LabTemplateSertifikatRepository.findOne({
      where: {
        lab: {
          id: idLab
        }
      },
      relations: ['lab']
    })
    if(!template){
      template = new LabTemplateSertifikat()
      template.lab = new LabDetail(idLab)
    }

    console.log(template)

    for (const key in body) {
      template[key] = body[key]
    }

    console.log(template)
    if(files){
      for (const typeFile of Object.keys(files)) {
        if(files[typeFile].length > 0){
          const image = files[typeFile][0]
          let fileImage = await this.awsService.HashAndUploadS3(image, 'template-pdf-lab')
          template[typeFile] = fileImage.s3.Location
        }
      }
    }

    console.log(template);
    
    if(template.id){
      return await this.LabTemplateSertifikatRepository.update({id: template.id}, template)
    }else{
      return await this.LabTemplateSertifikatRepository.save(template)
    }
  }
}
