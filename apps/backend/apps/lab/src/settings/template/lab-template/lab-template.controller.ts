import { BadRequestException, Body, Controller, Get, Post, Query, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { RequireAuth } from 'apps/lab/src/helper/auth.decorator';
import { LabTemplateService } from './lab-template.service';
import { SetTemplateDTO, SetTemplateFileDTO } from './type';

@ApiTags('Lab API')
@RequireAuth()
@Controller('setting/lab-template')
export class LabTemplateController {
  constructor(
    private labTemplateService: LabTemplateService
  ){}

  @Get('/')
  getTemplateLab(
    @Query('idlab')
    idLab: string
  ){
    try{
      return this.labTemplateService.getTemplateLab(idLab)
    }catch(err){
      throw new BadRequestException(err)
    }
  }

  @UseInterceptors(FileFieldsInterceptor([
    { name: 'headerLeftImage', maxCount: 1 },
    { name: 'headerRightImage', maxCount: 1 },
    { name: 'signaturImage', maxCount: 1 },
  ]))
  @Post('/')
  async setTemplateLab(
    @Query('idlab')
    idLab: string,
    @Body()
    body: SetTemplateDTO,
    @UploadedFiles()
    files: SetTemplateFileDTO
  ){
    try {
      console.log(body)
      console.log(files)
      return await this.labTemplateService.setTemplateLab(idLab, body, files)
    } catch (error) {
      throw new BadRequestException(error)
    }
  }
}
