export const state = () => ({
  apotekId: null,
  apotekSelected: { id: '', name: '' },
})

export const mutations = {
  setApotekId(state: any, apotekId: string) {
    state.apotekId = apotekId
  },
  setApotekSelected(state: any, item: { id: string; name: string }) {
    state.apotekSelected = { id: item.id, name: item.name }
  },
}
