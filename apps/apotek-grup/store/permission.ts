import { Context } from '@nuxt/types'
import jwtDecode from 'jwt-decode'

export const state = () => ({
  allowed: null,
})

export const mutations = {
  setAllowed(state: any, payload: any) {
    state.allowed = payload
  },
}

export const actions = {
  // `force` to force fetch data
  async fetchRole({ commit, state }: any, { context, force }: any) {
    const { $axios, query }: Context = context
    if (!query.apotekId) return false
    // if force to fetch, continue
    if (state.allowed) {
      if (!force) {
        return false
      }
    }
    let api = '/api-apotek-group/role-user/' + query.apotekId
    try {
      let result: any = await $axios.$get(api)

      if (result.roleId) {
        commit('setAllowed', result.roleId.permission)
      } else {
        throw new Error('No role selected')
      }
    } catch (error: any) {
      console.log(error.response)
    }
  },
}
