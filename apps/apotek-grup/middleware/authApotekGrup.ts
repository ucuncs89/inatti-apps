import { Context } from '@nuxt/types'

export const apotekId = 'apotekId'

/**
 * this middleware make apps to require query `apotekId`
 * ex: /home/?apotekId=123-24123-4123
 */

export default function (context: Context) {
  const excludeRoute = ['/login', '/login/app']
  const isInExclude = (route) => excludeRoute.some((v) => route.startsWith(v))
  
  if (!context.$auth.loggedIn && !isInExclude(context.route.path)) {
    return context.redirect('/login' + '?from=' + context.route.path)
  }
  const apotekIdQuery = context.query[apotekId]
  const apotekIdStore = context.store.state.apotekStore[apotekId]
  if (!apotekIdQuery) {
    if (apotekIdStore) {
      return context.redirect(context.route.path + '?apotekId=' + apotekIdStore)
    }
    if (!isInExclude(context.route.path)) {
      return context.redirect('/login/app' + '?from=' + context.route.path)
    }
  } else if (!apotekIdStore) {
    context.store.commit('apotekStore/setApotekId', apotekIdQuery)
  } else if (apotekIdStore !== apotekIdQuery) {
    context.store.commit('apotekStore/setApotekId', apotekIdQuery)
  }
}
