module.exports = {
  apps: [
  {
    name   : "apotek-group-inatti-prod",
    script : "yarn",
    args   : "start",
  },
  {
    name   : "apotek-group-inatti-stag",
    script : "yarn",
    args   : "start",
  },
  ]
}
