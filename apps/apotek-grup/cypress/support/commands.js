// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

//     ----------------- NOT READY TO USE! ----------------
// Cypress.Commands.add('loginProgramatically', (username, password) => {
//   cy.visit('/login')
//   cy.request('POST', 'https://dev-api.inatti.id/api-apotek-group/login', {
//     username,
//     password,
//   }).as('login')
//   cy.pause()
//   cy.get('@login').should((response) => {
//     expect(response.body).to.have.property('access_token')
//     // expect(localStorage.getItem('auth._token.local')).to.exist()
//   })
// })  ----------------- NOT READY TO USE! ----------------

Cypress.Commands.add('loginWithUi', (username, password) => {
  cy.visit('/login')
  cy.get('[data-cy="auth-form-login-field-username"]').type(username)
  cy.get('[data-cy="auth-form-login-field-password"]').type(password)
  cy.get('[data-cy="auth-form-login-submit"]').click()
})

