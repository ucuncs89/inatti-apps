describe('Choosing apotek', () => {
  const account = {
    username: Cypress.env('user').username,
    password: Cypress.env('user').password
  }

  before(() => {
    cy.loginWithUi(account.username, account.password)
  })

  it('Successfully go to page login app and show apotek list', () => {
    cy.url().should('include', '/login/app')
    cy.get('[data-cy="auth-app-list-apotek"]').should('have.class', 'v-list-item')
  })
    
  it('Successfully choose apotek', () => {
    cy.get('[data-cy="auth-app-list-apotek"]').contains('Apotek lenovo').click()
    cy.url().should('include', '/?')
    cy.url().should('include', 'apotekId=')
  })
})