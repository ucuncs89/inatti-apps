describe('Login to InaTTI App.', () => {
  const account = {
    username: Cypress.env('user').username,
    password: Cypress.env('user').password
  }

  afterEach(() => {
    cy.pause()
  })

  it('Unsuccessfully login: username not found.', () => {
    cy.loginWithUi('tachibana', 'kanade')
    cy.get('[data-cy="auth-form-login-alert"]').should('have.class', 'v-alert') // memunculkan alert component
    cy.get('[data-cy="auth-form-login-alert"]').should(
      'have.class',
      'error--text'
    ) // alert berupa error
    cy.get('[data-cy="auth-form-login-alert"]').should(
      'contain.text',
      'Pengguna tidak ditemukan.'
    ) // error dengan pesan tsb
  })

  it('Unsuccessfully login: password incorrect.', () => {
    cy.loginWithUi('nabilla', 'meowimoet')
    cy.get('[data-cy="auth-form-login-alert"]').should('have.class', 'v-alert') // memunculkan alert component
    cy.get('[data-cy="auth-form-login-alert"]').should(
      'have.class',
      'error--text'
    ) // alert berupa error
    cy.get('[data-cy="auth-form-login-alert"]').should(
      'contain.text',
      'Password yang anda masukkan salah.'
    )
  })

  it('Successfully login.', () => {
    cy.loginWithUi(account.username, account.password)
    cy.url().should('include', '/login/app')
  })
})
