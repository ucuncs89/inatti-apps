export const state = () => ({
  apotekId: null,
})

export const mutations = {
  setApotekId(state: any, apotekId: string) {
    state.apotekId = apotekId
  },
}
