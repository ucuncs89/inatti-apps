export const state = () => ({
  status: {
    isApotekerActive: {
      status: true,
      message: '',
      messagId: '',
    },
    isApotekActice: {
      status: true,
      message: '',
      messagId: '',
    },
  },
})

export const mutations = {
  SET_STATUS(state: any, payload: string) {
    state.status = payload
  },
}

export const actions = {
  async getStatus({ commit }, payload) {
    try {
      let res = await payload.getStatus()
      commit('SET_STATUS', res)
    } catch (error) {
      console.log(error)
    }
  },
}
