export const state = () => ({
    show: false,
    text: null,
    option: {}
  })
  
  export const mutations = {
    setText(state: any, payload: string) {
      state.text = payload
    },
    setShow(state: any, payload: boolean) {
      state.show = payload
    },
    setOptions(state: any, payload: object) {
      state.option = payload
    }
  }
  
  export const actions = {
    showSnackbar({ commit }: any, payload: string | { text: string, options: object }) {
      commit('setShow', true);
      if (typeof payload === 'string') {
        commit('setText', payload);
      } else {
        commit('setText', payload.text)
        commit('setOptions', payload.options)
      }
    },
    closeSnackbar({ commit }: any) {
      commit('setShow', false);
      commit('setText', null);
      commit('setOptions', {})
    }
  }
  