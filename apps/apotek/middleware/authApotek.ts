import { Context } from '@nuxt/types'

export const apotekId = 'apotekId'

export default function (context: Context) {
  let excludeRoute = ['/login', '/login/app', '/auth/register', '/auth/otp', '/register/setup']
  const isInExclude = (route) => excludeRoute.some((v) => route.startsWith(v))
  const originalUrl = new URL(context.route.fullPath, window.location.origin)
  if (!context.$auth.loggedIn && !isInExclude(context.route.path)) {
    originalUrl.pathname = '/login'
    originalUrl.searchParams.set('from', context.route.fullPath)
    return context.redirect(originalUrl.pathname + originalUrl.search)
  }
  let apotekIdQuery = context.query[apotekId]
  let apotekIdStore = context.store.state.apotekStore[apotekId]
  if (!apotekIdQuery) {
    if (apotekIdStore) {
      originalUrl.searchParams.set('apotekId', apotekIdStore)
      return context.redirect(originalUrl.pathname + originalUrl.search)
    }
    if (!isInExclude(context.route.path)) {
      originalUrl.searchParams.set('from', context.route.fullPath)
      originalUrl.pathname = '/login/app'
      return context.redirect(originalUrl.pathname + originalUrl.search)
    }
  } else if (!apotekIdStore) {
    context.store.commit('apotekStore/setApotekId', apotekIdQuery)
  } else if (apotekIdStore !== apotekIdQuery) {
    context.store.commit('apotekStore/setApotekId', apotekIdQuery)
  }
}
