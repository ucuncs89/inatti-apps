module.exports = {
  apps: [
  {
    name   : "apotek-inatti-prod",
    script : "yarn",
    args   : "start",
  },
  {
    name   : "apotek-inatti-stag",
    script : "yarn",
    args   : "start",
  },
  ]
}
