import dataCyLib from '../../fixtures/data-cy-lib.json'

Cypress.Commands.add('login', (username, password) => {
  cy.visit('/login')
  cy.get(`[data-cy="${dataCyLib.login.formUsername}"]`).type(username)
  cy.get(`[data-cy="${dataCyLib.login.formPassword}"]`).type(password)
  cy.get(`[data-cy="${dataCyLib.login.actionSubmit}"]`).click()
})

Cypress.Commands.add('chooseApotek', (apotekName) => {
  cy.get(`[data-cy="${dataCyLib.apotek.apotekListItem}"]`)
    .contains(apotekName)
    .click()
})
