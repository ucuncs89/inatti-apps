import dataCyLib from '../../fixtures/data-cy-lib.json'
Cypress.Commands.add(
  'fillFormAccountSettingAccount',
  (fullname, username, email) => {
    cy.get(`[data-cy="${dataCyLib.accountSetting.account.actionEdit}"]`).click()
    const fieldFullname = cy.get(
      `[data-cy="${dataCyLib.accountSetting.account.formFullname}"]`
    )
    fieldFullname.clear()
    if (fullname) {
      fieldFullname.type(fullname)
    }
    const fieldUsername = cy.get(
      `[data-cy="${dataCyLib.accountSetting.account.formUsername}"]`
    )
    fieldUsername.clear()
    if (username) {
      fieldUsername.type(username)
    }
    const fieldEmail = cy.get(
      `[data-cy="${dataCyLib.accountSetting.account.formEmail}"]`
    )
    fieldEmail.clear()
    if (email) {
      fieldEmail.type(email)
    }
    cy.get(`[data-cy="${dataCyLib.accountSetting.account.actionSave}"]`).click()
  }
)

Cypress.Commands.add(
  'validateUncompletedFormAccountSettingAccount',
  (fullname, username, email) => {
    if (!fullname) {
      const fieldFullname = cy.get(
        `[data-cy="${dataCyLib.accountSetting.account.formFullname}"]`
      )
      fieldFullname
        .parent()
        .parent()
        .parent()
        .children()
        .contains('Form ini wajib diisi')
    }
    if (!username) {
      const fieldUsername = cy.get(
        `[data-cy="${dataCyLib.accountSetting.account.formUsername}"]`
      )
      fieldUsername
        .parent()
        .parent()
        .parent()
        .children()
        .contains('Form ini wajib diisi')
    }
    if (!email) {
      const fieldEmail = cy.get(
        `[data-cy="${dataCyLib.accountSetting.account.formEmail}"]`
      )
      fieldEmail
        .parent()
        .parent()
        .parent()
        .children()
        .contains('Email Format Tidak Valid')
    }
  }
)

Cypress.Commands.add(
  'fillFormAccountSettingPassword',
  (oldPassword, newPassword, confirmPassword) => {
    cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.actionSettingPassword}"]`
    ).click()
    const fieldOldPassword = cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.formOldPassword}"]`
    )
    fieldOldPassword.clear()
    if (oldPassword) {
      fieldOldPassword.type(oldPassword)
    }
    const fieldNewPassword = cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.formNewPassword}"]`
    )
    fieldNewPassword.clear()
    if (newPassword) {
      fieldNewPassword.type(newPassword)
    }
    const fieldConfirmPassword = cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.formConfirmPassword}"]`
    )
    fieldConfirmPassword.clear()
    if (confirmPassword) {
      fieldConfirmPassword.type(confirmPassword)
    }
  }
)

Cypress.Commands.add(
  'validateNewPasswordLessThanMinimumCharacterAccountSettingPassword',
  () => {
    const fieldNewPassword = cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.formNewPassword}"]`
    )
    fieldNewPassword
      .parent()
      .parent()
      .parent()
      .children()
      .contains('Password Minimal 8 Karakter')
  }
)

Cypress.Commands.add(
  'validateOldPasswordDifferentAccountSettingPassword',
  () => {
    const fieldConfirmPassword = cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.formConfirmPassword}"]`
    )
    fieldConfirmPassword
      .parent()
      .parent()
      .parent()
      .children()
      .contains('Password Tidak Sama')
  }
)
