import Credentials from '../fixtures/credetials.json'
import dataCyLib from '../fixtures/data-cy-lib.json'

describe('Login', () => {
  it('Incorrect Credentials', () => {
    cy.login('test', 'test123')
    cy.get(`[data-cy="${dataCyLib.login.alertMessage}"]`).contains(
      'User tidak ditemukan'
    )
  })
  it('Success Login', () => {
    cy.login(Credentials.username, Credentials.password)
  })
  it('Sucess Access List Apotek', () => {
    cy.url().should('include', '/login/app')
  })
})

describe('Pilih Apotek', () => {
  let apotekName = 'Apotek lenovo'
  it('Check Availablelity List Apotek', () => {
    cy.get(`[data-cy="${dataCyLib.apotek.apotekList}"]`)
  })
  it('Choose Apotek', () => {
    cy.chooseApotek(apotekName)
  })
  it('Success Access Dashboard', () => {
    cy.url().should('include', '?apotekId')
  })
})
