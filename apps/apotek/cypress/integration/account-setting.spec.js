import Credentials from '../fixtures/credetials.json'
import AccountSetting from '../fixtures/account-setting.json'
import dataCyLib from '../fixtures/data-cy-lib.json'

describe('Login', () => {
  it('Incorrect Credentials', () => {
    cy.login('test', 'test123')
    cy.get(`[data-cy="${dataCyLib.login.alertMessage}"]`).contains(
      'User tidak ditemukan'
    )
  })
  it('Success Login', () => {
    cy.login(Credentials.username, Credentials.password)
  })
  it('Sucess Access List Apotek', () => {
    cy.url().should('include', '/login/app')
  })
})

describe('Pilih Apotek', () => {
  let apotekName = 'Apotek lenovo'
  it('Check Availablelity List Apotek', () => {
    cy.get(`[data-cy="${dataCyLib.apotek.apotekList}"]`)
  })
  it('Choose Apotek', () => {
    cy.chooseApotek(apotekName)
  })
  it('Success Access Dashboard', () => {
    cy.url().should('include', '?apotekId')
  })
})

describe('Account Setting', () => {
  it('Success Access Account Setting - Dropdown Menu Account', () => {
    cy.get(
      `[data-cy="${dataCyLib.application.appbar.menuAccountDropdownActivator}"]`
    ).click()
    cy.get(
      `[data-cy="${dataCyLib.application.appbar.menuAccountDropdownSetting}"]`
    ).click()
    cy.url().should('include', '/account/setting')
  })
  it('Uncompleted Form Update Account - Should Failed', () => {
    cy.fillFormAccountSettingAccount(
      '',
      AccountSetting.account.username,
      AccountSetting.account.email
    )
    cy.validateUncompletedFormAccountSettingAccount(
      '',
      AccountSetting.account.username,
      AccountSetting.account.email
    )
    cy.get(
      `[data-cy="${dataCyLib.accountSetting.account.actionCancel}"]`
    ).click()
  })
  it('Completed Form Update Account - Should Success', () => {
    cy.fillFormAccountSettingAccount(
      AccountSetting.account.fullname,
      AccountSetting.account.username,
      AccountSetting.account.email
    )
    cy.get(`[data-cy="${dataCyLib.accountSetting.alert}"]`).contains(
      'Sukses Update Akun'
    )
  })
  it('New Password Less Than Minimum Character Form Update Password - Should Failed', () => {
    cy.fillFormAccountSettingPassword(
      AccountSetting.password.oldPassword,
      'test',
      ''
    )
    cy.validateNewPasswordLessThanMinimumCharacterAccountSettingPassword()
    cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.actionCancel}"]`
    ).click()
  })
  it('Confirm Password Different Form Update Password - Should Failed', () => {
    cy.fillFormAccountSettingPassword(
      AccountSetting.password.oldPassword,
      '12345678',
      '123456'
    )
    cy.validateOldPasswordDifferentAccountSettingPassword()
    cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.actionCancel}"]`
    ).click()
  })
  it('Form Update Password With Correct Value - Should Success', () => {
    cy.fillFormAccountSettingPassword(
      AccountSetting.password.oldPassword,
      AccountSetting.password.newPassword,
      AccountSetting.password.confirmPassword
    )
    cy.get(
      `[data-cy="${dataCyLib.accountSetting.password.actionSave}"]`
    ).click()
    cy.get(`[data-cy="${dataCyLib.accountSetting.alert}"]`).contains(
      'Sukses Update Password Akun'
    )
  })
})
