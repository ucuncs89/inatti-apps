export interface PaginationResult<Type> {
  results: Type[]
  pageTotal: number // total perpage
  total: number // total all data
  page: number
  pageLength: number // number of page
}

export interface UserLogin {
  id: string
  email: string
  username: string
  fullname: string
}

/* eslint-disable camelcase */
import * as enumType from './enumType'
export interface Provinsi {
  id: number
  name: string
}

export interface Kabupaten {
  id: number
  name: string
  provinsi: Provinsi
}

export interface Kecamatan {
  id: number
  name: string
  kabupaten: Kabupaten
}

export interface Kelurahan {
  id: string
  name: string
  kecamatan: Kecamatan
}

export declare module PatientSwabDetailDTO {
  export interface Patient {
    id: string | null
    nik: string | null
    type: enumType.NikTypeEnum | null
    name: string | null
    height: string | number | null
    weight: string | number | null
    job: string | null
    placebirth: string | null
    datebirth: string | null
    gender: enumType.GenderEnum | string | null
    phonenumber: string | null
    email: string | null
    bloodtype: enumType.BloodTypeEnum | null
    createdAt: Date | null
    provinsi: Provinsi | number | null
    kabupaten: Kabupaten | number | null
    kecamatan: Kecamatan | number | null
    kelurahan: Kelurahan | number | null
    kelurahanRw: number | string | null
    kelurahanRt: number | string | null
    address: string | null
    domisiliProvinsi: Provinsi | number | null
    domisiliKabupaten: Kabupaten | number | null
    domisiliKecamatan: Kecamatan | number | null
    domisiliKelurahan: Kelurahan | number | null
    domisiliKelurahanRw: number | string | null
    domisiliKelurahanRt: number | string | null
    addressDomisili: string | null
    citizenship: string | null
    narOrangId: string | null
    detailSwab: Array<TestRecord> | null
    ownedBy: Object | null
  }

  export interface ProductSwab {
    id: number
    name: string
    description: string
  }

  export interface RefTarget {
    id: string
    name: string
    tenantId: string
    createdAt: Date
  }

  export interface RefShiftSchedule {
    id: number
    name: string
    day: number
    timeStart: string
    timeEnd: string
  }

  export interface LabDetail {
    id: number
    name: string
  }

  export interface OrderSwab {
    id: string
    price: number
    method: enumType.MethodPaymentEnum
    status: enumType.StatusOrderEnum
    createdAt: Date
    // eslint-disable-next-line no-use-before-define
    detailSwab: SwabDetail
  }

  export enum StatusNotifWA {
    QUEUE = 'queue',
    PROCESS = 'process',
    SENT = 'sent',
    FAILED = 'failed',
  }

  export interface WhatsappNotify {
    id?: number
    swabDetail: SwabDetail
    status?: StatusNotifWA
    messageId?: string
    message?: string
    createdAt?: Date
  }

  export interface SwabDetail {
    id: number
    status: enumType.StatusSwabEnum
    swabDate?: Date
    resultSwab?: enumType.ResultSwabEnum
    isSendToLab?: boolean
    sendToLabDate?: Date
    createdAt: Date
    patient: Patient
    productSwab?: ProductSwab
    refTarget?: RefTarget
    refShiftSchedule?: RefShiftSchedule
    swabBy: string
    labTarget: LabDetail
    requestSwabDate: Date
    orderSwab?: OrderSwab
    sampleId: string
    codeBooking: number | string
    resultSentAt: Date
    narSample: any
    notifyWhataspp?: WhatsappNotify
  }

  export interface TestRecord {
    id: number
    sampleId: string
    status: enumType.StatusSwabEnum
    requestSwabDate: Date
    swabDate?: Date
    resultSwab?: enumType.ResultSwabEnum
    resultSentAt?: Date
    isSendToLab?: boolean
    sendToLabDate?: Date
    createdAt: Date
    narSendAt?: Date
    narTestCovidId?: string
    codeBooking?: string
    productSwab: ProductSwab
  }
}

export declare module InfoReferenceDTO {
  export interface ProductDetail {
    id: number
    name: string
    description: string
  }

  export interface Product {
    id: number
    price: number
    name: string
    product: ProductDetail
    description?: string
    // eslint-disable-next-line no-use-before-define
    ref: InfoReference
    methodPayments: any
  }

  export interface Shift {
    id: number
    name: string
    day: number
    timeStart: string
    timeEnd: string
    // eslint-disable-next-line no-use-before-define
    reference: InfoReference
  }

  export interface InfoReference {
    id: string
    name: string
    tenantId: string
    createdAt: Date
    products: Product[]
    shifts: Shift[]
  }

  export interface QuickForm {
    id: string
    name: string
    expiredAt: string
    createdAt: string
  }
}

export declare module TransactionDetailDTO {
  export interface Customer {
    id: string
    fullname: string
    username: string
    password: string
    isActive: Boolean
    email: string
    phoneNumber: number
    isAnonymous: Boolean
  }
  export interface TransactionDetail {
    methodCash: any
    id: string
    trxId: string
    invId: string
    status: enumType.StatusOrderEnum
    totalPrice: number
    method: enumType.MethodPaymentEnum
    paymentChannelCode: string
    paymentChannelName: string
    createdAt: Date
    expiredAt: Date
    customer: Customer
    reference: InfoReferenceDTO.InfoReference
    product: InfoReferenceDTO.Product
    totalOrder: number
    orderSwab: PatientSwabDetailDTO.OrderSwab[]
  }
}
export interface UserLogin {
  id: string
  email: string
  username: string
  fullname: string
}

export interface User {
  id: string
  fullname: string
  username: string
  email: string
}
export interface Role {
  id: number
  name: string
  description: string
}
export interface UserRole {
  id: number
  userId: User
  roleId: Role
}
export interface PaginationResult<Type> {
  results: Type[]
  pageTotal: number // total perpage
  total: number // total all data
  page: number
  pageLength: number // number of page
}

export declare module LogActivityDTO {
  export interface type {
    id: number
    name: string
    activity: string
    area: string
    createdAt: string
  }

  export interface logActivity {
    id: number
    createdAt: string
    type: type
    user: User
    formatted_activity: string
  }
}
export interface ObatApotekData {
  id: number
  name: string
  createdAt: Date | string | null
  insertByApotek: {
    id: string
    name: string
  }
  insertBy: {
    id: string
    fullname: string
    username: string
  }
}

export interface ObatPublicData {
  id: number
  name: string
  createdAt: Date | string | null
}
