module.exports = {
  apps: [
  {
    name   : "ref-inatti-prod",
    script : "yarn",
    args   : "start",
  },
  {
    name   : "ref-inatti-stag",
    script : "yarn",
    args   : "start",
  },
  ]
}
