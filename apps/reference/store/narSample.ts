export const state = () => ({
  selected: null
})

export const mutations = {
  setSelected(state:any, payload:any){
    // inject property nar to null
    state.selected = payload.map((v:any) => ({
      ...v,
      nar: null
    }))
  },
  updateStatusSample(state:any, payload: { sampleId: string, isSuccess: boolean, message?: string }){
    for (const sample of state.selected) {
      if(sample.id === payload.sampleId){
        sample.nar = payload
      }
    }
  },
  removeSampleAtIndex(state:any, index:number){
    state.selected = state.selected.filter((v:any, i:number) => i !== index)
  }
}