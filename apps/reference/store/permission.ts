import { Context } from '@nuxt/types'
import jwtDecode from 'jwt-decode'
import { ReferenceApi } from '@inatti/shared/api-nuxt'

export const state = () => ({
  allowed: null,
})

export const mutations = {
  setAllowed(state: any, payload: any) {
    state.allowed = payload
  },
}

export const actions = {
  // `force` to force fetch data
  async fetchRole({ commit, state }: any, { context, force }: any) {
    const { $axios, query }: Context = context
    const referenceApi = new ReferenceApi($axios)
    if (!query.refId) return false

    // if force to fetch, continue
    if (state.allowed) {
      if (!force) {
        return false
      }
    }
    const refId = query.refId as string
    try {
      const result = await referenceApi.getRoleUser(refId)
      if (result.roleId) {
        commit('setAllowed', result.roleId.permission)
      } else {
        throw new Error('No role selected')
      }
    } catch (error: any) {
      console.log(error.response)
    }
  },
}
