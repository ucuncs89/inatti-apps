import Vue from 'vue'
import { Prop } from 'vue-property-decorator'
import Component from 'vue-class-component'
import { VSelect } from 'vuetify/lib'

@Component
export class MixinSelect extends VSelect {
  @Prop({ type: [Number, String, Object] })
  value!: string | Number | Object

  get selected(): any {
    return this.value
  }
  set selected(val) {
    this.$emit('input', val)
  }

  listItems: any[] = []
  isLoading: boolean = false
}
export default MixinSelect
