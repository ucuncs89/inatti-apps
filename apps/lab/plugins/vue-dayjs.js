import dayjs from 'dayjs'
import Vue from 'vue'

var utc = require("dayjs/plugin/utc")
var timezone = require("dayjs/plugin/timezone")

dayjs.extend(timezone)
dayjs.extend(utc)

Vue.filter('toLocale', (val) => val ? dayjs(val).utc("true").local().format('DD-MM-YYYY HH:mm:ss') : null)