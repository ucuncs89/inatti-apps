import colors from 'vuetify/es5/util/colors'

export default {
  target: 'static',
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Inatii Lab',
    title: 'Lab',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'https://inatti.id/asset-app/inatti-v2.2-favicon.png',
      },
    ],
    script: [
      {
        src: '/crisp.js',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/main.scss', '~/assets/scss/statusSample.scss'],

  router: {
    middleware: ['auth', 'authApp', 'resetSnackbar'],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~plugins/vue-dayjs.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '../../libs/shared/ui/nuxt-module',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseUrl: process.env.URL_BACKEND,
  },

  publicRuntimeConfig: {
    axios: {
      baseURL: process.env.URL_BACKEND,
    },
    URL_INATTI_IDENTITY:
      process.env.URL_INATTI_IDENTITY || 'http://localhost:3000',
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  auth: {
    redirect: {
      login: '/login',
      home: '/',
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api-lab/login',
            method: 'post',
            propertyName: 'access_token',
          },
          user: {
            url: '/account/me',
            method: 'get',
          },
          logout: {
            url: '/logou',
            method: 'get',
          },
        },
        token: {
          property: 'access_token',
        },
        user: {
          property: false,
          autoFetch: true, // default
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      // Add this to extends vuetify component
      'vuetify/lib',
    ],
  },

  env: {
    URL_BACKEND: process.env.URL_BACKEND || 'http://localhost:4000',
  },
}
