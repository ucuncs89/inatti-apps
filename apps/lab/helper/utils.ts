export function getAscii(number: number) {
  const start = 65
  return String.fromCharCode(start + number)
}

export function getCellAscii(row: number, col: number){
  return getAscii(+row) + String(+col+1)
}

export default {
  getAscii,
  getCellAscii
}