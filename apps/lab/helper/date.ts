import dayjs from "dayjs";
import localizedFormat from 'dayjs/plugin/localizedFormat';

export function helperFormatDate(date: string | Date) {
  if (!date) return "";
  dayjs.extend(localizedFormat)
  return dayjs(date).format('DD-MM-YYYY HH:mm:ss')
}