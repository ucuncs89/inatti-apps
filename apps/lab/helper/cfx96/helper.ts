import { resultCfx96 } from '@inatti/mapping-analyze-result'

export interface ValueChartCfx96 {
  labels: any[]
  datasets: {
    label: string
    data: any
    fill: string
    borderColor: string
    tension: string
  }[]
}

export interface SummaryCellCfx96 {
  gens: {
    IC: number[]
    'E GENE': number[]
    'RdRP GENE': number[]
  }
  summary: {
    Well: string
    Fluor: string
    Sample: string
    Target: string
    Content: string
    Cq: number
  }[]
}

export type ToolsSummaryType = {
  [cellA1: string]: { rawData: SummaryCellCfx96 | any; result: any }
}

/**
 * Name cell from "A01" must change to "A1"
 */
export function normalizeCell(cell: string) {
  return cell.replace(/(?=.)0(?=.)/, '')
}

// http://stackoverflow.com/questions/1484506/ddg#1484514
export function getRandomColor(
  opt: { isRgba?: boolean; isComma?: boolean } = { isComma: true }
) {
  let color = []
  for (let i = 0; i < 3; i++) {
    color.push(Math.floor(Math.random() * 255))
  }
  if (opt.isRgba) {
    return `rgba(${color.join(',')})`
  }
  if (opt.isComma) {
    return color.join(',')
  }
  return color
}

export function generateArrayOfNumbers(numbers: number): number[] {
  return [...Array(numbers).keys()].slice(1)
}

export class SummaryCfx96 {
  summaryTool: ToolsSummaryType | null = null
  rawResult: resultCfx96 | null = null
  constructor(rawResult?: resultCfx96) {
    if (rawResult) {
      this.rawResult = rawResult
    }
    if (!this.summaryTool && this.rawResult) {
      this.summaryTool = this.getSummary(this.rawResult)
    }
  }
  // this function from backend code
  getSummary(result: resultCfx96): ToolsSummaryType {
    const { summary: summaryTool, amplification } = result

    const resultSummary: ToolsSummaryType = {}

    // loop through all cell
    for (const cell of Object.keys(summaryTool['0'])) {
      resultSummary[cell] = {
        rawData: {},
        result: null,
      }
      const summaryCell = summaryTool['0'][cell]

      resultSummary[cell].rawData.summary = summaryCell
      resultSummary[cell].rawData.gens = {}

      let cellNormalize = normalizeCell(cell)
      let listGen = Object.keys(amplification).filter(
        (v) => v !== 'Run Information'
      ) // gen except 'Run Information'
      for (const gen of listGen) {
        if (!resultSummary[cell]) continue
        const genResult = amplification[gen][cellNormalize]
        resultSummary[cell].rawData.gens[gen] = genResult
      }
    }

    return resultSummary
  }

  rawDataToChart(position: string): any {
    if (!this.rawResult) throw new Error('Hasil dari alat tidak ditemukan')
    if (!this.summaryTool) this.summaryTool = this.getSummary(this.rawResult)
    let summaryData = this.summaryTool[position]
    if (!summaryData) throw new Error(`Data ${position} tidak ditemukna`)

    return this.generateChartData(summaryData.rawData.gens)
  }

  Cfx96ColorChart(name: string) {
    let colors: Record<string, string> = {
      'RdRP GENE': '#f9261b', // red
      'E GENE': '#221bf9', // blue
      IC: '#27ce02', // green
    }
    return colors[name] ?? getRandomColor({ isRgba: true })
  }

  generateChartData(gens: any) {
    let longestGen = 0
    for (const gen of Object.values(gens) as string[]) {
      longestGen = gen.length > longestGen ? gen.length : longestGen
    }
    let labelsStep = generateArrayOfNumbers(longestGen + 1)

    let labelsCategory = Object.keys(gens)

    return {
      labels: labelsStep,
      datasets: Object.values(gens).map((value: any, index: number) => ({
        label: labelsCategory[index],
        fill: this.Cfx96ColorChart(labelsCategory[index]),
        borderColor: this.Cfx96ColorChart(labelsCategory[index]),
        data: value,
        tension: '0.1',
      })),
    }
    /**
     *  fam, RdRP GENE, red
        hex, E GENE, blue
        cy5, IC green
     */
  }

  static getResultFromCq(value: number) {
    if (!value) {
      return
    }
    if (value < 35) return 'negatif'
    if (value < 38) return 'positif'
    return null
  }
}
