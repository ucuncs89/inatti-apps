import { getCellAscii } from './utils'

export type cellType = {row: number, col: number}
export function A1toNumber(valueA1: string): cellType{
  let row = valueA1[0].charCodeAt(0) - 65;
  let col = +valueA1.slice(1) - 1
  return {
    row,
    col
  }
}

export function NumberToA1(cell: cellType){
  return getCellAscii(cell.row, cell.col)
}