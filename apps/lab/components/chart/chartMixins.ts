import Chart from 'chart.js/auto'

/**
 * Component that import this mixins, 
 * must have $refs.chartCanvas
 */

 import Vue from 'vue'
 import Component from 'vue-class-component'
 
 // You can declare mixins as the same style as components.
 @Component
 export class ChartMixins extends Vue {
  renderChart(type:any, chartData:any, options:any){
    let ctx = this.$refs.chartCanvas as HTMLCanvasElement
    if(!ctx) return;
    let chart = new Chart(ctx, {
      type,
      data: chartData,
      options
    })
    return chart
  }
}

export default {}