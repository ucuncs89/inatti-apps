import { getAscii, getCellAscii } from '@/helper/utils'

export default {
  methods: {
    getAscii,
    getCellAscii,
  }
}