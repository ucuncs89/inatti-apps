import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { VSelect } from 'vuetify/lib'

@Component
export class MixinSelect extends VSelect {
  @Prop({ type: [Number, String, Object] })
  value!: string | Object | number

  get selected(): any {
    return this.value
  }
  set selected(val) {
    this.$emit('input', val)
  }

  listItems: any[] = []
  isLoading: boolean = false
}
export default MixinSelect
