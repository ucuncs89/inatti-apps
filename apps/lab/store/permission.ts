import { Context } from '@nuxt/types'
import jwtDecode from 'jwt-decode'

export const state = () => ({
  allowed: null,
})

export const mutations = {
  setAllowed(state: any, payload: any) {
    state.allowed = payload
  },
}

export const actions = {
  // `force` to force fetch data
  fetchRole({ commit, state }: any, { context, force }: any) {
    return new Promise(async (resolve, reject) => {
      const { $axios, query }: Context = context
      if (!query.labId) reject(false)

      // if force to fetch, continue
      if (state.allowed) {
        if (!force) {
          reject(false)
        }
      }

      let api = '/api-lab/role-user/' + query.labId
      try {
        let result: any = await $axios.$get(api)
        if (result.roleId) {
          commit('setAllowed', result.roleId.permission)
        } else {
          throw new Error('No role selected')
        }
      } catch (error: any) {
        console.log(error.response)
      } finally {
        resolve(true)
      }
    })
  },
}
