export type ModeMapType = null | 'fill' | 'detail' | 'move' | 'idle'

export const state = () => ({
  selectedCell: null,
  mode: null as unknown as ModeMapType,
  filledCell: null,
  
})

export const mutations = {
  setSelectedCell(state: any, payload:any) {
    state.selectedCell = payload
  },
  setMode(state: any, payload:ModeMapType){
    state.mode = payload
  },
  setFilledCell(state:any, payload:any){
    state.filledCell = payload
  }
}

export const actions = {
  reset({commit}:any){
    commit('setSelectedCell', null)
  }
}