export const state = () => ({
  labId: null
})

export const mutations = {
  setLabId(state:any, labId:string){
    state.labId = labId
  }
}