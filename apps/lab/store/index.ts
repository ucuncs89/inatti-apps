import { Context } from "vm"

export const actions = {
  async nuxtServerInit({ dispatch }:any, context:Context){
    await dispatch('permission/fetchRole', {
      context,
      force: true,
    })
  }
}