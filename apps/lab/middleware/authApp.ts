import { Context } from "@nuxt/types"

export const labId = "labId"

/**
 * this middleware make apps to require query `labId`
 * ex: /home/?labId=123-24123-4123
 */

export default function(context:Context){
  let excludeRoute = [
    "/login",
    "/login/app"
  ]
  
  let labIdQuery = context.query[labId]
  let labIdStore = context.store.state.labStore[labId]
  if(!labIdQuery){
    if(labIdStore){
      return context.redirect(context.route.path + "?labId=" + labIdStore)
    }
    if(!excludeRoute.some((v) => context.route.path.startsWith(v))){
      return context.redirect({
        path: '/login/app',
        query: {
          redirect: context.route.path
        }
      });
    }
  } else {
    if(!labIdStore){
      context.store.commit('labStore/setLabId', labIdQuery)
    } else {
      if(labIdStore !== labIdQuery){
        context.store.commit('labStore/setLabId', labIdQuery)
      }
    }
  }
}