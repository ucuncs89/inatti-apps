module.exports = {
  apps: [
  {
    name   : "lab-inatti-prod",
    script : "yarn",
    args   : "start",
  },
  {
    name   : "lab-inatti-stag",
    script : "yarn",
    args   : "start",
  },
  ]
}
