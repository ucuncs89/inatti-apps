const setToken = async (auth, token) => {
    await auth.setStrategy('local');
    await auth.strategy.token.set(token);
    await auth.$storage.setUniversal('loggedIn', true)
    await auth.fetchUser()
  }

export default setToken