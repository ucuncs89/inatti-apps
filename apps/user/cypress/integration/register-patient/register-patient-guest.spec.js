describe('Register patient on guest mode', () => {
  const testData = {
    testDate: '2022-03-30',
    testType: 'PCR',
    faskesName: 'Rujukan Ujicoba',
    patient: {
      fullname: 'Roddy',
      typeIdentitas: 'NIK',
      idNumber: '3000283829199190',
      gender: 'Laki-laki',
      birthPlace: 'Canada',
      birthDate: {
        year: '',
        month: '',
        date: '',
      },
      phonenumber: '85876881563',
      email: 'mnc@gmail.com',
      idcardAddress: 'Canada , CA',
      idCardarea: {
        province: 'Jawa Tengah',
        city: 'KAB. CILACAP',
        district: 'Kedungreja',
        village: 'Tambakreja',
      },
      isDomicilieIdAddress: true,
    },
  }
  it('Should success create order on guest mode', () => {
    cy.visit('/beranda')
    cy.get("[data-cy='beranda-navigation-test']").first().click()
    cy.get("[data-cy='form-start-testcovid-test-type']")
      .contains(testData.testType)
      .click()
    cy.get("[data-cy='btn-submit-form-testcovid-register']").click()
    cy.get("[data-cy='form-search-faskes']")
      .type(testData.faskesName)
      .type('{enter}')
    cy.get("[data-cy='faskes-card-item-testcovid']").click()
    cy.get("[data-cy='btn-submit-faskes-detail-confirm']").click()
    cy.get("[data-cy='btn-pilih-product-list-register-testcovid']")
      .first()
      .click()
    cy.get("[data-cy='patient-item-register-testcovid']").first().click()
    cy.get("[data-cy='form-register-patient-fullname']").type(
      testData.patient.fullname
    )
    cy.get("[data-cy='form-register-patient-fullname']").type(
      testData.patient.fullname
    )
    cy.get("[data-cy='form-register-patient-type-identitas']")
      .parent()
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.typeIdentitas)
      .click()
    cy.get("[data-cy='form-register-patient-idnumber']").type(
      testData.patient.idNumber
    )
    cy.get("[data-cy='form-register-patient-gender']")
      .parent()
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.gender)
      .click()
    cy.get("[data-cy='form-register-patient-birthplace']").type(
      testData.patient.birthPlace
    )
    cy.get("[data-cy='form-register-patient-birthdate']")
      .click()
      .get('button:contains("30")')
      .click()
    cy.get("[data-cy='form-register-patient-phonenumber']")
      .get("[data-cy='form-phone-number']")
      .type(testData.patient.phonenumber)

    cy.get("[data-cy='form-register-patient-email']").type(
      testData.patient.email
    )
    cy.get("[data-cy='form-register-patient-idcard-address']").type(
      testData.patient.idcardAddress
    )
    cy.get("[data-cy='form-register-patient-idcard-province']")
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.idCardarea.province)
      .click()
    cy.get("[data-cy='form-register-patient-idcard-city']")
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.idCardarea.city)
      .click()
    cy.get("[data-cy='form-register-patient-idcard-district']")
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.idCardarea.district)
      .click()
    cy.get("[data-cy='form-register-patient-idcard-village']")
      .click()
      .get('.v-menu__content')
      .contains(testData.patient.idCardarea.village)
      .click()
    if (testData.patient.isDomicilieIdAddress) {
      cy.get("[data-cy='form-register-patient-is-domicilie-id-address']")
        .parent()
        .click()
    }
    cy.get("[data-cy='btn-submit-form-detail-register-patient']").click()
    cy.get(
      "[data-cy='btn-submit-list-patien-registered-testcovid-register']"
    ).click()
    cy.get("[data-cy='btn-submit-preview-order-testcovid-register']").click()
    cy.get("[data-cy='btn-checklist-term-and-privacy-policy']").parent().click()
    cy.get("[data-cy='btn-submit-pay-testcovid-register-payment']").click()
  })
  // it('Should failed if not patient', () => {})
  // it('Should failed if form not filled correctly', () => {})
})
