describe('Test login', () => {
  const userTest = {
    phone: '82230475617',
    password: '12345678',
  }

  it('Should succes login with user existing', () => {
    cy.visit('/auth')
    cy.get("[data-cy='form-phone-number-login']")
      .get("[data-cy='form-phone-number']")
      .type(userTest.phone)
    cy.get("[data-cy='form-password']").type(userTest.password)
    cy.get("[data-cy='btn-login']").click()

    cy.url().should('include', '/beranda')
  })

  it('should failed with number phone not registered', () => {
    cy.visit('/auth')
    cy.get("[data-cy='form-phone-number-login']")
      .get("[data-cy='form-phone-number']")
      .first()
      .type(userTest.phonenumber)
    cy.get("[data-cy='form-password']").type(userTest.password)
    cy.get("[data-cy='btn-login']").click()
    cy.get('.v-alert').should('contain', 'No. Telepon belum terdaftar')
  })

  it('Should failed with wrong password', () => {
    cy.visit('/auth')
    cy.get("[data-cy='form-phone-number-login']")
      .get("[data-cy='form-phone-number']")
      .type(userTest.phone)
    cy.get("[data-cy='form-password']").type(userTest.password + '125123')
    cy.get("[data-cy='btn-login']").click()
    cy.get('.v-alert').should('contain', 'Password yang dimasukkan salah')
  })

  it('Should failed if password null', () => {
    cy.visit('/auth')
    cy.get("[data-cy='form-phone-number-login']")
      .get("[data-cy='form-phone-number']")
      .type(userTest.phone)
    cy.get("[data-cy='btn-login']").click()
    cy.get('.v-alert').should('contain', 'Mohon isi No. Telepon dan password.')
  })

  it('Should failed if phonenumber null', () => {
    cy.visit('/auth')
    cy.get("[data-cy='form-password']").type(userTest.password)
    cy.get("[data-cy='btn-login']").click()
    cy.get('.v-alert').should('contain', 'Mohon isi No. Telepon dan password.')
  })
})
