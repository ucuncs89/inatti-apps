describe('Test register user', () => {
  // need to run on local or reset database
  const userTest = {
    phonenumber: '85876881563',
    fullname: 'Efge Auto Test Dev',
    password: '1111',
    email: 'test@email.com',
  }
  it('Should open page register correctly', () => {
    cy.visit('/auth')
    cy.get("[data-cy='tab-access-register']").click()
  })
  it('Should success register user via whatsapp', () => {
    cy.visit('/auth')
    cy.get("[data-cy='tab-access-register']").click()
    cy.get("[data-cy='form-fullname-register-user']").type(userTest.fullname)
    cy.get("[data-cy='form-phone-number-register']")
      .get("[data-cy='form-phone-number']")
      .last()
      .type(userTest.phonenumber)
    cy.get("[data-cy='form-password-register-user']").type(userTest.password)
    cy.get("[data-cy='form-password-confirm-register-user']").type(
      userTest.password
    )
    cy.get("[data-cy='form-otp-option-email-register']").click()
  })
  it('Should success register user via email with phonenumber', () => {
    cy.visit('/auth')
    cy.get("[data-cy='tab-access-register']").click()
    cy.get("[data-cy='form-otp-option-email-register']").click()
    cy.get("[data-cy='form-fullname-register-user']").type(userTest.fullname)
    cy.get("[data-cy='form-email-register-user']").type(userTest.email)
    cy.get("[data-cy='form-phone-number-register']")
      .get("[data-cy='form-phone-number']")
      .last()
      .type(userTest.phonenumber)
    cy.get("[data-cy='form-password-register-user']").type(userTest.password)
    cy.get("[data-cy='form-password-confirm-register-user']").type(
      userTest.password
    )
  })
  it('Should success register user via email no phonenumber', () => {
    cy.visit('/auth')
    cy.get("[data-cy='tab-access-register']").click()
    cy.get("[data-cy='form-otp-option-email-register']").click()
    cy.get("[data-cy='form-fullname-register-user']").type(userTest.fullname)
    cy.get("[data-cy='form-email-register-user']").type(userTest.email)
    cy.get("[data-cy='form-password-register-user']").type(userTest.password)
    cy.get("[data-cy='form-password-confirm-register-user']").type(
      userTest.password
    )
  })
})
