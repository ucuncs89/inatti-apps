module.exports = {
  apps: [
    {
      name: "user-inatti-prod",
      script: "yarn",
      args: "start",
    },
    {
      name: "user-inatti-stag",
      script: "yarn",
      args: "start",
    },
  ]
}
