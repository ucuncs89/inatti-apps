/* eslint-disable require-await */
import colors from 'vuetify/es5/util/colors'

export default {
  target: 'static',
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: 'INATTI - USER',
    title: 'INATTI - User',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'https://inatti.id/asset-app/inatti-v2.2-favicon.png',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/date-fns',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '../../libs/shared/ui/nuxt-module',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseUrl: process.env.URL_BACKEND,
  },

  publicRuntimeConfig: {
    axios: {
      baseURL: process.env.URL_BACKEND,
    },
  },

  auth: {
    redirect: {
      home: false,
      logout: '/auth',
      login: '/auth',
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api-user/login',
            method: 'post',
            propertyName: 'access_token',
          },
          user: {
            url: '/account/me',
            method: 'get',
          },
          logout: false,
        },
        token: {
          property: 'access_token',
        },
        user: {
          property: false,
          autoFetch: true, // default
        },
      },
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#168AAD',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          inattigreen: '#79892C'
        },
        light: {
          secondary: colors.lightBlue.lighten1,
          inattigreen: '#79892C'
        },
      },
    },
    breakpoint: {
      mobileBreakpoint: 'sm', // This is equivalent to a value of 960
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vuetify/lib'],
    loaders: {
      vue: {
        compiler: require('vue-template-babel-compiler'),
      },
    },
  },
  router: {
    middleware: ['auth', 'index-middleware'],
  },

  babel: {
    plugins: [['babel-plugin-istanbul']],
  },
}
