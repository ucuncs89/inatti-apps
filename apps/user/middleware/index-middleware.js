export default async function (context) {
  const privatePath = ['/telefarmasi']
  if (privatePath.includes(context.route.path) && !context.$auth.loggedIn) {
    await context.$auth.logout()
  }
}
