export default async function (context) {
  const { patient, apotek, consultant } = context.store.state.telefarmasi
  if (
    context.route.path === '/telefarmasi/preview' &&
    (!patient || !apotek || !consultant)
  ) {
    context.redirect('/telefarmasi/apotek')
  }
}
