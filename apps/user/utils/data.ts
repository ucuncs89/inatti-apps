export const dataService = {
  title: 'Layanan Kami',
  subtitle: 'INATTI menawarkan 3 fitur utama terkait pelayanan kesehatan dari COVID-19. Bekerja sama dengan klinik, laboratorium dengan tenaga kesehatan dan peralatan terpercaya guna memudahkan pelacakan test dan isolasi serta terintegrasi pada apps pedulilindungi.',
  resutls: [
    {
      title: 'Pelacakan',
      imagePath: 'location.svg'
    },
    {
      title: 'Test Covid',
      imagePath: 'test_covid.svg',
      to: '/testcovid'
    },
    {
      title: 'Info Isoman',
      imagePath: 'info.svg'
    },
  ]
}

export const dataInformation = [
  {
    title: 'Test Covid 19',
    subtitle: 'Test COVID dapat membatu pelacakan penyebaran COVID 19 serta dapat mencegah penyebaranya. INATTI merupakan solusi untuk kebutuhan anda dengan metode PCR, Antigen, GeNOSE, yang tersedia pada sampling point di sekitar anda',
  },
  {
    title: 'Pelacakan Kontak',
    subtitle: 'Strategi pengendalian pandemik Covid-19 yang tepat memperluas kapasitas lab / test dan melakukan pelacakan kontak secara menyeluruh menemukan transmisi progresif yang positif untuk di isolasi.',
  },
  {
    title: 'Isolasi Mandiri',
    subtitle: 'Dapatkan tatacara isolasi mandiri - Live Chatting isolasi mandiri didampingi oleh apoteker yang merupakan bagian dari program kepatuhan terapi pengobatan (MTAC Unpad) secara online.',
  },
]

export const dataBannerSection = [
  {
    id: 1,
    title: '“Kelola sample dengan INATTI”',
    subtitle: 'INATTI Mempunyai sistem Pengelolaan Sample yang mana akan Mempermudah pengelolaan tes laboratorium untuk Fasilitas Kesehatan sebagai rujukan atau sampling poin.',
    paragraph: 'INATTI dapat membatu anda dalam mengelola sample test COVID 19 dengan system yang mudah di pahami dan di implementasikan guna memberikan hasil test dengan akurasi yang baik.',
    isOwner: true,
    backgroundColor: '#EBF2FA',
    imageUrl: 'hero_section.svg',
    topMargin: true,
  },
  {
    id: 2,
    title: '“INATTI BSL-2 Labo ”',
    subtitle: 'Integrasi dengan laboratorium melalui Sistem Informasi Laboratorium mempermudah pengelolaan sampel secara real time dengan hasil yang cepat dan akurat.',
    paragraph: 'Jadilah mitra kami dalam menyelengarakan dan mengelola test COVID 19',
    isOwner: false,
    backgroundColor: '#D9ED92',
    imageUrl: 'hero_section2.svg',
    topMargin: false,
  }
]

export const dataStatistic = [
  {
    id: 1,
    title: '+ 10347',
    subtitle: '<span class="font-weight-medium">Pasien</span>, yang telah melakukan pemeriksaan Covid-19; RT-PCR, Antigen, GeNose',
    isBorder: true,
  },
  {
    id: 2,
    title: '+ 21',
    subtitle: '<span class="font-weight-medium">Sampling Point</span> Faskes/Klinik/Rujukan yang bergabung dengan InaTTI, baik itu residen, drive thru, maupun homecare.',
    isBorder: true,
  },
  {
    id: 3,
    title: '2',
    subtitle: '<span class="font-weight-medium">Laboratorium Bergerak BSL-2</span> (Mobile Lab), silahkan berkonsultasi, apabila ingin merancang/membangun Mobile Lab yang sudah teruji hingga ke daerah pelosok terluar..',
    isBorder: true,
  },
  {
    id: 4,
    title: '1',
    subtitle: '<span class="font-weight-medium">Laboratorium</span> BSL-2 bagi pemerintah, bumn/d, swasta yang telah memiliki laboratorium dan ingin menggunakan InaTTI, silahkan bergabung dengan kami.',
    isBorder: false,
  },
]

export const aboutMe = {
  title: 'Tentang Kami',
  subtitle: 'InaTTI adalah aplikasi yang mempermudah melakukan test laboratorium, pelacakan kontak secara menyeluruh, dan pendampingan Apoteker untuk kepatuhan isolasi mandiri berkolaborasi dengan Medication Therapeutic Adherence Clinic (MTAC). MTAC pertama kali dikenalkan pada tahun 2004 yang merupakan bagian dari Ambulatory Clinic System. Sistem ini menekankan pada manajemen pengobatan untuk memperbaiki kualitas hidup, keamanan, dan keefektifan biaya pengobatan bagi pasien. MTAC adalah suatu pelayanan kefarmasian yang dilakukan oleh seorang apoteker sebagai bagian dari tim layanan kesehatan dalam konsep interprofesional collaborative practise, untuk pendampingan manajemen terapi obat baik secara farmakologi yang langsung terkait dengan interaksi obat dan terapi non farmakologi terkait hal-hal yang mendukung optimalisasi kerja obat seperti life style dan interaksi obat dengan makanan/minuman. MTAC bertujuan untuk meningkatkan kepatuhan pasien terhadap pengobatan yang diberikan, meningkatkan keberhasilan pengendalian keadaan suatu penyakit, mengurangi gejala penyakit, dan mengurangi kejadian medication errors.',
}

export const partnerData = [
  {
    id: 1,
    title: 'Kementerian Kesehatan Republik Indonesia',
    imageUrl: 'logo-kemenkes.png',
  },
  {
    id: 2,
    title: 'Indonesian High Alert Card - Kemenkes RI',
    imageUrl: 'qlogo.png',
  },
  {
    id: 3,
    title: 'Universitas Padjadjaran',
    imageUrl: 'logo_unpad.png',
  },
  {
    id: 4,
    title: 'Institut Pembangunan Jawa Barat',
    imageUrl: 'logo_injabar.png',
  },
  {
    id: 5,
    title: 'Pemerintah Jawa Barat',
    imageUrl: 'logo_perintah.png',
  },
  {
    id: 6,
    title: 'LG Internasional',
    imageUrl: 'logo_lg.png',
  },
  {
    id: 7,
    title: 'Institut Pembangunan Jawa Barat',
    imageUrl: 'logo_klinik_unpad.png',
  },
  {
    id: 8,
    title: 'MTAC Farmasi Unpad',
    imageUrl: 'logo_mtac.png',
  },
  {
    id: 10,
    title: 'PT. Jaswita Jabar',
    imageUrl: 'logo_jaswita.png',
  },
  {
    id: 11,
    title: 'IKatan Apoteker Indonesia',
    imageUrl: 'logo_ikai.png',
  },
  {
    id: 12,
    title: 'K-LAB COVID 19 Module LAB',
    imageUrl: 'logo_klab.png',
  },
  {
    id: 13,
    title: 'Universitas Padjadjaran',
    imageUrl: 'logo_medika.png',
  },
  {
    id: 14,
    title: 'Kimia Farma',
    imageUrl: 'logo_kimia_farma.png',
  },
  {
    id: 15,
    title: 'Mobile Clinic',
    imageUrl: 'logo_mobile_clinic.png',
  },
  {
    id: 16,
    title: 'Klinik Medika Antapani',
    imageUrl: 'logo_medika_antapni.png',
  },
  {
    id: 17,
    title: 'Klinik Rancamakmur & Apotek Al Rasyid',
    imageUrl: 'logo_rancamanyar.png',
  },
  {
    id: 18,
    title: 'PT. Usaha Bersama Jabar',
    imageUrl: 'logo_ubj.png',
  },
  {
    id: 19,
    title: 'INJABAR HomeCare',
    imageUrl: 'logo_home_care.png',
  },
]

export const mediaData = [
  {
    id: 1,
    title: 'Tribun Jabar',
    imagePath: 'logo_tribun.png',
  },
  {
    id: 2,
    title: 'Yahoo',
    imagePath: 'logo_yahoo.png',
  },
  {
    id: 3,
    title: 'Liputan 6',
    imagePath: 'logo_liputan6.png',
  },
  {
    id: 4,
    title: 'INews Jabar',
    imagePath: 'jabar.png',
  },
  {
    id: 5,
    title: 'Republika',
    imagePath: 'logo.png',
  },
  {
    id: 6,
    title: 'Pikiran Rakyat',
    imagePath: 'logo_pikiran_rakyat.png',
  },
  {
    id: 7,
    title: 'JPPN',
    imagePath: 'logo_jpn.png',
  },
  {
    id: 8,
    title: 'Kompas',
    imagePath: 'logo_kompas.png',
  },
  {
    id: 9,
    title: 'Tempo',
    imagePath: 'logo_tempo.png',
  },
  {
    id: 10,
    title: 'Berita Satu',
    imagePath: 'logo_news.png',
  },
  {
    id: 11,
    title: 'Sindo NEws',
    imagePath: 'logo_sindo.png',
  },
]
