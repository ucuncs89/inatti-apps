export const state = () => ({
  apotek: null,
  consultant: null,
  patient: null,
  apotekFilter: {
    selectedProv: { id: 32, name: 'Jawa Barat' },
    selectedKab: { id: 3204, name: 'KAB. BANDUNG' },
  },
})

export const mutations = {
  setApotek(state: any, payload: any) {
    state.apotek = payload
  },
  setConsultant(state: any, payload: any) {
    state.consultant = payload
  },
  setPatient(state: any, payload: any) {
    state.patient = payload
  },
  setApotekFilterProv(state: any, payload: any) {
    state.apotekFilter.selectedProv = payload
  },
  setApotekFilterKab(state: any, payload: any) {
    state.apotekFilter.selectedKab = payload
  },
  setApotekFilterSaved(state: any, payload: any) {
    state.apotekFilter.saved = payload
  },
  reset(state: any, payload: any) {
    state.apotek = null
    state.consultant = null
    state.patient = null
  },
}
