import { TransactionApi } from '@inatti/shared/api-nuxt'

export const state = () => ({
  dateTest: null,
  typeTest: {},
  amountPatient: null,
  patients: [],
  registerer: {
    name: null,
    countryCode: null,
    phonenumber: null,
    email: null,
  },
  faskes: {
    faskes: null,
    product: null,
  },

  selectedFilterProv: { id: 32, name: 'Jawa Barat' },
  selectedFilterKab: { id: 3204, name: 'KAB. BANDUNG' },
  savedFilter: null,

  foundAccount: {
    email: null,
    id: null,
    phoneNumber: null,
    username: null,
    sendOtpTo: null,
  },
})

export const mutations = {
  setFoundAccount(state, result) {
    state.foundAccount = result
  },

  setSendOtpForgetPass(state, val) {
    state.foundAccount.sendOtpTo = val
  },

  setDateTest(state, dateTest) {
    state.dateTest = dateTest
  },
  setTypeTest(state, typeTest) {
    state.typeTest = typeTest
  },
  setAmountPatient(state, amountPatient) {
    state.amountPatient = amountPatient
    if (state.patients.length < amountPatient) {
      for (let i = state.patients.length; i < amountPatient; i++) {
        state.patients.push({
          id: i,
        })
      }
    } else {
      state.patients.splice(amountPatient)
    }
  },
  addAmountPatient(state) {
    state.patients.push({ id: state.patients.length - 1 })
    state.amountPatient = state.patients.length
  },
  deletePatientAtIndex(state, index) {
    state.patients.splice(index, 1)
    state.amountPatient = state.patients.length
  },
  setPatientAtIndex(state, { index, patient }) {
    state.patients[index] = patient
  },

  setFaskes(state, payload) {
    state.faskes = { ...state.faskes, ...payload }
  },

  // not used,
  setRegisterer(state, registerer) {
    state.registerer = { ...state.registerer, ...registerer }
  },
  setFilterProv(state, val) {
    state.selectedFilterProv = val
  },
  setFilterKab(state, val) {
    state.selectedFilterKab = val
  },
  setFilterSaved(state, val) {
    state.savedFilter = val
  },
  resetState(state) {
    Object.assign(state, {
      dateTest: null,
      typeTest: {},
      amountPatient: null,
      patients: [],
      registerer: {
        name: '',
        countryCode: '+62',
        phonenumber: '',
        email: '',
      },
      faskes: {
        faskes: null,
        product: null,
      },

      selectedFilterProv: { id: 11, name: 'Aceh' },
      selectedFilterKab: { id: 1101, name: 'KAB. ACEH SELATAN' },
      savedFilter: null,
    })
  },
}

export const actions = {
  async createPayment({ commit, state }, methods) {
    // validations methods
    if (!methods) return
    if (typeof methods !== 'object') return
    if (!['Transfer', 'Cash'].includes(methods.type)) return

    const testcovid = {
      date: state.dateTest,
      type: state.typeTest.id,
    }
    const refTarget = {
      refId: state.faskes.faskes.id,
      productId: state.faskes.product.id,
    }
    const patients = []
    for (let i = 0; i < state.patients.length; i++) {
      const patient = state.patients[i]
      patients.push({
        id: patient.id,
        nik: patient.nik,
        passport: patient.passport,
        type: patient.type,
        name: patient.name,
        placebirth: patient.placebirth,
        datebirth: patient.datebirth,
        gender: patient.gender,
        phonenumber: patient.phonenumber,
        email: patient.email,
        provinsi: patient.provinsi ?? null,
        kabupaten: patient.kabupaten ?? null,
        kecamatan: patient.kecamatan ?? null,
        kelurahan: patient.kelurahan ?? null,
        kelurahanRt: patient.kelurahanRt,
        kelurahanRw: patient.kelurahanRw,
        domisiliProvinsi: patient.domisiliProvinsi ?? null,
        domisiliKabupaten: patient.domisiliKabupaten ?? null,
        domisiliKecamatan: patient.domisiliKecamatan ?? null,
        domisiliKelurahan: patient.domisiliKelurahan ?? null,
        domisiliKelurahanRt: patient.domisiliKelurahanRt,
        domisiliKelurahanRw: patient.domisiliKelurahanRw,
        address: patient.address,
        addressDomisili: patient.addressDomisili,
        citizenship: patient.citizenship,
      })
    }
    const data = {
      testcovid,
      refTarget,
      method: methods.type,
      patients,
    }

    const paymentParams = {
      data,
      productType: testcovid.type,
      axios: this.$axios,
      isLoggedIn: this.$auth.loggedIn,
    }
    let trx = onCreatePayment(paymentParams)
    return trx
  },
}

function onCreatePayment({ data, axios, isLoggedIn }) {
  const transactionApi = new TransactionApi(axios)
  data.info = data.testcovid
  delete data.testcovid
  return isLoggedIn ? transactionApi.createOrderLoggedIn(data) : transactionApi.createOrderGuest(data)
}
