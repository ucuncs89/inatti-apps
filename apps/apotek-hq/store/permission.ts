import { Context } from '@nuxt/types'
import jwtDecode from 'jwt-decode'

export const state = () => ({
  allowed: null,
  roleName: ''
})

export const mutations = {
  setAllowed(state: any, payload: any) {
    state.allowed = payload
  },
  setRoleName(state: any, payload: any) {
    state.roleName = payload
  },
}

export const actions = {
  // `force` to force fetch data
  async fetchRole({ commit, state }: any, { context, force }: any) {
    const { $axios }: Context = context
    // if force to fetch, continue
    if (state.allowed) {
      if (!force) {
        return false
      }
    }
    let api = '/apotek-headoffice-auth/role-user'
    try {
      let result: any = await $axios.$get(api)

      if (result.roleId) {
        commit('setAllowed', result.roleId.permission)
        commit('setRoleName', result.roleId.name)
      } else {
        throw new Error('No role selected')
      }
    } catch (error: any) {
      console.log(error.response)
    }
  },
}
