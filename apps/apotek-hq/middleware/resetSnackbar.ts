import { Context } from "@nuxt/types";

/**
 * Whenever change route, Reset Snackbar error !!
 */
export default function ({ store }: Context) {
  store.dispatch('snackbarStore/closeSnackbar');
}