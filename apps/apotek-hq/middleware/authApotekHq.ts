import { Context } from '@nuxt/types'

export default function (context: Context) {
  console.log("run middleware")
  const excludeRoute = ['/login']
  const isInExclude = (route) => excludeRoute.some((v) => route.startsWith(v))
  console.log(isInExclude(context.route.path))
  if (!context.$auth.loggedIn && !isInExclude(context.route.path)) {
    return context.redirect('/login' + '?from=' + context.route.path)
  }
}