import moment from 'moment'

/**
 * This function only works in Indonesia
 */
export const convertDate = (value, tzIndo) => {
  if (!value) { return '-' }

  let date = moment(value)

  const dbTzIndo = {
    WIB: 7, // +7:00 GMT
    WITA: 8,
    WIT: 9
  }

  if (tzIndo) {
    date = date.utcOffset(dbTzIndo[tzIndo])
  }

  return date.format(`DD MMMM YYYY, HH:mm:ss [${tzIndo ?? ''}]`)
}

export const convertDateBirth = (value) => {
  if (!value) { return '-' }

  return moment(value).format('DD MMMM YYYY')
}
