const QRCode = require('qrcode')

export const generateQRCode = async value => await QRCode.toDataURL(value)

export const UrlQrCodeTestCovid = (id) => {
  const urlUser = process.env.URL_FRONTEND_USER || 'https://app.inatti.id'
  return `${urlUser}/download-pdf/${id}`
}