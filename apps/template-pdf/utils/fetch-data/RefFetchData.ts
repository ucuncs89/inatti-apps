// eslint-disable-next-line import/named
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { generateQRCode, UrlQrCodeTestCovid } from '../GenerateQRCode'
import { SwabDetail, TypeTestCovid } from '../types'

export class RefFetchData {
  axios: NuxtAxiosInstance
  swabDetailData: null | SwabDetail = null

  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios
  }

  async fetchDetail(params: { type: TypeTestCovid, idSwab: string, idRef: string }): Promise<null | SwabDetail> {
    this.swabDetailData = await this.axios.$get('/api-ref/report-pdf/swab-detail/' + params.type, {
      params: {
        idswab: params.idSwab
      },
      headers: { Authorization: `Bearer ${process.env.USER_TOKEN}` }
    })

    return this.swabDetailData
  }

  async getQrCode(): Promise<null | { image: string, value: any }> {
    console.log(this.swabDetailData)
    if (!this.swabDetailData) { return null }
    const idOrder = this.swabDetailData.orderSwab.id
    const urlDownloadPdf = UrlQrCodeTestCovid(idOrder)
    // custom here
    const valueQrCode = urlDownloadPdf

    return {
      image: await generateQRCode(valueQrCode),
      value: valueQrCode
    }
  }

  async getTemplateInfo(params: { type: TypeTestCovid, idRef: string }) {
    const listType = {
      pcr: 1,
      antigen: 2
    }
    return await this.axios.$get('/api-ref/report-pdf/template', {
      params: {
        idref: params.idRef,
        typeProduct: listType[params.type]
      },
    })
  }
}
