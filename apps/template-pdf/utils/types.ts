
export interface Patient {
  id: string;
  nik: string;
  type: string;
  name: string;
  placebirth: string;
  datebirth: string;
  gender: string;
  phonenumber: string;
  email: string;
  bloodtype?: any;
  kelurahanRt?: any;
  kelurahanRw?: any;
  domisiliKelurahanRt?: any;
  domisiliKelurahanRw?: any;
  address: string;
  addressDomisili?: any;
  citizenship: string;
  createdAt: Date;
  narOrangId?: any;
}

export interface RefTarget {
  id: string;
  name: string;
  tenantId: string;
  createdAt: Date;
  narFaskesId: string;
}

export interface LabTarget {
  id: string;
  name: string;
  tenantId: string;
  createdAt: Date;
  narLabId: string;
}

export interface Summary {
  Cq: number;
  Well: string;
  Fluor: string;
  Sample: string;
  Target: string;
  Content: string;
}

export interface ResultTool {
  id: number;
  resultRaw: any;
  resultSummary: string;
  createdAt: Date;
}

export interface LabAnalyzeSession {
  id: number;
  locateRow: string;
  locateColumn: string;
  isControl: boolean;
  controlType?: any;
  controlName?: any;
  resultTool: ResultTool;
}

export interface SwabDetail {
  id: number;
  sampleId: string;
  status: string;
  swabDate: Date;
  resultSwab: string;
  resultSentAt?: any;
  isSendToLab: boolean;
  sendToLabDate: Date;
  createdAt: Date;
  narSendat?: any;
  narTestcovidId?: any;
  patient: Patient;
  refTarget: RefTarget;
  labTarget: LabTarget;
  labAnalyzeSession?: LabAnalyzeSession[];
  orderSwab: any;
}

export interface SwabOrders {
  id: string;
  price: number;
  method: string;
  detailSwab: SwabDetail;
}

export interface ProductSwab {
  id: number;
  name: string;
  description: string;
}

export interface RefProduct {
  id: number;
  name: string | null;
  price: number;
  product: ProductSwab;
}

export interface InvoiceDetail {
  invId: string;
  createdAt: string;
  totalPrice: number;
  feeAdmin: number | null;
  method: string;
  status: string;
  reference: RefTarget;
  orderSwab: SwabOrders[];
  product: RefProduct;
}

export type TypeTestCovid = 'pcr' | 'antigen'
