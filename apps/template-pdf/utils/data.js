export const data = {
  firstTable: [
    {
      title: 'Nama Pasien / Name',
      value: 'Hello'
    },
    {
      title: 'No. Identitas / Patien ID',
      value: 'Hello'
    },
    {
      title: 'Kebangsaan / Nationality',
      value: 'Hello'
    },
    {
      isSpace: true
    },
    {
      title: 'Sampling Point',
      value: 'Hello'
    },
    {
      title: 'Pengambilan Spesimen Date of Specimen Taken',
      value: 'Hello',
      isWidth: true
    },
    {
      title: 'Tanggal Sample Diterima Receiving date',
      value: 'Hello',
      isWidth: true
    }
  ],
  lastTable: [
    {
      title: 'Jenis Kelamin / Gender',
      value: 'Hello'
    },
    {
      title: 'Tgl. Lahir / DOB',
      value: 'Hello'
    },
    {
      title: 'Umur / Age',
      value: 'Hello'
    },
    {
      isSpace: true
    },
    {
      title: 'Tanggal Analisis Date of Analysis',
      value: 'Hello',
      isWidth: true
    },
    {
      title: 'Tanggal Hasil Keluar Result date',
      value: 'Hello',
      isWidth: true
    }
  ]
}

export const dataNotePcr = {
  title: 'Jenis Spesimen/ Spesimen Type: Naso and Oropharyngeal swab',
  note: 'Catatan/ Note:',
  list: [
    'Hasil hanya menggambarkan kondisi saat pengambilan sampel, sesuaikan dengan klinis pasien. Pemeriksaan ulang dapat dilakukan atas saran dokter penanggung jawab./ The result may only reﬂect the day when specimen was taken, please consult to your physician for follow up examination.',
    'Cut Off Gen Target: Positif 38, Negative > 38'
  ]
}

export const unisonDataTable = {
  firstTable: [
    {
      title: 'Nama / Name',
      value: 'Sukijan'
    },
    {
      title: 'NIK / ID Number',
      value: '12345678'
    },
    {
      title: 'Alamat / Address',
      value: 'Jl Jalan'
    },
    {
      title: 'Umur / Age',
      value: '90'
    },
    {
      title: 'Jenis Kelamin / Gender',
      value: 'Laki'
    }
  ],
  lastTable: [
    {
      title: 'Tempat Pengambilan Spesimen / Sampling Point',
      value: 'Unison Healthcare - Humble House Wijaya',
      isWidth: true
    },
    {
      title: 'Tanggal Pengambilan Spesimen / Date of Sample Collection',
      value: '04-11-2021 09:55 Nov-04-2021 09:55',
      isWidth: true
    },
    {
      title: 'Tanggal Pengambilan Spesimen / Sampling Point',
      value: '04-11-2021 09:55 Nov-04-2021 09:55',
      isWidth: true
    },
    {
      title: 'Tanggal Hasil Keluar / Result Date',
      value: '04-11-2021 09:55 Nov-04-2021 09:55',
      isWidth: true
    }
  ]
}

export const dataNoteUnison = {
  title: '',
  note: 'Catatan',
  list: [
    'Hasil pemeriksaan diatas dikerjakan dengan metode Swab Antigen dan hanya menunjukkan kondisi saat pengambilan specimen dan tidak mengesampingkan kemungkinan infeksi SARS-CoV-2',
    'Pemeriksaan ini untuk mendektesi materi genetik virus, tidak dapat membedakan kondisi virus yang hidup ataupun kondisi non-infeksius'
  ]
}

export const dataRemarkUnison = {
  title: null,
  note: 'Remarks',
  list: [
    'The result mentioned above uses Swab Antigen method and only describe condition at the time of specimen collection and does not rule out the possibility of SARS-CoV-2 Infection',
    'This test detects viral genetic material, this test cannot distinguish between live virus and non-infective virus'
  ]
}
