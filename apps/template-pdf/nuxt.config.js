export default {
  server:{
    port: process.env.NODE_PORT_TEMPLATE_PDF
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'template-pdf',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/utils'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxt/typescript-build',
    '@nuxtjs/date-fns'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  axios: {
    baseUrl: process.env.URL_BACKEND,
    headers: { Authorization: `Bearer ${process.env.USER_TOKEN}` }
  },

  env: {
    urlUser: process.env.URL_USER,
    userToken: process.env.USER_TOKEN,
    urlBackend: process.env.URL_BACKEND || 'https://api.inatti.id'
  },

  publicRuntimeConfig: {
    axios: {
      baseUrl: process.env.URL_BACKEND
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
