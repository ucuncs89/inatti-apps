/* eslint-disable @typescript-eslint/no-unused-vars */
import { Plugin } from '@nuxt/types'

declare module 'vue/types/vue' {
  // this.$toIdr inside Vue components
  interface Vue {
    $toIdr(number: number): void
    $returnValueWhenNull(text: string): void
  }
}

declare module '@nuxt/types' {
  // nuxtContext.app.$toIdr inside asyncData, fetch, plugins, middleware, nuxtServerInit
  interface NuxtAppOptions {
    $toIdr(number: number): void
    $returnValueWhenNull(text: string): void
  }
  // nuxtContext.$toIdr
  interface Context {
    $toIdr(number: number): void
    $returnValueWhenNull(text: string): void
  }
}

const utils: Plugin = (context, inject) => {
  inject('toIdr', (number: number) => { return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number) })
  inject('returnValueWhenNull', (text: string) => text ?? '-')
}

export default utils
