module.exports = {
  apps: [
    {
      name: 'template-inatti-prod',
      script: 'yarn',
      args: 'start'
    },
    {
      name: 'template-inatti-stag',
      script: 'yarn',
      args: 'start'
    }
  ]
}
