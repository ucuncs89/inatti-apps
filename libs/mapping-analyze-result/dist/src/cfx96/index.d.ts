export interface SummaryCellCfx96 {
    Well?: string;
    Fluor?: string;
    Sample?: string;
    Target?: string;
    Content?: string;
    Cq?: number;
}
export declare class resultCfx96 {
    amplification: any;
    summary: any;
}
export declare function readCfx96(amplificationFile: any, summaryFile: any): resultCfx96;
export declare type finalResult = 'positif' | 'negatif' | 'need verification';
export declare function Cfx96ResultDetector(summaries: SummaryCellCfx96[]): finalResult | null;
