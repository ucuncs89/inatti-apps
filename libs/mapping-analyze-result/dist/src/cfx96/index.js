"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cfx96ResultDetector = exports.readCfx96 = exports.resultCfx96 = void 0;
const helper_1 = require("./helper");
class resultCfx96 {
}
exports.resultCfx96 = resultCfx96;
function readCfx96(amplificationFile, summaryFile) {
    let amplificationJson = (0, helper_1.mapAmplificationResult)(amplificationFile);
    let summaryJson = (0, helper_1.mapSummaryResult)(summaryFile);
    return {
        amplification: amplificationJson,
        summary: summaryJson
    };
}
exports.readCfx96 = readCfx96;
function Cfx96ResultDetector(summaries) {
    let targetIc = null;
    let targetEgene = null;
    let targetRdrp = null;
    for (const summary of summaries) {
        if (summary.Target === 'IC' || summary.Target === 'HRP') {
            targetIc = summary.Cq;
        }
        if (summary.Target === 'E GENE') {
            targetEgene = summary.Cq;
        }
        if (summary.Target === 'RdRP GENE') {
            targetRdrp = summary.Cq;
        }
    }
    if (targetIc <= 35 && (!targetEgene || !targetRdrp)) {
        return 'negatif';
    }
    if (targetIc <= 35 && (targetEgene <= 38 && targetRdrp <= 38)) {
        return 'positif';
    }
    return 'need verification';
}
exports.Cfx96ResultDetector = Cfx96ResultDetector;
