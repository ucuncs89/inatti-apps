"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapSummaryResult = exports.mapAmplificationResult = void 0;
const xlsx = require("xlsx");
function mapAmplificationResult(bufferFile) {
    const workbook = xlsx.read(bufferFile, { type: 'buffer' });
    const MapResult = {};
    //sheet 'Run Information' no need to transform to JSON
    const sheetExcepJsonFormat = 'Run Information';
    // read per sheet
    workbook.SheetNames.forEach(sheetName => {
        if (sheetName === sheetExcepJsonFormat)
            return;
        const sheet = workbook.Sheets[sheetName];
        let cellArray = xlsx.utils.sheet_to_json(sheet, { header: 1 });
        let cellObjectHead = [];
        let cellObject = {};
        // transform sheet to json
        cellArray.forEach((row, index) => {
            if (index === 0) {
                cellObjectHead = row;
            }
            else {
                if (!cellObjectHead.length) {
                    throw new Error('Cannot found head cell');
                }
                cellObjectHead.forEach((head, indexHead) => {
                    if (cellObject[head]) {
                        cellObject[head].push(row[indexHead]);
                    }
                    else {
                        cellObject[head] = [row[indexHead]];
                    }
                });
            }
        });
        MapResult[sheetName] = cellObject;
    });
    MapResult[sheetExcepJsonFormat] = xlsx.utils.sheet_to_json(workbook.Sheets[sheetExcepJsonFormat], { header: "A" });
    return MapResult;
}
exports.mapAmplificationResult = mapAmplificationResult;
function mapSummaryResult(bufferFile) {
    const workbook = xlsx.read(bufferFile, { type: 'buffer' });
    const MapResult = {};
    //sheet 'Run Information' no need to transform to JSON
    const sheetExcepJsonFormat = 'Run Information';
    workbook.SheetNames.forEach(sheetName => {
        if (sheetName === sheetExcepJsonFormat)
            return;
        MapResult[sheetName] = {};
        const sheetJson = xlsx.utils.sheet_to_json(workbook.Sheets[sheetName]);
        sheetJson.forEach((row, index) => {
            if (!MapResult[sheetName][row['Well']]) {
                MapResult[sheetName][row['Well']] = [];
            }
            MapResult[sheetName][row['Well']].push(row);
        });
    });
    MapResult[sheetExcepJsonFormat] = xlsx.utils.sheet_to_json(workbook.Sheets[sheetExcepJsonFormat], { header: "A" });
    return MapResult;
}
exports.mapSummaryResult = mapSummaryResult;
