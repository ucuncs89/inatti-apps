"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readFilesQlAquant96 = exports.getResultWell = exports.readFileCurve = exports.readFileCt = void 0;
const type_1 = require("../type/type");
function readFileCt(fileBuffer) {
    const file = fileBuffer.toString();
    const lines = file.split("\n");
    // cut lines to 22
    const linesCut = lines.slice(22, lines.length);
    const lineData = linesCut.map((v) => v.split(";")); // split by ;
    const result = {};
    const parseCt = (val) => {
        val = val.replace(",", ".");
        return parseFloat(val);
    };
    for (const line of lineData) {
        const well = line[0];
        if (!well)
            break;
        const dye = line[4];
        const ct = parseCt(line[6]);
        const meanCt = parseCt(line[7]);
        const sampleName = line[2];
        const sampleType = line[3];
        const headerCt = {
            well,
            dye,
            ct,
            meanCt,
            sampleName,
            sampleType
        };
        if (!result[well]) {
            result[well] = [
                headerCt
            ];
        }
        else {
            result[well].push(headerCt);
        }
    }
    return result;
}
exports.readFileCt = readFileCt;
function readFileCurve(fileBuffer) {
    const file = fileBuffer.toString();
    const lines = file.split("\n");
    const linesCut = lines.slice(25, lines.length);
    const lineData = linesCut.map((v) => v.split(";")); // split by ;
    const result = {};
    let lastDye = null;
    for (const line of lineData) {
        if (["FAM", "VIC", "ROX"].includes(line[0])) {
            lastDye = line[0];
            continue;
        }
        const well = line[0];
        const dye = lastDye;
        const curve = line.slice(1, line.length).map((v) => {
            v = v.replace(",", ".");
            // Cut 5 character after ","
            v = v.slice(0, v.indexOf(".") + 5);
            return parseFloat(v);
        });
        if (!result[well]) {
            result[well] = {
                [dye]: curve
            };
        }
        else {
            result[well][dye] = curve;
        }
    }
    return result;
}
exports.readFileCurve = readFileCurve;
function getResultWell(ctValue) {
    const ctDye = {
        ROX: null,
        VIC: null,
        FAM: null,
    };
    for (const ct of ctValue) {
        ctDye[ct.dye] = ct.ct;
    }
    if (!ctDye.ROX) {
        return type_1.ResultToolI.NEED_VERIFICATION;
    }
    if (ctDye.VIC >= 40 || ctDye.FAM >= 40) {
        return type_1.ResultToolI.NEGATIF;
    }
    if (!ctDye.VIC || !ctDye.FAM) {
        return type_1.ResultToolI.NEGATIF;
    }
    if (ctDye.ROX < 40 && ctDye.VIC < 40 && ctDye.FAM < 40) {
        return type_1.ResultToolI.POSITIF;
    }
    return type_1.ResultToolI.NEED_VERIFICATION;
}
exports.getResultWell = getResultWell;
function readFilesQlAquant96(fileCt, fileCurve) {
    const resCt = readFileCt(fileCt);
    const resCurve = readFileCurve(fileCurve);
    const result = {};
    for (const well in resCt) {
        result[well] = {
            ct: resCt[well],
            curve: resCurve[well],
            result: getResultWell(resCt[well])
        };
    }
    return result;
}
exports.readFilesQlAquant96 = readFilesQlAquant96;
