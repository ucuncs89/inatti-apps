/// <reference types="node" />
import { ResultToolI } from "../type/type";
export interface headerCtResult {
    well: string;
    dye: string;
    ct: number;
    meanCt: number;
    sampleType?: string;
    sampleName?: string;
}
export interface ctResult {
    [id: string]: headerCtResult[];
}
export interface valueCurveResult {
    [id: string]: number[];
}
export interface curveResult {
    [id: string]: valueCurveResult;
}
export interface resultQlAquant96 {
    [id: string]: WellResultQlAquant96;
}
export interface WellResultQlAquant96 {
    ct: headerCtResult[];
    curve: valueCurveResult;
    result: ResultToolI;
}
export declare function readFileCt(fileBuffer: Buffer): ctResult;
export declare function readFileCurve(fileBuffer: Buffer): curveResult;
export declare function getResultWell(ctValue: headerCtResult[]): ResultToolI;
export declare function readFilesQlAquant96(fileCt: Buffer, fileCurve: Buffer): resultQlAquant96;
