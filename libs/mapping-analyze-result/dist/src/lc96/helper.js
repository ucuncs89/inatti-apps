"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapTxtToJson = void 0;
function mapTxtToJson(bufferFile, encoding) {
    let lines = bufferFile.toString(encoding).split('\r\n');
    let map = lines.map((line) => line.split('\t'));
    let header = map[0];
    let tree = map.slice(1).map((value) => {
        let obj = {};
        header.forEach((head, index) => {
            obj[head] = value[index];
        });
        return obj;
    });
    return tree;
}
exports.mapTxtToJson = mapTxtToJson;
