"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readLc69File = void 0;
const helper_1 = require("./helper");
const defaultOption = {
    removeEmptyValue: false
};
function readLc69File(fileBuffer, options = defaultOption) {
    let fileRaw = fileBuffer.fileRaw;
    let fileFluorescence = fileBuffer.fileFluorescence;
    let rawData = (0, helper_1.mapTxtToJson)(fileRaw, 'utf8');
    let floures = (0, helper_1.mapTxtToJson)(fileFluorescence, 'utf16le');
    let flouresMap = {};
    if (!Object.keys(floures).length) {
        throw new Error("FLoures file tidak terbaca");
    }
    // Loop floures value, and map based position, dye and value time
    for (const value of floures) {
        let key = Object.keys(value);
        let keyHead = key[key.length - 1];
        if (value[keyHead]) {
            let [position, dye] = value[keyHead].split(' ');
            // delete head, so left only value
            delete value[keyHead];
            let valueTime = Object.assign({}, value);
            let valueTimeNew = {};
            // remove empty value
            if (options.removeEmptyValue) {
                for (const time of Object.keys(valueTime)) {
                    if (!!valueTime[time]) {
                        valueTimeNew[time] = valueTime[time];
                    }
                }
            }
            else {
                valueTimeNew = Object.assign({}, valueTime);
            }
            if (!flouresMap[position]) {
                flouresMap[position] = {};
            }
            flouresMap[position][dye] = valueTimeNew;
        }
    }
    let resultLc96 = {};
    if (!Object.keys(rawData).length) {
        throw new Error("Raw data tidak terbaca");
    }
    // map raw data, and fix them to ResultLc96
    let index = 0;
    for (const rawValue of rawData) {
        // key color has unique unicode, so need to swab new value
        let keyColor = Object.keys(rawData[index])[0];
        rawData[index].ColorTrue = rawData[index][keyColor];
        if (!resultLc96[rawValue.Position]) {
            resultLc96[rawValue.Position] = {
                sampleName: rawValue["Sample Name"],
                dye: {}
            };
        }
        resultLc96[rawValue.Position].dye[rawValue.Dye] = rawValue;
        index++;
    }
    return {
        rawData: resultLc96,
        stepData: flouresMap
    };
}
exports.readLc69File = readLc69File;
