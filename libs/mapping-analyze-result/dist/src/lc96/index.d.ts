import { OptionLc96 } from "../../type";
export declare function readLc69File(fileBuffer: {
    fileFluorescence: any;
    fileRaw: any;
}, options?: OptionLc96): any;
