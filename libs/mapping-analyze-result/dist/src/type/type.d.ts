export declare enum ResultToolI {
    POSITIF = "positif",
    NEGATIF = "negatif",
    NEED_VERIFICATION = "need verification"
}
