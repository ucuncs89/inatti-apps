"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResultToolI = void 0;
var ResultToolI;
(function (ResultToolI) {
    ResultToolI["POSITIF"] = "positif";
    ResultToolI["NEGATIF"] = "negatif";
    ResultToolI["NEED_VERIFICATION"] = "need verification";
})(ResultToolI = exports.ResultToolI || (exports.ResultToolI = {}));
