"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CamelCaseToText = exports.textToCamelCase = void 0;
function textToCamelCase(text) {
    let words = text.split(' ');
    let result = '';
    for (let i = 0; i < words.length; i++) {
        result += words[i][0].toUpperCase() + words[i].slice(1);
    }
    return result;
}
exports.textToCamelCase = textToCamelCase;
function CamelCaseToText(camelCase) {
    let result = '';
    for (let i = 0; i < camelCase.length; i++) {
        if (camelCase[i] === camelCase[i].toUpperCase()) {
            result += ' ';
        }
        result += camelCase[i];
    }
    return result;
}
exports.CamelCaseToText = CamelCaseToText;
