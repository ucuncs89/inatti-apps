"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.transformToJson = void 0;
const text_1 = require("./text");
function transformToJson(rowArray, isHeadCamelcase = false) {
    let header = rowArray[0];
    header.forEach((head, index) => {
        if (isHeadCamelcase) {
            header[index] = (0, text_1.CamelCaseToText)(head);
        }
    });
    let result = [];
    for (let i = 1; i < rowArray.length; i++) {
        let row = {};
        for (let j = 0; j < rowArray[i].length; j++) {
            row[header[j]] = rowArray[i][j];
        }
        result.push(row);
    }
    return result;
}
exports.transformToJson = transformToJson;
