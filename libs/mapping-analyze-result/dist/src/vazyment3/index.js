"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readVazyment3 = void 0;
const xlsx = require("xlsx");
const type_1 = require("../type/type");
const data_1 = require("../utils/data");
function readVazyment3(bufferFile) {
    const workbook = xlsx.read(bufferFile, { type: 'buffer' });
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const sheetArr = xlsx.utils.sheet_to_json(worksheet, { header: 1 });
    let sheetJson = (0, data_1.transformToJson)(sheetArr);
    let group = groupResult(sheetJson);
    let drnGraph = getDrnGraph(sheetArr);
    for (const idSample in group) {
        if (!Object.prototype.hasOwnProperty.call(group, idSample)) {
            continue;
        }
        const row = group[idSample];
        group[idSample].result = findResult(row.rows);
        group[idSample].drn = drnGraph[idSample];
    }
    return group;
}
exports.readVazyment3 = readVazyment3;
function getDrnGraph(sheet) {
    const result = {};
    for (const row of sheet) {
        const id = row[1];
        const target = row[6];
        if (id !== 'ID' && !id) {
            continue;
        }
        const drn = row.slice(24, 68);
        if (!result[id]) {
            result[id] = {
                [target]: drn
            };
            continue;
        }
        result[id][target] = drn;
    }
    return result;
}
function groupResult(rowData) {
    let group = {};
    for (const row of rowData) {
        if (!row)
            continue;
        if (group[row['ID']]) {
            group[row['ID']].rows.push(row);
        }
        else {
            group[row['ID']] = {
                rows: [row],
                well: row['Well'],
                result: null,
                drn: null
            };
        }
    }
    return group;
}
// format : ORF1ab, N, IC
// ex : if ORF1ab, N, IC all positive, return 'positif'
let ResultMap = {
    '+++': type_1.ResultToolI.POSITIF,
    '++-': type_1.ResultToolI.POSITIF,
    '--+': type_1.ResultToolI.NEGATIF,
};
function textToSign(text) {
    if (/positi(f|ve)/i.test(text)) {
        return '+';
    }
    if (/negati(f|ve)/i.test(text)) {
        return '-';
    }
}
function findResult(dataRow) {
    var _a;
    let resultTarget = {
        'ORF1ab': null,
        'IC': null,
        'N': null,
    };
    // Negative, Positive
    dataRow.forEach((row) => {
        resultTarget[row.Target] = textToSign(row['Test Result']);
    });
    let compositionResult = `${resultTarget.ORF1ab}${resultTarget.N}${resultTarget.IC}`;
    return (_a = ResultMap[compositionResult]) !== null && _a !== void 0 ? _a : null;
}
