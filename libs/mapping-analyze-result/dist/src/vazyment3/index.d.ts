/// <reference types="node" />
export interface Header {
    'Date': string;
    'ID': string;
    'Well': string;
    'Sample Name': string;
    'Reporter': string;
    'Sample Type': string;
    'Target': string;
    'Ct': string;
    'Quantity': string;
    'Test Result': string;
    'Qualitative(Ct)': string;
    'Quantitative(Copy)': string;
    'Test Mode': string;
    'Unit': string;
    'Encode': string;
    'Name': string;
    'Gender': string;
    'Age': string;
    'Division': string;
    'Ward': string;
    'Out Patient No.': string;
    'Source': string;
    'Doctor': string;
    'Physician': string;
    'DRn': string;
}
export interface ResultTypeVazyment3 {
    well: any;
    rows: Header[];
    result: string | null;
    drn: string[] | any;
}
export declare type ObjectResultTypeVazyment3 = {
    [id: string]: ResultTypeVazyment3;
};
export declare function readVazyment3(bufferFile: Buffer): ObjectResultTypeVazyment3;
