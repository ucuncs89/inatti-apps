export declare type RawDataType = {
    'Color': string;
    'Position': string;
    'Sample Name': string;
    'Gene Name': string;
    'Cq': string;
    'Concentration': string;
    'Call': string;
    'Excluded': string;
    'Sample Type': string;
    'Standard': string;
    'Cq Mean': string;
    'Cq Error': string;
    'Concentration Mean': string;
    'Concentration Error': string;
    'Replicate Group': string;
    'Dye': string;
    'Edited Call': string;
    'Slope': string;
    'EPF': string;
    'Failure': string;
    'Notes': string;
    'Sample Prep Notes': string;
    'Number': string;
    ColorTrue: string;
};
export declare type ResultRawType = {
    [position: string]: {
        sampleName: string;
        dye: {
            [typeDye: string]: RawDataType;
        };
    };
};
export declare type StepDataType = {
    [position: string]: {
        [typeDye: string]: {
            [step: string]: string;
        };
    };
};
export declare type ResultLc96 = {
    rawData: ResultRawType;
    stepData: StepDataType;
};
export interface OptionLc96 {
    removeEmptyValue?: boolean;
}
