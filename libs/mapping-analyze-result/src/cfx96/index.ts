import { mapAmplificationResult, mapSummaryResult } from './helper';

export interface SummaryCellCfx96 {
  Well?: string
  Fluor?: string
  Sample?: string
  Target?: string
  Content?: string
  Cq?: number
}

export class resultCfx96 {
  amplification: any
  summary: any
}

export function readCfx96(amplificationFile, summaryFile): resultCfx96 {
  let amplificationJson = mapAmplificationResult(amplificationFile)
  let summaryJson = mapSummaryResult(summaryFile)

  return {
    amplification: amplificationJson,
    summary: summaryJson
  }
}

export type finalResult = 'positif' | 'negatif' | 'need verification'

export function Cfx96ResultDetector(summaries: SummaryCellCfx96[]): finalResult | null {
  let targetIc:number = null
  let targetEgene:number = null
  let targetRdrp:number = null
  for (const summary of summaries) {
    if (summary.Target === 'IC' || summary.Target === 'HRP') {
      targetIc = summary.Cq
    }
    if (summary.Target === 'E GENE' ) {
      targetEgene = summary.Cq
    }
    if (summary.Target === 'RdRP GENE') {
      targetRdrp = summary.Cq
    }
  }

  if(targetIc <= 35 && (!targetEgene || !targetRdrp)){
    return 'negatif'
  }
  if(targetIc <= 35 && (targetEgene <= 38 && targetRdrp <= 38)){
    return 'positif'
  }
  return 'need verification'
}