import * as xlsx from 'xlsx'

export function mapAmplificationResult(bufferFile: any): any {
  const workbook: xlsx.WorkBook = xlsx.read(bufferFile, { type: 'buffer' })

  const MapResult = {}
  //sheet 'Run Information' no need to transform to JSON
  const sheetExcepJsonFormat = 'Run Information'

  // read per sheet
  workbook.SheetNames.forEach(sheetName => {
    if (sheetName === sheetExcepJsonFormat) return;

    const sheet: xlsx.WorkSheet = workbook.Sheets[sheetName]
    let cellArray = xlsx.utils.sheet_to_json(sheet, { header: 1 })

    let cellObjectHead = []
    let cellObject = {}

    // transform sheet to json
    cellArray.forEach((row: string[], index) => {
      if (index === 0) {
        cellObjectHead = row
      } else {
        if (!cellObjectHead.length) {
          throw new Error('Cannot found head cell')
        }
        cellObjectHead.forEach((head, indexHead) => {
          if (cellObject[head]) {
            cellObject[head].push(row[indexHead])
          } else {
            cellObject[head] = [row[indexHead]]
          }
        })
      }
    })
    MapResult[sheetName] = cellObject
  });

  MapResult[sheetExcepJsonFormat] = xlsx.utils.sheet_to_json(workbook.Sheets[sheetExcepJsonFormat], { header: "A" })

  return MapResult
}

export function mapSummaryResult(bufferFile): any {
  const workbook: xlsx.WorkBook = xlsx.read(bufferFile, { type: 'buffer' })
  const MapResult = {}
  //sheet 'Run Information' no need to transform to JSON
  const sheetExcepJsonFormat = 'Run Information'

  workbook.SheetNames.forEach(sheetName => {
    if (sheetName === sheetExcepJsonFormat) return
    MapResult[sheetName] = {}

    const sheetJson = xlsx.utils.sheet_to_json(workbook.Sheets[sheetName])
    sheetJson.forEach((row: Record<string, string>, index) => {
      if (!MapResult[sheetName][row['Well']]) {
        MapResult[sheetName][row['Well']] = []
      }
      MapResult[sheetName][row['Well']].push(row)
    });
  })

  MapResult[sheetExcepJsonFormat] = xlsx.utils.sheet_to_json(workbook.Sheets[sheetExcepJsonFormat], { header: "A" })

  return MapResult
}