import { CamelCaseToText } from "./text"

export function transformToJson(rowArray: any, isHeadCamelcase: boolean = false): any {
  let header = rowArray[0];
  header.forEach((head, index) => {
    if (isHeadCamelcase) {
      header[index] = CamelCaseToText(head);
    }
  });
  
  let result = [];
  for (let i = 1; i < rowArray.length; i++) {
    let row = {};
    for (let j = 0; j < rowArray[i].length; j++) {
      row[header[j]] = rowArray[i][j];
    }
    result.push(row);
  }
  return result;
}

