export function textToCamelCase(text:string) {
  let words = text.split(' ');
  let result = '';
  for (let i = 0; i < words.length; i++) {
    result += words[i][0].toUpperCase() + words[i].slice(1);
  }
  return result;
}

export function CamelCaseToText(camelCase:string) {
  let result = '';
  for (let i = 0; i < camelCase.length; i++) {
    if (camelCase[i] === camelCase[i].toUpperCase()) {
      result += ' ';
    }
    result += camelCase[i];
  }
  return result;
}