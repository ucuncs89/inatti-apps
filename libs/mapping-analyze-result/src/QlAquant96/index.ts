import { ResultToolI } from "../type/type"

export interface headerCtResult {
  well: string,
  dye: string,
  ct: number,
  meanCt: number,
  sampleType?: string
  sampleName?: string,
}

export interface ctResult {
  [id: string]: headerCtResult[]
}

export interface valueCurveResult {
  [id: string]: number[]
}
export interface curveResult {
  [id: string]: valueCurveResult
}

export interface resultQlAquant96 {
  [id: string]: WellResultQlAquant96
}

export interface WellResultQlAquant96 {
  ct: headerCtResult[],
  curve: valueCurveResult,
  result: ResultToolI
}

export function readFileCt (fileBuffer: Buffer) {
  const file = fileBuffer.toString()
  const lines = file.split("\n")

  // cut lines to 22
  const linesCut = lines.slice(22, lines.length)
  const lineData = linesCut.map((v) => v.split(";")) // split by ;

  const result: ctResult = {}

  const parseCt = (val) =>{
    val = val.replace(",", ".")
    return parseFloat(val)
  }

  for (const line of lineData) {
    const well = line[0]
    if(!well) break;
    const dye = line[4]
    const ct = parseCt(line[6])
    const meanCt = parseCt(line[7])
    const sampleName = line[2]
    const sampleType = line[3]

    const headerCt: headerCtResult = {
      well,
      dye,
      ct,
      meanCt,
      sampleName,
      sampleType
    }

    if (!result[well]) {
      result[well] = [
        headerCt
      ]
    } else {
      result[well].push(headerCt)
    }
  }
  return result
}

export function readFileCurve (fileBuffer: Buffer):curveResult {
  const file = fileBuffer.toString()
  const lines = file.split("\n")
  
  const linesCut = lines.slice(25, lines.length)
  const lineData = linesCut.map((v) => v.split(";")) // split by ;
  const result: curveResult = {}

  let lastDye = null
  for (const line of lineData) {
    if(["FAM", "VIC", "ROX"].includes(line[0])){
      lastDye = line[0]
      continue
    }
    
    const well = line[0]
    const dye = lastDye
    const curve = line.slice(1, line.length).map((v) => {
      v = v.replace(",", ".")

      // Cut 5 character after ","
      v = v.slice(0, v.indexOf(".") + 5)
      return parseFloat(v)
    })

    if (!result[well]) {
      result[well] = {
        [dye]: curve
      }
    } else {
      result[well][dye] = curve
    }

  }

  return result
}

export function getResultWell (ctValue: headerCtResult[]): ResultToolI {
  const ctDye = {
    ROX: null,
    VIC: null,
    FAM: null,
  }

  for (const ct of ctValue) {
    ctDye[ct.dye] = ct.ct
  }
  if(!ctDye.ROX) {
    return ResultToolI.NEED_VERIFICATION
  }
  if(ctDye.VIC >= 40 || ctDye.FAM >= 40){
    return ResultToolI.NEGATIF
  }
  if(!ctDye.VIC || !ctDye.FAM){
    return ResultToolI.NEGATIF
  }
  if(ctDye.ROX < 40 && ctDye.VIC < 40 && ctDye.FAM < 40){
    return ResultToolI.POSITIF
  }
  return ResultToolI.NEED_VERIFICATION
}

export function readFilesQlAquant96 (fileCt: Buffer, fileCurve: Buffer):resultQlAquant96 {
  const resCt = readFileCt(fileCt)
  const resCurve = readFileCurve(fileCurve)

  const result:resultQlAquant96 = {}

  for (const well in resCt) {
    result[well] = {
      ct: resCt[well],
      curve: resCurve[well],
      result: getResultWell(resCt[well])
    }
  }

  return result
}