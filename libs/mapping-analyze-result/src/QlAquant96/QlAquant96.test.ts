import { getResultWell } from ".";
import { ResultToolI } from "../type/type";

describe("Test result well positif condition", () => {
  it("should return positif 1", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: 26.19, meanCt: 40},
      { well:"A1", dye: "VIC", ct: 17.62, meanCt: 40},
      { well:"A1", dye: "FAM", ct: 20.25, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.POSITIF)
  })
  it("should return positif 2", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: 27.81, meanCt: 40},
      { well:"A1", dye: "VIC", ct: 26.16, meanCt: 40},
      { well:"A1", dye: "FAM", ct: 28.86, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.POSITIF)
  })
})

describe("Test result well negatif condition", () => {
  it("should return negatif 1", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: 25.86, meanCt: 40},
      { well:"A1", dye: "VIC", ct: null, meanCt: 40},
      { well:"A1", dye: "FAM", ct: null, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.NEGATIF)
  })
  it("should return negatif 2", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: 25.63, meanCt: 40},
      { well:"A1", dye: "VIC", ct: 37.82, meanCt: 40},
      { well:"A1", dye: "FAM", ct: 40.89, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.NEGATIF)
  })
  it("should return negatif 3", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: 31.69, meanCt: 40},
      { well:"A1", dye: "VIC", ct: 38.39, meanCt: 40},
      { well:"A1", dye: "FAM", ct: 43.06, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.NEGATIF)
  })
})

describe("Test result need verification", () => {
  it("should return need verification", () => {
    
    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: null, meanCt: 40},
      { well:"A1", dye: "VIC", ct: 38.39, meanCt: 40},
      { well:"A1", dye: "FAM", ct: 43.06, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.NEED_VERIFICATION)
  })

  it("Should return need verification if null", () => {

    // ignore meanCt and well
    const ctValue = [
      { well:"A1", dye: "ROX", ct: null, meanCt: 40},
      { well:"A1", dye: "VIC", ct: null, meanCt: 40},
      { well:"A1", dye: "FAM", ct: null, meanCt: 40},
    ]
    expect(getResultWell(ctValue)).toBe(ResultToolI.NEED_VERIFICATION)

  })
})