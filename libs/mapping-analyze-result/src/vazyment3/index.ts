import * as xlsx from 'xlsx'
import { ResultToolI } from '../type/type'
import { transformToJson } from '../utils/data'

export interface Header {
  'Date': string
  'ID': string
  'Well': string
  'Sample Name': string
  'Reporter': string
  'Sample Type': string
  'Target': string
  'Ct': string
  'Quantity': string
  'Test Result': string
  'Qualitative(Ct)': string
  'Quantitative(Copy)': string
  'Test Mode': string
  'Unit': string
  'Encode': string
  'Name': string
  'Gender': string
  'Age': string
  'Division': string
  'Ward': string
  'Out Patient No.': string
  'Source': string
  'Doctor': string
  'Physician': string
  'DRn': string
}

export interface ResultTypeVazyment3 {
  well: any,
  rows: Header[],
  result: string | null,
  drn: string[] | any
}

export type ObjectResultTypeVazyment3 = {
  [id:string]:ResultTypeVazyment3
}

export function readVazyment3(bufferFile: Buffer): ObjectResultTypeVazyment3{
  const workbook: xlsx.WorkBook = xlsx.read(bufferFile, {type: 'buffer'})

  const worksheet = workbook.Sheets[workbook.SheetNames[0]]
  const sheetArr = xlsx.utils.sheet_to_json(worksheet, { header: 1 })

  let sheetJson:Header[] = transformToJson(sheetArr)
  
  let group = groupResult(sheetJson)
  
  let drnGraph = getDrnGraph(sheetArr)
  

  for (const idSample in group) {
    if (!Object.prototype.hasOwnProperty.call(group, idSample)) {
      continue;
    }
    const row = group[idSample];
    group[idSample].result = findResult(row.rows)
    group[idSample].drn = drnGraph[idSample];
  }
  return group
}

function getDrnGraph(sheet:any | string[][]){
  const result = {}
  for (const row of sheet) {
    const id = row[1];
    const target = row[6];

    if(id !== 'ID' && !id){
      continue;
    }

    const drn = row.slice(24, 68)
    
    if(!result[id]){
      result[id] = {
        [target] : drn
      }
      continue;
    }
    result[id][target] = drn;
  }
  return result;
}

function groupResult(
  rowData: Header[]
): ObjectResultTypeVazyment3 {
  let group:ObjectResultTypeVazyment3 = {}
  for (const row of rowData) { 
    if(!row) continue;

    if(group[row['ID']]){
      group[row['ID']].rows.push(row)
    } else {
      group[row['ID']] = {
        rows: [row],
        well: row['Well'],
        result: null,
        drn: null
      }
    }
  }
  return group;
}

// format : ORF1ab, N, IC
// ex : if ORF1ab, N, IC all positive, return 'positif'
let ResultMap:{[format: string]: ResultToolI} = {
  '+++': ResultToolI.POSITIF,
  '++-': ResultToolI.POSITIF,
  '--+': ResultToolI.NEGATIF,
}

function textToSign(text) {
  if(/positi(f|ve)/i.test(text)){
    return '+'
  }
  if(/negati(f|ve)/i.test(text)){
    return '-'
  }
}

function findResult(dataRow: Header[]):string | null{
  let resultTarget = {
    'ORF1ab': null,
    'IC': null,
    'N': null,
  }
  // Negative, Positive
  
  dataRow.forEach((row) => {
    resultTarget[row.Target] = textToSign(row['Test Result'])
  })
  
  let compositionResult = `${resultTarget.ORF1ab}${resultTarget.N}${resultTarget.IC}`;
  return ResultMap[compositionResult] ?? null
}