import { mapTxtToJson } from "./helper"
import { RawDataType, OptionLc96, ResultRawType, StepDataType } from "../../type"

const defaultOption: OptionLc96 = {
  removeEmptyValue: false
}

export function readLc69File(fileBuffer: { fileFluorescence: any, fileRaw: any }, options: OptionLc96 = defaultOption): any {
  let fileRaw = fileBuffer.fileRaw
  let fileFluorescence = fileBuffer.fileFluorescence
  let rawData: RawDataType[] = mapTxtToJson(fileRaw, 'utf8')
  let floures = mapTxtToJson(fileFluorescence, 'utf16le')
  let flouresMap: StepDataType = {}
  if (!Object.keys(floures).length) {
    throw new Error("FLoures file tidak terbaca")
  }

  // Loop floures value, and map based position, dye and value time
  for (const value of floures) {

    let key = Object.keys(value)
    let keyHead = key[key.length - 1]

    if (value[keyHead]) {
      let [position, dye] = value[keyHead].split(' ')
      // delete head, so left only value
      delete value[keyHead]
      let valueTime = { ...value }

      let valueTimeNew = {}

      // remove empty value
      if (options.removeEmptyValue) {
        for (const time of Object.keys(valueTime)) {
          if (!!valueTime[time]) {
            valueTimeNew[time] = valueTime[time]
          }
        }
      } else {
        valueTimeNew = { ...valueTime }
      }

      if (!flouresMap[position]) {
        flouresMap[position] = {}
      }

      flouresMap[position][dye] = valueTimeNew
    }
  }

  let resultLc96: ResultRawType = {}

  if (!Object.keys(rawData).length) {
    throw new Error("Raw data tidak terbaca")
  }
  // map raw data, and fix them to ResultLc96
  let index = 0;
  for (const rawValue of rawData) {
    // key color has unique unicode, so need to swab new value
    let keyColor = Object.keys(rawData[index])[0]
    rawData[index].ColorTrue = rawData[index][keyColor]
    if (!resultLc96[rawValue.Position]) {
      resultLc96[rawValue.Position] = {
        sampleName: rawValue["Sample Name"],
        dye: {}
      }
    }
    resultLc96[rawValue.Position].dye[rawValue.Dye] = rawValue
    index++;
  }

  return {
    rawData: resultLc96,
    stepData: flouresMap
  }
} 