export function mapTxtToJson(bufferFile: any, encoding: BufferEncoding): any {
  let lines = bufferFile.toString(encoding).split('\r\n')
  let map = lines.map((line) => line.split('\t'))
  let header = map[0]
  let tree = map.slice(1).map((value) => {
    let obj = {}
    header.forEach((head, index) => {
      obj[head] = value[index]
    });
    return obj
  })
  return tree
}