import * as fs from 'fs';
import { readVazyment3 } from '../src/vazyment3/index'

const fileExample = 'example/vazyment3/P_211213_SHIFT2_BATCH4.xls'
const file = fs.readFileSync(fileExample)

let result = readVazyment3(file);
for (const id in result) {
  if (Object.prototype.hasOwnProperty.call(result, id)) {
    const element = result[id];
    console.log(`${element.well} : ${element.result}`);
  }
}



fs.writeFile('result.json', JSON.stringify(result, null, 2), (err) => {
  console.log(err);
})