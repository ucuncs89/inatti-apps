import { open, openSync, readFileSync, writeFile } from "fs";
import { readLc69File } from "../src/lc96"

console.log(__dirname)

let fileRawData = '/home/riochndr/project/inatti/analyze-tool-convert/example/13082020 - Abs Quant.txt'
let fileFluorescence = '/home/riochndr/project/inatti/analyze-tool-convert/example/amplification row.txt'

let lc96Raw = readFileSync(fileRawData, "utf-8")
let lc96Fluo = readFileSync(fileFluorescence, "utf16le")


let result = readLc69File({
  fileFluorescence: lc96Fluo,
  fileRaw: lc96Raw
})

writeFile('result.json', JSON.stringify(result, null, 2), (err) => {
  console.log(err);
})