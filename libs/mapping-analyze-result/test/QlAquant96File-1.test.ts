import * as fs from 'fs'
import { readFilesQlAquant96 } from '../src/QlAquant96/index'
import { ResultToolI } from '../src/type/type'
/**
 * Test file in folrder example/QlAquant96/1
 */
const locateFileCt = 'example/QlAquant96/1/ct.csv'
const locateFileCurve = 'example/QlAquant96/1/curve.csv'

const fileCt = fs.readFileSync(locateFileCt)
const fileCurve = fs.readFileSync(locateFileCurve)

let res = readFilesQlAquant96(fileCt, fileCurve)

fs.writeFile('resultQlAquant-1.json', JSON.stringify(res, null, 2), () => {})

// print result from res
for (const id in res) {
  if (Object.prototype.hasOwnProperty.call(res, id)) {
    const element = res[id];
    console.log(`${id} : ${element.result}`);
    let dyes = []
    for (const ct of element.ct) {
      dyes.push(` ${ct.dye} : ${ct.ct}`);
    }
    console.log(dyes.join(","))
    console.log("")
  }
}