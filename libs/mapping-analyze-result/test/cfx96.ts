import * as fs from 'fs'
import { mapSummaryResult, mapAmplificationResult } from "../src/cfx96/helper"

const fileExampleSummary = 'example/CFX96-Quantification Summary.xlsx'
let fileSummary = fs.readFileSync(fileExampleSummary);
console.log(`File length ${fileSummary.length}`);

let summaryJson = mapSummaryResult(fileSummary)

const fileExampleAmplification = 'example/CFX96-Quantification Amplification Results.xlsx'
let fileAmplification = fs.readFileSync(fileExampleAmplification);
console.log(`File length ${fileAmplification.length}`);

let amplificationJson = mapAmplificationResult(fileAmplification)

let result = {
  summary: summaryJson,
  amplification: amplificationJson
}
fs.writeFile('result.json', JSON.stringify(result, null, 2), (err) => {
  console.log(err);
})