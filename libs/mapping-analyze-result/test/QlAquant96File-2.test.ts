import * as fs from 'fs'
import { readFilesQlAquant96 } from '../src/QlAquant96/index'
import { ResultToolI } from '../src/type/type'
/**
 * Test file in folrder example/QlAquant96/2
 */
const locateFileCt = 'example/QlAquant96/2/ct.csv'
const locateFileCurve = 'example/QlAquant96/2/curve.csv'

const fileCt = fs.readFileSync(locateFileCt)
const fileCurve = fs.readFileSync(locateFileCurve)

let res = readFilesQlAquant96(fileCt, fileCurve)

fs.writeFile('resultQlAquant-2.json', JSON.stringify(res, null, 2), () => {})

let debugResult = []
// print result from res
for (const id in res) {
  if (Object.prototype.hasOwnProperty.call(res, id)) {
    const element = res[id];
    debugResult.push(`${id} : ${element.result}`);
  }
}

console.log(debugResult)

it("Should have value", () => {
  expect(res).not.toBeNull()
})

const PositifWell = ["A6", "B6", "G6", "H8"]
const NeedVerifyWell = ["B8", "A8", "C8", "D8", "E8", "F8", "G8"]

const isControl = (val: any[]) =>{
  for(const resultCt of val){
    if(resultCt.sampleType.toLowerCase().includes("control")){
      return true
    }
  }
  return false
}

describe("Positif result must be correct", () => {
  for (const id in res) {
    if(!PositifWell.includes(id)) continue;
    if(isControl(res[id].ct)) continue;
    it(`Well ${id} should be positif`, () =>{
      expect(res[id].result).toBe(ResultToolI.POSITIF)
    })
  }
})

describe("Negatif result must be correct", () => {
  for (const id in res) {
    if([...PositifWell, ...NeedVerifyWell].includes(id)) continue;
    if(isControl(res[id].ct)) continue;
    it(`Well ${id} should be negatif`, () =>{
      expect(res[id].result).toBe(ResultToolI.NEGATIF)
    })
  }
})