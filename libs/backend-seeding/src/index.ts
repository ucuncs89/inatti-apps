import { RepositoryAction } from "./database/actions"
import { connectToDb } from "./database/connection"
import { SeedSystem } from './seed-system'

async function run() {
  const con = await connectToDb()
  const action = new RepositoryAction(con)
  await SeedSystem({
    action,
    nameFile: 'staging/state-staging.json'
  })
  con.close()
}

run()