import { Connection } from "typeorm";

export class RepositoryAction {
  constructor(private connection: Connection) { }

  runQuery(query: string) {
    return this.connection.query(query)
  }

  private columnName(columns: string[] | string) {
    if (Array.isArray(columns)) {
      return columns.map(c => `"${c}"`).join(', ')
    }
    return `"${columns}"`
  }

  cleanObject(data: Record<string, any>) {
    return Object.keys(data).reduce((acc, key) => {
      if (data[key]) {
        acc[key] = data[key]
      }
      return acc
    }, {})
  }

  async find(table: string, column: string, value: string) {
    return this.runQuery(`SELECT * FROM ${table} WHERE ${this.columnName(column)} = '${value}'`)
  }

  async insert(table: string, data: Record<string, any>) {
    data = this.cleanObject(data)
    const keys = Object.keys(data)
    const values = Object.values(data)
    const query = `INSERT INTO ${table} (${this.columnName(keys)}) VALUES (${values.map(v => `'${v}'`).join(', ')}) RETURNING *`
    return this.runQuery(query)
  }

  async update(table: string, column: string, value: string, data: Record<string, any>) {
    data = this.cleanObject(data)
    const keys = Object.keys(data)
    const values = Object.values(data)
    const query = `UPDATE ${table} SET ${keys.map((k, i) => `${this.columnName(k)} = '${values[i]}'`).join(', ')} WHERE ${column} = '${value}'`
    return this.runQuery(query)
  }
}