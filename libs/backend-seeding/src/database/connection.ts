import { createConnection, ConnectionOptions } from "typeorm"

export async function connectToDb() {
  try {
    const config: ConnectionOptions = {
      type: "postgres",
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      schema: process.env.DB_SCHEMA,
      synchronize: false
    }
    const con = await createConnection(config)
    console.log(`connected to database ${config.database} and schema ${config.schema}`)
    return con;
  } catch (err) {
    console.error(err)
  }
}