import chalk from 'chalk'
import { InterfaceSeed, SeedOption } from './type'
import { RepositoryAction } from './database/actions'


export async function SeedSystem({
  withOptional = false,
  action,
  nameFile
}: {
  withOptional?: boolean
  action: RepositoryAction,
  nameFile: string
}) {
  const log = {
    info: (message: string) => console.log(chalk.blue(message)),
    warn: (message: string) => console.log(chalk.yellow(message)),
    error: (message: string) => console.log(chalk.red(message)),
    success: (message: string) => console.log(chalk.green(message)),
    verbose: (message: string) => console.log(chalk.gray(message))
  }

  const dataSeed: InterfaceSeed = require(`../../../../libs/backend-seeding/seed/${nameFile}`)

  let count = 0
  //loop over object
  for (const key in dataSeed) {
    count++
    if (dataSeed.hasOwnProperty(key)) {
      const seed = dataSeed[key]
      log.info(`Seed ${count}: ${key}`)
      seed.comment && log.info(`Comment: ${seed.comment}`)
      // skip if optional
      if (!withOptional && seed.optional) {
        log.warn(`Skipping optional seed: ${key}`)
        continue
      }
      // skip if forEnv not match
      if (seed.forEnv && seed.forEnv !== process.env.ENV) {
        log.warn(`Skipping seed because only run for ${seed.forEnv}`)
        continue;
      }
      const items = seed.items
      log.info(`Processing ${items.data.length} items`)
      const logTransaction = {
        update: 0,
        insert: 0
      }
      for (const data of items.data) {
        const existData: any[] = await action.find(items.table, items.columnLookup, data[items.columnLookup])
        if (existData.length < 1) {
          await action.insert(items.table, data)
          logTransaction.insert++
          continue;
        }
        if (seed.forceUpdate) {
          await action.update(items.table, items.columnLookup, data[items.columnLookup], data)
          logTransaction.update++
        }
      }
      const total = items.data.length

      function printPercent(name: string, value: number) {
        const percent = Math.round((value / total) * 100)
        if (percent > 0) {
          return log.success(`${name}: ${value} (${percent}%)`)
        }
        return log.verbose(`${name}: ${value} (${percent}%)`)
      }

      printPercent('Insert', logTransaction.insert)
      printPercent('Update', logTransaction.update)
    }
  }
}