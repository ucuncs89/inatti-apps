export interface InterfaceSeed {
  [story: string]: SeedOption
}

export interface SeedOption {

  /** if true, it will skip by default */
  optional?: boolean;

  /** just a comment */
  comment?: string;

  /** if true, it will replace data */
  forceUpdate?: boolean;

  /** Only run for env selected */
  forEnv?: string;

  items: SeedItem
}

export interface SeedItem {
  /** Table name */
  table: string,

  /** Reference column to find in database make sure its unique, default is "id" */
  columnLookup?: string,

  /** data */
  data: Record<string, any>[]
}