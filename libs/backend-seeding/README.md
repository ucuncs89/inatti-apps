# backend-seeding

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test backend-seeding` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint backend-seeding` to execute the lint via [ESLint](https://eslint.org/).
