
export type DomisiliAddress = {
  [key: string]: any | string | number,
  address: string,
  provinsi: number,
  kota: number,
  kecamatan: number,
  kelurahan: number,
  rt: number,
  rw: number
}

export enum TujuanPemeriksaan {
  DiagnosisSuspek = 1,
  DiagnosisKontakErat = 2,
  Skrining = 3,
  FollowUp = 4,
}

export enum HasilPemeriksaan {
  Positif = "POSITIF",
  Negatif = "NEGATIF",
  Inkonklusif = "INKONKLUSIF",
  Invalid = "INVALID",
  DalamProses = "DALAM PROSES"
}

export type PersonPcrTest = {
  nik: string,
  phone: string,
  domisili?: DomisiliAddress,
  tanggalAmbilSpesimen: string,
  tujuanPemeriksaan: TujuanPemeriksaan,
  tanggalTerimaSpesimen: string,
  tanggalPeriksaLab: string,
  nomorSpesimen: string,
  tanggalHasilKeluar: string,
  hasilPemeriksaan: HasilPemeriksaan
}

export type typeSelectorElement<T> = { [key in keyof T]?: { selector: string, type: "text" | "select" } }

export interface ResponsePatientCheckNik {
  nik: string,
  name: string,
  address: string,
  ktp_address: string,
  rw: string,
  rt: string,
  nationality: string,
  gender: string,
  birthdate: string,
  province_id: string,
  city_id: string,
  district_id: string,
  is_nik_exist: string,
  id_litbang_new: string,
  next_url: string,
  village_id: string,
  sirs_online: string,
}

export interface ResponseCheckNik {
  result: boolean,
  message: string,
  data: ResponsePatientCheckNik
}

export class NarAddSampleExistingPatientDTO {
  id: string
  id_pemeriksaan: string
  jns_hal: string
  tgl_pengambilan: string
  tujuan: string
  tujuan_detail: string
  tgl_terima: string
  tgl_periksa: string
  tgl_hasil: string
  no_lab: string
  hsl_lab: string
  ct_value: string
  alamat_domisili: string
  alamat: string
  alamat_propkd: string
  alamat_kabkd: string
  alamat_keckd: string
  alamat_kelkd: string
  // default : 1
  sgtf = '1';
  // default : 1
  wgs = '1';
  // status perjalanan luar negeri
  sts_ppln = '1';
  sgtf_labtuju: string
  wgs_labtuju: string

}

export class NarAddSampleNewPatientDTO {
  nik: string;
  asal_data: string;
  jkel: string;
  jns_identitas: string;
  id: string;
  nama: string;
  tgl_lahir: string;
  telp: string;
  alamat_domisili_checked: string;
  alamat_domisili: string;
  alamat: string;
  propinsi: string;
  kabupaten: string;
  kecamatan: string;
  desa: string;
  rw: string;
  rt: string;
  tgl_pengambilan: string;
  tujuan: string;
  tujuan_detail: string;
  tgl_terima: string;
  no_lab: string;
  tgl_periksa: string;
  tgl_hasil: string;
  hsl_lab: string;

  // default : 5
  sess_status = '5';
  // default : null | ''
  ct_value =  '';
  // default : 1
  sgtf = '1';
  // default : 1
  wgs = '1';
  sts_ppln = '1';
  sgtf_labtuju: string
  wgs_labtuju: string
}