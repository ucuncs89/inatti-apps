import { NarPcr } from "./nar-pcr";
import { HasilPemeriksaan } from "./type";

async function run(){
  let narPcr = new NarPcr();
  try{
    narPcr.log = console.log
    // change this
    await narPcr.login('admin', 'admin')
    let res = await narPcr.AddResult({
      nik: '1408020109990003',
      asal_data: '1',
      jkel: 'L',
      jns_identitas: '1',
      id: '',
      sess_status: '5',
      nama: 'Rio Chandra',
      tgl_lahir: '04-09-1996',
      telp: '0822304756112',
      alamat_domisili_checked: 'on',
      alamat_domisili: 'Bandung',
      alamat: 'Riau',
      propinsi: '32',
      kabupaten: '3273',
      kecamatan: '327314',
      desa: '3273141002',
      rw: '4',
      rt: '3',
      tujuan: '4',
      tujuan_detail: '',
      no_lab: '123',
      tgl_pengambilan: '09-02-2022',
      tgl_terima: '09-02-2022',
      tgl_periksa: '09-02-2022',
      tgl_hasil: '09-02-2022',
      hsl_lab: HasilPemeriksaan.Negatif,
      ct_value: '',
      sgtf: "1",
      wgs: "1",
      sts_ppln: "1",
      sgtf_labtuju: "",
      wgs_labtuju: ""
    })
    console.log(res);
    
  } catch(err){
    console.log('Gagal proses NAR', err)
    console.log(err.message);    
  }
  
}

run();