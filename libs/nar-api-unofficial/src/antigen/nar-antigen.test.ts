import { HasilPemeriksaan } from ".."
import { NarAntigen } from "./nar-antigen"

async function runTest() {
  let narAntigen = new NarAntigen()
  narAntigen.log = console.log
  await narAntigen.setCookies(`[
    {
        "name": "_csrf",
        "value": "3c3e212a05bf48538300f732a176d5258a54025764ef23238d01d3092cb57dd2a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22paPvgnsU4pRL5b8dVspw9g-VV_2RbjIP%22%3B%7D",
        "domain": "allrecord-antigen.kemkes.go.id",
        "hostOnly": true,
        "path": "/",
        "secure": false,
        "httpOnly": true,
        "sameSite": "lax",
        "session": true,
        "firstPartyDomain": "",
        "partitionKey": null,
        "storeId": null
    },
    {
        "name": "PHPSESSID",
        "value": "ocut96mq2vaqocnq1tq1kdo6b2",
        "domain": "allrecord-antigen.kemkes.go.id",
        "hostOnly": true,
        "path": "/",
        "secure": false,
        "httpOnly": true,
        "sameSite": "lax",
        "session": true,
        "firstPartyDomain": "",
        "partitionKey": null,
        "storeId": null
    }
]`)

  let faskesId = await narAntigen.getFaskesId()

  console.log(`faskes id : ${faskesId}`);
  
  /**
  await narAntigen.registerPatient( { "id": "", "sess_status": "", "ct_value": "", "tujuan_detail": "1", "asal_data": "1", "tujuan": "3", "jns_identitas": "1", "nik": "1408020109990011", "jkel": "P", "nama": "Rio Chandra", "tgl_lahir": "09-09-1999", "telp": "82230475679", "alamat": "Bandung", "propinsi": "13", "kabupaten": "1302", "kecamatan": "130204", "desa": "1302042002", "rw": "12", "rt": "12", "alamat_domisili_checked": "on", "alamat_domisili": "Bandung", "no_lab": "A002387", "hsl_lab": "NEGATIF", "tgl_periksa": "18-01-2022" })
  
   */
  await narAntigen.registerPatient({
    nik: '1408020109990001',
    asal_data: '1',
    jkel: 'L',
    jns_identitas: '1',
    id: '',
    sess_status: '5',
    nama: 'Bukan Rio chandra',
    tgl_lahir: '04-09-1996',
    telp: '0822304756112',
    alamat_domisili_checked: 'on',
    alamat_domisili: 'JL.CICADAS PASAR II NO.38/142',
    alamat: 'JL.CICADAS PASAR II NO.38/142 ,RT 3/4 , Kel. CIKUTRA , Kec. CIBEUNYING KIDUL , KOTA BANDUNG , JAWA BARAT',
    propinsi: '32',
    kabupaten: '3273',
    kecamatan: '327314',
    desa: '3273141002',
    rw: '4',
    rt: '3',
    tujuan: '4',
    tujuan_detail: '',
    no_lab: '123',
    tgl_pengambilan: '13-12-2021',
    tgl_terima: '13-12-2021',
    tgl_periksa: '10-01-2021',
    tgl_hasil: '13-12-2021',
    hsl_lab: HasilPemeriksaan.Positif,
    ct_value: '',
  })


}

runTest()