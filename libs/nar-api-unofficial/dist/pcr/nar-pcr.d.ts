import { FormData } from 'formdata-node';
import { Got } from 'got';
import { CookieJar } from 'tough-cookie';
import { NarAddSampleExistingPatientDTO, NarAddSampleNewPatientDTO, ResponseCheckNik } from './type';
export declare class NarPcr {
    url: {
        base: string;
        login: string;
        saveSample: string;
        checkNik: string;
        addSample: string;
    };
    _got: Got;
    log: (...params: any[]) => void;
    responseAPICallback: (href?: string, body?: any) => any;
    constructor();
    isAuthenticated: boolean;
    canAccess(): boolean;
    getTitlePage(body: any): string | null;
    genFormData(data: any): FormData;
    _cookieJar: CookieJar;
    getCookie(): Promise<CookieJar>;
    saveCookie(cookie: any): Promise<void>;
    login(username: string, password: string): Promise<void>;
    /**
     * Function API to add result to existing patient
     */
    AddResultExistingPatient(payload: NarAddSampleExistingPatientDTO): Promise<void>;
    /**
     * Function API to add result to new patient
     */
    AddResultNewPatient(params: NarAddSampleNewPatientDTO): Promise<string>;
    readAlert(res: string): void;
    getDataNik(nik: string): Promise<ResponseCheckNik>;
    checkNik(nik: string): Promise<ResponseCheckNik>;
    AddResult(params: NarAddSampleNewPatientDTO): Promise<ResponseCheckNik | any>;
    successAddResult(params: string | ResponseCheckNik): Promise<ResponseCheckNik>;
}
export declare class NikRegisteredExeption extends Error {
    constructor(patient: ResponseCheckNik);
    patient: ResponseCheckNik;
    name: string;
    message: string;
}
export declare class NikNotFoundExeption extends Error {
    name: string;
    message: string;
}
export declare class NotAuthenticatedExeption extends Error {
    name: string;
    message: string;
}
