"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotAuthenticatedExeption = exports.NikNotFoundExeption = exports.NikRegisteredExeption = exports.NarPcr = void 0;
const formdata_node_1 = require("formdata-node");
const got_1 = require("got");
const tough_cookie_1 = require("tough-cookie");
const node_html_parser_1 = require("node-html-parser");
class NarPcr {
    constructor() {
        this.url = {
            base: 'https://allrecord-tc19.kemkes.go.id',
            login: `Index.rpd`,
            saveSample: 'Pasien/SaveSampel',
            checkNik: `Pasien/nik`,
            addSample: 'Pasien/tambah_save/1'
        };
        this.log = (...params) => { };
        this.responseAPICallback = (href, body) => { };
        this.isAuthenticated = false;
        this._cookieJar = new tough_cookie_1.CookieJar();
        this._got = got_1.default.extend({
            prefixUrl: this.url.base,
            hooks: {
                beforeRequest: [
                    (options) => {
                        this.log(`Request to : ${options.url.href}`);
                    }
                ],
                afterResponse: [
                    (response, _) => {
                        if (response.body) {
                            const title = this.getTitlePage(response.body);
                            if (title && title.includes('Halaman Login')) {
                                response.statusCode = 304;
                                response.body = 'No authentication';
                                throw new NotAuthenticatedExeption();
                            }
                            this.responseAPICallback(response.url, response.body);
                        }
                        return response;
                    }
                ]
            },
        });
    }
    canAccess() {
        return this.isAuthenticated;
    }
    getTitlePage(body) {
        let matched = body.match(/(?:<title>)(.*)(?:<\/title>)/);
        if (matched && matched.length > 0) {
            return matched[1];
        }
        return null;
    }
    genFormData(data) {
        const formData = new formdata_node_1.FormData();
        for (let key in data) {
            formData.set(key, data[key]);
        }
        return formData;
    }
    async getCookie() {
        await this._cookieJar.getCookies(this.url.base);
        return this._cookieJar;
    }
    async saveCookie(cookie) {
        this.log(`run save cookie with payload : ${cookie}, ${Array.isArray(cookie)}`);
        if (!cookie)
            return;
        if (Array.isArray(cookie)) {
            cookie = cookie[0];
        }
        await this._cookieJar.setCookie(cookie, this.url.base);
    }
    async login(username, password) {
        try {
            let res = await this._got.post(this.url.login, {
                form: {
                    user: username,
                    log_in: 'true',
                    pass: password,
                },
                followRedirect: false
            });
            await this.saveCookie(res.headers['set-cookie']);
            this.log('success Login');
            this.isAuthenticated = true;
        }
        catch (err) {
            this.isAuthenticated = false;
            throw new NotAuthenticatedExeption();
        }
    }
    /**
     * Function API to add result to existing patient
     */
    async AddResultExistingPatient(payload) {
        this.log('run save sample');
        try {
            const formData = this.genFormData(payload);
            let res = await this._got.post(this.url.saveSample, {
                form: formData,
                cookieJar: await this.getCookie(),
                followRedirect: true,
                hooks: {
                    beforeRedirect: [
                        (options, response) => {
                            this.log(`redirect to ${options.path}`);
                        }
                    ]
                }
            }).text();
            this.readAlert(res);
        }
        catch (err) {
            this.log(`gagal save sample`);
            this.log(err);
            throw err;
        }
    }
    /**
     * Function API to add result to new patient
     */
    async AddResultNewPatient(params) {
        const payload = {
            ...params,
            sess_status: '5',
            ct_value: ''
        };
        try {
            const formData = this.genFormData(payload);
            const res = await this._got.post(this.url.addSample, {
                form: formData,
                cookieJar: await this.getCookie()
            }).text();
            this.readAlert(res);
            return res;
        }
        catch (err) {
            throw err;
        }
    }
    readAlert(res) {
        // search tag 'alert alert-danger' in string res
        const root = (0, node_html_parser_1.parse)(res);
        const alert = root.querySelector('.alert.alert-danger');
        if (alert) {
            this.log(`alert: ${alert.toString()}`);
            throw new Error(alert.text);
        }
    }
    async getDataNik(nik) {
        if (nik.length !== 16) {
            throw new Error('Panjang nik tidak 16 digit');
        }
        return this._got.get(this.url.checkNik, {
            searchParams: {
                nik,
            },
            cookieJar: await this.getCookie()
        }).json();
    }
    async checkNik(nik) {
        const res = await this.getDataNik(nik);
        this.log(res);
        if (res.message.includes('Data Tidak Ditemukan')) {
            throw new NikNotFoundExeption();
        }
        if (res.message.includes('NIK sudah Terdaftar')) {
            throw new NikRegisteredExeption(res);
        }
        return res;
    }
    async AddResult(params) {
        try {
            await this.checkNik(params.nik);
            await this.AddResultNewPatient(params);
            this.log('Berhasil buat hasil baru');
            this.log(`Ambil data nik ${params.nik} lagi`);
            return this.successAddResult(params.nik);
        }
        catch (err) {
            if (err instanceof NikRegisteredExeption) {
                const { data } = err.patient;
                await this.AddResultExistingPatient({
                    id: data.id_litbang_new,
                    id_pemeriksaan: '',
                    jns_hal: '1',
                    tgl_pengambilan: params.tgl_pengambilan,
                    tujuan: params.tujuan,
                    tujuan_detail: params.tujuan_detail,
                    tgl_terima: params.tgl_terima,
                    tgl_periksa: params.tgl_periksa,
                    tgl_hasil: params.tgl_hasil,
                    no_lab: params.no_lab,
                    hsl_lab: params.hsl_lab,
                    ct_value: params.ct_value,
                    sgtf: params.sgtf,
                    wgs: params.wgs,
                    alamat_domisili: params.alamat_domisili,
                    alamat: params.alamat,
                    alamat_propkd: params.propinsi,
                    alamat_kabkd: params.kabupaten,
                    alamat_keckd: params.kecamatan,
                    alamat_kelkd: params.desa,
                    sts_ppln: params.sts_ppln,
                    sgtf_labtuju: params.sgtf_labtuju,
                    wgs_labtuju: params.wgs_labtuju
                });
                this.log('Berhasil simpan data yg sudah ada.');
                this.log(`Periksa data : ${data.next_url}`);
                return this.successAddResult(err.patient);
            }
            if (err instanceof NikNotFoundExeption) {
                await this.AddResultNewPatient(params);
                return await this.successAddResult(params.nik);
            }
            throw err;
        }
    }
    async successAddResult(params) {
        if (typeof params === 'object') {
            return params;
        }
        return await this.getDataNik(params);
    }
}
exports.NarPcr = NarPcr;
class NikRegisteredExeption extends Error {
    constructor(patient) {
        super();
        this.name = 'NikRegisteredExeption';
        this.message = 'Nik Sudah terdaftar';
        this.patient = patient;
    }
}
exports.NikRegisteredExeption = NikRegisteredExeption;
class NikNotFoundExeption extends Error {
    constructor() {
        super(...arguments);
        this.name = 'NikNotFoundExeption';
        this.message = 'Nik tidak ditemukan';
    }
}
exports.NikNotFoundExeption = NikNotFoundExeption;
class NotAuthenticatedExeption extends Error {
    constructor() {
        super(...arguments);
        this.name = 'NotAuthenticatedExeption';
        this.message = 'Tidak terauthentikasi, kemungkinan username/password salah';
    }
}
exports.NotAuthenticatedExeption = NotAuthenticatedExeption;
