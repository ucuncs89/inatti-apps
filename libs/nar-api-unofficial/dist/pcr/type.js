"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NarAddSampleNewPatientDTO = exports.NarAddSampleExistingPatientDTO = exports.HasilPemeriksaan = exports.TujuanPemeriksaan = void 0;
var TujuanPemeriksaan;
(function (TujuanPemeriksaan) {
    TujuanPemeriksaan[TujuanPemeriksaan["DiagnosisSuspek"] = 1] = "DiagnosisSuspek";
    TujuanPemeriksaan[TujuanPemeriksaan["DiagnosisKontakErat"] = 2] = "DiagnosisKontakErat";
    TujuanPemeriksaan[TujuanPemeriksaan["Skrining"] = 3] = "Skrining";
    TujuanPemeriksaan[TujuanPemeriksaan["FollowUp"] = 4] = "FollowUp";
})(TujuanPemeriksaan = exports.TujuanPemeriksaan || (exports.TujuanPemeriksaan = {}));
var HasilPemeriksaan;
(function (HasilPemeriksaan) {
    HasilPemeriksaan["Positif"] = "POSITIF";
    HasilPemeriksaan["Negatif"] = "NEGATIF";
    HasilPemeriksaan["Inkonklusif"] = "INKONKLUSIF";
    HasilPemeriksaan["Invalid"] = "INVALID";
    HasilPemeriksaan["DalamProses"] = "DALAM PROSES";
})(HasilPemeriksaan = exports.HasilPemeriksaan || (exports.HasilPemeriksaan = {}));
class NarAddSampleExistingPatientDTO {
    constructor() {
        // default : 1
        this.sgtf = '1';
        // default : 1
        this.wgs = '1';
        // status perjalanan luar negeri
        this.sts_ppln = '1';
    }
}
exports.NarAddSampleExistingPatientDTO = NarAddSampleExistingPatientDTO;
class NarAddSampleNewPatientDTO {
    constructor() {
        // default : 5
        this.sess_status = '5';
        // default : null | ''
        this.ct_value = '';
        // default : 1
        this.sgtf = '1';
        // default : 1
        this.wgs = '1';
        this.sts_ppln = '1';
    }
}
exports.NarAddSampleNewPatientDTO = NarAddSampleNewPatientDTO;
