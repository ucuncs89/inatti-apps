import { CookieJar } from "tough-cookie";
import { NikData, PatientDTO } from ".";
export declare class NarAntigen {
    constructor();
    faskesId: any;
    getFaskesId(): Promise<any>;
    registerPatient(patient: PatientDTO): Promise<string>;
    log: (message: any, type?: 'error' | 'success' | 'warning') => void;
    url: {
        base: string;
        checkNik: string;
        createByNik: string;
    };
    private _got;
    private _cookies;
    setCookies(cookies: string): Promise<void>;
    getCookies(): Promise<CookieJar>;
    getCsrf(url: any): Promise<any>;
    tempCsrf: string;
    checkNik(nik: string): Promise<NikData | null>;
    addManual(patient: PatientDTO): Promise<string>;
    addExistingPatient(patient: PatientDTO, nikData: NikData): Promise<string>;
    isFailedPage(res: string, filterTitle?: string): void;
    callbackResultPage(result: string, url: string): void;
}
