"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NarAntigen = void 0;
const source_1 = require("got/dist/source");
const tough_cookie_1 = require("tough-cookie");
const kemenkes_go_id_chain_pem_1 = require("./kemenkes-go-id-chain.pem");
const node_html_parser_1 = require("node-html-parser");
const toBase64 = (str) => Buffer.from(str, 'utf-8').toString('base64');
class NarAntigen {
    constructor() {
        this.faskesId = null;
        this.log = (message, type) => { console.log(message); };
        this.url = {
            base: 'https://allrecord-antigen.kemkes.go.id',
            checkNik: 'web/data-pasien/cek-nik',
            createByNik: 'web/data-pasien/create-by-nik'
        };
        this._cookies = new tough_cookie_1.CookieJar();
        this.tempCsrf = '';
        this._got = source_1.default.extend({
            prefixUrl: this.url.base,
            https: {
                certificateAuthority: kemenkes_go_id_chain_pem_1.CertPem
            },
            hooks: {
                beforeRequest: [
                    (options) => {
                        // this.log(options.url.href)
                    }
                ],
            }
        });
    }
    async getFaskesId() {
        const url = 'web/data-pasien/create-manual';
        let result = await this._got.get(url, {
            cookieJar: await this.getCookies()
        }).text();
        let tagIdFaskes = result.match(/<[^<]*name="DataPasienModel\[id_faskes\]"[^>]*>/gim);
        if (!tagIdFaskes || tagIdFaskes.length < 1) {
            throw "Faskes id not found";
        }
        let value = tagIdFaskes[0].match(/(value=)"([^"]*)"/);
        if (!value || value.length < 3) {
            throw "Faskes id not found";
        }
        this.faskesId = value[2];
        return this.faskesId;
    }
    async registerPatient(patient) {
        try {
            this.log(`Registering patient ${patient.nik}`);
            this.tempCsrf = await this.getCsrf(this.url.createByNik);
            let nikData = await this.checkNik(patient.nik);
            if (!nikData) {
                return await this.addManual(patient);
            }
            return await this.addExistingPatient(patient, nikData);
        }
        catch (err) {
            throw err;
        }
    }
    async setCookies(cookies) {
        let cookiesParse = JSON.parse(cookies);
        let arrayCookie = [];
        for (const cookie of cookiesParse) {
            await this._cookies.setCookie(`${cookie.name}=${cookie.value}`, this.url.base);
        }
    }
    async getCookies() {
        await this._cookies.getCookies(this.url.base);
        return this._cookies;
    }
    async getCsrf(url) {
        let res = await this._got.get(url, {
            cookieJar: await this.getCookies(),
            followRedirect: false,
        }).text();
        let lines = res.split("\n");
        let csrfToken = null;
        for (const line of lines) {
            let props = line.match(/(?<=name="csrf-token" +content=")[^"><]*/g);
            if (props && !!props[0]) {
                csrfToken = props[0];
                return csrfToken;
            }
        }
    }
    async checkNik(nik) {
        this.log(`Periksa nik ${nik}`);
        let res = await this._got.post(this.url.checkNik, {
            cookieJar: await this.getCookies(),
            form: {
                _csrf: this.tempCsrf,
                jenis_identitas: "KTP",
                nomor_identitas: nik,
            },
            followRedirect: false
        }).json();
        if (res.sukses === 1) {
            this.log(`${nik} Tidak ditemukan`, 'warning');
            return null;
        }
        this.log(`${nik} ditemukan !`);
        return res;
    }
    async addManual(patient) {
        this.log(`Persiapkan data untuk tambah manual`);
        const gejala = patient.hsl_lab === 'POSITIF' ? 'Bergejala+Covid+19' : "Tidak+Bergejala";
        const kesimpulan = patient.hsl_lab === 'POSITIF' ? 'Dianggap+Memiliki+COVID-19' : "Dilanjutkan+Swab+PCR";
        const jkel = patient.jkel === 'L' ? 'Laki-Laki' : 'Perempuan';
        const url = 'web/data-pasien/create-manual';
        const params = {
            '_csrf': await this.getCsrf(url),
            'DataPasienModel[id_faskes]': this.faskesId,
            'DataPasienModel[jenis_identitas]': 'KTP',
            'DataPasienModel[nomor_identitas]': patient.nik,
            'DataPasienModel[nama]': patient.nama,
            'DataPasienModel[tgl_lahir]': patient.tgl_lahir,
            'DataPasienModel[kd_pasport]': '',
            'DataPasienModel[jkel]': jkel,
            'DataPasienModel[phone]': patient.telp,
            'DataPasienModel[alamat_domisili]': patient.alamat_domisili,
            'DataPasienModel[alamat_identitas]': patient.alamat,
            'DataPasienModel[id_prov]': patient.propinsi,
            'DataPasienModel[id_kab]': patient.kabupaten,
            'DataPasienModel[id_kec]': patient.kecamatan,
            'DataPasienModel[id_desa]': patient.desa,
            'DataPasienModel[rt]': patient.rt,
            'DataPasienModel[rw]': patient.rw,
            'PemeriksaanModel[kriteria_kab]': 'A',
            'PemeriksaanModel[status_gejala]': gejala,
            'PemeriksaanModel[tujuan_pemeriksaan]': '3',
            'PemeriksaanModel[tujuan_pemeriksaan_detail]': '1',
            'PemeriksaanModel[nomor_spesimen]': patient.no_lab,
            'PemeriksaanModel[tgl_periksa]': patient.tgl_periksa,
            'PemeriksaanModel[status_pembiayaan]': 'Berbayar',
            'PemeriksaanModel[hasil_periksa]': patient.hsl_lab,
            'PemeriksaanModel[id_jenis]': '1',
            'PemeriksaanModel[kesimpulan]': kesimpulan,
            'submit1': 'submit_1',
        };
        try {
            let result = await this._got.post(url, {
                cookieJar: await this.getCookies(),
                form: params,
                followRedirect: true
            }).text();
            this.callbackResultPage(result, url);
            this.isFailedPage(result, 'Tambah Data Pasien');
            this.log(`Berhasil simpan pasien ${patient.nik} ke NAR dengan hasil ${patient.hsl_lab}`);
            return result;
        }
        catch (err) {
            let reason = err.messsage ?? err.toString();
            this.log(`Failed, Reason : ${reason}`, 'error');
            throw err;
        }
    }
    async addExistingPatient(patient, nikData) {
        this.log(`Menyiapkan data untuk simpan ke NAR, id pasien : ${nikData.id_pasien}`);
        const idPatient = toBase64(nikData.id_pasien.toString());
        const url = `web/pemeriksaan/create?id_pasien=` + idPatient;
        const gejala = patient.hsl_lab === 'POSITIF' ? 'Bergejala+Covid+19' : "Tidak+Bergejala";
        const kesimpulan = patient.hsl_lab === 'POSITIF' ? 'Dianggap+Memiliki+COVID-19' : "Dilanjutkan+Swab+PCR";
        const params = {
            _csrf: null,
            'PemeriksaanModel[created_date]': '2022-01-18+11:12:06',
            'PemeriksaanModel[id_pasien]': nikData.id_pasien,
            'PemeriksaanModel[id_faskes]': this.faskesId,
            'PemeriksaanModel[kriteria_kab]': 'A',
            'PemeriksaanModel[status_gejala]': gejala,
            'PemeriksaanModel[tujuan_pemeriksaan]': "3",
            'PemeriksaanModel[tujuan_pemeriksaan_detail]': "1",
            'PemeriksaanModel[id_jenis]': "1",
            'PemeriksaanModel[nomor_spesimen]': patient.no_lab,
            'PemeriksaanModel[tgl_periksa]': patient.tgl_periksa,
            'PemeriksaanModel[hasil_periksa]': patient.hsl_lab,
            'PemeriksaanModel[status_pembiayaan]': 'Berbayar',
            'PemeriksaanModel[kesimpulan]': kesimpulan,
        };
        params._csrf = await this.getCsrf(url);
        try {
            let result = await this._got.post(url, {
                cookieJar: await this.getCookies(),
                form: params,
                followRedirect: true
            }).text();
            this.callbackResultPage(result, url);
            this.isFailedPage(result, "Tambah Pengambilan Sampel Pasien");
            this.log(`Berhasil simpan pasien ${patient.nik} ke NAR dengan hasil ${patient.hsl_lab}`);
            return result;
        }
        catch (err) {
            let reason = err.messsage ?? err.toString();
            this.log(`Failed, Reason : ${reason}`, 'error');
            throw err;
        }
    }
    isFailedPage(res, filterTitle) {
        // search tag 'alert alert-danger' in string res
        const root = (0, node_html_parser_1.default)(res);
        if (root.querySelector('.alert.alert-danger')) {
            const alert = root.querySelector('.alert.alert-danger');
            this.log(`alert: ${alert.text}`);
            throw new Error(alert.text);
        }
        if (root.querySelector('title') && filterTitle) {
            let title = root.querySelector('title').text;
            // In exception title, fail it, because the page not moving on other page
            if (title.includes(filterTitle)) {
                throw new Error(`Request still on same page`);
            }
        }
    }
    // overridable
    callbackResultPage(result, url) { }
}
exports.NarAntigen = NarAntigen;
