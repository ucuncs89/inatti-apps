import {
  FeatureTelefarmasiDatabasePmrList,
  FeatureTelefarmasiDatabasePatientDetail,
  FeatureTelefarmasiApotekMedicineInventoryList,
  FeatureTelefarmasiDatabasePmrDetail,
  FeatureTelefarmasiDatabasePatientForm,
  FeatureTelefarmasiDatabasePatientLogActivity,
  FeatureTelefarmasiDatabasePatientMedicalRecordPillForm,
  FeatureTelefarmasiDatabasePatientOwnedByDetail,
  FeatureTelefarmasiDatabasePatientOwnedByForm,
  FeatureTelefarmasiDatabasePatientProofImageDropzonePreview,
  FeatureTelefarmasiDatabasePatientProofImageDropzoneUpload,
  FeatureTelefarmasiDatabasePatientFormPatientAccount,
  FeatureTelefarmasiDatabasePatientDataSourceDetail,
  FeatureTelefarmasiApotekMedicineInventoryUpload,
  FeatureTelefarmasiDatabasePatientList,
} from '../index';

import Vue from 'vue'

Vue.component('feature-telefarmasi-database-pmr-list', FeatureTelefarmasiDatabasePmrList);
Vue.component(
  'feature-telefarmasi-database-patient-detail',
  FeatureTelefarmasiDatabasePatientDetail
);
Vue.component(
  'feature-telefarmasi-database-pmr-detail',
  FeatureTelefarmasiDatabasePmrDetail
);
Vue.component(
  'feature-telefarmasi-database-patient-form',
  FeatureTelefarmasiDatabasePatientForm
);
Vue.component(
  'feature-telefarmasi-database-patient-list',
  FeatureTelefarmasiDatabasePatientList
);
Vue.component(
  'feature-telefarmasi-apotek-medicine-inventory-list',
  FeatureTelefarmasiApotekMedicineInventoryList
);
Vue.component(
  'feature-telefarmasi-database-patient-log-activity',
  FeatureTelefarmasiDatabasePatientLogActivity
);

Vue.component(
  'feature-telefarmasi-database-patient-medical-record-pill-form',
  FeatureTelefarmasiDatabasePatientMedicalRecordPillForm
);
Vue.component(
  'feature-telefarmasi-database-patient-owned-by-detail',
  FeatureTelefarmasiDatabasePatientOwnedByDetail
);
Vue.component(
  'feature-telefarmasi-database-patient-owned-by-form',
  FeatureTelefarmasiDatabasePatientOwnedByForm
);
Vue.component(
  'feature-telefarmasi-database-patient-proof-image-dropzone-preview',
  FeatureTelefarmasiDatabasePatientProofImageDropzonePreview
);
Vue.component(
  'feature-telefarmasi-database-patient-proof-image-dropzone-upload',
  FeatureTelefarmasiDatabasePatientProofImageDropzoneUpload
);
Vue.component(
  'feature-telefarmasi-database-patient-form-patient-account',
  FeatureTelefarmasiDatabasePatientFormPatientAccount
);
Vue.component(
  'feature-telefarmasi-database-patient-data-source-detail',
  FeatureTelefarmasiDatabasePatientDataSourceDetail
);

Vue.component(
  'feature-telefarmasi-apotek-medicine-inventory-upload',
  FeatureTelefarmasiApotekMedicineInventoryUpload
);
