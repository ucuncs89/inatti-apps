# features-telefarmasi

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build features-telefarmasi` to build the library.

## Running unit tests

Run `nx test features-telefarmasi` to execute the unit tests via [Jest](https://jestjs.io).
