import { PaginationOptionsInterface } from './pagination.options.interface';
import { PaginationResultInterface } from './pagination.results.interface';

export class Pagination<PaginationEntity> {
  public results: PaginationEntity[];
  public pageTotal: number; // total per page
  public page: number; // current page
  public total: number;
  public pageLength: number; //total page

  constructor(paginationResults: PaginationResultInterface<PaginationEntity>, paginateOption: PaginationOptionsInterface) {
    this.results = paginationResults.results;
    this.pageTotal = paginationResults.results.length;
    this.total = paginationResults.total;
    this.page = paginateOption.page
    this.pageLength = Math.floor(this.total / paginateOption.limit) + (this.total % paginateOption.limit > 0 ? 1 : 0)
  }
}

export const NullDataPagination = (paginate: PaginationOptionsInterface) => new Pagination({
  results: [],
  total: 0
}, paginate)
