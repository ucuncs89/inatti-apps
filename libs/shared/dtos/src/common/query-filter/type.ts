import { ApiProperty } from "@nestjs/swagger"

export class CommonFilterDTO {
  @ApiProperty({ required: false, name: 'filter[date]', type: 'string', example: 'YYYY-MM-DD' })
  date: any

  @ApiProperty({ required: false, name: 'filter[rangedate]', type: 'string', example: '{start: \'YYYY-MM-DD\', end: \'YYYY-MM-DD\'}' })
  rangedate: any

  @ApiProperty({ required: false, name: 'filter[search]', type: 'string' })
  search: string
}