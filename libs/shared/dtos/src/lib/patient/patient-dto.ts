import { BloodTypeEnum, GenderEnum, NikTypeEnum } from "../swab-data/swab-data.dto";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class PatientDTO {
  @ApiProperty({ description: 'if filled, other field ignored' })
  id?: string;

  @ApiProperty()
  nik: string;

  @ApiProperty()
  passport: string;

  @ApiProperty({ enum: NikTypeEnum })
  type: NikTypeEnum;

  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @ApiProperty()
  placebirth: string;

  @ApiProperty()
  datebirth: Date | string;

  @ApiProperty({ enum: GenderEnum })
  gender: GenderEnum;

  @ApiProperty()
  phonenumber?: string;

  @ApiProperty()
  email?: string;

  @ApiProperty({ enum: BloodTypeEnum })
  bloodtype?: BloodTypeEnum;

  @ApiProperty({ description: "Get id from /wilayah/provinsi" })
  provinsi: number;

  @ApiProperty({ description: "Get id from /wilayah/kabupaten" })
  kabupaten: number;

  @ApiProperty({ description: "Get id from /wilayah/kecamatan" })
  kecamatan: number;

  @ApiProperty({ description: "Get id from /wilayah/kelurahan" })
  kelurahan: string;

  @ApiProperty()
  kelurahanRt?: string;

  @ApiProperty()
  kelurahanRw?: string;

  @ApiProperty()
  domisiliProvinsi: number;

  @ApiProperty()
  domisiliKabupaten: number;

  @ApiProperty()
  domisiliKecamatan: number;

  @ApiProperty()
  domisiliKelurahan: string;

  @ApiProperty()
  domisiliKelurahanRt?: string;

  @ApiProperty()
  domisiliKelurahanRw?: string;

  @ApiProperty()
  address: string;

  @ApiProperty()
  addressDomisili?: string;

  @ApiProperty()
  citizenship: string;

  @ApiProperty()
  narOrangId: string;

  @ApiProperty()
  height: string

  @ApiProperty()
  weight: string

  @ApiProperty()
  job: string

  @ApiProperty()
  etc?: any

  ownedBy?: string
}

export class QuerySearchPatient {
  @ApiProperty()
  query: string
  @ApiProperty()
  id: string;
}
