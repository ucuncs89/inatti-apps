import { ApiProperty } from "@nestjs/swagger"
import { IsDefined, IsNotEmpty, IsNotEmptyObject, IsObject, ValidateNested } from "class-validator"
import { Type } from 'class-transformer';
import { CommonFilterDTO } from "../../common/query-filter/type";

export class MedicineDTO {
  @ApiProperty({ required: false, description: "If this null, will create new data" })
  id?: string
  @ApiProperty({ required: false })
  name?: string
}

/** This class is 'ok' to modify */
export class MedicinePmrNotes {
  @ApiProperty()
  signa: string

  @ApiProperty()
  caraPakai: string

  @ApiProperty()
  aturanPakai: string
}

export class MedicinePmrDTO {
  @ApiProperty()
  medicine: MedicineDTO
  @ApiProperty()
  amount: number
  @ApiProperty()
  amountUnit: string
  @ApiProperty({
    type: () => MedicinePmrNotes
  })
  notes: MedicinePmrNotes
}

export class PmrCreateDTO {
  @IsNotEmpty()
  @ApiProperty()
  patient: string

  @IsNotEmpty()
  @ApiProperty()
  case: string

  @IsNotEmpty()
  @ApiProperty()
  docterName: string

  @ApiProperty({ required: false })
  note: string

  @ApiProperty({ required: false })
  attachment?: string

  @ApiProperty({
    type: () => MedicinePmrDTO,
    isArray: true
  })
  medicines: MedicinePmrDTO[]

  @ApiProperty({
    required: false,
    example: "['image1.jpg', 'image2.jpg']"
  })
  proofConsultation?: string[]
}

export class PmrDataCreateDTO {
  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => PmrCreateDTO)
  @ApiProperty()
  pmrData: PmrCreateDTO

  @IsNotEmpty()
  @ApiProperty()
  apotekId: string
}

export class PmrDataUpdateDTO extends PmrDataCreateDTO { }

export class PmrMedicineCreateDTO extends MedicinePmrDTO { }
export class PmrMedicineUpdateDTO extends MedicinePmrDTO {
  @ApiProperty()
  id: number
}

export class FilterCommonTelemedicineList extends CommonFilterDTO {
  @ApiProperty({ required: false, name: 'filter[apotek]' })
  apotek?: string[]

  @ApiProperty({ required: false, name: 'filter[apotekGroup]' })
  apotekGroup?: string[]

  headOffice?: string
}

export class FilterPatientMedicine extends FilterCommonTelemedicineList {
  @ApiProperty({ required: false, name: 'filter[etc]' })
  etc?: any
  @ApiProperty({ required: false, name: 'filter[forceFilterApotek]' })
  forceFilterApotek?: string
}
