import { ApiProperty } from "@nestjs/swagger";

export class LoginTenancyDTO {
  @ApiProperty()
  roleId: string
}