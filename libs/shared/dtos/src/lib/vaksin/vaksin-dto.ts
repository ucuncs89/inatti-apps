import { ApiProperty } from "@nestjs/swagger";
import { PatientDTO } from "../patient/patient-dto";
import { OrderTrxInfo, TransactionCreateDTO } from "../transaction/transaction-dto";

export class VaksinInfo extends OrderTrxInfo { }

export class VaksinCreateOrderByUserDTO extends TransactionCreateDTO {
  @ApiProperty({ type: PatientDTO, isArray: true })
  patients: PatientDTO[];
}

