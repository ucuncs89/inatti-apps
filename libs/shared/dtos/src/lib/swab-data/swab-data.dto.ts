export enum NikTypeEnum {
  nik = "nik",
  passport = "passport",
}

export enum GenderEnum {
  male = "lk",
  female = "pr"
}

export enum BloodTypeEnum {
  A = "A",
  B = "B",
  AB = "AB",
  O = "O"
}

export enum StatusSwabEnum {
  registered = "registered",
  checkin = "checkin",
  done = "doneSwab",
  onLab = "onLab",
  progressLab = "progressLab",
  resultOutLab = "resultOutLab",
  resultOutRef = "resultOutRef",
}

export enum ResultSwabEnum {
  positif = "positif",
  negatif = "negatif",
  presumtifPositif = "presumtifPositif",
  presumtifNegatif = "presumtifNegatif",

}

export enum StatusOrderEnum {
  new = "new",
  paymentReceived = "paymentReceived",
  paymentFailed = "paymentFailed",
  inProgress = "inProgress",
  paid = "paid",
  close = "close",
  canceled = "canceled",
}

export enum MethodPaymentEnum {
  cash = "Cash",
  transfer = "Transfer"
}