import { MethodPaymentEnum } from "../swab-data/swab-data.dto"
import { ApiProperty } from "@nestjs/swagger";
import { VaksinInfo } from "../vaksin/vaksin-dto";
import { PatientDTO } from "../patient/patient-dto";

export class RefDTO {
  @ApiProperty()
  refId: string;

  @ApiProperty()
  productId: string;
}

export class PaymentChannelDTO {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;
}

export class OrderTrxInfo {
  @ApiProperty({
    description: 'Format : MM-DD-YYYY',
  })
  date: Date;

  @ApiProperty({
    example: '0',
    description: 'id test: PCR = 0, antigen = 1',
  })
  type: number; // id product
}


export class TransactionCreateDTO {
  @ApiProperty({ type: () => OrderTrxInfo })
  info: OrderTrxInfo;

  @ApiProperty()
  refTarget: RefDTO;

  @ApiProperty({
    enum: MethodPaymentEnum,
  })
  method?: MethodPaymentEnum;

  @ApiProperty({
    description: 'If method = transfer, fill this',
  })
  paymentChannel?: PaymentChannelDTO;
}

export class TransactionCreateOrderByUserDTO extends TransactionCreateDTO {
  @ApiProperty({ type: PatientDTO, isArray: true })
  patients: PatientDTO[];
}