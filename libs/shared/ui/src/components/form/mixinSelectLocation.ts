import Vue from 'vue';
import { Prop, Watch } from 'vue-property-decorator';
import Component from 'vue-class-component';
import { VSelect } from 'vuetify/lib';

@Component
export class MixinSelectLocation extends VSelect {
  @Prop({ type: [Number, String, Object] })
  value!: string | Number | Object;

  get selected(): any {
    return this.value;
  }
  set selected(val) {
    this.$emit('input', val);
  }

  listItems: any[] = [];
  isLoading: boolean = false;

  resetValue() {
    this.$emit('input', null)
    this.selected = null
  }

  isInListItems(selected: any) {
    if (!this.listItems) return false;
    if (this.listItems.length < 1) return false;
    return this.listItems.findIndex((val) => {
      let selectedId = selected
      let itemId = val
      if (typeof selectedId === 'object') {
        selectedId = selectedId.id
      }
      if (typeof itemId === 'object') {
        itemId = itemId.id
      }
      return itemId === selectedId
    }) > -1
  }

  checkValueValidityOnChange(value: any) {
    if (!value) return;
    if (!this.isInListItems(value)) {
      this.resetValue()
      return;
    }
    this.$emit('input', this.selected);
  }
}
export default MixinSelectLocation;
