const path = require('path');
const { resolve } = require('path');

/** For now, no config, maybe later */
const defaultOptions = {};

const assignPlugins = (plugins, configOptions, _this) => {
  /** Add all plugin files to nuxt using this.addPlugin() */
  for (let i = 0; i < plugins.length; i++) {
    let plugin = plugins[i];
    if (typeof plugin === 'string') {
      plugin = {
        src: plugin,
      };
    }
    _this.addPlugin({
      src: resolve(__dirname, './plugins/', plugin.src),
      mode: plugin.mode,
      options: configOptions,
    });
  }
};

const assignPluginGeneric = (plugins, config, _this) => {
  if (!_this.options.plugins) {
    _this.options.plugins = [];
  }
  for (let i = 0; i < plugins.length; i++) {
    const plugin = plugins[i];
    _this.options.plugins.push(path.resolve(__dirname, plugin));
  }
};

module.exports = function UiModule() {
  const configOptions = Object.assign(
    {},
    defaultOptions,
    this.options.uiConfig
  );
  // assign manual axios.js to plugins nuxt.
  // if it works, why not
  assignPluginGeneric(['./plugins/axios.js'], configOptions, this);

  // assign all plugin to nuxt before build
  let listPlugin = {
    'build:before': [
      'snackbar.ts',
      'utils.js',
      'components.js',
      'helpers.ts',
      'form.ts',
      'allow-permission.js',
      { src: 'vuex-persist.js', mode: 'client' },
      {
        src: 'excelJSExport.ts',
        mode: 'client',
      },
      {
        mode: 'client',
        src: 'printPdf.ts',
      },
      {
        src: 'vue-barcode-reader.js',
        mode: 'client',
      },
      {
        src: 'leafletGeosearch.js',
        mode: 'client',
      },
    ],
  };

  const hooks = Object.keys(listPlugin);
  for (let i = 0; i < hooks.length; i++) {
    const hook = hooks[i];
    this.nuxt.hook(hook, () => {
      assignPlugins(listPlugin[hook], configOptions, this);
    });
  }
};

module.exports.meta = require('../package.json');
