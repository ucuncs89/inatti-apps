// use guide :
// cantumkan params :
// body: [ data excel  ( penamaan variable-variable sesuaikan dengan id header ) ] ,
// header: [ { data header excel : variable header untuk label header , variable id untuk binding dengan data body , variable width untuk estimasi width header } ] ,
// worksheetName: String : nama sheet ,
// fileName: string :  nama file yang akan di export ,
// format: formatType : format file yang akan di export ( csv & xlsx ) ,
// headerStyle: { align: String positioning text per cell : center , fontColor: String  warna text per cell , fontBold: Boolean , bgColor : warna background per cell , borderColor : warna border per cell }
// bodyStyle: { align: String positioning text per cell : center , fontColor: String  warna text per cell , fontBold: Boolean , bgColor : warna background per cell , borderColor : warna border per cell }

import * as ExcelJS from 'exceljs';
import { Context } from '@nuxt/types';

export default (_context: Context, inject: any) => {
  inject('excelJSExport', exportExcel);
};

interface headerDto {
  header: string;
  key: string;
  width: number;
}

enum formatType {
  csv = 'csv',
  xlsx = 'xlsx',
}

enum alignType {
  center = 'center',
}

interface StyleDto {
  align: alignType;
  fontColor: string;
  fontBold: boolean;
  bgColor: string;
  borderColor: string;
}

interface Payload {
  body: [];
  header: headerDto[];
  worksheetName: string;
  fileName: string;
  format: formatType;
  headerStyle: StyleDto;
  bodyStyle: StyleDto;
}

const exportExcel = async (params: Payload) => {
  const workbook = new ExcelJS.Workbook();
  const worksheet = workbook.addWorksheet(params.worksheetName);
  worksheet.columns = params.header;
  let index = 0;
  while (index + 1 !== params.body.length) {
    worksheet.addRow(params.body[index]);
    let rowItem = worksheet.getRow(index + 2);
    rowItem.eachCell((cell: any) => {
      cell.border = {
        top: { style: 'thin', color: { argb: params.bodyStyle.borderColor } },
        left: { style: 'thin', color: { argb: params.bodyStyle.borderColor } },
        bottom: {
          style: 'thin',
          color: { argb: params.bodyStyle.borderColor },
        },
        right: { style: 'thin', color: { argb: params.bodyStyle.borderColor } },
      };
      cell.alignment =
        params.bodyStyle.align === 'center'
          ? { vertical: 'middle', horizontal: 'center' }
          : {};
      cell.font = {
        color: { argb: params.bodyStyle.fontColor },
        bold: params.bodyStyle.fontBold,
      };
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: params.bodyStyle.bgColor },
      };
    });
    index++;
  }

  const rowHeader = worksheet.getRow(1);
  rowHeader.eachCell((cell) => {
    cell.border = {
      top: { style: 'thin', color: { argb: params.headerStyle.borderColor } },
      left: { style: 'thin', color: { argb: params.headerStyle.borderColor } },
      bottom: {
        style: 'thin',
        color: { argb: params.headerStyle.borderColor },
      },
      right: { style: 'thin', color: { argb: params.headerStyle.borderColor } },
    };
    cell.alignment =
      params.headerStyle.align === 'center'
        ? { vertical: 'middle', horizontal: 'center' }
        : {};
    cell.font = {
      color: { argb: params.headerStyle.fontColor },
      bold: params.headerStyle.fontBold,
    };
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: params.headerStyle.bgColor },
    };
  });
  const buffer = await createBuffer(params.format, workbook);

  if (buffer) {
    const blob = new Blob([buffer]);
    const url = URL.createObjectURL(blob);
    let a = document.createElement('a');
    a.href = url;
    a.download = `${params.fileName}.${params.format}`;
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }
};

const createBuffer = async (format: any, workbook: any) => {
  if (format === 'xlsx') {
    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;
  }
  if (format === 'csv') {
    const buffer = await workbook.csv.writeBuffer();
    return buffer;
  }
  if (!format) {
    return false;
  }
};
