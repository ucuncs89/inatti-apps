import { Context } from "@nuxt/types"

/**
 * Snackbar plugin to show snackbar using simple code.
 * how to use :
 * nb : if using Typescript, add `$snackbar!: (arg: string) => void` to the class
 *
 * this.$snackbar('message')
 *
 * can be used on client or server side
 */
export default (context: Context, inject: any) => {
  inject("snackbar", (payload: string | { text: string, options?: object }) => {
    if (typeof payload === "string") {
      payload = {
        text: payload
      }
    }
    const { store } = context
    store.dispatch('snackbarStore/showSnackbar', payload)
  })
}
