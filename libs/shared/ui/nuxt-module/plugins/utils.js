class Utils {
  toIdr(number) {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(number);
  }

  /** this function same as dateTimeFormat */
  toFormatDateTime(value) {
    return this.dateTimeFormat(value)
  }

  secondToMinutes(value) {
    let time = Math.trunc(value);
    var minutes = Math.floor(time / 60);
    var seconds = time - minutes * 60;
    return `${minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
  }

  toIdrDecimaless(number) {
    const formatter = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0,
      maximumFractionDigits: 20,
    })
      .format(number)
      .trim();
    return formatter;
  }

  validatePhoneNumber(phone) {
    const regex = /^[0-9]{4,20}$/;
    return regex.test(phone);
  }

  validateCountryCode(countryCode) {
    const regex = /^\+?[0-9]{1,3}$/;
    return regex.test(countryCode);
  }

  wait(durr) {
    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve('');
      }, durr);
    });
  }

  calculateAgeByBirthdate(val) {
    var today = new Date();
    var birthDate = new Date(val);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return `${age} Tahun`;
  }

  toDecimal(number) {
    return new Intl.NumberFormat('ja-JP', {
      style: 'currency',
      currency: 'JPY',
      currencyDisplay: 'code',
    })
      .format(number)
      .replace('JPY', '')
      .trim();
  }

  emailFormatRule(val) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(val);
  }

  validateImageSize(fileSize) {
    const bytesToMegaBytes = fileSize / 1024 ** 2;
    if (bytesToMegaBytes > 1) {
      return false;
    }
    return true;
  }

  dirtyValidation(oldVal, newVal) {
    if (JSON.stringify(oldVal) === JSON.stringify(newVal)) {
      return false;
    }
    return true;
  }

  dateTimeFormat(val) {
    let date = new Date(val);
    if(date.toString() === 'Invalid Date') return '-'
    return date.toLocaleString()
  }

  saveFile(params) {
    console.log(params);
    const fileBlob = new Blob([params.byteArray], {
      type: params.blobType,
    });
    let a = document.createElement('a');
    let url = URL.createObjectURL(fileBlob);
    a.href = url;
    a.download = `${params.fileName}.${params.extension}`;
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }

  cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  distanceConverter(val, opt) {
    const calculate = parseFloat(val) / opt.ratio;
    return calculate.toFixed(opt.prepended);
  }

  validateImageSize(fileSize) {
    const bytesToMegaBytes = fileSize / 1024 ** 2;
    if (bytesToMegaBytes > 1) {
      return false;
    }
    return true;
  }

  dateToFullText(val) {
    const date = new Date(val);
    const monthName = date.toLocaleString('id-ID', { month: 'long' });
    const dayName = date.toLocaleDateString('id-ID', { weekday: 'long' });
    return `${dayName} ${date.getDate()} ${monthName} ${date.getFullYear()}`;
  }

  dateToMonthText(val) {
    const date = new Date(val);
    const monthName = date.toLocaleString('id-ID', { month: 'long' });
    return `${date.getDate()} ${monthName} ${date.getFullYear()}`;
  }

  dateSimpleFormat(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
}

export default (_context, inject) => {
  inject('utils', new Utils());
};
