import { canAccess, haveAccess } from "./allow-permission"

// const examplePermission = [
//   "view:list_sample",
//   "view:sample",
//   "manage:extraction_session",
//   "view:extraction_session",
//   "create:extraction_session",
//   "update:extraction_session",
//   "delete:extraction_session",
//   "manage:analyze_session",
//   "view:analyze_session",
//   "create:analyze_session",
//   "update:analyze_session",
//   "delete:analyze_session",
//   "manage:analyze_session",
//   "update:analyze_session-result",
//   "view:analyze_session-result",
//   "manage:analyze_session-result",
//   "create:docter_result",
//   "update:docter_result",
//   "manage:docter_result",
//   "manage:send_result",
//   "manage:user_management",
//   "manage:report",
//   "manage:certificate",
//   "manage:lab_information",
//   "manage:lab_setting"
// ]

describe('Test allow permission, function haveAccess', () => {
  it('Should can validate default format', () => {
    let permission = [
      'view:sample'
    ]
    expect(haveAccess(permission, 'sample')).toBe(true)
    expect(haveAccess(permission, 'view:sample')).toBe(true)
    expect(haveAccess(permission, 'read:sample')).toBe(false)
  })
  it('Should can validate half format', () => {
    let permission = [
      'view:sample'
    ]
    expect(haveAccess(permission, 'sample')).toBe(true)
  })
  it('Should validate complete format', () => {
    let permission = [
      "create:analyze_session",
      "update:analyze_session",
      "delete:analyze_session",
    ]
    expect(haveAccess(permission, 'create:analyze_session')).toBe(true)
    expect(haveAccess(permission, 'update:analyze_session')).toBe(true)
    expect(haveAccess(permission, 'delete:analyze_session')).toBe(true)
    expect(haveAccess(permission, 'analyze_session')).toBe(true)
    expect(haveAccess(permission, 'view:analyze_session')).toBe(false)
  })
})

describe('Test allow permission, function canAccess', () => {
  it('Should allow if only string', () => {
    let permission = [
      "manage:user_management",
      "manage:report",
    ]
    expect(canAccess(permission, 'report')).toBe(true)
    expect(canAccess(permission, 'manage:report')).toBe(true)
    expect(canAccess(permission, 'view:report')).toBe(false)
    expect(canAccess(permission, 'user_management')).toBe(true)
    expect(canAccess(permission, 'manage:user_management')).toBe(true)
  })
  it('Should allow if array', () => {
    let permission = [
      "manage:user_management",
      "manage:report",
    ]
    expect(canAccess(permission, [
      'report',
      'user_management'
    ])).toBe(true)
    expect(canAccess(permission, [
      'manage:report',
      'manage:user_management'
    ])).toBe(true)
    expect(canAccess(permission, [
      'user_management',
      'test_other'
    ])).toBe(true)
    expect(canAccess(permission, [
      'test other'
    ])).toBe(false)
  })
  it('Should not allow if not in array', () => {
    let permission = [
      "manage:user_management",
      "manage:report",
    ]
    expect(canAccess(permission, [
      'test other'
    ])).toBe(false)
  })
  it('Should allow if in array', () => {
    let permission = [
      "manage:user_management",
      "manage:report",
    ]
    expect(canAccess(permission, [
      'manage:report',
      'manage:user_management'
    ])).toBe(true)
  })
})


