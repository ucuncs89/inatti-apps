import { Context } from '@nuxt/types'

class FormValidation {
  refs: any
  constructor(context: any) {
    this.refs = context
  }

  validate(componentContext: any, formName: string) {
    return (componentContext[formName] as Vue & { validate: () => boolean }).validate()
  }

  reset(componentContext: any, formName: string): any {
    return (componentContext[formName] as Vue & { reset: () => boolean }).reset()
  }

  resetValidation(componentContext: any, formName: string): boolean {
    return (
      componentContext[formName] as Vue & { resetValidation: () => boolean }
    ).resetValidation()
  }

  test(): boolean {
    return true
  }
}

export default (context: Context, inject: any) => {
  inject('form', new FormValidation(context))
}

