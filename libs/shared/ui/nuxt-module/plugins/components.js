// register component pool

import {
  UiCommonImageDropzonePreview,
  UiCommonImageDropzoneUpload,
  UiCommonDialog,
  UiFormSelectProvinsi,
  UiFormSelectKabupaten,
  UiFormSelectKecamatan,
  UiFormSelectKelurahan,
  UiFormSelectPhoneCode,
  UiFormSelectGender,
  UiFormSelectCountry,
  UiContainerMenuAccount,
  UiContainerMenuNavigation,
  UiContainerLayout,
  UiFormSelectStatusOrder,
  UiCommonSnackbarError,
  UiPageManagementUserForm,
  UiFormFieldAttachmentPicker,
  UiFormFieldAttachmentPreview,
  UiFormSelectApotekGrup,
  UiFormSelectApotek,
  UiFormSelectReference,
  UiChipStatusVaksin,
  UiTableFilterSelect,
  UiIdentityForgetPasswordBtn
} from '@inatti/shared/ui';

import Vue from 'vue';
Vue.component('ui-common-image-dropzone-preview', UiCommonImageDropzonePreview);
Vue.component('ui-common-image-dropzone-upload', UiCommonImageDropzoneUpload);
Vue.component('ui-common-dialog', UiCommonDialog);
Vue.component('ui-form-select-provinsi', UiFormSelectProvinsi);
Vue.component('ui-form-select-kabupaten', UiFormSelectKabupaten);
Vue.component('ui-form-select-apotek-grup', UiFormSelectApotekGrup);
Vue.component('ui-form-select-apotek', UiFormSelectApotek);
Vue.component('ui-form-select-reference', UiFormSelectReference);
Vue.component('ui-form-select-kecamatan', UiFormSelectKecamatan);
Vue.component('ui-form-select-kelurahan', UiFormSelectKelurahan);
Vue.component('ui-form-select-status-order', UiFormSelectStatusOrder);
Vue.component('ui-form-select-phone-code', UiFormSelectPhoneCode);
Vue.component('ui-form-select-gender', UiFormSelectGender);
Vue.component('ui-form-select-country', UiFormSelectCountry);
Vue.component('ui-container-menu-account', UiContainerMenuAccount);
Vue.component('ui-container-menu-navigation', UiContainerMenuNavigation);
Vue.component('ui-container-layout', UiContainerLayout);
Vue.component('ui-common-snackbar-error', UiCommonSnackbarError);
Vue.component('ui-page-management-user-form', UiPageManagementUserForm);
Vue.component('ui-form-field-attachment-picker', UiFormFieldAttachmentPicker);
Vue.component('ui-form-field-attachment-preview', UiFormFieldAttachmentPreview);
Vue.component('ui-chip-status-vaksin', UiChipStatusVaksin);
Vue.component('ui-table-filter-select', UiTableFilterSelect);
Vue.component('ui-identity-forget-password-btn', UiIdentityForgetPasswordBtn);
