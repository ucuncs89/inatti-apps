export default function (...params) {
  const { $axios, $snackbar, $auth } = params[0]
  if(!$axios) return;
  $axios.onError(async (error) => {
    if (error.response) {
      const code = parseInt(error.response && error.response.status)
      if (code === 401 || code === '401') {
        // auto redirect to login if call API is failed
        await $auth.logout()
      }
      if (error.response.data.message) {
        $snackbar(JSON.stringify(error.response.data.message) ?? code)
      } else {
        $snackbar(JSON.stringify(error.response.data) ?? code)
      }
    } else if (error.message) {
      $snackbar(error.message)
    } else {
      $snackbar(error)
    }
    return Promise.reject(error)
  })
}
