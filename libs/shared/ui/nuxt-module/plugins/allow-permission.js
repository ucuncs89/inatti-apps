import { isEqual } from 'lodash';

/**
 * Periksa akses yang tersedia di permission(array)
 * format permission :
 * - view:context
 * - create:context
 * jika akses hanya "context" saja,
 * maka akan periksa yang memiliki akses ke konteks apapun permissionnya
 */

const extractPermission = (permission) => {
  if (permission.split(':').length > 1) {
    const [prmType, prmContext] = permission.split(':');
    return {
      type: prmType,
      context: prmContext,
    };
  }
  return {
    context: permission,
  };
};

/**
 * @param {*} permission  array list permission
 * @param {*} access string or array
 * @returns
 */
export function canAccess(permission, access) {
  if (!permission) return false;
  if (!access) return false;
  if (Array.isArray(access)) {
    return access.some((requestAccess) => {
      haveAccess(permission, requestAccess);
    });
  }
  return haveAccess(permission, access);
}

/**
 * Jika di input $allow("context"), maka periksa permission yangm memiliki
 * nama context, type apapun
 */
export function haveAccess(permissions, access) {
  return permissions.some((permission) => {
    const allowedPermission = extractPermission(permission);
    const requestPermission = extractPermission(access);

    if (!requestPermission.type) {
      return allowedPermission.context === requestPermission.context;
    }
    return isEqual(allowedPermission, requestPermission);
  });
}

export default (context, inject) => {
  inject('allow', (access) => {
    const { store } = context;
    return canAccess(store.state.permission.allowed, access);
  });
};
