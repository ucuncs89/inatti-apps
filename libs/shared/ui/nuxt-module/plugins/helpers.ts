import * as dayjs from 'dayjs';
import * as localizedFormat from 'dayjs/plugin/localizedFormat';

class Helpers {
  elperFormatDate(date: string | Date) {
    if (!date) return '';
    dayjs.extend(localizedFormat);
    return dayjs(date).format('DD-MM-YYYY HH:mm:ss');
  }
}

export default (_context: any, inject: any) => {
  inject('helpers', new Helpers());
};
