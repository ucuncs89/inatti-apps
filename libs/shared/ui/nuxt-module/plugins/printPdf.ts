import { Context } from '@nuxt/types';
import { jsPDF } from 'jspdf';
import 'jspdf-barcode';
export default (_context: Context, inject: any) => {
  // eslint-disable-next-line new-cap
  inject('printPdf', new printPdf());
};

interface PayloadPrintSample {
  idSample: string;
  dateTest: string;
  birthDate: string;
  gender: string;
  referenceName: string;
  patientName: string;
}

let genderList: any = {
  lk: 'La',
  pr: 'Pe',
};

class printPdf {
  printSample(params: PayloadPrintSample[]) {
    if (!Array.isArray(params)) return;
    const paperWidth = 58;
    const paperHeight = 230; // pada bagian tinggi ini harus lebih dari ukuran kertas yang di tentukan pada popup / window print out nya
    // eslint-disable-next-line new-cap
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm',
      format: [paperWidth, paperHeight],
    });
    let index = 0;
    const pageHeight = paperHeight;
    const pageMargin = 0;
    const cardHeight = 35;
    const cardMargin = 0;
    let start = pageMargin + cardMargin;
    while (index < params.length) {
      if (start + cardHeight >= pageHeight - pageMargin) {
        doc.addPage();
        start = pageMargin + cardMargin;
      }
      this.createCardSample(doc, index, params, start, cardHeight);
      start = start + cardHeight + cardMargin;
      index++;
    }
    doc.autoPrint();
    const hiddFrame: any = document.createElement('iframe');
    hiddFrame.style.position = 'fixed';
    hiddFrame.style.width = '1px';
    hiddFrame.style.height = '1px';
    hiddFrame.style.opacity = '0.01';
    hiddFrame.src = doc.output('bloburl');
    document.body.appendChild(hiddFrame);
  }
  createCardSample(
    doc: any,
    index: number,
    params: any,
    start: number,
    cardHeight: number
  ) {
    let dataURL = '';
    const textLineHeight = 3;
    let canvas = document.createElement('canvas');
    if (params[index].idSample) {
      // QRCode.toCanvas(canvas, params[index].idSample, {
      //   type: 'image/jpeg',
      //   scale: 3.5,
      //   quality: 1,
      //   errorCorrectionLevel: 'H',
      // })
    }

    const pageWidth =
      doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

    const addTextRightSide = (
      items: { text: string; alignRight?: boolean }[]
    ) => {
      let top = start + 20;
      items.forEach((item, i) => {
        let posX = pageWidth / 2 - 18; // default
        if (item.alignRight) {
          posX = pageWidth - item.text.length * 2;
        }
        doc
          .setFontSize(8)
          .setFont('Courier', 'bold')
          .text(item.text, posX, top);
        top += textLineHeight;
      });
    };

    let textRightSide = [
      {
        text: `Id : ${params[index].idSample ? params[index].idSample : '-'}`,
      },
      {
        text: `Test: ${
          params[index].dateTest
            ? this.dateToYearMonthDay(params[index].dateTest)
            : '-'
        }`,
      },
      {
        text: `${
          params[index].patientName
            ? `${params[index].patientName.substring(0, 19)}${
                params[index].patientName.length > 19 ? '...' : ''
              }`
            : '-'
        }`,
      },
      {
        text: `${this.dateToYearMonthDay(params[index].birthDate)} / ${
          genderList[params[index].gender]
        }`,
      },
      {
        text: `${
          params[index].referenceName
            ? `${params[index].referenceName.substring(0, 19)}${
                params[index].referenceName.length > 19 ? '...' : ''
              }`
            : '-'
        }`,
      },
    ];

    addTextRightSide(textRightSide);
    if (params[index].idSample) {
      doc.barcode(
        params[index].idSample,
        this.barcodeOption(params[index].idSample, pageWidth, start)
      );
    }
    let lineTopX = start + cardHeight;
    doc.setFontSize(10).line(0, lineTopX, pageWidth, lineTopX);
  }
  barcodeOption(payload: any, pageWidth: number, start: number) {
    let fontSize = 40;
    let scale = 1.7;
    return {
      fontSize,
      textColor: '#000000',
      x: (pageWidth - fontSize) / scale,
      y: start + 15,
    };
  }
  convertPixcelToMilimeter(pixel: any) {
    let mm = 0.264583;
    return Math.floor(pixel * mm);
  }
  setOptimalFontSizeForCanvas(canvas: HTMLCanvasElement, text: string): void {
    const fontFamaly = 'Arial';
    const canvasWidth = canvas.width;
    const horizontalPaddingPtc = 0.1;

    // Set font size to 1px for calculation
    const ctx: any = canvas.getContext('2d');
    ctx.font = `1px ${fontFamaly}`;

    const fontSize =
      (canvasWidth * (1 - horizontalPaddingPtc)) / ctx.measureText(text).width;
    ctx.font = `${fontSize}px ${fontFamaly}`;
  }
  dateToYearMonthDay(date: any) {
    let d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('-');
  }
}
