import { TransactionCreateOrderByUserDTO } from '@inatti/shared/dtos';
import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class TransactionApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getOneV2(idOrderTrx: string) {
    return this.axios.$get('/transaction-v2/detail/order-trx/' + idOrderTrx);
  }

  getRefIntegrated(refId: string) {
    return this.axios.$get('/payment-v2/ref-integrate/transfer-detail', {
      params: { refId },
    });
  }

  createOrderLoggedIn(payload: TransactionCreateOrderByUserDTO) {
    return this.axios.$post('/transaction-v2/create/order/loggedin', payload)
  }

  createOrderGuest(payload: TransactionCreateOrderByUserDTO) {
    return this.axios.$post('/transaction-v2/create/order/guest', payload)
  }
}
