import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class ReferenceProductApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  get(refId: string, query?: any) {
    return this.axios.$get('/api-ref/setting/products/' + refId, {
      params: query,
    });
  }
  delete(refId: string, productId: number) {
    return this.axios.$delete(
      '/api-ref/setting/product/' + refId + `/${productId}`
    );
  }
  add(data: any) {
    return this.axios.$post('/api-ref/setting/products', data);
  }
  update(refId: string, productId: number, data: any) {
    return this.axios.$patch(
      '/api-ref/setting/product/' + refId + `/${productId}`,
      data
    );
  }

  getReportOne(productId: string, query?: any) {
    return this.axios.$get('/api-ref/dashboard-product/product/' + productId, {
      params: query,
    });
  }

  getReport(idref: any) {
    return this.axios.$get('/api-ref/dashboard-product/products', {
      params: {
        idref,
      },
    });
  }

  downloadReportTransactionPerPatient(query?: any) {
    return this.axios.$get(
      '/api-ref/dashboard-product/products/export/transaction-perpatient',
      {
        params: query,
        responseType: 'blob',
      }
    );
  }

  downloadReportTransaction(query?: any) {
    return this.axios.$get(
      '/api-ref/dashboard-product/products/export/transaction',
      {
        params: query,
        responseType: 'blob',
      }
    );
  }
}

export class ReferenceUserApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  get(query?: any) {
    return this.axios.$get('/api-ref/user-manage/users', {
      params: query,
    });
  }
  update(id: number, data: any, query?: any) {
    return this.axios.$patch('/api-ref/user-manage/user/' + id, data, {
      params: query,
    });
  }
  delete(id: string, query: any) {
    return this.axios.$delete('/api-ref/user-manage/user/' + id, {
      params: query,
    });
  }
  add(data: any, query?: any) {
    return this.axios.$post('/api-ref/user-manage/user', data, {
      params: query,
    });
  }
  getRole() {
    return this.axios.$get('/api-ref/user-manage/roles')
  }
}

export class ReferencePaymentV2 {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  syncProvider(refId: string) {
    return this.axios.$get(
      '/payment-v2/ref-integrate/transfer-detail/sync-provider',
      {
        params: {
          refId,
        },
      }
    );
  }

  synch(refId: string) {
    return this.axios.$get('/payment-v2/ref-integrate/transfer-detail', {
      params: {
        refId,
      },
    });
  }

  createAccount(data: { email: string }) {
    return this.axios.$post('/payment-v2/ref-integrate/create-account', data);
  }
}

export class ReferenceShiftApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  add(data: any) {
    return this.axios.$post('/api-ref/setting/shift', data);
  }

  update(refId: string, shiftId: number, data: any) {
    return this.axios.$patch(
      `/api-ref/setting/shift/${refId}/${shiftId}`,
      data
    );
  }

  get(refId: string) {
    return this.axios.$get('/api-ref/setting/shift/' + refId);
  }

  delete(refId: string, shiftId: number) {
    return this.axios.$delete(`/api-ref/setting/shift/${refId}/${shiftId}`);
  }
}

export class ReferenceTemplateApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  get(query?: any) {
    return this.axios.$get('/api-ref/setting/template', {
      params: query,
    });
  }

  add(data: FormData, query?: any) {
    return this.axios.$post('/api-ref/setting/template', data, {
      params: query,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  }

  updateInformation(data: any, query?: any) {
    return this.axios.$post('/api-ref/setting/template/information', data, {
      params: query,
    });
  }
}

export class ReferenceTransactionApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getListMethod() {
    return this.axios.$get('/api-ref/transaction/method-cash');
  }

  get(query?: any) {
    return this.axios.$get('/api-ref/transaction/list', {
      params: query,
    });
  }

  getOne(idTransaction: string, query?: any) {
    return this.axios.$get('/api-ref/transaction/detail/' + idTransaction, {
      params: query,
    });
  }

  getInvoice(invoiceId: string) {
    return this.axios.$get('/api-ref/transaction/print-invoice/' + invoiceId, {
      responseType: 'blob',
    });
  }

  updateStatusBulk(data: any) {
    return this.axios.$patch('/api-ref/transaction/edit-status/bulk', data);
  }

  sendNotificationSwab(data: any) {
    return this.axios.$post(
      '/api-ref/notification-swab/send-notification',
      data
    );
  }

  checkNotificationSwab(data: any) {
    return this.axios.$post(
      '/api-ref/notification-swab/check-notification',
      data
    );
  }

  downloadCertificateZip(query?: any) {
    return this.axios.$get('/api-ref/report-pdf/download-sertifikat/zip', {
      responseType: 'blob',
      params: query,
    });
  }

  downloadCertificate(query?: any) {
    return this.axios.$get('/api-ref/report-pdf/download-sertifikat', {
      responseType: 'blob',
      params: query,
    });
  }
}

export class ReferencePatientSwabApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  get(type: string, query?: any) {
    return this.axios.$get('/api-ref/patient-swab/' + type, {
      params: query,
    });
  }

  getOne(idSwab: string) {
    return this.axios.$get('/api-ref/patient-swab/' + idSwab);
  }

  getStatus(query?: any) {
    return this.axios.$get('/api-ref/patient-swab/status', {
      params: query,
    });
  }

  checkingOne(idSwab: string) {
    return this.axios.$patch('/api-ref/patient-swab/checkin/' + idSwab);
  }

  checkingBulk(data: any) {
    return this.axios.$patch('/api-ref/patient-swab-bulk/checkin', data);
  }

  updateCodeBooking(idSwab: number, code: string) {
    return this.axios.$patch(
      '/api-ref/patient-swab/code-booking/' + idSwab,
      null,
      {
        params: {
          code,
        },
      }
    );
  }

  updateByOrderId(orderId: string, data: any) {
    return this.axios.$patch('/api-ref/patient-swab/order/' + orderId, data);
  }

  updateSwabResult(idSwab: string, resultSwab: string) {
    return this.axios.$patch(
      '/api-ref/patient-swab/swab-result/' + idSwab,
      null,
      {
        params: {
          resultSwab,
        },
      }
    );
  }

  updateSwabResultBulk(data: any) {
    return this.axios.$patch('/api-ref/patient-swab-bulk/set-result', data);
  }

  swabNow(idSwab: string) {
    return this.axios.$patch('/api-ref/patient-swab/swab-now/' + idSwab);
  }

  swabNowBulk(data: any) {
    return this.axios.$patch('/api-ref/patient-swab-bulk/swab-now', data);
  }

  sendToLab(idPatient: string, data: any) {
    return this.axios.$patch(
      '/api-ref/patient-swab/swab-to-lab/' + idPatient,
      data
    );
  }

  sendToLabBulk(data: any) {
    return this.axios.$patch('/api-ref/patient-swab-bulk/swab-to-lab', data);
  }

  add(data: any) {
    return this.axios.$post('/api-ref/patient-swab/register', data);
  }
}

export class ReferenceSettingApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getOne(refId: string) {
    return this.axios.$get('/api-ref/setting/ref/' + refId);
  }

  update(refId: string, data: any) {
    return this.axios.$patch('/api-ref/setting/ref/' + refId, data);
  }

  updateThumbnail(refId: string, formData: FormData) {
    return this.axios.$patch(
      '/api-ref/setting/ref/' + refId + '/thumbnail',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }
    );
  }
}

export class ReferencePatientApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getLogActivity(query?: any) {
    return this.axios.$get('/logger-activity/sample', {
      params: query,
    });
  }

  searchPatient(query?: any) {
    return this.axios.$get('/api-ref/patient-search', {
      params: query,
    });
  }

  getOne(patientId: string) {
    return this.axios.$get('/api-ref/patient/' + patientId);
  }

  get(query?: any) {
    return this.axios.$get('/api-ref/patients', {
      params: query,
    });
  }

  add(data: any) {
    return this.axios.$post('/api-ref/patient', data);
  }

  update(patientId: string, data: any) {
    return this.axios.$patch('/api-ref/patient/' + patientId, data);
  }

  delete(patientId: string) {
    return this.axios.$delete('/api-ref/patient/' + patientId);
  }
}

export class ReferenceQuickFormApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  add(data: any) {
    return this.axios.$post('/api-ref/quick-form', data);
  }

  get(query?: any) {
    return this.axios.$get('/api-ref/quick-form', {
      params: query,
    });
  }

  delete(id: string) {
    return this.axios.$delete('/api-ref/quick-form/' + id);
  }
}

export class ReferenceApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getLab(refId: string) {
    return this.axios.$get('/api-ref/labs/' + refId);
  }

  getRoleUser(refId: string) {
    return this.axios.$get('/api-ref/role-user/' + refId);
  }

  getDashboard(query?: any) {
    return this.axios.$get('/api-ref/dashboard', {
      params: query,
    });
  }

  getRegisteredRef() {
    return this.axios.$get('/api-ref/registered-ref');
  }

  getRegisteredRefOne(refId: string) {
    return this.axios.$get('/api-ref/registered-ref/' + refId);
  }
}
