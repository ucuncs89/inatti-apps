import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class InfoApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getProductType() {
    return this.axios.$get('/info/available-product');
  }

  getReferenceDetails(refId: any) {
    return this.axios.$get('/info/reference/' + refId);
  }
}
