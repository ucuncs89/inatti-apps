import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class AccountApi {
  axios: NuxtAxiosInstance;

  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  getInfo() {
    return this.axios.$get('/account/me');
  }

  updateInfo(formData: any) {
    return this.axios.$patch('/account/update', formData);
  }

  updatePassword(oldPassword: string, newPassword: string) {
    return this.axios.$patch('/account/update/password', {
      oldPassword,
      newPassword,
    });
  }

  async refetchToken(roleId: string) {
    let res: { token: string } = await this.axios.$post('/login/tenancy', {
      roleId,
    })
    return res.token
  }
}

export class AccountUserManageApi {
  axios: NuxtAxiosInstance;

  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  updateDefaultPassword(id: string, key: any, newPassword: string) {
    return this.axios.$post('/user-manage/change-default-password', {
      id,
      key,
      newPassword,
    });
  }

  verifyToken(userId: any, key: any) {
    return this.axios.$get('/user-manage/verify-key', {
      params: { userId, key },
    });
  }
}

export class AccountResetPasswordApi {
  axios: NuxtAxiosInstance;

  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  searchAccount(username: string) {
    return this.axios.$get('/reset-password/search-account', {
      params: { username },
    });
  }

  requestResetPassword(idUser: string, sendTo: string) {
    return this.axios.$post(`/reset-password/request-reset-password`, {
      idUser,
      sendTo,
    });
  }

  updatePassword(otp: string, idUser: string, newPassword: string) {
    return this.axios.post(`/reset-password/update-password`, {
      otp,
      idUser,
      newPassword,
    });
  }
}
