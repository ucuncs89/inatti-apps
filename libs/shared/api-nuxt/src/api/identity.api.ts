import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class ApiIdentity {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  signupV2(registerApp: string, data: any) {
    const url = new URL(window.location.origin)
    url.pathname = '/user-auth/v2/signup'
    if (registerApp) {
      // for now only apotek is supported
      if (['Apotek'].includes(registerApp)) {
        url.searchParams.set('register-app', registerApp)
      }
    }
    return this.axios.$post(url.pathname + url.search, data);
  }

  verifyUsername(data: string | { username: string }) {
    const username = typeof data === 'string' ? data : data.username
    return this.axios.$get('/user-auth/v2/verify-username?username=' + username);
  }

  resendOtp() {
    return this.axios.$get('/user-auth/v2/resend/otp')
  }

  validateOtp(data: any) {
    return this.axios.$patch('/user-auth/v2/validate/otp', data)
  }

  signupApotek(data: any) {
    return this.axios.$post('/user-auth/v2/signup-apotek/setup', data)
  }
}
