import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class KemenkesApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getKemenkesOrangPassport(query?: any) {
    return this.axios.$get('kemenkes/orang/passport', {
      params: query,
    });
  }
}
