export interface DataApotekDTO {
  id?: string | null;
  name?: string;
  address?: string;
  description?: string;
  notActiveAt?: string | null;
  totalPmr?: any;
  deletedAt?: string | null;
  phonenumber?: string;
  thumbnail?: string;
  isActive?: boolean;
  coord?: string;
  kabupaten?: string;
  provinsi?: string;
  apotekGroup?: string;
  typeApotek?: string;
  suratIzinNo?: string;
  suratIzinFile?: string;
}

export interface DataApotekGroupDTO {
  name?: string;
  address?: string;
  description?: string;
  phonenumber?: string;
}

export interface DataUserDTO {
  fullname?: string;
  username?: string;
  email?: string;
  password?: string;
  roleId?: number;
}
