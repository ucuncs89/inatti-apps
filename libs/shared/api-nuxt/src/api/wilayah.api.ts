import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class WilayahApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getWilayahAll(query?: any) {
    return this.axios.$get('/wilayah/all', {
      params: query,
    });
  }

  getProvince() {
    return this.axios.$get('/wilayah/provinsi');
  }

  getKabupaten(provinsiId: number) {
    return this.axios.$get('/wilayah/kabupaten', {
      params: {
        provinsi_id: provinsiId,
      },
    });
  }
}
