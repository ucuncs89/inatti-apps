import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class PmrApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  get(id: string) {
    return this.axios.$get(`/patient-pmr/pmr/${id}`);
  }

  getList(query?: any) {
    return this.axios.$get('/patient-pmr/pmr/list', {
      params: query,
    });
  }
  
  delete(id: string) {
    return this.axios.$delete('/patient-pmr/pmr/' + id);
  }

  getHistoryByPatientId(id: string) {
    return this.axios.$get(`/patient-pmr/patient/${id}`);
  }

  async toPdf(id: string) {
    let res = await this.axios.$get(`/patient-pmr/pmr/${id}/pdf`, {
      responseType: 'blob',
    });
    this.download(res, `PMR_${id}_${new Date().getTime()}`);
  }

  download(res: any, name: string) {
    const link = document.createElement('a');
    const blob = new Blob([res]);
    const downloadUrl = URL.createObjectURL(blob);

    link.setAttribute('href', downloadUrl);
    link.setAttribute('download', `${name}.pdf`);
    document.body.appendChild(link);
    link.click();
  }

  getApotekPmrList(idApotek: string, params: any) {
    return this.axios.$get(`/patient-pmr/pmr/apotek/${idApotek}`, {
      params: { ...params },
    });
  }
}
