import { VaksinCreateOrderByUserDTO } from '@inatti/shared/dtos';
import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { Vaksin } from '@inatti/backend/database/vaksin/vaksin.entity';
import { Pagination } from '@inatti/shared/dtos';

export class VaksinApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  get(query?: any) {
    return this.axios.$get('/vaksin/list/', {
      params: query,
    });
  }

  getByRef(refId: string, query?: any) {
    return this.axios.$get('/vaksin/list-by-ref/' + refId, {
      params: query,
    });
  }

  getLogActivity(query?: any) {
    return this.axios.$get('/logger-activity/tag', {
      params: query,
    });
  }

  getOne(id: string) {
    return this.axios.$get('/vaksin/detail/' + id);
  }

  downloadExcel(refId: string, query?: any) {
    return this.axios.$get('/vaksin/get-excel/' + refId, {
      params: query,
      responseType: 'blob',
    });
  }

  downloadExcelByAdmin(query?: any) {
    return this.axios.$get('/vaksin/get-excel-by-admin', {
      params: query,
      responseType: 'blob',
    });
  }

  downloadPdf(idvaksin: string) {
    return this.axios.$get('/vaksin/get-pdf/' + idvaksin, {
      responseType: 'blob',
    });
  }

  downloadCertificateZip(query?: any) {
    return this.axios.$get('/vaksin/get-pdf-bulk/', {
      responseType: 'blob',
      params: query,
    });
  }

  orderVaksin(payload: VaksinCreateOrderByUserDTO) {
    return this.axios.$post('/vaksin/order-vaksin', payload);
  }

  listByRef(refId: string): Promise<Pagination<Vaksin>> {
    return this.axios.$get(`/vaksin/list-by-ref/${refId}`);
  }

  listByUser(): any {
    return this.axios.$get(`/vaksin/list-by-user/relations`);
  }

  detailVaksin(id: string): Promise<Vaksin> {
    return this.axios.$get(`/vaksin/detail/${id}`);
  }

  actionCheckin(id: Array<string>): Promise<Vaksin> {
    return this.axios.$put(`/vaksin/action/checkin`, null, {
      params: {
        id,
      },
    });
  }

  actionVaksin(id: Array<string>): Promise<Vaksin> {
    return this.axios.$put(`/vaksin/action/vaksin`, null, {
      params: {
        id,
      },
    });
  }

  actionNotifyWhatsapp(id: Array<string>): Promise<Vaksin> {
    return this.axios.$put(`/vaksin/action/notify-whatsapp`, null, {
      params: {
        id,
      },
    });
  }

  actionCheckNotifyWhatsapp(id: Array<string>): Promise<Vaksin> {
    return this.axios.$put(`/vaksin/action/check-notify-whatsapp`, null, {
      params: {
        id,
      },
    });
  }

  getStatus(vaksin: Vaksin) {
    // order from highest status
    if (vaksin.vaksinDateAt) return { text: 'Telah di vaksin', color: '#999' };
    if (vaksin.checkinDateAt) return { text: 'Telah Checkin', color: '#999' };
    return null;
  }
}
