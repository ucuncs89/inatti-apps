import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { DataApotekDTO, DataUserDTO } from './type';

export class ApotekApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  get(params?: any) {
    return this.axios.$get('/apotek', {
      params,
    });
  }

  getOne(id: string) {
    return this.axios.$get('/apotek/' + id);
  }

  getByGroup(groupId: string, withDeleted = false, params: any) {
    return this.axios.$get('/apotek/filter/by-group/', {
      params: {
        ...params,
        groupApotekId: groupId,
        withDeleted,
      },
    });
  }

  getConsultant(apotekId: string) {
    return this.axios.$get('/apoteker-info/apotek/' + apotekId);
  }

  getWhatsappPreMessage(patientId: string, apotekerId: string) {
    return this.axios.$get('/apoteker-info/generate/pre-message/whatsapp', {
      params: { patientId, apotekerId },
    });
  }

  getApoteker(params: any) {
    return this.axios.$get('/apoteker-info/find', {
      params,
    });
  }

  getApotekerDetails(apotekerId: string) {
    return this.axios.$get(`/apoteker-info/detail/${apotekerId}`);
  }

  update(id: string, data: DataApotekDTO) {
    return this.axios.$patch('/apotek/' + id, data);
  }

  create(data: DataApotekDTO) {
    return this.axios.$post('/apotek', data);
  }

  delete(id: string) {
    return this.axios.$delete(`/apotek/${id}`, {
      params: {
        isDelete: 1, // default
      },
    });
  }

  updateLogo(id: string, data: any) {
    return this.axios.$patch(`/apotek/${id}/logo`, data);
  }

  updateThumbnail(id: string, data: any) {
    return this.axios.$patch(`/apotek/${id}/thumbnail`, data);
  }

  getDashboard(apotekId: string, isForce = false) {
    return this.axios.$get(`/dashboard-report/apotek/${apotekId}`, {
      params: {
        isForce,
      },
    });
  }

  getDashboardV2(isForce = false) {
    return this.axios.$get(`/dashboard-report/apotek/v2/dashboard`, {
      params: {
        isForce,
      },
    });
  }

  getRegisteredApotek() {
    return this.axios.$get('/api-apotek/registered-apotek');
  }

  verifyApotek(apotekId: string, data: any) {
    return this.axios.$patch(`/apotek/${apotekId}/verify`, data)
  }

  updateUnitBisnis(apotekId: string, data: any) {
    return this.axios.$patch(`/apotek/${apotekId}/update-apotek-group`, data);
  }

  getApotekActivityLog(apotekId: string) {
    return this.axios.$get(
      `/logger-activity/tag?label=admin-verify-apotek&id=${apotekId}`
    );
  }

  getApotekContact(apotekId: string) {
    return this.axios.$get(`/apotek/${apotekId}/contact`);
  }

  sendNotifVerification(apotekId: string, data: any) {
    return this.axios.$patch(`apotek/${apotekId}/verify/notification`, data);
  }
}

export class ApotekUserApi {
  constructor(axios: NuxtAxiosInstance, apotekId: string) {
    this.axios = axios;
    this.apotekId = apotekId;
  }

  axios: NuxtAxiosInstance;
  apotekId!: string;

  getUsers() {
    return this.axios.$get(`/apotek/${this.apotekId}/user`);
  }

  getUser(id: number) {
    return this.axios.$get(`/apotek/${this.apotekId}/user/${id}`);
  }

  addUser(dataUser: DataUserDTO) {
    return this.axios.$post(`/apotek/${this.apotekId}/user`, dataUser);
  }

  updateUser(id: string, dataUser: DataUserDTO) {
    return this.axios.$patch(`/apotek/${this.apotekId}/user/${id}`, dataUser);
  }

  deleteUser(id: number) {
    return this.axios.$delete(`/apotek/${this.apotekId}/user/${id}`);
  }

  getRoles() {
    return this.axios.$get(`/apotek-user/roles`);
  }
}

export class ApotekerApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getInfo(params?: any) {
    return this.axios.$get('/api-apotek/account-apoteker', {
      params: params,
    });
  }

  getStatus() {
    return this.axios.$get('/apotek-status/user');
  }

  updateInfo(formData: any) {
    return this.axios.$patch('/api-apotek/account-apoteker', formData);
  }

  updatePhoto(formData: any, params?: any) {
    return this.axios.$patch('/api-apotek/account-apoteker/photo', formData, {
      params: params,
    });
  }
}
