import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class ApotekHeadofficeApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;

  getList() {
    return this.axios.$get<ApotekHeadofficeData[]>('/apotek-headoffice');
  }
  getOne(id: string) {
    return this.axios.$get<ApotekHeadofficeData>('/apotek-headoffice/' + id);
  }

  create(data: { name: string }) {
    return this.axios.$post('/apotek-headoffice/', data);
  }

  update(id: string, data: { name: string }) {
    return this.axios.$patch('/apotek-headoffice/' + id, data);
  }

  delete(id: string) {
    return this.axios.$delete('/apotek-headoffice/' + id);
  }

  getDashboardV2(isForce = false) {
    return this.axios.$get('/dashboard-report/apotek-headoffice/v2/dashboard', {
      params: {
        isForce,
      },
    });
  }
  getDashboardV2ByAdmin(queries: string, isForce = false) {
    return this.axios.$get(
      '/dashboard-report/apotek-headoffice/v2/dashboard/admin?' + queries,
      {
        params: {
          isForce,
        },
      }
    );
  }
}

export class ApotekHeadofficeUserApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }
  axios: NuxtAxiosInstance;
  getRoles() {
    return this.axios.$get('/apotek-headoffice-user/role');
  }

  getUsers(headofficeId: string) {
    let params = {
      apotekHeadoffice: headofficeId,
    };
    return this.axios.$get('/apotek-headoffice-user/user-manage', { params });
  }

  addUser(data: {}) {
    return this.axios.$post('/apotek-headoffice-user/user-manage', data);
  }

  getUserById(id: string) {
    return this.axios.$get('/apotek-headoffice-user/user-manage/' + id);
  }

  updateUser(id: string, data: {}) {
    return this.axios.$patch('/apotek-headoffice-user/user-manage/' + id, data);
  }

  deleteUser(id: string) {
    return this.axios.$delete('/apotek-headoffice-user/user-manage/' + id);
  }

  getListApotekGroup(headofficeId: string) {
    let params = {
      apotekHeadoffice: headofficeId,
    };
    return this.axios.$get('/apotek-group', { params });
  }
}

export interface ApotekHeadofficeData {
  id: string;
  name: string;
  createdAt: string;
  deleteAt: string;
}
