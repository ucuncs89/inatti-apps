/* eslint-disable @typescript-eslint/no-explicit-any */
import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class AdminApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  /* config-app */

  getAppConfig(name: string, category?: any) {
    return this.axios.$get('/api-admin/config-app/config', {
      params: { name, ...category },
    });
  }

  getNotificationTemplates() {
    return this.axios.$get('/api-admin/config-app/template-notify');
  }

  getWhatsappService() {
    return this.axios.$get('/api-admin/config-app/whatsapp-service');
  }

  updateAppConfig(idconfig: number, formData: any) {
    return this.axios.$patch(
      `/api-admin/config-app/update/${idconfig}`,
      formData
    );
  }

  updateAppConfigNotification(formData: any) {
    return this.axios.$patch('/api-admin/config-app/update-config', formData);
  }

  /* dashboard */

  getDashboard(params: any) {
    return this.axios.$get('/api-admin/dashboard', {
      params: { ...params },
    });
  }

  getDashboardTransaction(params?: Record<string, any>) {
    return this.axios.$get('/api-admin/dashboard/transaction', {
      params,
    });
  }

  /* lab */

  getLabs() {
    return this.axios.$get('/api-admin/lab');
  }

  getLabById(labId: string) {
    return this.axios.$get(`/api-admin/lab/${labId}`);
  }

  createLab(formData: any) {
    return this.axios.$post('/api-admin/lab', formData);
  }

  updateLab(labId: string, formData: any) {
    return this.axios.$patch(`/api-admin/lab/${labId}`, formData);
  }

  deleteLabById(labId: any) {
    return this.axios.$delete(`/api-admin/lab/${labId}`);
  }

  /* ref */

  getRefs() {
    return this.axios.$get('/api-admin/ref');
  }

  getRefById(id: any) {
    return this.axios.$get(`/api-admin/ref/${id}`);
  }

  createRef(formData: any) {
    return this.axios.$post('/api-admin/ref', formData);
  }

  updateRef(refId: string, formData: any) {
    return this.axios.$patch(`/api-admin/ref/${refId}`, formData);
  }

  deleteRefById(refId: any) {
    return this.axios.$delete(`/api-admin/ref/${refId}`);
  }

  /* lab-role */

  getLabRoles() {
    return this.axios.$get('/api-admin/lab-role/roles');
  }

  getRolesAssignedByLabId(labId: string) {
    return this.axios.$get(`/api-admin/lab-role/roles-assigned/${labId}`);
  }

  getRolesAssignedByLabUserId(userId: string) {
    return this.axios.$get(
      `/api-admin/lab-role/roles-assigned-to-user/${userId}`
    );
  }

  createRoleAssignedToLabUser(formData: any) {
    return this.axios.$post(
      '/api-admin/lab-role/roles-assign/add-user',
      formData
    );
  }

  updateRolesAssignedByLabId(userId: any, labId: string, roleId: any) {
    return this.axios.$patch('/api-admin/lab-role-role/roles-assign', {
      userId,
      labId,
      roleId,
    });
  }

  deleteRoleAssignedByLabId(id: any) {
    return this.axios.$delete(`/api-admin/lab-role/roles-assign/${id}`);
  }

  /* transaction */

  getAsignedLabsForTransaction() {
    return this.axios.$get('/api-admin/transaction/lab/assigned');
  }

  getAsignedRefsForTransaction() {
    return this.axios.$get('/api-admin/transaction/ref/assigned');
  }

  getTransactionById(transactionId: string) {
    return this.axios.$get(`/api-admin/transaction/detail/${transactionId}`);
  }

  getTransactionPerPatient(params: any) {
    return this.axios.$get('/api-admin/transaction/perpatient', {
      params: { ...params },
    });
  }

  getTransactionHistory(params: any) {
    return this.axios.$get('/api-admin/transaction/history', {
      params: { ...params },
    });
  }

  exportTransactionPerPatient(params: any) {
    return this.axios.$get('/api-admin/transaction/perpatient/export', {
      responseType: 'blob',
      params: { ...params },
    });
  }

  exportTransactionHistory(params: any) {
    return this.axios.$get('/api-admin/transaction/history/export', {
      responseType: 'blob',
      params: { ...params },
    });
  }

  /* ref-role */

  getRefRoles() {
    return this.axios.$get('/api-admin/ref-role/roles');
  }

  getRolesAssignedByRefUserId(userId: string) {
    return this.axios.$get(
      `/api-admin/ref-role/roles-assigned-to-user/${userId}`
    );
  }

  getRolesAssignedByRefId(refId: string) {
    return this.axios.$get(`/api-admin/ref-role/roles-assigned/${refId}`);
  }

  createRoleAssignedToRefUser(formData: any) {
    return this.axios.$post(
      '/api-admin/ref-role/roles-assign/add-user',
      formData
    );
  }

  updateRoleAssignedByRefId(userId: any, refId: string, roleId: any) {
    return this.axios.$patch('/api-admin/ref-role/roles-assign', {
      userId,
      refId,
      roleId,
    });
  }

  deleteRoleAssignedByRefId(id: any) {
    return this.axios.$delete(`/api-admin/ref-role/roles-assign/${id}`);
  }

  /* role-user */

  getRoleUser() {
    return this.axios.$get('/api-admin/role-user/');
  }

  /* admin-user */

  getAdminUserRoles() {
    return this.axios.$get('/api-admin/admin-user/roles');
  }

  getAdminUsers() {
    return this.axios.$get('/api-admin/admin-user/user');
  }

  getRolesAssignedByAdminUserId(userId: string) {
    return this.axios.$get(
      `/api-admin/admin-user/roles-assigned-to-user/${userId}`
    );
  }

  createRoleAssignedToAdminUser(formData: any) {
    return this.axios.$post(
      '/api-admin/admin-user/roles-assign/add-user',
      formData
    );
  }

  updateRoleAssignedByAdminUserId(userId: any, roleId: any) {
    return this.axios.$patch('/api-admin/admin-user/roles-assign', {
      userId,
      roleId,
    });
  }

  deleteRoleAssignedByAdminUserId(id: string) {
    return this.axios.$delete(`/api-admin/admin-user/roles-assign/${id}`);
  }

  /* user */

  getUserById(id: any) {
    return this.axios.$get(`/api-admin/user/${id}`);
  }

  updateUserInfoById(id: string, formData: any) {
    return this.axios.$patch(`/api-admin/user/${id}`, formData);
  }

  findUserByUsername(username: any) {
    return this.axios.$get('/api-admin/user/find-username', {
      params: { username },
    });
  }
}
