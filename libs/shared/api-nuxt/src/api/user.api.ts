/* eslint-disable @typescript-eslint/no-explicit-any */
import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class UserApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getUserRelation() {
    return this.axios.$get(`/api-user/relations/list`);
  }

  getSwabHistories(id: string) {
    return this.axios.$get(`/api-user/relations/detail-swab/${id}`);
  }

  getRelationDetails(id: string) {
    return this.axios.$get(`/api-user/relations/detail-info/${id}`);
  }

  getPatientTestResults() {
    return this.axios.$get('/api-user/patient/list');
  }

  getPatientDetail(id: string) {
    return this.axios.$get(`/api-user/patient/detail/${id}`);
  }
}

export class UserPaymentApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getPaymentChannels() {
    return this.axios.$get(`/api-user/payment/faspay/channel`);
  }
}

export class UserOrderApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getList() {
    return this.axios.$get('/api-user/order/list');
  }

  getDetails(orderId: string) {
    return this.axios.$get(`/api-user/order/detail/${orderId}`);
  }

  getForgottenOrder(formData: any) {
    return this.axios.$post('/api-user/order/forget', formData);
  }

  assignTransactionToUser(orderTrxId: string) {
    return this.axios.$patch('/api-user/order/assign-to-me', null, {
      params: { orderTrxId },
    });
  }

  findInvoice(inv: string) {
    return this.axios.$get('/api-user/order/search-inv', {
      params: { inv },
    });
  }

  getInvoicePdf(invoiceId: string) {
    return this.axios.$get(`/api-user/order/invoice-pdf/${invoiceId}`, {
      responseType: 'blob',
    });
  }
}

export class UserAuthApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  register(formData: any) {
    return this.axios.$post('/user-auth/signup', formData);
  }

  validateOtp(otp: string, username: string) {
    return this.axios.$post('/user-auth/validate/otp', { otp, username });
  }

  resendOtp(username: string, sendTo: string) {
    return this.axios.$post('/user-auth/resend/otp', { username, sendTo });
  }

  forgetPasswordSendOtp(username: string) {
    return this.axios.$get(
      '/user-auth/v2/forget-password/send-otp' + `?username=${username}`
    );
  }

  forgetPasswordChangePassword(password: string) {
    return this.axios.$post('/user-auth/v2/forget-password/change-password', {
      password,
    });
  }
}

export class UserTestCovidApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getCertificate(id: string) {
    return this.axios.$get(`/api-user/testcovid/print-certificate/${id}`, {
      responseType: 'blob',
    });
  }
}

export class UserFaskesApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getDetails(faskesId: string) {
    return this.axios.get(`/api-user/faskes/${faskesId}`);
  }

  getProducts(faskesId: string, type?: string) {
    return this.axios.$get(`/api-user/faskes/${faskesId}/products`, {
      params: { type },
    });
  }

  getAmount() {
    return this.axios.$get('api-user/faskes/count-faskes');
  }

  getLocationFiltered(params: any) {
    return this.axios.$get(`api-user/faskes/filter/location`, { params });
  }

  findQuickForm(id: string) {
    return this.axios.$get(`/api-user/faskes/quick-form/${id}`);
  }

  findNearLocation(product_id: any, near: any) {
    return this.axios.$get('api-user/faskes/', {
      params: { product_id, near },
    });
  }
}

export class PatientV2Api {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  getPatientsStringifiedQueries(queries: string) {
    return this.axios.$get('/patient-v2/list?' + queries);
  }

  getPatientDetailsById(id: string) {
    return this.axios.$get('/patient-v2/' + id);
  }

  updateDetails(id: string, formData: any) {
    return this.axios.$patch(`/patient-v2/${id}`, formData);
  }

  createByUser(formData: any) {
    return this.axios.$post('/patient-v2/by-user', formData);
  }

  deletePatient(id: string) {
    return this.axios.$delete(`/patient-v2/${id}`);
  }

  downloadExcelPatientList(stringifiedQuery: string) {
    return this.axios.$get('/patient-v2/export/to-xlsx?' + stringifiedQuery, {
      responseType: 'blob',
    });
  }
}
