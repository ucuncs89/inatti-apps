import { NuxtAxiosInstance } from '@nuxtjs/axios';

export class MedicinePublicApi {
  axios: NuxtAxiosInstance;

  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  getList(params: any = {}) {
    return this.axios.$get('/medicine/public', {
      params: { ...params },
    });
  }
  
  create(name: string) {
    return this.axios.$post('/medicine/public', { name });
  }

  update(id: number, name: string) {
    return this.axios.$patch('/medicine/public', { id, name });
  }

  delete(id: number) {
    return this.axios.$delete('/medicine/public', {
      params: { id },
    });
  }

  uploadExcel(form: any) {
    return this.axios.$post('/medicine/public/upload-excel', form);
  }
}

export class MedicineApotekBasedApi {
  axios: NuxtAxiosInstance;
  id: string | string[];

  constructor(axios: NuxtAxiosInstance, id: string | string[]) {
    this.axios = axios;
    this.id = id;
  }

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  getList(params: any = {},  withDeleted: number = 0) {
    return this.axios.$get('/medicine/apotek/', {
      params: {
        ...params,
        withDeleted
      },
    });
  }

  create(name: string) {
    return this.axios.$post(
      '/medicine/apotek',
      { name },
      { params: { idapotek: this.id } }
    );
  }

  update(id: number, name: string) {
    return this.axios.$patch(
      '/medicine/apotek',
      { id, name },
      { params: { idapotek: this.id } }
    );
  }

  delete(id: number) {
    return this.axios.$delete('/medicine/apotek', {
      params: { idapotek: this.id, id},
    });
  }

  uploadExcel(form: any) {
    return this.axios.$post('/medicine/apotek/upload-excel', form, {
      params: { idapotek: this.id },
    });
  }
}
