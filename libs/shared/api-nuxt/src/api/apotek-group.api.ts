import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { DataApotekGroupDTO, DataUserDTO } from './type';

export class ApotekGroupApi {
  constructor(axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  axios: NuxtAxiosInstance;

  get(withDeleted = false as boolean, apotekHeadoffice? : string) {
    return this.axios.$get('/apotek-group', {
      params: {
        withDeleted,
        apotekHeadoffice
      },
    });
  }

  getOne(id: string) {
    return this.axios.$get('/apotek-group/' + id);
  }

  update(id: string, data: DataApotekGroupDTO) {
    return this.axios.$patch('/apotek-group/' + id, data);
  }

  create(data: DataApotekGroupDTO) {
    return this.axios.$post('/apotek-group', data);
  }

  delete(id: string) {
    return this.axios.$delete(`/apotek-group/${id}`, {
      params: {
        isDelete: 1, // default
      },
    });
  }

  updateThumbnail(id: string, data: any) {
    return this.axios.$patch(`/apotek-group/${id}/thumbnail`, data);
  }

  getRegisteredApotekGroup(withDeleted = false) {
    return this.axios.$get('/api-apotek-group/registered-apotek-group', {
      params: {
        withDeleted,
      },
    });
  }

  getListBasedUser(withDeleted = false) {
    return this.axios.$get('/apotek-group/registered/to-user', {
      params: {
        withDeleted,
      },
    });
  }

  getListBasedHeadoffice() {
    return this.axios.$get('/apotek-group/based/headoffice')
  }

  getDashboard(apotekGroupId: string, isForce = false) {
    return this.axios.$get(`/dashboard-report/apotek-group/${apotekGroupId}`, {
      params: {
        isForce,
      },
    });
  }
  getDashboardV2(isForce = false) {
    return this.axios.$get(`/dashboard-report/apotek-group/v2/dashboard`, {
      params: {
        isForce,
      },
    });
  }
}

export class ApotekGroupUserApi {
  axios: NuxtAxiosInstance;
  apotekGroupId!: string;

  constructor(axios: NuxtAxiosInstance, apotekGroupId: string) {
    this.axios = axios;
    this.apotekGroupId = apotekGroupId;
  }

  getUsers() {
    return this.axios.$get(`/apotek-group/${this.apotekGroupId}/user`);
  }

  getUser(id: number) {
    return this.axios.$get(`/apotek-group/${this.apotekGroupId}/user/${id}`);
  }

  addUser(dataUser: DataUserDTO) {
    return this.axios.$post(
      `/apotek-group/${this.apotekGroupId}/user`,
      dataUser
    );
  }

  updateUser(id: string, dataUser: DataUserDTO) {
    return this.axios.$patch(
      `/apotek-group/${this.apotekGroupId}/user/${id}`,
      dataUser
    );
  }

  deleteUser(id: number) {
    return this.axios.$delete(`/apotek-group/${this.apotekGroupId}/user/${id}`);
  }

  getRoles() {
    return this.axios.$get(`/apotek-group-user/roles`);
  }
}
