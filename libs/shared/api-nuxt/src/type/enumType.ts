export enum NikTypeEnum {
  nik = 'nik',
  passport = 'passport',
}

export type ModeMapType = null | 'fill' | 'detail' | 'move' | 'idle';

export const TypeNikSelect = [
  { name: 'Nik', value: 'nik' },
  { name: 'Passport', value: 'passport' },
];

export enum GenderEnum {
  male = 'lk',
  female = 'pr',
}

export enum BloodTypeEnum {
  A = 'A',
  B = 'B',
  AB = 'AB',
  O = 'O',
}

export enum StatusSwabEnum {
  paid = 'paid',
  new = 'new',
  registered = 'registered',
  checkin = 'checkin',
  done = 'doneSwab',
  onLab = 'onLab',
  progressLab = 'progressLab',
  resultOutLab = 'resultOutLab',
  resultOutRef = 'resultOutRef',
}

export enum ResultSwabEnum {
  positif = 'Positif',
  negatif = 'Negatif',
}

export enum StatusOrderEnum {
  new = 'new',
  paymentReceived = 'paymentReceived',
  paymentFailed = 'paymentFailed',
  inProgress = 'inProgress',
  paid = 'paid',
  close = 'close',
  canceled = 'canceled',
}

export enum StatusVaksinEnum {
  registered = 'registered',
  checkin = 'checkin',
  vaccine = 'vaccine',
}

export enum MethodPaymentEnum {
  cash = 'Cash',
  transfer = 'Transfer',
}

export enum TelepharmacyRolesEnum {
  headoffice = 'headoffice',
  apotek = 'apotek',
  apotekGroup = 'apotek-group'
}
