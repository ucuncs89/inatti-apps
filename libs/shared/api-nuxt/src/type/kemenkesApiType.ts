/* eslint-disable camelcase */

export interface KemenkesApiOrang {
  id: string
  nama: string
  tgl_lahir: string
  jenis_kelamin: string
  nik: string
  ktp_alamat: string
  ktp_rw: string
  ktp_rt: string
  ktp_lokasi_kode: string
  domisili_alamat: string
  domisili_rw: string
  domisili_rt: string
  domisili_lokasi_kode: string
  telepon: string
  recorded_at: string
}
