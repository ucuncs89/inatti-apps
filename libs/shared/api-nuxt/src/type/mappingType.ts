import * as enumType from './enumType';

interface TypeMapping {
  text: string | number;
  color: string;
}

// Type to make strict for object type
type StatusSwabMapType = {
  [key in enumType.StatusSwabEnum]: TypeMapping;
};
type StatusOrderMappingType = {
  [key in enumType.StatusOrderEnum]: TypeMapping;
};

type StatusPaymentMappingType = {
  [key in enumType.StatusOrderEnum]: TypeMapping;
};

type StatusVaksinMapType = {
  [key in enumType.StatusVaksinEnum]: TypeMapping;
};

export const StatusVaksinMapping: StatusVaksinMapType = {
  registered: {
    text: 'Terdaftar',
    color: 'orange lighten-1',
  },
  checkin: {
    text: 'Checkin',
    color: 'blue lighten-1',
  },
  vaccine: {
    text: 'Sudah Vaksin',
    color: 'light-green lighten-1',
  },
};

export const StatusSwabMapping: StatusSwabMapType = {
  new: {
    text: 'Order Baru',
    color: 'orange lighten-1',
  },
  registered: {
    text: 'Terdaftar',
    color: 'orange lighten-1',
  },
  checkin: {
    text: 'Checkin',
    color: 'blue lighten-1',
  },
  paid: {
    text: 'Sudah Terbayar',
    color: 'blue darken-1',
  },
  doneSwab: {
    text: 'Telah di Swab',
    color: 'light-green lighten-1',
  },
  onLab: {
    text: 'Sedang di lab',
    color: 'deep-orange darken-2',
  },
  progressLab: {
    text: 'Sedang di proses di lab',
    color: 'light-blue darken-2',
  },
  resultOutLab: {
    text: 'Hasil keluar dari lab',
    color: 'green lighten-1',
  },
  resultOutRef: {
    text: 'Hasil keluar dari Rujukan',
    color: 'green lighten-1',
  },
};

export const StatusOrderMapping: StatusOrderMappingType = {
  new: {
    text: 'Order Baru',
    color: 'yellow darken-4',
  },
  paymentReceived: {
    text: 'Sudah diterima',
    color: 'green darken-4',
  },
  paymentFailed: {
    text: 'Pembayaran Gagal',
    color: 'deep-orange darken-4',
  },
  inProgress: {
    text: 'Sedang di proses',
    color: 'light-blue darken-4',
  },
  paid: {
    text: 'Sudah Terbayar',
    color: 'blue darken-1',
  },
  close: {
    text: 'Pembayaran ditutup',
    color: 'red darken-3',
  },
  canceled: {
    text: 'Pembayaran dibatalkan',
    color: 'red darken-4',
  },
};

export const StatusPaymentMapping: StatusPaymentMappingType = {
  new: {
    text: 'Pesanan Baru',
    color: '#82B1FF',
  },
  paymentReceived: {
    text: 'Order Diterima',
    color: '#1976D2',
  },
  paymentFailed: {
    text: 'Order Gagal',
    color: '#FF5252',
  },
  inProgress: {
    text: 'Dalam Proses',
    color: '#2196F3',
  },
  paid: {
    text: 'Lunas',
    color: '#4CAF50',
  },
  close: {
    text: 'Ditutup',
    color: '#8BC34A',
  },
  canceled: {
    text: 'Dibatalkan',
    color: '#FB8C00',
  },
};

export function MappingToSelect(mappingData: any) {
  return Object.keys(mappingData).map((k, i, arr: any) => {
    return {
      ...mappingData[k],
      id: k,
      name: mappingData[k].text,
    };
  });
}
