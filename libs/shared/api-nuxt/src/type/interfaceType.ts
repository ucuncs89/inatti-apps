export interface UserLogin {
  id: string;
  email: string;
  username: string;
  fullname: string;
}

export declare module ReadExtractionDTO {
  export interface ResultTool {
    id: number;
    resultRaw: any;
    resultSummary: object;
    createdAt: string;
  }

  export interface ResultDoctor {
    result: string;
    note: string;
    sample: PatientSwabDetailDTO.SwabDetail;
    // eslint-disable-next-line no-use-before-define
    sampleSession: Mapping;
    publishAt: string | Date;
    createdAt: string | Date;
  }
  export interface Mapping {
    id: number;
    locateRow: number;
    locateColumn: number;
    isControl?: any;
    controlType?: any;
    controlName: string;
    sample: PatientSwabDetailDTO.SwabDetail;
    resultTool: ResultTool;
    resultDoctor: ResultDoctor;
  }
  export interface AnalyzeTool {
    id: number;
    name: string;
  }
  export interface Session {
    id: number;
    name: string;
    rowTotal: number;
    columnTotal: number;
    createdAt: Date;
    tool: AnalyzeTool;
    mapping: Mapping[];
    result: any;
    resultUploadDate: Date;
    isExtracted: boolean;
    dateExtracted: Date;
  }
}

export interface ValueChartCfx96 {
  labels: any[];
  datasets: {
    label: string;
    data: any;
    fill: string;
    borderColor: string;
    tension: string;
  }[];
}

export interface LabAnalyzeTool {
  id: number;
  name: string;
}
export interface LabAnalyzeResultEntity {
  id: number;
  sampleSession: ReadExtractionDTO.Mapping;
  tool: LabAnalyzeTool;
  resultRaw: any;
  resultSummary: string;
  createdAt: string;
}

import { PatientDTO } from '@inatti/shared/dtos';
/* eslint-disable camelcase */
import * as enumType from './enumType';
export interface Provinsi {
  id: number;
  name: string;
}

export class ExtractionSessionMapping {
  locateRow?: string | number;
  locateColumn?: string | number;
  // Sample Id
  sample?: PatientSwabDetailDTO.SwabDetail | string | number;
  isControl?: boolean;
  controlType?: ControlTypeMapping;
  controlName?: string;
}

export class ExtractionSessionDTO {
  name?: string;
  rowTotal?: number;
  columnTotal?: number;
  lab?: string;
  mapping?: ExtractionSessionMapping[];
  tool?: number;
}

export enum ControlTypeMapping {
  positif = 'positif',
  negatif = 'negatif',
}

export interface TransactionHistory {
  id: string;
  method: enumType.MethodPaymentEnum;
  status: enumType.StatusOrderEnum;
  price: number;
  createdAt: Date;
  detailSwab: PatientSwabDetailDTO.SwabDetail;
  orderTrx: OrderTransaction;
}

export interface OrderTransaction {
  id: string;
  invId: string;
  totalPrice: number;
  feeAdmin?: number;
  status: enumType.StatusOrderEnum;
  method: enumType.MethodPaymentEnum;
  paymentChannelCode?: string;
  paymentChannelName?: string;
  trxId: string;
  createdAt: Date;
  expiredAt: Date;
}

export interface Kabupaten {
  id: number;
  name: string;
  provinsi: Provinsi;
}

export interface Kecamatan {
  id: number;
  name: string;
  kabupaten: Kabupaten;
}

export interface Kelurahan {
  id: string;
  name: string;
  kecamatan: Kecamatan;
}

export enum StatusNotifWA {
  QUEUE = 'queue',
  PROCESS = 'process',
  SENT = 'sent',
  FAILED = 'failed',
}

export declare module PatientVaksinDetailDto {
  export interface Notify {
    id?: number;
    status?: StatusNotifWA;
    messageId?: string;
    message?: string;
    createdAt?: Date;
  }

  export interface VaksinDetail {
    id: string;
    requestDate?: Date | string;
    checkinDateAt?: Date | string;
    vaksinDateAt?: Date | string;
    createdAt: Date | string;
    patient: PatientSwabDetailDTO.Patient;
    ref: Reference;
    order: Order;
    orderTrx: OrderTrx;
    notify?: Notify;
  }

  export interface OrderTrx {
    id: string;
    trxId: string;
    invId: string;
    status: enumType.StatusOrderEnum;
    totalPrice: number;
    feeAdmin?: number;
    method: enumType.MethodPaymentEnum;
    paymentChannelCode?: string;
    paymentChannelName?: string;
    createdAt: Date | string;
    expiredAt: Date | string;
  }

  export interface Order {
    id: string;
    price?: number;
    method: enumType.MethodPaymentEnum;
    status: enumType.StatusOrderEnum;
    createdAt: Date | string;
    product?: InfoReferenceDTO.Product;
  }

  export interface Reference {
    id: string;
    name: string;
    tenantId?: string;
    contactWhatsapp?: string;
    refAddress?: string;
    refAddressPoint?: string;
    image?: string;
    narFaskesId?: string;
    createdAt: Date | string;
    isDelete?: boolean;
    deletedAt?: Date | string;
  }
}

export declare module PatientSwabDetailDTO {
  export interface Patient {
    id: string | null;
    nik: string | null;
    passport?: string;
    type: enumType.NikTypeEnum | null;
    name: string | null;
    height: string | number | null;
    weight: string | number | null;
    job: string | null;
    placebirth: string | null;
    datebirth: string | null;
    gender: enumType.GenderEnum | string | null;
    phonenumber: string | null;
    email: string | null;
    bloodtype: enumType.BloodTypeEnum | null;
    createdAt: Date | null;
    provinsi: Provinsi | number | null;
    kabupaten: Kabupaten | number | null;
    kecamatan: Kecamatan | number | null;
    kelurahan: Kelurahan | number | null;
    kelurahanRw: number | string | null;
    kelurahanRt: number | string | null;
    address: string | null;
    domisiliProvinsi: Provinsi | number | null;
    domisiliKabupaten: Kabupaten | number | null;
    domisiliKecamatan: Kecamatan | number | null;
    domisiliKelurahan: Kelurahan | number | null;
    domisiliKelurahanRw: number | string | null;
    domisiliKelurahanRt: number | string | null;
    addressDomisili: string | null;
    citizenship: string | null;
    narOrangId: string | null;
    detailSwab: Array<TestRecord> | null;
    ownedBy: Object | null;
  }

  export interface ProductSwab {
    id: number;
    name: string;
    description: string;
  }

  export interface RefTarget {
    id: string;
    name: string;
    tenantId: string;
    createdAt: Date;
  }

  export interface RefShiftSchedule {
    id: number;
    name: string;
    day: number;
    timeStart: string;
    timeEnd: string;
  }

  export interface LabDetail {
    id: number;
    name: string;
  }

  export interface OrderSwab {
    id: string;
    price: number;
    method: enumType.MethodPaymentEnum;
    status: enumType.StatusOrderEnum;
    createdAt: Date;
    // eslint-disable-next-line no-use-before-define
    detailSwab: SwabDetail;
    patient?: Patient;
    detailOrderProduct?: SwabDetail;
    methodCash?: string;
  }

  export interface WhatsappNotify {
    id?: number;
    swabDetail: SwabDetail;
    status?: StatusNotifWA;
    messageId?: string;
    message?: string;
    createdAt?: Date;
  }

  export interface SwabDetail {
    id: number;
    status: enumType.StatusSwabEnum;
    swabDate?: Date;
    resultSwab?: enumType.ResultSwabEnum;
    isSendToLab?: boolean;
    sendToLabDate?: Date;
    createdAt: Date;
    patient: Patient;
    productSwab?: ProductSwab;
    refTarget?: RefTarget;
    refShiftSchedule?: RefShiftSchedule;
    swabBy: string;
    labTarget: LabDetail;
    requestSwabDate: Date;
    orderSwab?: OrderSwab;
    sampleId: string;
    codeBooking: number | string;
    resultSentAt: Date;
    history: any;
    narSample: any;
    notifyWhataspp?: WhatsappNotify;
  }

  export interface TestRecord {
    id: number;
    sampleId: string;
    status: enumType.StatusSwabEnum;
    requestSwabDate: Date;
    swabDate?: Date;
    resultSwab?: enumType.ResultSwabEnum;
    resultSentAt?: Date;
    isSendToLab?: boolean;
    sendToLabDate?: Date;
    createdAt: Date;
    narSendAt?: Date;
    narTestCovidId?: string;
    codeBooking?: string;
    productSwab: ProductSwab;
  }
}

export declare module InfoReferenceDTO {
  export interface ProductDetail {
    id: number;
    name: string;
    description: string;
  }

  export interface Product {
    id: number;
    price: number;
    name: string;
    product: ProductDetail;
    description?: string;
    // eslint-disable-next-line no-use-before-define
    ref: InfoReference;
    methodPayments: any;
  }

  export interface Shift {
    id: number;
    name: string;
    day: number;
    timeStart: string;
    timeEnd: string;
    // eslint-disable-next-line no-use-before-define
    reference: InfoReference;
  }

  export interface InfoReference {
    id: string;
    name: string;
    tenantId: string;
    createdAt: Date;
    products: Product[];
    shifts: Shift[];
  }

  export interface QuickForm {
    id: string;
    name: string;
    expiredAt: string;
    createdAt: string;
  }
}

export declare module TransactionDetailDTO {
  export interface Customer {
    id: string;
    fullname: string;
    username: string;
    password: string;
    isActive: Boolean;
    email: string;
    phoneNumber: number;
    isAnonymous: Boolean;
  }
  export interface Patient {
    nik: string;
    name: string;
    placebirth: string;
    datebirth: string;
    gender: enumType.GenderEnum;
    phonenumber: string;
    address: string;
  }
  export interface TransactionDetail {
    methodCash: any;
    id: string;
    trxId: string;
    invId: string;
    status: enumType.StatusOrderEnum;
    totalPrice: number;
    method: enumType.MethodPaymentEnum;
    paymentChannelCode: string;
    paymentChannelName: string;
    createdAt: Date;
    expiredAt: Date;
    customer: Customer;
    reference: InfoReferenceDTO.InfoReference;
    product: InfoReferenceDTO.Product;
    totalOrder: number;
    orderSwab: PatientSwabDetailDTO.OrderSwab[];
  }
  export interface TransactionDetailPerPatient {
    id: string;
    price: number;
    method: enumType.MethodPaymentEnum;
    status: enumType.StatusOrderEnum;
    createdAt: Date;
    detailSwab: SwabDetail;
    customer: Customer;
    reference: InfoReferenceDTO.InfoReference;
    product: InfoReferenceDTO.Product;
    orderTrx: OrderTransfer;
  }
  export interface OrderTransfer {
    id: string;
    trxId: string;
    invId: string;
    status: enumType.StatusOrderEnum;
    totalPrice: number;
    feeAdmin?: number;
    method: enumType.MethodPaymentEnum;
    paymentChannelCode?: string;
    paymentChannelName?: string;
    createdAt: Date;
    expiredAt?: Date;
  }
  export interface SwabDetail {
    id: number;
    sampleId?: string;
    status: enumType.StatusSwabEnum;
    swabDate?: string;
    sendToLabDate?: Date;
    patient: Patient;
    resultSwab?: enumType.ResultSwabEnum;
    resultSentAt: Date;
  }
}
export interface UserLogin {
  id: string;
  email: string;
  username: string;
  fullname: string;
}

export interface User {
  id: string;
  fullname: string;
  username: string;
  email: string;
}
export interface Role {
  id: number;
  name: string;
  description: string;
}
export interface UserRole {
  id: number;
  userId: User;
  roleId: Role;
}
export interface PaginationResult<Type> {
  results: Type[];
  pageTotal: number; // total perpage
  total: number; // total all data
  page: number;
  pageLength: number; // number of page
}

export declare module LogActivityDTO {
  export interface type {
    id: number;
    name: string;
    activity: string;
    area: string;
    createdAt: string;
  }

  export interface logActivity {
    id: number;
    createdAt: string;
    type: type;
    user: User;
    formatted_activity: string;
  }
}
export interface ObatApotekData {
  id: number;
  name: string;
  createdAt: Date | string | null;
  insertByApotek: {
    id: string;
    name: string;
  };
  insertBy: {
    id: string;
    fullname: string;
    username: string;
  };
}

export interface ObatPublicData {
  id: number;
  name: string;
  createdAt: Date | string | null;
}

export interface ApotekGroup {
  id: string;
  name: string;
  address: string;
  description: string;
  phonenumber: string;
  thumbnail: string;
  createdAt: Date | string;
  deleteAt: Date | string | null;
  headOffice?: HeadOffice;
}

export interface HeadOffice {
  createdAt: string;
  deleteAt?: string;
  id: string;
  name: string;
}

export interface Apotek {
  id: string;
  name: string;
  deletedAt?: string;
  createdAt?: string;
  address: string;
  phonenumber?: string;
  apotekGroup?: ApotekGroup;
}
