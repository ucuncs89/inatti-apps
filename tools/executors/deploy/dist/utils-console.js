"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.LogInfoOptions = exports.LogReadyRun = exports.HighlightText = exports.LogExecutor = void 0;
var chalk_1 = __importDefault(require("chalk"));
var log = console.log;
var LogExecutor = function (msg, type) {
    if (type === void 0) { type = 'info'; }
    var typePrefix = {
        info: chalk_1["default"].gray('Info'),
        success: chalk_1["default"].green('Succes'),
        warning: chalk_1["default"].yellow('Warning'),
        error: chalk_1["default"].red('Error')
    };
    return log("".concat(typePrefix[type], " : ").concat(msg));
};
exports.LogExecutor = LogExecutor;
/** Helper log */
exports.HighlightText = chalk_1["default"].yellowBright;
var LogReadyRun = function (projectName, typeDeploy) {
    return (0, exports.LogExecutor)("Project ".concat((0, exports.HighlightText)(projectName), ", ready to run ").concat((0, exports.HighlightText)(typeDeploy)));
};
exports.LogReadyRun = LogReadyRun;
var LogInfoOptions = function (option) {
    (0, exports.LogExecutor)(chalk_1["default"].green(chalk_1["default"].bgGray("Print Info options")));
    Object.keys(option).forEach(function (key) {
        (0, exports.LogExecutor)("".concat(chalk_1["default"].yellow(key), " : ").concat(JSON.stringify(option[key])));
    });
};
exports.LogInfoOptions = LogInfoOptions;
