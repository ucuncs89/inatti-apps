"use strict";
exports.__esModule = true;
exports.DeployStrategy = void 0;
var deploy_backend_1 = require("./deploy-backend");
var deploy_frontend_csr_1 = require("./deploy-frontend-csr");
var deploy_frontend_ssr_1 = require("./deploy-frontend-ssr");
exports.DeployStrategy = {
    'frontend-csr': deploy_frontend_csr_1.DeployFrontendCsr,
    'frontend-ssr': deploy_frontend_ssr_1.DeployFrontendSsr,
    'backend': deploy_backend_1.DeployBackend
};
