"use strict";
exports.__esModule = true;
exports.getFolderDist = exports.getProjectConfig = void 0;
function getProjectConfig(context) {
    return context.workspace.projects[context.projectName];
}
exports.getProjectConfig = getProjectConfig;
function getFolderDist(context, isFolderDist) {
    if (isFolderDist === void 0) { isFolderDist = false; }
    var projectConfig = getProjectConfig(context);
    var folderDist = projectConfig.targets.build.options.buildDir;
    if (isFolderDist) {
        folderDist += "/dist";
    }
    return folderDist;
}
exports.getFolderDist = getFolderDist;
