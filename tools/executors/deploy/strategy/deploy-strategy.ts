import { ExecutorContext } from "nx/src/config/misc-interfaces";
import { DeployExecutorOptions } from "../type";
import { DeployBackend } from "./deploy-backend";
import { DeployFrontendCsr } from "./deploy-frontend-csr";
import { DeployFrontendSsr } from "./deploy-frontend-ssr";

export const DeployStrategy:Record<string, (options:DeployExecutorOptions, context: ExecutorContext) => Promise<any> | any> = {
  'frontend-csr': DeployFrontendCsr,
  'frontend-ssr': DeployFrontendSsr,
  'backend': DeployBackend
}