import { exec } from "child_process"
import { runExecutor } from "nx/src/command-line/run"
import { ExecutorContext } from "nx/src/config/misc-interfaces"
import { DeployExecutorOptions } from "../type"
import { LogExecutor } from "../utils-console"

export const deployApp = async (options: DeployExecutorOptions, context: ExecutorContext) => {
  const targetExecutorBuild:{
    project:string,
    target:string,
    configuration?: string
  } = { project: context.projectName, target: 'build'}

  if(options.deployEnvirontment === 'production'){
    targetExecutorBuild.configuration = 'production'
  }
  
  let executors = await Promise.all([
    await runExecutor(
      targetExecutorBuild,
      {},
      context),
  ])

  for await (const res of executors) {
    if (!res) return res;
  }
  return true
}

/**
 * Run pm2 instance, options pm2 must be filled
 * and make sure name in pm2 same as name apps in ecosystem.config.js
 */
export const runPm2 = (options: DeployExecutorOptions, context: ExecutorContext) => {
  if(!options.pm2){
    throw new Error("Options pm2 not filled")
  }
  const pm2Name = options.pm2[options.deployEnvirontment]
  const commandPm2 = `pm2 restart ecosystem.config.js --only ${pm2Name}`
  LogExecutor(`Read run command PM2 : ${commandPm2}`, 'info')
  exec(commandPm2)
}
