import { ExecutorContext } from "nx/src/config/misc-interfaces";
import { DeployExecutorOptions } from "../type";
import { deployApp, runPm2 } from "./utils-strategy";

export async function DeployBackend(options: DeployExecutorOptions, context: ExecutorContext){
  const res = await deployApp(options, context) 
  if(!res) return res
  runPm2(options, context)
  return true;
}