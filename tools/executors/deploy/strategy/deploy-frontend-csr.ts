import { runExecutor } from "nx/src/command-line/run";
import { ExecutorContext } from "nx/src/config/misc-interfaces";
import { WEB_STATIC_FOLDER } from "../const";
import { DeployExecutorOptions } from "../type";
import { copy, ensureDir } from 'fs-extra'
import { getFolderDist } from "../utils";
import { LogExecutor } from "../utils-console";

export async function DeployFrontendCsr(options: DeployExecutorOptions, context: ExecutorContext){  
  let executors = await Promise.race([
    await runExecutor(
      { project: context.projectName, target: 'static' },
      {}, 
      context)
  ])

  for await (const res of executors) {
    if (!res.success) return res;
  }
  LogExecutor('Finish generate static', 'success')
  
  // copy dist result to static folder 
  const dest = `${WEB_STATIC_FOLDER}/${options.subdomain[options.deployEnvirontment]}`

  const folderDist = getFolderDist(context, true)
  LogExecutor(`Copy ${folderDist} to ${dest}`)
  try{
    await ensureDir(dest)
    await copy(folderDist, dest)
    LogExecutor(`Finish Copy ${context.projectName} to ${dest}`, 'success')
    return true;
  } catch(error){
    LogExecutor(`Failed Copy ${context.projectName} to ${dest}`, 'error')
    LogExecutor(error, 'error')
    return false
  }
}