export interface DeployExecutorOptions {
  deployEnvirontment: "staging" | "production"; // staging, production
  deployType: "backend" | "frontend-csr" | "frontend-ssr";
  subdomain?: Record<string, string>;
  pm2?: Record<string, string>
}