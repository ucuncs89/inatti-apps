import type { ExecutorContext } from '@nrwl/devkit';
import { DeployStrategy } from './strategy/deploy-strategy';
import { DeployExecutorOptions } from './type';
import { HighlightText, LogExecutor, LogInfoOptions, LogReadyRun } from './utils-console';

const defaultOptions:DeployExecutorOptions = {
  deployEnvirontment: 'staging',
  deployType: 'frontend-csr'
}

export default async function deployExecutor(
  options: DeployExecutorOptions,
  context: ExecutorContext
): Promise<{ success: boolean }> {
  if(!options.deployEnvirontment && process.env.DEPLOY_ENV){
    LogExecutor(`${HighlightText('deployEnvirontment')} are not set, get from ${HighlightText('DEPLOY_ENV')}`)
    //@ts-ignore
    options.deployEnvirontment = process.env.DEPLOY_ENV
  }
  options = Object.assign({}, defaultOptions, options)
  LogReadyRun(context.projectName, options.deployType)
  LogInfoOptions(options)
  let isSuccess = await DeployStrategy[options.deployType](options, context)

  return { success: isSuccess };
}