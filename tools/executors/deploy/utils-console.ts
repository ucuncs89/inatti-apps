import chalk from 'chalk'
import { DeployExecutorOptions } from './type'

const log = console.log

type TypeLog = 'info' | 'success' | 'warning' | 'error'

export const LogExecutor = (msg: string, type: TypeLog = 'info') => {
  const typePrefix:Record<TypeLog, String> = {
    info: chalk.gray('Info'),
    success: chalk.green('Succes'),
    warning: chalk.yellow('Warning'),
    error: chalk.red('Error')
  }
  return log(`${typePrefix[type]} : ${msg}`)
}

/** Helper log */
export const HighlightText = chalk.yellowBright

export const LogReadyRun = (projectName: string, typeDeploy:string) => 
  LogExecutor(`Project ${HighlightText(projectName)}, ready to run ${HighlightText(typeDeploy)}`)

export const LogInfoOptions = (option: DeployExecutorOptions) => {
  LogExecutor(chalk.green(chalk.bgGray("Print Info options")))
  Object.keys(option).forEach((key) => {
    LogExecutor(`${chalk.yellow(key)} : ${JSON.stringify(option[key])}`)
  })
}