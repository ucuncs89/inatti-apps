import { ExecutorContext } from "nx/src/config/misc-interfaces";

export function getProjectConfig(context: ExecutorContext){
  return context.workspace.projects[context.projectName]
}

export function getFolderDist(context: ExecutorContext, isFolderDist = false){
  const projectConfig = getProjectConfig(context)
  let folderDist = projectConfig.targets.build.options.buildDir
  if(isFolderDist){
    folderDist += "/dist"
  }
  return folderDist
}