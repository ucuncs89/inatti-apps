import { getJestProjects } from '@nrwl/jest';

export default {
  projects: getJestProjects(),
  testEnvironment: 'jest-environment-node',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};
